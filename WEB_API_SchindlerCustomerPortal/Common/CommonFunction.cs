﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;
using WEB_API_SchindlerCustomerPortal.Controllers;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Common
{
    public class CommonFunction
    {
        public string setNullToEmpty(string strData)
        {
            return (string.IsNullOrEmpty(strData) ? "" : strData);
        }

        public string setDateToshort(string dtData)
        {
            return (string.IsNullOrEmpty(dtData) ? "" : dtData.Split(' ')[0]);
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public void SendCalendarEmail(CalendarEventEntity model)
        {
            CommonData CD = new CommonData();

            try
            {
                EditUserDataController edituser = new EditUserDataController();
                UserDAL UD = new UserDAL();
                UserEntity senderData = edituser.GetUserById(Convert.ToInt32(model.MailSenderID));
                string senderName = senderData.FirstName + " [" + senderData.Email.ToString() + "]";

                foreach (var item in model.SendTo.Trim().Split(','))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        UserEntity userResult = UD.Find(item.Trim());
                        StaticUtilities SU = new StaticUtilities();
                        string CalendarUrl = WebConfigurationManager.AppSettings["BaseURL"].ToString() + WebConfigurationManager.AppSettings["CalendarUrl"].ToString();

                        #region Email Send

                        string SFrom = WebConfigurationManager.AppSettings["AccountMail"].ToString();
                        
                        string Sbody = null;

                        Sbody = "Subject: " + model.Subject;
                        Sbody += "<br/><br/>Start Date: " + model.StartDate + "<br/>End Date: " + model.EndDate;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", userResult.FirstName);
                        dic.Add("SenderName", senderName);
                        dic.Add("MessageURL", CalendarUrl);
                        dic.Add("MessageBody", Sbody);

                        Sbody = SU.getEmailBody("Calendar", model.EmailTemplateName, dic);

                        //SendEmail.SendMailtoMultiple(SFrom, item.Receiver, model.CC, model.BCC, Subject, Sbody);

                        SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item, model.EmailHeaderSubject, Sbody);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendCalendarEmail");
                throw ex;
            }
        }


        public Tuple<double, double> GenerateLatLong(string address)
        {
            string secureKey = WebConfigurationManager.AppSettings["GoogleLocationKey"].ToString();
            double latitude = 0.00;
            double longitude = 0.00;
            try
            {
                var requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address=" + Uri.EscapeDataString(address) + "&key=" + secureKey);

                var request = WebRequest.Create(requestUri);
                var response = request.GetResponse();
                var xdoc = XDocument.Load(response.GetResponseStream());

                if (xdoc.Element("GeocodeResponse").Element("status").Value == "OK")
                {
                    var result = xdoc.Element("GeocodeResponse").Element("result");
                    var locationElement = result.Element("geometry").Element("location");

                    latitude = Convert.ToDouble(locationElement.Element("lat").Value);
                    longitude = Convert.ToDouble(locationElement.Element("lng").Value);
                }
                else if (xdoc.Element("GeocodeResponse").Element("status").Value == "OVER_QUERY_LIMIT")
                {
                    return new Tuple<double, double>(0, 0);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<double, double>(0, 0);
            }
            return new Tuple<double, double>(latitude, longitude);
        }
    }
}