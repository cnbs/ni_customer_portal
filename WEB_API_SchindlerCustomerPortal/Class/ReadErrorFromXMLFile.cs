﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace WEB_API_SchindlerCustomerPortal.Class
{
    public class ReadErrorFromXMLFile
    {
        public static string ReadMessage(string msg)
        {
            string strFilename = WebConfigurationManager.AppSettings["ErrorXMLFilePath"].ToString() + "ErrorMessageFile.xml";
           
            using (XmlReader reader = XmlReader.Create(strFilename))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == msg)
                        {
                            return reader.ReadString();
                        }
                    }
                }
            }
            return "Wrong Tag Name(XML)...";
        }
    }
}