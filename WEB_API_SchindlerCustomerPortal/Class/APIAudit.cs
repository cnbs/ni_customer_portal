﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WEB_API_OmniSalesHub.DAL;

namespace WEB_API_OmniSalesHub.Class
{
    public class APIAudit
    {
        private const string APIAuditMaster = "sp_OSH_WebAPIAuditMaster";

        public static bool APIAuditRecord(string APIName, string Message, string UserID, string DeviceID, string Operation)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[5];
            int result;

            try
            {

                param[0] = new SqlParameter("@APIName", APIName);
                param[1] = new SqlParameter("@Message", Message);
                param[2] = new SqlParameter("@UserID", UserID);
                param[3] = new SqlParameter("@DeviceID", DeviceID);
                param[4] = new SqlParameter("@Operation", Operation);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, APIAuditMaster, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - APIAuditRecord");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }


    }
}