﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WEB_API_OmniSalesHub.DAL;

namespace WEB_API_OmniSalesHub.Class
{
    public class SyncDataModule
    {

        private const string GetCampaignProcedure = "sp_OSH_CampaignDetails_SelectForSyncData";
        private const string GetCampaignContactsProcedure = "sp_OSH_CampaignContacts_SelectForSyncData";
        private const string GetCampaignAssetsProcedure = "sp_OSH_CampaignContent_SelectForSyncData";
        private const string GetAssetNameByID = "sp_OSH_AssetsName_select";

        public static DataSet GetCampaignByCompanyID(int CompanyID,string UserID,string Timestamp)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@CompanyID", CompanyID);
                param[1] = new SqlParameter("@UserID", UserID);
                param[2] = new SqlParameter("@Timestamp", Timestamp);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetCampaignProcedure, param);
                ds.Tables[0].TableName = "CampaignDetailList";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetCampaignByCompanyID");
                return ds;
                throw ex;
            }
            return ds;
        }

        public static DataSet GetContactsByCampaignID(int CampaignID, string Timestamp)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@CampaignID", CampaignID);
                param[1] = new SqlParameter("@Timestamp", Timestamp);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetCampaignContactsProcedure, param);
                ds.Tables[0].TableName = "CampaignContactList";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetContactsByCampaignID");
                return ds;
                throw ex;
            }
            return ds;
        }

        public static DataSet GetAssetsByCampaignID(int CampaignID,int CompanyID, string Timestamp)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@CampaignID", CampaignID);
                param[1] = new SqlParameter("@CompanyID", CompanyID);
                param[2] = new SqlParameter("@Timestamp", Timestamp);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetCampaignAssetsProcedure, param);
                ds.Tables[0].TableName = "CampaignAssetsList";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetAssetsByCampaignID");
                return ds;
                throw ex;
            }
            return ds;
        }

        public static DataSet GetAssetNameByAssetID(string AssetID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@AssetsID", AssetID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetAssetNameByID, param);
                ds.Tables[0].TableName = "CampaignAssetsList";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetAssetNameByAssetID");
                return ds;
                throw ex;
            }
            return ds;
        }
    }
}