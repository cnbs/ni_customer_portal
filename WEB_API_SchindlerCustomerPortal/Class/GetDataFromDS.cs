﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Class
{
    public class GetDataFromDS
    {
        static string KeyList = "";
        public static CommonData GetCommonData(DataSet ds)
        {
            ArrayList AL = new ArrayList();
            CommonData CD = new CommonData();

            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                string msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;
            }
            else
            {
                string msg = ReadErrorFromXMLFile.ReadMessage("Success");
                CD.Status = "1";
                CD.Message = msg;
                CD.Data = GetDynamicDataTable(ds);
                CD.List = KeyList.ToString();
                KeyList = "";
            }
            return CD;
        }

        public static ArrayList GetDynamicDataTable(DataSet ds)
        {
            ArrayList AL = new ArrayList();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();
                for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                {
                    dynamicProperties[ds.Tables[0].Columns[j].ColumnName.ToString()] = ds.Tables[0].Rows[i][j].ToString();
                    if(i==0)
                        KeyList += ds.Tables[0].Columns[j].ColumnName.ToString() + ", ";
                }
                AL.Add(dynamicProperties);
            }
            return AL;
        }
    }
}