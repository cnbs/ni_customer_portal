﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Class
{
    public class DashboardDataModule
    {
        #region SPNameList

        private const string GetTotalSalesOrderProcedure = "USP_DASHBOARD_GET_TOTAL_SALES_ORDER";
        private const string GetTotalSalesByCountryProcedure = "USP_DASHBOARD_GET_TOTAL_SALES_BY_COUNTRY_SELECT";
        private const string GetNumberOfOrderByCountryProcedure = "USP_DASHBOARD_GET_NUMBER_OF_ORDERS_BY_COUNTRY_SELECT";
        private const string GetNumberOfOrdersPerYearByProductProcedure = "USP_DASHBOARD_GET_NUMBER_OF_ORDERS_PER_YEAR_BY_PRODUCT_SELECT";
        private const string GetOrderVsSalesProcedure = "USP_DASHBOARD_GET_ORDER_VS_SALES_SELECT";
        private const string GetSalesComparisionProcedure = "USP_DASHBOARD_GET_SALES_COMPARISION_SELECT";

        private const string GetNumberOfOrdersPerYearByProductTopMinMaxProcedure = "USP_DASHBOARD_GET_NUMBER_OF_ORDERS_PER_YEAR_BY_PRODUCT_TOP_MIN_MAX_SELECT";
        private const string GetNumberOfOrderByCountryTopMinMaxProcedure = "USP_DASHBOARD_GET_NUMBER_OF_ORDERS_BY_COUNTRY_TOP_MIN_MAX_SELECT";
        private const string GetTotalSalesByCountryTopMinMaxProcedure = "USP_DASHBOARD_GET_TOTAL_SALES_BY_COUNTRY_TOP_MIN_MAX_SELECT";

        private const string GET_RecapByMonth = "USP_DASHBOARD_GET_RecapByMonth";
        private const string GET_OrderLineTrade = "USP_DASHBOARD_GET_OrderLine";
        private const string GET_InValueCostGroup = "USP_DASHBOARD_GET_InValueCostGroup";
        private const string GET_ReceivedMDTCostGroup = "USP_DASHBOARD_GET_ReceivedMDTCostGroup";
        private const string GET_Absorption = "USP_DASHBOARD_GET_Absorption";


        private const string GET_Territory_SELECT = "USP_DASHBOARD_SALES_BREAKDOWN_ByRegion_SELECT";
        private const string GET_Country_SELECT = "USP_DASHBOARD_SALES_BREAKDOWN_ByCountry_SELECT";
        private const string GET_State_SELECT= "USP_DASHBOARD_SALES_BREAKDOWN_SELECT";

        #endregion

        public static DataSet GetRegionWiseSalesDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_Territory_SELECT, param);
                ds.Tables[0].TableName = "TerritoryData";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetRegionWiseSalesDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetCountryWiseSalesDetail(string year, string TerritoryName)
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];

            try
            {
                param[0] = new SqlParameter("@Year", year);
                param[1] = new SqlParameter("@TerritoryGroup", TerritoryName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_Country_SELECT, param);
                ds.Tables[0].TableName = "CountryData";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetCountryWiseSalesDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetStateWiseSalesDetail(string year, string TerritoryName,string CountryName)
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[3];

            try
            {
                param[0] = new SqlParameter("@Year", year);
                param[1] = new SqlParameter("@TerritoryGroup", TerritoryName);
                param[2] = new SqlParameter("@CountryName", CountryName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_State_SELECT, param);
                ds.Tables[0].TableName = "StateData";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetStateWiseSalesDetail");
                return ds;
            }
            return ds;
        }


        public static DataSet GetRecapDetail()
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_RecapByMonth);
                ds.Tables[0].TableName = "RecapByMonth";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetRecapDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetOrderLineTradeDetail()
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_OrderLineTrade);
                ds.Tables[0].TableName = "OrderLineTrade";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetOrderLineTradeDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetInValueCostGroupDetail()
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_InValueCostGroup);
                ds.Tables[0].TableName = "InValueCostGroup";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetInValueCostGroupDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetReceivedMDTCostGroupDetail()
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_ReceivedMDTCostGroup);
                ds.Tables[0].TableName = "ReceivedMDTCostGroup";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetReceivedMDTCostGroupDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetAbsorptionDetail()
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GET_Absorption);
                ds.Tables[0].TableName = "Absorption";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetAbsorptionDetail");
                return ds;
            }
            return ds;
        }


        public static DataSet GetTotalSalesOrder(SalesMaster data)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@DATE_FROM", data.StartDate);
                param[1] = new SqlParameter("@DATE_TO", data.EndDate);
                param[2] = new SqlParameter("@TYPE", data.OrderType);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetTotalSalesOrderProcedure, param);
                ds.Tables[0].TableName = "TotalSalesOrder";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetTotalSalesOrder");
                return ds;
                //throw ex;
            }
            return ds;
        }

        public static DataSet GetCountryWiseTotalSalesDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetTotalSalesByCountryProcedure, param);
                ds.Tables[0].TableName = "CountryWiseTotalSales";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetCountryWiseTotalSalesDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetCountryWiseNoOfOrderDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetNumberOfOrderByCountryProcedure, param);
                ds.Tables[0].TableName = "NumberOfOrderByCountry";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetCountryWiseNoOfOrderDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetProductWiseNoOfOrderDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetNumberOfOrdersPerYearByProductProcedure, param);
                ds.Tables[0].TableName = "NumberOfOrdersPerYearByProduct";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetProductWiseNoOfOrderDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetOrderVsSalesDataDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetOrderVsSalesProcedure, param);
                ds.Tables[0].TableName = "OrderVsSales";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetOrderVsSalesDataDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetSalseComparisionDetail(string year)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetSalesComparisionProcedure, param);
                ds.Tables[0].TableName = "SalesComparision";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetSalseComparisionDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetNumberOfOrdersPerYearByProductTopMinMaxDetail(string year, string tag, int count)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);
                param[1] = new SqlParameter("@Tag", tag);
                param[2] = new SqlParameter("@TopN", count.ToString());

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetNumberOfOrdersPerYearByProductTopMinMaxProcedure, param);
                ds.Tables[0].TableName = "NumberOfOrdersPerYearByProductTopMinMax";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetNumberOfOrdersPerYearByProductTopMinMaxDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetCountryWiseTopMaxMinDetail(string year, string tag, int count)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);
                param[1] = new SqlParameter("@Tag", tag);
                param[2] = new SqlParameter("@TopN", count.ToString());

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetNumberOfOrderByCountryTopMinMaxProcedure, param);
                ds.Tables[0].TableName = "NumberOfOrderByCountryTopMinMax";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetNumberOfOrdersPerYearByProductTopMinMaxDetail");
                return ds;
            }
            return ds;
        }

        public static DataSet GetCountryWiseTotalSalesTopMaxMinDetail(string year, string tag, int count)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Year", year);
                param[1] = new SqlParameter("@Tag", tag);
                param[2] = new SqlParameter("@TopN", count.ToString());

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetTotalSalesByCountryTopMinMaxProcedure, param);
                ds.Tables[0].TableName = "NumberOfOrderByCountryTopMinMax";

                dal.Close();
            }
            catch (Exception ex)
            {
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, ex.Message + " - GetCountryWiseTotalSalesTopMaxMinDetail");
                return ds;
            }
            return ds;
        }
    }
}