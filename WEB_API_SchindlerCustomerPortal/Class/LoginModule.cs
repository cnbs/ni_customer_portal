﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Web;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_OmniSalesHub.Models;

using System.IO;
using System.Text;
using System.Web.Configuration;

namespace WEB_API_OmniSalesHub.Class
{
    public class LoginModule
    {

        private const string LoginProcedureName = "sp_OSH_LoginUser";
        private const string UpdatePasswrodProcedureName = "sp_OSH_UpdatePassword";
        private const string DeviceLogoutProcedureName = "sp_OSH_DeviceLogout";
        private const string UpdateDeviceProcedureName = "sp_OSH_UpdateDevice";
        private const string UpdateLoginStatusProcedureName = "sp_OSH_UpdateLoginStatus";
        private const string AddDeviceProcedureName = "sp_OSH_AddDevice";
        private const string UpdateDeviceSetting = "sp_OSH_UpdateDeviceSetting";
        private const string AddCampaignContactProcedure = "sp_OSH_CampaignContacts_Insert";
        private const string UpdateCampaignContactProcedure = "sp_OSH_CampaignContacts_Update";
        private const string UpdateProfilePicProcedureName = "sp_OSH_UserMaster_UpdateProfilePic";
        private const string UserProfilePicProcedureName = "sp_OSH_UserMaster_GetLatestProfilePic";
        private const string UserDeviceTypeProcedureName = "sp_OSH_UserDevice_UpdateDeviceType";
        private const string GetUserIDFromEmail = "sp_OSH_GetUserID";
        private const string AddUserToken = "sp_OSH_UserMaster_AddToken";
        private const string InserEmailHistoryProcedure = "sp_OSH_EmailHistory_Insert";
        private const string CheckingForgotPasswordEmail = "sp_OSH_ForgotPassword_ExistEmail";
        private const string GetFirstNameFromEmail = "sp_OSH_FirstName_Select";

        public static DataSet LoginNow(string email, string password)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();


            try
            {

                param[0] = new SqlParameter("@Email", email);
                param[1] = new SqlParameter("@Password", password);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, LoginProcedureName, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();

                return ds;
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - LoginNow");
                return ds;
                throw ex;
            }

        }

        public static DataSet UpdateDevice(string DeviceID, string UserID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@DeviceID", DeviceID);
                param[1] = new SqlParameter("@UserID", UserID);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, UpdateDeviceProcedureName, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdateDevice");
                return ds;
                throw ex;
            }
            return ds;
        }

        public static bool UpdateLoginStatus(string DeviceID, string UserID, string UpdatedBy)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            int result;

            try
            {

                param[0] = new SqlParameter("@DeviceID", DeviceID);
                param[1] = new SqlParameter("@UserID", UserID);
                param[2] = new SqlParameter("@UpdatedBy", UpdatedBy);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UpdateLoginStatusProcedureName, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdateLoginStatus");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public static bool AddDevice(string UserID, string DeviceID, int CompanyID, string CreatedBy)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;

            try
            {

                param[0] = new SqlParameter("@DeviceID", DeviceID);
                param[1] = new SqlParameter("@UserID", UserID);
                param[2] = new SqlParameter("@CreatedBy", CreatedBy);
                param[3] = new SqlParameter("@CompanyID", CompanyID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, AddDeviceProcedureName, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - AddDevice");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public static bool UpdatePassword(string UserID, string password, string updatedBy, string DeviceID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;

            try
            {

                param[0] = new SqlParameter("@UserID", UserID);
                param[1] = new SqlParameter("@Password", password);
                param[2] = new SqlParameter("@UpdatedBy", updatedBy);
                param[3] = new SqlParameter("@DeviceID", DeviceID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UpdatePasswrodProcedureName, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdatePassword");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public static bool UpdateUserPic(string DeviceID, string FileName, string UserID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;

            try
            {

                param[0] = new SqlParameter("@DeviceID", DeviceID);
                param[1] = new SqlParameter("@ProfilePic", FileName);
                param[2] = new SqlParameter("@UpdatedBy", UserID);
                param[3] = new SqlParameter("@UserID", UserID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UpdateProfilePicProcedureName, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdateUserPic");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public static bool SendMail(string from, string to, string cc, string subject, string body, string attchments, string FilePath, string remindfollowupdate, int companyid, string userid)
        {
            SmtpClient smtpMailObj = new SmtpClient();
            MailMessage mail = new MailMessage();
            MailAddress maFrom = new MailAddress(from);

            try
            {
                subject = subject.Replace("\r", "");



                mail.From = maFrom;

                char toCSep1 = ',';

                if (to.IndexOf(',') > 0)
                {
                    toCSep1 = ',';
                }

                else if (to.IndexOf(';') > 0)
                {
                    toCSep1 = ';';
                }

                string[] emailTo = to.Split(toCSep1);
                string[] attchment = attchments.Split(toCSep1);

                if (cc != null && cc != "")
                {
                    string[] ccemail = cc.Split(toCSep1);
                    if (ccemail.Length > 0)
                    {
                        foreach (string str in ccemail)
                        {
                            mail.CC.Add(new MailAddress(str));
                        }
                    }
                }


                if (emailTo.Length > 0)
                {
                    foreach (string str in emailTo)
                    {
                        mail.To.Add(new MailAddress(str));
                    }
                }


                if (FilePath != "")
                {
                    if (attchment.Length > 0)
                    {

                        foreach (string str in attchment)
                        {
                            if (str != "")
                            {
                                var fileSavePath = Path.Combine(FilePath, str);
                                mail.Attachments.Add(new Attachment(fileSavePath));
                            }
                        }
                    }
                }


                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;

                //smtpMailObj.Credentials = new System.Net.NetworkCredential("athakkar@ismnet.com","Jatinthakkar");
                //smtpMailObj.UseDefaultCredentials = true;
                //smtpMailObj.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpMailObj.EnableSsl = true;

                string hostname = "";

                hostname = WebConfigurationManager.AppSettings["hostname"].ToString();
                smtpMailObj.Host = hostname;
                //smtpMailObj.Port = 25;
                smtpMailObj.Send(mail);


                DataAccessLayer dal = new DataAccessLayer();
                SqlParameter[] param = new SqlParameter[9];
                int result;

                try
                {

                    param[0] = new SqlParameter("@EmailFrom", from);
                    param[1] = new SqlParameter("@EmailTo", to);
                    param[2] = new SqlParameter("@EmailCC", cc);
                    param[3] = new SqlParameter("@Subject", subject);
                    param[4] = new SqlParameter("@EmailBody", body);
                    param[5] = new SqlParameter("@AttachmentFile", attchments);
                    param[6] = new SqlParameter("@ReminderDate", remindfollowupdate);
                    param[7] = new SqlParameter("@CompanyID", companyid);
                    param[8] = new SqlParameter("@CreatedBy", userid);


                    result = dal.ExecuteNonQuery(CommandType.StoredProcedure, InserEmailHistoryProcedure, param);

                }
                catch (Exception ex)
                {
                    LoginModule lm = new LoginModule();
                    lm.CreateLogFiles();
                    lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - SendMail");
                    return false;
                    throw ex;
                }
                finally
                {
                    dal.Close();
                }


                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message);
                return false;
                throw ex;
            }

            finally
            {
                smtpMailObj.Dispose();
                mail.Dispose();

            }

        }

        public static bool DeviceLogout(string UserID, string DeviceID, string updatedBy)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[3];
            int result;

            try
            {

                param[0] = new SqlParameter("@UserID", UserID);
                param[1] = new SqlParameter("@DeviceID", DeviceID);
                param[2] = new SqlParameter("@updatedBy", updatedBy);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, DeviceLogoutProcedureName, param);

                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - DeviceLogout");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public static bool UpdateDeviceSettings(string UserID, string DeviceID, bool PushNotification)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;

            try
            {

                param[0] = new SqlParameter("@UserID", UserID);
                param[1] = new SqlParameter("@DeviceID", DeviceID);
                param[2] = new SqlParameter("@PushNotification", PushNotification);
                param[3] = new SqlParameter("@updatedBy", UserID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UpdateDeviceSetting, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdateDeviceSettings");
                return false;
                throw ex;

            }
            finally
            {
                dal.Close();
            }

        }

        public static bool AddCampaignContact(string ContactID, string ContactProfilePic, string BusinessCardPic, string FirstName, string LastName, string Title, int CampaignID, string PhoneNumber1, string PhoneNumber2, string Email, string CompanyName, string Address1, string Address2, string City, string State, string Zip, string WebSiteAddress, string Notes, string CreatedBy, string DeviceID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[20];
            int result;

            try
            {
                param[0] = new SqlParameter("@FirstName", FirstName);
                param[1] = new SqlParameter("@LastName", LastName);
                param[2] = new SqlParameter("@Title", Title);
                param[3] = new SqlParameter("@CampaignID", CampaignID);
                param[4] = new SqlParameter("@PhoneNumber1", PhoneNumber1);
                param[5] = new SqlParameter("@PhoneNumber2", PhoneNumber2);
                param[6] = new SqlParameter("@Email", Email);
                param[7] = new SqlParameter("@CompanyName", CompanyName);
                param[8] = new SqlParameter("@Address1", Address1);
                param[9] = new SqlParameter("@Address2", Address2);
                param[10] = new SqlParameter("@City", City);
                param[11] = new SqlParameter("@State", State);
                param[12] = new SqlParameter("@Zip", Zip);
                param[13] = new SqlParameter("@WebSiteAddress", WebSiteAddress);
                param[14] = new SqlParameter("@Notes", Notes);
                param[15] = new SqlParameter("@ContactID", ContactID);
                param[16] = new SqlParameter("@CreatedBy", CreatedBy);
                param[17] = new SqlParameter("@DeviceID", DeviceID);
                param[18] = new SqlParameter("@ContactProfilePic", ContactProfilePic);
                param[19] = new SqlParameter("@BusinessCardPic", BusinessCardPic);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, AddCampaignContactProcedure, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - AddCampaignContact");
                return false;
                throw ex;

            }
            finally
            {
                dal.Close();
            }

        }

        public static bool UpdateCampaignContact(string ContactID, string ContactProfilePic, string BusinessCardPic, string FirstName, string LastName, string Title, int CampaignID, string PhoneNumber1, string PhoneNumber2, string Email, string CompanyName, string Address1, string Address2, string City, string State, string Zip, string WebSiteAddress, string Notes, string CreatedBy, string DeviceID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[20];
            int result;

            try
            {

                param[0] = new SqlParameter("@FirstName", FirstName);
                param[1] = new SqlParameter("@LastName", LastName);
                param[2] = new SqlParameter("@Title", Title);
                param[3] = new SqlParameter("@CampaignID", CampaignID);
                param[4] = new SqlParameter("@PhoneNumber1", PhoneNumber1);
                param[5] = new SqlParameter("@PhoneNumber2", PhoneNumber2);
                param[6] = new SqlParameter("@Email", Email);
                param[7] = new SqlParameter("@CompanyName", CompanyName);
                param[8] = new SqlParameter("@Address1", Address1);
                param[9] = new SqlParameter("@Address2", Address2);
                param[10] = new SqlParameter("@City", City);
                param[11] = new SqlParameter("@State", State);
                param[12] = new SqlParameter("@Zip", Zip);
                param[13] = new SqlParameter("@WebSiteAddress", WebSiteAddress);
                param[14] = new SqlParameter("@Notes", Notes);
                param[15] = new SqlParameter("@ContactID", ContactID);
                param[16] = new SqlParameter("@UpdatedBy", CreatedBy);
                param[17] = new SqlParameter("@DeviceID", DeviceID);
                param[18] = new SqlParameter("@ContactProfilePic", ContactProfilePic);
                param[19] = new SqlParameter("@BusinessCardPic", BusinessCardPic);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UpdateCampaignContactProcedure, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UpdateCampaignContact");
                return false;
                throw ex;

            }
            finally
            {
                dal.Close();
            }

        }

        public static string GetProfilePicData(string UserID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            string ProfilePic = "";

            try
            {

                param[0] = new SqlParameter("@UserID", UserID);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, UserProfilePicProcedureName, param);
                ProfilePic = ds.Tables[0].Rows[0]["ProfilePic"].ToString();

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetProfilePicData");
                return ProfilePic;
                throw ex;
            }
            return ProfilePic;
        }

        public static bool UserDeviceType(string UserID, string DeviceID, string DeviceType, string DeviceToken, string APIKey)
        {


            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[5];
            int result;

            try
            {

                param[0] = new SqlParameter("@UserID", UserID);
                param[1] = new SqlParameter("@DeviceID", DeviceID);
                param[2] = new SqlParameter("@DeviceType", DeviceType);
                param[3] = new SqlParameter("@DeviceToken", DeviceToken);
                param[4] = new SqlParameter("@APIKey", APIKey);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, UserDeviceTypeProcedureName, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - UserDeviceType");
                return false;
                throw ex;

            }
            finally
            {
                dal.Close();
            }


        }

        public static DataSet GetUserID(string Email)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@Email", Email);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetUserIDFromEmail, param);
                ds.Tables[0].TableName = "UserID";

                dal.Close();
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - GetUserID");
                
                throw ex;
            }
            return ds;

        }

        public static bool AddTokenWithUserID(string UserID, string TokenID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[2];
            int result;

            try
            {

                param[0] = new SqlParameter("@TokenID", TokenID);
                param[1] = new SqlParameter("@UserID", UserID);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, AddUserToken, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", ex.Message + " - AddTokenWithUserID");
                return false;
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        string sLogFormat;
        string sErrorTime;

        public void CreateLogFiles()
        {

            //sLogFormat used to create log files format :
            // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
            sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            //this variable used to create log filename format "
            //for example filename : ErrorLogYYYYMMDD
            string sYear = DateTime.Now.Year.ToString();
            string sMonth = DateTime.Now.Month.ToString();
            string sDay = DateTime.Now.Day.ToString();
            sErrorTime = sYear + sMonth + sDay;
        }

        public void ErrorLog(string sPathName, string sErrMsg)
        {
            StreamWriter sw = new StreamWriter(sPathName + sErrorTime, true);
            sw.WriteLine(sLogFormat + sErrMsg);
            sw.Flush();
            sw.Close();
        }

        public static DataSet FirstNameFromEmail(string Email)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();


            try
            {

                param[0] = new SqlParameter("@Email", Email);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, GetFirstNameFromEmail, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public static DataSet CheckForgotPasswordEmail(string Email)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();


            try
            {

                param[0] = new SqlParameter("@Email", Email);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, CheckingForgotPasswordEmail, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        
    }
}