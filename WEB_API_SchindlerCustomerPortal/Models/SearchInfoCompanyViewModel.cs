﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class SearchInfoCompanyViewModel
    {
        public int CompanyId { get; set; }
        public string SearchTerm { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public int TotalCount { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string ApplicationOperation { get; set; }

        public int UserID { get; set; }
    }
}