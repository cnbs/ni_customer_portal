﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class OrderLineData
    {
        public string OTD_Trade_Inter_Intra { get; set; }
        public string To_CRD { get; set; }
        public string NumberOfOrder_Line_Late_CRD { get; set; }
        public string To_SSD { get; set; }
        public string NumberOfOrder_Line_Late_SSD { get; set; }
        public string TotalLinesShipped{ get; set; }
    }
}