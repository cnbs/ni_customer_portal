﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectMasterListEntity
    {
        public int ProjectID { get; set; }
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public int Banks { get; set; }
        public int Units { get; set; }
        public string ContractorSuper { get; set; } // G.C.  Superintendent 
        public string ContractorProjMgr { get; set; } // G.C. Proj Mgr
        public string SchindlerProjMgr { get; set; }
        public string SchindlerSuperintendent { get; set; }
        public string Status { get; set; }
        public string MessageCount { get; set; }

    }
    public class ProjectMasterEntity
    {
       
        public int ProjectID { get; set; }
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
        public int Banks { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ProjectMgrPhone { get; set; }
        public string OriginalNODDate { get; set; }
        public string NOD { get; set; }
        public string NODConfirm { get; set; }
        public string IsContractSigned { get; set; }
        public string ContractorSuper { get; set; }
        public string SchindlerSuperintendent { get; set; }
        public string SchindlerSalesExecutive { get; set; }
        public string TeamAssigned { get; set; }
        public string Job_Address { get; set; }
        public string Job_City { get; set; }
        public string Job_State { get; set; }
        public string Job_Zip { get; set; }
        public double FactoryMaterialEstimate { get; set; }
        public double FactoryMaterialActual { get; set; }
        public string ContractorProjMgr { get; set; }
        public string ContractReceivedDate { get; set; }
        public string AwardDate { get; set; }
        public Int64 CustomerNo { get; set; }
        public string NODConfirmDate { get; set; }
        public int UnitCount { get; set; }
        public string BookingComplDate { get; set; }
        public string Proj_Mgr_Email { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string InceptionDate { get; set; }
        public string TurnOverDate { get; set; }
        public string AlternateProjectNo { get; set; }
        public string ContractorContactPerson { get; set; }
        public string SchindlerProjMgr { get; set; }
        public int ParentProjectId { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string DeviceID { get; set; }

     
    }

    public class ProjectJobListEntity
    {

        public string JobNo { get; set; }
        public string BankDesc { get; set; }
        public int Units { get; set; }
        public string ProdType { get; set; }
        public string ProdCode { get; set; }
        public string Capacity { get; set; }
        public string Speed { get; set; }
        public string Floor { get; set; }
        public string Rear { get; set; }
        public string StatusName { get; set; }
        public string MessageCount { get; set; }
    }
    public class ProjectJobEntity
    {
        public int JobId { get; set; }
        public string JobNo { get; set; }
        public string JobName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectNo { get; set; }
        public string CompanyName { get; set; }
        public string BankDesc { get; set; }
        public string Prod { get; set; }
        public string District { get; set; }
        public int Office { get; set; }
        public string ProdCode { get; set; }
        public string OriginalNODDate { get; set; }
        public string NOD { get; set; }
        public string NODConfirm { get; set; }
        public string ActionText { get; set; }
        public string IsContractSigned { get; set; }
        public string ForecastCloseDate { get; set; }
        public decimal Price { get; set; }
        public decimal PaidPercent { get; set; }
        public decimal BilledPercent { get; set; }
        public int BaseHrs { get; set; }
        public int EstHrs { get; set; }
        public int ActHrs { get; set; }
        public decimal CompletionPercent { get; set; }
        public UInt64 ProjectMgrPhone { get; set; }
        public string ContractorSuper { get; set; }
        public string TeamAssigned { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string Elev_Type { get; set; }
        public int Speed { get; set; }
        public int Capacity { get; set; }
        public int Num_Floors { get; set; }
        public int Volt { get; set; }
        public string DoorType { get; set; }
        public int Rear { get; set; }
        public string Travel { get; set; }
        public string CwtLoc { get; set; }
        public decimal FactoryMaterialEstimate { get; set; }
        public decimal FactoryMaterialActual { get; set; }
        public string Serial_Num { get; set; }
        public string ContractorProjMgr { get; set; }
        public string ContractRcvdDate { get; set; }
        public string AwardDate { get; set; }
        public string ApprovalstoGC { get; set; }
        public string ApprovalstoGBer { get; set; }
        public string NOSPlanned { get; set; }
        public string NOSActual { get; set; }
        public string NODConfirmDate { get; set; }
        public string Proj_Mgr_Email { get; set; }
        public string TurnOverDate { get; set; }
        public int UnitCount { get; set; }
        public string SuperComments { get; set; }
        public string SalesComments { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class ProjectDocumentEntity
    {
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public int DocumentTypeID { get; set; }
        public string DocumentType { get; set; }
        public string Description { get; set; }
        public string DocumentPath { get; set; }
        public string ExpiredDate { get; set; }
        public string PublishedBy { get; set; }
        public Nullable<DateTime> PublishedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string DeviceID { get; set; }
        public string StatusName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ProjectID { get; set; }
        public int StatusId { get; set; }
        public string ProjectName { get; set; }
        public int Status { get; set; }
        public string FileType { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int JobId { get; set; }
        public string JobName { get; set; }
        public bool IsPublished { get; set; }
        public string PublishedStatus { get; set; }

        public string ExpiredDateString { get; set; }

        public string ApplicationOperation { get; set; }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FileData { get; set; }
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }

    }
}