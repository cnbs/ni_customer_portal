﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class NotificationEntity
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string DeviceID { get; set; }
        public string SenderID { get; set; }
        public string SenderName { get; set; }
        public string ReceiverID { get; set; }
        public string ReceiverName { get; set; }
        public string DeviceLastLoginDateTime { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime DateSent { get; set; }
        public string SentDate { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
        public string CreatedBy { get; set; }
        public string DeviceType { get; set; }

    }
}