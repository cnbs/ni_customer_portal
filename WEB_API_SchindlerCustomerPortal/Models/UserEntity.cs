﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class UserEntity
    {
        public UserEntity()
        {
            userpermissionviewModel = new List<UserPermissionViewModel>();
        }
        public int UserID { get; set; }
        public int RoleID { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }

        public string ContactNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int StateID { get; set; }
        public string Country { get; set; }

        public string ProfilePic { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string NewPassword { get; set; }
        //public string PasswordSalt { get; set; }
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }
        public int Theme { get; set; }
        public string LoginAs { get; set; }
        public bool IsActive { get; set; }
        public string AdminEmail { get; set; }
        public string AdminPassword { get; set; }
        public int IsAdmin { get; set; }
        public string Remember { get; set; }
        public string Token { get; set; }
        public DateTime TokenExpireDate { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public int IsDeleted { get; set; }
        public string Done { get; set; }
        public int AccountStatus { get; set; }
        public string StatusName { get; set; }
        public int OfflineAccessDuration { get; set; }
        public int TokenExpire { get; set; }
        public int Status { get; set; }
        public string IdList { get; set; }

        public int DesignationId { get; set; }
        public string Designation { get; set; }

        public string ModuleName { get; set; }
        public bool HasViewPermission { get; set; }

        public string ReturnURL { get; set; }

        public string RegisteredDate { get; set; }

        public List<PermissionList> permissionList { get; set; }

        public string ApplicationOperation { get; set; }

        public string Requested_Projects { get; set; }
        public bool Unsubscribe { get; set; }

        public List<UserPermissionViewModel> userpermissionviewModel { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string StateText { get; set; }
    }

    public class PermissionList
    {
        public int UserID { get; set; }
        public int ModuleId { get; set; }
        public bool HasPermission { get; set; }
    }

    public class ChangePassword
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string PasswordSalt { get; set; }
        public string ConfirmPassword { get; set; }
        public int UserID { get; set; }
        public string Email { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string DeviceID { get; set; }
        public string UpdatedBy { get; set; }

        public string ApplicationOperation { get; set; }
    }
}