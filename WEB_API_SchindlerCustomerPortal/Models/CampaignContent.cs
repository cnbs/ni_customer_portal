﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class CampaignContent
    {
        public int ContentID { get; set; }
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }



    }
}