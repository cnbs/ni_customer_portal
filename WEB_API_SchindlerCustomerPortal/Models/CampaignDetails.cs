﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class CampaignDetails
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentID { get; set; }
        public int CompanyID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        

        public List<CampaignDetails> campaigndetials = new List<CampaignDetails>();
        public CampaignDetails(int id, int parent_id)
        {
        ID = id;
        ParentID = parent_id;
        }


    }
}