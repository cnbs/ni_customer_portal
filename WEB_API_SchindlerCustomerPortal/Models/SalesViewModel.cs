﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class SalesViewModel
    {
        public List<SalesMaster> lstSalesOrder { get; set; }
        public List<SalesMaster> lstPurchaseOrder { get; set; }

    }
}