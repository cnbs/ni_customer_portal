﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class AbsorptionData
    {
        public string Status { get; set; }
        public string Absorption { get; set; }
        public string MonthToDate { get; set; }
        public string OPS_FY { get; set; }
        public string AbsorptionPercOfOPS { get; set; }
        public string TargetPercOfMTD { get; set; }
    }
}