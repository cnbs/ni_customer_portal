﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class CalendarEventEntity
    {
        public CalendarEventEntity()
        {
            CategoryData = new CalendarCategory();
        }
        public CalendarCategory CategoryData { get; set; }
        public int Id { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string Subject { get; set; }
        public string EmailHeaderSubject { get; set; }
        public string SearchTearm { get; set; }
        public string Location { get; set; }
        public string SendTo { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MessageBody { get; set; }
        public int CategoryID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string AppAuditID { get; set; }
        public string SessionID { get; set; }
        public string ApplicationOperation { get; set; }
        public string AttachmentPath { get; set; }
        public string EmailTemplateName { get; set; }
        public string MailSenderID { get; set; }
    }

}