﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectJobDueStatusModel
    {
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }
        public string Milestone { get; set; }
    }
}