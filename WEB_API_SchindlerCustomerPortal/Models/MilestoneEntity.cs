﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class MilestoneEntity
    {
        public int MilestoneId { get; set; }
        public string MilestoneTitle { get; set; }

    }
}