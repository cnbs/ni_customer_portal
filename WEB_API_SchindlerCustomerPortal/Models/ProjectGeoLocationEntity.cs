﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectGeoLocationEntity
    {
        public int ProjectID { get; set; }
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
        public string StatusName { get; set; }
        public string Job_Address { get; set; }
        public string Job_City { get; set; }
        public string Job_State { get; set; }
        public string Job_Zip { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double User_Latitude { get; set; }
        public double User_Longitude { get; set; }

    }

}