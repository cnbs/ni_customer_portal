﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class SalesBreakdown
    {
        public string[] Year { get; set; }
        public string[] Territory { get; set; }
        public string Country { get; set; }
        //public string State { get; set; }
    }
}