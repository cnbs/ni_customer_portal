﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class SalesDetails
    {
        public string TotalSales { get; set; }
        public string TotalOrder { get; set; }
        public string CurrentYear { get; set; }
        public string CurrentYearFormated { get; set; }
        public string LastYear { get; set; }
        public string Percent { get; set; }
        public string UpDown { get; set; }

    }
}