﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class CampaignContact
    {

        public string ContactID { get; set; }
        public string ContactProfilePic { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string WebsiteAddress { get; set; }
        public int CampaignID { get; set; }
        public string Notes { get; set; }
        public string DeviceID { get; set; }
        public string CreatedBy { get; set; }
        public string BusinessCardPic { get; set; }


    }
}