﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class StateEntity
    {
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public int StateID { get; set; }
        public string CountryName { get; set; }

    }
}