﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class UserDevice
    {
        public string UserID { get; set;}
        public string DeviceID { get; set;}
        public bool LoginStatus { get; set;}
        public string PushNotification { get; set;}
        public DateTime LastSync { get; set;}
        public int CompanyID { get; set;}
        public string CreatedBy { get; set;}
        public string UpdatedBy { get; set; }
        public string DeviceType { get; set; }
        public string DeviceToken { get; set; }
        public string APIKey { get; set; }

    }
}