﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ReceivedMDTCostData
    {
        public string Receipts { get; set; }
        public string ReceivedMTDPOValue { get; set; }
        public string ReceivedMTDStandard { get; set; }
        public string PPV { get; set; }
        public string MTDVoucheredIPV { get; set; }
        public string MTDCalculated { get; set; }
        public string Difference { get; set; }
    }
}