﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class ZipfileContent
    {
        public string ZipfileName { get; set; }
        public string TimeStamp { get; set; }
        public int CompanyID { get; set; }
    }
}