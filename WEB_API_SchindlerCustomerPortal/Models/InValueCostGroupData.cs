﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class InValueCostGroupData
    {
        public string Status { get; set; }
        public string Inventory { get; set; }
        public string RAW { get; set; }
        public string WP{ get; set; }
        public string FG { get; set; }
        public string TotalInventoryEOD { get; set; }
        public string OPS_FY { get; set; }
        public string Difference { get; set; }
        public string PercOfOPS { get; set; }
    }
}