﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectAssignEntity
    {
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }
        public string ProjectName { get; set; }
        public string JobName { get; set; }
        public string Company { get; set; }
        public string CheckAssigned { get; set; }
    }
}