﻿using System.Collections.Generic;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectMilestoneStatusEntity
    {
        public ProjectMilestoneStatusEntity()
        {
            BankList = new List<ProjectMilestoneStatusEntity>();
        }

        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectNo { get; set; }
        public string ContractExecuted { get; set; }
        public string DownPayment { get; set; }
        public string DrawingsApproved { get; set; }
        public string FinishesComplete { get; set; }
        //public string MaterialOrdered { get; set; }
        //public string MaterialAtHub { get; set; }
        public string MaterialInStorageHub { get; set; }
        public string ReadyToPull { get; set; }
        public string JobNo { get; set; }
        public string InstallationStart { get; set; }
        //public string FinalInspection { get; set; }
        public string EquipmentTurnover { get; set; }
        public string Title { get; set; }
        public string DataType { get; set; }
        public string DueDate { get; set; }
        public int TotalProjectCount { get; set; }
        public string Billed { get; set; }
        public string Paid { get; set; }
        public int ProjectCompanyId { get; set; }
//added by arpit
        public string ContractExecutedDate { get; set; }
        public string DownPaymentDate { get; set; }
        public string DrawingsApprovedDate { get; set; }
        public string FinishesCompleteDate { get; set; }
        //public string MaterialOrdered { get; set; }
        //public string MaterialAtHub { get; set; }
        public string MaterialInStorageHubDate { get; set; }
        public string ReadyToPullDate { get; set; }
        public string JobNoDate { get; set; }
        public string InstallationStartDate { get; set; }
        //public string FinalInspection { get; set; }
        public string EquipmentTurnoverDate { get; set; }


        public List<ProjectMilestoneStatusEntity> BankList { get; set; }
    }
}