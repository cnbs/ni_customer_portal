﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class SalesRecapData
    {
        public string Status { get; set; }
        public string SalesTradeAndInterCo { get; set; }
        public string MTDShipments { get; set; }
        public string OPS_FY { get; set; }
        public string CurrentMonthBackLog_CRD { get; set; }
        public string MTD_Booking { get; set; }
        public string SalesPercOfOPS { get; set; }
        public string TargetPercOfMTD{ get; set; }

    }
}