﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class GeoLocationInfoModel
    {
        public int ProjectID { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}