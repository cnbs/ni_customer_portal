﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class ContentRepo
    {

        public int ContentID { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string Keyword { get; set; }
        public string Path { get; set; }
        public int CompanyID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string ContentIDList { get; set; }
        public double AvailableSize { get; set; }

    }
}