﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class PlayStoreDetails
    {
        public int PlayStoreID { get; set; }
        public int UserID { get; set; }
        public string IUserName { get; set; }
        public string IPassword { get; set; }
        public string GUserName { get; set; }
        public string GPassword { get; set; }
        public string Splash_I1 { get; set; }
        public string Splash_G1 { get; set; }
        public string AppIcon { get; set; }
        public int Theme { get; set; }
        public int RequestStatus{ get; set; }
        public int CompanyID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

    }
}