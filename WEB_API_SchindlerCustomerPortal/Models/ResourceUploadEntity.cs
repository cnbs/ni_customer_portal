﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ResourceUploadEntity
    {

        
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentType { get; set; }
        public string ProductName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UploadDate { get; set; }
        public string Status { get; set; }
        public string PublishedStatus { get; set; }
        public bool IsPublished { get; set; }
        public string DocumentPath { get; set; }
        public string DocExtension { get; set; }
        public string FileName { get; set; }
        public string URLName { get; set; }
        public string ResourceType { get; set; }
        public string PublishedBy { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string DeviceID { get; set; }
        public string ThumbnailImage{get;set;}
        public string TitleFileExtension { get; set; }
        public string ApplicationOperation { get; set; }
    }
}