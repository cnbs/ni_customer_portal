﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ModuleEntity
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }

    }
}