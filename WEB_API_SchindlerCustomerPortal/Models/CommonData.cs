﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class CommonData
    {
        public CommonData()
        {
            StatusList = new List<StatusData>();
            List = new object();
        }
        public string ProjectCompanyID { get; set; }
        public string Status { get; set; }
        public List<StatusData> StatusList { get; set; }
        public object List { get; set; }
        public string Message { get; set; }
        public int TotalProjects { get; set; }
        public int Count { get; set; }
        public int UserCount { get; set; }
        public int MailCount { get; set; }
        public object Data { get; set; }

    }
    public class StatusData
    {
        public int Status { get; set; }
        public string StatusName { get; set; }
    }
}