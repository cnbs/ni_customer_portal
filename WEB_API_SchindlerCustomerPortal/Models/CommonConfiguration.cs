﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class CommonConfiguration
    {
        public static string numberOfRecordsToDisplay = ConfigurationManager.AppSettings["numberOfRecords"].ToString();
        public static string userProfilePicPath = ConfigurationManager.AppSettings["userProfilePicPath"].ToString();
        public static string domainPath = ConfigurationManager.AppSettings["DomainPath"].ToString();
        public static string companyLogoPath = ConfigurationManager.AppSettings["CompanyLogo"].ToString();
        public static string docuploadPath = ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
        public static string TempDocPath = ConfigurationManager.AppSettings["TempDocumentUploadPath"].ToString();
        public static string emailAttachmentPath = ConfigurationManager.AppSettings["EmailAttachmentPath"].ToString();

        public static string readyToPullAttachmentsPath = ConfigurationManager.AppSettings["readyToPullAttachmentsPath"].ToString();
        public static string EmailTemplatePath = ConfigurationManager.AppSettings["EmailTemplatePath"].ToString();

        public static string MilestoneStatusUpdateEmailToPerson = ConfigurationManager.AppSettings["MilestoneStatusUpdateEmailToPerson"].ToString();
        public static string MilestoneStatusUpdateEmail = ConfigurationManager.AppSettings["MilestoneStatusUpdateEmail"].ToString();

        public static string NoReplyEmail = ConfigurationManager.AppSettings["NoReplyEmail"].ToString();



    }
}