﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class ProjectBankDocDraw
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string DeviceID { get; set; }

        public string SearchTerm { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int CompanyID { get; set; }


        public int ProjectNo { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string ApplicationOperation { get; set; }
        public int Count { get; set; }
    }
}
