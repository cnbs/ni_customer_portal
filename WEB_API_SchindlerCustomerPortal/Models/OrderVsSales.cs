﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class OrderVsSales
    {
        public string OrderMonth { get; set; }
        public string OrderMonthYear { get; set; }
        public string OrderYear { get; set; }
        public string SumOfTotalOrder { get; set; }
        public string SumOfSales { get; set; }
        public string AverageSales { get; set; }
    }
}