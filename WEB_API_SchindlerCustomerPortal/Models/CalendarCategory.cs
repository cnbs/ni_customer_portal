﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class CalendarCategory
    {
        public int CatID { get; set; }
        public string CategoryName { get; set; }
        public string ColorCode { get; set; }
    }
}