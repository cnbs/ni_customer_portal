﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class UserGroup
    {

        public int GroupID { get; set;}
        public int UserID { get; set;}
        public int CompanyID { get; set;}
        public string CreatedBy { get; set;}
        public string UpdatedBy { get; set; }


    }
}