﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class SyncData
    {
        // CampaignDetails
        public int CampignID { get; set; }
        public string CampignName { get; set; }
        public string CampignDescription { get; set; }
        public int CampignParentID { get; set; }
        public int CampignCompanyID { get; set; }
        public string CampignCreatedBy { get; set; }
        public string CampignUpdatedBy { get; set; }

        // CampaignContacts
        public string ContactID { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactPhoneNumber1 { get; set; }
        public string ContactPhoneNumber2 { get; set; }
        public string ContactEmail { get; set; }
        public string ContactCompanyName { get; set; }
        public string ContactAddress1 { get; set; }
        public string ContactAddress2 { get; set; }
        public string ContactCity { get; set; }
        public string ContactState { get; set; }
        public string ContactZip { get; set; }
        public string ContactWebsiteAddress { get; set; }
        public int ContactCampaignID { get; set; }
        public string ContactNotes { get; set; }
        public string ContactDeviceID { get; set; }
        public string ContactCreatedBy { get; set; }

        // ContentRepos
        public int ContentID { get; set; }
        public string ContentFileName { get; set; }
        public string ContentDescription { get; set; }
        public string ContentKeyword { get; set; }
        public string ContentPath { get; set; }
        public int ContentCompanyID { get; set; }
        public string ContentCreatedBy { get; set; }
        public string ContentUpdatedBy { get; set; }


        //General
        public string DeviceID { get; set; }
        public string UserID { get; set; }
        public string Timestamp { get; set; }

    }
}