﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class HelpEntity
    {
        public HelpEntity()
        {
            SubMenuList = new List<HelpEntity>();
        }
        public int HelpId { get; set; }
        public int ModuleId { get; set; }
        public int ParentId { get; set; }
        public int SequenceNo { get; set; }
        public int ParentSequenceNo { get; set; }
        
        public string ModuleName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool PublishedStatus { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }

        public string ApplicationOperation { get; set; }


        public List<HelpEntity> SubMenuList { get; set; }
    }
}