﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class DocumentPathData
    {
        public int ProjectNo { get; set; }
        public string JobNo { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public string Extension { get; set; }
    }
    public class DocumentData
    {
        public List<DocumentPathData> Invoice = new List<DocumentPathData>();
        public List<DocumentPathData> Contract = new List<DocumentPathData>();
    }
}