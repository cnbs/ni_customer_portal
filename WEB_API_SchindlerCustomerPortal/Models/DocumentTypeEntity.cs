﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class DocumentTypeEntity
    {
        public int DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
    }
}