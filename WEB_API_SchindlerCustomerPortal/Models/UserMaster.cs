﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WEB_API_OmniSalesHub.Models
{
    public class UserMaster
    {
        public string UserID { get; set; }
        public int RoleID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CompanyID { get; set; }
        public string ContactNumber { get; set; }
        public string ProfilePic { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string NewPassword { get; set; }
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }
        public int Theme { get; set; }

        
    }
}