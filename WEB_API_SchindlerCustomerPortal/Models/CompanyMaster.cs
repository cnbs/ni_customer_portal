﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_OmniSalesHub.Models
{
    public class CompanyMaster
    {
       public int CompanyID { get; set; }
       public string Name { get; set; }
            
    }
}