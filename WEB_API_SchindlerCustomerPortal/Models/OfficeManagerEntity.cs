﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class OfficeManagerEntity
    {
        public string Email { get; set; }
        public int UserID { get; set; }

        public string FirstName { get; set; }

        public string OfficeID { get; set; }

        public string SapID { get; set; }

        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public string Office_Desc { get; set; }
        public string Title { get; set; }

        public int OfficeManagerID { get; set; }

        public int CompanyID { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string DeviceID { get; set; }
        
        public string ApplicationOperation { get; set; }

        public bool? IsDeleted { get; set; }

        public string FullName { get; set; }

    }
}