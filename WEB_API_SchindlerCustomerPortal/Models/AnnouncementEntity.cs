﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class AnnouncementEntity
    {
            public int AnnMsgId { get; set; }
            [AllowHtml]
            public string MessageText { get; set; }
            public string Frequency { get; set; }
            public string DisplayOn { get; set; }
            public string ValidDate { get; set; }
           
    }
}