﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.Models
{
    public class SalesMaster
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OrderType { get; set; }

    }

    public class YearData
    {
        public string[] Year { get; set; }
    }

    public class TopNData
    {
        public string[] Year { get; set; }
        public string MinMax { get; set; }
        public int Count{ get; set; }  
    }
}