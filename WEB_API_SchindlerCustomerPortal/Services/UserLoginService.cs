﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Services
{
    public class UserLoginService
    {
        UserDAL userdal = new UserDAL();
        UserEntity userds;
        CommonData CD = new CommonData();

        public UserEntity Find(string Email)
        {
            try
            {
                UserEntity userds = (from row in userdal.LoginNow(Email)
                                     select new UserEntity
                                     {
                                         Email = Convert.ToString(row["Email"]),
                                         RoleID = Convert.ToInt32(row["RoleID"]),
                                         UserID = Convert.ToInt32(row["UserID"]),
                                         FirstName = Convert.ToString(row["FirstName"]),
                                         LastName = Convert.ToString(row["LastName"]),
                                         CompanyID = string.IsNullOrEmpty(Convert.ToString(row["CompanyID"])) ? 0 : Convert.ToInt32(row["CompanyID"]),
                                         CompanyName = Convert.ToString(row["CompanyName"]),
                                         ContactNumber = Convert.ToString(row["ContactNumber"]),
                                         ProfilePic = string.IsNullOrEmpty(Convert.ToString(row["ProfilePic"].ToString())) ? "" : Convert.ToString(row["ProfilePic"]),
                                         //CreatedBy = row["CreatedBy"].ToString(),
                                         //UpdatedBy = row["UpdatedBy"].ToString(),
                                         PostalCode = Convert.ToString(row["PostalCode"]),
                                         Password = Convert.ToString(row["Password"]),
                                         PasswordSalt = Convert.ToString(row["Password_Salt"]),
                                         AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                         DesignationId = Convert.ToInt32(row["Designation"]),
                                         OfflineAccessDuration = Convert.ToInt32(row["OfflineAccessDuration"]),
                                         Address1 = Convert.ToString(row["Address1"]),
                                         State = Convert.ToString(row["StateName"]),
                                         StateID = string.IsNullOrEmpty(Convert.ToString(row["StateId"])) ? 0 : Convert.ToInt32(row["StateId"].ToString()),
                                         Designation = Convert.ToString(row["DesignationName"]),
                                         City = Convert.ToString(row["City"]),
                                         Unsubscribe = String.IsNullOrEmpty(Convert.ToString(row["IsUnSubscribed"])) ? false : Convert.ToBoolean(row["IsUnSubscribed"]),
                                         Latitude = Convert.ToDouble(row["Latitude"]),
                                         Longitude = Convert.ToDouble(row["Longitude"])
                                     }).FirstOrDefault();

                return userds;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserPermissionViewModel> GetUserModulePermissionForMenu(int userid, int parentId)
        {
            List<UserPermissionViewModel> UserPermissionlst = new List<UserPermissionViewModel>();
            UserPermissionlst = (from row in userdal.GetModulesPermissionForDynamicMenu(userid, parentId)
                                 select new UserPermissionViewModel
                                 {

                                     UserID = Convert.ToInt32(row["UserID"]),
                                     ModuleId = Convert.ToInt32(row["ModuleId"]),
                                     ModuleURL = (row["URL"]).ToString(),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                                     HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                                     HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                                     ModulePermissionName = (row["ModulePermissionName"]).ToString(),
                                     ModuleDisplayName = (row["DisplayName"]).ToString(),
                                     SequenceNO = Convert.ToInt32(row["SequenceNO"]),
                                     Restricted = Convert.ToInt32(row["Restricted"]),
                                     VisibleOnMenu = Convert.ToInt32(row["VisibleOnMenu"]),

                                 }).ToList();

            return UserPermissionlst;

        }

        public List<UserPermissionViewModel> GetAllMenuByParentCategoryId(int userID,int CompanyID, int parentCategoryId)
        {

            int UserID = userID;
            var result = new List<UserPermissionViewModel>();

            var data = GetUserModulePermissionForMenu(UserID, parentCategoryId);

            foreach (var item in data)
            {
                var menuModel = new UserPermissionViewModel
                {
                    UserID = item.UserID,
                    ModuleId = item.ModuleId,
                    //ModuleName = item.ModuleName,
                    ModuleDisplayName = StripHTML(item.ModuleDisplayName),
                    ModuleURL = item.ModuleURL,
                    ModulePermissionName = item.ModulePermissionName,
                    HasViewPermission = item.HasViewPermission,
                    HasAddPermission = item.HasAddPermission,
                    HasEditPermission = item.HasEditPermission,
                    HasDeletePermission = item.HasDeletePermission,
                    HasPrintPermission = item.HasPrintPermission,
                    SequenceNO = item.SequenceNO,
                    Restricted = item.Restricted,
                    VisibleOnMenu = item.VisibleOnMenu
                };

                if (StripHTML(menuModel.ModuleDisplayName) == "User Management")
                {
                    menuModel.ModuleURL = menuModel.ModuleURL + CompanyID;
                }

                var subMenus = GetAllMenuByParentCategoryId(userID, CompanyID,item.ModuleId);
                menuModel.SubMenus.AddRange(subMenus);

                result.Add(menuModel);
            }
            //modulePermission.AddRange(model);

            return result;
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

    }
}