﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class UpdateProfilePicController : ApiController
    {
        [HttpPost]
        public object PostFile()
        {
            try
            {
                string file = "";
                string userid = "";

                string APIPathProfilePic = WebConfigurationManager.AppSettings["APIPathProfilePic"].ToString();
                string WebPathProfilePic = WebConfigurationManager.AppSettings["WebPathProfilePic"].ToString();

                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["AttachmentFile"];

                    if (httpPostedFile != null)
                    {
                        // Get the complete file path
                        file = httpPostedFile.FileName;
                        string[] ret = file.Split('.');
                        userid = ret[0].ToString();


                        var fileSavePath1 = Path.Combine(APIPathProfilePic, httpPostedFile.FileName);
                        var fileSavePath2 = Path.Combine(WebPathProfilePic, httpPostedFile.FileName);
                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath1);
                        httpPostedFile.SaveAs(fileSavePath2);

                    }
                }

                

                var device = System.Web.HttpContext.Current.Request["DeviceID"];

                var result = LoginModule.UpdateUserPic(device, file, userid);

                DataSet tempds = new DataSet();

                if (result == true)
                {
                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "UpdateSetting";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "1";
                    dr["Message"] = "Your profile picture has been updated!";

                    t.Rows.Add(dr);

                    //bool audit = APIAudit.APIAuditRecord("api/ChangeSetting", "ChangeSetting Success", data.UserID, data.DeviceID, "Update");

                    return tempds;
                }
                else
                {


                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "UpdateSetting";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "0";
                    dr["Message"] = "No User/Device Matched!";

                    t.Rows.Add(dr);

                    //bool audit = APIAudit.APIAuditRecord("api/ChangeSetting", "ChangeSetting - No User/Device Matched", data.UserID, data.DeviceID, "Update");

                    return tempds;

                }

            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message);
                return false;
                //throw ex;
                
            }
        }

    }
}
