﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class BankDetailListandViewController : ApiController
    {
        [HttpPost]
        public CommonData BankListandView(DataTableModel model)
        {
            ManageProjectDAL MPD = new ManageProjectDAL();
            CommonData CD = new CommonData();
            try
            {
                int totalcount = 0;
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "JobName";
                }

                if (string.IsNullOrEmpty(model.SortDirection))
                {
                    model.SortColumn = "asc";
                }

                #region BankView&List
                model.ApplicationOperation = "ProjectManagement_MasterJobList";
                try
                {
                    bool ViewForCustomers = true;

                    if (model.RoleID == 1 || model.RoleID == 3)
                    { ViewForCustomers = false; }
                    else { ViewForCustomers = true; }

                    List<ProjectJobEntity> projectJobRecord = new List<ProjectJobEntity>();
                    projectJobRecord = (from row in MPD.GetProjectJobRecordByJobNo(model, ref totalcount)
                                        select new ProjectJobEntity
                                        {
                                            JobId = DBNull.Value.Equals(row["JobId"]) ? 0 : Convert.ToInt32(row["JobId"]),
                                            JobNo = Convert.ToString(row["JobNo"]),
                                            JobName = Convert.ToString(row["JobName"]),
                                            ProjectId = DBNull.Value.Equals(row["ProjectId"]) ? 0 : Convert.ToInt32(row["ProjectId"]),
                                            BankDesc = Convert.ToString(row["BankDesc"]),
                                            Prod = Convert.ToString(row["ProdType"]),
                                            District = Convert.ToString(row["District"]),
                                            Office = DBNull.Value.Equals(row["Office"]) ? 0 : Convert.ToInt32(row["Office"]),
                                            ProdCode = Convert.ToString(row["ProdCode"]),
                                            OriginalNODDate = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["OriginalNODDate"])) ? "" : Convert.ToString(row["OriginalNODDate"]).Replace(" 12:00:00 AM", "")),
                                            NOD = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOD"])) ? "" : Convert.ToString(row["NOD"]).Replace(" 12:00:00 AM", "")),
                                            NODConfirm = (ViewForCustomers == true ? "" : Convert.ToString(row["NODConfirm"])),
                                            ActionText = (ViewForCustomers == true ? "" : Convert.ToString(row["ActionText"])),
                                            IsContractSigned = Convert.ToString(row["IsContractSigned"]),
                                            ForecastCloseDate = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ForecastCloseDate"])) ? "" : Convert.ToString(row["ForecastCloseDate"]).Replace(" 12:00:00 AM", "")),
                                            Price = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Price"]) ? 0 : Convert.ToDecimal(row["Price"])),
                                            PaidPercent = DBNull.Value.Equals(row["PaidPercent"]) ? 0 : Convert.ToDecimal(row["PaidPercent"]),
                                            BilledPercent = DBNull.Value.Equals(row["BilledPercent"]) ? 0 : Convert.ToDecimal(row["BilledPercent"]),
                                            BaseHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["BaseHrs"]) ? 0 : Convert.ToInt32(row["BaseHrs"])),
                                            EstHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["EstHrs"]) ? 0 : Convert.ToInt32(row["EstHrs"])),
                                            ActHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["ActHrs"]) ? 0 : Convert.ToInt32(row["ActHrs"])),
                                            CompletionPercent = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["CompletionPercent"]) ? 0 : Convert.ToDecimal(row["CompletionPercent"])),
                                            ProjectMgrPhone = DBNull.Value.Equals(row["ProjectMgrPhone"]) ? 0 : Convert.ToUInt64(row["ProjectMgrPhone"]),
                                            ContractorSuper = Convert.ToString(row["ContractorSuper"]),
                                            TeamAssigned = (ViewForCustomers == true ? "" : Convert.ToString(row["TeamAssigned"])),
                                            Status = DBNull.Value.Equals(row["Status"]) ? 0 : Convert.ToInt32(row["Status"]),
                                            StatusName = Convert.ToString(row["StatusName"]),
                                            Elev_Type = Convert.ToString(row["Elev_Type"]),
                                            Speed = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Speed"]) ? 0 : Convert.ToInt32(row["Speed"])),
                                            Capacity = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Capacity"]) ? 0 : Convert.ToInt32(row["Capacity"])),
                                            Num_Floors = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Floor"]) ? 0 : Convert.ToInt32(row["Floor"])),
                                            Volt = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Volt"]) ? 0 : Convert.ToInt32(row["Volt"])),
                                            DoorType = (ViewForCustomers == true ? "" : Convert.ToString(row["DoorType"])),
                                            Rear = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Rear"]) ? 0 : Convert.ToInt32(row["Rear"])),
                                            Travel = (ViewForCustomers == true ? "" : Convert.ToString(row["Travel"])),
                                            CwtLoc = (ViewForCustomers == true ? "" : Convert.ToString(row["CwtLoc"])),
                                            FactoryMaterialEstimate = Convert.IsDBNull(row["FactoryMaterialEstimate"]) ? 0 : Convert.ToDecimal(row["FactoryMaterialEstimate"]),
                                            FactoryMaterialActual = Convert.IsDBNull(row["FactoryMaterialActual"]) ? 0 : Convert.ToDecimal(row["FactoryMaterialActual"]),
                                            Serial_Num = Convert.ToString(row["Serial_Num"]),
                                            ContractorProjMgr = Convert.ToString(row["ContractorProjMgr"]),
                                            ContractRcvdDate = string.IsNullOrEmpty(Convert.ToString(row["ContractRcvdDate"])) ? "" : Convert.ToString(row["ContractRcvdDate"]).Replace(" 12:00:00 AM", ""),
                                            AwardDate = string.IsNullOrEmpty(Convert.ToString(row["AwardDate"])) ? "" : Convert.ToString(row["AwardDate"]).Replace(" 12:00:00 AM", ""),
                                            ApprovalstoGC = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ApprovalstoGC"])) ? "" : Convert.ToString(row["ApprovalstoGC"]).Replace(" 12:00:00 AM", "")),
                                            ApprovalstoGBer = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ApprovalstoGBer"])) ? "" : Convert.ToString(row["ApprovalstoGBer"]).Replace(" 12:00:00 AM", "")),
                                            NOSPlanned = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOSPlanned"])) ? "" : Convert.ToString(row["NOSPlanned"]).Replace(" 12:00:00 AM", "")),
                                            NOSActual = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOSActual"])) ? "" : Convert.ToString(row["NOSActual"]).Replace(" 12:00:00 AM", "")),
                                            NODConfirmDate = string.IsNullOrEmpty(Convert.ToString(row["NODConfirmDate"])) ? "" : Convert.ToString(row["NODConfirmDate"]).Replace(" 12:00:00 AM", ""),
                                            Proj_Mgr_Email = Convert.ToString(row["Proj_Mgr_Email"]),
                                            TurnOverDate = string.IsNullOrEmpty(Convert.ToString(row["TurnOverDate"])) ? "" : Convert.ToString(row["TurnOverDate"]).Replace(" 12:00:00 AM", ""),
                                            UnitCount = DBNull.Value.Equals(row["UnitCount"]) ? 0 : Convert.ToInt32(row["UnitCount"]),
                                            SuperComments = (ViewForCustomers == true ? "" : Convert.ToString(row["SuperComments"])),
                                            SalesComments = (ViewForCustomers == true ? "" : Convert.ToString(row["SalesComments"])),
                                            ProjectName = Convert.ToString(row["ProjectName"]),
                                            ProjectNo = Convert.ToString(row["ProjectNo"]),
                                            CompanyName = Convert.ToString(row["CompanyName"]),
                                            IsDeleted = Convert.ToBoolean(row["IsDeleted"])
                                        }).ToList();
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = projectJobRecord;
                    CD.Count = 0;

                }
                catch (Exception ex)
                {
                    CD.Status = "0";
                    CD.Message = "Fail";
                    CD.Data = "";
                    CD.Count = 0;
                    string error = ex.Message;
                    throw;
                }

                #endregion
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                CD.Count = 0;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - BankDetailListandViewController");
            }
            return CD;
        }
    }
}
