﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class NewlyRegisterUserListController : ApiController
    {
        public CommonData NewlyRegisterUserList(DataTableModel model)
        {
            int count = 0;
            var permission = new UserPermissionViewModel();
            Dashboard dash = new Dashboard();

            permission = CheckUserModulePermission(model.UserID, "User Management");

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            if (string.IsNullOrEmpty(model.SortColumn))
            {
                model.SortColumn = "FirstName";
            }

            if (string.IsNullOrEmpty(model.SortDirection))
            {
                model.SortDirection = "ASC";
            }

            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                model.SearchTerm = "";
            }

            CommonData CD = new CommonData();
            try
            {
                model.ApplicationOperation = "UserManagement_ListUser";

                if (model.RoleID != 1)
                {
                    // user don't have permission to access User management module
                    if (!permission.HasViewPermission)
                    {
                        var userList = new List<UserEntity>();
                        CD.Status = "0";
                        CD.Message = "You don't have permission to access User management module.";
                        return CD;
                    }
                    else
                    {
                        var user = dash.NewlyRegisteredUser(model, ref count);

                        if (!string.IsNullOrEmpty(model.SearchTerm))
                        {
                            count = user.Count;
                        }
                        CD.Count = count;
                        CD.Status = "1";
                        CD.Message = "Success";
                        CD.Data = user;
                        return CD;
                    }
                }
                else
                {
                    var user = dash.NewlyRegisteredUser(model, ref count);

                    if (!string.IsNullOrEmpty(model.SearchTerm))
                    {
                        count = user.Count;
                    }
                    CD.Count = count;
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = user;
                    return CD;
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Somthing went wrong...!";
                CD.Data = ex.Message.ToString();
                return CD;
            }

        }

        public UserPermissionViewModel CheckUserModulePermission(int userId, string moduleName)
        {
            UserDAL UD = new UserDAL();
            UserPermissionViewModel UserPermissionlst = new UserPermissionViewModel();
            UserPermissionlst = (from row in UD.CheckModulePermission(userId, moduleName)
                                 select new UserPermissionViewModel
                                 {
                                     UserID = Convert.ToInt32(row["UserID"]),
                                     ModuleId = Convert.ToInt32(row["ModuleId"]),
                                     ModuleURL = (row["URL"]).ToString(),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                                     HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                                     HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                                     ModulePermissionName = (row["ModulePermissionName"]).ToString(),
                                     ModuleDisplayName = (row["DisplayName"]).ToString(),
                                     SequenceNO = Convert.ToInt32(row["SequenceNO"]),
                                     Restricted = Convert.ToInt32(row["Restricted"]),
                                     VisibleOnMenu = Convert.ToInt32(row["VisibleOnMenu"]),
                                 }).FirstOrDefault();

            return UserPermissionlst;
        }
    }
}
