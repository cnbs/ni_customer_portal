﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetJobMasterListingController : ApiController
    {
        [HttpPost]
        public CommonData GetJobMasterListing(DataTableModel model)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();

            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                model.SearchTerm = string.Empty;
            }

            if (string.IsNullOrEmpty(model.LogDateFrom))
            {
                model.LogDateFrom = string.Empty;
            }

            if (string.IsNullOrEmpty(model.LogDateTo))
            {
                model.LogDateTo = string.Empty;
            }

            List<ProjectMilestoneEntity> projectListResult = new List<ProjectMilestoneEntity>();

            CalendarDAL obj = new CalendarDAL();
            try
            {
                CommonFunction CF = new CommonFunction();

                projectListResult = (from row in obj.GetJobData(model)
                                     select new ProjectMilestoneEntity
                                     {
                                         ProjectNo = Convert.ToString(row["ProjectNo"]),
                                         ProjectName = Convert.ToString(row["ProjectName"]),
                                         JobNo = Convert.ToString(row["JobNo"]),
                                         JobDesc = Convert.ToString(row["BankDesc"]),
                                         MilestoneId = Convert.ToInt32(row["MilestoneId"]),
                                         MilestoneShortName = Convert.ToString(row["Milestone"]),
                                         MilestoneName = Convert.ToString(row["MilestoneFullName"]),
                                         StatusName = Convert.ToString(row["Status"]),
                                         Date = (string.IsNullOrEmpty(Convert.ToString(row["Date"])) ? "": CF.setDateToshort(Convert.ToString(row["Date"]))),
                                         Color = ((Convert.ToString(row["Status"]) == "O") ? "#ec4a4a" :
                                                 (Convert.ToString(row["Status"]) == "C") ? "#15bb15" :
                                                 (Convert.ToString(row["Status"]) == "P") ? "#bbbbbb" : "#ffdf36").ToString()
                                     }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = projectListResult;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobMasterListing");
            }
            return CD;
        }
    }
}
