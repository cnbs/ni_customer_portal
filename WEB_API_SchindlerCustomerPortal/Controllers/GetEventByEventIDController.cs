﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetEventByEventIDController : ApiController
    {
        [HttpGet]
        public CommonData GetEventByEventID(string ID)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();
            CommonFunction commonFunction = new CommonFunction();

            var eventDetail = new List<CalendarEventEntity>();

            try
            {
                eventDetail = (from row in calDal.SelectEventByID(ID)
                                 select new CalendarEventEntity()
                                 {
                                     Id = Convert.ToInt32(row["CalTaskMstId"]),
                                     SendTo = (string.IsNullOrEmpty(Convert.ToString(row["EmailSentTo"]))) ? "" :Convert.ToString(row["EmailSentTo"]).Trim() + ", ",
                                     Subject = Convert.ToString(row["EmailSubject"]),
                                     Location = Convert.ToString(row["Location"]),
                                     StartDate = commonFunction.setDateToshort(Convert.ToString(row["StartDt"])),
                                     EndDate = commonFunction.setDateToshort(Convert.ToString(row["EndDt"])),
                                     MessageBody = Convert.ToString(row["MessageBody"]),
                                     CreatedBy = Convert.ToString(row["CreatedBy"]),
                                     CategoryData = {
                                       CatID = Convert.ToInt32(row["CategoryId"]),
                                       CategoryName = Convert.ToString(row["CategoryName"]),
                                       ColorCode = Convert.ToString(row["ColorCode"])
                                     }
                              }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = eventDetail;

                GetCalendarCategoryListController calendarCategoryList = new GetCalendarCategoryListController();
                CD.List = calendarCategoryList.GetCalendarCategoryList().Data;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetEventByEventID");
            }
            return CD;
        }
    }
}
