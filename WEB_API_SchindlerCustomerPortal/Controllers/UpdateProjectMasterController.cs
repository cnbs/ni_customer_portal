﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateProjectMasterController : ApiController
    {
        [HttpPost]
        public CommonData UpdateProjectMaster(ProjectMasterEntity projectMaster)
        {
            CommonData CD = new CommonData();
            ManageProjectDAL MPD = new ManageProjectDAL();
            bool result = MPD.UpdateProjectMaster(projectMaster);
            CD.Status = (result == true) ?"1" : "0";
            CD.Message = (result == true) ? "Successfully Data Updated..." : "Fail to update record...";
            return CD;
        }
    }
}
