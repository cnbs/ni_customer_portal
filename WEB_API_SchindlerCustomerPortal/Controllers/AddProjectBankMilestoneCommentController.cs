﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddProjectBankMilestoneCommentController : ApiController
    {
        [HttpPost]
        public CommonData AddProjectBankMilestoneComment(ProjectStatusDueDateEntity model)
        {
            CommonData CD = new CommonData();
            ProjectDAL PD = new ProjectDAL();
            try
            {
                model.Milestone = model.Milestone.Replace("’", "'").ToString();

                string ProjectBankTitle = model.ProjectNo + "_" + model.JobNo + "_" + model.Milestone;
                if (string.IsNullOrEmpty(model.Comment))
                {
                    model.Comment ="";
                }
                int userId = model.UserId;

                var ProjData = PD.GetProjectandJobPreviousStatus(model.ProjectNo, model.JobNo).Where(a => Convert.ToString(a.ItemArray[4]) == model.Milestone.ToString()).ToList();

                string ProjJob = ProjData[0].ItemArray[0].ToString();

                model.CompanyId = Convert.ToInt32(ProjData[0]["CompanyId"].ToString().Trim());
                model.ProjectName = ProjJob.Split('|')[0].ToString().Trim();
                model.JobDesc = ProjJob.Split('|')[1].ToString().Trim();

                model.PreviousStatusName = ProjData[0]["StatusName"].ToString().Trim();
                model.StatusName = PD.GetStatusFromStatusID(model.StatusId);

                bool result = PD.AddProjectBankMilestoneCommentDueDate(model.Comment, ProjectBankTitle, model.UserId, model.StatusId, model.DueDate, model.CompletedDate);

                #region Email Send

                var mailNotificationUserList = (from row in PD.GetProjectAssignedUserListForMilestoneEmail(model.ProjectNo)
                                                select new UserEntity
                                                {
                                                    Email = Convert.ToString(row["Email"]),
                                                    FirstName = Convert.ToString(row["FirstName"]),
                                                    DeviceID = Convert.ToString(row["DeviceID"]),
                                                    DeviceType = Convert.ToString(row["DeviceType"]),
                                                    UserID = Convert.ToInt32(row["UserID"]),
                                                }).ToList();

                if (model.StatusName.ToUpper() != model.PreviousStatusName.ToUpper())
                {
                    foreach (var item in mailNotificationUserList)
                    {
                        string Subject = "myschindlerprojects – [[ProjectNoProjectNameBankNoBankDesc]] status has been updated to [[StatusName]].";
                        //string headurl = Request.RequestUri.AbsoluteUri;
                        //var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                        string headurl = WebConfigurationManager.AppSettings["BaseURL"].ToString() + "/ManageProject/ProjectJobMainView/"+ model.JobNo + "/"+ model.ProjectNo + "/"+ model.CompanyId + "/" + "Milestone";
                        string Sbody = null;
                        StaticUtilities SU = new StaticUtilities();

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", model.Milestone);
                        dic.Add("ProjectNo", model.ProjectNo + (model.JobNo != null ? " - " + model.JobNo : ""));
                        dic.Add("StatusName", model.StatusName);
                        dic.Add("PreviousStatusName", model.PreviousStatusName);
                        //dic.Add("UserName", User.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (model.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName + " and Bank " + model.JobDesc + " [" + model.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        Subject = Subject.Replace("[[ProjectNoProjectNameBankNoBankDesc]]", ProjectNoProjectNameBankNoBankDesc).Replace("[[StatusName]]", model.StatusName);

                        dic.Add("MessageURL", headurl);

                        Sbody = SU.getEmailBody("ProjectMilestone", "MilestoneStatusChangeNew.html", dic);

                        SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item.Email, Subject, Sbody);

                    }

                    #region Push Notification
                    foreach (var item in mailNotificationUserList)
                    {
                        //string headurl = Request.RequestUri.AbsoluteUri;
                        string headurl = WebConfigurationManager.AppSettings["BaseURL"].ToString();

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", model.MilestoneName);
                        dic.Add("ProjectNo", model.ProjectNo + (model.JobNo != null ? " - " + model.JobNo : ""));
                        dic.Add("StatusName", model.StatusName);
                        dic.Add("PreviousStatusName", model.PreviousStatusName);
                        dic.Add("UserName", item.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (model.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName + " and Bank " + model.JobDesc + " [" + model.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        dic.Add("MessageURL", headurl);

                        string IsNotificationEnable = WebConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = WebConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        StaticUtilities SU = new StaticUtilities();
                        UserDAL UD = new UserDAL();
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "Milestone Status Changed";
                                string NotificationBody = SU.getNotificationBody("ProjectMilestone", "MilestoneStatusChangeNew.txt", dic);

                                try
                                {
                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {

                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }

                                //Insert into NotificationLogId table

                                data.CompanyID = item.CompanyID;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(item.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                data.CreatedBy = item.FirstName + " " + item.LastName;
                                data.DeviceType = item.DeviceType;
                                data.ModuleID = 28;
                                var Notify = UD.InsertNotification(data);
                            }
                        }
                    }
                    #endregion
                }

                #endregion


                CD.Status = "1";
                CD.Message = "Record updated successfully...";
                CD.Data = result;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong while adding milestone comment...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectBankMilestoneCommentController");
            }
            return CD;
        }
    }
}
