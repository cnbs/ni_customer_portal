﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateProjectContactController : ApiController
    {
        [HttpPost]
        public CommonData UpdateProjectContact(ProjectContactEntity model)
        {
            if (!string.IsNullOrEmpty(model.JobName))
            {
                if (string.IsNullOrEmpty(model.JobNo))
                {
                    model.JobNo = "-1";
                }
            }
            else
            {
                model.JobNo = "";
            }


            if (string.IsNullOrEmpty(model.ContactNumber))
            {
                model.ContactNumber = string.Empty;
            }
            else
            {
                model.ContactNumber = model.ContactNumber.Replace("-", "");
            }

            ProjectContactDAL PCD = new ProjectContactDAL();
            CommonData CD = new CommonData();

            int appAuditID = 0;
          
            model.appAuditID = appAuditID;
            model.ApplicationOperation = "ProjectManagement_UpdateProjectContact";

            try
            {
                var user = PCD.UpdateProjectContact(model);
                CD.Status = "1";
                CD.Message = "Successfully Data Updated...";
                CD.Data = "Successfully Data Updated...";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Failure...";
                CD.Data = "Something went wrong...";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectContactController");
            }
            return CD;
        }
    }
}
