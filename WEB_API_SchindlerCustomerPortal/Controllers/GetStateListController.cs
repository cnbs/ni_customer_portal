﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetStateListController : ApiController
    {
        [HttpGet]
        public CommonData stateList(int countryId)
        {
            CommonData CD = new CommonData();
            try
            {
                UserDAL UD = new UserDAL();
                List<StateEntity> state = (from row in UD.GetStateList(countryId)
                                           select new StateEntity
                                           {
                                               StateID = Convert.ToInt32(row["StateID"]),
                                               CountryId = Convert.ToInt32(row["CountryId"]),
                                               CountryName = row["CountryName"].ToString(),
                                               StateCode = row["StateCode"].ToString(),
                                               StateName = row["StateName"].ToString(),
                                           }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = state;
            }
            catch (SqlException ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStateListController");
            }
            return CD;
        }
    }
}
