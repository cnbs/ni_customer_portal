﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestAPI.Controllers
{
    public class ValuesController : ApiController
    {
        [HttpPost]
        public string PostTestMethod([FromBody]string value)
        {
            return "Hello from http post web api controller: " + value;
        }
    }
}
