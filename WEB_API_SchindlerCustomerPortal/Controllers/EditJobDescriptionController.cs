﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class EditJobDescriptionController : ApiController
    {
        [HttpPost]
        public CommonData EditJobDescription(DataTableModel model)
        {
            CommonData CD = new CommonData();
            model.ApplicationOperation = "UpdateJobDescription";
            ProjectDAL projectService = new ProjectDAL();

            try
            {
                if (string.IsNullOrEmpty(model.JobNo))
                {
                    CD.Status = "0";
                    CD.Message = "Insert Jobno...";
                    CD.Data = "";
                    CD.Count = 0;
                    return CD;
                }

                CD.Data = projectService.Update_BankDescByJobNo(model);
                CD.Status = "1";
                CD.Message = "Job description added successfully...";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                CD.Count = 0;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - BankDetailListandViewController");
            }

            return CD;
        }
    }
}
