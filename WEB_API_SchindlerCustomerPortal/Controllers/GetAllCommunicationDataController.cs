﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetAllCommunicationDataController : ApiController
    {
        [HttpPost]
        public CommonData GetAllCommunication(DataTableModel model)
        {
            ManageProjectDAL MPD = new ManageProjectDAL();
            CommonData CD = new CommonData();

            List<CommunicationEntity> obj = new List<CommunicationEntity>();
            try
            {
                model.ApplicationOperation = "ProjectManagement_CommunicationList";
                if (string.IsNullOrEmpty(model.JobNo))
                    model.JobNo = string.Empty;

                //var result = MPD.GetAllCommunicationList(model.ProjectNo, model.JobNo, model.appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID);

                obj = (from row in MPD.GetAllCommunicationList(model.ProjectNo, model.JobNo, model.appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID)
                       select new CommunicationEntity
                       {
                           CommunicationId = Convert.ToInt32(row["CommunicationId"]),
                           ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                           JobNo = Convert.ToString(row["JobNo"]),
                           UserId = Convert.ToInt32(row["UserId"]),
                           UserName = Convert.ToString(row["Username"]),
                           ProfilePic = (string.IsNullOrEmpty(Convert.ToString(row["ProfilePic"])) ? "" : HttpUtility.UrlPathEncode(WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() + Convert.ToString(row["ProfilePic"]))),
                           Message = Convert.ToString(row["MessageText"]),
                           NotifyToEmail = Convert.ToString(row["NotifiedTo"]),
                           AttachmentPath = (string.IsNullOrEmpty(Convert.ToString(row["AttachmentPath"]))) ? "" : HttpUtility.UrlPathEncode(WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["EmailAttachmentPath"].ToString() + Convert.ToString(row["AttachmentPath"])),
                           //CreatedBy = Convert.ToString(row["CreatedBy"]),
                           CreatedOn = Convert.ToString(row["CreatedDate"]),
                           IsDeleted = Convert.ToBoolean(row["isdeleted"])
                       }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = obj;
                CD.Count = 0;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllCommunicationController");
            }
            return CD;
        }
    }
}
