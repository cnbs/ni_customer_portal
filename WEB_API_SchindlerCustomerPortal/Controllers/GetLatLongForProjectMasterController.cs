﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetLatLongForProjectMasterController : ApiController
    {
        [HttpPost]
        public CommonData GetLatLongForProjectMaster(DataTableModel model)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            #region LogData

            ErrorLogClass lm = new ErrorLogClass();
            lm.DataLogFiles();
            var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
            string LogData = "UserID : " + model.UserID + ", SearchTerm : " + model.SearchTerm;
            lm.ErrorLog(Errorlogpath, LogData + " - GetLatLongForProjectMaster" + DateTime.Now.ToLongDateString());

            #endregion

            try
            {
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = string.Empty;
                }

                List<ProjectGeoLocationEntity> projectListResult = new List<ProjectGeoLocationEntity>();

                ProjectDAL obj = new ProjectDAL();

                projectListResult = (from row in obj.ProjectMasterLatLongDetail(model)
                                     select new ProjectGeoLocationEntity
                                     {
                                         ProjectNo = Convert.ToString(row["ProjectNo"]),
                                         ProjectName = Convert.ToString(row["ProjectName"]),
                                         StatusName = Convert.ToString(row["CurrentProjectStatus"]),
                                         Job_Address = Convert.ToString(row["Job_Address"]),
                                         Job_City = Convert.ToString(row["Job_City"]),
                                         Job_State = Convert.ToString(row["Job_StateName"]),
                                         Job_Zip = Convert.ToString(row["Job_Zip"]),
                                         Latitude = Convert.ToDouble(row["Latitude"]),
                                         Longitude = Convert.ToDouble(row["Longitude"]),
                                         User_Latitude = Convert.ToDouble(row["UserLatitude"]),
                                         User_Longitude= Convert.ToDouble(row["UserLongitude"])
                                     }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = projectListResult;
            }
            catch (Exception ex)
            {
                //ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                //var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailforDashboardWidget");
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message;
            }
            return CD;
        }
    }
}
