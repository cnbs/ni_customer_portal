﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class UpdateDeviceTokenController : ApiController
    {

        [HttpPost]
        public object UpdateDeviceType([FromBody]UserDevice data)
        {
            if(data.APIKey == "" || data.APIKey == null)
            {
                data.APIKey = "";
            }

            UserDevice item = new UserDevice()
            {

                UserID = data.UserID,
                DeviceID = data.DeviceID,
                APIKey = data.APIKey,
                DeviceToken = data.DeviceToken,
                DeviceType = data.DeviceType

            };

            

            var result = LoginModule.UserDeviceType(item.UserID, item.DeviceID, item.DeviceType,item.DeviceToken,item.APIKey);

            DataSet tempds = new DataSet();

            if (result == true)
            {
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "UpdateDeviceType";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "1";
                dr["Message"] = "Your new settings has been updated!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdateDeviceToken", "UpdateDeviceToken Success", data.UserID, data.DeviceID, "Update");

                return tempds;
            }
            else
            {


                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "UpdateDeviceToken";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "No User/Device Matched!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdateDeviceToken", "UpdateDeviceToken - No User/Device Matched", data.UserID, data.DeviceID, "Update");

                return tempds;

            }
        }

    }
}
