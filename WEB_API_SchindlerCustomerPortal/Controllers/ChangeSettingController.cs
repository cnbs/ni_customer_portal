﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class ChangeSettingController : ApiController
    {
        [HttpPost]
        public object ChangeSetting([FromBody]UserDevice data)
        {
            bool pushnotify ;
            UserDevice item = new UserDevice()
            {

                PushNotification = data.PushNotification,
                UserID = data.UserID,
                DeviceID = data.DeviceID

            };

            if(item.PushNotification == "1")
            {
                pushnotify = true;
            }
            else
            {
                pushnotify = false;
            }

            var result = LoginModule.UpdateDeviceSettings(item.UserID, item.DeviceID, pushnotify);

            DataSet tempds = new DataSet();

            if (result == true)
            {
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "UpdateSetting";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "1";
                dr["Message"] = "Your new settings has been updated!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/ChangeSetting", "ChangeSetting Success", data.UserID, data.DeviceID, "Update");

                return tempds;
            }
            else
            {

                
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "UpdateSetting";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "No User/Device Matched!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/ChangeSetting", "ChangeSetting - No User/Device Matched", data.UserID, data.DeviceID, "Update");

                return tempds;

            }
        }

    }
}
