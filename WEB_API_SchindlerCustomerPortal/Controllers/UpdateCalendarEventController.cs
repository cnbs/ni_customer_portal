﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateCalendarEventController : ApiController
    {
        [HttpPost]
        public CommonData UpdateCalendarEvent(CalendarEventEntity model)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();
            CommonFunction commonFunc = new CommonFunction();

            try
            {
                if (string.IsNullOrEmpty(model.SendTo))
                {
                    model.SendTo = string.Empty;
                }
                if (string.IsNullOrEmpty(model.MessageBody))
                {
                    model.MessageBody = string.Empty;
                }
                if (string.IsNullOrEmpty(model.Location))
                {
                    model.Location = string.Empty;
                }
                if (string.IsNullOrEmpty(model.StartDate))
                {
                    model.StartDate = DateTime.Today.ToShortDateString().ToString();
                }
                if (string.IsNullOrEmpty(model.EndDate))
                {
                    model.EndDate = model.StartDate;
                }
                if (string.IsNullOrEmpty(model.AttachmentPath))
                {
                    model.AttachmentPath = string.Empty;
                }

                model.ApplicationOperation = "Update_Calendar_Event";

                int result = calDal.UpdateEvent(model);

                if (result == 1 && (!string.IsNullOrEmpty(model.SendTo.Trim())))
                {
                    model.MailSenderID = model.UpdatedBy;
                    model.EmailHeaderSubject = "myschindlerprojects - Calendar event update notification";
                    model.EmailTemplateName = "CalendarUpdateNotification.html";
                    commonFunc.SendCalendarEmail(model);
                }

                CD.Status = result.ToString();
                CD.Message = (result == 1) ? "Event updated successfully..." : (result == -1) ? "Email address does not exist in the system." : "Event update failed ..!";
                CD.Data = "";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateCalendarEvent");
            }
            return CD;
        }
    }
}
