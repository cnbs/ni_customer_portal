﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UnreadMailCountforHeaderController : ApiController
    {
        [HttpPost]
        public CommonData UnreadMailCountforHeader(UserEntity User)
        {
            var model = new List<MailBoxEntity>();
            MailBoxDAL MBD = new MailBoxDAL();
            CommonData CD = new CommonData();

            if (User != null)
            {
                model = (from row in MBD.GetUserMailforHeaderMenu(User.Email)
                         select new MailBoxEntity
                         {
                             MailId = Convert.ToInt32(row["MailId"]),
                             Sender = row["SenderId"].ToString(),
                             Receiver = row["ReceiverId"].ToString(),
                             Subject = row["Subject"].ToString(),
                             UserName = Convert.ToString(row["UserName"]),
                             ProfilePic = (string.IsNullOrEmpty(row["ProfilePic"].ToString())) ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                          WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() +
                                          row["ProfilePic"].ToString(),
                         }).ToList();

                if (model.Count > 0)
                {
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = model;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No record found..!";
                    CD.Data = model;
                }
            }
            else
            {
                CD.Status = "0";
                CD.Message = "Please insert valid email..!";
                CD.Data = model;
            }
            return CD;
        }
    }
}
