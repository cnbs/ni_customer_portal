﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UnreadMailForDashboardWidgetController : ApiController
    {
        [HttpPost]
        public CommonData UnreadMailForDashboardWidget(UserEntity model)
        {
            MailBoxDAL MBD = new MailBoxDAL();
            CommonData CD = new CommonData();
            var userList = new List<MailBoxEntity>();

            try
            {
                userList = (from row in MBD.GetUserMailforDashboardWidget(model.Email)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                ViewStatus = Convert.ToString(row["View_Status"]),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                UserName = Convert.ToString(row["UserName"]),
                                ProfilePic = (string.IsNullOrEmpty(row["ProfilePic"].ToString())) ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                             WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() +
                                             row["ProfilePic"].ToString(),
                                DateSent = Convert.ToString(row["DateSent"]),
                            }).ToList();

                if (userList.Count > 0)
                {
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = userList;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No record Found..!";
                    CD.Data = userList;
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UnreadMailForDashboardWidgetController");
            }
            return CD;
        }
    }
}
