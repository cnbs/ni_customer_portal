﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AllUserMailforHeaderMenuController : ApiController
    {
        [HttpPost]
        public CommonData AllUserMailforHeadermenu(DataTableModel model)
        {
            var userList = new List<MailBoxEntity>();
            MailBoxDAL MBD = new MailBoxDAL();
            CommonData CD = new CommonData();
            try
            {
                userList = (from row in MBD.GetUserMailforHeaderMenu(model.UserEmail)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                UserName = Convert.ToString(row["UserName"]),
                                ProfilePic = WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"] + row["ProfilePic"].ToString(),
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AllUserMailforHeaderMenuController");
            }
            return CD;
        }
    }
}
