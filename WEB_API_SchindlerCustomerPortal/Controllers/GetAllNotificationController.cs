﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetAllNotificationController : ApiController
    {
        [HttpPost]
        public CommonData AllNotificationList(NotificationEntity model)
        {
            CommonData CD = new CommonData();
            List<NotificationEntity> notificationList = new List<NotificationEntity>();
            int count = 0;
            try
            {
                UserDAL UD = new UserDAL();
                CommonFunction CF = new CommonFunction();

                notificationList = (from row in UD.UserNotificationList(model.ReceiverID, ref count)
                                    select new NotificationEntity
                                    {
                                        CompanyID = Convert.ToInt32(row["CompanyId"]),
                                        CompanyName = Convert.ToString(row["CompanyName"]),
                                        ReceiverID = Convert.ToString(row["ReceiverId"]),
                                        ReceiverName = Convert.ToString(row["ReceiverName"]),
                                        Subject = Convert.ToString(row["Subject"]),
                                        Message = Convert.ToString(row["Message"]),
                                        SentDate = string.IsNullOrEmpty(Convert.ToString(row["DateSent"])) ? Convert.ToString(row["DateSent"]) : Convert.ToDateTime(row["DateSent"].ToString()).ToString("MM/dd/yyyy"),
                                        StatusID = Convert.ToInt32(row["StatusId"]),
                                        StatusName = Convert.ToString(row["StatusName"]),
                                        ModuleID = Convert.ToInt32(row["ModuleId"]),
                                        ModuleName = Convert.ToString(row["ModuleName"])
                                    }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = notificationList;
                CD.Count = count;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllNotificationController");
            }
            return CD;
        }
    }
}
