﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AbsorptionDataController : ApiController
    {
        [HttpPost]
        public CommonData GetAbsorptionData()
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                DataSet ds = new DataSet();
                ds = DashboardDataModule.GetAbsorptionDetail();

                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<AbsorptionData> AD = new List<AbsorptionData>();

                    AD = (from row in ds.Tables[0].AsEnumerable().ToList()
                          select new AbsorptionData
                          {
                              Status = Convert.ToString(row[0]),
                              Absorption = Convert.ToString(row[1]),
                              MonthToDate = Convert.ToString(row[2]),
                              OPS_FY = Convert.ToString(row[3]),
                              AbsorptionPercOfOPS = Convert.ToString(row[4]),
                              TargetPercOfMTD = Convert.ToString(row[5])
                          }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = AD;
                }
            }
            catch (System.Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetAbsorptionData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
