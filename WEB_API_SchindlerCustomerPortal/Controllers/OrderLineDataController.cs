﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class OrderLineDataController : ApiController
    {
        [HttpPost]
        public CommonData GetOrderLineTradeData()
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;
            try
            {
                DataSet ds = new DataSet();
                ds = DashboardDataModule.GetOrderLineTradeDetail();

                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<OrderLineData> OLD = new List<OrderLineData>();

                    OLD = (from row in ds.Tables[0].AsEnumerable().ToList()
                           select new OrderLineData
                           {
                               OTD_Trade_Inter_Intra = Convert.ToString(row[0]),
                               To_CRD = Convert.ToString(row[1]),
                               NumberOfOrder_Line_Late_CRD = Convert.ToString(row[2]),
                               To_SSD = Convert.ToString(row[3]),
                               NumberOfOrder_Line_Late_SSD = Convert.ToString(row[4]),
                               TotalLinesShipped = Convert.ToString(row[5])
                           }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = OLD;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetOrderLineTradeData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
