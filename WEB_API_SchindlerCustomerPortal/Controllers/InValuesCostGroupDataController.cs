﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class InValuesCostGroupDataController : ApiController
    {
        [HttpPost]
        public CommonData GetInValuesCostGroupData()
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;
            try
            {
                DataSet ds = new DataSet();
                ds = DashboardDataModule.GetInValueCostGroupDetail();

                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<InValueCostGroupData> IVCG = new List<InValueCostGroupData>();

                    IVCG = (from row in ds.Tables[0].AsEnumerable().ToList()
                            select new InValueCostGroupData
                            {
                                Status = Convert.ToString(row[0]),
                                Inventory = Convert.ToString(row[1]),
                                RAW = Convert.ToString(row[2]),
                                WP = Convert.ToString(row[3]),
                                FG = Convert.ToString(row[4]),
                                TotalInventoryEOD = Convert.ToString(row[5]),
                                OPS_FY = Convert.ToString(row[6]),
                                Difference = Convert.ToString(row[7]),
                                PercOfOPS = Convert.ToString(row[8])
                            }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = IVCG;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetInValuesCostGroupData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
