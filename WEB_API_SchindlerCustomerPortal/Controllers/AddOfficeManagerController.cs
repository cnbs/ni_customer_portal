﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddOfficeManagerController : ApiController
    {
        [HttpPost]
        public CommonData AddOfficeManager(OfficeManagerEntity model)
        {
            CommonData CD = new CommonData();

            try
            {
                model.CreatedBy = model.UserID.ToString();
                OfficeManagerDAL OMD = new OfficeManagerDAL();

                if (string.IsNullOrEmpty(model.Phone))
                    model.Street = string.Empty;
                else
                    model.Phone = model.Phone.Replace("-", "");

                if (string.IsNullOrEmpty(model.Street))
                    model.Street = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.Zip))
                    model.Zip = string.Empty;

                model.Email = model.Email.ToLower();

                var user = OMD.InsertOfficeManager(model);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = user;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddOfficeManagerController");
            }
            return CD;
        }
    }
}
