﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class RemoveDocAndDrawingDataController : ApiController
    {
        [HttpPost]
        public CommonData DeleteDocumentService(DocumentEntity doc)
        {
            CommonData CD = new CommonData();
            ManageDocumentDAL MDD = new ManageDocumentDAL();
            doc.ApplicationOperation = "ProjectManagement_DeleteProjectDocument-";
            var result = MDD.DeleteDocument(doc);
            if (result)
            {
                CD.Status = "1";
                CD.Message = "Successfully Document Deleted...";
            }
            else
            {
                CD.Status = "0";
                CD.Message = "Record Not Found...";
            }
            CD.Data = result;
            return CD;
        }
    }
}
