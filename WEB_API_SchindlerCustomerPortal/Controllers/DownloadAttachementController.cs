﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Models;
//using System.Web.Mvc;
using System.IO;
using System.Net.Http.Headers;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Common;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DownloadAttachementController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage DownloadAttachement(MailBoxEntity model)
        {
            byte[] fileBytes;

            model.AttachmentPath = System.Web.HttpUtility.UrlDecode(model.AttachmentPath);

            var comPath = WebConfigurationManager.AppSettings["mainDocPath"].ToString() + 
                          model.AttachmentPath;

            CommonData CD = new CommonData();

            try
            {
                if (System.IO.File.Exists(comPath))
                {
                    //Actuall code
                    //fileBytes = System.IO.File.ReadAllBytes(comPath);
                    //string fileName = Path.GetFileName(comPath);
                    //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);


                    //new added code
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream = new FileStream(comPath, FileMode.Open);
                    //result.Content = new StreamContent(stream);
                    //result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    return result;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "File not found..!";
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.NoContent);
                    return result;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DownloadAttachement");
                throw ex;
            }
            finally
            {
                fileBytes = null;
                GC.Collect();
            }
        }
    }
}
