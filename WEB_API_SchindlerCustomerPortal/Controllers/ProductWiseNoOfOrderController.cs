﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ProductWiseNoOfOrderController : ApiController
    {
        [HttpPost]
        public CommonData GetProductWiseNoOfOrderData(YearData yearList)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                string yearData = string.Empty;
                DataSet ds = new DataSet();

                if (yearList.Year[0] == null) {
                    yearData = DateTime.Now.Year.ToString();
                } else {
                    yearData = string.Join(",", yearList.Year);
                }

                ds = DashboardDataModule.GetProductWiseNoOfOrderDetail(yearData);
                CD = GetDataFromDS.GetCommonData(ds);
            }
            catch (System.Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetProductWiseNoOfOrderData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return null;
            }
            return CD;
        }
    }
}
