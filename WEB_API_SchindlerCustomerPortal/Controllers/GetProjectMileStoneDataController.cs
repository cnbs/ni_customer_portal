﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetProjectMileStoneDataController : ApiController
    {
        [HttpPost]
        public CommonData GetProjectMileStone(DataTableModel model)
        {
            MilestoneDAL MD = new MilestoneDAL();
            ManageProjectDAL MPD = new ManageProjectDAL();
            List<ProjectMilestoneEntity> docList = new List<ProjectMilestoneEntity>();
            CommonData CD = new CommonData();

            try
            {
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }
                model.ApplicationOperation = "ProjectManagement_ProjectMilestoneList".ToString();

                if(string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "";
                }

                if(string.IsNullOrEmpty(model.SortDirection))
                {
                    model.SortDirection = "";
                }
                int count = 0;

                CommonFunction CF = new CommonFunction();

                docList = (from row in MD.MilestoneSelect(model, ref count)
                           select new ProjectMilestoneEntity
                           {
                               MilestoneTitle = Convert.ToInt32(row["MilestoneTitle"]),
                               MilestoneId = Convert.ToInt32(row["Milestoneid"]),
                               MilestoneName = CF.setNullToEmpty(Convert.ToString(row["MilestoneName"])),
                               JobNo = Convert.ToString(row["JobNo"]),
                               PredecessorName = CF.setNullToEmpty(Convert.ToString(row["Predecessor"])),
                               SuccessorName = CF.setNullToEmpty(Convert.ToString(row["Successor"])),
                               Status = Convert.ToInt32(row["StatusId"]),
                               DueDate = string.IsNullOrEmpty(row["DueDate"].ToString()) ? "" : Convert.ToDateTime(row["DueDate"]).ToString("MM/dd/yyyy"),
                               StatusName = CF.setNullToEmpty(Convert.ToString(row["StatusName"])),
                               WaitingOn = CF.setNullToEmpty(Convert.ToString(row["WaitingOn"])),
                               Note = CF.setNullToEmpty(Convert.ToString(row["Note"])),
                               CompletedDate = string.IsNullOrEmpty(row["CompletedDate"].ToString()) ? "" : Convert.ToDateTime(row["CompletedDate"]).ToString("MM/dd/yyyy")
                           }).Where(a => a.MilestoneName.ToLower() != "material ordered").ToList();

                CD.StatusList = (from row in MPD.GetStatusList("Milestone")
                                 select new StatusData
                                 {
                                     Status = Convert.ToInt32(row["StatusId"]),
                                     StatusName = row["StatusName"].ToString(),
                                 }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = docList;
                CD.Count = count;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectMileStoneDataController");
            }
            return CD;
        }
    }
}
