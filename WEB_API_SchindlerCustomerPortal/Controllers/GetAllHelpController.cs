﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetAllHelpController : ApiController
    {
        protected void SetSubMenu(List<HelpEntity> list, List<HelpEntity> newList)
        {
            foreach (var item in newList)
            {
                item.SubMenuList.AddRange(list.Where(x => x.ParentId == item.ModuleId));
            }
            foreach (var item in list.Where(x => x.ParentId != 0).OrderBy(x => x.ParentId).OrderBy(x => x.ParentSequenceNo))
            {
                if (!newList.Any(x => x.ModuleId == item.ModuleId))
                {
                    newList.AddRange(list.Where(x => x.ParentId == item.ParentId));
                }
            }
        }

        //protected void SetSubMenu(List<HelpEntity> list, List<HelpEntity> newList)
        //{
        //    foreach (var item in list)
        //    {
        //        if(list.Any(x => x.ParentSequenceNo < item.SequenceNo && x.ParentSequenceNo != 0))
        //        {
        //            List<HelpEntity> tempList = list.Where(x => x.ParentSequenceNo < item.SequenceNo && x.ParentSequenceNo != 0).OrderBy(x => x.ParentSequenceNo).ToList();

        //            if (!tempList.Any(x => newList.Contains(x)))
        //            {
        //                foreach (var item1 in tempList)
        //                {
        //                    List<HelpEntity> parentList = list.Where(x => x.SequenceNo < item1.ParentSequenceNo && x.ParentId == 0).OrderBy(x => x.SequenceNo).ToList();
        //                    if (!newList.Any(x => parentList.Contains(x)))
        //                        newList.AddRange(parentList);

        //                    newList.Add(item1);
        //                }
        //            }

        //        }
        //        else
        //        {
        //            if (!newList.Any(x => x.HelpId == item.HelpId))
        //            {
        //                newList.Add(list.Where(x => x.ModuleId == item.ModuleId).FirstOrDefault());
        //            }
        //        }
        //    }
        //}

        [HttpGet]
        public CommonData GetHelp()
        {

            CommonData CD = new CommonData();
            try
            {
                var helpList = new List<HelpEntity>();
                HelpDAL HD = new HelpDAL();

                helpList = (from row in HD.GetHelp()
                            select new HelpEntity
                            {
                                ParentSequenceNo = (string.IsNullOrEmpty(Convert.ToString(row["ParentSequenceNo"]))) ? 0 : Convert.ToInt16(row["ParentSequenceNo"]),
                                HelpId = Convert.ToInt32(row["HelpId"]),
                                ParentId = Convert.ToInt32(row["ParentId"]),
                                SequenceNo = Convert.ToInt32(row["SequenceNo"]),
                                ModuleName = Convert.ToString(row["ModuleName"]),
                                ModuleId = Convert.ToInt32(row["ModuleId"]),
                                Title = Convert.ToString(row["Title"]),
                                Description = CommonFunction.StripHTML(Convert.ToString(row["Description"])),
                                PublishedStatus = Convert.ToBoolean(row["PublishedStatus"])

                            }).ToList();

                 //var newList = helpList.Where(x => x.ParentId == 0).OrderBy(x => x.SequenceNo).ToList();
                //SetSubMenu(helpList, newList);
               // List<HelpEntity> newList = new List<HelpEntity>();
               // SetSubMenu(helpList, newList);
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = helpList;

            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Somthing went wrong....!";
                CD.Data = ex.Message.ToString();
            }
            return CD;
        }
    }
}
