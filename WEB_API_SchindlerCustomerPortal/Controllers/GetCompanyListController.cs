﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Models;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetCompanyListController : ApiController
    {
        [HttpGet]
        public CommonData GetDrawingAndDocument()
        {
            CommonData CD = new CommonData();
            List<CompanyMaster> companyList = new List<CompanyMaster>();

            try
            {
                CompanyDAL company = new CompanyDAL();
                companyList = (from row in company.CompanyMaster()
                               select new CompanyMaster
                               {
                                   CompanyID = Convert.ToInt32(row["CompanyID"]),
                                   Name = Convert.ToString(row["CompanyName"])
                               }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = companyList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetCompanyListController");
            }
            return CD;
        }
    }
}
