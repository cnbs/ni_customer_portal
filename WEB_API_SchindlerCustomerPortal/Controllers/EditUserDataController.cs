﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Security;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class EditUserDataController : ApiController
    {
       //public readonly CommonFunction commonFunctionLat;


        //public EditUserDataController(CommonFunction commonFunction)
        //{
        //    this._commonFunction = commonFunction;
        //}

        [HttpPost]
        public CommonData UpdateUser()
        {
            CommonData CD = new CommonData();
            UserDAL UD = new UserDAL();
            UserEntity model = new UserEntity();
            StaticUtilities SU = new StaticUtilities();
            CommonFunction CF = new CommonFunction();

            try
            {
                model.FirstName = Convert.ToString(HttpContext.Current.Request.Form["FirstName"]);
                model.LastName = "";
                model.DesignationId = Convert.ToInt32(HttpContext.Current.Request.Form["DesignationId"]);
                model.Address1 = Convert.ToString(HttpContext.Current.Request.Form["Address1"]);
                model.State = Convert.ToString(HttpContext.Current.Request.Form["State"]);
                model.City = Convert.ToString(HttpContext.Current.Request.Form["City"]);
                model.PostalCode = Convert.ToString(HttpContext.Current.Request.Form["PostalCode"]);
                model.ContactNumber = Convert.ToString(HttpContext.Current.Request.Form["ContactNumber"]);
                model.UserID = Convert.ToInt32(HttpContext.Current.Request.Form["UserID"]);
                model.Unsubscribe = Convert.ToBoolean(HttpContext.Current.Request.Form["Unsubscribe"]);

                model.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
                model.DeviceID = Convert.ToString(HttpContext.Current.Request.Form["DeviceID"]);
                model.SessionID = Convert.ToString(HttpContext.Current.Request.Form["SessionID"]);
                model.IsDeleted = Convert.ToInt32(HttpContext.Current.Request.Form["IsDeleted"]);
                model.StateText = Convert.ToString(HttpContext.Current.Request.Form["StateText"]);
                model.UpdatedBy = model.UserID.ToString();

                if (string.IsNullOrEmpty(model.ContactNumber))
                {
                    model.ContactNumber = string.Empty;
                }
                else
                {
                    model.ContactNumber = model.ContactNumber.Replace("-", "");
                }

                string CurrentDateTime = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var logo = System.Web.HttpContext.Current.Request.Files["img"];
                    if (logo.ContentLength > 0)
                    {
                        var imgpath = WebConfigurationManager.AppSettings["userProfilePicPath"].ToString();
                        var imgSave = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString();

                        var fileName = CurrentDateTime + "_" + (logo.FileName);

                        //imgSave = imgSave + fileName;

                        logo.SaveAs(imgSave + fileName);
                        model.ProfilePic = fileName;

                        // Start : Thumbnail code added by Vivek
                        Image image = Image.FromFile(imgSave + fileName);
                        Size thumbnailSize = SU.GetThumbnailSize(image);
                        // Get thumbnail.
                        Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                            thumbnailSize.Height, null, IntPtr.Zero);
                        var thumbpath = Path.Combine(imgSave, "Thumb_" + fileName);
                        // Save thumbnail.
                        thumbnail.Save(thumbpath);
                        // End : Thumbnail code added by Vivek
                    }
                    else
                    {
                        model.ProfilePic = "-1";
                    }
                }
                else
                {
                    model.ProfilePic = "-1";
                }

                if(model.IsDeleted == 1)
                {
                    model.ProfilePic = "no_images.png";
                }

                if (string.IsNullOrEmpty(model.Address1))
                    model.Address1 = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.PostalCode))
                    model.PostalCode = string.Empty;

                string token = (Guid.NewGuid().ToString("N"));

                var getUserCompanyName = GetUserById(model.UserID);

                if (getUserCompanyName.CompanyID == 0)
                {
                    model.ApplicationOperation = "EmployeeManagement_UpdateEmployee";
                }
                else
                {
                    model.ApplicationOperation = "UserManagement_UpdateUser";
                }
                model.AccountStatus = getUserCompanyName.AccountStatus;
                model.Requested_Projects = "";

                // Added By Arpit Patel
                // On:02-Apr-2018
                // Desc: Update User Lat. Long.
                var address = "";
                address = Convert.ToString(model.City) + " " + Convert.ToString(model.StateText) + " " + Convert.ToString(model.PostalCode);
                Tuple<double, double> abc = CF.GenerateLatLong(address);
                model.Latitude = abc.Item1;
                model.Longitude = abc.Item2;


                bool result = UD.UpdateUser(model);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = result;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - EditUserDataController");
            }
            return CD;
        }

        public UserEntity GetUserById(int userId)
        {
            UserEntity user = new UserEntity();
            UserDAL UD = new UserDAL();

            user = (from row in UD.CompanyUserSelectById(userId)
                    select new UserEntity
                    {
                        Email = row["Email"].ToString(),
                        RoleID = Convert.ToInt32(row["RoleID"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        //LastName = row["LastName"].ToString(),
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        //ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                        Designation = row["DesignationName"].ToString(),
                        DesignationId = Convert.ToInt32(row["DesignationId"]),
                        Address1 = Convert.ToString(row["Address1"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        PostalCode = Convert.ToString(row["PostalCode"]),
                        AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                        Requested_Projects = Convert.ToString(row["Requested_Projects"])
                        //CreatedBy = row["CreatedBy"].ToString(),
                        //UpdatedBy = row["UpdatedBy"].ToString(),

                    }).FirstOrDefault();
            return user;
        }
    }
}
