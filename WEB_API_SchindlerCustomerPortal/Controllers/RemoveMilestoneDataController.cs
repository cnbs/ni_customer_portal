﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class RemoveMilestoneDataController : ApiController
    {
        [HttpPost]
        public CommonData DeleteMilestoneService(ProjectMilestoneEntity doc)
        {
            CommonData CD = new CommonData();
            MilestoneDAL MD = new MilestoneDAL();
            doc.ApplicationOperation = "ProjectManagement_DeleteProjectMilestone";
            doc.DeletedBy = doc.UserID.ToString();

            try
            {
                bool result = MD.DeleteMilestone(doc);

                CD.Status = "1";
                CD.Message = "Record Succesfully Deleted...";
                CD.Data = result;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - RemoveMilestoneDataController");
            }
            return CD;
        }
    }
}