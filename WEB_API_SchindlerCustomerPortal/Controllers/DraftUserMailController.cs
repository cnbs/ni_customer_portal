﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DraftUserMailController : ApiController
    {
        [HttpPost]
        public CommonData DraftUserMail()
        {
            MailBoxEntity model = new MailBoxEntity();
            CommonData CD = new CommonData();
            MailBoxDAL MBD = new MailBoxDAL();
            model.ApplicationOperation = "MailBox_DeleteMail";
            string returnFileName = string.Empty;

            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];

                        var ext = Path.GetExtension(file.FileName);
                        string onlyFilename = file.FileName.Substring(0, file.FileName.LastIndexOf('.'));
                        var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
                        var newFileName = (dateTime + ext).ToString();
                        var comPath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() +
                                        WebConfigurationManager.AppSettings["EmailAttachmentPath"] + newFileName;

                        file.SaveAs(comPath);

                        if (string.IsNullOrEmpty(returnFileName))
                        {
                            returnFileName = newFileName;
                        }
                        else
                        {
                            returnFileName += "|" + newFileName;
                        }
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No attachment found..!";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong during file upload..!";
                return CD;
            }

            try
            {
                if (System.Web.HttpContext.Current.Request.Form.AllKeys.Length > 0)
                {
                    string xyz = HttpContext.Current.Request.Form["MailData"];
                    model = JsonConvert.DeserializeObject<MailBoxEntity>(xyz.Substring(1, xyz.Length - 2));
                    model.TotalFileName = returnFileName;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No key found..!";
                    return CD;
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                return CD;
            }

            if (model.MailId != 0 || string.IsNullOrEmpty(Convert.ToString(model.MailId))) // for draft mail to send
            {
                var mail = MBD.UserMailLogicalDelete(Convert.ToString(model.MailId), model.appAuditID, model.SessionID, model.UserID, model.ApplicationOperation);
            }

            model.ViewStatus = "read";
            model.Status = (int)MailStatus.Draft;

            #region Old Code

            //if (HttpContext.Current.Request.Files.AllKeys.Any())
            //{
            //    var logo = HttpContext.Current.Request.Files["img"];
            //    if (logo.ContentLength > 0)
            //    {
            //        var fileName = logo.FileName;

            //        string onlyFilename = fileName.Substring(0, fileName.LastIndexOf('.'));

            //        var ext = Path.GetExtension(logo.FileName);
            //        var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
            //        var newFileName = (dateTime + ext).ToString();
            //        var comPath = WebConfigurationManager.AppSettings["EmailAttachmentPath"].ToString() + newFileName;
            //        var imgSave = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + comPath;

            //        logo.SaveAs(imgSave);
            //        model.AttachmentPath = comPath;
            //    }
            //    model.HasAttachment = true;
            //}
            //else
            //{
            //    model.HasAttachment = false;
            //    model.AttachmentPath = string.Empty;
            //}

            #endregion

            if (string.IsNullOrEmpty(model.TotalFileName) || model.TotalFileName == "null")
            {
                model.HasAttachment = false;
                model.AttachmentPath = string.Empty;
            }
            else
            {
                model.HasAttachment = true;
                model.AttachmentPath = model.TotalFileName;
            }


            if (string.IsNullOrEmpty(model.ToList))
                model.ToList = string.Empty;

            if (string.IsNullOrEmpty(model.CC))
                model.CC = string.Empty;

            if (string.IsNullOrEmpty(model.BCC))
                model.BCC = string.Empty;

            try
            {
                model.ApplicationOperation = "DraftUserMail_SendMailFunctionality";
                var mailResult = (from row in MBD.SendNewMail(model)
                                  select new MailBoxEntity
                                  {
                                      MailId = Convert.ToInt32(row["MailId"]),
                                      Receiver = row["ReceiverId"].ToString(),
                                      ToList = Convert.ToString(row["Tolist"]),
                                      CC = Convert.ToString(row["CC"]),
                                      BCC = Convert.ToString(row["BCC"]),
                                      UserName = Convert.ToString(row["UserName"]),
                                      DeviceID = Convert.ToString(row["DeviceId"]),
                                      DeviceType = Convert.ToString(row["DeviceType"]),
                                      DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"])
                                  }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = mailResult;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DraftUserMailController");
            }
            return CD;
        }
    }
}
