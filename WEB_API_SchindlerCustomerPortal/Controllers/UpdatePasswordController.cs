﻿    using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class UpdatePasswordController : ApiController
    {

        [HttpPost]
        public object UpdatePassword([FromBody]UserMaster data)
        {
            UserMaster item = new UserMaster()
            {
                UserID = data.UserID,
                Password = data.Password,
                UpdatedBy = data.UpdatedBy,
                DeviceID = data.DeviceID
            };


            bool result = LoginModule.UpdatePassword(item.UserID, item.Password, item.UpdatedBy, item.DeviceID);

            if (result == true)
            {
                DataSet tempds = new DataSet();
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "UpdatePassword";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "1";
                dr["Message"] = "Your Password has been updated successfully!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdatePassword", "UpdatePassword - Success", data.UpdatedBy, data.DeviceID, "Update");

                return tempds;
            }

            else
            {
                DataSet tempds1 = new DataSet();
                DataTable t = new DataTable();
                tempds1.Tables.Add(t);
                tempds1.Tables[0].TableName = "UpdatePassword";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "No User Matched!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdatePassword", "UpdatePassword - Failed", data.UpdatedBy, data.DeviceID, "Update");

                return tempds1;


            }

        }
    }
}
