﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetDrawingAndDocumentController : ApiController
    {
        [HttpPost]
        public CommonData GetDrawingAndDocument(DataTableModel model)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;
            int totalcount = 0;

            try
            {
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                model.ApplicationOperation = "ProjectManagement_ProjectDocumentList".ToString() + "-" + model.DocumentType;
                if(string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "DocumentName";
                }

                ManageDocumentDAL MDD = new ManageDocumentDAL();
                if (String.IsNullOrEmpty(model.JobNo)) { model.JobNo = String.Empty; }

                if (!string.IsNullOrEmpty(model.DocumentType) && (model.DocumentType.ToUpper() == "Documents".ToUpper() || model.DocumentType.ToUpper() == "Drawing".ToUpper()))
                {
                    List<ProjectDocumentEntity> projectDocumentList = new List<ProjectDocumentEntity>();

                    projectDocumentList = (from row in MDD.GetProjectDocumentList(model, ref totalcount)
                           select new ProjectDocumentEntity
                           {
                               DocumentID = Convert.ToInt32(row["DocumentID"]),
                               DocumentName = row["DocumentName"].ToString(),
                               DocumentTypeID = Convert.ToInt32(row["DocumentTypeID"]),
                               Description = row["Description"].ToString(),
                               ExpiredDate = Convert.ToString(row["Expireddate"]).Replace(" 12:00:00 AM", ""),
                               StatusName = row["Published"].ToString(),
                               PublishedBy = row["PublishedBy"].ToString(),
                               Status = Convert.ToInt32(row["StatusId"]),
                               CompanyName = row["CompanyName"].ToString(),
                               ProjectName = row["ProjectName"].ToString(),
                               JobName = row["JobName"].ToString(),
                               IsPublished = Convert.ToBoolean(row["IsPublished"]),
                               DocumentPath = WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                              HttpUtility.UrlPathEncode(Convert.ToString(row["DocumentPath"].ToString())),
                               DocumentType = Convert.ToString(row["DocumentType"]),
                               CreatedDate = Convert.ToDateTime(row["CreatedDate"])
                           }).ToList();

                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = projectDocumentList;
                    CD.Count = totalcount;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "Value Missing...";
                    CD.Count = 0;
                    CD.Data = "";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Count = 0;
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDrawingAndDocumentController");
            }
            return CD;
        }
    }
}
