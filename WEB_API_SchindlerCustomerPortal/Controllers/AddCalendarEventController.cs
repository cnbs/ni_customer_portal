﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddCalendarEventController : ApiController
    {
        [HttpPost]
        public CommonData AddNewCalendarEvent(CalendarEventEntity model)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();
            CommonFunction commonFunc = new CommonFunction();
            int EventId = 0;

            model.ApplicationOperation = "AddCalendarEvent";

            try
            {
                if (string.IsNullOrEmpty(model.SendTo))
                {
                    model.SendTo = string.Empty;
                }
                if (string.IsNullOrEmpty(model.MessageBody))
                {
                    model.MessageBody = string.Empty;
                }
                if (string.IsNullOrEmpty(model.Location))
                {
                    model.Location = string.Empty;
                }
                if (string.IsNullOrEmpty(model.StartDate))
                {
                    model.StartDate = DateTime.Today.ToShortDateString().ToString();
                }
                if (string.IsNullOrEmpty(model.EndDate))
                {
                    model.EndDate = model.StartDate;
                }
                if (string.IsNullOrEmpty(model.AttachmentPath))
                {
                    model.AttachmentPath = string.Empty;
                }

                int result = calDal.AddEvent(model, ref EventId);

                if(result == 1 && (!string.IsNullOrEmpty(model.SendTo.Trim())))
                {
                    model.MailSenderID = model.CreatedBy;
                    model.EmailHeaderSubject = "myschindlerprojects - Calendar event notification";
                    model.EmailTemplateName = "CalendarNotification.html";
                    commonFunc.SendCalendarEmail(model);
                    #region EmailCommentCode
                    ///*
                    //EditUserDataController edituser = new EditUserDataController();
                    //UserDAL UD = new UserDAL();
                    //UserEntity senderData = edituser.GetUserById(Convert.ToInt32(model.CreatedBy));
                    //string senderName = senderData.FirstName + " [" + senderData.Email.ToString() +"]";

                    //foreach (var item in model.SendTo.Trim().Split(','))
                    //{
                    //    if (!string.IsNullOrEmpty(item))
                    //    {

                    //        UserEntity userResult = UD.Find(item.Trim());
                    //        StaticUtilities SU = new StaticUtilities();
                    //        string CalendarUrl = WebConfigurationManager.AppSettings["BaseURL"].ToString() + WebConfigurationManager.AppSettings["CalendarUrl"].ToString();

                    //        #region Email Send

                    //        string SFrom = WebConfigurationManager.AppSettings["AccountMail"].ToString();
                    //        string Subject = "myschindlerprojects - Calendar event notification";
                    //        string Sbody = null;

                    //        Sbody = "Subject: " + model.Subject;
                    //        Sbody += "<br/><br/>Start Date: " + model.StartDate + "<br/>End Date: " + model.EndDate;

                    //        Dictionary<string, string> dic = new Dictionary<string, string>();

                    //        dic.Add("PersonName", userResult.FirstName);
                    //        dic.Add("SenderName", senderName);
                    //        dic.Add("MessageURL", CalendarUrl);
                    //        dic.Add("MessageBody", Sbody);

                    //        Sbody = SU.getEmailBody("Calendar", "CalendarNotification.html", dic);

                    //        //SendEmail.SendMailtoMultiple(SFrom, item.Receiver, model.CC, model.BCC, Subject, Sbody);

                    //        SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item, Subject, Sbody);

                    //        #endregion 
                    //    }
                    //}
                    //*/ 
                    #endregion
                }

                CD.Status = result.ToString();
                CD.Message = (result == 1) ? "Event added successfully..." : (result == -1) ? "Email address does not exist in the system." : "Problem while adding an event..!";
                CD.Data = EventId.ToString();
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddNewCalendarEvent");
            }
            return CD;
        }
    }
}
