﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class AddContactProfilePicController : ApiController
    {
        [HttpPost]
        public object PostFile()
        {
            try
            {
                int file = 0;

                string ContactProfilePic = WebConfigurationManager.AppSettings["ContactProfilePic"].ToString();
                
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["ProfilePic"];

                    if (httpPostedFile != null)
                    {

                        var fileSavePath1 = Path.Combine(ContactProfilePic, httpPostedFile.FileName);
                        
                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath1);
                        file = 1;

                        if(httpPostedFile.FileName.StartsWith("BUSINESSCARD"))
                        {
                            Image image = Image.FromFile(fileSavePath1);
                            Image thumbnail = image.GetThumbnailImage(120, 120, null, IntPtr.Zero);
                            var thumbpath = Path.Combine(ContactProfilePic, "Thumb_" + httpPostedFile.FileName);
                            thumbnail.Save(thumbpath);
                            thumbnail.Dispose();
                            image.Dispose();
                        }
                    }
                }

                DataSet tempds = new DataSet();

                if (file == 1)
                {
                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "UploadPic";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "1";
                    dr["Message"] = "Photo Uploaded!";

                    t.Rows.Add(dr);

                    return tempds;
                }
                else
                {
                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "UploadPic";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "0";
                    dr["Message"] = "No Photo Uploaded!";

                    t.Rows.Add(dr);
                    //bool audit = APIAudit.APIAuditRecord("api/ChangeSetting", "ChangeSetting - No User/Device Matched", data.UserID, data.DeviceID, "Update");
                    return tempds;
                }

            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message);
                return false;
                //throw ex;

            }
        }

        
    }
}
