﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetProjectWiseGraphAndDocumentsController : ApiController
    {
        [HttpPost]
        public CommonData GetProjectWiseGraphAndDocuments(DataTableModel model)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = string.Empty;
                }
                DocumentData obj = new DocumentData();
                ProjectDAL PD = new ProjectDAL();

                if (model.ViewType.ToLower() == "payment" || model.ViewType.ToLower() == "contact")
                {
                    CD.Data = JsonConvert.DeserializeObject("[" + PD.GetProjectWiseGraphAndDocuments(model) + "]");
                    CD.Status = "1";
                    CD.Message = "success";
                    return CD;
                }

                var result = PD.GetProjectWiseGraphAndDocuments(model);

                if (result == null || result == "")
                {
                    CD.Data = "";
                    CD.Status = "0";
                    CD.Message = "No data found!";
                    return CD;
                }

                DocumentData a = new DocumentData();
                a = JsonConvert.DeserializeObject<DocumentData>(result);

                foreach (var item in a.Invoice.Count > 0 ? a.Invoice : a.Contract)
                {
                    item.ProjectNo = model.ProjectNo;
                    item.JobNo = "";
                    item.DocumentPath = (Convert.ToString(item.DocumentPath) == null) ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() + HttpUtility.UrlPathEncode(Convert.ToString(item.DocumentPath));
                    item.Extension = SetThumbnail(Path.GetExtension(item.DocumentPath));
                }

                CD.Data = a;
                CD.Status = "1";
                CD.Message = "Success";
            }
            catch (Exception ex)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = ex.Message.ToString();

                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectWiseGraphAndDocumentsController");
            }
            return CD;
        }

        public string SetThumbnail(string type)
        {
            string thumbnailPath = WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                   WebConfigurationManager.AppSettings["FiletreePath"].ToString();

            switch (type.ToLower())
            {
                case ".doc":
                case ".docx":
                    thumbnailPath += "/doc.png";
                    break;
                case ".pptx":
                case ".ppt":
                    thumbnailPath += "/ppt.png";
                    break;
                case ".xls":
                case ".xlsx":
                    thumbnailPath += "/xls.png";
                    break;
                case ".jpe":
                case ".jpeg":
                case ".jpg":
                case ".png":
                case ".gif":
                    thumbnailPath += "/psd.png";
                    break;
                case ".pdf":
                    thumbnailPath += "/pdf.png";
                    break;
                case ".txt":
                    thumbnailPath += "/txt.png";
                    break;
                case ".html":
                    thumbnailPath += "/html.png";
                    break;
                default:
                    thumbnailPath += "/file.png";
                    break;
            }
            return thumbnailPath;
        }
    }
}
