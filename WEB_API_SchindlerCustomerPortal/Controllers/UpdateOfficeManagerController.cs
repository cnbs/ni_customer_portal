﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;


namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateOfficeManagerController : ApiController
    {
        [HttpPost]
        public CommonData UpdateOfficeManager(OfficeManagerEntity model)
        {
            OfficeManagerDAL OMD = new OfficeManagerDAL();
            CommonData CD = new CommonData();


            model.UpdatedBy = model.UserID.ToString();
            model.ApplicationOperation = "EmployeeManagement_UpdateEmployee";

            if (string.IsNullOrEmpty(model.Street))
                model.Street = string.Empty;

            if (string.IsNullOrEmpty(model.State))
                model.State = string.Empty;

            if (string.IsNullOrEmpty(model.City))
                model.City = string.Empty;

            if (string.IsNullOrEmpty(model.Zip))
                model.Zip = string.Empty;

            if (string.IsNullOrEmpty(model.Phone))
                model.Phone = string.Empty;
            else
                model.Phone = model.Phone.Replace("-", "");


            try
            {
                bool officemanager = OMD.UpdateOfficeManager(model); // update user detail

                CD.Data = officemanager;
                CD.Status = (officemanager) ? "1" : "0";
                CD.Message = (officemanager) ? "Successfully data updated..." : "Record not found...!";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateOfficeManagerController");
            }
            return CD;
        }
    }
}
