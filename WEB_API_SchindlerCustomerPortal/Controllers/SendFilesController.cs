﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class SendFilesController : ApiController
    {

        [HttpPost]
        public object SendFiles([FromBody]EmailHistory data)
        {
            
            string Subject = "Information from OmniSalesHub!";
            string AssetPath = System.Configuration.ConfigurationManager.AppSettings["AssetsPath"].ToString();
            int Company = data.CompanyID;
            string UserID = data.UserID;

            try
            {
                EmailHistory item = new EmailHistory();

                item.EmailTo = data.EmailTo;
                item.EmailFrom = data.EmailFrom;
                item.EmailCC = string.IsNullOrEmpty(data.EmailCC) ? "" : data.EmailCC;
                item.RemindFollowUpDate = string.IsNullOrEmpty(data.RemindFollowUpDate) ? "" : data.RemindFollowUpDate;

                if (data.AttachmentFile == null || data.AttachmentFile == "")
                {
                    item.AttachmentFile = "";
                }
                else
                {
                    item.AttachmentFile = data.AttachmentFile;
                }

                if (data.Subject == "" || data.Subject == null)
                {
                    item.Subject = Subject;
                }
                else
                {
                    item.Subject = data.Subject;
                }

               

                if (data.EmailBody == "" || data.EmailBody == null)
                {
                    item.EmailBody = "Your document(s) are attached with this email!<br/><br/>";
                }
                else
                {
                    item.EmailBody = data.EmailBody;
                }

                
                DataSet tempds = new DataSet();

                AssetPath = AssetPath + "\\" + Company;

                bool update = LoginModule.SendMail(item.EmailFrom, item.EmailTo, item.EmailCC, item.Subject, item.EmailBody, item.AttachmentFile, AssetPath, item.RemindFollowUpDate, Company, UserID);

                if (update == true)
                {

                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "SendEmail";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "1";
                    dr["Message"] = "Email has been sent!";

                    t.Rows.Add(dr);
                    bool audit = APIAudit.APIAuditRecord("api/SendEmail", "SendEmail - Success", data.EmailTo, data.DeviceID, "SendEmail");

                    return tempds;
                }

                else
                {

                    DataTable t = new DataTable();
                    tempds.Tables.Add(t);
                    tempds.Tables[0].TableName = "SendEmail";

                    t.Columns.Add("Status");
                    t.Columns.Add("Message");

                    DataRow dr = t.NewRow();

                    dr["Status"] = "0";
                    dr["Message"] = "Email Sending Failed, Please try agian!";

                    t.Rows.Add(dr);

                    bool audit = APIAudit.APIAuditRecord("api/SendEmail", "SendEmail - Failed", data.EmailTo, data.DeviceID, "SendEmail");

                    return tempds;
                }


            }
            catch (Exception ex)
            {
                throw ex;

            }

        }

    }
}
