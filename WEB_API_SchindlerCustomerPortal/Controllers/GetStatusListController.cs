﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetStatusListController : ApiController
    {
        [HttpGet]
        public CommonData GetStatusList(int statusID = 0)
        {
            CommonData CD = new CommonData();
            ManageProjectDAL MPD = new ManageProjectDAL();
            List<StatusData> statusList = new List<StatusData>();
            List<StatusData> newstatusList = new List<StatusData>();

            try
            {
                statusList = (from row in MPD.GetStatusList("User")
                              select new StatusData
                              {
                                  Status = Convert.ToInt32(row["StatusId"]),
                                  StatusName = row["StatusName"].ToString(),
                              }).ToList();

                if (statusID != 0)
                {
                    for (int i = 0; i < statusList.Count; i++)
                    {
                        if ((statusList[i].Status == 4 || statusList[i].Status == 19) && statusID == 4)
                        {
                            newstatusList.Add(statusList[i]);
                        }
                        else if ((statusList[i].Status == 17 || statusList[i].Status == 18 || statusList[i].Status == 19) && statusID == 17)
                        {
                            newstatusList.Add(statusList[i]);
                        }
                    }
                }

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = statusID != 0 ? newstatusList : statusList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStatusListController");
            }
            return CD;
        }
    }
}
