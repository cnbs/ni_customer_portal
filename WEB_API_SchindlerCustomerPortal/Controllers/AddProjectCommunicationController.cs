﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddProjectCommunicationController : ApiController
    {
        [HttpPost]
        public CommonData AddProjectCommunication()
        {
            ManageProjectDAL MPD = new ManageProjectDAL();
            CommunicationEntity obj = new CommunicationEntity();
            CommonData CD = new CommonData();
            CommunicationEntity communication = new CommunicationEntity();
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string filename = string.Empty;
                    var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

                    var docpath = WebConfigurationManager.AppSettings["EmailAttachmentPath"].ToString();
                    var DocSavePath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["EmailAttachmentPath"].ToString();

                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["AttachmentFile"];

                    if (httpPostedFile != null)
                    {
                        //docpath = docpath + mainDocPath;
                        filename = DatewithSeconds + "_" + httpPostedFile.FileName;
                        var fileSavePath1 = Path.Combine(DocSavePath, filename);
                        httpPostedFile.SaveAs(fileSavePath1);
                    }
                    communication.AttachmentPath = filename;
                }

                if (HttpContext.Current.Request.Form.Count > 0)
                {
                    communication.ProjectNo = Convert.ToInt32(HttpContext.Current.Request.Form["ProjectNo"]);
                    communication.JobNo = Convert.ToString(HttpContext.Current.Request.Form["JobNo"]);
                    communication.UserId = Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]);
                    communication.Message = Convert.ToString(HttpContext.Current.Request.Form["Message"]);
                    communication.NotifyToEmail = Convert.ToString(HttpContext.Current.Request.Form["NotifyToEmail"]);
                    communication.CreatedBy = Convert.ToString(HttpContext.Current.Request.Form["CreatedBy"]);
                    communication.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
                    communication.SessionID = Convert.ToString(HttpContext.Current.Request.Form["SessionID"]);
                    communication.ApplicationOperation = "ProjectManagement_AddCommunication";
                    communication.CompanyId = Convert.ToInt32(HttpContext.Current.Request.Form["CompanyID"]); ;
                }
               
                string ActivationUrl = string.Empty;
                string ProjectNoProjectNameBankNoBankDesc = string.Empty;

                //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                string headurl = WebConfigurationManager.AppSettings["BaseURL"];

                if (string.IsNullOrEmpty(communication.JobNo) || communication.JobNo == "undefined")
                {
                    communication.JobNo = string.Empty;
                    communication.ProjectName = MPD.GetProjectandJobName(communication.ProjectNo.ToString(), communication.JobNo);

                    ActivationUrl = HttpUtility.UrlPathEncode("" + headurl + "/ManageProject/ProjectMasterView/" + communication.ProjectNo + "/Messages");
                    ProjectNoProjectNameBankNoBankDesc = "Project # " + communication.ProjectNo + " - " + communication.ProjectName;
                }
                else
                {
                    string tempData= MPD.GetProjectandJobName(communication.ProjectNo.ToString(), communication.JobNo);

                    communication.ProjectName = tempData.Split('|')[0].ToString().Trim();
                    communication.JobName = tempData.Split('|')[1].ToString().Trim();

                    ActivationUrl = HttpUtility.UrlPathEncode("" + headurl + "/ManageProject/ProjectJobMainView/" + communication.JobNo + "/" + communication.ProjectNo + "/" + communication.CompanyId + "/Messages");
                    ProjectNoProjectNameBankNoBankDesc = "Bank " + communication.JobName + " [" + communication.JobNo + "]" + " of Project # " + communication.ProjectNo + " - " + communication.ProjectName;

                }

                if (string.IsNullOrEmpty(communication.JobNo))
                {
                    communication.JobNo = "";
                }

                if (string.IsNullOrEmpty(communication.NotifyToEmail))
                {
                    communication.NotifyToEmail = "";
                }

                if (string.IsNullOrEmpty(communication.AttachmentPath))
                {
                    communication.AttachmentPath = "";
                }

                obj = (from row in MPD.AddProjectCommunication(communication)
                       select new CommunicationEntity
                       {
                           CommunicationId = Convert.ToInt32(row["CommunicationId"]),
                       }).FirstOrDefault();


                StaticUtilities SU = new StaticUtilities();

                if (string.IsNullOrEmpty(communication.NotifyToEmail))
                {
                    var userList = new List<UserEntity>();

                    userList = (from row in MPD.GetNotifyToMailID(communication.CompanyId, string.Empty)
                                select new UserEntity
                                {
                                    Email = Convert.ToString(row["Email"])
                                }).ToList();

                    for (int i = 0; i < userList.Count; i++)
                    {
                        string comma = ",";
                        communication.NotifyToEmail += Convert.ToString(userList[i].Email.Trim()) + ((userList.Count == i+1) ? "" : comma);
                    }
                }

                if (!string.IsNullOrEmpty(communication.NotifyToEmail))
                {
                    foreach (var item in communication.NotifyToEmail.Split(','))
                    {
                        UserDAL UD = new UserDAL();
                        UserEntity userResult = UD.Find(item.Trim());

                        #region Email Send

                        string SFrom = WebConfigurationManager.AppSettings["AccountMail"].ToString();
                        string Subject = "myschindlerprojects - notification";
                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", userResult.FirstName.Trim());
                        dic.Add("SenderName", communication.CreatedBy);
                        dic.Add("MessageURL", ActivationUrl);
                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);
                        

                        Sbody = SU.getEmailBody("User", "ProjectJobCommunication.html", dic);

                        //SendEmail.SendMailtoMultiple(SFrom, item.Receiver, model.CC, model.BCC, Subject, Sbody);

                        SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item, Subject, Sbody);

                        #endregion

                        #region Push Notification
                        string IsNotificationEnable = WebConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = WebConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(userResult.DeviceID) && !string.IsNullOrEmpty(userResult.DeviceType))
                            {
                                string NotificationSubject = "New Email Notification";
                                string NotificationBody = SU.getNotificationBody("User", "EmailNotificationNew.txt", dic);

                                try
                                {
                                    if (userResult.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(userResult.DeviceID, NotificationSubject, NotificationBody);
                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {
                                        Isnotify = SendNotification.NotifyIOSDevice(userResult.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }

                                //Insert into NotificationLogId table

                                data.CompanyID = communication.CompanyId;
                                data.DeviceID = userResult.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(userResult.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                data.CreatedBy = communication.CreatedBy;
                                data.DeviceType = userResult.DeviceType;
                                data.ModuleID = 28;
                                var Notify = UD.InsertNotification(data);

                            }
                        }

                        #endregion

                    }
                }
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = obj;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectCommunicationController");
            }
            return CD;
        }
    }
}
