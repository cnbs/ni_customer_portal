﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetEventsByUserIDController : ApiController
    {
        [HttpPost]
        public CommonData GetEventsByUserID(CalendarEventEntity model)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();
            DataTableModel dataModel = new DataTableModel();
            CommonFunction commonFunction = new CommonFunction();
            int countData = 0;

            model.ApplicationOperation = "AddCalendarEvent";
            var eventList = new List<CalendarEventEntity>();

            try
            {
                if (string.IsNullOrEmpty(model.StartDate))
                {
                    model.StartDate = commonFunction.setDateToshort(DateTime.Now.ToString());
                }

                eventList = (from row in calDal.SelectEventList(model,ref countData)
                             select new CalendarEventEntity()
                             {
                                 Id = Convert.ToInt32(row["CalTaskMstId"]),
                                 SendTo = Convert.ToString(row["EmailSentTo"]),
                                 Subject = Convert.ToString(row["EmailSubject"]),
                                 Location = Convert.ToString(row["Location"]),
                                 StartDate = commonFunction.setDateToshort(Convert.ToString(row["StartDt"])),
                                 EndDate = commonFunction.setDateToshort(Convert.ToString(row["EndDt"])),
                                 MessageBody = Convert.ToString(row["MessageBody"]),
                                 CreatedBy = Convert.ToString(row["CreatedBy"]),
                                 CategoryData = {
                                   CatID = Convert.ToInt32(row["CategoryId"]),
                                   CategoryName = Convert.ToString(row["CategoryName"]),
                                   ColorCode = Convert.ToString(row["ColorCode"])
                                 }
                             }).ToList();


                dataModel.UserID = model.Id;
                dataModel.SearchTerm = model.SearchTearm;

                GetJobMasterListingController getJobList = new GetJobMasterListingController();
                var jobMasterList = getJobList.GetJobMasterListing(dataModel);

                CD.Count = countData;
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = new { eventList = eventList, jobMasterList = jobMasterList };
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetEventsByUserID");
            }
            return CD;
        }
        
    }
}
