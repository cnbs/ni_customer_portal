﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class RemoveCalendarEventsController : ApiController
    {
        [HttpPost]
        public CommonData RemoveCalendarEvents(CalendarEventEntity model)
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();

            try
            {
                model.ApplicationOperation = "Delete_Event_Operation";
                int result = calDal.RemoveEvent(model);

                CD.Status = result.ToString();
                CD.Message = (result == 1) ? "Event data deleted successfully..." : "Delete opertion get failed ..!";
                CD.Data = "";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateCalendarEvent");
            }
            return CD;
        }
    }
}
