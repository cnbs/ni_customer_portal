﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class LogoutController : ApiController
    {

        [HttpPost]
        public object UserLogout([FromBody]UserDevice data)
        {
            UserDevice item = new UserDevice()
            {

                DeviceID = data.DeviceID,
                UserID = data.UserID,
                UpdatedBy = data.UpdatedBy
            };

            bool result = LoginModule.DeviceLogout(item.UserID, item.DeviceID, item.UpdatedBy);

            if (result == true)
            {
                DataSet tempds = new DataSet();
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "DeviceLogout";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "1";
                dr["Message"] = "You are successfully logout!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/Logout", "Logout - Success", data.UserID, data.DeviceID, "Update");

                return tempds;
            }

            else
            {
                DataSet tempds1 = new DataSet();
                DataTable t = new DataTable();
                tempds1.Tables.Add(t);
                tempds1.Tables[0].TableName = "DeviceLogout";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "Something Wrong Here!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/Logout", "Logout - Failed", data.UserID, data.DeviceID, "Update");

                return tempds1;


            }

        }

    }
}
