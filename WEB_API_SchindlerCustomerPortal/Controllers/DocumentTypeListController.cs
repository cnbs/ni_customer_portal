﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DocumentTypeListController : ApiController
    {
        [HttpPost]
        public CommonData DoctypeList(DocumentTypeEntity DTE)
        {
            CommonData CD = new CommonData();
            ManageDocumentDAL MDD = new ManageDocumentDAL();

            try
            {
                List<DocumentTypeEntity> doctype = (from row in MDD.GetDocTypeList(DTE)
                                                    select new DocumentTypeEntity
                                                    {
                                                        DocumentTypeID = Convert.ToInt32(row["Documenttypeid"]),
                                                        DocumentTypeName = row["documenttype"].ToString(),
                                                    }).ToList();
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = doctype;
            }
            catch (SqlException ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                CD.Data = ex.Message;
                string error = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DocumentTypeListController");
            }
            return CD;
        }
    }
}
