﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetCalendarCategoryListController : ApiController
    {
        [HttpGet]
        public CommonData GetCalendarCategoryList()
        {
            CommonData CD = new CommonData();
            CalendarDAL calDal = new CalendarDAL();
            var catList = new List<CalendarCategory>();

            try
            {
                catList = (from row in calDal.CategoryList()
                            select new CalendarCategory
                            {
                                CatID = Convert.ToInt32(row["CatMstId"]),
                                CategoryName = Convert.ToString(row["CatName"]),
                                ColorCode = Convert.ToString(row["ColorCode"])
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = catList;
            }
            catch(Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetCalendarCategoryList");
            }
            return CD;
        }
    }
}
