﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UserMailByMailIdController : ApiController
    {
        [HttpPost]
        public CommonData GetUserMailByMailId(MailBoxEntity model)
        {
            var userList = new MailBoxEntity();
            MailBoxDAL MBD = new DAL.MailBoxDAL();
            CommonData CD = new CommonData();
            CommonFunction CF = new CommonFunction();

            try
            {
                //model.ApplicationOperation = "MailBoxByNewMail";
                userList = (from row in MBD.GetUserMailByMailId(model.MailId, model.appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = Convert.ToString(row["SenderId"]),
                                Receiver = Convert.ToString(row["ReceiverId"]),
                                Subject = Convert.ToString(row["Subject"]),
                                Message = Convert.ToString(row["Message"]),
                                ViewStatus = Convert.ToString(row["View_Status"]),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                Status = Convert.ToInt32(row["Status"]),
                                DateSent = CF.setDateToshort(row["DateSent"].ToString()),
                                //SentDate = Convert.ToDateTime(row["DateSent"]),
                                IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                AttachmentPath = Convert.ToString(row["AttachmentPath"]),
                                AttachmentId = (string.IsNullOrEmpty(row["AttachmentId"].ToString())) ? 0 : Convert.ToInt32(row["Status"]),
                                ProfilePic = WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"] + Convert.ToString(row["ProfilePic"]),
                            }).FirstOrDefault();

                string[] data = userList.AttachmentPath.Split('|');
                //DirectoryInfo di = new DirectoryInfo("c:\\");
                string tempFileName = "";
                int j = 1;
                for (int i =0; i < data.Length; i++)
                {
                    var obj = new MailBoxAttachment();

                    tempFileName = WebConfigurationManager.AppSettings["BasicPath"].ToString() +
                                                            WebConfigurationManager.AppSettings["EmailAttachmentPath"] +
                                                            HttpUtility.UrlPathEncode(data[i]);

                    if (File.Exists(tempFileName))
                    {
                        obj.AttachmentPath = WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                                           WebConfigurationManager.AppSettings["EmailAttachmentPath"] +
                                                           HttpUtility.UrlPathEncode(data[i]);
                        obj.AttachmentId = j++;
                        obj.AttachmentName = data[i];
                        obj.Size = (Convert.ToDouble(new FileInfo(tempFileName).Length / 1000)).ToString() + " KB";
                        userList.Attachment.Add(obj);
                    }
                }

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UserMailByMailIdController");
            }
            return CD;
        }
    }
}
