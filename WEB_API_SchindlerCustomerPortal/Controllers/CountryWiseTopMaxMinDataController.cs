﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class CountryWiseTopMaxMinDataController : ApiController
    {
        [HttpPost]
        public dynamic GetCountryWiseTopMaxMinData(TopNData Data)
        {
            try
            {
                if (Data.Year[0] == null)
                {
                    string yearData = DateTime.Now.Year.ToString();
                    DataSet ds = new DataSet();

                    ds = DashboardDataModule.GetCountryWiseTopMaxMinDetail(yearData, Data.MinMax, Data.Count);

                    dynamic obj11 = new Dictionary<string, object>();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            dynamicProperties[ds.Tables[0].Columns[j].ColumnName.ToString()] = ds.Tables[0].Rows[i][j].ToString();
                        }
                        obj11.Add(i.ToString(), dynamicProperties);
                    }
                    return obj11;
                }
                else
                {
                    DataSet ds = new DataSet();

                    string yearData = string.Join(",", Data.Year);
                    ds = DashboardDataModule.GetCountryWiseTopMaxMinDetail(yearData, Data.MinMax, Data.Count);

                    dynamic obj11 = new Dictionary<string, object>();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            dynamicProperties[ds.Tables[0].Columns[j].ColumnName.ToString()] = ds.Tables[0].Rows[i][j].ToString();
                        }
                        obj11.Add(i.ToString(), dynamicProperties);
                    }
                    return obj11;
                }
            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message + " - GetCountryWiseTopMaxMinData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return null;
            }
        }
    }
}
