﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    //[RoutePrefix("api/Dashboard")]
    public class DashboardController : ApiController
    {
        [HttpPost]
        [Route("api/Dashboard/TotalPurchaseOrder")]
        //[ActionName("TotalPurchaseOrder")]
        public List<SalesDetails> TotalSalesOrder(SalesMaster data)
        {
            //HttpResponseMessage result = new HttpResponseMessage();
            DataSet ds = new DataSet();
            try
            {
                if (string.IsNullOrEmpty(data.StartDate))
                {
                    data.StartDate = DateTime.Today.Date.ToString();
                }

                if (string.IsNullOrEmpty(data.EndDate))
                {
                    DateTime d = Convert.ToDateTime(data.StartDate);
                    data.EndDate = d.AddMonths(-1).ToString();
                }

                ds = DashboardDataModule.GetTotalSalesOrder(data);
                List<SalesDetails> lstSalesDtl = new List<SalesDetails>();
                //SalesViewModel salesView = new SalesViewModel();
                if (data.OrderType.ToUpper() == "SO")
                {
                    lstSalesDtl = (from row in ds.Tables[0].AsEnumerable().ToList()
                                   select new SalesDetails
                                   {
                                       TotalSales = Convert.ToString(row["TOTAL_SALES"]),
                                       CurrentYear = Convert.ToString(row["CURRENT_YEAR"]),
                                       LastYear = Convert.ToString(row["LAST_YEAR"]),
                                       Percent = Convert.ToString(row["PERCENT"]),
                                       UpDown = Convert.ToString(row["UP_OR_DOWN"])
                                   }
                             ).ToList();
                }
                else if (data.OrderType.ToUpper() == "PO")
                {
                    lstSalesDtl = (from row in ds.Tables[0].AsEnumerable().ToList()
                                   select new SalesDetails
                                   {
                                       TotalOrder = Convert.ToString(row["TOTAL_ORDER"]),
                                       CurrentYear = Convert.ToString(row["CURRENT_YEAR"]),
                                       LastYear = Convert.ToString(row["LAST_YEAR"]),
                                       Percent = Convert.ToString(row["PERCENT"]),
                                       UpDown = Convert.ToString(row["UP_OR_DOWN"])
                                   }
                             ).ToList();
                }

                return lstSalesDtl;
                //HttpResponseMessage response = client.PostAsync(API_URL, new StringContent(lstSalesDtl, Encoding.UTF8, "application/json")).Result;
            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message + " - GetTotalSales" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return null;
            }
        }


        ////[ActionName("GetCountryWiseTotalSales")]
        //[Route("api/Dashboard/GetCountryWiseTotalSales/{year}")]
        //[HttpPost]
        //public string GetCountryWiseTotalSales(string year)
        //{
            
        //    //year[0] = "2017";
        //    StringBuilder JsonData = new StringBuilder();

        //    if (year == null)
        //    {
        //        DataSet ds = new DataSet();
        //        year = DateTime.Now.Year.ToString();
        //        ds = DashboardDataModule.GetCountryWiseTotalSalesDetail(year);

        //        List<CountryWiseTotalSales> obj = new List<CountryWiseTotalSales>();

        //        List<string> yearName = new List<string>();
        //        for (int i = 1; i < ds.Tables[0].Columns.Count; i++)
        //        {
        //            yearName.Add(ds.Tables[0].Columns[i].ColumnName.ToString());
        //        }

        //        JsonData.Append("[");
        //        for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            JsonData.Append("{ ");
        //            //CountryWiseTotalSales newobj = new CountryWiseTotalSales();
        //            // newobj.Country = ds.Tables[0].Rows[i][0].ToString();
        //            //JsonData.Append(ds.Tables[0].Rows[i][0].ToString());

        //            //List<string> yearValue = new List<string>();

        //            for (int j = 1; j < ds.Tables[0].Columns.Count; j++)
        //            {
        //                JsonData.Append("," + ds.Tables[0].Columns[i].ColumnName.ToString() + " : ");
        //                JsonData.Append(ds.Tables[0].Rows[i][j].ToString());
        //                //yearName.Add(ds.Tables[0].Columns[i].ColumnName.ToString());
        //                //yearValue.Add(ds.Tables[0].Rows[i][j].ToString());
        //            }
        //            JsonData.Append("}");
        //            //newobj.Value.AddRange(yearValue);
        //        }
        //        JsonData.Append("]");
        //        //List<CountryWiseTotalSales> lstYearWiseList = new List<DataRow>();

        //        //lstYearWiseList = (from row in ds.Tables[0].AsEnumerable().ToList()
        //        //                   select new CountryWiseTotalSales
        //        //                   {

        //        //                       Country = Convert.ToString(row["COUNTRY"]),
        //        //                       YearData[1] = Convert.ToString(row["COUNTRY"])

        //        //                   }
        //        //                  ).ToList();

        //    }
        //    else
        //    {

        //        DataSet ds = new DataSet();
        //        ////year[0] = DateTime.Now.Year.ToString();
        //        string yearData = string.Join(",", year);
        //        ds = DashboardDataModule.GetCountryWiseTotalSalesDetail(yearData);

        //        //List<CountryWiseTotalSales> obj = new List<CountryWiseTotalSales>();

        //        //List<string> yearName = new List<string>();
        //        //for (int i = 1; i < ds.Tables[0].Columns.Count; i++)
        //        //{
        //        //    yearName.Add(ds.Tables[0].Columns[i].ColumnName.ToString());
        //        //}

        //        //JsonData.Append("[");
        //        //for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
        //        //{
        //        //    JsonData.Append("{ ");
        //        //    //CountryWiseTotalSales newobj = new CountryWiseTotalSales();
        //        //    // newobj.Country = ds.Tables[0].Rows[i][0].ToString();
        //        //   // JsonData.Append(ds.Tables[0].Rows[i][0].ToString());

        //        //    //List<string> yearValue = new List<string>();

        //        //    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
        //        //    {
        //        //        JsonData.Append($@"""{ds.Tables[0].Columns[j].ColumnName.ToString() }""" + " : ");
        //        //        if((j + 1) == ds.Tables[0].Columns.Count)
        //        //            JsonData.Append($@"""{ds.Tables[0].Rows[i][j].ToString()}""");
        //        //        else
        //        //            JsonData.Append($@"""{ ds.Tables[0].Rows[i][j].ToString()}""" + ", ");

        //        //        //yearName.Add(ds.Tables[0].Columns[i].ColumnName.ToString());
        //        //        //yearValue.Add(ds.Tables[0].Rows[i][j].ToString());
        //        //    }
        //        //    JsonData.Append("}");
        //        //    //newobj.Value.AddRange(yearValue);
        //        //}
        //        //JsonData.Append("]");


        //        //string str = JsonData.Replace("", "");
        //        List<CountryWiseTotalSales> lstYearWiseList = new List<CountryWiseTotalSales>();

        //        lstYearWiseList = (from row in ds.Tables[0].AsEnumerable().ToList()
        //                           select new CountryWiseTotalSales
        //                           {
        //                               Country = Convert.ToString(row["COUNTRY"]),
        //                               Key = Convert.ToString(row[1]),
        //                               Value = Convert.ToString(row[2])
        //                           }
        //                          ).ToList();
        //    }

        //    var data = JsonData.ToString();
        //    data = data.Replace("\\", "");
        //    return data;
        //}

        //[HttpGet]
        //public string Hello()
        //{
        //    return "Hello";
        //}

    }
}
