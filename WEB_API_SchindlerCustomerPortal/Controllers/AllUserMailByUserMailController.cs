﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AllUserMailByUserMailController : ApiController
    {
        [HttpPost]
        public CommonData GetAllMailByUser(DataTableModel model)
        {
            List<MailBoxEntity> userList = new List<MailBoxEntity>();
            CommonData CD = new CommonData();

            int count = 0;
            model.StatusValue = 0;

            MailStatus choice;
            if (Enum.TryParse(model.Status.ToString(), out choice))
            {
                model.StatusValue = (int)choice;
            }

            try
            {
                MailBoxDAL MBD = new MailBoxDAL();
                model.ApplicationOperation = "MailBox_List";

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "DateSent";
                }

                if (string.IsNullOrEmpty(model.SortDirection))
                {
                    model.SortDirection = "Desc";
                }

                userList = (from row in MBD.GetUserMailByEmail(model, ref count)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                Message = row["Message"].ToString(),
                                ViewStatus = row["View_Status"].ToString(),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                Status = Convert.ToInt32(row["Status"]),
                                DateSent = row["DateSent"].ToString(),
                                IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                //Attachment = 
                                
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                //Designation = row["DesignationName"].ToString(),
                                //DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
                CD.Count = count;
            }
            catch (Exception ex)
            {
                CD.Status = "0";  
                CD.Message = "Something went wrong...!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AllUserMailByUserMailController");
            }
            return CD;
        }
    }
}
