﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ReceivedMDTCostDataController : ApiController
    {
        [HttpPost]
        public CommonData GetReceivedMDTCostData()
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;
            try
            {
                DataSet ds = new DataSet();
                ds = DashboardDataModule.GetReceivedMDTCostGroupDetail();

                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<ReceivedMDTCostData> RMCD = new List<ReceivedMDTCostData>();

                    RMCD = (from row in ds.Tables[0].AsEnumerable().ToList()
                            select new ReceivedMDTCostData
                            {
                                Receipts = Convert.ToString(row[0]),
                                ReceivedMTDPOValue = Convert.ToString(row[1]),
                                ReceivedMTDStandard = Convert.ToString(row[2]),
                                PPV = Convert.ToString(row[3]),
                                MTDVoucheredIPV = Convert.ToString(row[4]),
                                MTDCalculated = Convert.ToString(row[5]),
                                Difference = Convert.ToString(row[6])
                            }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = RMCD;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetReceivedMDTCostData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
