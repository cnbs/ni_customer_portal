﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DashboardProjectListingMilestoneDataController : ApiController
    {
      
        public CommonData DashboardProjectListingMilestone(DataTableModel model)
        {

            CommonData CD = new CommonData();
            string msg = string.Empty;
            int count = 0, CompanyId = -1,TotalProject = 0 ;
            string Email = "Email";
            try
            {
              
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = string.Empty;
                }

                List<ProjectMilestoneStatusEntity> projectListResult = new List<ProjectMilestoneStatusEntity>();

                ProjectDAL obj = new ProjectDAL();


                projectListResult = (from row in obj.DashboardProjectListingMilestone(model, ref count, ref Email,ref TotalProject,ref CompanyId)
                                     select new ProjectMilestoneStatusEntity
                                     {
                                         ProjectCompanyId = Convert.ToInt32(row["ProjectCompanyId"]),
                                         ProjectNo = Convert.ToString(row["ProjectNo"]),
                                         JobNo = Convert.ToString(row["JobNo"]),
                                         ProjectName = Convert.ToString(row["ProjectName"]),
                                         ContractExecuted = Convert.ToString(row["Contract Executed"]),
                                         DownPayment = Convert.ToString(row["Down Payment Rec'd"]),
                                         DrawingsApproved = Convert.ToString(row["Drawings Approved"]),
                                         FinishesComplete = Convert.ToString(row["Finishes Complete"]),
                                         MaterialInStorageHub = Convert.ToString(row["Material in Storage Hub"]),
                                         ReadyToPull = Convert.ToString(row["Job-Site Ready"]),
                                         InstallationStart = Convert.ToString(row["Installation Start"]),
                                         EquipmentTurnover = Convert.ToString(row["Equipment Turnover"]),
                                         //MaterialOrdered = Convert.ToString(row["Material Ordered"]),
                                         Title = Convert.ToString(row["Title"]),
                                         DataType = Convert.ToString(row["DataType"]),
                                         Billed= Convert.ToString(row["BilledPercent"]),
                                         Paid= Convert.ToString(row["PaidPercent"]),
                                         //added by Arpit Patel
                                         ContractExecutedDate = todate(Convert.ToString(row["Contract Executed Date"])),
                                         DownPaymentDate = todate(Convert.ToString(row["Down Payment Rec'd Date"])),
                                         DrawingsApprovedDate = todate(Convert.ToString(row["Drawings Approved Date"])),
                                         FinishesCompleteDate = todate(Convert.ToString(row["Finishes Complete Date"])),
                                         MaterialInStorageHubDate = todate(Convert.ToString(row["Material in Storage Hub Date"])),
                                         JobNoDate = todate(Convert.ToString(row["Job-Site Ready Date"])),
                                         InstallationStartDate = todate(Convert.ToString(row["Installation Start Date"])),
                                         EquipmentTurnoverDate = todate(Convert.ToString(row["Equipment Turnover Date"]))


                                     }).ToList();

                MailBoxDAL MBD = new MailBoxDAL();


                List<ProjectMilestoneStatusEntity> onlyProjectRecords = projectListResult.Where(x => x.DataType == "Project Data").ToList();

                foreach (var item in onlyProjectRecords)
                {
                    var onlyProjectRelatedBanksRecords = projectListResult.Where(x => x.DataType == "Job Data" && x.ProjectNo == item.ProjectNo).ToList();

                    item.BankList.AddRange(onlyProjectRelatedBanksRecords);
                }

                UserDAL UD = new UserDAL();


                CD.UserCount = Convert.ToInt32(UD.NewlyRegisteredUserCount(CompanyId));
                CD.MailCount = (!string.IsNullOrEmpty(Email)) ? Convert.ToInt32( MBD.GetUserMailCount(Email)[0][0]) : 0;
                CD.Status = (projectListResult.Count != 0) ? "1":"0";
                CD.Message = (projectListResult.Count != 0) ? ReadErrorFromXMLFile.ReadMessage("Success"): "No assigned project found.";
                CD.Data = onlyProjectRecords;
                CD.Count = count;
                CD.TotalProjects = TotalProject;
                
            }
            catch (Exception ex)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = ex.Message.ToString();

                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DashboardProjectListingMilestoneDataController");
            }
            return CD;
        }

        public string todate(string str)
        {
            if (str != "")
            {
                var parts = str.Split('-');
                return (parts[1] + '-' + parts[2].Substring(0,2) + '-' + parts[0]).ToString();
            }
            return str;
        }
    }
}
