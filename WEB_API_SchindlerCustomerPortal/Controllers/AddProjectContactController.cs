﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddProjectContactController : ApiController
    {
        public CommonData AddProjectContact(ProjectContactEntity model)
        {
            CommonData CD = new CommonData();

            try
            {
                ProjectContactDAL PCD = new ProjectContactDAL();

                if (string.IsNullOrEmpty(model.JobNo))
                {
                    model.JobNo = string.Empty;
                }

                if (string.IsNullOrEmpty(model.ContactNumber))
                    model.ContactNumber = string.Empty;
                else
                    model.ContactNumber = model.ContactNumber.Replace("-", "");

                #region "Audit Trail"
                //int appAuditID = 0;

                //model.appAuditID = appAuditID;
                model.ApplicationOperation = "ProjectManagement_AddProjectContact";

                #endregion

                var result = PCD.AddProjectContact(model);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = "Data Successfully Added...";

                //string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
                //string SuperAdmin = System.Configuration.ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();

                #region Email Send

                //var userResult = _userService.Find(model.Email);

                //string Subject = "Schindler - Please verfiy account";

                //string token = (Guid.NewGuid().ToString("N"));

                //int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                //_userService.SetTokenService(token, userResult.UserID, ExpToken);

                //string ActivationUrl = string.Empty;

                //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                //ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                //string Sbody = null;

                //Sbody += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi," + model.FirstName.Trim() + "," + "<br/><br/>";
                //Sbody += "You have successfully registered on Schindler Customer Portal<br/><br/>";
                //Sbody += "Please  <a href=" + ActivationUrl + ">" + ActivationUrl + "</a>  to verify your email<br/><br/>";
                //Sbody += "Sincerely,<br/>The Schindler Support Team</p>";
                //Sbody += "</div></body></html>";

                //SendEmail.Send(SFrom, model.Email, Subject, Sbody);

                #endregion

                //return user;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                CD.Data = "Something went wrong...";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectContactController");
            }
            return CD;
        }
    }
}
