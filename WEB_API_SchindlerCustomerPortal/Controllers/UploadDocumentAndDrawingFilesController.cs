﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UploadDocumentAndDrawingFilesController : ApiController
    {
        [HttpPost]
        public CommonData UploadFiles()
        {
            CommonData CD = new CommonData();

            var docpath = WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString();
            
            string filename = string.Empty;
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["AttachmentFile"];

                if (httpPostedFile != null)
                {
                    // Get the complete file path
                    filename = DatewithSeconds + "_" + httpPostedFile.FileName;

                    var fileSavePath1 = Path.Combine(docpath, filename);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath1);

                    CD.Status = "1";
                    CD.Message = "Data Uploaded successfully...";
                    CD.Data = filename;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "Fail to uploaded data...";
                }
            }
            else
            {
                CD.Status = "0";
                CD.Message = "Fail to uploaded data...";
            }
            return CD;
        }
    }
}
