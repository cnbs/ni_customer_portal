﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ProjectListandViewController : ApiController
    {
        [HttpPost]
        public CommonData ProjectListandView(DataTableModel model)
        {
            ManageProjectDAL MPD = new ManageProjectDAL();
            CommonData CD = new CommonData();
            try
            {
                int totalcount = 0;
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "ProjectName";
                }

                if (string.IsNullOrEmpty(model.SortDirection))
                {
                    model.SortColumn = "asc";
                }

                #region View&List
                model.ApplicationOperation = "ProjectManagement_MasterList";

                List<ProjectMasterEntity> projectList = new List<ProjectMasterEntity>();

                try
                {
                    projectList = (from row in MPD.GetAllProjectList(model, ref totalcount)
                                   select new ProjectMasterEntity
                                   {
                                       ProjectID = Convert.ToInt32(row["ProjectID"]),
                                       ProjectNo = Convert.ToString(row["ProjectNo"]),
                                       ProjectName = Convert.ToString(row["ProjectName"]),
                                       Banks = Convert.ToInt32(row["Banks"]),
                                       CompanyId = Convert.ToInt32(row["CompanyId"]),
                                       CompanyName = Convert.ToString(row["CompanyName"]),
                                       ProjectMgrPhone = Convert.ToString(row["ProjectMgrPhone"]),
                                       OriginalNODDate = Convert.ToString(row["OriginalNODDate"]).Replace(" 12:00:00 AM", ""),
                                       NOD = Convert.ToString(row["NOD"]).Replace(" 12:00:00 AM", ""),
                                       NODConfirm = "false",
                                       //This will be change based on data(Now its false only)
                                       //NODConfirm = Convert.ToString(row["NODConfirm"]),
                                       IsContractSigned = (Convert.ToString(row["IsContractSigned"]).ToUpper() == "YES") ? "true" : "false",
                                       ContractorSuper = Convert.ToString(row["ContractorSuper"]),
                                       SchindlerSuperintendent = Convert.ToString(row["SchindlerSuperintendent"]),
                                       SchindlerSalesExecutive = Convert.ToString(row["SchindlerSalesExecutive"]),
                                       TeamAssigned = Convert.ToString(row["TeamAssigned"]),
                                       Job_Address = Convert.ToString(row["Job_Address"]),
                                       Job_City = Convert.ToString(row["Job_City"]),
                                       Job_State = Convert.ToString(row["Job_State"]),
                                       Job_Zip = Convert.ToString(row["Job_Zip"]),
                                       FactoryMaterialEstimate = Convert.ToDouble(row["FactoryMaterialEstimate"]),
                                       FactoryMaterialActual = Convert.ToDouble(row["FactoryMaterialActual"]),
                                       ContractorProjMgr = Convert.ToString(row["ContractorProjMgr"]),
                                       ContractReceivedDate = Convert.ToString(row["ContractReceivedDate"]).Replace(" 12:00:00 AM", ""),
                                       AwardDate = Convert.ToString(row["AwardDate"]).Replace(" 12:00:00 AM", ""),
                                       CustomerNo = Convert.ToInt64(row["CustomerNo"]),
                                       NODConfirmDate = Convert.ToString(row["NODConfirmDate"]).Replace(" 12:00:00 AM", ""),
                                       UnitCount = Convert.ToInt32(row["UnitCount"]),
                                       BookingComplDate = Convert.ToString(row["BookingComplDate"]).Replace(" 12:00:00 AM", ""),
                                       Proj_Mgr_Email = Convert.ToString(row["Proj_Mgr_Email"]),
                                       Status = Convert.ToInt32(row["Status"]),
                                       StatusName = Convert.ToString(row["StatusName"]),
                                       InceptionDate = Convert.ToString(row["InceptionDate"]).Replace(" 12:00:00 AM", ""),
                                       TurnOverDate = Convert.ToString(row["TurnOverDate"]).Replace(" 12:00:00 AM", ""),
                                       AlternateProjectNo = Convert.ToString(row["AlternateProjectNo"]),
                                       ContractorContactPerson = Convert.ToString(row["ContractorContactPerson"]),
                                       SchindlerProjMgr = Convert.ToString(row["SchindlerProjMgr"]),
                                       ParentProjectId = Convert.ToInt32(row["ParentProjectId"]),

                                   }).ToList();

                    CD.StatusList = (from row in MPD.GetStatusList("Project")
                                                 select new StatusData
                                                 {
                                                     Status = Convert.ToInt32(row["StatusId"]),
                                                     StatusName = row["StatusName"].ToString(),
                                                 }).ToList();
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = projectList;
                    CD.Count = totalcount;
                }
                catch (Exception ex)
                {
                    CD.Status = "0";
                    CD.Message = "Fail";
                    CD.Data = "";
                    CD.Count = 0;
                    string error = ex.Message;
                }
                #endregion
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectListandViewController");
            }
            return CD;
        }
    }
}
