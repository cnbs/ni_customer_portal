﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetDesignationListController : ApiController
    {
        [HttpGet]
        public CommonData designationList()
        {
            CommonData CD = new CommonData();

            try
            {
                ProjectContactDAL PCD = new ProjectContactDAL();
                List<DesignationEntity> designation = (from row in PCD.GetDesignationList()
                                                       select new DesignationEntity
                                                       {
                                                           DesignationId = Convert.ToInt32(row["DesignationId"]),
                                                           Designation = row["Designation"].ToString(),
                                                       }).ToList();
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = designation;
                
            }
            catch (SqlException ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                string error = ex.Message;
                CD.Data = error;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDesignationListController");
            }
            return CD;
        }
    }
}
