﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AllUserByCompanyIdController : ApiController
    {
        [HttpPost]
        public CommonData GetAllUserByCompanyId(DataTableModel model)
        {
            var userList = new List<UserEntity>();
            MailBoxDAL MBD = new MailBoxDAL();
            CommonData CD = new CommonData();

            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                model.SearchTerm = string.Empty;
            }

            try
            {
                userList = (from row in MBD.GetUserByCompanyIdForMailBox(model.CompanyID, model.SearchTerm)
                            select new UserEntity
                            {
                                Email = row["Email"].ToString(),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = row["FirstName"].ToString(),
                                LastName = row["LastName"].ToString(),
                                Designation = row["DesignationName"].ToString(),
                                DesignationId = Convert.ToInt32(row["DesignationId"])
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AllUserByCompanyIdController");
            }
            return CD;
        }
    }
}
