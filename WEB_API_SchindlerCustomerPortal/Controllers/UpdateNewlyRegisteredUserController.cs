﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateNewlyRegisteredUserController : ApiController
    {
        [HttpPost]
        public CommonData UpdateNewlyRegisteredUser(UserEntity model)
        {
            UserDAL UD = new UserDAL();
            CommonData CD = new CommonData();

            model.ApplicationOperation = "UserManagement_UpdateUser";
            model.UpdatedBy = model.UserID.ToString();

            var companyNameCheck = UD.CheckCompanyforEditNewRegisterUser(model.CompanyName);

            if (!companyNameCheck && model.AccountStatus != 19)
            {
                var returnResult = "Company not exist";
                CD.Status = "0";
                CD.Message = returnResult;
                CD.Data = "";
                return CD;
            }
            else
            {
                model.LastName = string.Empty;

                if (string.IsNullOrEmpty(model.Address1))
                    model.Address1 = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.PostalCode))
                    model.PostalCode = string.Empty;

                if (string.IsNullOrEmpty(model.ContactNumber))
                    model.ContactNumber = string.Empty;
                else
                    model.ContactNumber = model.ContactNumber.Replace("-", "");


                if(model.AccountStatus == 19)
                {
                    if (string.IsNullOrEmpty(model.FirstName))
                        model.FirstName = string.Empty;

                    if (string.IsNullOrEmpty(model.LastName))
                        model.LastName = string.Empty;

                    if (string.IsNullOrEmpty(model.CompanyName))
                        model.CompanyName = string.Empty;

                    if (string.IsNullOrEmpty(model.Designation))
                        model.Designation = string.Empty;
                }

                string token = (Guid.NewGuid().ToString("N"));
                model.Token = token;

                int ExpToken = Convert.ToInt32(WebConfigurationManager.AppSettings["TokenExpireHours"]);
                SetTokenService(token, model.UserID, ExpToken);

                var user = UD.NewlyRegisteredUserUpdate(model);

                if (model.AccountStatus == 18)
                {
                    #region Email

                    string From = WebConfigurationManager.AppSettings["AccountMail"].ToString();

                    string Subject = "myschindlerprojects – your account has been approved";

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = WebConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = HttpUtility.UrlPathEncode("" + headurl + "/Activate/ActiveAccount?TokenID=" + model.Token + "");

                        #region Email Send

                            StaticUtilities SU = new StaticUtilities();

                            string Sbody = null;

                            Dictionary<string, string> dic = new Dictionary<string, string>();

                            dic.Add("PersonName", model.FirstName);
                            dic.Add("MessageURL", ActivationUrl);

                            Sbody = SU.getEmailBody("User", "ActivateAccountNew.html", dic);
                            bool result = SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"], model.Email, Subject, Sbody);


                        #endregion

                    #endregion
                }

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = user;
                return CD;
            }
        }

        public void SetTokenService(string Token, int UserID, int ExpireToken)
        {
            UserDAL UD = new UserDAL();
            try
            {
                bool CheckTokenSet = UD.SetTokenOnEmail(Token, UserID, ExpireToken);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateNewlyRegisteredUserController -- SetTokenService");
            }
        }
    }
}
