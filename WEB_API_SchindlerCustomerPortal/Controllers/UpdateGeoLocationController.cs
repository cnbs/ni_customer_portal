﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Configuration;
using System.Web.Http;
using System.Xml.Linq;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class UpdateGeoLocationController : ApiController
    {
        CommonData CD = new CommonData();
        string secureKey = WebConfigurationManager.AppSettings["GoogleLocationKey"].ToString();
        GeoLocationDAL GLD = new GeoLocationDAL();
        List<ProjectGeoLocationEntity> ProjectLatLong = new List<ProjectGeoLocationEntity>();
        List<UserEntity> UserLatLong = new List<UserEntity>();

        [HttpGet]
        public CommonData UpdateGeoLocation()
        {
            try
            {
                ProjectLatLong = (from row in GLD.GetProjectMasterDetail()
                                  select new ProjectGeoLocationEntity
                                  {
                                      ProjectID = Convert.ToInt32(row["ProjectId"]),
                                      ProjectNo = Convert.ToString(row["ProjectNo"]),
                                      Job_Address = Convert.ToString(row["Job_Address"]),
                                      Job_City = Convert.ToString(row["Job_City"]),
                                      Job_State = Convert.ToString(row["Job_StateName"]),
                                      Job_Zip = Convert.ToString(row["Job_Zip"])
                                  }).ToList();

                CD = GenerateLatLong();

                ProjectLatLong = null;
                UserLatLong = (from row in GLD.GetUserMasterDetail()
                                  select new UserEntity
                                  {
                                      UserID = Convert.ToInt32(row["UserID"]),
                                      City = Convert.ToString(row["City"]),
                                      State = Convert.ToString(row["StateName"]),
                                      PostalCode = Convert.ToString(row["PostalCode"])
                                  }).ToList();

                CD = GenerateLatLong();
                

            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message;
            }
            return CD;
        }

        private CommonData GenerateLatLong()
        {
            bool flag = (ProjectLatLong == null) ? true : false;
            int cnt = flag ? UserLatLong.Count : ProjectLatLong.Count;

            for (int i = 0; i < cnt; i++)
            {
                try
                {
                    var address = "";
                    if (!flag)
                    {
                        address = ProjectLatLong[i].Job_Address + " " + ProjectLatLong[i].Job_City
                                           + " " + ProjectLatLong[i].Job_State + " " + ProjectLatLong[i].Job_Zip;
                    }
                    else
                    {
                        address = Convert.ToString(UserLatLong[i].City) + " " + Convert.ToString(UserLatLong[i].State) + " " + Convert.ToString(UserLatLong[i].PostalCode);
                    }
                    var requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address=" + Uri.EscapeDataString(address) + "&key=" + secureKey);

                    var request = WebRequest.Create(requestUri);
                    var response = request.GetResponse();
                    var xdoc = XDocument.Load(response.GetResponseStream());

                    if (xdoc.Element("GeocodeResponse").Element("status").Value == "OK")
                    {
                        var result = xdoc.Element("GeocodeResponse").Element("result");
                        var locationElement = result.Element("geometry").Element("location");

                        if (!flag)
                        {
                            ProjectLatLong[i].Latitude = Convert.ToDouble(locationElement.Element("lat").Value);
                            ProjectLatLong[i].Longitude = Convert.ToDouble(locationElement.Element("lng").Value); 
                        }
                        else
                        {
                            UserLatLong[i].Latitude = Convert.ToDouble(locationElement.Element("lat").Value);
                            UserLatLong[i].Longitude = Convert.ToDouble(locationElement.Element("lng").Value);
                        }
                    }
                    else if (xdoc.Element("GeocodeResponse").Element("status").Value == "OVER_QUERY_LIMIT")
                    {
                        CD.Status = "0";
                        CD.Message = "You have exceeded your daily request quota for this API.";
                        return CD;
                    }
                }
                catch (Exception ex)
                {
                    --i;
                }
            }

            bool latlong = (flag) ? GLD.SetUserAddressLatLong(UserLatLong) : GLD.SetLatLong(ProjectLatLong);

            CD.Status = "1";
            CD.Message = "Successfully Data update...";
            CD.Data = (latlong) ? "latitude/longitude values updated successfully..." : "latitude/longitude values not updated...!";

            return CD;
        }
    }
}