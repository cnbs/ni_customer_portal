﻿using System;
using System.Data;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class CountryWiseTotalSalesDataController : ApiController
    {
        [HttpPost]
        public CommonData GetCountryWiseTotalSalesData(SalesBreakdown TData)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                string yearData = string.Empty;
                string TerritoryData = string.Empty;
                DataSet ds = new DataSet();

                TData.Territory = TData.Territory[0].Split(',');

                foreach (string item in TData.Territory)
                {
                    TerritoryData += "'" + item.Trim() + "',";
                }
                TerritoryData = TerritoryData.Substring(0, TerritoryData.Length - 1);

                if (TData.Year[0] == null) {
                    yearData = DateTime.Now.Year.ToString();
                } else {
                    yearData = string.Join(",", TData.Year);
                }

                ds = DashboardDataModule.GetCountryWiseSalesDetail(yearData, TerritoryData);
                CD = GetDataFromDS.GetCommonData(ds);
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetCountryWiseSalesData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
