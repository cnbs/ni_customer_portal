﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class MultipleFileUploadController : ApiController
    {
        [HttpPost]
        public CommonData MultipleFileUpload()
        {
            string returnFileName = string.Empty;
            CommonData CD = new CommonData();
            // Checking no of files injected in Request object  

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];

                        var ext = Path.GetExtension(file.FileName);
                        string onlyFilename = file.FileName.Substring(0, file.FileName.LastIndexOf('.'));
                        var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
                        var newFileName = (dateTime + ext).ToString();
                        var comPath =   WebConfigurationManager.AppSettings["documentStartPath"].ToString() +
                                        WebConfigurationManager.AppSettings["EmailAttachmentPath"] + newFileName;

                        file.SaveAs(comPath);

                        if (string.IsNullOrEmpty(returnFileName))
                        {
                            returnFileName = newFileName;
                        }
                        else
                        {
                            returnFileName += "|" + newFileName;
                        }
                    }
                    // Returns message that successfully uploaded  
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = returnFileName;
                }
                catch (Exception ex)
                {
                    CD.Status = "0";
                    CD.Message = "Something went wrong..!";
                    CD.Data = ex.Message.ToString();
                    ErrorLogClass lm = new ErrorLogClass();
                    lm.CreateLogFiles();
                    var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                    lm.ErrorLog(Errorlogpath, ex.Message + " - MultipleFileUploadController");
                }
            }
            else
            {
                CD.Status = "0";
                CD.Message = "No attachment found..!";
            }
            return CD;
        }
    }
}
