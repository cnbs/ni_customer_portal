﻿using Newtonsoft.Json;
using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class EmployeeManagementController : ApiController
    {
        [HttpPost]
        public CommonData UpdateUser()
        {
            CommonData CD = new CommonData();
            UserDAL UD = new UserDAL();
            UserEntity model = new UserEntity();
            StaticUtilities SU = new StaticUtilities();

            //int appAuditID = 0;
            string CurrentDateTime = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            model.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
            model.DeviceID = Convert.ToString(HttpContext.Current.Request.Form["DeviceID"]);
            model.SessionID = Convert.ToString(HttpContext.Current.Request.Form["SessionID"]);
            model.UpdatedBy = Convert.ToString(HttpContext.Current.Request.Form["UpdatedBy"]);
            model.UserID = Convert.ToInt32(HttpContext.Current.Request.Form["UserID"]);

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    var imgpath = WebConfigurationManager.AppSettings["userProfilePicPath"].ToString();
                    var imgSave = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString();

                    var fileName = CurrentDateTime + "_" + (logo.FileName);

                    imgSave = imgSave + fileName;

                    logo.SaveAs(imgSave);
                    model.ProfilePic = fileName;

                    // Start : Thumbnail code added by Vivek
                    Image image = Image.FromFile(imgSave);
                    Size thumbnailSize = SU.GetThumbnailSize(image);
                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);
                    var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath), "Thumb_" + fileName);
                    // Save thumbnail.
                    thumbnail.Save(thumbpath);
                    // End : Thumbnail code added by Vivek
                }
                else
                {
                    model.ProfilePic = "-1";
                }
            }
            else
            {
                model.ProfilePic = "-1";
            }

            model.LastName = string.Empty;

            if (string.IsNullOrEmpty(model.Address1))
                model.Address1 = string.Empty;

            if (string.IsNullOrEmpty(model.State))
                model.State = string.Empty;

            if (string.IsNullOrEmpty(model.City))
                model.City = string.Empty;

            if (string.IsNullOrEmpty(model.PostalCode))
                model.PostalCode = string.Empty;

            string token = (Guid.NewGuid().ToString("N"));
            model.Token = token;

            try
            {
                int superAdminCompanyId = 0;

                var getUserCompanyName = GetUserById(model.UserID);

                if (getUserCompanyName.CompanyID == 0)
                {
                    model.ApplicationOperation = "EmployeeManagement_UpdateEmployee";
                }
                else
                {
                    model.ApplicationOperation = "UserManagement_UpdateUser";
                }

                bool result = UD.UpdateUser(model);

                if (model.AccountStatus == 18)
                {
                    if (getUserCompanyName.AccountStatus != 18)
                    {
                        int ExpToken = Convert.ToInt32(WebConfigurationManager.AppSettings["TokenExpireHours"]);
                        UD.SetTokenOnEmail(token, model.UserID, ExpToken);

                        #region Email Send

                        //string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                        //string Subject = "myschindlerprojects – your account has been approved";

                        //string ActivationUrl = string.Empty;

                        ////string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        //string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        //ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/ActiveAccount?TokenID=" + model.Token + "");

                        //string Sbody = null;

                        //Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", model.FirstName);
                        ////dic.Add("UserName", model.FirstName);

                        //dic.Add("MessageURL", ActivationUrl);

                        //Sbody = StaticUtilities.getEmailBody("User", "ActivateAccountNew.html", dic);

                        //SendEmail.Send(CommonConfiguration.NoReplyEmail, model.Email, Subject, Sbody);


                        #endregion

                    }
                }

                // permission update
                List<PermissionList> lstPermissionList = new List<PermissionList>();

                var permissionList = System.Web.HttpContext.Current.Request.Form["permissionList"];
                if (permissionList != null)
                {
                    lstPermissionList = JsonConvert.DeserializeObject<List<PermissionList>>(permissionList);

                    UserPermissionModuleModel obj = new UserPermissionModuleModel();
                   
                    obj.appAuditID = model.appAuditID;
                    obj.ApplicationOperation = "EmployeeUserModulePermission";
                    obj.SessionID = model.SessionID;
                    obj.CreatedBy = model.UserID.ToString();
                    obj.UpdatedBy = model.UserID.ToString();

                    foreach (var item in lstPermissionList)
                    {
                        obj.UserID = item.UserID;
                        obj.ModuleId = item.ModuleId;
                        obj.ColumnValue = item.HasPermission;

                        var ColumnName = "AllColumn"; // set all column for permission

                        obj.ColumnName = ColumnName;

                        var resultData = UD.InsertUpdateUserModuleAccessPermissionByModule(obj);
                    }
                }


                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = result;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
            }
            return CD;
        }

        public UserEntity GetUserById(int userId)
        {
            UserEntity user = new UserEntity();
            UserDAL UD = new UserDAL();

            user = (from row in UD.CompanyUserSelectById(userId)
                    select new UserEntity
                    {
                        Email = row["Email"].ToString(),
                        RoleID = Convert.ToInt32(row["RoleID"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        //LastName = row["LastName"].ToString(),
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        ContactNumber = string.IsNullOrEmpty(row["ContactNumber"].ToString()) ? "" : row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                        Designation = row["DesignationName"].ToString(),
                        DesignationId = Convert.ToInt32(row["DesignationId"]),
                        Address1 = Convert.ToString(row["Address1"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        PostalCode = Convert.ToString(row["PostalCode"]),
                        AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                        Requested_Projects = Convert.ToString(row["Requested_Projects"])
                        //CreatedBy = row["CreatedBy"].ToString(),
                        //UpdatedBy = row["UpdatedBy"].ToString(),

                    }).FirstOrDefault();
            return user;
        }
    }
}