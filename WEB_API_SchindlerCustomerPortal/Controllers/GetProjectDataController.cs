﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetProjectDataController : ApiController
    {
        [HttpPost]
        public CommonData GetProjectKeyValueData(DataTableModel model)
        {
            List<ProjectAssignEntity> jobList = new List<ProjectAssignEntity>();
            CommonData CD = new CommonData();
            try
            {
                if(string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }
                ProjectContactDAL PCD = new ProjectContactDAL();
                var ad = PCD.GetProjectAndBankListByCompanyIdandSearchTerm(model.CompanyID, model.SearchTerm, model.RoleID, model.UserID, model.ProjectNo);
            
                jobList = (from row in PCD.GetProjectAndBankListByCompanyIdandSearchTerm(model.CompanyID, model.SearchTerm, model.RoleID, model.UserID,model.ProjectNo)
                           select new ProjectAssignEntity
                           {
                               ProjectNo = Convert.ToString(row["ProjectNo"]),
                               ProjectName = Convert.ToString(row["ProjectName"]),
                               JobNo = (model.ProjectNo == 0) ? null : Convert.ToString(row["JobNo"]),
                               JobName = (model.ProjectNo == 0) ? null : Convert.ToString(row["JobName"])
                           }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = jobList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = null;
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectDataController");
            }
            return CD;
        }
    }
}
