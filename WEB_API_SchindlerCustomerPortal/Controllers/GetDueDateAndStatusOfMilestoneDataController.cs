﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetDueDateAndStatusOfMilestoneDataController : ApiController
    {
        [HttpPost]
        public CommonData GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model)
        {
            List<ProjectStatusDueDateEntity> projectListResult = new List<ProjectStatusDueDateEntity>();
            CommonData CD = new CommonData();
            ProjectDAL obj = new ProjectDAL();
            CommonFunction CF = new CommonFunction();
            ManageProjectDAL MPD = new ManageProjectDAL();

            try
            {
                projectListResult = (from row in obj.GetDueDateAndStatusOfMilestone(model)
                                     select new ProjectStatusDueDateEntity
                                     {
                                         ProjectNo = Convert.ToString(row["ProjectNo"]),
                                         JobNo = Convert.ToString(row["JobNo"]),
                                         StatusName = Convert.ToString(row["StatusName"]),
                                         CompletedDate = CF.setDateToshort(Convert.ToString(row["CompletedDate"])),
                                         DueDate = CF.setDateToshort(Convert.ToString(row["DueDate"])),
                                         Milestone = Convert.ToString(row["Milestone"]),
                                         Milestoneid = Convert.ToInt32(row["Milestoneid"]),
                                         StatusId = Convert.ToInt32(row["StatusId"]),
                                         Comment = Convert.ToString(row["Note"])
                                     }).ToList();

                CD.StatusList = (from row in MPD.GetStatusList("Milestone")
                                 select new StatusData
                                 {
                                     Status = Convert.ToInt32(row["StatusId"]),
                                     StatusName = row["StatusName"].ToString(),
                                 }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = projectListResult;
            }
            catch (Exception ex)
            {
                CD.Status = "1";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDueDateAndStatusOfMilestoneDataController");
            }
            return CD;
        }
    }
}
