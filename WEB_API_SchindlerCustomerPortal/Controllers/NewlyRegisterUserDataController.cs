﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class NewlyRegisterUserDataController : ApiController
    {
        [HttpPost]
        public CommonData NewlyRegisterUserCount(DataTableModel model)
        {
            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                model.SearchTerm = string.Empty;
            }

            if(string.IsNullOrEmpty(model.SortColumn))
            {
                model.SortColumn = "UserName";
            }

            if (string.IsNullOrEmpty(model.SortDirection))
            {
                model.SortDirection = "asc";
            }

            int count = 0;
            var permission = new UserPermissionViewModel();
            int user = 0;
            UserDAL UD = new UserDAL();
            CommonData CD = new CommonData();
            List<UserEntity> userList = new List<UserEntity>();

            string modelName = "User Management";

            permission = (from row in UD.CheckModulePermission(model.UserID, modelName)
                          select new UserPermissionViewModel
                          {
                              UserID = Convert.ToInt32(row["UserID"]),
                              ModuleId = Convert.ToInt32(row["ModuleId"]),
                              ModuleURL = (row["URL"]).ToString(),
                              HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                              HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                              HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                              HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                              HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                              ModulePermissionName = (row["ModulePermissionName"]).ToString(),
                              ModuleDisplayName = (row["DisplayName"]).ToString(),
                              SequenceNO = Convert.ToInt32(row["SequenceNO"]),
                              Restricted = Convert.ToInt32(row["Restricted"]),
                              VisibleOnMenu = Convert.ToInt32(row["VisibleOnMenu"]),
                          }).FirstOrDefault();

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            try
            {
                model.ApplicationOperation = "UserManagement_ListUser";

                if (model.RoleID != 1)
                {
                    // user don't have permission to access User management module
                    if (!permission.HasViewPermission)
                    {
                        CD.Status = "0";
                        CD.Count = user;
                        CD.Message = "You don't have permission to access User management module.";
                        return CD;
                    }
                    else
                    {
                        try
                        {
                            userList = (from row in UD.NewlyRegisteredUser(model, ref count)
                                        select new UserEntity
                                        {
                                            Email = row["Email"].ToString(),
                                            RoleID = Convert.ToInt32(row["RoleID"]),
                                            UserID = Convert.ToInt32(row["UserID"]),
                                            FirstName = row["UserName"].ToString(),
                                            CompanyName = Convert.ToString(row["CompanyName"]),
                                            ContactNumber = string.IsNullOrEmpty(row["ContactNumber"].ToString()) ? "" : row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                            //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                            ProfilePic = WebConfigurationManager.AppSettings["userProfilePicPath"] + HttpUtility.UrlPathEncode(row["ProfilePic"].ToString()),
                                            IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                            AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                            StatusName = Convert.ToString(row["StatusName"]),
                                            Designation = row["DesignationName"].ToString(),
                                            DesignationId = Convert.ToInt32(row["DesignationId"]),
                                            RegisteredDate = Convert.ToString(row["RegisteredDate"]),
                                            Requested_Projects = Convert.ToString(row["Requested_Projects"]),
                                            Address1="",
                                            Address2 = "",
                                            City ="",
                                            PostalCode="",
                                            State = "",

                                        }).ToList();

                            CD.Status = "1";
                            CD.Count = count;
                            CD.Message = "Success";
                            CD.Data = userList;
                        }
                        catch (Exception ex)
                        {
                            CD.Status = "0";
                            CD.Message = "Something went wrong...";
                            CD.Data = ex.Message;
                        }
                    }
                }
                else
                {
                    userList = (from row in UD.NewlyRegisteredUser(model, ref count)
                                select new UserEntity
                                {
                                    Email = row["Email"].ToString(),
                                    RoleID = Convert.ToInt32(row["RoleID"]),
                                    UserID = Convert.ToInt32(row["UserID"]),
                                    FirstName = row["UserName"].ToString(),
                                    CompanyName = Convert.ToString(row["CompanyName"]),
                                    //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                    ContactNumber = string.IsNullOrEmpty(row["ContactNumber"].ToString()) ? "" : row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                    ProfilePic = WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() + HttpUtility.UrlPathEncode(row["ProfilePic"].ToString()),
                                    IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                    AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                    StatusName = Convert.ToString(row["StatusName"]),
                                    Designation = row["DesignationName"].ToString(),
                                    DesignationId = Convert.ToInt32(row["DesignationId"]),
                                    RegisteredDate = Convert.ToString(row["RegisteredDate"]),
                                    Requested_Projects = Convert.ToString(row["Requested_Projects"])
                                }).ToList();

                    CD.Status = "1";
                    CD.Count = count;
                    CD.Message = "Success";
                    CD.Data = userList;
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisterUserDataController");
            }
            return CD;
        }
    }
}
