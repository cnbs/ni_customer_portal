﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class GetContactProfilePicController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage GetContactProfilePic([FromBody]CampaignContact data)
        {
            try
            {
                string ProfilePic = data.ContactProfilePic;

                string file = "";

                string ContactProfilePic = System.Web.Configuration.WebConfigurationManager.AppSettings["ContactProfilePic"].ToString();

                if (ProfilePic != "")
                {
                    file = ContactProfilePic + "\\" + ProfilePic;
                }

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(file, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                return result;


            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return result;

            }
        }

    }
}
