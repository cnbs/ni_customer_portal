﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class JobPivotTableController : ApiController
    {
        [HttpPost]
        public CommonData JobPivotTable(DataTableModel model)
        {
            CommonData CD= new CommonData();
            ProjectDAL PD = new ProjectDAL();

            try
            {
                model.ApplicationOperation = "GetJobPivotTable";

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }
                CD = GetDataFromDS.GetCommonData(PD.GetJobPivotTable(model));
                CD.Count = model.Count;
                CD.ProjectCompanyID = model.ProjectCompanyID.ToString();
                
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobPivotTable");
            }
            return CD;
        }
    }
}
