﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class CompanyUserAutoCompleteController : ApiController
    {
        [HttpPost]
        public CommonData CompanyUserAutoComplete(DataTableModel model)
        {
            CommonData CD = new CommonData();
            List<UserEntity> userList = new List<UserEntity>();
            MailBoxDAL MBD = new MailBoxDAL();

            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                CD.Status = "0";
                CD.Message = "Email value can't be empty..!";
                return CD;
            }

            if (model.SearchTerm.Contains(","))
            {
                string result = model.SearchTerm.Split(',')[model.SearchTerm.Split(',').Length - 1];
                if (!string.IsNullOrEmpty(result))
                {
                    model.SearchTerm = result.Trim();
                }
            }

            try
            {
                userList = (from row in MBD.GetUserByCompanyIdForMailBox(model.CompanyID, model.SearchTerm)
                            select new UserEntity
                            {
                                Email = row["Email"].ToString(),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = row["FirstName"].ToString(),
                                LastName = row["LastName"].ToString(),
                                Designation = row["DesignationName"].ToString(),
                                DesignationId = Convert.ToInt32(row["DesignationId"])
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyUserAutoCompleteController");
            }
            return CD;
        }
    }
}
