﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetAllContactListByCompanyController : ApiController
    {
        [HttpPost]
        public CommonData GetAllContactListByCompanyId(DataTableModel model)
        {
            if (string.IsNullOrEmpty(Convert.ToString(model.ProjectNo)))
            {
                model.ProjectNo = 0;
            }

            if (string.IsNullOrEmpty(Convert.ToString(model.SearchTerm)))
            {
                model.SearchTerm = "";
            }

            if (string.IsNullOrEmpty(model.SortColumn))
            {
                model.SortColumn = "UserName";
            }

            if (string.IsNullOrEmpty(model.SortDirection))
            {
                model.SortColumn = "asc";
            }

            CommonData CD = new CommonData();
            ProjectContactDAL PCD = new ProjectContactDAL();
            int count = 0;
            var userList = new List<ProjectContactEntity>();
            try
            {
                userList = (from row in PCD.ProjectContactListByCompanyId(model, ref count)
                            select new ProjectContactEntity
                            {
                                ContactID = Convert.ToInt32(row["ContactId"]),
                                ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                                ProjectName = Convert.ToString(row["ProjectName"]),
                                FirstName = Convert.ToString(row["UserName"]),
                                //LastName = row["LastName"].ToString(),
                                DesignationId = Convert.ToInt32(row["Designation"]),
                                Designation = Convert.ToString(row["DesignationName"]),
                                Email = Convert.ToString(row["Email"]),
                                ContactNumber = string.IsNullOrEmpty(row["PhoneNo"].ToString()) ? "" : row["PhoneNo"].ToString().Insert(3, "-").Insert(7, "-"),
                                //Address = row["JobStreetAddress"].ToString(),
                                //City = row["City"].ToString(),
                                //State = row["State"].ToString(),
                                //PostalCode = row["Zip"].ToString(),
                                //ContactNumber = Convert.ToString(row["PhoneNo"]),
                                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                JobName = Convert.ToString(row["JobName"]),
                                JobNo = Convert.ToString(row["JobNo"]),
                                BankDesc = Convert.ToString(row["BankDesc"]),
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
                CD.Count = count;
               // return userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllContactListByCompanyId");
            }
            return CD;
        }

    }
}
