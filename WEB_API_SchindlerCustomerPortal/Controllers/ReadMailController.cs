﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ReadMailController : ApiController
    {
        [HttpGet]
        public CommonData ReadMail(MailBoxEntity model)
        {
            int appAuditID = 0;
            CommonData CD = new CommonData();
            MailBoxDAL MBD = new MailBoxDAL();
            model.ApplicationOperation = "MailBox_ReadMail";

            if (!string.IsNullOrEmpty(model.StatusValue))
            {
                if (model.StatusValue != "Draft" && model.StatusValue != "Sent" && model.StatusValue != "Delete")
                {
                    model.ViewStatus = "read";
                    model.Status = (int)MailStatus.Inbox;

                    var updateStatus = MBD.UpdateStatusOfUserMail(Convert.ToString(model.MailId), model.ViewStatus, model.Status, appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID);

                    CD.Status = "1";
                    CD.Message = "Success";
                }
            }

            var mailData = (from row in MBD.GetUserMailByMailId(model.MailId, model.appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                Message = row["Message"].ToString(),
                                ViewStatus = row["View_Status"].ToString(),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                Status = Convert.ToInt32(row["Status"]),
                                DateSent = row["DateSent"].ToString(),
                                IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                AttachmentPath = Convert.ToString(row["AttachmentPath"]),
                                ProfilePic = WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"] + Convert.ToString(row["ProfilePic"]),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                //Designation = row["DesignationName"].ToString(),
                                //DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).FirstOrDefault();


            if (mailData.Status != 10 && mailData.Status != 11 && mailData.Status != 12)
            {
                if (mailData.Receiver.Trim() == model.Sender.Trim())
                {
                    if (!string.IsNullOrEmpty(model.ReadmailId))
                    {
                        string viewStatus = "read";
                        model.Status = (int)MailStatus.Inbox;

                        var updateStatus = MBD.UpdateStatusOfUserMail(Convert.ToString(model.MailId), viewStatus, model.Status, appAuditID, model.ApplicationOperation, model.DeviceID, model.UserID);

                        CD.Status = "1";
                        CD.Message = "Success";
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "Please login first...";
                    return CD;
                }
            }
            CD.Data = mailData;
            return CD;
        }
    }
}
