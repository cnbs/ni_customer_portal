﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class TotalSalesOrderController : ApiController
    {
        [HttpPost]
        public CommonData TotalSalesOrder(SalesMaster data)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            DataSet ds = new DataSet();
            try
            {
                if (string.IsNullOrEmpty(data.StartDate))
                {
                    data.StartDate = DateTime.Today.Date.ToString();
                }

                if (string.IsNullOrEmpty(data.EndDate))
                {
                    DateTime d = Convert.ToDateTime(data.StartDate);
                    data.EndDate = d.AddMonths(-1).ToString();
                }

                ds = DashboardDataModule.GetTotalSalesOrder(data);
                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<SalesDetails> lstSalesDtl = new List<SalesDetails>();
                    //SalesViewModel salesView = new SalesViewModel();
                    if (data.OrderType.ToUpper() == "SO")
                    {
                        lstSalesDtl = (from row in ds.Tables[0].AsEnumerable().ToList()
                                       select new SalesDetails
                                       {
                                           TotalSales = Convert.ToString(row["TOTAL_SALES"]),
                                           TotalOrder = "",
                                           CurrentYear = Convert.ToString(row["CURRENT_YEAR"]),
                                           CurrentYearFormated = Convert.ToString(row["CURRENT_YEAR_FORMATED"]),
                                           LastYear = Convert.ToString(row["LAST_YEAR"]),
                                           Percent = Convert.ToString(row["PERCENT"]),
                                           UpDown = Convert.ToString(row["UP_OR_DOWN"])
                                       }
                                      ).ToList();
                    }
                    else if (data.OrderType.ToUpper() == "PO")
                    {
                        lstSalesDtl = (from row in ds.Tables[0].AsEnumerable().ToList()
                                       select new SalesDetails
                                       {
                                           TotalSales = "",
                                           TotalOrder = Convert.ToString(row["TOTAL_ORDER"]),
                                           CurrentYear = Convert.ToString(row["CURRENT_YEAR"]),
                                           CurrentYearFormated = "",
                                           LastYear = Convert.ToString(row["LAST_YEAR"]),
                                           Percent = Convert.ToString(row["PERCENT"]),
                                           UpDown = Convert.ToString(row["UP_OR_DOWN"])
                                       }
                                 ).ToList();
                    }

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = lstSalesDtl;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;
                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetTotalSales" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
