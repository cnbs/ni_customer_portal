﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class OrderVsSalesDataController : ApiController
    {
        [HttpPost]
        public CommonData GetOrderVsSalesData(YearData yearList)
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                string yearData = string.Empty;
                DataSet ds = new DataSet();

                if (yearList.Year[0] == null)
                {
                    yearData = DateTime.Now.Year.ToString();
                }
                else
                {
                    yearData = string.Join(",", yearList.Year);
                }

                ds = DashboardDataModule.GetOrderVsSalesDataDetail(yearData);
                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<OrderVsSales> ordersalesData = new List<OrderVsSales>();

                    ordersalesData = (from row in ds.Tables[0].AsEnumerable().ToList()
                                      select new OrderVsSales
                                      {
                                          OrderMonth = Convert.ToString(row["ORD_MONTH"]),
                                          OrderMonthYear = Convert.ToString(row["ORD_MONTH_YEAR"]),
                                          OrderYear = Convert.ToString(row["ORD_YEAR"]),
                                          SumOfTotalOrder = Convert.ToString(row["SUM_OF_TOTAL_ORDERS"]),
                                          SumOfSales = Convert.ToString(row["SUM_OF_SALES"]),
                                          AverageSales = Convert.ToString(row["AVG_SALES"])
                                      }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = ordersalesData;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetOrderVsSalesData" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
