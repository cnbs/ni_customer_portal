﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetAnnouncementController : ApiController
    {
        [HttpGet]
        public CommonData GetAnnouncement(string DisplayOn)
        {

            CommonData CD = new CommonData();
            CommonFunction CF = new CommonFunction();
            try
            {
                var announcementList = new List<AnnouncementEntity>();
                AnnouncementDAL AD = new AnnouncementDAL();

                announcementList = (from row in AD.AllAnnouncementSelect(DisplayOn)
                            select new AnnouncementEntity
                            {
                                AnnMsgId = Convert.ToInt32(row["AnnMsgId"]),
                                MessageText = Convert.ToString(row["MessageText"]),
                                Frequency = Convert.ToString(row["Frequency"]),
                                DisplayOn = Convert.ToString(row["DisplayOn"]),
                                ValidDate = CF.setDateToshort(Convert.ToString(row["ValidDate"]))
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = announcementList;

            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Somthing went wrong....!";
                CD.Data = ex.Message.ToString();
            }
            return CD;
        }
    }
}
