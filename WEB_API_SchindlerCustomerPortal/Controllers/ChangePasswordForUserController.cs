﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;
using WEB_API_SchindlerCustomerPortal.Services;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ChangePasswordForUserController : ApiController
    {
        [HttpPost]
        public CommonData changePasswordForUser(ChangePassword user)
        {
            CommonData CD = new CommonData();
            UserDAL UD = new UserDAL();

            try
            {
                user.ApplicationOperation = "ChangePassword";
                user.UpdatedBy = user.UserID.ToString();

                string saltKey = EncryptionService.CreateSaltKey(5);
                user.PasswordSalt = saltKey;
                user.NewPassword = EncryptionService.CreatePasswordHash(user.NewPassword, saltKey, "");

                var userresult = UD.Find(user.Email.ToString());
                user.OldPassword = EncryptionService.CreatePasswordHash(user.OldPassword, userresult.PasswordSalt, "");

                var result = UD.UpdateUserPassword(user);
                
                CD.Status = (result == true) ? "1" : "0";
                CD.Message = (result == true) ? "Successfully password changed..." : "Old password does not match...";
                CD.Data = "";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ChangePasswordForUserController");
            }
            return CD;
        }
    }
}
