﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetOfficeManagerListController : ApiController
    {
        [HttpPost]
        public CommonData SelectOfficeManager(DataTableModel model)
        {
            int count = 0;
            CommonData CD = new CommonData();
            try
            {
                model.ApplicationOperation = "OfficeManager_List";
                model.ModuleName = "Office Manager";

                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "FullName";
                }

                if (string.IsNullOrEmpty(model.SortDirection))
                {
                    model.SortDirection = "asc";
                }

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                var userList = new List<OfficeManagerEntity>();
                OfficeManagerDAL OMD = new OfficeManagerDAL();

                userList = (from row in OMD.OfficeManagerSelect(model, ref count)
                            select new OfficeManagerEntity
                            {
                                FullName = Convert.ToString(row["FullName"]),
                                Email = Convert.ToString(row["Email"]),
                                OfficeManagerID = Convert.ToInt32(row["OfficeManagerID"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                LastName = Convert.ToString(row["LastName"]),
                                CompanyID = Convert.ToInt32(row["CompanyID"]),
                                Phone = (string.IsNullOrEmpty(row["Phone"].ToString())) ? "" : Convert.ToString(row["Phone"]).Insert(3, "-").Insert(7, "-"),
                                //Phone = row["Phone"].ToString(),
                                SapID = Convert.ToString(row["SapID"]),
                                Office_Desc = Convert.ToString(row["Office_Desc"]),
                                Title = Convert.ToString(row["Title"]),
                                Street = Convert.ToString(row["Street"]),
                                City = Convert.ToString(row["City"]),
                                State = Convert.ToString(row["State"]),
                                Zip = Convert.ToString(row["Zip"]),
                                OfficeID = Convert.ToString(row["OfficeID"]),
                                StateName = Convert.ToString(row["StateName"])
                            }).ToList();


                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //count = user.Count;
                }
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
                CD.Count = count;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetOfficeManagerListController");
            }
            return CD;
        }
    }
}
