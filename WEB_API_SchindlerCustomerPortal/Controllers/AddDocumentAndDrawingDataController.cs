﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class AddDocumentAndDrawingDataController : ApiController
    {
        [HttpPost]
        //public CommonData AddProjectDocument([FromBody]ProjectDocumentEntity projectDocument)
        public CommonData AddProjectDocument()
        {
            ProjectDocumentEntity projectDocument = new ProjectDocumentEntity();
            CommonData CD = new CommonData();

            //var docpath = WebConfigurationManager.AppSettings["DomainPath"].ToString();
            var docpath = WebConfigurationManager.AppSettings["documentPath"].ToString();
            var DocSavePath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["documentPath"].ToString();

            string filename = string.Empty;
            //string mainDocPath = WebConfigurationManager.AppSettings["mainDocPath"].ToString();
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            try
            {
                //if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                if (true)
                {
                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["AttachmentFile"];

                    if (httpPostedFile != null)
                    {
                        //docpath = docpath + mainDocPath;
                        filename = DatewithSeconds + "_" + httpPostedFile.FileName;
                        var fileSavePath1 = Path.Combine(DocSavePath, filename);
                        projectDocument.DocumentPath = fileSavePath1;
                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath1);
                    }
                    else
                    {
                        CD.Status = "0";
                        CD.Message = "Please attach document...";
                        CD.Data = "";
                        return CD;
                        //filename = "2017-28-3--10-20-07_moon.png";
                        //var fileSavePath1 = "D:\\Projects\\Customer Portal\\SourceCode\\SchindlerCustomerPortal\\SchindlerCustomerPortal\\images\\ProfilePics\\2017-28-3--10-20-07_moon.png";
                        //projectDocument.DocumentPath = fileSavePath1;
                    }
                    projectDocument.DocumentName = Convert.ToString(HttpContext.Current.Request.Form["DocumentName"]);
                    projectDocument.DocumentTypeID = Convert.ToInt32(HttpContext.Current.Request.Form["DocumentTypeID"]);
                    projectDocument.Description = Convert.ToString(HttpContext.Current.Request.Form["Description"]);
                    projectDocument.ExpiredDate = Convert.ToString(HttpContext.Current.Request.Form["ExpiredDate"]);
                    //projectDocument.IsDeleted = Convert.ToBoolean(HttpContext.Current.Request.Form["IsDeleted"]);
                    projectDocument.IsDeleted = false;
                    projectDocument.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
                    projectDocument.DeviceID = Convert.ToString(HttpContext.Current.Request.Form["DeviceID"]);
                    //projectDocument.StatusId = Convert.ToString(HttpContext.Current.Request.Form["StatusId"]);
                    projectDocument.JobNo = Convert.ToString(HttpContext.Current.Request.Form["JobNo"]);
                    projectDocument.IsPublished = Convert.ToBoolean(HttpContext.Current.Request.Form["IsPublished"]);
                    projectDocument.PublishedBy = Convert.ToString(HttpContext.Current.Request.Form["PublishedBy"]);
                    projectDocument.UserId = Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]);
                    
                    projectDocument.DocumentPath = docpath + filename;

                    //projectDocument.DocumentPath = docpath + "2017-28-3--10-20-07_moon.png";
                    projectDocument.CompanyName = Convert.ToString(HttpContext.Current.Request.Form["CompanyName"]);
                    projectDocument.ProjectNo = Convert.ToString(HttpContext.Current.Request.Form["ProjectNo"]);

                    if (string.IsNullOrEmpty(projectDocument.CompanyName))
                    {
                        projectDocument.CompanyName = "NONE";
                    }

                    //string fileExtension = System.IO.Path.GetExtension(docpath + filename);
                    string fileExtension = System.IO.Path.GetExtension(DocSavePath + filename);
                    projectDocument.FileType = fileExtension;

                    try
                    {
                        ManageDocumentDAL MDD = new ManageDocumentDAL();

                        bool chkResult = MDD.CheckDocumentAndDrawningName(projectDocument);

                        if(chkResult == true)
                        {
                            CD.Status = "0";
                            CD.Message = "Document name already exist.";
                            CD.Data = "";
                            return CD;
                        }

                        projectDocument.ApplicationOperation = "ProjectManagement_AddProjectDocument-";
                        ProjectDocumentEntity docds = (from row in MDD.DocumentInsert(projectDocument)
                                                       select new ProjectDocumentEntity
                                                       {
                                                           DocumentName = row["DocumentName"].ToString(),
                                                       }).FirstOrDefault();

                        CD.Status = "1";
                        CD.Message = "Success";
                        CD.Data = docds;
                    }
                    catch (Exception ex)
                    {
                        CD.Status = "0";
                        CD.Message = "Something went wrong...";
                        CD.Data = ex.Message.ToString();
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "Data Not Exist...";
                    CD.Data = "";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddDocumentAndDrawingDataController");
            }
            return CD;
        }
    }
}
