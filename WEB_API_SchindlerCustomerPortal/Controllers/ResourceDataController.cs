﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ResourceDataController : ApiController
    {
        [HttpPost]
        public CommonData AllResourceData(DataTableModel model)
        {
            CommonData CD = new CommonData();
            try
            {
                model.ApplicationOperation = "ResourceCenter_List";

                int totalcount = 0;
                var Resources = new List<ResourceUploadEntity>();

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                model.PageNumber = 0;
                model.PageSize = 1000;

                //Get Sort columns value
                model.SortColumn = "ResourceName";
                model.SortDirection = "ASC";

                #region Resource center old design code

                //model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                //var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                //model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                //model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //model.RoleID = User.RoleId;

                ////Get Sort columns value
                //model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                //model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                #endregion

                Resources = new List<ResourceUploadEntity>();
                ResourceDAL RD = new ResourceDAL();

                Resources = (from row in RD.ResourceSelect(model, ref totalcount)
                             select new ResourceUploadEntity
                             {
                                 ResourceId = Convert.ToInt32(row["ResourceID"]),
                                 ResourceName = row["ResourceName"].ToString(),
                                 ProductName = row["ProductName"].ToString(),
                                 DocumentType = row["DocumentType"].ToString(),
                                 //ExpiredDate = DBNull.Value.Equals(row["Expireddate"]) ? (DateTime?)null : Convert.ToDateTime(row["Expireddate"]),
                                 CreatedBy = row["CreatedBy"].ToString(),
                                 UpdatedBy = row["UpdatedBy"].ToString(),
                                 UploadDate = Convert.ToDateTime(row["CreatedDate"]),
                                 PublishedStatus = row["Published"].ToString(),
                                 URLName = row["URLName"].ToString(),
                                 DocumentPath = (Convert.ToString(row["DocumentPath"]) == "") ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() + HttpUtility.UrlPathEncode(Convert.ToString(row["DocumentPath"])),
                                 DocExtension = Convert.ToString(row["Document_FileExtension"]),
                                 //DocumentPath = row["DocumentPath"].ToString(),
                                 //PublishedOn = DBNull.Value.Equals(row["PublishedOn"]) ? (DateTime?)null : Convert.ToDateTime(row["PublishedOn"])
                                 ThumbnailImage = (Convert.ToString(row["TitleImage"])=="") ? SetThumbnail(Convert.ToString(row["Document_FileExtension"])) : 
                                                    WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() + HttpUtility.UrlPathEncode(Convert.ToString(row["TitleImage"])),
                                 TitleFileExtension = Convert.ToString(row["TitleImage_FileExtension"])
                             }).ToList();

                CD.Status = "1";
                CD.Message = (Resources.Count >= 1) ? "Success" : "No Resource Found..!";
                CD.Data = Resources;
                CD.Count = totalcount;

                //return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Resources }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Count = 0;
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceDataController");
            }
            return CD;
        }

        public string SetThumbnail(string type)
        {
            string thumbnailPath = WebConfigurationManager.AppSettings["DomainPath"].ToString() +
                                   WebConfigurationManager.AppSettings["thumbnailImagePath"].ToString(); 

            switch (type)
            {
                case ".doc" :
                case ".docx":
                    thumbnailPath += "/word.png";
                    break;
                case ".pptx":
                case ".ppt":
                    thumbnailPath += "/ppt.png";
                    break;
                case ".xls":
                case ".xlsx":
                    thumbnailPath += "/xls.png";
                    break;
                case ".jpe":
                case ".jpeg":
                    thumbnailPath += "/jpeg.png";
                    break;
                case ".png":
                    thumbnailPath += "/png.png";
                    break;
                case ".bim":
                    thumbnailPath += "/bim.png";
                    break;
                case ".odt":
                    thumbnailPath += "/odt.png";
                    break;
                case ".pdf":
                    thumbnailPath += "/pdf.png";
                    break;
                case ".cad":
                    thumbnailPath += "/cad.png";
                    break;
                case ".txt":
                    thumbnailPath += "/txt.png";
                    break;
                case ".gif":
                    thumbnailPath += "/gif.png";
                    break;
                case ".pps":
                    thumbnailPath += "/pps.png";
                    break;
                case ".rfa":
                    thumbnailPath += "/rfa.png";
                    break;
                case ".html":
                    thumbnailPath += "/www.png";
                    break;
                default:
                    thumbnailPath += "/no-image.png";
                    break;
            }

            return thumbnailPath;
        }
    }
}
