﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class EditDocumentController : ApiController
    {
        [HttpPost]
        public CommonData EditProjectDocument()
        {
            CommonData CD = new CommonData();
            ManageDocumentDAL MDD = new ManageDocumentDAL();
            ProjectDocumentEntity projectDocument = new ProjectDocumentEntity();

            var docpath = WebConfigurationManager.AppSettings["documentPath"].ToString();
            var DocSavePath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["documentPath"].ToString();

            string filename = string.Empty;
            //string mainDocPath = WebConfigurationManager.AppSettings["mainDocPath"].ToString();
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["AttachmentFile"];

                    if (httpPostedFile != null)
                    {
                        //docpath = docpath + mainDocPath;
                        filename = DatewithSeconds + "_" + httpPostedFile.FileName;
                        var fileSavePath1 = Path.Combine(DocSavePath, filename);
                        //projectDocument.DocumentPath = fileSavePath1;
                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath1);
                    }
                    string fileExtension = System.IO.Path.GetExtension(DocSavePath + filename);
                    projectDocument.DocumentPath = docpath + filename;
                    projectDocument.FileType = fileExtension;
                }
                if (HttpContext.Current.Request.Form.Count > 0) { 
                    projectDocument.DocumentID = Convert.ToInt32(HttpContext.Current.Request.Form["DocumentID"]);
                    projectDocument.DocumentName = Convert.ToString(HttpContext.Current.Request.Form["DocumentName"]);
                    projectDocument.DocumentTypeID = Convert.ToInt32(HttpContext.Current.Request.Form["DocumentTypeID"]);
                    projectDocument.Description = Convert.ToString(HttpContext.Current.Request.Form["Description"]);
                    projectDocument.ExpiredDate = Convert.ToString(HttpContext.Current.Request.Form["ExpiredDate"]);
                    //projectDocument.IsDeleted = Convert.ToBoolean(HttpContext.Current.Request.Form["IsDeleted"]);
                    projectDocument.IsDeleted = false;
                    projectDocument.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
                    projectDocument.DeviceID = Convert.ToString(HttpContext.Current.Request.Form["DeviceID"]);
                    projectDocument.SessionID = Convert.ToString(HttpContext.Current.Request.Form["SessionID"]);
                    //projectDocument.StatusId = Convert.ToString(HttpContext.Current.Request.Form["StatusId"]);
                    projectDocument.JobNo = Convert.ToString(HttpContext.Current.Request.Form["JobNo"]);
                    projectDocument.UpdatedBy = Convert.ToString(HttpContext.Current.Request.Form["UpdatedBy"]);
                    //projectDocument.DocumentPath = (docpath.Remove(0, 1) + filename);
                    projectDocument.CompanyName = Convert.ToString(HttpContext.Current.Request.Form["CompanyName"]);
                    projectDocument.ProjectNo = Convert.ToString(HttpContext.Current.Request.Form["ProjectNo"]);

                    if (string.IsNullOrEmpty(projectDocument.CompanyName))
                    {
                        projectDocument.CompanyName = "NONE";
                    }
                    //projectDocument.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                    
                    projectDocument.IsPublished = Convert.ToBoolean(HttpContext.Current.Request.Form["IsPublished"]);
                    projectDocument.ApplicationOperation = "ProjectManagement_AddProjectMilestone";
                    projectDocument.UserId = Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]);

                    try
                    {
                        bool chkResult = MDD.CheckDocumentAndDrawningName(projectDocument);

                        if (chkResult == true)
                        {
                            CD.Status = "0";
                            CD.Message = "Document name already exist.";
                            CD.Data = "";
                            return CD;
                        }

                        DocumentEntity docds = (from row in MDD.DocumentUpdate(projectDocument)
                                                select new DocumentEntity
                                                {
                                                    DocumentName = row["DocumentName"].ToString(),
                                                }).FirstOrDefault();

                        CD.Status = "1";
                        CD.Message = "Success";
                        CD.Data = docds;
                    }
                    catch (Exception ex)
                    {
                        CD.Status = "0";
                        CD.Message = "Fail to update record...";
                        CD.Data = ex.Message;
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "Data Not Exist...";
                    CD.Data = "";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateDocumentController/EditDocumentController");
            }
            return CD;
        }
    }
}