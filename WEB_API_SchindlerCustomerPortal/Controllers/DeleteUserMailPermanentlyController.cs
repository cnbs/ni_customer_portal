﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DeleteUserMailPermanentlyController : ApiController
    {
        [HttpPost]
        public CommonData DeleteUserMail(MailBoxEntity model)
        {
            model.ViewStatus = "read";
            model.Status = (int)MailStatus.Delete;
            MailBoxDAL MBD = new MailBoxDAL();
            CommonData CD = new CommonData();

            model.ApplicationOperation = "MailBox_LogicalDelete";

            try
            {
                var mail = MBD.GetUserMailLogicalDelete(model.MailId.ToString(), model.appAuditID, model.DeviceID, model.UserID, model.ApplicationOperation);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = mail;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteUserMailPermanentlyController");
            }
            return CD;
        }
    }
}
