﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class MajorProjectsController : ApiController
    {
        [HttpPost]
        [Route("api/MajorProjects/AddMajorProjects")]
        public CommonData AddMajorProjects(MajorProjectMasterEntity res)
        {
            CommonData CD = new CommonData();
            try
            {
                MajorProjectMasterDAL MD = new MajorProjectMasterDAL();
                var project = MD.Insert(res);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = project;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MajorProjectsController");
            }
            return CD;
        }

        [HttpPost]
        [Route("api/MajorProjects/UpdateMajorProjects")]
        public CommonData UpdateMajorProjects(MajorProjectMasterEntity res)
        {
            CommonData CD = new CommonData();
            try
            {
                MajorProjectMasterDAL MD = new MajorProjectMasterDAL();
                var project = MD.Update(res);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = project;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MajorProjectsController");
            }
            return CD;
        }

        [HttpPost]
        [Route("api/MajorProjects/PostMajorProjects")]
        public HttpResponseMessage PostMajorProjects(CommonEntityData userdata)
        {
            SearchInfoMajorProjectsViewModel model = new SearchInfoMajorProjectsViewModel();

            int totalcount = 0;
            var draw = 0;
            #region "Audit Trail"
            int appAuditID = 0;           
            model.appAuditID = appAuditID;
            model.ApplicationOperation ="GetMajorProjectList";
            model.SessionID = userdata.UserId;


            #endregion
            CommonData CD = new CommonData();

            List<MajorProjectMasterEntity> majorProjectList = new List<MajorProjectMasterEntity>();

            DateTime? NullableDate = null;
            try

            {              
                MajorProjectMasterDAL majorProjectsDAL = new MajorProjectMasterDAL();  
                model.SearchTerm = System.Web.HttpContext.Current.Request.Form.GetValues("search[value]")[0] ?? "";
                model.PageNumber = Convert.ToInt16(System.Web.HttpContext.Current.Request.Form.GetValues("start")[0]);
                //model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["numberOfRecords"].ToString());
                model.PageSize = Convert.ToInt32(System.Web.HttpContext.Current.Request.Params.GetValues("length")[0]);
                model.SortColumn = System.Web.HttpContext.Current.Request.Form.GetValues("columns[" + System.Web.HttpContext.Current.Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault() ?? "";
                model.SortDirection = System.Web.HttpContext.Current.Request.Form.GetValues("order[0][dir]").FirstOrDefault() ?? "";
                draw = Convert.ToInt16(System.Web.HttpContext.Current.Request.Form.GetValues("draw").FirstOrDefault());


                majorProjectList = (from row in majorProjectsDAL.SelectByPage(model , ref totalcount)

                                    select new MajorProjectMasterEntity

                                    {

                                        MajorProjectID = Convert.ToInt32(row["MajorProjectID"]),
                                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                                        CompanyName = Convert.ToString(row["CompanyName"]),

                                        ProjectName = Convert.ToString(row["ProjectName"]),

                                        ProjectShortName = Convert.ToString(row["ProjectShortName"]),

                                       // ProjectAdditionalInfo = Convert.ToString(row["ProjectAdditionalInfo"]),

                                      //  ProjectConfidentialInfo = Convert.ToString(row["ProjectConfidentialInfo"]),

                                      //  ProjectMgrPhone = string.IsNullOrEmpty(row["ProjectMgrPhone"].ToString()) ? "" : Convert.ToString(row["ProjectMgrPhone"]).ToString(),

                                      //  ProjectMgrEmail = string.IsNullOrEmpty(row["ProjectMgrPhone"].ToString()) ? "" : Convert.ToString(row["ProjectMgrEmail"]).ToString(),

                                       // ContractorAltPhone = string.IsNullOrEmpty(row["ContractorAltPhone"].ToString()) ? "" : Convert.ToString(row["ContractorAltPhone"]).ToString(),

                                        StreetAddress = string.IsNullOrEmpty(row["StreetAddress"].ToString()) ? "" : Convert.ToString(row["StreetAddress"]).ToString(),

                                      //  StateCode = string.IsNullOrEmpty(row["StateCode"].ToString()) ? "" : Convert.ToString(row["StateCode"]).ToString(),

                                       // City = string.IsNullOrEmpty(row["City"].ToString()) ? "" : Convert.ToString(row["City"]).ToString(),

                                      //  ZipCode = string.IsNullOrEmpty(row["ZipCode"].ToString()) ? "" : Convert.ToString(row["ZipCode"]).ToString(),

                                      //  Phone = string.IsNullOrEmpty(row["Phone"].ToString()) ? "" : Convert.ToString(row["Phone"]).ToString(),


                                        Contact1Name = string.IsNullOrEmpty(row["Contact1Name"].ToString()) ? "" : Convert.ToString(row["Contact1Name"]).ToString(),

                                      //  Contact1Email = string.IsNullOrEmpty(row["Contact1Email"].ToString()) ? "" : Convert.ToString(row["Contact1Email"]).ToString(),

                                      //  Contact1Designation = string.IsNullOrEmpty(row["Contact1Designation"].ToString()) ? "" : Convert.ToString(row["Contact1Designation"]).ToString(),

                                      //  Contact1Phone = string.IsNullOrEmpty(row["Contact1Phone"].ToString()) ? "" : Convert.ToString(row["Contact1Phone"]).ToString(),



                                        Contact2Name = string.IsNullOrEmpty(row["Contact2Name"].ToString()) ? "" : Convert.ToString(row["Contact2Name"]).ToString(),

                                      //  Contact2Email = string.IsNullOrEmpty(row["Contact2Email"].ToString()) ? "" : Convert.ToString(row["Contact2Email"]).ToString(),

                                     //   Contact2Designation = string.IsNullOrEmpty(row["Contact2Designation"].ToString()) ? "" : Convert.ToString(row["Contact2Designation"]).ToString(),

                                       // Contact2Phone = string.IsNullOrEmpty(row["Contact2Phone"].ToString()) ? "" : Convert.ToString(row["Contact2Phone"]).ToString(),



                                       // ProjectLogo = string.IsNullOrEmpty(row["ProjectLogo"].ToString()) ? "" : Convert.ToString(row["ProjectLogo"]).ToString(),

                                      //  CreatedBy = string.IsNullOrEmpty(row["CreatedBy"].ToString()) ? "" : Convert.ToString(row["CreatedBy"]).ToString(),

                                       // UpdatedBy = string.IsNullOrEmpty(row["UpdatedBy"].ToString()) ? "" : Convert.ToString(row["UpdatedBy"]).ToString(),

                                       // UpdatedDate = string.IsNullOrEmpty(row["UpdatedDate"].ToString()) ? NullableDate : Convert.ToDateTime(row["UpdatedDate"]),



                                     //   PublishedBy = string.IsNullOrEmpty(row["PublishedBy"].ToString()) ? "" : Convert.ToString(row["PublishedBy"]).ToString(),

                                      //  IsPublished = string.IsNullOrEmpty(row["IsPublished"].ToString()) ? false : Convert.ToBoolean(row["IsPublished"]),

                                     //  PublishedDate = string.IsNullOrEmpty(row["PublishedDate"].ToString()) ? NullableDate : Convert.ToDateTime(row["PublishedDate"])

                                    }).ToList();



                CD.Status = "1";

                CD.Message = "Success";

                //CD.Data = majorProjectList;
                CD.Data = new { majorProjectList};
            }

            catch (Exception ex)

            {

                CD.Status = "0";

                CD.Message = "Something went wrong..!";

                CD.Data = ex.Message.ToString();

                ErrorLogClass lm = new ErrorLogClass();

                lm.CreateLogFiles();

                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();

                lm.ErrorLog(Errorlogpath, ex.Message + " - GeMajorProjectsController");

            }
            return Request.CreateResponse(HttpStatusCode.OK, new { draw = draw, recordsFiltered = majorProjectList.Count, recordsTotal = majorProjectList.Count, data = majorProjectList });
          //  return CD;

        }



        [HttpGet]
        [Route("api/MajorProjects/GeMajorProjectsByID")]
        public CommonData GeMajorProjectsByID(int MajorProjectID)
        {
            CommonData CD = new CommonData();
            MajorProjectMasterEntity objMajorProject = new MajorProjectMasterEntity();

            try
            {
                MajorProjectMasterDAL majorProjectsDAL = new MajorProjectMasterDAL();

                objMajorProject = majorProjectsDAL.Select(MajorProjectID);
                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = objMajorProject;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GeMajorProjectsController");
            }
            return CD;
        }
    }
}
