﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DeleteOfficeManagerController : ApiController
    {
        public CommonData OfficeManagerDelete(OfficeManagerEntity model)
        {
            CommonData CD = new CommonData();

            //var getUserCompanyName = GetOfficeManagerById(model.OfficeManagerID);
            model.ApplicationOperation = "OfficeManager_Delete";
            
            model.UpdatedBy = model.UserID.ToString();
            try
            {
                OfficeManagerDAL OMD = new OfficeManagerDAL();
                bool user = OMD.DeleteOfficeManager(model);

                CD.Status = "1";
                CD.Data = (user==true) ? "Record successfully deleted..." : "Record Not Found...!";
                CD.Message = "Success";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Data = ex.Message.ToString();
                CD.Message = "Something went wrong";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteOfficeManagerController");
            }
            return CD;
        }
    }
}
