﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Models;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;
using WEB_API_SchindlerCustomerPortal.Services;

namespace WEB_API_OmniSalesHub.Controllers
{

    public class LoginController : ApiController
    {
        [HttpPost]
        public CommonData UserLogin([FromBody]UserMaster data)
        {
            #region LogData

            ErrorLogClass lm = new ErrorLogClass();
            lm.DataLogFiles();
            var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
            string LogData = "Email : " + data.Email + "Password : " + data.Password+ "DeviceID : " + data.DeviceID+ "DeviceType : " + data.DeviceType;
            lm.ErrorLog(Errorlogpath, LogData + " - Login" + DateTime.Now.ToLongDateString());

            #endregion

            CommonData CD = new CommonData();
            string IpAddress = string.Empty;
            string Clientbrowser = string.Empty;
            string ClientOS = string.Empty;
            string ServerIP = string.Empty;
            string ServerName = string.Empty;

            UserMaster user = new UserMaster()
            {
                Email = data.Email,
                Password = data.Password,
            };

            if (Convert.ToString(user.Email).Trim() == null)
            {
                ModelState.AddModelError("Email", "Email is required");
            }

            if (Convert.ToString(user.Password).Trim() == null)
            {
                ModelState.AddModelError("Password", "Password is required");
            }

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(user.Email))
                {
                    user.Email = user.Email.Trim();
                }

                if (!string.IsNullOrEmpty(user.Password))
                {
                    user.Password = user.Password.Trim();
                }

                UserLoginService ULS = new UserLoginService();

                UserEntity userresult = ULS.Find(Convert.ToString(user.Email.ToLower()));
                if (userresult != null)
                {
                    if (userresult.AccountStatus == 1)
                    {
                        string NewPwd = EncryptionService.CreatePasswordHash(user.Password, userresult.PasswordSalt, "");

                        if (NewPwd == userresult.Password)
                        {
                            if (userresult.CompanyID == 1 || userresult.CompanyID == 3)
                            {
                                userresult.CompanyID = 0;
                                userresult.CompanyName = "Schindler";
                            }

                            //Remove password and salt
                            userresult.Password = "";
                            userresult.PasswordSalt = "";
                            userresult.appAuditID = 0;

                            var menuModel = new List<UserPermissionViewModel>();
                            menuModel = ULS.GetAllMenuByParentCategoryId(userresult.UserID, userresult.CompanyID, 0);

                            userresult.ProfilePic = (string.IsNullOrEmpty(Convert.ToString(userresult.ProfilePic))) ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() + WebConfigurationManager.AppSettings["userProfilePicPath"].ToString() + userresult.ProfilePic;
                            userresult.userpermissionviewModel = menuModel;

                            try
                            {
                                if (!string.IsNullOrEmpty(data.DeviceType))
                                {
                                    userresult.DeviceID = data.DeviceID;
                                    userresult.DeviceType = data.DeviceType;
                                    UserDAL UD = new UserDAL();
                                    int i = UD.UpdateLoginDeviceInfo(userresult);
                                }

                                userresult.ContactNumber = string.IsNullOrEmpty(userresult.ContactNumber.ToString()) ? "" : userresult.ContactNumber.ToString().Insert(3, "-").Insert(7, "-");
                                CD.Status = "1";
                                CD.Message = "Success";
                                CD.Data = userresult;
                            }
                            catch (Exception ex)
                            {
                                CD.Status = "0";
                                CD.Message = "Problem During Updating Device Information...!";
                                CD.Data = ex.Message.ToString();
                            }
                        }
                        else
                        {
                            CD.Status = "0";
                            CD.Data = "";
                            CD.Message = "Invalid LoginID or Password";
                        }
                    }
                    else if (userresult.AccountStatus == 4)
                    {
                        CD.Status = "0";
                        CD.Data = "";
                        CD.Message = "Your account is not Verified.";
                    }
                    else if (userresult.AccountStatus == 17)
                    {
                        CD.Status = "0";
                        CD.Data = "";
                        CD.Message = "Your account is not active.";
                    }
                    else if (userresult.AccountStatus == 18)
                    {
                        CD.Status = "0";
                        CD.Data = "";
                        CD.Message = "Your account is approved and pending activation. Please check approval Email send by myschindlerprojects.com and follow the instruction.";
                    }
                    else
                    {
                        CD.Status = "0";
                        CD.Data = "";
                        CD.Message = "Invalid Status";

                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Data = "";
                    CD.Message = "Invalid LoginID or Password";
                }
            }
            else
            {
                CD.Status = "0";
                CD.Data = "";
                CD.Message = "Invalid Status";

            }

            #region LogData

            lm.DataLogFiles();
            LogData = "Message : " + CD.Message;
            lm.ErrorLog(Errorlogpath, LogData + " - Login" + DateTime.Now.ToLongDateString());

            #endregion

            return CD;
        }
    }
}
