﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetJobInvoiceDocPhotoDataController : ApiController
    {
        [HttpPost]
        public CommonData GetJobInvoiceDocPhotoData(DataTableModel model)
        {
            CommonData CD = new CommonData();
            ProjectDAL PD = new ProjectDAL();
            List<DocumentPathData> docPath = new List<DocumentPathData>();

            try
            {
                model.ApplicationOperation = "GetJobPivotTable";

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                var docData = new List<DocumentPathData>();
                docData = JsonConvert.DeserializeObject<List<DocumentPathData>>(PD.GetJobInvoiceDocPhotoList(model));

                if(docData == null)
                {
                    CD.Data = "";
                    CD.Status = "0";
                    CD.Message = "No data found!";
                    return CD;
                }

                docPath = (from row in docData
                           select new DocumentPathData
                           {
                               ProjectNo = Convert.ToInt32(row.ProjectNo),
                               JobNo = Convert.ToString(row.JobNo),
                               DocumentName = Convert.ToString(row.DocumentName),
                               DocumentPath = (Convert.ToString(row.DocumentPath) == null) ? "" : WebConfigurationManager.AppSettings["DomainPath"].ToString() + HttpUtility.UrlPathEncode(Convert.ToString(row.DocumentPath))
                           }).ToList();

                CD.Data = docPath;
                CD.Status = "1";
                CD.Message = "Success";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - JobInvoiceDocPhotoLinksandPath");
            }
            return CD;
        }
    }
}
