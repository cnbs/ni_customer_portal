﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class SyncDataWithZipController : ApiController
    {

        // Content type for our body
        private static readonly MediaTypeHeaderValue _mediaType = MediaTypeHeaderValue.Parse("application/octet-stream");

        [HttpPost]
        public HttpResponseMessage GeZipFiles([FromBody]ContentRepo data)
        {
            HttpResponseMessage result = new HttpResponseMessage();

            string AssetPath = System.Configuration.ConfigurationManager.AppSettings["AssetsPath"].ToString();

            string zipPath = AssetPath + "\\" + data.CompanyID + "\\" + data.FileName + ".zip";
            byte[] _content = File.ReadAllBytes(zipPath);

            MemoryStream memStream = new MemoryStream(_content);

            if (Request.Headers.Range != null)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", "Range" + this.Request.Headers.Range.Ranges.FirstOrDefault() + " - SyncDataWithZip");

                try
                {
                    HttpResponseMessage partialResponse = Request.CreateResponse(HttpStatusCode.PartialContent);
                    partialResponse.Content = new ByteRangeStreamContent(memStream, Request.Headers.Range, _mediaType);
                    return partialResponse;
                }
                catch (InvalidByteRangeException invalidByteRangeException)
                {
                    return Request.CreateErrorResponse(invalidByteRangeException);
                }
            }
            else
            {
                // If it is not a range request we just send the whole thing as normal
                HttpResponseMessage fullResponse = Request.CreateResponse(HttpStatusCode.OK);
                fullResponse.Content = new StreamContent(memStream);
                fullResponse.Content.Headers.ContentType = _mediaType;
                return fullResponse;
            }
        }
    }
}
