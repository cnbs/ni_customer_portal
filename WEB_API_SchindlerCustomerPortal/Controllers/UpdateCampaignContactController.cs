﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class UpdateCampaignContactController : ApiController
    {
        [HttpPost]
        public object UpdateContact([FromBody]CampaignContact data)
        {
            data.Title = string.IsNullOrEmpty(data.Title) ? "" : data.Title;
            data.PhoneNumber2 = string.IsNullOrEmpty(data.PhoneNumber2) ? "" : data.PhoneNumber2;
            data.CompanyName = string.IsNullOrEmpty(data.CompanyName) ? "" : data.CompanyName;
            data.Address1 = string.IsNullOrEmpty(data.Address1) ? "" : data.Address1;
            data.Address2 = string.IsNullOrEmpty(data.Address2) ? "" : data.Address2;
            data.City = string.IsNullOrEmpty(data.City) ? "" : data.City;
            data.State = string.IsNullOrEmpty(data.State) ? "" : data.State;
            data.Zip = string.IsNullOrEmpty(data.Zip) ? "" : data.Zip;
            data.WebsiteAddress = string.IsNullOrEmpty(data.WebsiteAddress) ? "" : data.WebsiteAddress;
            data.Notes = string.IsNullOrEmpty(data.Notes) ? "" : data.Notes;
            data.ContactProfilePic = string.IsNullOrEmpty(data.ContactProfilePic) ? "" : data.ContactProfilePic;
            data.BusinessCardPic = string.IsNullOrEmpty(data.BusinessCardPic) ? "" : data.BusinessCardPic;

            bool result = LoginModule.UpdateCampaignContact(data.ContactID,data.ContactProfilePic,data.BusinessCardPic ,data.FirstName, data.LastName, data.Title, data.CampaignID, data.PhoneNumber1, data.PhoneNumber2, data.Email, data.CompanyName, data.Address1, data.Address2, data.City, data.State, data.Zip, data.WebsiteAddress, data.Notes, data.CreatedBy,data.DeviceID);

            if (result == true)
            {
                DataSet tempds = new DataSet();
                DataTable t = new DataTable();
                tempds.Tables.Add(t);
                tempds.Tables[0].TableName = "CampaignContact";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "1";
                dr["Message"] = "Your Contact has been updated successfully!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdateCampaignContact", "UpdateCampaignContact - Success", data.CreatedBy, data.DeviceID, "Update");

                return tempds;
            }

            else
            {
                DataSet tempds1 = new DataSet();
                DataTable t = new DataTable();
                tempds1.Tables.Add(t);
                tempds1.Tables[0].TableName = "CampaignContact";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "No Contact Matched!";

                t.Rows.Add(dr);

                bool audit = APIAudit.APIAuditRecord("api/UpdateCampaignContact", "UpdateCampaignContact - Failed", data.CreatedBy, data.DeviceID, "Update");

                return tempds1;


            }
        }
    }
}
