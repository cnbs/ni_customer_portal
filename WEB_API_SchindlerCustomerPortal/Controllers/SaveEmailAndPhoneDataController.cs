﻿using System;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class SaveEmailAndPhoneDataController : ApiController
    {
        [HttpPost]
        public CommonData SaveEmailAndPhoneData(DataTableModel model)
        {
            CommonData CD = new CommonData();
            if(model.Phone != null)
            {
                model.Phone = model.Phone.Replace("-", "");
            }
            try
            {
                ProjectDAL projectSevice = new ProjectDAL();
                model.ApplicationOperation = "SaveEmailAndPhone";

                CD.Data = (projectSevice.updateEmailAndPhoneData(model) == true) ? true : false ;
                CD.Status = (projectSevice.updateEmailAndPhoneData(model) == true) ? "1" : "0";
                CD.Message = (projectSevice.updateEmailAndPhoneData(model) == true) ? "Data successfully updated..." : "Update failed..!";
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = "";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SaveEmailAndPhoneData");
            }
            return CD;
        }

    }
}
