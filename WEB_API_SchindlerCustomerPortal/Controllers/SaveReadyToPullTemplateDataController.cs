﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class SaveReadyToPullTemplateDataController : ApiController
    {
        public CommonData AddReadyToPullTemplateDetails()
        {
            List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails = new List<ReadyToPullTemplateDetails>();
            int ProjectNo = 0;
            string JobNo = "";
            CommonData CD = new CommonData();
            ProjectStatusDueDateEntity projectMilestoneEntity = new ProjectStatusDueDateEntity();

            projectMilestoneEntity.Comment = Convert.ToString(System.Web.HttpContext.Current.Request.Form["Note"]);
            projectMilestoneEntity.Milestone = Convert.ToString("Job-Site Ready");
            projectMilestoneEntity.StatusId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["Status"]);
            projectMilestoneEntity.StatusName = Convert.ToString(System.Web.HttpContext.Current.Request.Form["StatusName"]);

            if (System.Web.HttpContext.Current.Request.Form["DueDate"] == null || System.Web.HttpContext.Current.Request.Form["DueDate"] == "")
            {
                projectMilestoneEntity.DueDate = null;
            }
            else
            {
                projectMilestoneEntity.DueDate = Convert.ToString(System.Web.HttpContext.Current.Request.Form["DueDate"]);
            }


            if (System.Web.HttpContext.Current.Request.Form["CompletedDate"] == "" || System.Web.HttpContext.Current.Request.Form["CompletedDate"] == null)
            {
                projectMilestoneEntity.CompletedDate = null;
            }
            else
            {
                projectMilestoneEntity.CompletedDate = Convert.ToString(System.Web.HttpContext.Current.Request.Form["CompletedDate"]);
            }
            projectMilestoneEntity.PreviousStatusName = Convert.ToString(System.Web.HttpContext.Current.Request.Form["PreviousStatusName"]);
            projectMilestoneEntity.ProjectNo = Convert.ToString(System.Web.HttpContext.Current.Request.Form["ProjectNo"]);
            projectMilestoneEntity.JobNo = Convert.ToString(System.Web.HttpContext.Current.Request.Form["JobNo"]);
            projectMilestoneEntity.UserId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["UserID"]);

            AddProjectBankMilestoneCommentController addProjectBankMilestoneComment = new AddProjectBankMilestoneCommentController();
            CommonData addData = addProjectBankMilestoneComment.AddProjectBankMilestoneComment(projectMilestoneEntity);

            if(addData.Status != "1")
            {
                return addData;
            }

            try
            {
                if (System.Web.HttpContext.Current.Request.Form.AllKeys.Length > 0)
                {
                    foreach (string key in System.Web.HttpContext.Current.Request.Form.AllKeys)
                    {
                        switch (key)
                        {
                            case "lstReadyToPullTemplateDetails":
                                {
                                    var xyz = System.Web.HttpContext.Current.Request.Form[key];
                                    //var comPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString() + "abc.txt";
                                    //File.WriteAllText(comPath, Convert.ToString(xyz));
                                    lstReadyToPullTemplateDetails = JsonConvert.DeserializeObject<List<ReadyToPullTemplateDetails>>(xyz);
                                    break;
                                }
                            case "ProjectNo":
                                {
                                    ProjectNo = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form[key]);
                                    //var comPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString() + "abc1.txt";
                                    //File.WriteAllText(comPath, Convert.ToString(ProjectNo));
                                    break;
                                }
                            case "JobNo":
                                {
                                    JobNo = System.Web.HttpContext.Current.Request.Form[key];
                                    //var comPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString() + "abc2.txt";
                                    //File.WriteAllText(comPath, Convert.ToString(JobNo));
                                    break;
                                }
                            default:
                                {
                                    int TempId = 0;
                                    break;
                                }
                        }
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "NO Key";
                }
                if (lstReadyToPullTemplateDetails.Count > 0)
                {
                    foreach (ReadyToPullTemplateDetails rd in lstReadyToPullTemplateDetails)
                    {
                        //rd.CreatedBy = Convert.ToString(lstReadyToPullTemplateDetails);
                        //rd.CreatedBy = Convert.ToString(HttpContext.Current.Request.Form["UserId"]);
                        //rd.appAuditID = Convert.ToInt32(HttpContext.Current.Request.Form["appAuditID"]);
                        //rd.SessionID = Convert.ToString(HttpContext.Current.Request.Form["SessionID"]);

                        rd.CreatedDate = DateTime.Now.ToString("M/d/yyyy");
                        rd.ApplicationOperation = "ProjectManagement_SaveReadyToPullMilestone";

                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            if (System.Web.HttpContext.Current.Request.Files.Count > 0)
                            {
                                foreach (string attachment in System.Web.HttpContext.Current.Request.Files)
                                {
                                    if (attachment.IndexOf('~') > 0)
                                    {
                                        string[] strFileinfo = attachment.Split('~');
                                        string originalFileName = strFileinfo[1];
                                        string newfilename = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + strFileinfo[1];

                                        if ("filename_" + rd.TemplateId.ToString().Trim() == strFileinfo[0])
                                        {
                                            HttpPostedFile file = HttpContext.Current.Request.Files[attachment];
                                            //var comPath = WebConfigurationManager.AppSettings["readyToPullAttachmentsPath"].ToString() + newfilename;
                                            var comPath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() + WebConfigurationManager.AppSettings["readyToPullAttachmentsPath"].ToString() + newfilename;
                                            file.SaveAs(comPath);
                                            rd.Attachment = newfilename;
                                            rd.RealAttachment = originalFileName;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                MilestoneDAL MD = new MilestoneDAL();
                bool result = MD.AddMilestoneReadyToPullTemplateDetails(lstReadyToPullTemplateDetails, ProjectNo, JobNo);

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = result;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong while uploading files...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SaveReadyToPullTemplateDataController");
            }
            return CD;
        }
    }
}
