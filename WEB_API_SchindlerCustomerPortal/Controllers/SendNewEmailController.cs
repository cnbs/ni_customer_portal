﻿using Newtonsoft.Json;
using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class SendNewEmailController : ApiController
    {
        public CommonData SendMail()
        {
            MailBoxDAL MBD = new MailBoxDAL();
            MailBoxEntity model = new MailBoxEntity();
            var userList = new List<MailBoxEntity>();
            CommonData CD = new CommonData();
            string returnFileName = string.Empty;

            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];

                        var ext = Path.GetExtension(file.FileName);
                        string onlyFilename = file.FileName.Substring(0, file.FileName.LastIndexOf('.'));
                        var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
                        var newFileName = (dateTime + ext).ToString();
                        var comPath = WebConfigurationManager.AppSettings["documentStartPath"].ToString() +
                                        WebConfigurationManager.AppSettings["EmailAttachmentPath"] + newFileName;

                        file.SaveAs(comPath);

                        if (string.IsNullOrEmpty(returnFileName))
                        {
                            returnFileName = newFileName;
                        }
                        else
                        {
                            returnFileName += "|" + newFileName;
                        }
                    }
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No attachment found..!";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong during file upload..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendNewEmailController");
                return CD;
            }

            try
            {
                if (System.Web.HttpContext.Current.Request.Form.AllKeys.Length > 0)
                {
                    string xyz = HttpContext.Current.Request.Form["MailData"];
                    model = JsonConvert.DeserializeObject<MailBoxEntity>(xyz.Substring(1, xyz.Length - 2));
                    model.TotalFileName = returnFileName;
                }
                else
                {
                    CD.Status = "0";
                    CD.Message = "No key found..!";
                    return CD;
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendNewEmailController");
                return CD;
            }


            model.ApplicationOperation = "MailBox_SendMail";

            if (model.MailId != 0 || string.IsNullOrEmpty(Convert.ToString(model.MailId))) // for draft mail to send
            {
                var mail = MBD.UserMailLogicalDelete(Convert.ToString(model.MailId), model.appAuditID, model.SessionID, model.UserID, model.ApplicationOperation.ToString());
            }

            model.ViewStatus = "unread";
            model.Status = (int)MailStatus.Inbox; //9;

            if (string.IsNullOrEmpty(model.ForwardFileList) || model.ForwardFileList == "null")
            {
                if (string.IsNullOrEmpty(model.TotalFileName) || model.TotalFileName == "null")
                {
                    model.HasAttachment = false;
                    model.AttachmentPath = string.Empty;
                }
                else
                {
                    model.HasAttachment = true;
                    model.AttachmentPath = model.TotalFileName;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.TotalFileName) || model.TotalFileName == "null")
                {
                    model.HasAttachment = false;
                    model.AttachmentPath = string.Empty;
                }
                else
                {
                    model.HasAttachment = true;
                    model.AttachmentPath = model.ForwardFileList + "|" + model.TotalFileName;
                }
            }

            if (string.IsNullOrEmpty(model.ToList))
                model.ToList = string.Empty;

            if (string.IsNullOrEmpty(model.CC))
                model.CC = string.Empty;

            if (string.IsNullOrEmpty(model.BCC))
                model.BCC = string.Empty;
            //var result = mailBox.SendNewMail(mail);
            //return result;

            try
            {
                userList = (from row in MBD.SendNewMail(model)
                            select new MailBoxEntity
                            {
                                MailId = Convert.ToInt32(row["MailId"]),
                                Receiver = row["ReceiverId"].ToString(),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                DeviceID = Convert.ToString(row["DeviceId"]),
                                DeviceType = Convert.ToString(row["DeviceType"]),
                                DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"]),
                                SenderId = Convert.ToString(row["SenderId"]),
                                SenderName = Convert.ToString(row["SenderName"])
                            }).ToList();


                // var mailResult = MBD.SendNewMail(model);
                var mailId = 0;
                StaticUtilities SU = new StaticUtilities();
                UserDAL UD = new UserDAL();
                string chkMailId = "";

                foreach (var item in userList)
                {
                    if (mailId == item.MailId)
                        continue;

                    if (!string.IsNullOrEmpty(chkMailId))
                    {
                        if (chkMailId.Contains(item.Receiver))
                            continue;
                    }
                    
                    chkMailId += item.Receiver;
                    
                    //if (mailId == 0)
                    //{
                        mailId = item.MailId;

                        #region Email Send

                        string SFrom = WebConfigurationManager.AppSettings["AccountMail"].ToString();
                        string Subject = "myschindlerprojects - " + model.Subject;

                        string ActivationUrl = string.Empty;

                        //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        string headurl = WebConfigurationManager.AppSettings["BaseURL"];

                        ActivationUrl = HttpUtility.UrlPathEncode("" + headurl + "/mailbox/maillist?mail=" + item.MailId + "");

                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", item.UserName.Split(' ')[0]);
                        dic.Add("SenderName", item.SenderName);

                        dic.Add("MessageURL", ActivationUrl);

                        Sbody = SU.getEmailBody("User", "EmailNotificationNew.html", dic);

                        SendEmail.SendMailtoMultiple(SFrom, item.Receiver, "", "", Subject, Sbody);

                        #region Push Notification
                        string IsNotificationEnable = WebConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = WebConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "New Email Notification";
                                string NotificationBody = SU.getNotificationBody("User", "EmailNotificationNew.txt", dic);

                                try
                                {
                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);

                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {
                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }


                                //Insert into NotificationLogId table

                                data.CompanyID = model.CompanyId;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(model.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                data.ModuleID = 34;

                                data.CreatedBy = model.FirstName + " " + model.LastName;
                                data.DeviceType = item.DeviceType;
                                var Notify = UD.InsertNotification(data);

                            }
                        }

                        #endregion

                        #endregion

                    //}
                }

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong..!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendNewEmailController");
            }
            return CD;
        }
    }
}
