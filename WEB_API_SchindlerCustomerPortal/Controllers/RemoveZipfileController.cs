﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class RemoveZipfileController : ApiController
    {
        [HttpPost]
        public object RemoveZip([FromBody]ZipfileContent data)
        {
            string ZipAssetPath = System.Configuration.ConfigurationManager.AppSettings["AssetsPath"].ToString();
            string zipPath = ZipAssetPath + "\\" + data.CompanyID + "\\" + data.ZipfileName + ".zip";

            try
            {
                if (File.Exists(zipPath)) // check zipfile is exist or not
                {
                    File.Delete(zipPath); // delete zipfile
                    var resp = new HttpResponseMessage()
                    {
                        Content = new StringContent("{\"Status\":\"1\",\"Message\":\"Success\"}")
                    };
                    resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    return resp;
                }
                else
                {
                    var resp = new HttpResponseMessage()
                    {
                        Content = new StringContent("{\"Status\":\"0\",\"Message\":\"Fail\"}")
                    };
                    resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    return resp;
                }
                
            }
            catch (Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message + " - RemoveZipfile api controller");
                return false;
            }
            finally
            {
                
            }
        }
    }
}
