﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class MilestoneUpdateDataController : ApiController
    {
        [HttpPost]
        public CommonData MilestoneUpdateService(ProjectMilestoneEntity milestone)
        {
            MilestoneDAL MD = new MilestoneDAL();
            ManageProjectDAL MPD = new ManageProjectDAL();
            CommonData CD = new CommonData();
            ProjectDAL PD = new ProjectDAL();
            ErrorLogClass lm = new ErrorLogClass();
            lm.DataLogFiles();
            var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
            string LogData = "";
            try
            {
                if (string.IsNullOrEmpty(milestone.Note))
                {
                    milestone.Note = "";
                }
                else
                {
                    milestone.Note = DateTime.Now.ToString("MM/dd/yyyy") + " " + milestone.UpdatedBy  + "\r \n" + milestone.Note + "\r \n";

                    LogData = "milestone.Note : " + milestone.Note ;
                    lm.ErrorLog(Errorlogpath, LogData + " - MilestoneUpdateService" + DateTime.Now.ToLongDateString());
                }

                milestone.ApplicationOperation = "ProjectManagement_UpdateProjectMilestone";

                var ProjData = PD.GetProjectandJobPreviousStatus(milestone.ProjectNo, milestone.JobNo).Where(a => Convert.ToString(a.ItemArray[5]) == milestone.MilestoneTitle.ToString()).ToList();
                milestone.CompanyId = Convert.ToInt32(ProjData[0]["CompanyId"].ToString().Trim());

                milestone.ProjectName = ProjData[0].ItemArray[0].ToString().Split('|')[0].ToString().Trim();
                milestone.MilestoneName = ProjData[0].ItemArray[4].ToString().Trim();
                milestone.PreviousStatusName = ProjData[0].ItemArray[2].ToString().Trim();
                milestone.StatusName = PD.GetStatusFromStatusID(milestone.Status);

                var mailNotificationUserList = GetProjectAssignedUserListForMilestoneEmail(milestone.ProjectNo);

                bool obj = MD.MilestoneUpdate(milestone);


                #region Email Send

                #region LogData

                LogData = "ProjectName : " + milestone.ProjectName + ", MilestoneName : " + milestone.MilestoneName +
                                    ", StatusName : " + milestone.StatusName + ", PreviousStatusName : " + milestone.PreviousStatusName;
                lm.ErrorLog(Errorlogpath, LogData + " - MilestoneUpdateService" + DateTime.Now.ToLongDateString());

                #endregion

                if (milestone.StatusName.ToUpper() != milestone.PreviousStatusName.ToUpper())
                {
                    foreach (var item in mailNotificationUserList)
                    {
                        string Subject = "myschindlerprojects – [[ProjectNoProjectNameBankNoBankDesc]] status has been updated to [[StatusName]].";
                        //string headurl = Request.RequestUri.AbsoluteUri;
                        //var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                        string headurl = WebConfigurationManager.AppSettings["BaseURL"].ToString() + "/ManageProject/ProjectJobMainView/" + milestone.JobNo + "/" + milestone.ProjectNo + "/" + milestone.CompanyId + "/" + "Milestone";

                        string Sbody = null;
                        StaticUtilities SU = new StaticUtilities();

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", milestone.MilestoneName);
                        dic.Add("ProjectNo", milestone.ProjectNo + (milestone.JobNo != null ? " - " + milestone.JobNo : ""));
                        dic.Add("StatusName", milestone.StatusName);
                        dic.Add("PreviousStatusName", milestone.PreviousStatusName);
                        //dic.Add("UserName", User.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (milestone.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestone.ProjectNo + " - " + milestone.ProjectName + " and Bank " + milestone.JobDesc + " [" + milestone.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestone.ProjectNo + " - " + milestone.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        Subject = Subject.Replace("[[ProjectNoProjectNameBankNoBankDesc]]", ProjectNoProjectNameBankNoBankDesc).Replace("[[StatusName]]", milestone.StatusName);

                        dic.Add("MessageURL", headurl);

                        Sbody = SU.getEmailBody("ProjectMilestone", "MilestoneStatusChangeNew.html", dic);

                        LogData = ", Mail Body : " + Sbody;
                        lm.ErrorLog(Errorlogpath, LogData + " - MilestoneUpdateService" + DateTime.Now.ToLongDateString());

                        SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item.Email, Subject, Sbody);

                    }

                    #region Push Notification
                    foreach (var item in mailNotificationUserList)
                    {
                        string headurl = Request.RequestUri.AbsoluteUri;
                        //string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", milestone.MilestoneName);
                        dic.Add("ProjectNo", milestone.ProjectNo + (milestone.JobNo != null ? " - " + milestone.JobNo : ""));
                        dic.Add("StatusName", milestone.StatusName);
                        dic.Add("PreviousStatusName", milestone.PreviousStatusName);
                        dic.Add("UserName", item.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (milestone.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestone.ProjectNo + " - " + milestone.ProjectName + " and Bank " + milestone.JobDesc + " [" + milestone.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestone.ProjectNo + " - " + milestone.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        dic.Add("MessageURL", headurl);

                        string IsNotificationEnable = WebConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = WebConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        StaticUtilities SU = new StaticUtilities();
                        UserDAL UD = new UserDAL();
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "Milestone Status Changed";
                                string NotificationBody = SU.getNotificationBody("ProjectMilestone", "MilestoneStatusChangeNew.txt", dic);

                                try
                                {
                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {

                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }

                                //Insert into NotificationLogId table

                                data.CompanyID = item.CompanyID;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(item.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                data.CreatedBy = item.FirstName + " " + item.LastName;
                                data.DeviceType = item.DeviceType;
                                data.ModuleID = 28;
                                var Notify = UD.InsertNotification(data);
                            }
                        }
                    }
                    #endregion
                }

                #endregion

                CD.Status = (obj == true) ? "1" : "0";
                CD.Message = (obj == true) ? "Successfully Data Updated..." : "Update Failed...";
                CD.Data = obj;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message.ToString();
                //ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                //Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MilestoneUpdateDataController");
            }
            return CD;
        }

        public List<UserEntity> GetProjectAssignedUserListForMilestoneEmail(string projectNo)
        {
            MilestoneDAL MD = new MilestoneDAL();
            List<UserEntity> user = new List<UserEntity>();
            user = (from row in MD.GetProjectAssignedUserListForMilestoneEmail(projectNo)
                    select new UserEntity
                    {
                        Email = Convert.ToString(row["Email"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        DeviceID = Convert.ToString(row["DeviceID"]),
                        DeviceType = Convert.ToString(row["DeviceType"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                    }).ToList();
            return user;
        }
    }
}
