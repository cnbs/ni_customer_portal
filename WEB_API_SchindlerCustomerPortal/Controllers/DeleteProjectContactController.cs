﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class DeleteProjectContactController : ApiController
    {
        [HttpPost]
        public CommonData DeleteProjectContact(ProjectContactEntity model)
        {
            CommonData CD = new CommonData();

            ProjectContactDAL PCD = new ProjectContactDAL();

            model.UpdatedBy = model.UserID.ToString();

            #region "Audit Trail"
            //if(model.appAuditID == 0)
            //{
            //    model.appAuditID = 0;
            //}

            model.ApplicationOperation = "ProjectManagement_DeleteProjectContact";

            #endregion

            try
            {
                var user = PCD.DeleteProjectContact(model);

                if(user == true)
                {
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = "Successfully Data Deleted...";
                }
                else
                {
                    CD.Status = "1";
                    CD.Message = "Success";
                    CD.Data = "Record not exists...";
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Failure...";
                CD.Data = "Something went wrong...";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteProjectContactController");
            }
            return CD;
        }
    }
}
