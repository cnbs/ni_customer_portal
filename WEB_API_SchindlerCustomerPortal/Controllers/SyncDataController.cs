﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class SyncDataController : ApiController
    {

        [HttpPost]
        public object UserLogin([FromBody]SyncData data)
        {

            var DeviceID = data.DeviceID;
            var UserID = data.UserID;
            string[] blankArray = new string[0];
            var Timestamp = data.Timestamp.ToString();

            DataSet mainds = new DataSet();
            DataSet tempds = SyncDataModule.GetCampaignByCompanyID(data.CampignCompanyID, UserID, Timestamp);
            try
            {
                tempds.Tables["CampaignDetailList"].Columns.Add("Contacts", typeof(object));

                int[] campaignid = new int[tempds.Tables["CampaignDetailList"].Rows.Count];

                DataTable table1 = mainds.Tables.Add("Campaigns");
                mainds.Tables["Campaigns"].Columns.Add("CampaignID", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignName", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignDescription", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignParentID", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignCompanyID", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignStartDate", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignEndDate", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignCode", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignCost", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("CampaignType", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("IsDeleted", typeof(string));
                mainds.Tables["Campaigns"].Columns.Add("Contacts", typeof(object));
                mainds.Tables["Campaigns"].Columns.Add("Assets", typeof(object));

                for (int k = 0; k < tempds.Tables["CampaignDetailList"].Rows.Count; k++)
                {
                    DataRow newdr = mainds.Tables["Campaigns"].NewRow();
                    newdr["CampaignID"] = tempds.Tables["CampaignDetailList"].Rows[k]["ID"].ToString();
                    newdr["CampaignName"] = tempds.Tables["CampaignDetailList"].Rows[k]["Name"].ToString();
                    newdr["CampaignDescription"] = tempds.Tables["CampaignDetailList"].Rows[k]["Description"].ToString();
                    newdr["CampaignParentID"] = tempds.Tables["CampaignDetailList"].Rows[k]["ParentID"].ToString();
                    newdr["CampaignCompanyID"] = tempds.Tables["CampaignDetailList"].Rows[k]["CompanyID"].ToString();
                    newdr["CampaignStartDate"] = tempds.Tables["CampaignDetailList"].Rows[k]["StartDate"].ToString();
                    newdr["CampaignEndDate"] = tempds.Tables["CampaignDetailList"].Rows[k]["EndDate"].ToString();
                    newdr["CampaignCode"] = tempds.Tables["CampaignDetailList"].Rows[k]["Code"].ToString();
                    newdr["CampaignCost"] = tempds.Tables["CampaignDetailList"].Rows[k]["Cost"].ToString();
                    newdr["CampaignType"] = tempds.Tables["CampaignDetailList"].Rows[k]["CampaignTypeName"].ToString();
                    newdr["IsDeleted"] = tempds.Tables["CampaignDetailList"].Rows[k]["IsDeleted"].ToString();
                    mainds.Tables["Campaigns"].Rows.Add(newdr);

                }


                for (int i = 0; i < mainds.Tables["Campaigns"].Rows.Count; i++)
                {

                    DataTable table2 = tempds.Tables.Add("Contacts" + i);
                    table2.Columns.Add("ContactID", typeof(string));
                    table2.Columns.Add("ContactProfilePic", typeof(string));
                    table2.Columns.Add("FirstName", typeof(string));
                    table2.Columns.Add("LastName", typeof(string));
                    table2.Columns.Add("Title", typeof(string));
                    table2.Columns.Add("PhoneNumber1", typeof(string));
                    table2.Columns.Add("PhoneNumber2", typeof(string));
                    table2.Columns.Add("Email", typeof(string));
                    table2.Columns.Add("CompanyName", typeof(string));
                    table2.Columns.Add("Address1", typeof(string));
                    table2.Columns.Add("Address2", typeof(string));
                    table2.Columns.Add("City", typeof(string));
                    table2.Columns.Add("State", typeof(string));
                    table2.Columns.Add("Zip", typeof(string));
                    table2.Columns.Add("WebsiteAddress", typeof(string));
                    table2.Columns.Add("CampaignID", typeof(string));
                    table2.Columns.Add("Notes", typeof(string));
                    table2.Columns.Add("IsDeleted", typeof(string));
                    table2.Columns.Add("BusinessCardPic", typeof(string));

                    campaignid[i] = Convert.ToInt32(mainds.Tables["Campaigns"].Rows[i]["CampaignID"]);
                    DataSet tempds1 = new DataSet();

                    

                    if (tempds.Tables["CampaignDetailList"].Rows[i]["IsDeleted"].ToString() == "0" )
                    {
                        if ((tempds.Tables["CampaignDetailList"].Rows[i]["AssociateCreateDateStatus"].ToString() == "1") & (tempds.Tables["CampaignDetailList"].Rows[i]["AssociateUpdateDateStatus"].ToString() == "0"))
                        {
                            tempds1 = SyncDataModule.GetContactsByCampaignID(campaignid[i], Timestamp);
                        }
                        else
                        {
                            tempds1 = SyncDataModule.GetContactsByCampaignID(campaignid[i], "-1");
                        }
                         
                    }

                    if (tempds1.Tables.Count > 0)
                    {
                        if (tempds1.Tables["CampaignContactList"].Rows.Count > 0)
                        {


                            for (int j = 0; j < tempds1.Tables["CampaignContactList"].Rows.Count; j++)
                            {


                                DataRow newdr = table2.NewRow();
                                newdr["ContactID"] = tempds1.Tables["CampaignContactList"].Rows[j]["ContactID"].ToString();
                                newdr["ContactProfilePic"] = tempds1.Tables["CampaignContactList"].Rows[j]["ContactProfilePic"].ToString();
                                newdr["FirstName"] = tempds1.Tables["CampaignContactList"].Rows[j]["FirstName"].ToString();
                                newdr["LastName"] = tempds1.Tables["CampaignContactList"].Rows[j]["LastName"].ToString();
                                newdr["Title"] = tempds1.Tables["CampaignContactList"].Rows[j]["Title"].ToString();
                                newdr["PhoneNumber1"] = tempds1.Tables["CampaignContactList"].Rows[j]["PhoneNumber1"].ToString();
                                newdr["PhoneNumber2"] = tempds1.Tables["CampaignContactList"].Rows[j]["PhoneNumber2"].ToString();
                                newdr["Email"] = tempds1.Tables["CampaignContactList"].Rows[j]["Email"].ToString();
                                newdr["CompanyName"] = tempds1.Tables["CampaignContactList"].Rows[j]["CompanyName"].ToString();
                                newdr["Address1"] = tempds1.Tables["CampaignContactList"].Rows[j]["Address1"].ToString();
                                newdr["Address2"] = tempds1.Tables["CampaignContactList"].Rows[j]["Address2"].ToString();
                                newdr["City"] = tempds1.Tables["CampaignContactList"].Rows[j]["City"].ToString();
                                newdr["State"] = tempds1.Tables["CampaignContactList"].Rows[j]["State"].ToString();
                                newdr["Zip"] = tempds1.Tables["CampaignContactList"].Rows[j]["Zip"].ToString();
                                newdr["WebsiteAddress"] = tempds1.Tables["CampaignContactList"].Rows[j]["WebsiteAddress"].ToString();
                                newdr["CampaignID"] = tempds1.Tables["CampaignContactList"].Rows[j]["CampaignID"].ToString();
                                newdr["Notes"] = tempds1.Tables["CampaignContactList"].Rows[j]["Notes"].ToString();
                                newdr["IsDeleted"] = tempds1.Tables["CampaignContactList"].Rows[j]["IsDeleted"].ToString();
                                newdr["BusinessCardPic"] = tempds1.Tables["CampaignContactList"].Rows[j]["BusinessCardPic"].ToString();
                                table2.Rows.Add(newdr);


                            }

                            mainds.Tables["Campaigns"].Rows[i]["Contacts"] = table2.Copy();
                        }
                        else
                        {
                            mainds.Tables["Campaigns"].Rows[i]["Contacts"] = blankArray;
                        }
                    }
                    else
                    {
                        mainds.Tables["Campaigns"].Rows[i]["Contacts"] = blankArray;
                    }
                    

                }



                for (int i = 0; i < mainds.Tables["Campaigns"].Rows.Count; i++)
                {
                    DataTable table3 = tempds.Tables.Add("Assets" + i);

                    table3.Columns.Add("ContentID", typeof(string));
                    table3.Columns.Add("CampaignID", typeof(string));
                    table3.Columns.Add("CompanyID", typeof(string));
                    table3.Columns.Add("FileName", typeof(string));
                    table3.Columns.Add("FileDescription", typeof(string));
                    table3.Columns.Add("FileKeyword", typeof(string));
                    table3.Columns.Add("FileCreatedDate", typeof(string));
                    table3.Columns.Add("FileModifiedDate", typeof(string));
                    table3.Columns.Add("IsDeleted", typeof(string));

                    campaignid[i] = Convert.ToInt32(mainds.Tables["Campaigns"].Rows[i]["CampaignID"]);
                    DataSet tempds2 = new DataSet();

                    if (tempds.Tables["CampaignDetailList"].Rows[i]["IsDeleted"].ToString() == "0")
                    {
                        if ((tempds.Tables["CampaignDetailList"].Rows[i]["AssociateCreateDateStatus"].ToString() == "1") & (tempds.Tables["CampaignDetailList"].Rows[i]["AssociateUpdateDateStatus"].ToString() == "0"))
                        {
                            tempds2 = SyncDataModule.GetAssetsByCampaignID(campaignid[i], data.CampignCompanyID,Timestamp);
                        }
                        else
                        {
                            tempds2 = SyncDataModule.GetAssetsByCampaignID(campaignid[i], data.CampignCompanyID, "-1");
                        }

                        
                    }

                    if (tempds2.Tables.Count > 0)
                    {
                        if (tempds2.Tables["CampaignAssetsList"].Rows.Count > 0)
                        {


                            for (int j = 0; j < tempds2.Tables["CampaignAssetsList"].Rows.Count; j++)
                            {
                                DataRow newdr = table3.NewRow();
                                newdr["ContentID"] = Convert.ToInt32(tempds2.Tables["CampaignAssetsList"].Rows[j]["ContentID"].ToString());
                                newdr["CampaignID"] = Convert.ToInt32(tempds2.Tables["CampaignAssetsList"].Rows[j]["CampaignID"].ToString());
                                newdr["CompanyID"] = Convert.ToInt32(tempds2.Tables["CampaignAssetsList"].Rows[j]["CompanyID"].ToString());
                                newdr["FileName"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["FileName"].ToString();
                                newdr["FileDescription"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["FileDescription"].ToString();
                                newdr["FileKeyword"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["FileKeyword"].ToString();
                                newdr["FileCreatedDate"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["FileCreatedDate"].ToString();
                                newdr["FileModifiedDate"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["FileUpdatedDate"].ToString();
                                newdr["IsDeleted"] = tempds2.Tables["CampaignAssetsList"].Rows[j]["IsDeleted"].ToString();
                                table3.Rows.Add(newdr);

                            }
                            mainds.Tables["Campaigns"].Rows[i]["Assets"] = table3.Copy();

                        }
                        else
                        {
                            mainds.Tables["Campaigns"].Rows[i]["Assets"] = blankArray;
                        }
                    }
                    else
                    {
                        mainds.Tables["Campaigns"].Rows[i]["Assets"] = blankArray;
                    }
                    

                }

                DataTable tabletmstmp = mainds.Tables.Add("TimeStamp");
                mainds.Tables["TimeStamp"].Columns.Add("Timestamp", typeof(string));
                DataRow newdr12 = mainds.Tables["TimeStamp"].NewRow();
                newdr12["Timestamp"] = DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss.fff");
                mainds.Tables["TimeStamp"].Rows.Add(newdr12);


                bool result = APIAudit.APIAuditRecord("api/SyncData", "SyncData - Success", UserID, DeviceID, "SyncData");

                return mainds;

            }
            catch (System.Exception e)
            {

                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message);



                DataSet tempds1 = new DataSet();
                DataTable t = new DataTable();
                tempds1.Tables.Add(t);
                tempds1.Tables[0].TableName = "CampaignDetails";

                t.Columns.Add("Status");
                t.Columns.Add("Message");

                DataRow dr = t.NewRow();

                dr["Status"] = "0";
                dr["Message"] = "No New Data Found!";

                t.Rows.Add(dr);

                bool result = APIAudit.APIAuditRecord("api/SyncData", "SyncData - Failed", UserID, DeviceID, "SyncData");

                return tempds1;

            }
        }

    }
}
