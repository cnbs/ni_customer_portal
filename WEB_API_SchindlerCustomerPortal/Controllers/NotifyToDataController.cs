﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class NotifyToDataController : ApiController
    {
        [HttpPost]
        public CommonData NotifyToMailList(DataTableModel model)
        {
            CommonData CD = new CommonData();
            try
            {
                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = "";
                }

                if (model.SearchTerm.Contains(","))
                {
                    string result = model.SearchTerm.Split(',')[model.SearchTerm.Split(',').Length - 1];
                    if (!string.IsNullOrEmpty(result))
                    {
                        model.SearchTerm = result.Trim();
                    }
                }

                ManageProjectDAL MPD = new ManageProjectDAL();
                var userList = new List<UserEntity>();

                userList = (from row in MPD.GetNotifyToMailID(model.CompanyID, model.SearchTerm)
                            select new UserEntity
                            {
                                Email = row["Email"].ToString(),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = row["FirstName"].ToString(),
                                LastName = row["LastName"].ToString(),
                                //CompanyID = Convert.ToInt32(row["CompanyID"]),
                                //CompanyName = row["CompanyName"].ToString(),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                Designation = row["DesignationName"].ToString(),
                                DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),
                            }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data = userList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Fail";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NotifyToDataController");
            }
            return CD;
        }
    }
}
