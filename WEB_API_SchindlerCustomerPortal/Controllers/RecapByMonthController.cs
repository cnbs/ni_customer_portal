﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_SchindlerCustomerPortal.Class;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class RecapByMonthController : ApiController
    {
        [HttpPost]
        public CommonData GetRecapByMonth()
        {
            CommonData CD = new CommonData();
            string msg = string.Empty;

            try
            {
                DataSet ds = new DataSet();
                ds = DashboardDataModule.GetRecapDetail();

                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    msg = ReadErrorFromXMLFile.ReadMessage("NoData");
                    CD.Status = "0";
                    CD.Message = msg;
                    CD.Data = null;
                }
                else
                {
                    List<SalesRecapData> SRD = new List<SalesRecapData>();

                    SRD = (from row in ds.Tables[0].AsEnumerable().ToList()
                           select new SalesRecapData
                           {
                               Status = Convert.ToString(row[0]),
                               SalesTradeAndInterCo = Convert.ToString(row[1]),
                               MTDShipments = Convert.ToString(row[2]),
                               OPS_FY = Convert.ToString(row[3]),
                               CurrentMonthBackLog_CRD = Convert.ToString(row[4]),
                               MTD_Booking = Convert.ToString(row[5]),
                               SalesPercOfOPS = Convert.ToString(row[6]),
                               TargetPercOfMTD = Convert.ToString(row[7])
                           }).ToList();

                    msg = ReadErrorFromXMLFile.ReadMessage("Success");
                    CD.Status = "1";
                    CD.Message = msg;
                    CD.Data = SRD;
                }
            }
            catch (Exception e)
            {
                msg = ReadErrorFromXMLFile.ReadMessage("SomethingWrong");
                CD.Status = "0";
                CD.Message = msg;
                CD.Data = null;

                string ErrorPath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog(ErrorPath, e.Message + " - GetRecapByMonth" + e.InnerException + e.StackTrace + e.Source);
                //result = new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return CD;
        }
    }
}
