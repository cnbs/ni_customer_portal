﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class MilestoneInsertController : ApiController
    {
        [HttpPost]
        public CommonData MilestoneInsert(ProjectMilestoneEntity res)
        {
            CommonData CD = new CommonData();
            try
            {
                MilestoneDAL MD = new MilestoneDAL();
                ProjectMilestoneEntity obj = new ProjectMilestoneEntity();
                res.ApplicationOperation = "ProjectManagement_AddProjectMilestone";
                obj = (from row in MD.MilestoneInsertdata(res)
                       select new ProjectMilestoneEntity
                       {
                           Exist = row["Exist"].ToString(),
                       }).FirstOrDefault();

                CD.Status = (obj.Exist == "1") ? "0" : "1";
                CD.Message = (obj.Exist == "1") ? "Milestone is already assigned..." : "Record saved successfully!";
                CD.Data = obj.Exist;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...";
                CD.Data = ex.Message;
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MilestoneInsertController");
            }
            return CD;
        }
    }
}
