﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetMilestoneTypeListController : ApiController
    {
        [HttpGet]
        public CommonData GetMiletypelist()
        {
            MilestoneDAL MD = new MilestoneDAL();
            CommonData CD = new CommonData();
            try
            {
                List<MilestoneEntity> Miletype = (from row in MD.GetMilestoneTypeList()
                                                  select new MilestoneEntity
                                                  {
                                                      MilestoneId = Convert.ToInt32(row["MilestoneId"]),
                                                      MilestoneTitle = row["Milestone"].ToString(),
                                                  }).Where(a => a.MilestoneTitle.ToLower() != "material ordered").ToList();

                CD.Status = "1";
                CD.Data = Miletype;
                CD.Message = "Success";
            }
            catch (SqlException ex)
            {
                CD.Status = "1";
                CD.Data = ex.Message.ToString();
                CD.Message = "Fail";
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetMilestoneTypeListController");
            }
            return CD;
        }
    }
}
