﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class GetJobSiteReadyDataController : ApiController
    {
        [HttpPost]
        public CommonData GetAllReadyToPullTemplateMasterList(DataTableModel model)
        {
            MilestoneDAL MD = new MilestoneDAL();
            CommonData CD = new CommonData();
            List<ReadyToPullTemplateMaster> projectJobList = new List<ReadyToPullTemplateMaster>();
            try
            {
                projectJobList = (from row in MD.GetAllReadyToPullTemplateMasterList(model)
                                  select new ReadyToPullTemplateMaster
                                  {
                                      JobNo = Convert.ToString(row["JobNo"]),
                                      TemplateId = Convert.ToInt32(row["TemplateId"]),
                                      TemplateName = Convert.ToString(row["TemplateName"]),
                                      ProjectNo = Convert.ToString(row["ProjectNo"]),
                                      Comment = Convert.ToString(row["Comment"]),
                                      Attachment = (string.IsNullOrEmpty(Convert.ToString(row["Attachment"]))) ? "" : 
                                                    WebConfigurationManager.AppSettings["DomainPath"].ToString() + 
                                                    WebConfigurationManager.AppSettings["readyToPullAttachmentsPath"].ToString() + 
                                                    Convert.ToString(row["Attachment"]),
                                      RealAttachment = Convert.ToString(row["RealAttachment"]),
                                      Selected = ((string.IsNullOrEmpty(row["IsDeleted"].ToString())) ? false : true)
                                  }).ToList();

                CD.Status = "1";
                CD.Message = "Success";
                CD.Data= projectJobList;
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something Went Wrong...";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobSiteReadyDataController");
            }
            return CD;
        }
    }
}
