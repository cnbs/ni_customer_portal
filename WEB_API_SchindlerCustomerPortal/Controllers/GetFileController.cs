﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class GetFileController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage GeFiles([FromBody]ContentRepo data)
        {
            string file = "";
            FileStream stream = null;
            HttpResponseMessage result = new HttpResponseMessage();

            try
            {
                string AssetPath = System.Configuration.ConfigurationManager.AppSettings["AssetsPath"].ToString();
                file = AssetPath + "\\" + data.CompanyID + "\\" + data.FileName;

                result = new HttpResponseMessage(HttpStatusCode.OK);
                stream = new FileStream(file, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
               
                return result;

            }
            catch (System.Exception e)
            {
                LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message + " - GetFileControll");
                result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return result;

            }
            finally
            {
                
                
            }
        }

    }
}
