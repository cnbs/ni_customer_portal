﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WEB_API_OmniSalesHub.Models;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.DAL;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.Controllers
{
    public class ForgotPasswordController : ApiController
    {
        [HttpPost]
        public CommonData ForgotPassword(EmailHistory model)
        {
            string From = WebConfigurationManager.AppSettings["AccountMail"].ToString();
            string Subject = "Schindler Password Assistance!";
            CommonData CD = new CommonData();
            var exist = string.Empty;

            try
            {
                var CheckEmailExist = ForgotPwdService(model.EmailTo.ToLower());
                EmailHistory item = new EmailHistory();
                item.EmailTo = model.EmailTo;

                if (CheckEmailExist.Email == "0")
                {
                    CD.Status = "0";
                    CD.Message = "Email Not Exist...";
                    CD.Data = "";
                    return CD;
                }
                else
                {
                    if (CheckEmailExist.AccountStatus == 4 || CheckEmailExist.AccountStatus == 17) // if user status is pending or verified then return
                    {
                        CD.Status = "0";
                        CD.Message = "Email status is pending...";
                        CD.Data = "";
                        return CD;
                    }

                    var FirstName = string.Empty;
                    FirstName = CheckEmailExist.FirstName;

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(WebConfigurationManager.AppSettings["TokenExpireHours"].ToString());
                    int UserID = CheckEmailExist.UserID;
                    SetTokenService(token, UserID, ExpToken);
                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = WebConfigurationManager.AppSettings["BaseURL"].ToString();

                    ActivationUrl = HttpUtility.UrlPathEncode(Convert.ToString("" + headurl + "/Login/ResetPassword?TokenID=" + token + ""));

                    #region Email Send

                    string Sbody = null;
                    StaticUtilities SU = new StaticUtilities();
                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", FirstName);
                    dic.Add("UserEmailID", item.EmailTo);
                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = SU.getEmailBody("User", "ResetPasswordNew.html", dic);

                    SendEmail.Send(WebConfigurationManager.AppSettings["NoReplyEmail"].ToString(), item.EmailTo, Subject, Sbody);

                    #endregion

                    DataSet tempds = new DataSet();
                    //bool update = SendEmail.Send(From, item.EmailTo, Subject, body);

                    bool update = true;

                    if (update == true)
                    {
                        CD.Status = "1";
                        CD.Message = "Your password reset link has been sent to your registered email address!";
                        CD.Data = "";
                        return CD;
                    }
                    else
                    {
                        CD.Status = "0";
                        CD.Message = "There is some issue with mail send, please contact administrator.";
                        CD.Data = "";
                        return CD;
                    }
                }
            }
            catch (Exception ex)
            {
                CD.Status = "0";
                CD.Message = "Something went wrong...!";
                CD.Data = ex.Message.ToString();
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ForgotPasswordController");
                return CD;
            }
        }

        public UserEntity ForgotPwdService(string Email)
        {
            UserDAL UD = new UserDAL();
            try
            {

                UserEntity user = (from row in UD.CheckForgotPwdEmail(Email)
                                   select new UserEntity
                                   {
                                       Email = row["Email"].ToString(),
                                       FirstName = row["FirstName"].ToString(),
                                       UserID = Convert.ToInt32(row["UserID"]),
                                       AccountStatus = Convert.ToInt32(row["AccountStatus"])
                                   }).FirstOrDefault();

                return user;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ForgotPwdService");
                throw ex;
            }
        }

        public void SetTokenService(string Token, int UserID, int ExpireToken)
        {
            UserDAL UD = new UserDAL();
            try
            {
                bool CheckTokenSet = UD.SetTokenOnEmail(Token, UserID, ExpireToken);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SetTokenService");
            }
        }
    }
}
