﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WEB_API_OmniSalesHub.Class;
using WEB_API_OmniSalesHub.Models;
using Ionic.Zip;
using System.Data;
using System.IO.Compression;

namespace WEB_API_OmniSalesHub.Controllers
{
    public class GetZipFileController : ApiController
    {
        // Content type for our body
        private static readonly MediaTypeHeaderValue _mediaType = MediaTypeHeaderValue.Parse("application/octet-stream");

        [HttpPost]
        public HttpResponseMessage GeZipFiles([FromBody]ContentRepo data)
        {
            string file = "";
            string zipfilePath = "";
            //FileStream stream = null;
            LoginModule lm = new LoginModule();
            double FreeAvailableSpace = data.AvailableSize * 1000000; // AvailableSize converted into bytes
            double TotalFileSize = 0;

            DataSet tempds = SyncDataModule.GetAssetNameByAssetID(data.ContentIDList);
            HttpResponseMessage result = new HttpResponseMessage();
            
            string AssetPath = System.Configuration.ConfigurationManager.AppSettings["AssetsPath"].ToString();

            try
            {
                using (var TempzipFile = new ZipFile())
                {
                    for (int i = 0; i < tempds.Tables["CampaignAssetsList"].Rows.Count; i++)
                    {
                        file = AssetPath + "\\" + data.CompanyID + "\\" + tempds.Tables["CampaignAssetsList"].Rows[i]["FileName"];
                        TempzipFile.AddFile(file, "");

                        FileInfo info = new FileInfo(file);
                        TotalFileSize += info.Length; // for file size
                    }

                    TempzipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.Level7;
                    zipfilePath = AssetPath + "\\" + data.CompanyID + "\\" + data.FileName + ".zip";

                    if (File.Exists(zipfilePath)) // check zipfile is exist or not
                    {
                        File.Delete(zipfilePath); // delete zipfile
                    }

                    TempzipFile.Save(zipfilePath); // save zipfile at this location

                    FileInfo zipinfo = new FileInfo(zipfilePath); // zipfile path to get size of zipfile
                    TotalFileSize += zipinfo.Length;

                    if (FreeAvailableSpace > TotalFileSize)
                    {
                        byte[] _content = File.ReadAllBytes(zipfilePath);
                        MemoryStream memStream = new MemoryStream(_content);

                        if (Request.Headers.Range != null)
                        {
                            //LoginModule lm = new LoginModule();
                            lm.CreateLogFiles();
                            lm.ErrorLog("D:\\Projects\\ErrorLog\\", "Range " + this.Request.Headers.Range.Ranges.FirstOrDefault() + " - GetZipFile API Controller ");

                            try
                            {
                                HttpResponseMessage partialResponse = Request.CreateResponse(HttpStatusCode.PartialContent);
                                partialResponse.Content = new ByteRangeStreamContent(memStream, Request.Headers.Range, _mediaType);
                                return partialResponse;
                            }
                            catch (InvalidByteRangeException invalidByteRangeException)
                            {
                                return Request.CreateErrorResponse(invalidByteRangeException);
                            }
                        }
                        else
                        {
                            // If it is not a range request we just send the whole thing as normal
                            HttpResponseMessage fullResponse = Request.CreateResponse(HttpStatusCode.OK);
                            fullResponse.Content = new StreamContent(memStream);
                            fullResponse.Content.Headers.ContentType = _mediaType;
                            return fullResponse;
                        }
                    }
                    else
                    {
                        const double OneKb = 1024;
                        const double OneMb = OneKb * 1024;
                        double requiredSpace = Math.Round((TotalFileSize - FreeAvailableSpace) / OneMb); // round up required sapce

                        if (requiredSpace == 0.0) // sapce between 0 to 0.5 MB then its calculated as 1 MB
                        {
                            requiredSpace = 1;
                        }
                        var resp = new HttpResponseMessage()
                        {
                            Content = new StringContent("{\"Status\":\"0\",\"Message\":\"Insufficient Storage. " + requiredSpace + " MB Space Required.\"}")
                        };
                        resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        return resp;
                    }
                }
            }
            catch (System.Exception e)
            {
                //LoginModule lm = new LoginModule();
                lm.CreateLogFiles();
                lm.ErrorLog("D:\\Projects\\ErrorLog\\", e.Message + " - GetZipFileController" + e.InnerException + e.StackTrace + e.Source);
                result = new HttpResponseMessage(HttpStatusCode.NoContent);
                return result;
            }
            finally
            {
                
            }
        }    
    }
}
