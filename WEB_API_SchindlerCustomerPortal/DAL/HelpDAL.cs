﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class HelpDAL
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> GetHelp()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetHelp);
                ds.Tables[0].TableName = "HelpMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetHelp");
                throw ex;
            }
            List<DataRow> helpData = ds.Tables[0].AsEnumerable().ToList();
            return helpData;
        }
    }
}