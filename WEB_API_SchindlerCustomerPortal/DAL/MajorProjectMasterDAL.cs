using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using DAL;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
//using BL;
/// <summary>
/// Summary description for MajorProjectMasterEntity
/// </summary>
namespace WEB_API_SchindlerCustomerPortal.DAL
{

    public class MajorProjectMasterDAL : BaseDAL
    {     

        #region Public Constructors
        public MajorProjectMasterDAL()
        {
            //
            // TODO: Add constructor logic here
            //

        }
        #endregion
        
        #region Public Methods
        public int Insert(MajorProjectMasterEntity objMajorProjectMasterDL)
        {
            DataSet ds = new DataSet();
            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_Insert";
            SqlParameter Sqlparam;

            Sqlparam = new SqlParameter("@CompanyID", SqlDbType.Int);
            Sqlparam.Value = objMajorProjectMasterDL.CompanyID;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectName", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectName;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectShortName", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectShortName;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectAdditionalInfo", SqlDbType.NText);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectAdditionalInfo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectConfidentialInfo", SqlDbType.NText);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectConfidentialInfo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectMgrPhone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectMgrPhone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectMgrEmail", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectMgrEmail;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ContractorAltPhone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ContractorAltPhone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@StreetAddress", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.StreetAddress;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@StateCode", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.StateCode;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@City", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.City;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ZipCode;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Name", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Name;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Email", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Email;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Designation", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Designation;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Name", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Name;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Email", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Email;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Designation", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Designation;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectLogo", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectLogo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.CreatedBy;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.CreatedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {               
                Sqlparam.Value = objMajorProjectMasterDL.CreatedDate;
            }          
            Sqlcomm.Parameters.Add(Sqlparam);
            

            Sqlparam = new SqlParameter("@UpdatedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.UpdatedBy;
            Sqlcomm.Parameters.Add(Sqlparam);


            Sqlparam = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.UpdatedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {
                Sqlparam.Value = objMajorProjectMasterDL.UpdatedDate;
            }
            Sqlcomm.Parameters.Add(Sqlparam);


            Sqlparam = new SqlParameter("@IsDeleted", SqlDbType.Bit);
            Sqlparam.Value = objMajorProjectMasterDL.IsDeleted;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@IsPublished", SqlDbType.Bit);
            Sqlparam.Value = objMajorProjectMasterDL.IsPublished;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@PublishedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.PublishedBy;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@PublishedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.PublishedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {
                Sqlparam.Value = objMajorProjectMasterDL.PublishedDate;
            }
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ID", SqlDbType.Int);
            Sqlparam.Direction = ParameterDirection.ReturnValue;
            Sqlcomm.Parameters.Add(Sqlparam);

            int result = base.ExecuteNoneQuery(Sqlcomm);

            if (!Convert.IsDBNull(Sqlcomm.Parameters["@ID"]))
                result = Convert.ToInt32(Sqlcomm.Parameters["@ID"].Value);

            //List<DataRow> ResData = ds.Tables[0].AsEnumerable().ToList();
            return result;
        }

        public int Update(MajorProjectMasterEntity objMajorProjectMasterDL)
        {

            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_UpdateByPK";
            SqlParameter Sqlparam;

            Sqlparam = new SqlParameter("@MajorProjectID", SqlDbType.Int);
            Sqlparam.Value = objMajorProjectMasterDL.MajorProjectID;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@CompanyID", SqlDbType.Int);
            Sqlparam.Value = objMajorProjectMasterDL.CompanyID;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectName", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectName;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectShortName", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectShortName;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectAdditionalInfo", SqlDbType.NText);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectAdditionalInfo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectConfidentialInfo", SqlDbType.NText);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectConfidentialInfo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectMgrPhone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectMgrPhone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectMgrEmail", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectMgrEmail;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ContractorAltPhone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ContractorAltPhone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@StreetAddress", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.StreetAddress;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@StateCode", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.StateCode;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@City", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.City;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ZipCode;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Name", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Name;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Email", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Email;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Designation", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Designation;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact1Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact1Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Name", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Name;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Email", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Email;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Designation", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Designation;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@Contact2Phone", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.Contact2Phone;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@ProjectLogo", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.ProjectLogo;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.CreatedBy;
            Sqlcomm.Parameters.Add(Sqlparam);

            //Sqlparam = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            //Sqlparam.Value = objMajorProjectMasterDL.CreatedDate;
            //Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.CreatedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {
                Sqlparam.Value = objMajorProjectMasterDL.CreatedDate;
            }
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@UpdatedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.UpdatedBy;
            Sqlcomm.Parameters.Add(Sqlparam);

            //Sqlparam = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            //Sqlparam.Value = objMajorProjectMasterDL.UpdatedDate;
            //Sqlcomm.Parameters.Add(Sqlparam);
            Sqlparam = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.UpdatedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {
                Sqlparam.Value = objMajorProjectMasterDL.UpdatedDate;
            }
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@IsDeleted", SqlDbType.Bit);
            Sqlparam.Value = objMajorProjectMasterDL.IsDeleted;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@IsPublished", SqlDbType.Bit);
            Sqlparam.Value = objMajorProjectMasterDL.IsPublished;
            Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@PublishedBy", SqlDbType.VarChar);
            Sqlparam.Value = objMajorProjectMasterDL.PublishedBy;
            Sqlcomm.Parameters.Add(Sqlparam);

            //Sqlparam = new SqlParameter("@PublishedDate", SqlDbType.DateTime);
            //Sqlparam.Value = objMajorProjectMasterDL.PublishedDate;
            //Sqlcomm.Parameters.Add(Sqlparam);

            Sqlparam = new SqlParameter("@PublishedDate", SqlDbType.DateTime);
            if (Convert.ToString(objMajorProjectMasterDL.PublishedDate) == "1/1/0001 12:00:00 AM")
            {
                Sqlparam.Value = DBNull.Value;
            }
            else
            {
                Sqlparam.Value = objMajorProjectMasterDL.PublishedDate;
            }
            Sqlcomm.Parameters.Add(Sqlparam);


            Sqlparam = new SqlParameter("@ErrorCode", SqlDbType.Int);
            Sqlparam.Direction = ParameterDirection.ReturnValue;
            Sqlcomm.Parameters.Add(Sqlparam);

            int result = base.ExecuteNoneQuery(Sqlcomm);

            if (!Convert.IsDBNull(Sqlcomm.Parameters["@ErrorCode"]))
                result = Convert.ToInt32(Sqlcomm.Parameters["@ErrorCode"].Value);

            return result;
        }

        public int Delete(MajorProjectMasterEntity objMajorProjectMasterDL)
        {

            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_DeleteByPK";
            SqlParameter Sqlparam;


            Sqlparam = new SqlParameter("@MajorProjectID", SqlDbType.Int);
            Sqlparam.Value = objMajorProjectMasterDL.MajorProjectID;
            Sqlcomm.Parameters.Add(Sqlparam);



            int result = base.ExecuteNoneQuery(Sqlcomm);
            return result;
        }

        public int DeleteAll()
        {

            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_DeleteAll";

            int result = base.ExecuteNoneQuery(Sqlcomm);
            return result;
        }

        public MajorProjectMasterEntity Select(int MajorProjectID)
        {
            MajorProjectMasterEntity objMajorProjectMasterDL = new MajorProjectMasterEntity();
            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_GetByPK";
            SqlParameter Sqlparam;
            
            Sqlparam = new SqlParameter("@MajorProjectID", SqlDbType.Int);
            Sqlparam.Value = MajorProjectID;
            Sqlcomm.Parameters.Add(Sqlparam);
            
            DataSet ds = base.GetDataSet(Sqlcomm);
            DataRow dr = null;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dr = ds.Tables[0].Rows[0];

                if (!Convert.IsDBNull(dr["MajorProjectID"]))
                    objMajorProjectMasterDL.MajorProjectID = Convert.ToInt32(dr["MajorProjectID"]);
                if (!Convert.IsDBNull(dr["CompanyID"]))
                    objMajorProjectMasterDL.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                if (!Convert.IsDBNull(dr["ProjectName"]))
                    objMajorProjectMasterDL.ProjectName = Convert.ToString(dr["ProjectName"]);
                if (!Convert.IsDBNull(dr["ProjectShortName"]))
                    objMajorProjectMasterDL.ProjectShortName = Convert.ToString(dr["ProjectShortName"]);
                if (!Convert.IsDBNull(dr["ProjectAdditionalInfo"]))
                    objMajorProjectMasterDL.ProjectAdditionalInfo = Convert.ToString(dr["ProjectAdditionalInfo"]);
                if (!Convert.IsDBNull(dr["ProjectConfidentialInfo"]))
                    objMajorProjectMasterDL.ProjectConfidentialInfo = Convert.ToString(dr["ProjectConfidentialInfo"]);
                if (!Convert.IsDBNull(dr["ProjectMgrPhone"]))
                    objMajorProjectMasterDL.ProjectMgrPhone = Convert.ToString(dr["ProjectMgrPhone"]);
                if (!Convert.IsDBNull(dr["ProjectMgrEmail"]))
                    objMajorProjectMasterDL.ProjectMgrEmail = Convert.ToString(dr["ProjectMgrEmail"]);
                if (!Convert.IsDBNull(dr["ContractorAltPhone"]))
                    objMajorProjectMasterDL.ContractorAltPhone = Convert.ToString(dr["ContractorAltPhone"]);
                if (!Convert.IsDBNull(dr["StreetAddress"]))
                    objMajorProjectMasterDL.StreetAddress = Convert.ToString(dr["StreetAddress"]);
                if (!Convert.IsDBNull(dr["StateCode"]))
                    objMajorProjectMasterDL.StateCode = Convert.ToString(dr["StateCode"]);
                if (!Convert.IsDBNull(dr["City"]))
                    objMajorProjectMasterDL.City = Convert.ToString(dr["City"]);
                if (!Convert.IsDBNull(dr["ZipCode"]))
                    objMajorProjectMasterDL.ZipCode = Convert.ToString(dr["ZipCode"]);
                if (!Convert.IsDBNull(dr["Phone"]))
                    objMajorProjectMasterDL.Phone = Convert.ToString(dr["Phone"]);
                if (!Convert.IsDBNull(dr["Contact1Name"]))
                    objMajorProjectMasterDL.Contact1Name = Convert.ToString(dr["Contact1Name"]);
                if (!Convert.IsDBNull(dr["Contact1Email"]))
                    objMajorProjectMasterDL.Contact1Email = Convert.ToString(dr["Contact1Email"]);
                if (!Convert.IsDBNull(dr["Contact1Designation"]))
                    objMajorProjectMasterDL.Contact1Designation = Convert.ToString(dr["Contact1Designation"]);
                if (!Convert.IsDBNull(dr["Contact1Phone"]))
                    objMajorProjectMasterDL.Contact1Phone = Convert.ToString(dr["Contact1Phone"]);
                if (!Convert.IsDBNull(dr["Contact2Name"]))
                    objMajorProjectMasterDL.Contact2Name = Convert.ToString(dr["Contact2Name"]);
                if (!Convert.IsDBNull(dr["Contact2Email"]))
                    objMajorProjectMasterDL.Contact2Email = Convert.ToString(dr["Contact2Email"]);
                if (!Convert.IsDBNull(dr["Contact2Designation"]))
                    objMajorProjectMasterDL.Contact2Designation = Convert.ToString(dr["Contact2Designation"]);
                if (!Convert.IsDBNull(dr["Contact2Phone"]))
                    objMajorProjectMasterDL.Contact2Phone = Convert.ToString(dr["Contact2Phone"]);
                if (!Convert.IsDBNull(dr["ProjectLogo"]))
                    objMajorProjectMasterDL.ProjectLogo = Convert.ToString(dr["ProjectLogo"]);
                if (!Convert.IsDBNull(dr["CreatedBy"]))
                    objMajorProjectMasterDL.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                if (!Convert.IsDBNull(dr["CreatedDate"]))
                    objMajorProjectMasterDL.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                if (!Convert.IsDBNull(dr["UpdatedBy"]))
                    objMajorProjectMasterDL.UpdatedBy = Convert.ToString(dr["UpdatedBy"]);
                if (!Convert.IsDBNull(dr["UpdatedDate"]))
                    objMajorProjectMasterDL.UpdatedDate = Convert.ToDateTime(dr["UpdatedDate"]);
                if (!Convert.IsDBNull(dr["IsDeleted"]))
                    objMajorProjectMasterDL.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                if (!Convert.IsDBNull(dr["IsPublished"]))
                    objMajorProjectMasterDL.IsPublished = Convert.ToBoolean(dr["IsPublished"]);
                if (!Convert.IsDBNull(dr["PublishedBy"]))
                    objMajorProjectMasterDL.PublishedBy = Convert.ToString(dr["PublishedBy"]);
                if (!Convert.IsDBNull(dr["PublishedDate"]))
                    objMajorProjectMasterDL.PublishedDate = Convert.ToDateTime(dr["PublishedDate"]);
            }

            return objMajorProjectMasterDL;
        }

        public ArrayList SelectAll1()
        {

            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_GetAll";

            DataSet ds = base.GetDataSet(Sqlcomm);
            DataTable dt = null;
            ArrayList arrMajorProjectMasterDL = new ArrayList();
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MajorProjectMasterEntity objMajorProjectMasterDL = new MajorProjectMasterEntity();
                    if (!Convert.IsDBNull(dr["MajorProjectID"]))
                        objMajorProjectMasterDL.MajorProjectID = Convert.ToInt32(dr["MajorProjectID"]);
                    if (!Convert.IsDBNull(dr["CompanyID"]))
                        objMajorProjectMasterDL.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    if (!Convert.IsDBNull(dr["ProjectName"]))
                        objMajorProjectMasterDL.ProjectName = Convert.ToString(dr["ProjectName"]);
                    if (!Convert.IsDBNull(dr["ProjectShortName"]))
                        objMajorProjectMasterDL.ProjectShortName = Convert.ToString(dr["ProjectShortName"]);
                    if (!Convert.IsDBNull(dr["ProjectAdditionalInfo"]))
                        objMajorProjectMasterDL.ProjectAdditionalInfo = Convert.ToString(dr["ProjectAdditionalInfo"]);
                    if (!Convert.IsDBNull(dr["ProjectConfidentialInfo"]))
                        objMajorProjectMasterDL.ProjectConfidentialInfo = Convert.ToString(dr["ProjectConfidentialInfo"]);
                    if (!Convert.IsDBNull(dr["ProjectMgrPhone"]))
                        objMajorProjectMasterDL.ProjectMgrPhone = Convert.ToString(dr["ProjectMgrPhone"]);
                    if (!Convert.IsDBNull(dr["ProjectMgrEmail"]))
                        objMajorProjectMasterDL.ProjectMgrEmail = Convert.ToString(dr["ProjectMgrEmail"]);
                    if (!Convert.IsDBNull(dr["ContractorAltPhone"]))
                        objMajorProjectMasterDL.ContractorAltPhone = Convert.ToString(dr["ContractorAltPhone"]);
                    if (!Convert.IsDBNull(dr["StreetAddress"]))
                        objMajorProjectMasterDL.StreetAddress = Convert.ToString(dr["StreetAddress"]);
                    if (!Convert.IsDBNull(dr["StateCode"]))
                        objMajorProjectMasterDL.StateCode = Convert.ToString(dr["StateCode"]);
                    if (!Convert.IsDBNull(dr["City"]))
                        objMajorProjectMasterDL.City = Convert.ToString(dr["City"]);
                    if (!Convert.IsDBNull(dr["ZipCode"]))
                        objMajorProjectMasterDL.ZipCode = Convert.ToString(dr["ZipCode"]);
                    if (!Convert.IsDBNull(dr["Phone"]))
                        objMajorProjectMasterDL.Phone = Convert.ToString(dr["Phone"]);
                    if (!Convert.IsDBNull(dr["Contact1Name"]))
                        objMajorProjectMasterDL.Contact1Name = Convert.ToString(dr["Contact1Name"]);
                    if (!Convert.IsDBNull(dr["Contact1Email"]))
                        objMajorProjectMasterDL.Contact1Email = Convert.ToString(dr["Contact1Email"]);
                    if (!Convert.IsDBNull(dr["Contact1Designation"]))
                        objMajorProjectMasterDL.Contact1Designation = Convert.ToString(dr["Contact1Designation"]);
                    if (!Convert.IsDBNull(dr["Contact1Phone"]))
                        objMajorProjectMasterDL.Contact1Phone = Convert.ToString(dr["Contact1Phone"]);
                    if (!Convert.IsDBNull(dr["Contact2Name"]))
                        objMajorProjectMasterDL.Contact2Name = Convert.ToString(dr["Contact2Name"]);
                    if (!Convert.IsDBNull(dr["Contact2Email"]))
                        objMajorProjectMasterDL.Contact2Email = Convert.ToString(dr["Contact2Email"]);
                    if (!Convert.IsDBNull(dr["Contact2Designation"]))
                        objMajorProjectMasterDL.Contact2Designation = Convert.ToString(dr["Contact2Designation"]);
                    if (!Convert.IsDBNull(dr["Contact2Phone"]))
                        objMajorProjectMasterDL.Contact2Phone = Convert.ToString(dr["Contact2Phone"]);
                    if (!Convert.IsDBNull(dr["ProjectLogo"]))
                        objMajorProjectMasterDL.ProjectLogo = Convert.ToString(dr["ProjectLogo"]);
                    if (!Convert.IsDBNull(dr["CreatedBy"]))
                        objMajorProjectMasterDL.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    if (!Convert.IsDBNull(dr["CreatedDate"]))
                        objMajorProjectMasterDL.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    if (!Convert.IsDBNull(dr["UpdatedBy"]))
                        objMajorProjectMasterDL.UpdatedBy = Convert.ToString(dr["UpdatedBy"]);
                    if (!Convert.IsDBNull(dr["UpdatedDate"]))
                        objMajorProjectMasterDL.UpdatedDate = Convert.ToDateTime(dr["UpdatedDate"]);
                    if (!Convert.IsDBNull(dr["IsDeleted"]))
                        objMajorProjectMasterDL.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                    if (!Convert.IsDBNull(dr["IsPublished"]))
                        objMajorProjectMasterDL.IsPublished = Convert.ToBoolean(dr["IsPublished"]);
                    if (!Convert.IsDBNull(dr["PublishedBy"]))
                        objMajorProjectMasterDL.PublishedBy = Convert.ToString(dr["PublishedBy"]);
                    if (!Convert.IsDBNull(dr["PublishedDate"]))
                        objMajorProjectMasterDL.PublishedDate = Convert.ToDateTime(dr["PublishedDate"]);
                    arrMajorProjectMasterDL.Add(objMajorProjectMasterDL);
                }
            }
            return arrMajorProjectMasterDL;
        }

        public List<DataRow> SelectAll()
        {

            SqlCommand Sqlcomm = new SqlCommand();
            Sqlcomm.CommandType = CommandType.StoredProcedure;
            Sqlcomm.CommandText = "MajorProjectMaster_GetAll";

            DataSet ds = base.GetDataSet(Sqlcomm);
            DataTable dt = null;

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

            }
            //return dt;
            var majorProjectList = ds.Tables[0].AsEnumerable().ToList();
            return majorProjectList;
        }
        #endregion
        public List<DataRow> SelectByPage(SearchInfoMajorProjectsViewModel obj, ref int totalcount)

        {

            DataAccessLayer dal = new DataAccessLayer();

            DataSet ds = new DataSet();

            SqlParameter[] param = new SqlParameter[10];



            param[0] = new SqlParameter("@SearchTerm", obj.SearchTerm);

            param[1] = new SqlParameter("@PageNumber", obj.PageNumber);

            param[2] = new SqlParameter("@PageSize", obj.PageSize);

            param[3] = new SqlParameter("@SortColumn", obj.SortColumn);

            param[4] = new SqlParameter("@SortDirection", obj.SortDirection);

            param[5] = new SqlParameter("@TotalCount", obj.TotalCount)

            {

                Direction = ParameterDirection.Output

            };

            param[6] = new SqlParameter("@appAuditID", obj.appAuditID);

            param[7] = new SqlParameter("@SessionID", obj.SessionID);

            param[8] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);

            param[9] = new SqlParameter("@UserID", obj.UserID);

            try

            {

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, "sp_MajorProjectMaster_SelectByPage", param);

                ds.Tables[0].TableName = "MajorProjectMasterDetails";

                totalcount = Convert.ToInt32(param[5].Value);

                dal.Close();

            }

            catch (Exception ex)

            {

                ErrorLogClass lm = new ErrorLogClass();

                lm.CreateLogFiles();

                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();

                lm.ErrorLog(Errorlogpath, ex.Message + " - MajorProjectMaster");

                throw ex;

            }

            var majorProjectsList = ds.Tables[0].AsEnumerable().ToList();

            return majorProjectsList;

        }

    }

}
