﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class Dashboard
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> NewlyRegisteredUser(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyID", model.CompanyID);
                param[1] = new SqlParameter("@UserID", model.UserID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserList, param);
                ds.Tables[0].TableName = "UserDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUser");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }
    }
}