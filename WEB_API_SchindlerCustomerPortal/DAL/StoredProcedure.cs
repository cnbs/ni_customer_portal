﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class StoredProcedure
    {
        //public const string LoginProcedureName = "sp_LoginUser_FromMobile";
        public const string LoginProcedureName = "usp_LoginUser_FromMobile_API";
        //public const string ForgotPasswordName = "sp_ForgotPassword_ExistingEmail";
        public const string DynamicMenu = "usp_UserModuleAccess_DynamicMenu_SelectById_API";
        
       
        // Sp updated by arpit patel on 02-March-2018
        public const string DashboardProjectListingSP = "[dbo].[usp_DashboardProjectListing_API]";
        
        public const string AddProjectContactSP = "usp_ProjectContact_Insert_API";
        //public const string ProjectContactByIdSP = "sp_ProjectContacts_SelectByID";
        public const string SelectAllContactsByProjectId = "usp_ProjectContact_SelectByPage_API";
        //public const string GetAllContactsByCompanyId = "[dbo].[sp_ProjectContactByCompanyId_SelectByPage]";
        public const string GetAllContactsByCompanyId = "[dbo].[usp_ProjectContactByCompanyId_SelectByProjectNo_API]";
        //public const string DeleteProjectContact = "sp_SplitProjectContact_Delete";//Audit Trail - Verified
        public const string DeleteProjectContact = "usp_SplitProjectContact_Delete_API";//Audit Trail - Verified
        //public const string UpdateProjectContactSP = "sp_ProjectContact_Update";//Audit Trail - Verified
        public const string UpdateProjectContactSP = "usp_ProjectContact_Update_API";//Audit Trail - Verified
        //public const string ResourceSelect = "sp_ResourceUploadMaster_SelectByPage";//Audit Trail - Verified
        public const string ResourceSelect = "usp_ResourceUploadMaster_SelectByPage_API";//Audit Trail - Verified

        public const string GetProjectandJobData = "usp_GetProjectandJobData";//Audit Trail - Verified
        public const string GetStatusByStatusID = "usp_GetStatusName";

        public const string USP_GetProjectWiseGraphAndDocuments_Select_API = "usp_GetProjectWiseGraphAndDocuments_Select_API";

        public const string UpdateEmailAndPhoneData = "[usp_Dashboard_ProjectContact_Update_API]";

        public const string JobMaster_UpdateBankDescByJobNo = "usp_JobMaster_UpdateBankDescByJobNo_API";


        #region ProjectMasterList

        //public const string ProjectMasterList_SelectByPage = "[dbo].[sp_ProjectMasterList_SelectByPage_AllColumns]"; //Audit Trail - Verified

        public const string ProjectMasterList_SelectByPage = "[dbo].[usp_ProjectMasterList_SelectByPage_AllColumns_API]"; //Audit Trail - Verified
        //public const string ProjectMasterList_SelectByPage = "usp_ProjectMasterList_SelectByPage_AllColumns_API_05082017"; //Audit Trail - Verified
        
        //public const string ProjectMaster_SelectByProjectID = "[dbo].[sp_ProjectMaster_SelectByID]";//Audit Trail - Verified

        //public const string ProjectJobList_SelectByPage = "[dbo].[sp_JobMasterList_SelectByProjectNo_AllColumns]";//Audit Trail - Verified
        public const string ProjectJobList_SelectByPage = "[dbo].[usp_JobMasterList_SelectByProjectNo_AllColumns_API]";//Audit Trail - Verified

        public const string UpdateProjectMasterByID = "[dbo].[usp_ProjectMaster_UpdateByID_API]";
        public const string ProjectJob_SelectByJobNo = "[dbo].[usp_ProjectJob_SelectbyJobNo_API]";
        
        //public const string ProjectDocumentSelectbyType_CompanywiseSP = "sp_DocumentMaster_SelectByPage_ProjectNo_CompanyId"; //sp_DocumentMaster_SelectByPage_ProjectId_CompanyId
        public const string ProjectDocumentSelectbyType_CompanywiseSP = "usp_DocumentMaster_SelectByPage_ProjectNo_CompanyId_API";
        //public const string MileStoneUpdateEmailNotification = "[dbo].[sp_MilestoneUpdate_EmailNotification]";//Audit Trail - Verified
        public const string GetProjectByCompanyIdList = "usp_ProjectMasterList_Select_API";
        
        // public const string MilestoneSelect = "sp_ProjectJobsMilestone_SelectByPage";//Audit Trail - Verified
        public const string MilestoneSelect = "usp_ProjectJobsMilestone_SelectByPage_API";//Audit Trail - Verified
        
       // public const string MilestoneSelectById = "sp_ProjectJobsMilestone_SelectByMilestoneId";

        //public const string GetAllCommunicationList = "[dbo].[sp_CommunicationDetails_Select]";//Audit Trail - Verified
        public const string GetAllCommunicationList = "[dbo].[usp_CommunicationDetails_Select_API]";//Audit Trail - Verified
        
        //public const string GetAllPostByProjectId = "sp_Post_Select_ByProjectId";

        //public const string SelectAllDesignation = "sp_DesignationMaster_Select";
        public const string SelectAllDesignation = "usp_DesignationMaster_Select_API";

        //public const string ModulePermissionByUserIdModuleName = "sp_UserModuleAccess_SelectByID_ModuleName";
        public const string ModulePermissionByUserIdModuleName = "usp_UserModuleAccess_SelectByID_ModuleName_API";
        public const string NewlyRegisterUserCount = "[dbo].[usp_GetRegisteredUserCount_Select_API]";//Audit Trail - Verified

        public const string GetStatus = "[dbo].[usp_StatusMaster_SelectByModule_API]";
        public const string NewlyRegisterUserList = "[dbo].[usp_NewRegisteredUser_Select_API]";//Audit Trail - Verified

        // public const string GetDocumentTypes = "sp_DocumentType_Select";
        public const string GetDocumentTypes = "usp_DocumentType_SelectByModule_API";

        public const string DocumentInsert = "usp_DocumentMaster_Insert_API";

        //public const string DocumentInsert = "[dbo].[sp_DocumentMaster_Insert]";
        public const string CheckDocumentAndDrawningName = "usp_CheckDocumentName_API";

        //public const string DocumentDeleteSP = "sp_DocumentMaster_Delete";//Audit Trail - Verified
        public const string DocumentDeleteSP = "usp_DocumentMaster_Delete_API";//Audit Trail - Verified
        //public const string DocumentUpdateSP = "sp_DocumentMaster_Update";//Audit Trail - Verified
        public const string DocumentUpdateSP = "usp_DocumentMaster_Update_API";//Audit Trail - Verified
        //public const string GetMilestoneTypes = "sp_MilestoneMaster_Select";
        public const string GetMilestoneTypes = "usp_MilestoneMaster_Select_API";
        //public const string MilestoneDeleteSP = "sp_ProjectJobsMilestone_DeleteById";//Audit Trail - Verified
        public const string MilestoneDeleteSP = "usp_ProjectJobsMilestone_DeleteById_API";//Audit Trail - Verified
        //public const string MilestoneUpdate = "sp_ProjectJobsMilestone_Update";//Audit Trail - Verified
        public const string MilestoneUpdate = "usp_ProjectJobsMilestone_Update_API";//Audit Trail - Verified
      //public const string MilestoneUpdate = "usp_ProjectJobsMilestone_Update_API_05092017";//Audit Trail - Verified
        
        //public const string MilestoneInsert = "sp_ProjectJobsMilestone_Insert";//Audit Trail - Verified
        public const string MilestoneInsert = "usp_ProjectJobsMilestone_Insert_API";//Audit Trail - Verified
        //public const string MilestoneReadyToPullTemplateDetailsSelect = "sp_ReadyToPullTemplateDetails_Select";
        public const string MilestoneReadyToPullTemplateDetailsSelect = "usp_ReadyToPullTemplateDetails_Select_API";
        public const string AddCommunicationMessage = "[dbo].[usp_CommunicationDetails_Insert_API]"; //Audit Trail - Verified
        public const string GetNotifyToMailID = "[dbo].[usp_UserMaster_NotifyToMail_Select_API]";

        public const string MilestoneReadyToPullTemplateDetailsInsert = "usp_ReadyToPullTemplateDetails_Insert_API";//Audit Trail - Verified

        public const string SetTokenUser = "usp_UserMaster_AddToken_API";
        public const string CompanyUserSelectByUserId = "usp_CompanyUser_SelectByID_API";
        public const string CompanyUserUpdate = "usp_CompanyUser_Update_API";//Audit Trail - Verified
        public const string UserModuleAccess_UpdateInsertModulePermission = "usp_UserModuleAccess_UpdateInsertModulePermission_API";
        public const string GetStateListFromCountry = "[dbo].[usp_StateMaster_Select_API]";
        public const string MileStoneUpdateEmailNotification = "[dbo].[usp_MilestoneUpdate_EmailNotification_API]";//Audit Trail - Verified
        public const string DueDateStatusSP = "[dbo].[usp_Dashboard_GetStatusDueDateCompDate_Select_API]";

        public const string ForgotPasswordName = "usp_ForgotPassword_ExistingEmail_API";
        public const string ProjectMilestonDueDateInsertSP = "[dbo].[usp_projectmilestonenote_duedate_insert_API]";

        public const string UpdateLoginDeviceInfoSP = "[dbo].[usp_UserMaster_Device_Update_API]";
        public const string ChangePassword = "usp_UserMaster_ChangePassword_API";//Audit Trail - Verified

        public const string NewlyRegisterUserUpdate = "[dbo].[usp_NewRegisterUser_Update_API]";//Audit Trail - Verified

        public const string CheckCompanyforEditNewRegisterUserSP = "[dbo].[usp_CompanyMaster_SelectbyCompanyName_API]";

        public const string GetJobPivotTable_SP = "[dbo].[usp_GetJobPivotTable_API]";

        public const string GetJobPivotTableWith_DocumentDrawingPhoto = "[dbo].usp_GetJobPivotTableWith_DocumentDrawingPhoto_API";

        #endregion

        #region MailBox

        public const string GetUserByCompanyForMailBox = "[dbo].[usp_MailBox_SelectbyCoId_Users_API]";
        public const string MailBoxInsert = "[dbo].[usp_MailBox_Insert_API]";//Audit Trail - Verified
        public const string GetMailByUserID = "[dbo].[usp_MailBox_Select_API]";//Audit Trail - Verified
        public const string GetMailByMailId = "[dbo].[usp_Mailbox_Select_MailId_API]";//Audit Trail - Verified
        public const string GetMailCount = "[dbo].[usp_Mailbox_Select_MailCount_ReceiverId_API]";
        public const string UpdateMailSattus = "[dbo].[usp_SplitMailbox_Update_API]";//Audit Trail - Verified
        public const string MailDeleteforLogicalDelete = "[dbo].[usp_MailBox_Update_API]";//Audit Trail - Verified

        public const string MailListForHeaderMenu = "[dbo].[usp_Mailbox_HeaderCount_API]";

        public const string MailListForDashboardWidget = "[dbo].[usp_Dashboard_MessageWidget_API]";

        #endregion

        #region Help
        public const string GetHelp = "usp_ModuleMasterHelpMaster_Select";
        #endregion

        #region OfficeManager
        public const string OfficeManagerInsert = "usp_OfficeManager_Insert_API";//Audit Trail - Verified
        public const string OfficeManagerSelect = "usp_OfficeManager_Select_API";//Audit Trail - Verified
        public const string OfficeManagerByUserId = "usp_OfficeManager_Select_ByID_API";
        public const string OfficeManagerUpdate = "usp_OfficeManager_Update_API";//Audit Trail - Verified
        public const string OfficeManagerDelete = "usp_OfficeManager_Delete_API"; //Audit Trail - Verified
        #endregion

        #region Calendar
        public const string GetCategory = "usp_CalendarCategoryMaster_Select";
        public const string AddTask = "usp_CalendarTaskMaster_Insert";
        public const string UpdateEventTask = "usp_CalendarTaskMaster_Update";
        public const string RemoveEventTask = "usp_CalendarTaskMaster_Delete";
        public const string SelectEventTask = "usp_CalendarTaskMaster_SelectByDate";
        
        public const string GetJobMasterListing = "[dbo].[usp_GetJobMasterListing_Select_API]";
        public const string GetEventByEventID = "[dbo].[usp_CalendarTaskMaster_SelectById]";
        
        #endregion

        public const string InsertNotification = "[dbo].[usp_NotificationLog_Insert]";
        public const string GetCompanyMaster = "[dbo].[usp_CompanyMaster_Select_API]";
        public const string GetNotificationList = "usp_NotificationLog_Select";

        public const string SelectProjectMasterLatLong = "usp_ProjectMaster_GetLatLongInfo_Select";
        public const string SelectUserMasterData = "usp_UserMaster_GetLatLongInfo_Select";
        public const string UPdateProjectMasterLatLong = "usp_ProjectMaster_LatLong_Update";
        public const string UpdateUserMasterLatLong = "usp_UserMaster_LatLong_Update";

        public const string GetProjectMasterLatLong = "[dbo].[usp_GetProjectMasterLatLong_Select_API]";

        #region Announcement
        public const string SelectAllAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_BeforeAfterLoginSelect_API]";
        #endregion

    }
}