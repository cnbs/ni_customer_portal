﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class OfficeManagerDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> OfficeManagerSelect(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[6] = new SqlParameter("@UserID", model.UserID);
                param[7] = new SqlParameter("@appAuditID", model.appAuditID);
                param[8] = new SqlParameter("@SessionID", model.DeviceID);
                param[9] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.OfficeManagerSelect, param);

                count = Convert.ToInt32(param[5].Value);

                ds.Tables[0].TableName = "OfficeManagerDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - OfficeManagerSelect");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public int InsertOfficeManager(OfficeManagerEntity officeManager)
        {
            SqlParameter[] param = new SqlParameter[16];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@FirstName", officeManager.FirstName);
                param[1] = new SqlParameter("@LastName", officeManager.LastName);
                param[2] = new SqlParameter("@OfficeID", officeManager.OfficeID);
                param[3] = new SqlParameter("@Phone", officeManager.Phone);
                param[4] = new SqlParameter("@SapID", officeManager.SapID);
                param[5] = new SqlParameter("@Title", officeManager.Title);
                param[6] = new SqlParameter("@Office_Desc", officeManager.Office_Desc);
                param[7] = new SqlParameter("@Email", officeManager.Email);
                param[8] = new SqlParameter("@Street", officeManager.Street);
                param[9] = new SqlParameter("@City", officeManager.City);
                param[10] = new SqlParameter("@State", officeManager.State);
                param[11] = new SqlParameter("@Zip", officeManager.Zip);
                param[12] = new SqlParameter("@CompanyID", officeManager.CompanyID);
                param[13] = new SqlParameter("@CreatedBy", officeManager.CreatedBy);
                param[14] = new SqlParameter("@appAuditID", officeManager.appAuditID);
                param[15] = new SqlParameter("@SessionID", officeManager.DeviceID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.OfficeManagerInsert, param);

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertOfficeManager");
                throw ex;
            }
            int i = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return 1;
        }

        public bool UpdateOfficeManager(OfficeManagerEntity officeManager)
        {
            SqlParameter[] param = new SqlParameter[18];

            try
            {
                param[0] = new SqlParameter("@OfficeManagerID", officeManager.OfficeManagerID);
                param[1] = new SqlParameter("@FirstName", officeManager.FirstName);
                param[2] = new SqlParameter("@LastName", officeManager.LastName);
                param[3] = new SqlParameter("@OfficeID", officeManager.OfficeID);
                param[4] = new SqlParameter("@Phone", officeManager.Phone);
                param[5] = new SqlParameter("@SapID", officeManager.SapID);
                param[6] = new SqlParameter("@Title", officeManager.Title);
                param[7] = new SqlParameter("@Office_Desc", officeManager.Office_Desc);
                param[8] = new SqlParameter("@Email", officeManager.Email);
                param[9] = new SqlParameter("@Street", officeManager.Street);
                param[10] = new SqlParameter("@City", officeManager.City);
                param[11] = new SqlParameter("@State", officeManager.State);
                param[12] = new SqlParameter("@Zip", officeManager.Zip);
                param[13] = new SqlParameter("@CompanyID", officeManager.CompanyID);
                param[14] = new SqlParameter("@UpdatedBy", officeManager.UpdatedBy);
                param[15] = new SqlParameter("@appAuditID", officeManager.appAuditID);
                param[16] = new SqlParameter("@SessionID", officeManager.DeviceID);
                param[17] = new SqlParameter("@ApplicationOperation", officeManager.ApplicationOperation);

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.OfficeManagerUpdate, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateOfficeManager");
                throw ex;
            }
            return true;
        }

        public List<DataRow> OfficeManagerSelectById(int OfficeManagerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@OfficeManagerID", OfficeManagerID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.OfficeManagerByUserId, param);
                ds.Tables[0].TableName = "OfficeManagerDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - OfficeManagerSelectById");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool DeleteOfficeManager(OfficeManagerEntity officeManager)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@OfficeManagerID", officeManager.OfficeManagerID);
                param[1] = new SqlParameter("@CompanyID", officeManager.CompanyID);
                param[2] = new SqlParameter("@UpdatedBy", officeManager.UpdatedBy);
                param[3] = new SqlParameter("@appAuditID", officeManager.appAuditID);
                param[4] = new SqlParameter("@SessionID", officeManager.DeviceID);
                param[5] = new SqlParameter("@ApplicationOperation", officeManager.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.OfficeManagerDelete, param);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteOfficeManager");
                throw ex;
            }
        }
    }
}