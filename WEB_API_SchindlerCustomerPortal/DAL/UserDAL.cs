﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class UserDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> LoginNow(string Email)
        {

            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@Email", Email);
                // param[1] = new SqlParameter("@Password", Password);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.LoginProcedureName, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - LoginNow");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public int UpdateLoginDeviceInfo(UserEntity model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                param[0] = new SqlParameter("@UserID", model.UserID);
                param[1] = new SqlParameter("@Email", model.Email);
                param[2] = new SqlParameter("@DeviceId", model.DeviceID);
                param[3] = new SqlParameter("@DeviceType", model.DeviceType);
                param[4] = new SqlParameter("@UpdatedBy", model.UserID);
                param[5] = new SqlParameter("@appAuditID", model.appAuditID);
                param[6] = new SqlParameter("@SessionID", model.DeviceID);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateLoginDeviceInfoSP, param);
                ds.Tables[0].TableName = "UpdateDeviceInformation";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateLoginDeviceInfo");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) ;
        }

        public List<DataRow> GetModulesPermissionForDynamicMenu(int userid, int parentId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            try
            {
                param[0] = new SqlParameter("@UserID", userid);
                param[1] = new SqlParameter("@ParentID", parentId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DynamicMenu, param);
                ds.Tables[0].TableName = "DynamicMenu";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetModulesPermissionForDynamicMenu");
                throw ex;
            }
            var moduleAccessList = ds.Tables[0].AsEnumerable().ToList();
            return moduleAccessList;

        }

        public List<DataRow> CheckModulePermission(int userId, string moduleName)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@USerID", userId);
                param[1] = new SqlParameter("@ModuleName", moduleName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ModulePermissionByUserIdModuleName, param);
                ds.Tables[0].TableName = "ModulePermission";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckModulePermission");
                throw ex;
            }

            var moduleAccessList = ds.Tables[0].AsEnumerable().ToList();
            return moduleAccessList;
        }

        public List<DataRow> NewlyRegisteredUser(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyID", model.CompanyID);
                param[1] = new SqlParameter("@UserID", model.UserID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.DeviceID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserList, param);
                ds.Tables[0].TableName = "UserDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUser");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool NewlyRegisteredUserUpdate(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[14];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ContactNumber", user.ContactNumber);
                param[1] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[2] = new SqlParameter("@appAuditID", user.appAuditID);
                param[3] = new SqlParameter("@SessionID", user.DeviceID);
                param[4] = new SqlParameter("@UserID", user.UserID);
                param[5] = new SqlParameter("@Designation", user.Designation);
                param[6] = new SqlParameter("@Address", user.Address1);
                param[7] = new SqlParameter("@State", user.State);
                param[8] = new SqlParameter("@City", user.City);
                param[9] = new SqlParameter("@PostalCode", user.PostalCode);
                param[10] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[11] = new SqlParameter("@CompanyName", user.CompanyName);
                param[12] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[13] = new SqlParameter("@Requested_Projects", user.Requested_Projects);

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserUpdate, param);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserAccountStatus");
                throw ex;
            }

            return true;
        }

        public bool UpdateUser(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[20];
            try
            {
                param[0] = new SqlParameter("@FirstName", user.FirstName);
                param[1] = new SqlParameter("@LastName", user.LastName);
                param[2] = new SqlParameter("@ContactNumber", user.ContactNumber);
                param[3] = new SqlParameter("@ProfilePic", user.ProfilePic);
                param[4] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[5] = new SqlParameter("@appAuditID", user.appAuditID);
                param[6] = new SqlParameter("@SessionID", user.SessionID);
                param[7] = new SqlParameter("@UserID", user.UserID);
                param[8] = new SqlParameter("@Designation", user.DesignationId);
                param[9] = new SqlParameter("@Address", user.Address1);
                param[10] = new SqlParameter("@State", user.State);
                param[11] = new SqlParameter("@City", user.City);
                param[12] = new SqlParameter("@PostalCode", user.PostalCode);
                param[13] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[14] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[15] = new SqlParameter("@Requested_Projects", user.Requested_Projects);
                param[16] = new SqlParameter("@IsUnSubscribed", user.Unsubscribe);
                param[17] = new SqlParameter("@Latitude", user.Latitude);
                param[18] = new SqlParameter("@Longitude", user.Longitude);
                param[19] = new SqlParameter("@flag", "0");

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.CompanyUserUpdate, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUser");
                throw ex;
            }
            return true;
        }

        public List<DataRow> CompanyUserSelectById(int UserID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserSelectByUserId, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyUserSelectById");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool SetTokenOnEmail(string Token, int UserID, int ExpireHours)
        {
            SqlParameter[] param = new SqlParameter[3];
            int result;
            try
            {
                param[0] = new SqlParameter("@TokenID", Token);
                param[1] = new SqlParameter("@UserID", UserID);
                param[2] = new SqlParameter("@HoursAdd", ExpireHours);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.SetTokenUser, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SetTokenOnEmail");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

        }

        public int InsertUpdateUserModuleAccessPermissionByModule(UserPermissionModuleModel obj)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[9];
            try
            {
                param[0] = new SqlParameter("@UserID", obj.UserID);
                param[1] = new SqlParameter("@ModuleID", obj.ModuleId);
                param[2] = new SqlParameter("@ColumnName", obj.ColumnName);
                param[3] = new SqlParameter("@ColumnValue", obj.ColumnValue);
                param[4] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[5] = new SqlParameter("@SessionID", obj.SessionID);
                param[6] = new SqlParameter("@updatedBy", obj.UpdatedBy);
                param[7] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[8] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);
                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.UserModuleAccess_UpdateInsertModulePermission, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertUpdateUserModuleAccessPermissionByModule");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;
        }

        public List<DataRow> GetStateList(int CountryID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetStateListFromCountry, param);
                ds.Tables[0].TableName = "StateList";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStateList");
                throw ex;
            }
            List<DataRow> stateList = ds.Tables[0].AsEnumerable().ToList();
            return stateList;
        }

        public List<DataRow> CheckForgotPwdEmail(string Email)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@Email", Email);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ForgotPasswordName, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckForgotPwdEmail");
                throw ex;
            }
            List<DataRow> EmailCheck = ds.Tables[0].AsEnumerable().ToList();
            return EmailCheck;
        }

        public UserEntity Find(string Email)
        {
            try
            {
                UserEntity userds = (from row in LoginNow(Email)
                                     select new UserEntity
                                     {
                                         Email = row["Email"].ToString(),
                                         RoleID = Convert.ToInt32(row["RoleID"]),
                                         UserID = Convert.ToInt32(row["UserID"]),
                                         FirstName = row["FirstName"].ToString(),
                                         LastName = row["LastName"].ToString(),
                                         CompanyID = Convert.IsDBNull(row["CompanyID"]) ? 0 : Convert.ToInt32(row["CompanyID"]),
                                         CompanyName = row["CompanyName"].ToString(),
                                         ContactNumber = row["ContactNumber"].ToString(),
                                         ProfilePic = string.IsNullOrEmpty(row["ProfilePic"].ToString()) ? "" : row["ProfilePic"].ToString(),
                                         //CreatedBy = row["CreatedBy"].ToString(),
                                         //UpdatedBy = row["UpdatedBy"].ToString(),
                                         Password = row["Password"].ToString(),
                                         PasswordSalt = row["Password_Salt"].ToString(),
                                         AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                         DeviceID = Convert.ToString(row["DeviceID"]),
                                         DeviceType = Convert.ToString(row["DeviceType"])
                                     }).FirstOrDefault();

                return userds;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Find");
                throw ex;
            }
        }

        public bool UpdateUserPassword(ChangePassword user)
        {
            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@OldPassword", user.OldPassword);
                param[1] = new SqlParameter("@NewPassword", user.NewPassword);
                param[2] = new SqlParameter("@PasswordSalt", user.PasswordSalt);
                param[3] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[4] = new SqlParameter("@appAuditID", user.appAuditID);
                param[5] = new SqlParameter("@SessionID", user.DeviceID);
                param[6] = new SqlParameter("@UserID", user.UserID);
                param[7] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ChangePassword, param);

                var result = ds.Tables[0].Rows[0]["password"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserPassword");
                throw ex;
            }
        }

        public bool CheckCompanyforEditNewRegisterUser(string companyName)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyName", companyName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckCompanyforEditNewRegisterUserSP, param);
                var result = ds.Tables[0].Rows[0]["Existing"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckCompanyforEditNewRegisterUser");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public int NewlyRegisteredUserCount(int CompanyID)
        {
            int iCount = 0;
            SqlParameter[] param = new SqlParameter[1];

            try
            {
                param[0] = new SqlParameter("@CompanyID", CompanyID);
                iCount = Convert.ToInt32(dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserCount, param));
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUserCount");
                throw ex;
            }
            return iCount;
        }

        public bool InsertNotification(NotificationEntity data)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyId", data.CompanyID);
                param[1] = new SqlParameter("@DeviceId", data.DeviceID);
                param[2] = new SqlParameter("@SenderId", data.SenderID);
                param[3] = new SqlParameter("@ReceiverId", data.ReceiverID);
                param[4] = new SqlParameter("@Subject", data.Subject);
                param[5] = new SqlParameter("@Message", data.Message);
                param[6] = new SqlParameter("@DateSent", data.DateSent);
                param[7] = new SqlParameter("@StatusId", data.StatusID);
                param[8] = new SqlParameter("@CreatedBy", data.CreatedBy);
                param[9] = new SqlParameter("@DeviceType", data.DeviceType);
                param[10] = new SqlParameter("@ModuleId", data.ModuleID);


                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.InsertNotification, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertNotification");
                throw ex;
                //return false;
            }
            return true;
        }

        public List<DataRow> UserNotificationList(string UserID,ref int count)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@UserId", UserID);
                param[1] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetNotificationList, param);
                ds.Tables[0].TableName = "NotificationList";
                count = Convert.ToInt32(param[1].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UserNotificationList");
            }
            List<DataRow> notificationList = ds.Tables[0].AsEnumerable().ToList();
            return notificationList;
        }
    }
}