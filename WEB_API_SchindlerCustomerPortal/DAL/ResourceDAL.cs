﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class ResourceDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> ResourceSelect(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[6] = new SqlParameter("@RoleId", model.RoleID);
                param[7] = new SqlParameter("@appAuditID", model.appAuditID);
                param[8] = new SqlParameter("@SessionID", model.DeviceID);
                param[9] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[10] = new SqlParameter("@UserId", model.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceSelect, param);
                ds.Tables[0].TableName = "ResourceDetails";

                count = Convert.ToInt32(param[5].Value);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceSelect");
                throw ex;
            }
            List<DataRow> ResourceData = ds.Tables[0].AsEnumerable().ToList();
            return ResourceData;
        }
    }
}