﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using System.Data;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class CompanyDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> CompanyMaster()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetCompanyMaster);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyMaster");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;
        }
    }
}