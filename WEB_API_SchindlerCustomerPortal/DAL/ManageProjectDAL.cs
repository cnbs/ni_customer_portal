﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class ManageProjectDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetAllProjectList(DataTableModel model, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[13];
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);

                param[3] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                param[4] = new SqlParameter("@PageNumber", model.PageNumber);
                param[5] = new SqlParameter("@PageSize", model.PageSize);
                param[6] = new SqlParameter("@SortColumn", model.SortColumn ?? string.Empty);
                param[7] = new SqlParameter("@SortDirection", model.SortDirection ?? string.Empty);
                param[8] = new SqlParameter("@TotalCount", totalcount)
                {
                    Direction = ParameterDirection.Output
                };

                //For Audit Trail
                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.DeviceID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[12] = new SqlParameter("@ProjectNo", model.ProjectNo);

                if (model.ProjectNo == 0) { 
                    param[12] = new SqlParameter("@ProjectNo", string.Empty);
                }
                
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectMasterList_SelectByPage, param);
                ds.Tables[0].TableName = "ProjectMasterList";
                totalcount = Convert.ToInt32(param[8].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProjectList");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public List<DataRow> GetProjectJobRecordByJobNo(DataTableModel model, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[11];
            try
            {
                param[0] = new SqlParameter("@ProjectNo", model.ProjectNo);

                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                param[2] = new SqlParameter("@PageNumber", model.PageNumber);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn ?? string.Empty);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection ?? string.Empty);
                param[6] = new SqlParameter("@TotalCount", totalcount)
                {
                    Direction = ParameterDirection.Output
                };
                param[7] = new SqlParameter("@UserId", model.UserID);

                //For Audit Trail
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.DeviceID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectJobList_SelectByPage, param);
                ds.Tables[0].TableName = "ProjectJobList";
                totalcount = Convert.ToInt32(param[6].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectJobRecordByJobNo");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public List<DataRow> GetAllCommunicationList(int projectNo, string jobNo, int appAuditID, string ApplicationOperation, string DeviceID, int UserId)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectNo);
                param[1] = new SqlParameter("@JobNo", jobNo);
                param[2] = new SqlParameter("@UserId", UserId);
                param[3] = new SqlParameter("@appAuditID", appAuditID);
                param[4] = new SqlParameter("@SessionID", DeviceID);
                param[5] = new SqlParameter("@ApplicationOperation", ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetAllCommunicationList, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllCommunicationList");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

            List<DataRow> postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }

        public List<DataRow> AddProjectCommunication(CommunicationEntity obj)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ProjectNo", obj.ProjectNo);
                param[1] = new SqlParameter("@JobNo", obj.JobNo);
                param[2] = new SqlParameter("@UserId", obj.UserId);
                param[3] = new SqlParameter("@MessageText", obj.Message);
                param[4] = new SqlParameter("@NotifyToEmail", obj.NotifyToEmail);
                param[5] = new SqlParameter("@AttachmentPath", obj.AttachmentPath);
                param[6] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[7] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[8] = new SqlParameter("@SessionID", obj.SessionID);
                param[9] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.AddCommunicationMessage, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectCommunication");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public List<DataRow> GetStatusList(string module)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Module", module);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetStatus, param);
                ds.Tables[0].TableName = "StatusList";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStatusList");
                throw ex;
            }

            List<DataRow> statusList = ds.Tables[0].AsEnumerable().ToList();

            return statusList;
        }

        public bool UpdateProjectMaster(ProjectMasterEntity projectMaster)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[6];
            int result;

            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectMaster.ProjectNo);
                param[1] = new SqlParameter("@SchindlerProjMgr", projectMaster.SchindlerProjMgr);
                param[2] = new SqlParameter("@Status", projectMaster.Status);
                param[3] = new SqlParameter("@UpdatedBy", projectMaster.UpdatedBy);
                param[4] = new SqlParameter("@appAuditID", projectMaster.appAuditID);
                param[5] = new SqlParameter("@SessionID", projectMaster.DeviceID);

                //param[1] = new SqlParameter("@ContractorSuper", projectMaster.ContractorSuper);
                //param[2] = new SqlParameter("@ContractorProjMgr", projectMaster.ContractorProjMgr);
                //param[3] = new SqlParameter("@ContractorContactPerson", projectMaster.ContractorContactPerson);
                //param[4] = new SqlParameter("@SchindlerSuper", projectMaster.SchindlerSuperintendent);
                //param[5] = new SqlParameter("@SchindlerSales", projectMaster.SchindlerSalesExecutive);
                // param[7] = new SqlParameter("@TurnOverDate", projectMaster.TurnOverDate);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateProjectMasterByID, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectMaster");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return true;
        }

        public List<DataRow> GetNotifyToMailID(int companyId, string searchTerm)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyId", companyId);
                param[1] = new SqlParameter("@SearchTerm", searchTerm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetNotifyToMailID, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetNotifyToMailID");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public string GetProjectandJobName(string ProjectNo, string JobNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", ProjectNo);
                param[1] = new SqlParameter("@JobNo", JobNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectandJobData, param);
                ds.Tables[0].TableName = "GetProjectandJobData";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetNotifyToMailID");
                throw ex;
            }
            return ds.Tables[0].Rows[0]["ProjJobData"].ToString();
        }

    }
}