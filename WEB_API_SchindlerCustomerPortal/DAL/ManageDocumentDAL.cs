﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class ManageDocumentDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetProjectDocumentList(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[15];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@RoleId", model.RoleID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);
                param[3] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[4] = new SqlParameter("@JobNo", model.JobNo);
                param[5] = new SqlParameter("@DocumentType", model.DocumentType);
                param[6] = new SqlParameter("@PageNumber", model.PageNumber);
                param[7] = new SqlParameter("@PageSize", model.PageSize);
                param[8] = new SqlParameter("@SortColumn", model.SortColumn);
                param[9] = new SqlParameter("@SortDirection", model.SortDirection);
                param[10] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[11] = new SqlParameter("@UserId", model.UserID);

                //For Audit Trail
                param[12] = new SqlParameter("@appAuditID", model.appAuditID);
                param[13] = new SqlParameter("@SessionID", model.DeviceID);
                param[14] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectDocumentSelectbyType_CompanywiseSP, param);
                ds.Tables[0].TableName = "ProjectDocumentDetails";

                count = Convert.ToInt32(param[10].Value);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectDocumentList");
                throw ex;
            }
            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;
        }

        public List<DataRow> GetDocTypeList(DocumentTypeEntity DTE)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            DTE.DocumentTypeName = string.IsNullOrEmpty(DTE.DocumentTypeName) ? "Document" : DTE.DocumentTypeName.ToString();
            param[0] = new SqlParameter("@ModuleType", DTE.DocumentTypeName);

            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetDocumentTypes, param);
                ds.Tables[0].TableName = "DocumentList";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDocTypeList");
                throw ex;
            }
            List<DataRow> doctypeList = ds.Tables[0].AsEnumerable().ToList();
            return doctypeList;
        }

        public bool CheckDocumentAndDrawningName(ProjectDocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectID", doc.ProjectNo);
                param[1] = new SqlParameter("@JobID", doc.JobNo);
                param[2] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[3] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[4] = new SqlParameter("@DocumentID", doc.DocumentID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckDocumentAndDrawningName, param);
                ds.Tables[0].TableName = "CheckDocumentAndDrawning";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckDocumentAndDrawningName");
                throw ex;
            }
            return (ds.Tables[0].Rows[0][0].ToString() == "1") ? true : false;
        }
        
        public List<DataRow> DocumentInsert(ProjectDocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[1] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[2] = new SqlParameter("@Description", doc.Description);
                param[3] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[4] = new SqlParameter("@Expireddate", string.IsNullOrEmpty(doc.ExpiredDate)?null: doc.ExpiredDate);
                param[5] = new SqlParameter("@PublishedBy", doc.PublishedBy);
                param[6] = new SqlParameter("@IsDeleted", doc.IsDeleted);
                param[7] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[8] = new SqlParameter("@SessionID", doc.DeviceID);
                param[9] = new SqlParameter("@FileType", doc.FileType);
                param[10] = new SqlParameter("@StatusId", doc.StatusId);
                param[11] = new SqlParameter("@ProjectNo", string.IsNullOrEmpty(doc.ProjectNo) ? null : doc.ProjectNo);
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@JobNo", string.IsNullOrEmpty(doc.JobNo) ? null : doc.JobNo);
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);
                //{
                //    Direction = ParameterDirection.Output
                //};

                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentInsert, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DocumentInsert");
                throw ex;
            }
            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public List<DataRow> DocumentUpdate(ProjectDocumentEntity doc)
        {
            if (doc.DocumentPath == null && doc.FileType == null)
            {
                doc.DocumentPath = "-1";
                doc.FileType = "-1";
            }
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[2] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[3] = new SqlParameter("@Description", doc.Description);
                param[4] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[5] = new SqlParameter("@Expireddate", doc.ExpiredDate);
                param[6] = new SqlParameter("@UpdatedBy", doc.UpdatedBy);
                param[7] = new SqlParameter("@FileType", doc.FileType);
                param[8] = new SqlParameter("@StatusId", doc.StatusId);
                param[9] = new SqlParameter("@ProjectNo", doc.ProjectNo);
                param[10] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[11] = new SqlParameter("@SessionID", doc.DeviceID);
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@JobNo", doc.JobNo);
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);

                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);
                //{
                //    Direction = ParameterDirection.Output
                //};

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentUpdateSP, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DocumentUpdate");
                throw ex;
            }
            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public bool DeleteDocument(DocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DeletedBy", doc.DeletedBy);
                param[2] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[3] = new SqlParameter("@SessionID", doc.DeviceID);

                param[4] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[5] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentDeleteSP, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteDocument");
                throw ex;
            }
            return true;
        }
    }
}