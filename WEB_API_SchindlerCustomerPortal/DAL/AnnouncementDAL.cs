﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class AnnouncementDAL
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> AllAnnouncementSelect(string DisplayOn)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@DisplayOn", DisplayOn);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectAllAnnouncementMessage, param);
                ds.Tables[0].TableName = "Announcement";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AnnouncementSelect");
                throw ex;
            }

            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;

        }
    }
}