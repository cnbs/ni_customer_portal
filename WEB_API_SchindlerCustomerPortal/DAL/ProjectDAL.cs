﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class ProjectDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> DashboardProjectListingMilestone(DataTableModel model,ref int count, ref string Email, ref int TotalProject, ref int CompanyId)
        {
            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@Count", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[6] = new SqlParameter("@UserEmail", SqlDbType.NVarChar, 100)
                {
                    Direction = ParameterDirection.Output
                };
                param[7] = new SqlParameter("@TotalProject", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@CompanyId", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DashboardProjectListingSP, param);
                ds.Tables[0].TableName = "ProjectJobsMilestone";
                count = Convert.ToInt32(param[5].Value);
                Email = Convert.ToString(param[6].Value);
                TotalProject = Convert.ToInt32(param[7].Value);
                CompanyId = Convert.ToInt32(param[8].Value);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DashboardProjectListingMilestone");
                throw ex;
            }
            List<DataRow> projectData = ds.Tables[0].AsEnumerable().ToList();
            return projectData;
        }

        public List<DataRow> GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            model.Milestone = model.Milestone.Replace("'", "''");
            model.Milestone = model.Milestone.Replace("’", "''");

            try
            {
                param[0] = new SqlParameter("@JobNo", model.JobNo);
                param[1] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[2] = new SqlParameter("@MilestoneName", model.Milestone);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DueDateStatusSP, param);
                ds.Tables[0].TableName = "DueDateStatus";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDueDateAndStatusOfMilestone");
                throw ex;
            }
            List<DataRow> projectData = ds.Tables[0].AsEnumerable().ToList();
            return projectData;
        }

        public bool AddProjectBankMilestoneCommentDueDate(string comment, string projectBankTitle, int userID, int statusid, string duedate, string CompletedDate)
        {
            SqlParameter[] param = new SqlParameter[6];
            try
            {
                param[0] = new SqlParameter("@note", comment);
                param[1] = new SqlParameter("@projobmile", projectBankTitle);
                param[2] = new SqlParameter("@UserId", userID);
                param[3] = new SqlParameter("@status", Convert.ToInt32(statusid));

                if (!string.IsNullOrEmpty(duedate))
                    param[4] = new SqlParameter("@duedate", duedate);
                else
                    param[4] = new SqlParameter("@duedate", DBNull.Value);


                if (!string.IsNullOrEmpty(CompletedDate))
                    param[5] = new SqlParameter("@completeddate", CompletedDate);
                else
                    param[5] = new SqlParameter("@completeddate", DBNull.Value);

                
                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.ProjectMilestonDueDateInsertSP, param);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectBankMilestoneCommentDueDate");
                throw ex;
            }
            return true;
        }

        public List<DataRow> ProjectMasterLatLongDetail(DataTableModel model)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectMasterLatLong, param);
                ds.Tables[0].TableName = "ProjectMasterLatLong";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectMasterLatLongDetail");
                throw ex;
            }
            List<DataRow> projectData = ds.Tables[0].AsEnumerable().ToList();
            return projectData;
        }

        public List<DataRow> GetProjectAssignedUserListForMilestoneEmail(string projectNo)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MileStoneUpdateEmailNotification, param);
                ds.Tables[0].TableName = "ProjectMasterRecords";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectAssignedUserListForMilestoneEmail");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public List<DataRow> GetProjectandJobPreviousStatus(string ProjectNo, string JobNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", ProjectNo);
                param[1] = new SqlParameter("@JobNo", JobNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectandJobData, param);
                ds.Tables[0].TableName = "GetPreviousStatusName";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStatusName");
                throw ex;
            }
            var ProjJobData = ds.Tables[0].AsEnumerable().ToList();
            return ProjJobData;
        }

        public string GetStatusFromStatusID(int StatusID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@StatusID", StatusID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetStatusByStatusID, param);
                ds.Tables[0].TableName = "GetStatusName";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetStatusFromStatusID");
                throw ex;
            }
            return ds.Tables[0].Rows[0]["StatusName"].ToString();
        }

        public string GetProjectWiseGraphAndDocuments(DataTableModel model)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNumber", model.ProjectNo);
                param[1] = new SqlParameter("@UserId", model.UserID);
                param[2] = new SqlParameter("@RoleId", model.RoleID);
                param[3] = new SqlParameter("@CompanyId", model.CompanyID);
                param[4] = new SqlParameter("@ViewType", model.ViewType);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.USP_GetProjectWiseGraphAndDocuments_Select_API, param);
                ds.Tables[0].TableName = "GetProjectWiseGraphAndDocuments";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectWiseGraphAndDocuments");
                throw ex;
            }
            return ds.Tables[0].Rows[0]["JsonGraphAndDocument"].ToString();
        }

        public DataSet GetJobPivotTable(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[13];
            try
            {
                param[0] = new SqlParameter("@ProjectCompanyId", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[1] = new SqlParameter("@ProjectNumber",model.ProjectNo);
                param[2] = new SqlParameter("@JobNo", model.JobNo);
                param[3] = new SqlParameter("@UserId", model.UserID);
                param[4] = new SqlParameter("@RoleId", model.RoleID);
                param[5] = new SqlParameter("@CompanyId", model.CompanyID);
                param[6] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[7] = new SqlParameter("@PageNumber", model.PageNumber);
                param[8] = new SqlParameter("@PageSize", model.PageSize);
                param[9] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[10] = new SqlParameter("@appAuditID", model.appAuditID);
                param[11] = new SqlParameter("@SessionID", model.SessionID);
                param[12] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobPivotTable_SP, param);
                ds.Tables[0].TableName = "JobPivotTable";

                dal.Close();

                model.ProjectCompanyID = Convert.ToInt32(param[0].Value);
                model.Count = Convert.ToInt32(param[9].Value);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobPivotTable");
                throw ex;
            }
            return ds;
        }

        public string GetJobInvoiceDocPhotoList(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[13];
            try
            {
                param[0] = new SqlParameter("@ProjectCompanyId", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[1] = new SqlParameter("@ProjectNumber", model.ProjectNo);
                param[2] = new SqlParameter("@JobNo", model.JobNo);
                param[3] = new SqlParameter("@UserId", model.UserID);
                param[4] = new SqlParameter("@RoleId", model.RoleID);
                param[5] = new SqlParameter("@CompanyId", model.CompanyID);
                param[6] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[7] = new SqlParameter("@PageNumber", model.PageNumber);
                param[8] = new SqlParameter("@PageSize", model.PageSize);
                param[9] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[10] = new SqlParameter("@appAuditID", model.appAuditID);
                param[11] = new SqlParameter("@SessionID", model.SessionID);
                param[12] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[12] = new SqlParameter("@LinkType", model.DocumentType);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobPivotTableWith_DocumentDrawingPhoto, param);
                ds.Tables[0].TableName = "DocumentData";

                dal.Close();

                //model.ProjectCompanyID = Convert.ToInt32(param[0].Value);
                //model.Count = Convert.ToInt32(param[9].Value);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobInvoiceDocPhotoList");
                throw ex;
            }
            return ds.Tables[0].Rows[0][model.JobNo].ToString();
        }

        public bool updateEmailAndPhoneData(DataTableModel projectMaster)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[9];
            int result;

            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectMaster.ProjectNo);
                param[1] = new SqlParameter("@CompanyID", projectMaster.CompanyID);
                param[2] = new SqlParameter("@Email", projectMaster.UserEmail);
                param[3] = new SqlParameter("@Phone", projectMaster.Phone);
                param[4] = new SqlParameter("@Role", projectMaster.RoleName);
                param[5] = new SqlParameter("@UpdatedBy", projectMaster.UserID);
                param[6] = new SqlParameter("@appAuditID", projectMaster.appAuditID);
                param[7] = new SqlParameter("@SessionID", projectMaster.SessionID);
                param[8] = new SqlParameter("@ApplicationOperation", projectMaster.ApplicationOperation);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateEmailAndPhoneData, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - updateEmailAndPhoneData");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public bool Update_BankDescByJobNo(DataTableModel model)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[6];
            int result;

            try
            {
                param[0] = new SqlParameter("@BankDesc", model.Description);
                param[1] = new SqlParameter("@JobNo", model.JobNo);
                param[2] = new SqlParameter("@UpdatedBy", model.UserID);
                param[3] = new SqlParameter("@appAuditID", model.appAuditID);
                param[4] = new SqlParameter("@SessionID", model.SessionID);
                param[5] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.JobMaster_UpdateBankDescByJobNo, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Update_BankDescByJobNo");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }
    }
}