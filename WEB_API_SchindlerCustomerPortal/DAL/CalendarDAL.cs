﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class CalendarDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetJobData(DataTableModel model)
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@DateFrom", model.LogDateFrom);
                param[3] = new SqlParameter("@DateTo", model.LogDateTo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobMasterListing, param);
                ds.Tables[0].TableName = "GetJobMasterListing";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobData");
                throw ex;
            }
            List<DataRow> JobData = ds.Tables[0].AsEnumerable().ToList();
            return JobData;
        }

        public List<DataRow> CategoryList()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetCategory);
                ds.Tables[0].TableName = "CategoryList";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CategoryList");
                throw ex;
            }
            var CategoryList = ds.Tables[0].AsEnumerable().ToList();
            return CategoryList;
        }

        public int AddEvent(CalendarEventEntity calendarEvent, ref int EventId)
        {
            SqlParameter[] param = new SqlParameter[12];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@EmailSentTo", calendarEvent.SendTo);
                param[1] = new SqlParameter("@EmailSubject", calendarEvent.Subject);
                param[2] = new SqlParameter("@MessageBody", calendarEvent.MessageBody);
                param[3] = new SqlParameter("@CategoryId", calendarEvent.CategoryID);
                param[4] = new SqlParameter("@Location", calendarEvent.Location);
                param[5] = new SqlParameter("@StartDt", calendarEvent.StartDate);
                param[6] = new SqlParameter("@EndDt", calendarEvent.EndDate);
                param[7] = new SqlParameter("@CreatedBy", calendarEvent.CreatedBy);
                param[8] = new SqlParameter("@appAuditID", calendarEvent.AppAuditID);
                param[9] = new SqlParameter("@SessionID", calendarEvent.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", calendarEvent.ApplicationOperation);
                param[11] = new SqlParameter("@EventId", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.AddTask, param);
                ds.Tables[0].TableName = "AddNewCalendarEvent";

                EventId = Convert.ToInt32(param[11].Value);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddEvent");
                throw ex;
            }

            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public int UpdateEvent(CalendarEventEntity calendarEvent)
        {
            SqlParameter[] param = new SqlParameter[12];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@EmailSentTo", calendarEvent.SendTo);
                param[1] = new SqlParameter("@EmailSubject", calendarEvent.Subject);
                param[2] = new SqlParameter("@MessageBody", calendarEvent.MessageBody);
                param[3] = new SqlParameter("@CategoryId", calendarEvent.CategoryID);
                param[4] = new SqlParameter("@Location", calendarEvent.Location);
                param[5] = new SqlParameter("@StartDt", calendarEvent.StartDate);
                param[6] = new SqlParameter("@EndDt", calendarEvent.EndDate);
                param[7] = new SqlParameter("@UpdatedBy", calendarEvent.UpdatedBy);
                param[8] = new SqlParameter("@appAuditID", calendarEvent.AppAuditID);
                param[9] = new SqlParameter("@SessionID", calendarEvent.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", calendarEvent.ApplicationOperation);
                param[11] = new SqlParameter("@CalTaskMstId", calendarEvent.Id);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateEventTask, param);
                ds.Tables[0].TableName = "UpdateCalendarEvent";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateEvent");
                throw ex;
            }

            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public int RemoveEvent(CalendarEventEntity calendarEvent)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CalTaskMstId", calendarEvent.Id);
                param[1] = new SqlParameter("@DeletedBy", calendarEvent.DeletedBy);
                param[2] = new SqlParameter("@appAuditID", calendarEvent.AppAuditID);
                param[3] = new SqlParameter("@SessionID", calendarEvent.SessionID);
                param[4] = new SqlParameter("@ApplicationOperation", calendarEvent.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.RemoveEventTask, param);
                ds.Tables[0].TableName = "RemoveCalendarEvent";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - RemoveEvent");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public List<DataRow> SelectEventList(CalendarEventEntity calendarEvent, ref int Count)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", calendarEvent.Id);
                param[1] = new SqlParameter("@cDate", calendarEvent.StartDate);
                param[2] = new SqlParameter("@Count", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectEventTask, param);
                ds.Tables[0].TableName = "EventList";
                Count = Convert.ToInt32(param[2].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SelectEventList");
                throw ex;
            }
            var CategoryList = ds.Tables[0].AsEnumerable().ToList();
            return CategoryList;
        }

        public List<DataRow> SelectEventByID(string ID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CalTaskMstId", Convert.ToInt32(ID));
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetEventByEventID, param);
                ds.Tables[0].TableName = "EventDetail";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SelectEventByID");
                throw ex;
            }
            var CategoryList = ds.Tables[0].AsEnumerable().ToList();
            return CategoryList;
        }
    }
}
