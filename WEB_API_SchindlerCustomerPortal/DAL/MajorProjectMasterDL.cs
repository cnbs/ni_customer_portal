using DAL;
using System;
using System.Collections;

namespace DL
{
   [Serializable ]
   public class MajorProjectMasterDL : BaseDAL
    {

        /// <summary>
        /// Summary description for MajorProjectMasterDL
        /// </summary>
		#region Private Variables
		private Int32 _MajorProjectID;
		private Int32 _CompanyID;
		private String _ProjectName;
		private String _ProjectShortName;
		private String _ProjectAdditionalInfo;
		private String _ProjectConfidentialInfo;
		private String _ProjectMgrPhone;
		private String _ProjectMgrEmail;
		private String _ContractorAltPhone;
		private String _StreetAddress;
		private String _StateCode;
		private String _City;
		private String _ZipCode;
		private String _Phone;
		private String _Contact1Name;
		private String _Contact1Email;
		private String _Contact1Designation;
		private String _Contact1Phone;
		private String _Contact2Name;
		private String _Contact2Email;
		private String _Contact2Designation;
		private String _Contact2Phone;
		private String _ProjectLogo;
		private String _CreatedBy;
		private DateTime _CreatedDate;
		private String _UpdatedBy;
		private DateTime _UpdatedDate;
		private Boolean _IsDeleted;
		private Boolean _IsPublished;
		private String _PublishedBy;
		private DateTime _PublishedDate;

		#endregion

		#region Public Properties
		public Int32 MajorProjectID
		{
			get
			{
				return _MajorProjectID;
			}
			set
			{
				_MajorProjectID = value;
			}
		}
		public Int32 CompanyID
		{
			get
			{
				return _CompanyID;
			}
			set
			{
				_CompanyID = value;
			}
		}
		public String ProjectName
		{
			get
			{
				return _ProjectName;
			}
			set
			{
				_ProjectName = value;
			}
		}
		public String ProjectShortName
		{
			get
			{
				return _ProjectShortName;
			}
			set
			{
				_ProjectShortName = value;
			}
		}
		public String ProjectAdditionalInfo
		{
			get
			{
				return _ProjectAdditionalInfo;
			}
			set
			{
				_ProjectAdditionalInfo = value;
			}
		}
		public String ProjectConfidentialInfo
		{
			get
			{
				return _ProjectConfidentialInfo;
			}
			set
			{
				_ProjectConfidentialInfo = value;
			}
		}
		public String ProjectMgrPhone
		{
			get
			{
				return _ProjectMgrPhone;
			}
			set
			{
				_ProjectMgrPhone = value;
			}
		}
		public String ProjectMgrEmail
		{
			get
			{
				return _ProjectMgrEmail;
			}
			set
			{
				_ProjectMgrEmail = value;
			}
		}
		public String ContractorAltPhone
		{
			get
			{
				return _ContractorAltPhone;
			}
			set
			{
				_ContractorAltPhone = value;
			}
		}
		public String StreetAddress
		{
			get
			{
				return _StreetAddress;
			}
			set
			{
				_StreetAddress = value;
			}
		}
		public String StateCode
		{
			get
			{
				return _StateCode;
			}
			set
			{
				_StateCode = value;
			}
		}
		public String City
		{
			get
			{
				return _City;
			}
			set
			{
				_City = value;
			}
		}
		public String ZipCode
		{
			get
			{
				return _ZipCode;
			}
			set
			{
				_ZipCode = value;
			}
		}
		public String Phone
		{
			get
			{
				return _Phone;
			}
			set
			{
				_Phone = value;
			}
		}
		public String Contact1Name
		{
			get
			{
				return _Contact1Name;
			}
			set
			{
				_Contact1Name = value;
			}
		}
		public String Contact1Email
		{
			get
			{
				return _Contact1Email;
			}
			set
			{
				_Contact1Email = value;
			}
		}
		public String Contact1Designation
		{
			get
			{
				return _Contact1Designation;
			}
			set
			{
				_Contact1Designation = value;
			}
		}
		public String Contact1Phone
		{
			get
			{
				return _Contact1Phone;
			}
			set
			{
				_Contact1Phone = value;
			}
		}
		public String Contact2Name
		{
			get
			{
				return _Contact2Name;
			}
			set
			{
				_Contact2Name = value;
			}
		}
		public String Contact2Email
		{
			get
			{
				return _Contact2Email;
			}
			set
			{
				_Contact2Email = value;
			}
		}
		public String Contact2Designation
		{
			get
			{
				return _Contact2Designation;
			}
			set
			{
				_Contact2Designation = value;
			}
		}
		public String Contact2Phone
		{
			get
			{
				return _Contact2Phone;
			}
			set
			{
				_Contact2Phone = value;
			}
		}
		public String ProjectLogo
		{
			get
			{
				return _ProjectLogo;
			}
			set
			{
				_ProjectLogo = value;
			}
		}
		public String CreatedBy
		{
			get
			{
				return _CreatedBy;
			}
			set
			{
				_CreatedBy = value;
			}
		}
		public DateTime CreatedDate
		{
			get
			{
				return _CreatedDate;
			}
			set
			{
				_CreatedDate = value;
			}
		}
		public String UpdatedBy
		{
			get
			{
				return _UpdatedBy;
			}
			set
			{
				_UpdatedBy = value;
			}
		}
		public DateTime UpdatedDate
		{
			get
			{
				return _UpdatedDate;
			}
			set
			{
				_UpdatedDate = value;
			}
		}
		public Boolean IsDeleted
		{
			get
			{
				return _IsDeleted;
			}
			set
			{
				_IsDeleted = value;
			}
		}
		public Boolean IsPublished
		{
			get
			{
				return _IsPublished;
			}
			set
			{
				_IsPublished = value;
			}
		}
		public String PublishedBy
		{
			get
			{
				return _PublishedBy;
			}
			set
			{
				_PublishedBy = value;
			}
		}
		public DateTime PublishedDate
		{
			get
			{
				return _PublishedDate;
			}
			set
			{
				_PublishedDate = value;
			}
		}

        #endregion

	}
}
