﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class GeoLocationDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetProjectMasterDetail()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectProjectMasterLatLong);
                ds.Tables[0].TableName = "ProjectMasterLatLong";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectMasterDetail");
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public List<DataRow> GetUserMasterDetail()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectUserMasterData);
                ds.Tables[0].TableName = "ProjectMasterLatLong";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectMasterDetail");
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool SetLatLong(List<ProjectGeoLocationEntity> UpdatedData)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                DataTable _dt;

                // create data table to insert items
                _dt = new DataTable("LatLongTableType");
                _dt.Columns.Add("ProjectId", typeof(int));
                _dt.Columns.Add("Latitude", typeof(decimal));
                _dt.Columns.Add("Longitude", typeof(decimal));

                foreach (ProjectGeoLocationEntity rd in UpdatedData)
                {
                    _dt.Rows.Add(rd.ProjectID, rd.Latitude, rd.Longitude);
                }

                param[0] = new SqlParameter("@ProjectMaster", SqlDbType.Structured);
                param[0].Value = _dt;
                param[0].Direction = ParameterDirection.Input;

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UPdateProjectMasterLatLong, param);
                dal.Close();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SetLatLong");
            }
            return false;
        }

        public bool SetUserAddressLatLong(List<UserEntity> UpdatedData)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                DataTable _dt;

                // create data table to insert items
                _dt = new DataTable("UserLatLongTableType");
                _dt.Columns.Add("UserID", typeof(int));
                _dt.Columns.Add("Latitude", typeof(decimal));
                _dt.Columns.Add("Longitude", typeof(decimal));

                foreach (UserEntity rd in UpdatedData)
                {
                    _dt.Rows.Add(rd.UserID, rd.Latitude, rd.Longitude);
                }

                param[0] = new SqlParameter("@UserMaster", SqlDbType.Structured);
                param[0].Value = _dt;
                param[0].Direction = ParameterDirection.Input;

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateUserMasterLatLong, param);
                dal.Close();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SetUserAddressLatLong");
            }
            return false;
        }
    }
}
