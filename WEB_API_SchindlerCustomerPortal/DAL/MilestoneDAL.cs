﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WEB_API_OmniSalesHub.DAL;
using WEB_API_SchindlerCustomerPortal.Common;
using WEB_API_SchindlerCustomerPortal.Models;

namespace WEB_API_SchindlerCustomerPortal.DAL
{
    public class MilestoneDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetMilestoneTypeList()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetMilestoneTypes);
                ds.Tables[0].TableName = "MilestoneList";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetMilestoneTypeList");
                throw ex;
            }
            List<DataRow> miletypeList = ds.Tables[0].AsEnumerable().ToList();
            return miletypeList;
        }

        public List<DataRow> MilestoneSelect(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[12];
            DataSet ds = new DataSet();
            try
            {
                //param[0] = new SqlParameter("@DocumentType", model.DocumentType);
                param[0] = new SqlParameter("@JobNo", model.JobNo ?? string.Empty);
                param[1] = new SqlParameter("@ProjectNo", model.ProjectNumber);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                //For Audit Trail
                param[8] = new SqlParameter("@UserId", model.UserID);
                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.DeviceID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneSelect, param);
                ds.Tables[0].TableName = "MilestoneDetails";
                count = Convert.ToInt32(param[7].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MilestoneSelect");
                throw ex;
            }
            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;
        }

        public List<DataRow> MilestoneInsertdata(ProjectMilestoneEntity milestone)
        {
            SqlParameter[] param = new SqlParameter[15];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@MilestoneTitle", milestone.MilestoneTitle);
                param[1] = new SqlParameter("@JobNo", milestone.JobNo);
                param[2] = new SqlParameter("@ProjectNo", milestone.ProjectNo);
                param[3] = new SqlParameter("@Predecessor", milestone.Predecessor);
                param[4] = new SqlParameter("@Successor", milestone.Successor);
                param[5] = new SqlParameter("@DueDate", milestone.DueDate);
                param[6] = new SqlParameter("@Status", milestone.Status);
                param[7] = new SqlParameter("@WaitingOn", milestone.WaitingOn ?? string.Empty);
                param[8] = new SqlParameter("@CreatedBy", milestone.PublishedBy);
                param[9] = new SqlParameter("@appAuditID", milestone.appAuditID);
                param[10] = new SqlParameter("@SessionID", milestone.SessionID);
                //For Audit Trail
                param[11] = new SqlParameter("@UserId", milestone.UserID);
                param[12] = new SqlParameter("@ApplicationOperation", milestone.ApplicationOperation);
                param[13] = new SqlParameter("@Note", milestone.Note);
                param[14] = new SqlParameter("@CompletedDate", milestone.CompletedDate);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneInsert, param);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MilestoneInsertdata");
                throw ex;
            }
            List<DataRow> ResData = ds.Tables[0].AsEnumerable().ToList();
            return ResData;
        }

        public bool DeleteMilestone(ProjectMilestoneEntity mile)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@MilestoneId", mile.MilestoneId);
                param[1] = new SqlParameter("@appAuditID", mile.appAuditID);
                param[2] = new SqlParameter("@SessionID", mile.DeviceID);
                param[3] = new SqlParameter("@DeletedBy", mile.DeletedBy);
                //For Audit Trail
                param[4] = new SqlParameter("@UserId", mile.UserID);
                param[5] = new SqlParameter("@ApplicationOperation", mile.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneDeleteSP, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteMilestone");
                throw ex;
            }
            return true;
        }

        public bool MilestoneUpdate(ProjectMilestoneEntity milestone)
        {
            SqlParameter[] param = new SqlParameter[16];
            DataSet ds = new DataSet();
            int count = 0;
            try
            {
                param[0] = new SqlParameter("@MilestoneId", milestone.MilestoneId);
                param[1] = new SqlParameter("@MilestoneTitle", milestone.MilestoneTitle);
                param[2] = new SqlParameter("@JobNo", milestone.JobNo);
                param[3] = new SqlParameter("@ProjectNo", milestone.ProjectNo);
                param[4] = new SqlParameter("@Predecessor", milestone.Predecessor);
                param[5] = new SqlParameter("@Successor", milestone.Successor);
                param[6] = new SqlParameter("@DueDate", milestone.DueDate);
                param[7] = new SqlParameter("@Status", milestone.Status);
                param[8] = new SqlParameter("@WaitingOn", milestone.WaitingOn ?? string.Empty);
                param[9] = new SqlParameter("@appAuditID", milestone.appAuditID);
                param[10] = new SqlParameter("@SessionID", milestone.DeviceID);
                param[11] = new SqlParameter("@UpdatedBy", milestone.UpdatedBy);

                //For Audit Trail
                param[12] = new SqlParameter("@UserId", milestone.UserID);
                param[13] = new SqlParameter("@ApplicationOperation", milestone.ApplicationOperation);
                param[14] = new SqlParameter("@Note", milestone.Note);
                param[15] = new SqlParameter("@CompletedDate", milestone.CompletedDate);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneUpdate, param);
                count = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());

                dal.Close();
                if (count == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - MilestoneUpdate");
                throw ex;
            }
        }

        public List<DataRow> GetAllReadyToPullTemplateMasterList(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[5];
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);
                param[3] = new SqlParameter("@projectno", model.ProjectNumber);
                param[4] = new SqlParameter("@jobno", model.JobNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneReadyToPullTemplateDetailsSelect, param);
                ds.Tables[0].TableName = "MilestoneReadyToPullTemplateMasterList";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectMilestone - GetAllReadyToPullTemplateMasterList");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }

        public bool AddMilestoneReadyToPullTemplateDetails(List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails, int ProjectNo, string JobNo)
        {
            SqlParameter[] param = new SqlParameter[7];
            DataSet ds = new DataSet();
            try
            {
                int appAuditID = 0;
                string SessionID = "";
                int UserId = 0;
                string ApplicationOperation = "";
                DataTable _dt;

                // create data table to insert items
                _dt = new DataTable("readytopull");
                _dt.Columns.Add("TemplateId", typeof(int));
                _dt.Columns.Add("ProjectNo", typeof(int));
                _dt.Columns.Add("JobNo", typeof(string));
                _dt.Columns.Add("Comment", typeof(string));
                _dt.Columns.Add("CreatedBy", typeof(string));
                //_dt.Columns.Add("CreatedDate", typeof(DateTime));
                _dt.Columns.Add("Attachment", typeof(string));
                _dt.Columns.Add("RealAttachment", typeof(string));

                foreach (ReadyToPullTemplateDetails rd in lstReadyToPullTemplateDetails)
                {
                    _dt.Rows.Add(rd.TemplateId, rd.ProjectNo, rd.JobNo, rd.Comment, rd.CreatedBy, rd.Attachment, rd.RealAttachment);
                    appAuditID = rd.appAuditID;
                    SessionID = rd.SessionId;
                    ApplicationOperation = rd.ApplicationOperation;
                    UserId = rd.UserId;
                    ProjectNo = Convert.ToInt32(rd.ProjectNo);
                    JobNo = rd.JobNo.ToString();
                }

                _dt.AcceptChanges();

                param[0] = new SqlParameter("@readytopull", SqlDbType.Structured);
                param[0].Value = _dt;
                param[0].Direction = ParameterDirection.Input;
                param[1] = new SqlParameter("@appAuditID", appAuditID);
                param[2] = new SqlParameter("@sessionid", SessionID);
                param[3] = new SqlParameter("@ProjectNo2", ProjectNo);
                param[4] = new SqlParameter("@JobNo2", JobNo);

                param[5] = new SqlParameter("@ApplicationOperation", ApplicationOperation);
                param[6] = new SqlParameter("@UserId", UserId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MilestoneReadyToPullTemplateDetailsInsert, param);
                //ds.Tables[0].TableName = "MilestoneReadyToPullTemplateDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddMilestoneReadyToPullTemplateDetails");
                throw ex;
            }
            return true;
        }

        public List<DataRow> GetProjectAssignedUserListForMilestoneEmail(string projectNo)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MileStoneUpdateEmailNotification, param);
                ds.Tables[0].TableName = "ProjectMasterRecords";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectAssignedUserListForMilestoneEmail");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }
    }
}