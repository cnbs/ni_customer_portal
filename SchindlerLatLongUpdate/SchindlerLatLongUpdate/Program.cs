﻿using SchindlerLatLongUpdate.DAL;
using System;
using System.Configuration;

namespace SchindlerLatLongUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            int flag = 0;
            UpdateLatLongInfo updateLatLong = new UpdateLatLongInfo();
            
            Console.WriteLine("Update latitude/longitude...");
            flag = updateLatLong.UpdateInfo();
            if(flag == 1)
                Console.WriteLine("Data successfully updated...");
            else
                Console.WriteLine("Update failed...");

        }
    }
}
