﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using SchindlerLatLongUpdate.Model;

namespace SchindlerLatLongUpdate.DAL
{
    class UpdateLatLongInfo
    {
        DataAccessLayer dal = new DataAccessLayer();

        public int UpdateInfo()
        {
            try
            {
                using (var client = new WebClient()) //WebClient  
                {
                    client.Headers.Add("Content-Type:application/json"); //Content-Type  
                    client.Headers.Add("Accept:application/json");
                    var result = client.DownloadString(ConfigurationManager.AppSettings["BaseURL"].ToString() + "api/UpdateGeoLocation");
                    CommonData common= JsonConvert.DeserializeObject<CommonData>(result);
                    return Convert.ToInt32(common.Status);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + ex.Message + "\n");
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateLatLongInfo");
            }
            return 0;
        }
    }
}
