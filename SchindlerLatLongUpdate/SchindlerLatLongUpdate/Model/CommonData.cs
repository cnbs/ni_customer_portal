﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerLatLongUpdate.Model
{
    class CommonData
    {
        public string Status { get; set; }
        public object List { get; set; }
        public string Message { get; set; }
        public int TotalProjects { get; set; }
        public int Count { get; set; }
        public int UserCount { get; set; }
        public int MailCount { get; set; }
        public object Data { get; set; }
    }
}
