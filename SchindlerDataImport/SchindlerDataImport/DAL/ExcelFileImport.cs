﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerDataImport.DAL
{
    class ExcelFileImport
    {
        DataAccessLayer dal = new DataAccessLayer();

        public int ImportExcelFile()
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                string FilePath = ConfigurationManager.AppSettings["FilePath"].ToString() + ConfigurationManager.AppSettings["FileName"].ToString(); 
                string SheetNo = ConfigurationManager.AppSettings["Sheet"].ToString();

                param[0] = new SqlParameter("@FileLocation", FilePath);
                param[1] = new SqlParameter("@ExcelSheetName", SheetNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SP_ExcelFile_Import, param);
                ds.Tables[0].TableName = "ImportExcelFile";

                dal.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n"+ ex.Message + "\n");
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ImportExcelFile");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public int ProjectMasterImport()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SP_ProjectMaster_MainImport);
                ds.Tables[0].TableName = "ProjectMasterImport";

                dal.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + ex.Message + "\n");
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectMasterImport");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public int CompanyMasterImport()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SP_CompanyMaster_MainImport);
                ds.Tables[0].TableName = "CompanyMasterImport";

                dal.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + ex.Message + "\n");
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyMasterImport");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public int JobMasterImport()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SP_JobMaster_MainImport);
                ds.Tables[0].TableName = "JobMasterImport";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - JobMasterImport");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }
    }
}
