﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerDataImport.DAL
{
    class StoredProcedure
    {
        //Actual
        public const string SP_ExcelFile_Import = "usp_ImportExcelFile";
        public const string SP_ProjectMaster_MainImport = "sp_ProjectMaster_MainImport";
        public const string SP_CompanyMaster_MainImport = "sp_CompanyMaster_MainImport";
        public const string SP_JobMaster_MainImport = "sp_JobMaster_MainImport";

        //Testing
        //public const string SP_ExcelFile_Import = "usp_ImportExcelFile";
        //public const string SP_ProjectMaster_MainImport = "sp_ProjectMaster_MainImport_Demo_C";
        //public const string SP_CompanyMaster_MainImport = "sp_CompanyMaster_MainImport_Demo_A";
        //public const string SP_JobMaster_MainImport = "sp_JobMaster_MainImport_Demo_B";
    }
}
