﻿using SchindlerDataImport.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerDataImport
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelFileImport fileimport = new ExcelFileImport();

            int flag = 0;

            Console.WriteLine("Importing data from File...");
            flag = fileimport.ImportExcelFile();

            Console.WriteLine("\nWriting Data into Project Master table...");
            flag = fileimport.ProjectMasterImport();

            Console.WriteLine("\nWriting Data into Company Master table...");
            flag = fileimport.CompanyMasterImport();

            Console.WriteLine("\nWriting Data into Job Master table...");
            flag = fileimport.JobMasterImport();
            
        }
    }
}
