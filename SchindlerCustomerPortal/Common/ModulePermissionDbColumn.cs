﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    public enum ModulePermissionDbColumn
    {
        View_Permission,
        Add_Permission,
        Delete_Permission,
        Print_Permission,
        Edit_Permission,
        Upload_Permission
    }
}