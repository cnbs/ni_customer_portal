﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    public class CommonMessage
    {
        public static string AddedCompany = "Company added successfully";

        public static string AlreadyExistsCompany = "Company already exists";
        public static string DeletedCompany = "Company deleted successfully";
        public static string FailedDelete = "Failed to delete";
        public static string FailedInsert = "Failed to insert";
        public static string FailedUpdate = "Failed to update";
        public static string UpdatedCompany = "Company updated successfully";
        public static string NotExist = "Not Exist";
        public static string Exist = "Exist";
        public static string Success = "Success";
        public static string Invalid = "Invalid";
        public static string EmailSendError = "There is some issue with mail send, please contact administrator";
        public static string Registered = "Registered";
        public static string AccessUpdatedSuccessfully = "Access updated succcessfully";
        public static string InvalidError = "Some error occured";
        public static string Error = "Something went wrong";
        public static string PostMessageSuccess = "Message has been posted successfully";
        public static string PostReplyMessageSuccess = "Reply has been posted successfully";
        public static string PostInsertedSuccess = "Post has been done successfully";
        public static string CompanyAccountNumberDuplicate = "Company Account Number already exist";

    }
}