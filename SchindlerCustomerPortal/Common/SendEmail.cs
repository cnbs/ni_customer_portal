﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.Common
{
    public class SendEmail
    {

        public static bool Send(string from, string to, string subject, string body)
        {

            try
            {
                subject = subject.Replace("\r", "");

                MailMessage mail = new MailMessage();
                MailAddress maFrom = new MailAddress(from);

                mail.From = maFrom;

                char toCSep1 = ',';

                if (to.IndexOf(',') > 0)
                {
                    toCSep1 = ',';
                }

                else if (to.IndexOf(';') > 0)
                {
                    toCSep1 = ';';
                }

                string[] emailTo = to.Split(toCSep1);

                if (emailTo.Length > 0)
                {
                    foreach (string str in emailTo)
                    {
                        mail.To.Add(new MailAddress(str));
                    }
                }


                //Added By Manoj Mevada
                string bcc = "";
                bcc = System.Configuration.ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();
                if (bcc != "")
                {
                    mail.Bcc.Add(bcc);
                }
                
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;


                SmtpClient smtpMailObj = new SmtpClient();


                string hostname = "";
                smtpMailObj.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpport"].ToString());
                hostname = System.Configuration.ConfigurationManager.AppSettings["hostname"].ToString();
                smtpMailObj.Host = hostname;
                smtpMailObj.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["mailusername"], WebConfigurationManager.AppSettings["mailuserpassword"]);
                //var enableSSL = (System.Configuration.ConfigurationManager.AppSettings["EnableSSL"].ToString() == "true") ? true : false;
                //smtpMailObj.EnableSsl = enableSSL;
                smtpMailObj.Send(mail);

                return true;
            }

            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendEmail");
                return false;
                throw ex;
            }

            finally
            {

            }

        }


        public static bool SendMailtoMultiple(string from, string to, string cc, string bcc, string subject, string body)
        {

            try
            {
                subject = subject.Replace("\r", "");

                MailMessage mail = new MailMessage();
                MailAddress maFrom = new MailAddress(from);

                mail.From = maFrom;

                char toCSep1 = ',';

                if (to.IndexOf(',') > 0)
                {
                    toCSep1 = ',';
                }

                else if (to.IndexOf(';') > 0)
                {
                    toCSep1 = ';';
                }

                string[] emailTo = to.Split(toCSep1);

                if (emailTo.Length > 0)
                {
                    foreach (string str in emailTo)
                    {
                        mail.To.Add(new MailAddress(str.Trim()));
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                    {
                        mail.CC.Add(new MailAddress(CCEmail.Trim())); //Adding Multiple CC email Id
                    }
                }

                if (!string.IsNullOrEmpty(bcc))
                {
                    string[] bccid = bcc.Split(',');

                    foreach (string bccEmailId in bccid)
                    {
                        mail.Bcc.Add(new MailAddress(bccEmailId.Trim())); //Adding Multiple BCC email Id
                    }
                }

                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;


                SmtpClient smtpMailObj = new SmtpClient();


                string hostname = "";
                smtpMailObj.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpport"].ToString());
                hostname = System.Configuration.ConfigurationManager.AppSettings["hostname"].ToString();
                smtpMailObj.Host = hostname;
                smtpMailObj.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["mailusername"], WebConfigurationManager.AppSettings["mailuserpassword"]);
                //var enableSSL = (System.Configuration.ConfigurationManager.AppSettings["EnableSSL"].ToString() == "true") ? true : false;
                //smtpMailObj.EnableSsl = enableSSL;
                smtpMailObj.Send(mail);

                return true;
            }

            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendMailtoMultiple");
                return false;
                throw ex;
            }

            finally
            {

            }

        }

    }
}