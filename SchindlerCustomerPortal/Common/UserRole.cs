﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    public enum UserRole
    {
        SuperAdmin=1,
        CustomerAdmin = 2,
        Employee=3,
        Customer=4
    }
}