﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using System.Data.SqlClient;

using System.Configuration;
using System.Globalization;

using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using System.Net.Sockets;
using System.Drawing;

namespace SchindlerCustomerPortal.Common
{
    public static class StaticUtilities
    {
        public static string ErrorFolderPath = ConfigurationManager.AppSettings["ErrorPath"].ToString();
        public static string EventLogName = ConfigurationManager.AppSettings["EventLogName"].ToString();

        public static T Cast<T>(this Object myobj)
        {
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();
            PropertyInfo propertyInfo;
            object value;
            foreach (var memberInfo in members)
            {
                propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                value = myobj.GetType().GetProperty(memberInfo.Name).GetValue(myobj, null);

                propertyInfo.SetValue(x, value, null);
            }
            return (T)x;
        }
        public static string getEmailBody(string moduleName, string fileName, Dictionary<string, string> maildictionary)
        {
            string emailBody;

            string filePath;

            filePath = SchindlerCustomerPortal.Models.CommonConfiguration.EmailTemplatePath + moduleName + "\\" + fileName;

            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                emailBody = streamReader.ReadToEnd();
            }

            foreach (KeyValuePair<string, string> pair in maildictionary)
            {
                emailBody = emailBody.Replace("[[" + pair.Key + "]]", pair.Value);
            }


            return emailBody;
        }

        public static string getNotificationBody(string moduleName, string fileName, Dictionary<string, string> maildictionary)
        {
            string emailBody;

            string filePath;

            filePath = SchindlerCustomerPortal.Models.CommonConfiguration.NotificationTemplatePath + moduleName + "\\" + fileName;

            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                emailBody = streamReader.ReadToEnd();
            }

            foreach (KeyValuePair<string, string> pair in maildictionary)
            {
                emailBody = emailBody.Replace("[[" + pair.Key + "]]", pair.Value);
            }


            return emailBody;
        }
        public static bool CheckUserPermission(int userId, string moduleName)
        {
            IUserService _userService = new UserService();

            return _userService.CheckUserModulePermission(userId, moduleName).HasViewPermission;
        }

        public static Int32 ApplicationAuditRecord(string PageName, string Message, string UserID, int RoleID, string Operation, string SessionID, string IPAddress, string Clientbrowser, string ClientOS, string ServerIP, string ServerName, out int id)
        {
            SqlParameter[] param = new SqlParameter[12];
            int result;
            DataAccessLayer dal = new DataAccessLayer();

            try
            {
                param[0] = new SqlParameter("@PageName", PageName);
                param[1] = new SqlParameter("@Message", Message);
                param[2] = new SqlParameter("@UserID", UserID);
                param[3] = new SqlParameter("@RoleID", RoleID);
                param[4] = new SqlParameter("@Operation", Operation);
                param[5] = new SqlParameter("@SessionID", SessionID);
                param[6] = new SqlParameter("@IPAddress", IPAddress);
                param[7] = new SqlParameter("@RetVal", SqlDbType.Int);
                param[8] = new SqlParameter("@Clientbrowser", Clientbrowser);
                param[9] = new SqlParameter("@ClientOS", ClientOS);
                param[10] = new SqlParameter("@ServerIP", ServerIP);
                param[11] = new SqlParameter("@ServerName", ServerName);

                param[7].Direction = ParameterDirection.Output;

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.ApplicationAuditMaster, param);

                if (result >= 1)
                {
                    id = (int)param[7].Value;
                    return id;
                }
                else
                {
                    id = 0;
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }


        //StaticUtilities.cs

        public static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 160;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }


        public static string getIP()
        {
            string hostName = Dns.GetHostName();
            string IpAdd = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return IpAdd;
        }

        public static string getClientbrowser()
        {
            string browserName = "";

            if (HttpContext.Current.Request.UserAgent.Contains("Edge"))
            {
                string version = Convert.ToString(HttpContext.Current.Request.UserAgent);
                string j = "0";

                if (!string.IsNullOrEmpty(version))
                {
                    int i = version.IndexOf("Edge");
                    j = version.Substring(i + 5, 4).ToString();
                }

                browserName = "Edge" + " Version - " + j;
            }
            else
            {
                var browserDetail = HttpContext.Current.Request.Browser;
                browserName = browserDetail.Browser + " Version - " + browserDetail.Version;
            }

            return browserName;
        }

     
        public static string getClientOS()
        {
            string userAgent = Convert.ToString(HttpContext.Current.Request.UserAgent);
            string osName = string.Empty;

            //HttpBrowserCapabilities browse = HttpContext.Current.Request.Browser;
            //string platform = browse.Platform;

            if (userAgent.IndexOf("Windows NT 6.3") > 0)
            {
                osName = "Windows 8.1";
            }
            else if (userAgent.IndexOf("Windows NT 6.2") > 0)
            {
                osName = "Windows 8";
            }
            else if (userAgent.IndexOf("Windows NT 6.1") > 0)
            {
                osName = "Windows 7";
            }
            else if (userAgent.IndexOf("Windows NT 6.0") > 0)
            {
                osName = "Windows Vista";
            }
            else if (userAgent.IndexOf("Windows NT 5.2") > 0)
            {
                osName = "Windows Server 2003; Windows XP x64 Edition";
            }
            else if (userAgent.IndexOf("Windows NT 5.1") > 0)
            {
                osName = "Windows XP";
            }
            else if (userAgent.IndexOf("Windows NT 5.01") > 0)
            {
                osName = "Windows 2000, Service Pack 1 (SP1)";
            }
            else if (userAgent.IndexOf("Windows NT 5.0") > 0)
            {
                osName = "Windows 2000";
            }
            else if (userAgent.IndexOf("Windows NT 4.0") > 0)
            {
                osName = "Microsoft Windows NT 4.0";
            }
            else if (userAgent.IndexOf("Win 9x 4.90") > 0)
            {
                osName = "Windows Millennium Edition (Windows Me)";
            }
            else if (userAgent.IndexOf("Windows 98") > 0)
            {
                osName = "Windows 98";
            }
            else if (userAgent.IndexOf("Windows 95") > 0)
            {
                osName = "Windows 95";
            }
            else if (userAgent.IndexOf("Windows CE") > 0)
            {
                osName = "Windows CE";
            }
            else
            {
                osName = string.Empty;
            }


            return osName;
        }

        public static string getServerIP()
        {
            string strHostName = System.Net.Dns.GetHostName();
            //IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName()); <-- Obsolete
            IPHostEntry ipHostInfo = Dns.GetHostEntry(strHostName);
            foreach (IPAddress address in ipHostInfo.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    return address.ToString();

            }
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            return ipAddress.ToString();
        }

        public static string getServerName()
        {
            string strHostName = System.Net.Dns.GetHostName();
            return strHostName.ToString();
        }

        public static string getVersion()
        {
            DateTime date = new DateTime();
            date = DateTime.Now;
            
            return long.Parse(date.ToString("yyyyMMddHH")).ToString();
        }

        public class ErrorLogAdditionalinfoEntity
        {
            public string AdditionalInfo { get; set; }
            public string AppUserName { get; set; }
            public string WindowUserName { get; set; }
            public string IpAddress { get; set; }
            public string StoreProcedure { get; set; }
            public string URL { get; set; }
        }

        /// <summary>
		/// This method WriteToErrorLog is developed for : log any error in text file
		/// </summary>
		/// <param name="objException">The obj exception.</param>
		/// <param name="title">The title.</param>
		/// <author>
		/// Developed By :  manoj on 5/2/2012 5:08 PM
		/// </author>
		public static void WriteToErrorLog(Exception objException, ErrorLogAdditionalinfoEntity errorLogAdditionalinfoEntity)
        {
            if (!System.IO.Directory.Exists(ErrorFolderPath))
            {
                System.IO.Directory.CreateDirectory(ErrorFolderPath);
            }

            FileStream errorLog =
                new FileStream(
                    ErrorFolderPath + "\\errlog" + System.DateTime.Now.Date.ToString("yyyyMMdd") + ".txt",
                    FileMode.OpenOrCreate,
                    FileAccess.ReadWrite);
            StreamWriter errorLogDataWrite = new StreamWriter(errorLog);
            errorLogDataWrite.Close();
            errorLog.Close();
            FileStream errorLogApp =
                new FileStream(
                    ErrorFolderPath + "\\errlog" + System.DateTime.Now.Date.ToString("yyyyMMdd") + ".txt",
                    FileMode.Append,
                    FileAccess.Write);
            StreamWriter errorLogDataAppend = new StreamWriter(errorLogApp);



            errorLogDataAppend.WriteLine("Source		: " + objException.Source.ToString().Trim());
            errorLogDataAppend.WriteLine("Method		: " + objException.TargetSite.Name.ToString());
            errorLogDataAppend.WriteLine("Date		: " + DateTime.Now.ToLongTimeString());
            errorLogDataAppend.WriteLine("Time		: " + DateTime.Now.ToShortDateString());
            errorLogDataAppend.WriteLine("Computer	: " + Dns.GetHostName().ToString());

            errorLogDataAppend.WriteLine("Error		: " + objException.Message.ToString().Trim());
            errorLogDataAppend.WriteLine("Stack Trace	: " + objException.StackTrace.ToString().Trim());
            if (errorLogAdditionalinfoEntity.AdditionalInfo != null)
            {
                errorLogDataAppend.WriteLine("AdditionalInfo: " + errorLogAdditionalinfoEntity.AdditionalInfo);
            }
            if (errorLogAdditionalinfoEntity.AppUserName != null)
            {
                errorLogDataAppend.WriteLine("Application User: " + errorLogAdditionalinfoEntity.AppUserName);
            }
            if (errorLogAdditionalinfoEntity.WindowUserName != null)
            {
                errorLogDataAppend.WriteLine("Window User Name: " + errorLogAdditionalinfoEntity.WindowUserName);
            }
            if (errorLogAdditionalinfoEntity.IpAddress != null)
            {
                errorLogDataAppend.WriteLine("IP Address: " + errorLogAdditionalinfoEntity.IpAddress);
            }
            if (errorLogAdditionalinfoEntity.StoreProcedure != null)
            {
                errorLogDataAppend.WriteLine("Store Procedure: " + errorLogAdditionalinfoEntity.StoreProcedure);
            }
            if (errorLogAdditionalinfoEntity.URL != null)
            {
                errorLogDataAppend.WriteLine("URL: " + errorLogAdditionalinfoEntity.URL);
            }
            errorLogDataAppend.WriteLine("^^-------------------------------------------------------------------^^");
            errorLogDataAppend.Flush();
            errorLogDataAppend.Close();
            errorLogApp.Close();

        }
        public static void WriteToErrorLogXML(Exception objException, ErrorLogAdditionalinfoEntity errorLogAdditionalinfoEntity)
        {
            if (!System.IO.Directory.Exists(ErrorFolderPath))
            {
                System.IO.Directory.CreateDirectory(ErrorFolderPath);
            }
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode errorsNode;
            if (!File.Exists(ErrorFolderPath + "\\errlog" + System.DateTime.Now.Date.ToString("yyyyMMdd") + ".xml"))
            {
                errorsNode = xmlDoc.CreateElement("Errors");
            }
            else
            {

                xmlDoc.Load(ErrorFolderPath + "\\errlog" + System.DateTime.Now.Date.ToString("yyyyMMdd") + ".xml");
                errorsNode = xmlDoc.SelectSingleNode("Errors");
            }

            XmlNode errorNode = xmlDoc.CreateElement("Error");

            XmlNode sourceNode = xmlDoc.CreateElement("Source");
            sourceNode.InnerText = objException.Source.ToString();
            errorNode.AppendChild(sourceNode);

            XmlNode methodNode = xmlDoc.CreateElement("Method");
            methodNode.InnerText = objException.TargetSite.Name.ToString();
            errorNode.AppendChild(methodNode);

            XmlNode dateTimeNode = xmlDoc.CreateElement("DateTime");
            dateTimeNode.InnerText = DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToLongTimeString();
            errorNode.AppendChild(dateTimeNode);

            XmlNode computerNode = xmlDoc.CreateElement("Computer");
            computerNode.InnerText = Dns.GetHostName().ToString();
            errorNode.AppendChild(computerNode);

            XmlNode errorInfoNode = xmlDoc.CreateElement("ErrorInfo");
            errorInfoNode.InnerText = objException.Message.ToString().Trim();
            errorNode.AppendChild(errorInfoNode);

            XmlNode stackTraceNode = xmlDoc.CreateElement("StackTrace");
            stackTraceNode.InnerText = objException.StackTrace.ToString().Trim();
            errorNode.AppendChild(stackTraceNode);

            XmlNode additionalInfoNode = xmlDoc.CreateElement("AdditionalInfo");
            additionalInfoNode.InnerText = errorLogAdditionalinfoEntity.AdditionalInfo != null ? errorLogAdditionalinfoEntity.AdditionalInfo.ToString() : "";
            errorNode.AppendChild(additionalInfoNode);

            XmlNode applicationUserNode = xmlDoc.CreateElement("ApplicationUser");
            applicationUserNode.InnerText = errorLogAdditionalinfoEntity.AppUserName != null ? errorLogAdditionalinfoEntity.AppUserName.ToString() : "";
            errorNode.AppendChild(applicationUserNode);

            XmlNode windowUserNameNode = xmlDoc.CreateElement("WindowUserName");
            windowUserNameNode.InnerText = errorLogAdditionalinfoEntity.WindowUserName != null ? errorLogAdditionalinfoEntity.WindowUserName.ToString() : "";
            errorNode.AppendChild(windowUserNameNode);

            XmlNode ipAddressNode = xmlDoc.CreateElement("IPAddress");
            ipAddressNode.InnerText = errorLogAdditionalinfoEntity.IpAddress != null ? errorLogAdditionalinfoEntity.IpAddress.ToString() : "";
            errorNode.AppendChild(ipAddressNode);

            XmlNode urlNode = xmlDoc.CreateElement("URL");
            urlNode.InnerText = errorLogAdditionalinfoEntity.URL != null ? errorLogAdditionalinfoEntity.URL.ToString() : "";
            errorNode.AppendChild(urlNode);
            if (errorsNode != null)
            {
                errorsNode.AppendChild(errorNode);
                xmlDoc.AppendChild(errorsNode);
            }

            if (objException.Message.StartsWith("Thread") == false)
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml("<Errors>" + errorNode.OuterXml.ToString() + "</Errors>");
                string htmlData = Transform(xmlDocument, ConfigurationManager.AppSettings["XSLT_Path"].ToString());
                xmlDoc.Save(ErrorFolderPath + "\\errlog" + System.DateTime.Now.Date.ToString("yyyyMMdd") + ".xml");
                HttpContext.Current.Application.Add("myEx", "<Errors>" + errorNode.OuterXml.ToString() + "</Errors>");

                HttpContext.Current.Response.Redirect("/Common/ErrorPage");
            }
        }
        public static string Transform(XmlDocument xmlDoc, string sXslPath)
        {
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(sXslPath);
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter myWriter = new XmlTextWriter(stringWriter);
            myXslTrans.Transform(xmlDoc, null, myWriter, null);

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("AppName", "Error Log");
            dic.Add("SendTime", System.DateTime.Now.ToString(CultureInfo.InvariantCulture));
            dic.Add("ErrorData", stringWriter.ToString());

            string subject = ConfigurationManager.AppSettings["ErrorLog_Subject"].ToString() +
                             System.DateTime.Now.ToString(CultureInfo.InvariantCulture);

            string Sbody = StaticUtilities.getEmailBody("ErrorLog", "ErrorNotifications.htm", dic);
            SchindlerCustomerPortal.Common.SendEmail.SendMailtoMultiple(ConfigurationManager.AppSettings["ErrorLog_From"].ToString(), ConfigurationManager.AppSettings["ErrorLog_To"].ToString(), ConfigurationManager.AppSettings["ErrorLog_CC"].ToString(), "", subject, Sbody);
            myWriter.Close();
            return stringWriter.ToString();
        }

    }
}