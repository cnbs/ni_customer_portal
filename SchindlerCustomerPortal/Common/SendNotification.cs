﻿using IOSNotification_Class;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SchindlerCustomerPortal.Common
{
    public class SendNotification
    {
        public static Boolean bsandbox = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["bsandbox"]);

        
        public static string p12fileName = System.Configuration.ConfigurationManager.AppSettings["p12fileName"].ToString();
        

        public static string p12password = System.Configuration.ConfigurationManager.AppSettings["p12password"].ToString();

        public static bool NotifyIOSDevice(string deviceID, string Subject, string Body)
        {

            try
            {
                var payload1 = new NotificationPayload(deviceID, Subject, Body);
              
                
                var notificationList = new List<NotificationPayload> { payload1};

                var push = new PushNotification(bsandbox, p12fileName, p12password);

                var rejected = push.SendToApple(notificationList);

                if (rejected.Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NotifyIOSDevice");
                throw ex;

            }
        }

        public static int NotifyAndroidDevice(string deviceID, string Subject, string Body)
        {
            try
            {
                // Token ID
                
                string regId = deviceID;
                int success = 0;
                int abc = regId.Count();
                // Server API Key
                var applicationID = System.Configuration.ConfigurationManager.AppSettings["AndroidApplicationID"].ToString();


                // Project ID
                var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();

                //var value = Username + '-' + Message;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = regId,
                    notification = new
                    {
                        body = Body,
                        title = Subject,
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;

                                JObject jsondata = JObject.Parse(str);
                                 success = Convert.ToInt32(jsondata["success"].ToString());


                            }
                        }
                    }
                }
                return success;

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NotifyAndroidDevice");
                throw ex;

            }
        }
              
    }
}