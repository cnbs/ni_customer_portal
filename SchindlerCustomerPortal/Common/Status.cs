﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    public enum Status
    {
        Active=1,
        Inactive = 2,
        Registered=3,
        Pending=4,
        Progress=5,
        Verified=17,
        Approved=18,
        Disapproved=19
    }
}