﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    //public static class ApplicationOperation
    //{
    //    public const string List = "List";
    //    public const string Insert = "Insert";
    //    public const string Update = "Update";
    //    public const string Delete = "Delete";
    //    public const string ModulePermission = "ModulePermission";
    //    public const string ProjectMapping = "ProjectMapping";
    //    public const string ChangePassword = "ChangePassword";
    //    public const string Verification = "Verification";
    //    public const string Activation = "Activation";
    //    public const string Approval = "Approval";
    //    public const string Download = "Download";
    //    public const string ReadyToPull = "ReadyToPull";
    //}

    public enum ApplicationOperation
    {
        UserManagement_ListUser,
        UserManagement_AddUser,
        UserManagement_UpdateUser,
        UserManagement_DeleteUser,
        EmployeeUserModulePermission,
        EmployeeManagement_ListEmployee,
        EmployeeManagement_AddEmployee,
        EmployeeManagement_UpdateEmployee,
        EmployeeManagement_DeleteEmployee,
        ChangePassword,
        AccountManagement_List,
        AccountManagement_Update,
        ResourceCenter_List,
        ResourceCenter_Add,
        ResourceCenter_Update,
        ResourceCenter_Delete,
        ResourceCenter_Download,
        HelpManagement_List,
        HelpManagement_Add,
        HelpManagement_Update,
        HelpManagement_Delete,

        HelpManagement_HeaderHelp,
        ProjectMapping,
        RegisterPage,
        MailBox_List,
        MailBox_NewMail,
        MailBox_ReplyMail,
        MailBox_ForwardMail,
        MailBox_DeleteMail,
        MailBox_DraftMail,
        MailBox_ReadMail,
        MailBox_SendMail,
        ForgotPassword,


        //For Project Management
        ProjectManagement_MasterList,
        ProjectManagement_MasterView,
        ProjectManagement_MasterJobList,
        ProjectManagement_ViewProjectRecord,
        ProjectManagement_ViewProjectJobRecord,
        ProjectManagement_ProjectDocumentList,
        ProjectManagement_ContactList,
        ProjectManagement_AddCommunication,
        ProjectManagement_AddProjectContact,
        ProjectManagement_DeleteProjectContact,
        ProjectManagement_UpdateProjectContact,
        ProjectManagement_UpdateProjectMilestone,
        ProjectManagement_AddProjectMilestone,
        ProjectManagement_ProjectMilestoneList,
        ProjectManagement_DeleteProjectMilestone,
        ProjectManagement_SaveReadyToPullMilestone,
        ProjectManagement_ProjectContactList,
        ProjectManagement_CommunicationList,
        ProjectManagement_AddProjectDocument,
        ProjectManagement_UpdateProjectDocument,
        ProjectManagement_DeleteProjectDocument,
        ProjectManagement_JobView,

        Dashboard,

        // Office Manager
        OfficeManager_List,
        OfficeManager_Add,
        OfficeManager_Edit,
        OfficeManager_Delete,

        //AnnouncementMessage
        AnnouncementMessage_List,
        AnnouncementMessage_Add,
        AnnouncementMessage_Edit,
        AnnouncementMessage_Delete,
        AnnouncementMessage_Update,
    }
}