﻿using SchindlerCustomerPortal.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.Common
{
    public class Utility
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetCountyList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetCountry);
                ds.Tables[0].TableName = "CountryList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> countryList = ds.Tables[0].AsEnumerable().ToList();

            return countryList;
        }

        public List<DataRow> GetStatusList(string module)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Module", module);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetStatus, param);
                ds.Tables[0].TableName = "StatusList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> statusList = ds.Tables[0].AsEnumerable().ToList();

            return statusList;
        }

        public List<DataRow> GetStateList(int CountryID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetStateListFromCountry, param);
                ds.Tables[0].TableName = "StateList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> stateList = ds.Tables[0].AsEnumerable().ToList();
            return stateList;
        }



        public List<DataRow> GetCityList(int CountryID, string StateCode)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);
                param[1] = new SqlParameter("@StateCode", StateCode);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCityListFromState, param);
                ds.Tables[0].TableName = "CityList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> cityList = ds.Tables[0].AsEnumerable().ToList();
            return cityList;
        }
        public List<DataRow> GetCityListAutoComplete(int CountryID, string StateCode, string searchterm)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);
                param[1] = new SqlParameter("@StateCode", StateCode);
                param[2] = new SqlParameter("@SearchTerm", searchterm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CityMaster_SelectAutoCompleteNew, param);
                ds.Tables[0].TableName = "CityList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> cityList = ds.Tables[0].AsEnumerable().ToList();
            return cityList;
        }

        public static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 160;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }



        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        //public List<DataRow> GetRoleList()
        //{

        //    DataSet ds = new DataSet();

        //    try
        //    {

        //        ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetRoles);
        //        ds.Tables[0].TableName = "RoleList";

        //        dal.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    List<DataRow> roleList = ds.Tables[0].AsEnumerable().ToList();
        //    return roleList;
        //}


        public List<DataRow> GetDocTypeList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetDocumentTypes);
                ds.Tables[0].TableName = "DocumentList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> doctypeList = ds.Tables[0].AsEnumerable().ToList();

            return doctypeList;
        }

        public List<DataRow> GetMilestoneTypeList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetMilestoneTypes);
                ds.Tables[0].TableName = "MilestoneList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> miletypeList = ds.Tables[0].AsEnumerable().ToList();

            return miletypeList;
        }

        public List<DataRow> GetDesignationList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.SelectAllDesignation);
                ds.Tables[0].TableName = "Designation";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> designationList = ds.Tables[0].AsEnumerable().ToList();

            return designationList;
        }

        public List<DataRow> GetMilestoneListOfJob(int JobId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@jobid", JobId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMilestoneListByJob, param);
                ds.Tables[0].TableName = "Milestonelist";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> milestoneList = ds.Tables[0].AsEnumerable().ToList();

            return milestoneList;
        }

        public List<DataRow> GetProjectStatusList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetProjectStatus);
                ds.Tables[0].TableName = "StatusList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> statusList = ds.Tables[0].AsEnumerable().ToList();

            return statusList;
        }


        public List<DataRow> GetCityListAutoCompleteForAdd(string searchTerm, int CountryID, string StateCode)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);
                param[1] = new SqlParameter("@StateCode", Convert.ToInt32(StateCode));
                param[2] = new SqlParameter("@SearchTerm", searchTerm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCityListForAutoComplete, param);
                ds.Tables[0].TableName = "CityList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> cityList = ds.Tables[0].AsEnumerable().ToList();
            return cityList;
        }

        public List<DataRow> GetCompanyList(string CompanyName)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@SearchTerm", CompanyName);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCompanyList, param);
                ds.Tables[0].TableName = "CompanyList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> companyList = ds.Tables[0].AsEnumerable().ToList();

            return companyList;
        }

        public List<DataRow> GetProjectListbyCompanyId(string ProjectName, int CompanyId, int RoleId)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@SearchTerm", ProjectName);
                param[1] = new SqlParameter("@companyid", CompanyId);
                param[2] = new SqlParameter("@RoleId", RoleId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectbyCompanyIdForDocument, param);
                ds.Tables[0].TableName = "ProjectList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> projectList = ds.Tables[0].AsEnumerable().ToList();

            return projectList;
        }

        public List<DataRow> GetJobListbyProjectId(string JobName, int ProjectId)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@SearchTerm", JobName);
                param[1] = new SqlParameter("@ProjId", ProjectId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobByProjectIdList, param);
                ds.Tables[0].TableName = "JobList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> jobtList = ds.Tables[0].AsEnumerable().ToList();

            return jobtList;
        }


        public List<DataRow> GetCityListAutoComplete(string searchTerm, int CountryID, string StateCode)

        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CountryID", CountryID);
                param[1] = new SqlParameter("@StateCode", Convert.ToInt32(StateCode));
                param[2] = new SqlParameter("@SearchTerm", searchTerm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCityListForAutoComplete, param);
                ds.Tables[0].TableName = "CityList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> cityList = ds.Tables[0].AsEnumerable().ToList();
            return cityList;
        }


        public List<DataRow> GetResourceDocTypeList()
        {
            DataSet ds = new DataSet();

            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetResourceDocumentTypes);
                ds.Tables[0].TableName = "DocumentList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> doctypeList = ds.Tables[0].AsEnumerable().ToList();

            return doctypeList;
        }

        public List<DataRow> GetAlertMessageByModule(string moduleName)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@ModuleName", moduleName);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.AlertMessageByModule, param);
                ds.Tables[0].TableName = "AlertMessage";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAlertMessageByModule");
                throw ex;
            }

            List<DataRow> milestoneList = ds.Tables[0].AsEnumerable().ToList();

            return milestoneList;
        }

        public List<DataRow> GetJobListbyProjectNo(string JobName, int ProjectNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@SearchTerm", JobName);
                param[1] = new SqlParameter("@ProjectNo", ProjectNo);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobByProjectIdList, param);
                ds.Tables[0].TableName = "JobList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> jobtList = ds.Tables[0].AsEnumerable().ToList();

            return jobtList;
        }
    }
}