﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Common
{
    public static class ModuleName
    {
        public const string Administrator = "Admin";
        public const string AccountManagement = "Account Management";
        public const string EmployeeManagement = "Employee Management";
        public const string Invoice = "Invoice";
        public const string ProjectManagement = "Project Management";
        public const string ProjectPlanner = "Project Planner";
        public const string DashBoard = "DashBoard";
        public const string HelpManagement = "Help Management";
        public const string DocManagement = "Documents Management";
        public const string ProjectDocumentManagement = "Project Document Management";
        public const string UserManagement = "User Management";

        //public string LoginProcedureName = "Administrator"; 

        public const string ResourceCenter = "Resource Center";
        public const string OfficeManager = "Office Manager";
        public const string Contacts = "Contacts";
        public const string MyProfile = "My Profile";

        public const string Register = "Register";
        public const string MailBox = "MailBox";
        public const string ForgotPassword = "ForgotPassword";
        public const string AuditTrail = "Audit Trail";
        public const string MyProjects = "My Projects";
        public const string Announcement = "Announcement Message";
        public const string CalendarView = "Calendar";
        public const string MapView = "Map View";
    }
}