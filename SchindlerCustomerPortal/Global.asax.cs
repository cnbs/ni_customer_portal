﻿using Newtonsoft.Json;
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Controllers;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Login;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.User;
using SchindlerCustomerPortal.Services.Announcement;
using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
//Testing
namespace SchindlerCustomerPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        protected void Application_BeginRequest()
        {
            ////Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddMilliseconds(-1));
            //Response.Cache.SetNoStore();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
            {
                HttpContext.Current.Cache.Remove(Convert.ToString(entry.Key));
            }
        }

      
        protected void Application_Error()
        {
            // An error has occured on a .Net page.
            var serverError = Server.GetLastError() as HttpException;
            HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                  
            if (null != serverError)
            {
                int errorCode = serverError.GetHttpCode();

                if (404 == errorCode)
                {
                    RouteData routeData = new RouteData();
                    routeData.Values.Add("controller", "Common");
                    routeData.Values.Add("action", "Error404Page");
                    routeData.Values.Add("Summary", "Error");
                    routeData.Values.Add("Description", serverError.Message);
                    IController controller = new CommonController();
                    controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                    //Response.Redirect("/Common/ErrorPage");
                    // Clear the error from the server
                    Server.ClearError();
                }

                else
                {
                    if (nameCookie == null)
                    {
                        RouteData routeData = new RouteData();
                        routeData.Values.Add("controller", "Login");
                        routeData.Values.Add("action", "UserLogin");
                        routeData.Values.Add("Summary", null);
                        routeData.Values.Add("Description", null);
                        IUserService userService = new UserService();
                        ICommonService commonService = new CommonService();
                        //IAnnouncementService announcementService = new AnnouncementService();
                        IController controller = new LoginController(userService, commonService);
                        controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                        //Response.Redirect("/Common/ErrorPage");
                        // Clear the error from the server
                        Server.ClearError();
                    }                  
                }
            }

            if (serverError==null)
            {
                if (nameCookie == null)
                {
                    RouteData routeData = new RouteData();
                    routeData.Values.Add("controller", "Login");
                    routeData.Values.Add("action", "UserLogin");
                    routeData.Values.Add("Summary", null);
                    routeData.Values.Add("Description", null);
                    //IController controller = new CommonController();
                    IUserService userService = new UserService();
                    ICommonService commonService = new CommonService();
                    //IAnnouncementService announcementService = new AnnouncementService();
                    IController controller = new LoginController(userService, commonService);
                    controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                    //Response.Redirect("/Common/ErrorPage");
                    // Clear the error from the server
                    Server.ClearError();
                }
                else
                {
                    Exception exLastError = Server.GetLastError();
                    RouteData routeData = new RouteData();
                    routeData.Values.Add("controller", "Common");
                    routeData.Values.Add("action", "ErrorPage");
                    routeData.Values.Add("Summary", "Error");
                    routeData.Values.Add("Description", exLastError.Message);
                    IController controller = new CommonController();
                    controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                    //Response.Redirect("/Common/ErrorPage");
                    // Clear the error from the server
                    Server.ClearError();
                }
            }

            //Exception exLastError = Server.GetLastError();
            ////HttpException htexception = Server.GetLastError();
                    
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {

                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserId = serializeModel.UserId;
                newUser.Email = serializeModel.Email;
                newUser.FirstName = serializeModel.FirstName;
                newUser.LastName = serializeModel.LastName;
                newUser.CompanyID = serializeModel.CompanyID;
                newUser.ProfilePic = serializeModel.ProfilePic;
                newUser.CompanyName = serializeModel.CompanyName;
                newUser.RoleId = serializeModel.RoleId;
                newUser.SessionId = serializeModel.SessionID;
                HttpContext.Current.User = newUser;
            }

        }


    }

}
