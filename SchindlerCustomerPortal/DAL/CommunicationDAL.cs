﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class CommunicationDAL
    {
        DataAccessLayer dal = new DataAccessLayer();


        public List<DataRow> AddProjectCommunication(CommunicationEntity obj)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@ProjectNo", obj.ProjectNo);
                param[1] = new SqlParameter("@JobNo", obj.JobNo);
                param[2] = new SqlParameter("@UserId", obj.UserId);
                param[3] = new SqlParameter("@MessageText", obj.Message);
                param[4] = new SqlParameter("@NotifiedTo", obj.NotifyToEmail);
                param[5] = new SqlParameter("@AttachmentPath", obj.AttachmentPath);
                param[6] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[7] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[8] = new SqlParameter("@SessionID", obj.SessionID);
                param[9] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.AddCommunicationMessage, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectCommunication");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public List<DataRow> GetAllCommunicationList(int projectNo, string jobNo, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@ProjectNo", projectNo);
                param[1] = new SqlParameter("@JobNo", jobNo);
                param[2] = new SqlParameter("@UserId", UserId);

                //For Audit Trail
                param[3] = new SqlParameter("@appAuditID", appAuditID);
                param[4] = new SqlParameter("@SessionID", SessionID);
                param[5] = new SqlParameter("@ApplicationOperation", ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetAllCommunicationList, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllCommunicationList");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }

        public List<DataRow> GetPostlistByProjectId(int projectid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@ProjectId", projectid);
          
            
            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetAllPostByProjectId, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                 
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;

        }

        //public List<DataRow> GetSinglePostByIdForPostDetail(CommunicationThreadSearchInfo obj)
        //{
        //    DataSet ds = new DataSet();
        //    SqlParameter[] param = new SqlParameter[1];
        //    try
        //    {
        //        param[0] = new SqlParameter("@postid",obj.Id);
        //        ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetPostById, param);
        //        ds.Tables[0].TableName = "CompanyMasterDetails";
        //        dal.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogClass lm = new ErrorLogClass();
        //        lm.CreateLogFiles();
        //        var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
        //        lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
        //        throw ex;
        //    }
        //    var postobj = ds.Tables[0].AsEnumerable().ToList();
        //    return postobj;

        //}


        public List<DataRow> GetPostByProjectId(PostEntity obj)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@ProjectId", obj.ProjectId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.PostSelectByProjectId, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public int InsertCommunicationThread(ThreadEntity obj)
        { 
            int result = 0;
            SqlParameter[] param = new SqlParameter[14];
            //DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@ParentThreadId", obj.ParentThreadId);
                param[1] = new SqlParameter("@PostId", obj.PostId);
                param[2] = new SqlParameter("@ThreadType", obj.ThreadType);
                param[3] = new SqlParameter("@Title", obj.Title);
                param[4] = new SqlParameter("@Content", obj.Content);
                param[5] = new SqlParameter("@NotifyToEmail", obj.strNotifyToEmail);
                param[6] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[7] = new SqlParameter("@CreatedOn", obj.CreatedOn);
                param[8] = new SqlParameter("@IsActive", obj.IsActive);
                param[9] = new SqlParameter("@IsReply", obj.IsReply);
                param[10] = new SqlParameter("@PostBy", obj.PostBy);
                param[11] = new SqlParameter("@PostByEmail", obj.PostByEmail??string.Empty);
                param[12] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[13] = new SqlParameter("@SessionID", obj.SessionID);  

                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.InsertCommunicationThread, param);
                 
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Communication");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }

        public List<DataRow> GetAllThreadPostById(int postid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@postid", postid);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetAllThreadPostById, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }

        public List<DataRow> GetSingleThreadById(int threadid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@ThreadId", threadid);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CommunicationThread_Select_ById, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Communication");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public int InsertCommunicationPost(PostEntity obj)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[8];            
            try
            {

                param[0] = new SqlParameter("@ProjectId", obj.ProjectId);
                param[1] = new SqlParameter("@PostTitle", obj.PostTitle);
                param[2] = new SqlParameter("@PostContent", obj.PostContent);
                param[3] = new SqlParameter("@ParentPostId", obj.ParentPostId);
                param[4] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[5] = new SqlParameter("@CreatedOn", obj.CreatedOn);
                param[6] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[7] = new SqlParameter("@SessionID", obj.SessionID);  
                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.CommunicationProjectPost_Insert, param);

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Communication- InsertCommunicationPostByProject");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }


        public List<DataRow> GetPostById(int postid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@postid", postid);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.Post_Select_ById, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


    }
}