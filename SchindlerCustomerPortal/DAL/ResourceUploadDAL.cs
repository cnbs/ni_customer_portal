﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Resource;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class ResourceUploadDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> ResourceSelect(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[6] = new SqlParameter("@RoleId", model.RoleID);
                param[7] = new SqlParameter("@appAuditID", model.appAuditID);
                param[8] = new SqlParameter("@SessionID", model.SessionID);
                param[9] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[10] = new SqlParameter("@UserID", model.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceSelect, param);
                ds.Tables[0].TableName = "ResourceDetails";

                count = Convert.ToInt32(param[5].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceSelect");
                throw ex;
            }

            List<DataRow> ResourceData = ds.Tables[0].AsEnumerable().ToList();
            return ResourceData;

        }

        public bool ResourceUploadInsert(ResourceUploadEntity resource)
        {
            SqlParameter[] param = new SqlParameter[12];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@ResourceName", resource.ResourceName);
                param[1] = new SqlParameter("@ProductName", resource.ProductName);
                param[2] = new SqlParameter("@DocumentTypeId", resource.DocumentTypeId);
                param[3] = new SqlParameter("@IsPublished", resource.PublishedStatus);
                param[4] = new SqlParameter("@ResourceType", resource.ResourceType);
                param[5] = new SqlParameter("@DocumentPath", resource.DocumentPath ?? string.Empty);
                param[6] = new SqlParameter("@URLName", resource.URLName ?? string.Empty);
                param[7] = new SqlParameter("@CreatedBy", resource.PublishedBy);
                param[8] = new SqlParameter("@appAuditID", resource.appAuditID);
                param[9] = new SqlParameter("@SessionID", resource.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", resource.ApplicationOperation);
                param[11] = new SqlParameter("@TitleImage", resource.TitleImage);
                

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceUploadInsert, param);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceUploadInsert");
                throw ex;
            }

            return true;
        }

        public List<DataRow> ResourceSelectById(int ResID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ResourceId", ResID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceSelectById, param);
                ds.Tables[0].TableName = "ResourceDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceSelectById");
                throw ex;
            }

            List<DataRow> ResourceData = ds.Tables[0].AsEnumerable().ToList();
            return ResourceData;
        }

        public bool ResourceUpdate(ResourceUploadEntity res)
        {
            if (res.DocumentPath == null)
            {
                res.DocumentPath = "-1";
                
            }

            SqlParameter[] param = new SqlParameter[13];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ResourceId", res.ResourceId);
                param[1] = new SqlParameter("@ResourceName", res.ResourceName);
                param[2] = new SqlParameter("@productname", res.ProductName);
                param[3] = new SqlParameter("@DocumentTypeId", res.DocumentTypeId);
                param[4] = new SqlParameter("@IsPublished", res.PublishedStatus);
                param[5] = new SqlParameter("@ResourceType", res.ResourceType);
                param[6] = new SqlParameter("@DocumentPath", res.DocumentPath);
                param[7] = new SqlParameter("@URLName", res.URLName);
                param[8] = new SqlParameter("@UpdatedBy", res.UpdatedBy);
                param[9] = new SqlParameter("@appAuditID", res.appAuditID);
                param[10] = new SqlParameter("@SessionID", res.SessionID);
                param[11] = new SqlParameter("@ApplicationOperation", res.ApplicationOperation);
                param[12] = new SqlParameter("@TitleImage", res.TitleImage);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceUpdateSP, param);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ResourceUpdate");
                throw ex;
            }

            return true;
        }

        public bool DeleteResource(ResourceUploadEntity doc)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ResourceId", doc.ResourceId);
                param[1] = new SqlParameter("@DeletedBy", doc.UpdatedBy);
                param[2] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[3] = new SqlParameter("@SessionID", doc.SessionID);
                param[4] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ResourceDeleteSP, param);
                return true;


            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteResource");
                throw ex;
            }
        }
    }
}