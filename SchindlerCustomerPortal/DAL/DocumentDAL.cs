﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class DocumentDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> DocumentInsert(DocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[1] = new SqlParameter("@DocumentTypeID",doc.DocumentTypeID);
                param[2] = new SqlParameter("@Description", doc.Description);
                param[3] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[4] = new SqlParameter("@Expireddate", doc.ExpiredDate);
                param[5] = new SqlParameter("@PublishedBy", doc.PublishedBy);
                param[6] = new SqlParameter("@IsDeleted", doc.IsDeleted);
                param[7] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[8] = new SqlParameter("@SessionID",doc.SessionID);
                param[9] = new SqlParameter("@FileType", doc.FileType);
                param[10] = new SqlParameter("@StatusId", doc.StatusId);
                param[11] = new SqlParameter("@Projectname", HttpUtility.HtmlDecode(doc.ProjectName));
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@Jobname", HttpUtility.HtmlDecode(doc.JobName));
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);
                //{
                //    Direction = ParameterDirection.Output
                //};

                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentInsert, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DocumentMannage");
                throw ex;
            }

            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public List<DataRow> DocumentSelect(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                //param[0] = new SqlParameter("@DocumentType", model.DocumentType);
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int,4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelect, param);
                ds.Tables[0].TableName = "DocumentDetails";

                count = Convert.ToInt32(param[5].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;
        
        }

        public List<DataRow> DocumentSelectByCompany(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[7];
            DataSet ds = new DataSet();
            try
            {
                //param[0] = new SqlParameter("@DocumentType", model.DocumentType);
                param[0] = new SqlParameter("@CompanyId", model.CompanyID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@PageNumber", model.PageNumber);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection);
                param[6] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelectByCompany, param);
                ds.Tables[0].TableName = "DocumentDetails";

                count = Convert.ToInt32(param[6].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;

        }

        public List<DataRow> DocumentSelectById(int DocID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", DocID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelectById, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> DocumentData = ds.Tables[0].AsEnumerable().ToList();
            return DocumentData;
        }

        public List<DataRow> DocumentUpdate(DocumentEntity doc)
        {
            if(doc.DocumentPath==null && doc.FileType==null)
            {
                doc.DocumentPath = "-1";
                doc.FileType = "-1";
            }
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[2] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[3] = new SqlParameter("@Description", doc.Description);
                param[4] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[5] = new SqlParameter("@Expireddate", doc.ExpiredDate);
                param[6] = new SqlParameter("@UpdatedBy", doc.UpdatedBy);
                param[7] = new SqlParameter("@FileType", doc.FileType);
                param[8] = new SqlParameter("@StatusId", doc.StatusId);
                param[9] = new SqlParameter("@Projectname", doc.ProjectName);
                param[10] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[11] = new SqlParameter("@SessionID", doc.SessionID);
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@Jobname", doc.JobName);
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);

                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);
                //{
                //    Direction = ParameterDirection.Output
                //};

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentUpdateSP, param);
                ds.Tables[0].TableName = "DocumentDetails";
                
                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public bool DeleteDocument(DocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DeletedBy", doc.DeletedBy);
                param[2] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[3] = new SqlParameter("@SessionID", doc.SessionID);

                param[4] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[5] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentDeleteSP, param);
                return true;
               

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteDocument");
                throw ex;
            }
        }

        public List<DataRow> GetDocumentPermissionByUser(int docid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@DocumentID", docid);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetDocumentsPermissionByUser, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }
            var DocAccessList = ds.Tables[0].AsEnumerable().ToList();
            return DocAccessList;

        }

        public int InsertUpdateUserDocumentAccessPermissionByModule(UserPermissionDocumentModel obj)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[8];
            try
            {

                param[0] = new SqlParameter("@UserID", obj.UserID);
                param[1] = new SqlParameter("@DocumentID", obj.DocumentID);
                param[2] = new SqlParameter("@ColumnName", obj.ColumnName);
                param[3] = new SqlParameter("@ColumnValue", obj.ColumnValue);
                param[4] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[5] = new SqlParameter("@updatedBy", obj.UpdatedBy);
                param[6] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[7] = new SqlParameter("@SessionID", obj.SessionID);
                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.UserDocumentAccess_UpdateInsertDocumentPermission, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }

        public bool UserDocumentAccess_UpdateAll(UserPermissionDocumentMulti obj)
        {
            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", obj.UserIds);
                param[1] = new SqlParameter("@DocumentID", obj.DocumentID);
                param[2] = new SqlParameter("@View_FieldValue", obj.View_Permission);
                param[3] = new SqlParameter("@Upload_FieldValue", obj.Upload_Permission);
                param[4] = new SqlParameter("@Delete_FieldValue", obj.Delete_Permission);
                param[5] = new SqlParameter("@UpdatedBy", obj.UpdatedBy);
                param[6] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[7] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[8] = new SqlParameter("@SessionID", obj.SessionID);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UserDocumentAccess_UpdateAll, param);
                return true;

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document Update");
                throw ex;
            }
        }

        public List<DataRow> DocumentSelectByType(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();
            try
            {
                //param[0] = new SqlParameter("@DocumentType", model.DocumentType);
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@ProjectId", model.ProjectID);
                param[2] = new SqlParameter("@DocumentType", model.DocumentType);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelectbyTypeSP, param);
                ds.Tables[0].TableName = "DocumentDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;

        }

        public List<DataRow> DocumentSelectByCompanyId_ByProjectId_ByType(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();
            try
            {
                //param[0] = new SqlParameter("@DocumentType", model.DocumentType);
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@CompanyId", model.CompanyID);
                param[2] = new SqlParameter("@ProjectId", model.ProjectID);
                param[3] = new SqlParameter("@DocumentType", model.DocumentType);
                param[4] = new SqlParameter("@PageNumber", model.PageNumber);
                param[5] = new SqlParameter("@PageSize", model.PageSize);
                param[6] = new SqlParameter("@SortColumn", model.SortColumn);
                param[7] = new SqlParameter("@SortDirection", model.SortDirection);
                param[8] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelectbyType_CompanywiseSP, param);
                ds.Tables[0].TableName = "DocumentDetails";

                count = Convert.ToInt32(param[8].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;

        }
    }
}