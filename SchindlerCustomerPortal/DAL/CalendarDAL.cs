﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class CalendarDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> GetUserCalendarEventHeaderMenu(string mail)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserEmail", mail);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CalendarEventHeaderMenu, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserCalendarEventHeaderMenu");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }
    }
}