﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SchindlerCustomerPortal.DAL
{
    public class DataAccessLayer
    {
        SqlConnection _pSQLCon;
        SqlDataAdapter _pSQLDataAdapter;
        SqlCommand _pSQLCommand;
        SqlDataReader _pSQLDataReader;

        public DataAccessLayer()
        {

        }

        #region Private Methods
        private static string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["SchindlerConnString"].ConnectionString;
        }
        private void Dispose()
        {
            if (_pSQLCommand != null)
                _pSQLCommand.Dispose();

            if (_pSQLCon.State != ConnectionState.Closed)
            {
                _pSQLCon.Close();
                _pSQLCon.Dispose();
            }
        }
        private void BuildCommand(CommandType cmdType, string cmdText)
        {
            _pSQLCommand = new SqlCommand(cmdText, _pSQLCon);
            _pSQLCommand.CommandType = cmdType;
        }
        #endregion

        #region Public Methods
        public SqlDataReader ExecuteReader(CommandType cmdType, string cmdText, params SqlParameter[] parameters)
        {
            try
            {
                _pSQLCon = new SqlConnection(GetConnectionString());
                BuildCommand(cmdType, cmdText);
                foreach (SqlParameter param in parameters)
                {
                    _pSQLCommand.Parameters.Add(param);
                }
                _pSQLCon.Open();
                _pSQLDataReader = _pSQLCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return _pSQLDataReader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_pSQLCommand != null)
                {
                    _pSQLCommand.Dispose();
                }
            }
        }
        public object ExecuteScalar(CommandType cmdType, string cmdText, params SqlParameter[] parameters)
        {
            try
            {
                _pSQLCon = new SqlConnection(GetConnectionString());
                BuildCommand(cmdType, cmdText);
                foreach (SqlParameter param in parameters)
                {
                    _pSQLCommand.Parameters.Add(param);
                }
                _pSQLCon.Open();
                return _pSQLCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_pSQLCommand != null)
                {
                    _pSQLCommand.Dispose();
                }
            }
        }
        public int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] param)
        {
            try
            {
                int result;
                _pSQLCon = new SqlConnection(GetConnectionString());
                _pSQLCommand = new SqlCommand(cmdText, _pSQLCon);
                _pSQLCommand.CommandType = cmdType;
                _pSQLCommand.CommandTimeout = 0;
                _pSQLCon.Open();

                foreach (SqlParameter par in param)
                {
                    _pSQLCommand.Parameters.Add(par);
                }
                result = _pSQLCommand.ExecuteNonQuery();
                _pSQLCon.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }

        public DataSet ExecuteDataSet(CommandType cmdType, string cmdText, params SqlParameter[] parameters)
        {
            DataSet ds = new DataSet();
            _pSQLCon = new SqlConnection(GetConnectionString());
            BuildCommand(cmdType, cmdText);
            _pSQLDataAdapter = new SqlDataAdapter();

            foreach (SqlParameter param in parameters)
            {
                _pSQLCommand.Parameters.Add(param);
            }
            _pSQLDataAdapter.SelectCommand = _pSQLCommand;
            _pSQLDataAdapter.Fill(ds);
            return ds;
        }
        public DataTable ExecuteDataTable(CommandType cmdType, string cmdText, params SqlParameter[] parameters)
        {
            DataSet ds = ExecuteDataSet(cmdType, cmdText, parameters);
            return ds.Tables[0];
        }
        public void Close()
        {
            this.Dispose();
        }


        public DataSet ExecuteDataSetWithoutParameter(CommandType cmdType, string cmdText)
        { 

            DataSet ds = new DataSet();
            _pSQLCon = new SqlConnection(GetConnectionString());
            BuildCommand(cmdType, cmdText);
            _pSQLDataAdapter = new SqlDataAdapter();

            _pSQLDataAdapter.SelectCommand = _pSQLCommand;
            _pSQLDataAdapter.Fill(ds);
            return ds;
        }


        #endregion
    }
}