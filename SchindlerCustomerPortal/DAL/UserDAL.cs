﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class UserDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> LoginNow(string Email)
        {

            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@Email", Email);
                // param[1] = new SqlParameter("@Password", Password);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.LoginProcedureName, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - LoginNow");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> CheckForgotPwdEmail(string Email)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Email", Email);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ForgotPasswordName, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckForgotPwdEmail");
                throw ex;
            }
            List<DataRow> EmailCheck = ds.Tables[0].AsEnumerable().ToList();
            return EmailCheck;
        }

        public bool UpdatePassword(string Token, string Password, string PasswordSalt, int AccountStatus)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;


            try
            {

                param[0] = new SqlParameter("@Token", Token);
                param[1] = new SqlParameter("@Password", Password);
                param[2] = new SqlParameter("@PasswordSalt", PasswordSalt);
                param[3] = new SqlParameter("@AccountStatus", AccountStatus);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdatePassword, param);

                //if (result >= 1)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdatePassword");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public bool SetTokenOnEmail(string Token, int UserID, int ExpireHours)
        {
            SqlParameter[] param = new SqlParameter[3];
            int result;
            try
            {

                param[0] = new SqlParameter("@TokenID", Token);
                param[1] = new SqlParameter("@UserID", UserID);
                param[2] = new SqlParameter("@HoursAdd", ExpireHours);

                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.SetTokenUser, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SetTokenOnEmail");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

        }

        public List<DataRow> GetExpireTokenDateTime(string Token)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@TokenID", Token);


                //Oupput parameter
                //param[1] = new SqlParameter("@TokenExpireDate", SqlDbType.DateTime);

                //param[1].Direction = ParameterDirection.Output;

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetTokenExpireDate, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetExpireTokenDateTime");
                throw ex;
            }
            List<DataRow> GetExpDate = ds.Tables[0].AsEnumerable().ToList();
            return GetExpDate;

        }

        public List<DataRow> GetUserEmailFromToken(string Token)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@TokenID", Token);


                //Oupput parameter
                //param[1] = new SqlParameter("@TokenExpireDate", SqlDbType.DateTime);

                //param[1].Direction = ParameterDirection.Output;

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateUserStatus_FromTokenID, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetExpireTokenDateTime");
                throw ex;
            }
            List<DataRow> GetExpDate = ds.Tables[0].AsEnumerable().ToList();
            return GetExpDate;

        }

        public int InsertUser(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[23];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@Email", user.Email);
                param[1] = new SqlParameter("@FirstName", user.FirstName);
                param[2] = new SqlParameter("@LastName", user.LastName);
                param[3] = new SqlParameter("@ContactNumber", user.ContactNumber);
                param[4] = new SqlParameter("@ProfilePic", user.ProfilePic);
                param[5] = new SqlParameter("@CreatedBy", user.CreatedBy);
                param[6] = new SqlParameter("@CompanyID", user.CompanyID);
                param[7] = new SqlParameter("@RoleID", user.RoleID);
                param[8] = new SqlParameter("@appAuditID", user.appAuditID);
                param[9] = new SqlParameter("@SessionID", user.SessionID);
                param[10] = new SqlParameter("@AccountStatus", user.Status);
                param[11] = new SqlParameter("@TokenID", user.Token);
                param[12] = new SqlParameter("@HoursAdd", user.TokenExpire);
                param[13] = new SqlParameter("@Designation", user.Designation);
                param[14] = new SqlParameter("@Address", user.Address1);
                param[15] = new SqlParameter("@State", user.State);
                param[16] = new SqlParameter("@City", user.City);
                param[17] = new SqlParameter("@PostalCode", user.PostalCode);
                param[18] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[19] = new SqlParameter("@Unsubscribe", user.Unsubscribe);
                param[20] = new SqlParameter("@Latitude", user.Latitude);
                param[21] = new SqlParameter("@Longitude", user.Longitude);
                param[22] = new SqlParameter("@flag", "0");
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserInsert, param);

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertUser");
                throw ex;
            }

            var result = ds.Tables[0].Rows[0]["Email"].ToString();
            if (result == "1")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public bool InsertNotification(NotificationEntity data)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyId", data.CompanyID);
                param[1] = new SqlParameter("@DeviceId", data.DeviceID);
                param[2] = new SqlParameter("@SenderId", data.SenderID);
                param[3] = new SqlParameter("@ReceiverId", data.ReceiverID);
                param[4] = new SqlParameter("@Subject", data.Subject);
                param[5] = new SqlParameter("@Message", data.Message);
                param[6] = new SqlParameter("@DateSent", data.DateSent);
                param[7] = new SqlParameter("@StatusId", data.StatusID);
                param[8] = new SqlParameter("@CreatedBy", data.CreatedBy);
                param[9] = new SqlParameter("@DeviceType", data.DeviceType);


                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.InsertNotification, param);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertUser");
                //throw ex;
                return false;
            }
                       
        }

        public List<DataRow> CompanyUserSelect(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[12];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyID", model.CompanyID);
                param[1] = new SqlParameter("@UserID", model.UserID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@ModuleName", model.ModuleName);
                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.SessionID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserSelect, param);
                ds.Tables[0].TableName = "UserDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyUserSelect");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public List<DataRow> CompanyUserSelectById(int UserID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserSelectByUserId, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyUserSelectById");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool UpdateUser(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[20];

            try
            {
                param[0] = new SqlParameter("@FirstName", user.FirstName);
                param[1] = new SqlParameter("@LastName", user.LastName);
                param[2] = new SqlParameter("@ContactNumber", user.ContactNumber);
                param[3] = new SqlParameter("@ProfilePic", user.ProfilePic);
                param[4] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[5] = new SqlParameter("@appAuditID", user.appAuditID);
                param[6] = new SqlParameter("@SessionID", user.SessionID);
                param[7] = new SqlParameter("@UserID", user.UserID);
                param[8] = new SqlParameter("@Designation", user.Designation);
                param[9] = new SqlParameter("@Address", user.Address1);
                param[10] = new SqlParameter("@State", user.State);
                param[11] = new SqlParameter("@City", user.City);
                param[12] = new SqlParameter("@PostalCode", user.PostalCode);
                param[13] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[14] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[15] = new SqlParameter("@Requested_Projects", user.Requested_Projects);
                param[16] = new SqlParameter("@Unsubscribe", user.Unsubscribe);
                param[17] = new SqlParameter("@Latitude", user.Latitude);
                param[18] = new SqlParameter("@Longitude", user.Longitude);
                param[19] = new SqlParameter("@flag", "0");

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.CompanyUserUpdate, param);

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUser");
                throw ex;
            }

            return true;
        }

        public List<DataRow> Registration(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[20];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@RoleID", user.RoleID);
                param[1] = new SqlParameter("@Email", user.Email);
                param[2] = new SqlParameter("@FirstName", user.FirstName);
                param[3] = new SqlParameter("@MiddleName", user.MiddleName);
                param[4] = new SqlParameter("@LastName", user.LastName);
                param[5] = new SqlParameter("@CompanyName", user.CompanyName);
                param[6] = new SqlParameter("@ContactNumber", user.ContactNumber ?? string.Empty);
                param[7] = new SqlParameter("@Address1", user.Address1 ?? string.Empty);
                param[8] = new SqlParameter("@Address2", user.Address2 ?? string.Empty);
                param[9] = new SqlParameter("@PostalCode", user.PostalCode ?? string.Empty);
                param[10] = new SqlParameter("@City", user.City ?? string.Empty);
                param[11] = new SqlParameter("@State", user.State ?? string.Empty);
                param[12] = new SqlParameter("@CountryID", user.Country ?? string.Empty);
                param[13] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[14] = new SqlParameter("@CompanyID", user.CompanyID);
                param[15] = new SqlParameter("@ProfilePic", user.ProfilePic);
                param[16] = new SqlParameter("@Requested_Projects", user.Requested_Projects);
                param[17] = new SqlParameter("@Latitude", user.Latitude);
                param[18] = new SqlParameter("@Longitude", user.Longitude);
                param[19] = new SqlParameter("@flag", "0");

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.RegistrationUserInsert, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Registration");
                throw ex;
            }
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public bool UpdateUserPassword(ChangePassword user)
        {
            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@OldPassword", user.OldPassword);
                param[1] = new SqlParameter("@NewPassword", user.NewPassword);
                param[2] = new SqlParameter("@PasswordSalt", user.PasswordSalt);
                param[3] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[4] = new SqlParameter("@appAuditID", user.appAuditID);
                param[5] = new SqlParameter("@SessionID", user.SessionID);
                param[6] = new SqlParameter("@UserID", user.UserID);
                param[7] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ChangePassword, param);

                var result = ds.Tables[0].Rows[0]["password"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserPassword");
                throw ex;
            }
        }

        public List<DataRow> GetModulesPermissionByUser(int userid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@UserID", userid);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetModulesPermissionByUser, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetModulesPermissionByUser");
                throw ex;
            }
            var moduleAccessList = ds.Tables[0].AsEnumerable().ToList();
            return moduleAccessList;

        }

        public int InsertUpdateUserModuleAccess(UserPermissionViewModel obj)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[10];
            try
            {

                param[0] = new SqlParameter("@UserID", obj.UserID);
                param[1] = new SqlParameter("@ModuleID", obj.ModuleId);
                param[2] = new SqlParameter("@View_FieldValue", obj.HasViewPermission);
                param[3] = new SqlParameter("@Add_FieldValue", obj.HasAddPermission);
                param[4] = new SqlParameter("@Edit_FieldValue", obj.HasEditPermission);
                param[5] = new SqlParameter("@Delete_FieldValue", obj.HasDeletePermission);
                param[7] = new SqlParameter("@appAuditID", obj.HasPrintPermission);
                param[8] = new SqlParameter("@SessionID", obj.HasPrintPermission);

                if (obj.IsUpdate)
                {
                    param[9] = new SqlParameter("@updatedBy", obj.UpdatedBy);
                }
                else
                {
                    param[9] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                }

                if (obj.IsUpdate)
                {
                    result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.UpdateUserModuleAccess, param);
                    //if result==1 than already user assigned module
                }
                else
                {
                    result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.InsertUserModuleAccess, param);
                }

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertUpdateUserModuleAccess");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }

        public int InsertUpdateUserModuleAccessPermissionByModule(UserPermissionModuleModel obj)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[9];
            try
            {

                param[0] = new SqlParameter("@UserID", obj.UserID);
                param[1] = new SqlParameter("@ModuleID", obj.ModuleId);
                param[2] = new SqlParameter("@ColumnName", obj.ColumnName);
                param[3] = new SqlParameter("@ColumnValue", obj.ColumnValue);
                param[4] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[5] = new SqlParameter("@SessionID", obj.SessionID);
                param[6] = new SqlParameter("@updatedBy", obj.UpdatedBy);
                param[7] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[8] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);
                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.UserModuleAccess_UpdateInsertModulePermission, param);
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertUpdateUserModuleAccessPermissionByModule");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }

        public bool DeleteCompanyUser(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[1] = new SqlParameter("@appAuditID", user.appAuditID);
                param[2] = new SqlParameter("@SessionID", user.SessionID);
                param[3] = new SqlParameter("@UserID", user.UserID);
                param[4] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserDelete, param);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteCompanyUser");
                throw ex;
            }
        }

        public bool DeleteMultipleCompanyUser(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[1] = new SqlParameter("@appAuditID", user.appAuditID);
                param[2] = new SqlParameter("@SessionID", user.SessionID);
                param[3] = new SqlParameter("@ids", user.IdList);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyUserMultipleDelete, param);
                return true;
                //if (result == "1")
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteMultipleCompanyUser");
                throw ex;
            }
        }

        public bool UserModuleAccess_UpdateAll(UserPermissionModuleModelMulti obj)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", obj.UserID);
                param[1] = new SqlParameter("@ModuleId", obj.ModuleIds);
                param[2] = new SqlParameter("@View_FieldValue", obj.View_Permission);
                param[3] = new SqlParameter("@Add_FieldValue", obj.Add_Permission);
                param[4] = new SqlParameter("@Edit_FieldValue", obj.Edit_Permission);
                param[5] = new SqlParameter("@Delete_FieldValue", obj.Delete_Permission);
                param[6] = new SqlParameter("@Print_FieldValue", obj.Print_Permission);
                param[7] = new SqlParameter("@UpdatedBy", obj.UpdatedBy);
                param[8] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[9] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[10] = new SqlParameter("@SessionID", obj.SessionID);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UserModuleAccess_UpdateAll, param);
                return true;
                //if (result == "1")
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UserModuleAccess_UpdateAll");
                throw ex;
            }
        }

        public List<DataRow> CheckCompanyName(string CompanyName)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyName", CompanyName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyName, param);
                ds.Tables[0].TableName = "CompanyMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckCompanyName");
                throw ex;
            }
            List<DataRow> CompNameCheck = ds.Tables[0].AsEnumerable().ToList();
            return CompNameCheck;
        }

        public List<DataRow> CheckCompanyNameNew(string CompanyName)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyName", CompanyName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyNameNew, param);
                ds.Tables[0].TableName = "CompanyMaster";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckCompanyName");
                throw ex;
            }
            List<DataRow> CompNameCheck = ds.Tables[0].AsEnumerable().ToList();
            return CompNameCheck;
        }

        public List<DataRow> GetModulesPermissionForDynamicMenu(int userid, int parentId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            try
            {
                param[0] = new SqlParameter("@UserID", userid);
                param[1] = new SqlParameter("@ParentID", parentId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DynamicMenu, param);
                ds.Tables[0].TableName = "DynamicMenu";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetModulesPermissionForDynamicMenu");
                throw ex;
            }
            var moduleAccessList = ds.Tables[0].AsEnumerable().ToList();
            return moduleAccessList;

        }

        public List<DataRow> CheckModulePermission(int userId, string moduleName)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@USerID", userId);
                param[1] = new SqlParameter("@ModuleName", moduleName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ModulePermissionByUserIdModuleName, param);
                ds.Tables[0].TableName = "ModulePermission";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckModulePermission");
                throw ex;
            }

            var moduleAccessList = ds.Tables[0].AsEnumerable().ToList();
            return moduleAccessList;
        }

        public bool UpdateUserAccountStatus(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@UserID", user.UserID);
                param[1] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[2] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[3] = new SqlParameter("@appAuditID", user.appAuditID);
                param[4] = new SqlParameter("@SessionID", user.SessionID);

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateUserAccountStatus, param);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserAccountStatus");
                throw ex;
            }

            return true;
        }

        public List<DataRow> NewlyRegisteredUser(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyID", model.CompanyID);
                param[1] = new SqlParameter("@UserID", model.UserID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserList, param);
                ds.Tables[0].TableName = "UserDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUser");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }


        public int NewlyRegisteredUserCount(int CompanyID,ref int count)
        {
            int iCount = 0;
            SqlParameter[] param = new SqlParameter[1];
            
            try
            {
                param[0] = new SqlParameter("@CompanyID", CompanyID);
                iCount = Convert.ToInt32(dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserCount,param));             
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUser");
                throw ex;
            }            
            return iCount;
        }

        public List<DataRow> NewlyRegisteredUserEdit(int UserID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserID", UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserEdit, param);
                ds.Tables[0].TableName = "UserDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - NewlyRegisteredUserEdit");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool NewlyRegisteredUserUpdate(UserEntity user)
        {
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ContactNumber", user.ContactNumber);
                param[1] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[2] = new SqlParameter("@appAuditID", user.appAuditID);
                param[3] = new SqlParameter("@SessionID", user.SessionID);
                param[4] = new SqlParameter("@UserID", user.UserID);
                param[5] = new SqlParameter("@Designation", user.Designation);
                param[6] = new SqlParameter("@Address", user.Address1);
                param[7] = new SqlParameter("@State", user.State);
                param[8] = new SqlParameter("@City", user.City);
                param[9] = new SqlParameter("@PostalCode", user.PostalCode);
                param[10] = new SqlParameter("@AccountStatus", user.AccountStatus);
                param[11] = new SqlParameter("@CompanyName", user.CompanyName);
                param[12] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[13] = new SqlParameter("@Requested_Projects", user.Requested_Projects);
                param[14] = new SqlParameter("@Latitude", user.Latitude);
                param[15] = new SqlParameter("@Longitude", user.Longitude);
                param[16] = new SqlParameter("@flag", "0");
                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.NewlyRegisterUserUpdate, param);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserAccountStatus");
                throw ex;
            }

            return true;
        }

        public bool UpdateUserProjectAssign(UserProjectAssignEntity model)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[6];
            int result;

            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@ProjectNo_TVP", CreateDataTable(model.ProjectNo, model.UnsubscribeProjectNo));
                //param[1] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[2] = new SqlParameter("@UpdatedBy", model.UpdatedBy);
                param[3] = new SqlParameter("@appAuditID", model.appAuditID);
                param[4] = new SqlParameter("@SessionID", model.SessionID);
                param[5] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateUserProjectAssign, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateUserProjectAssign");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public List<DataRow> GetUserProjectAssign(UserProjectAssignEntity model)
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];

            try
            {

                param[0] = new SqlParameter("@UserId", model.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetUserProjectAssign, param);
                ds.Tables[0].TableName = "UserDetails";

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserProjectAssign");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public bool CheckCompanyforEditNewRegisterUser(string companyName)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyName", companyName);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckCompanyforEditNewRegisterUserSP, param);
                var result = ds.Tables[0].Rows[0]["Existing"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckCompanyforEditNewRegisterUser");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public List<DataRow> CheckPermissionForMailNotification(int companyId)
        {
            DataAccessLayer dal = new DataAccessLayer();
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];

            try
            {

                param[0] = new SqlParameter("@CompanyId", companyId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckPermissionForMailNotificationSP, param);
                ds.Tables[0].TableName = "UserDetails";

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CheckPermissionForMailNotification");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> ApplicationAuditMasterList(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyID", model.CompanyID);
                param[1] = new SqlParameter("@UserID", model.UserID);
                param[2] = new SqlParameter("@RoleId", model.RoleID);
                param[3] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[4] = new SqlParameter("@PageNumber", model.PageNumber);
                param[5] = new SqlParameter("@PageSize", model.PageSize);
                param[6] = new SqlParameter("@SortColumn", model.SortColumn);
                param[7] = new SqlParameter("@SortDirection", model.SortDirection);
                param[8] = new SqlParameter("@LogDateFrom", model.LogDateFrom);
                param[9] = new SqlParameter("@LogDateTo", model.LogDateTo);

                param[10] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ApplicationAuditMasterListSP, param);
                ds.Tables[0].TableName = "UserDetails";

                count = Convert.ToInt32(param[10].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ApplicationAuditMasterList");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool IsUserValidForProject(DataTableModel model)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[2] = new SqlParameter("@CompanyID", model.CompanyID);
                param[3] = new SqlParameter("@appAuditID", model.appAuditID);
                param[4] = new SqlParameter("@SessionID", model.SessionID);
                param[5] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckUserProjectAccess, param);
                var result = ds.Tables[0].Rows[0]["IsValid"].ToString();

                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - IsUserValidForProject");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        private static DataTable CreateDataTable(string ProjNoData,string UnsubscribeProjectNo)
        {
            string[] ProjNo = ProjNoData.Split(',');
            string[] UnSubProjNo = UnsubscribeProjectNo.Split(',');

            DataTable table = new DataTable();
            table.Columns.Add("ProjectNo", typeof(string));
            table.Columns.Add("IsUnSubscribed", typeof(bool));
            foreach (string id in ProjNo)
            {
                bool has = Array.IndexOf(UnSubProjNo, id) >= 0;
                table.Rows.Add(id, has);
            }
            return table;
        }
    }
}