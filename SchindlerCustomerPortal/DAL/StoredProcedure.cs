﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.DAL
{
    /// <summary>
    /// List of Used StoredProcedure
    /// </summary>
    public static class StoredProcedure
    {

        #region Register Login Forgot Password SetToken

        public const string RegistrationUserInsert = "sp_UserMaster_Register";
        public const string LoginProcedureName = "sp_LoginUser";
        public const string ForgotPasswordName = "sp_ForgotPassword_ExistingEmail";

        public const string SetTokenUser = "sp_UserMaster_AddToken";
        public const string GetTokenExpireDate = "sp_TokenID_ExpireDate";
        public const string UpdateUserStatus_FromTokenID = "sp_UpdateUserStatus_FromTokenID";

        #endregion

        #region Country State City

        public const string GetCountry = "[dbo].[sp_CountryMaster_Select]";
        public const string GetStateListFromCountry = "[dbo].[sp_StateMaster_Select]";
        public const string GetCityListFromState = "[dbo].[sp_CityMaster_Select]";
        public const string CityMaster_SelectAutoCompleteNew = "[dbo].[sp_CityMaster_SelectAutoCompleteNew]";

        #endregion

        #region Account Management

        public const string GetCompanyMasterPageing = "[dbo].[sp_CompanyMaster_SelectByPage]"; //Audit Trail - Verified
        public const string GetCompanyMaster = "[dbo].[sp_CompanyMaster_Select]";
        public const string InsertInCompanyMaster = "[dbo].[sp_CompanyMaster_Insert]";
        public const string UpdateInCompanyMaster = "[dbo].[sp_CompanyMaster_Update]";//Audit Trail - Verified
        public const string DeleteFromCompanyMaster = "[dbo].[sp_CompanyMaster_Delete]";
        public const string GetCompanyById = "[dbo].[sp_CompanyMaster_Select_ById]";
        public const string CheckCompanyBeforeDelete = "[dbo].[sp_CompanyMaster_CheckCompany]";
        public const string GetAssciatedCompanyDetails = "sp_CompanyMaster_AssociateCompanies";

        #endregion

        #region Audit Trial

        public const string ApplicationAuditMaster = "sp_ApplicationAuditMaster";
        public const string ApplicationAuditMasterListSP = "sp_ApplicationAuditMaster_Select";

        #endregion

        #region CompanyUser Employee CRUD Project Mapping

        public const string CompanyUserInsert = "sp_CompanyUser_Insert";//Audit Trail - Verified
        public const string CompanyUserSelect = "sp_CompanyUser_Selectsss";//Audit Trail - Verified
        public const string CompanyUserSelectByUserId = "sp_CompanyUser_SelectByID";
        public const string CompanyUserUpdate = "sp_CompanyUser_Update";//Audit Trail - Verified
        public const string CompanyUserDelete = "[dbo].[sp_CompanyUser_Delete]"; //Audit Trail - Verified
        public const string CompanyUserMultipleDelete = "sp_SplitCompanyUser_Delete";

        public const string UpdateUserProjectAssign = "[dbo].[sp_UserProjectMap_Insert]";//Audit Trail - Verified
        public const string GetUserProjectAssign = "[dbo].[sp_UserProjectMap_Select]";

        #endregion

        #region Office Manager CRUD Project Mapping

        public const string OfficeManagerInsert = "usp_OfficeManager_Insert";//Audit Trail - Verified
        public const string OfficeManagerSelect = "usp_OfficeManager_Select";//Audit Trail - Verified
        public const string OfficeManagerByUserId = "usp_OfficeManager_Select_ByID";
        public const string OfficeManagerUpdate = "usp_OfficeManager_Update";//Audit Trail - Verified
        public const string OfficeManagerDelete = "usp_OfficeManager_Delete"; //Audit Trail - Verified
     
        #endregion

        #region Get Roles

        public const string GetRoles = "sp_RoleMaster_Select";

        #endregion

        #region Dynamic Menu

        public const string DynamicMenu = "sp_UserModuleAccess_DynamicMenu";

        #endregion

        #region User Profile - Change Password

        public const string ChangePassword = "sp_UserMaster_ChangePassword";//Audit Trail - Verified
        public const string UpdatePassword = "sp_UserMaster_UpdatePassword";

        #endregion

        #region Help Management

        public const string HelpInsert = "[dbo].[sp_HelpMaster_Insert]";//Audit Trail - Verified
        public const string HelpSelect = "sp_HelpMaster_Select";//Audit Trail - Verified
        public const string HelpSelectById = "sp_HelpMaster_SelectById";
        public const string HelpUpdateSP = "sp_HelpMaster_Update";//Audit Trail - Verified
        public const string HelpDeleteSP = "sp_HelpMaster_Delete";//Audit Trail - Verified
        public const string ModuleHelp_SelectAll = "sp_HelpMaster_SelectbyPage_ModuleId";//Audit Trail - Verified

        #endregion

        #region Resource Center

        public const string ResourceSelect = "sp_ResourceUploadMaster_SelectByPage";//Audit Trail - Verified
        public const string ResourceUploadInsert = "sp_ResourceUploadMaster_Insert";//Audit Trail - Verified
        public const string ResourceSelectById = "sp_ResourceUploadMaster_SelectById";
        public const string ResourceUpdateSP = "sp_ResourceUploadMaster_Update";//Audit Trail - Verified
        public const string ResourceDeleteSP = "sp_ResourceUploadMaster_Delete";//Audit Trail - Verified

        #endregion

        #region UserAccessModule

        public const string InsertUserModuleAccess = "sp_UserModuleAccess_Insert";
        public const string UpdateUserModuleAccess = "sp_UserModuleAccess_Update";
        public const string UserModuleAccess_UpdateInsertModulePermission = "sp_UserModuleAccess_UpdateInsertModulePermission";
        public const string UserModuleAccess_UpdateAll = "sp_UserModuleAccess_UpdateAll";

        #endregion

        #region MailBox

        public const string GetUserByCompanyForMailBox = "[dbo].[sp_MailBox_SelectbyCoId_Users]";
        public const string MailBoxInsert = "[dbo].[sp_MailBox_Insert]";//Audit Trail - Verified
        public const string GetMailByUserID = "[dbo].[sp_MailBox_Select]";//Audit Trail - Verified
        public const string GetMailByMailId = "[dbo].[sp_Mailbox_Select_MailId]";//Audit Trail - Verified
        public const string GetMailCount = "[dbo].[sp_Mailbox_Select_MailCount_ReceiverId]";
        public const string UpdateMailSattus = "[dbo].[sp_SplitMailbox_Update]";//Audit Trail - Verified
        public const string MailDeleteforLogicalDelete = "[dbo].[sp_MailBox_Update]";//Audit Trail - Verified
        public const string SaveDraftData = "[dbo].[sp_MailBox_SaveDraft]";
        #endregion

        #region Alert Message

        public const string AlertMessageByModule = "[dbo].[sp_AlertMessageMaster_SelectbyModuleName]";

        #endregion

        #region Dashboard User List

        public const string NewlyRegisterUserList = "[dbo].[sp_NewRegisteredUser_Select]";//Audit Trail - Verified
        public const string NewlyRegisterUserCount = "[dbo].[usp_GetRegisteredUserCount_Select]";//Audit Trail - Verified

        public const string NewlyRegisterUserEdit = "[dbo].[sp_NewRegisterUser_SelectByID]";
        public const string NewlyRegisterUserUpdate = "[dbo].[sp_NewRegisterUser_Update]";//Audit Trail - Verified

        public const string DashboardProjectListingSP = "[dbo].[sp_DashboardProjectListing]";
        public const string DueDateStatusSP = "[dbo].[usp_Dashboard_GetStatusDueDateCompDate_Select]";
        public const string GetJobMasterListing = "[dbo].[usp_GetJobMasterListing_Select_API]";
        
        public const string ProjectMilestoneNoteInsertSP = "[dbo].[sp_ProjectMilestoneNote_Insert]";

        public const string ProjectMilestonDueDateInsertSP = "[dbo].[sp_projectmilestonenote_duedate_insert]";
        #endregion

        public const string GetAllModules = "[dbo].[sp_ModuleMaster_Select]";
        public const string GetModulesPermissionByUser = "[dbo].[sp_UserModuleAccess_Select]";

        public const string CompanyName = "[dbo].[sp_CheckCompanyName]";
        public const string CompanyNameNew = "[dbo].[sp_CheckCompanyNameNew]";

        public const string UpdateEmailAndPhoneData = "[dbo].[sp_Dashboard_ProjectContact_Update]";

        public const string DocumentInsert = "[dbo].[sp_DocumentMaster_Insert]"; //Audit Trail - Verified
        public const string DocumentSelect = "sp_DocumentMaster_ByType_SelectByPage";

        public const string GetDocumentTypes = "sp_DocumentType_Select";

        public const string CompanyMultipleDelete = "sp_SplitCompany_Delete";

        public const string CompanySelectByCustomer = "sp_CompanyMaster_SelectByCustomer";

        public const string ModulePermissionByUserIdModuleName = "sp_UserModuleAccess_SelectByID_ModuleName";

        #region Communication

        public const string GetAllPostByProjectId = "sp_Post_Select_ByProjectId";
        public const string GetAllThreadPostById = "sp_GetAllThreadPostById";
        public const string InsertCommunicationThread = "[dbo].[sp_CommunicationThread_Insert]";
        public const string CommunicationThread_Select_ById = "[dbo].[sp_CommunicationThread_Select_ById]";
        public const string CommunicationProjectPost_Insert = "[dbo].[sp_CommunicationProjectPost_Insert]";
        public const string PostSelectByProjectId = "sp_Post_Select_ByProjectId";
        public const string Post_Select_ById = "sp_Post_Select_ById";
        
        #endregion

        public const string UpdateUserAccountStatus = "sp_UserMaster_UserStatusUpdate";

        public const string DocumentSelectById = "sp_DocumentMaster_SelectById";
        public const string DocumentUpdateSP = "sp_DocumentMaster_Update";//Audit Trail - Verified

        public const string DocumentDeleteSP = "sp_DocumentMaster_Delete";//Audit Trail - Verified

        public const string GetDocumentsPermissionByUser = "sp_UserDocumentAccess_Select";

        public const string UserDocumentAccess_UpdateInsertDocumentPermission = "sp_UserDocumentAccess_UpdatePermission";


        public const string AddProjectContactSP = "sp_ProjectContact_Insert"; //Audit Trail - Verified

        public const string ProjectContactByIdSP = "sp_ProjectContacts_SelectByID";

        public const string UpdateProjectContactSP = "sp_ProjectContact_Update";//Audit Trail - Verified

        public const string DeleteProjectContact = "sp_SplitProjectContact_Delete";//Audit Trail - Verified

        public const string SelectAllContactsByProjectId = "sp_ProjectContact_SelectByPage";

        public const string SelectAllDesignation = "sp_DesignationMaster_Select";


        public const string UserDocumentAccess_UpdateAll = "sp_UserDocumentAccess_UpdateAll";

        public const string ContractDocument_SelectAll = "sp_DocumentMaster_SelectByPage_ProjectNo";//sp_DocumentMaster_SelectByPage_ProjectId

        public const string ProjectSelectById = "sp_ProjectMaster_SelectById"; // TODO: Remove this code
        public const string GetStatus = "[dbo].[sp_StatusMaster_SelectByModule]";


        

        #region ProjectList

        public const string ProjectMaster_SelectByPage = "[dbo].[sp_ProjectMaster_SelectByPage]";
        public const string UpdateProjectMaster = "[dbo].[sp_ProjectMaster_UpdateById]";

        #endregion

        #region ProjectMasterList

        public const string ProjectMasterList_SelectByPage = "[dbo].[sp_ProjectMasterList_SelectByPage]"; //Audit Trail - Verified
        public const string ProjectMaster_SelectByProjectID = "[dbo].[sp_ProjectMaster_SelectByID]";//Audit Trail - Verified
        public const string ProjectJobList_SelectByPage = "[dbo].[sp_JobMasterList_SelectByProjectNo]";//Audit Trail - Verified
        public const string UpdateProjectMasterByID = "[dbo].[sp_ProjectMaster_UpdateByID]";
        public const string CheckDocumentName = "[dbo].[usp_CheckDocumentName]";
        public const string ProjectJob_SelectByJobNo = "[dbo].[sp_ProjectJob_SelectByJobNo]";

        public const string ProjectDocumentSelectbyType_CompanywiseSP = "sp_DocumentMaster_SelectByPage_ProjectNo_CompanyId"; //sp_DocumentMaster_SelectByPage_ProjectId_CompanyId

        public const string MileStoneUpdateEmailNotification = "[dbo].[sp_MilestoneUpdate_EmailNotification]";//Audit Trail - Verified

        public const string CheckUserProjectAccess = "[dbo].[usp_CheckUserProjectAccess]";

        public const string GetPaymentDocInvoiceAndContact = "[dbo].[usp_GetProjectWiseGraphAndDocuments_Select]";

        public const string JobList_GetJobPivotTable = "[dbo].[usp_GetJobPivotTable]";

        #endregion


        public const string GetModuleByName = "sp_ModuleMaster_SelectbyModuleName";

        public const string GetMilestoneListByJob = "sp_MilestonePredecessor_SelectByJobId";
        public const string GetProjectStatus = "[dbo].[sp_ProjectStatus_Select]";
        //public const string GetProjectByCompanyIdList = "sp_ProjectMaster_SelectBySearchterm_CompanyId";
        public const string GetProjectByCompanyIdList = "sp_ProjectMaster_SelectBySearchterm_CompanyIdForProjectAssignment";
        public const string GetCompanyList = "sp_CompanyMaster_CompanyWise_SelectBySearch";
        public const string GetJobByProjectIdList = "sp_JobMaster_SelectBySearchterm_ProjectNo";
        public const string GetCityListForAutoComplete = "[dbo].[sp_CityMaster_SelectAutoComplete]";

        public const string DocumentSelectByCompany = "sp_DocumentMaster_CompanyWise_SelectByPage";

        public const string MailListForHeaderMenu = "[dbo].[sp_Mailbox_HeaderCount]";

        
        public const string GetResourceDocumentTypes = "sp_DocumentType_SelectByModule";

        public const string DocumentSelectbyTypeSP = "sp_DocumentMaster_SelectByPage_ProjectNo"; //sp_DocumentMaster_SelectByPage_ProjectId

        public const string DocumentSelectbyType_CompanywiseSP = "sp_DocumentMaster_SelectByPage_ProjectNo_CompanyId"; //TODO:Remove this code

        public const string MailListForDashboardWidget = "[dbo].[sp_Dashboard_MessageWidget]";


        public const string GetMilestoneTypes = "sp_MilestoneMaster_Select";

        public const string MilestoneInsert = "sp_ProjectJobsMilestone_Insert";//Audit Trail - Verified


        public const string GetJobBySearchTermandprojectNo = "[dbo].[sp_JobMaster_SelectBySearchterm_ProjectNo]";

        public const string MilestoneUpdate = "sp_ProjectJobsMilestone_Update";//Audit Trail - Verified

        public const string MilestoneSelect = "sp_ProjectJobsMilestone_SelectByPage";//Audit Trail - Verified

        public const string MilestoneSelectById = "sp_ProjectJobsMilestone_SelectByMilestoneId";

        public const string MilestoneDeleteSP = "sp_ProjectJobsMilestone_DeleteById";//Audit Trail - Verified

        public const string MilestoneReadyToPullTemplateDetailsSelect = "sp_ReadyToPullTemplateDetails_Select";
        public const string MilestoneReadyToPullTemplateDetailsInsert = "sp_ReadyToPullTemplateDetails_Insert";//Audit Trail - Verified
            

        public const string JobMaster_SelectByJobID = "";
        public const string JobMaster_UpdateBankDescByJobNo = "sp_JobMaster_UpdateBankDescByJobNo";

        public const string GetAllContactsByCompanyId = "[dbo].[sp_ProjectContactByCompanyId_SelectByPage]";

        public const string AddCommunicationMessage = "[dbo].[sp_CommunicationDetails_Insert]"; //Audit Trail - Verified

        public const string AssociateCompanyUpdateSP = "sp_CompanyMaster_AssociateUpdate";

        public const string GetAllCommunicationList = "[dbo].[sp_CommunicationDetails_Select]";//Audit Trail - Verified

        public const string GetProjectbyCompanyIdForDocument = "[dbo].[sp_ProjectMaster_SelectBySearchterm_CompanyIdForDocument]";

        public const string GetProjectMilestoneProgress = "[dbo].[sp_DashBoard_ProjectMilestoneProgress]";



        public const string CheckCompanyforEditNewRegisterUserSP = "[dbo].[sp_CompanyMaster_SelectbyCompanyName]";

        public const string CheckPermissionForMailNotificationSP = "[dbo].[sp_RegisterUser_MailNotification]";

        public const string InsertNotification = "[dbo].[usp_NotificationLog_Insert]";

        public const string GetProjectandJobData = "[dbo].[usp_GetProjectandJobData]";

        public const string CalendarEventHeaderMenu = "usp_CalendarTaskMaster_GetTodayEvent_SelectByEmail";

        #region Announcement
        public const string InsertAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_Insert]";
        public const string UpdateAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_Update]";
        public const string DeleteAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_Delete]";
        public const string SelectAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_SelectByPage]";
        public const string SelectAnnouncementMessagebyID = "[dbo].[usp_AnnouncementMessageMaster_SelectById]";
        public const string SelectAllAnnouncementMessage = "[dbo].[usp_AnnouncementMessageMaster_BeforeAfterLoginSelect]";

        #endregion
    }
}