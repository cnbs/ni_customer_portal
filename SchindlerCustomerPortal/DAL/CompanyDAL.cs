﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class CompanyDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> CompanyMaster()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetCompanyMaster);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;

        }

        public List<DataRow> CompanyMaster(SearchInfoCompanyViewModel obj, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[10];

            param[0] = new SqlParameter("@SearchTerm", obj.SearchTerm);
            param[1] = new SqlParameter("@PageNumber", obj.PageNumber);
            param[2] = new SqlParameter("@PageSize", obj.PageSize);
            param[3] = new SqlParameter("@SortColumn", obj.SortColumn);
            param[4] = new SqlParameter("@SortDirection", obj.SortDirection);
            param[5] = new SqlParameter("@TotalCount", obj.TotalCount)
            {
                Direction = ParameterDirection.Output
            };
            param[6] = new SqlParameter("@appAuditID", obj.appAuditID);
            param[7] = new SqlParameter("@SessionID", obj.SessionID);
            param[8] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);
            param[9] = new SqlParameter("@UserID", obj.UserID);

            try
            {
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCompanyMasterPageing, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                totalcount = Convert.ToInt32(param[5].Value);
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - CompanyMaster");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;

        }

        public List<DataRow> GetCompanyById(int id)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@CompanyID", id);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetCompanyById, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;

        }

        public int InsertCompanyMaster(CompanyEntity obj)
        {



            int result = 0;
            SqlParameter[] param = new SqlParameter[28];
            //DataSet ds = new DataSet();

            try
            {

                param[0] = new SqlParameter("@CompanyName", obj.CompanyName);
                param[1] = new SqlParameter("@HeadOfficeAddress1", obj.HeadOfficeAddress1);
                param[2] = new SqlParameter("@HeadOfficeAddress2", obj.HeadOfficeAddress2);
                param[3] = new SqlParameter("@HeadOfficeCountryID", obj.HeadOfficeCountryID);
                param[4] = new SqlParameter("@HeadOfficeStateCode", obj.HeadOfficeStateCode);
                param[5] = new SqlParameter("@HeadOfficeCityCode", obj.HeadOfficeCityCode);
                param[6] = new SqlParameter("@HeadOfficeZipCode", obj.HeadOfficeZipCode);
                param[7] = new SqlParameter("@BillingAddress1", obj.BillingAddress1);
                param[8] = new SqlParameter("@BillingAddress2", obj.BillingAddress2);
                param[9] = new SqlParameter("@BillingCountryID", obj.BillingCountryID);
                param[10] = new SqlParameter("@BillingStateCode", obj.BillingStateCode);
                param[11] = new SqlParameter("@BillingCityCode", obj.BillingCityCode);
                param[12] = new SqlParameter("@BillingZipCode", obj.BillingZipCode);
                param[13] = new SqlParameter("@InchargeFirstName", obj.InchargeFirstName);
                param[14] = new SqlParameter("@InchargeLastName", obj.InchargeLastName);
                param[15] = new SqlParameter("@InchargeEmail", obj.InchargeEmail);
                param[16] = new SqlParameter("@InchargeDesignation", obj.InchargeDesignation);
                param[17] = new SqlParameter("@InchargePhone", obj.InchargePhone);
                param[18] = new SqlParameter("@ContactPersonFirstName", obj.ContactPersonFirstName);
                param[19] = new SqlParameter("@ContactPersonLastName", obj.ContactPersonLastName);
                param[20] = new SqlParameter("@CompanyLogo", obj.CompanyLogo);
                param[21] = new SqlParameter("@CreatedBy", obj.CreatedBy);
                param[22] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[23] = new SqlParameter("@SessionID", obj.SessionID);
                param[24] = new SqlParameter("@OfflineAccessDuration", obj.OfflineAccessDuration);
                param[25] = new SqlParameter("@CCEmailAddress", obj.CCEmailAddress);
                param[26] = new SqlParameter("@AccountNumber", obj.AccountNumber);
                param[27] = new SqlParameter("@ExternalAccountNumber", obj.ExternalAccountNumber);

                result = (int)dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.InsertInCompanyMaster, param);

                //if result ==2 than already exist
                //else inserted successfully
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
            return result;

        }

        public bool UpdateCompanyMaster(CompanyEntity obj)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[33];
            int result;
            try
            {
                param[0] = new SqlParameter("@HeadOfficeAddress1", obj.HeadOfficeAddress1);
                param[1] = new SqlParameter("@HeadOfficeAddress2", obj.HeadOfficeAddress2);
                param[2] = new SqlParameter("@HeadOfficeCountryID", obj.HeadOfficeCountryID);
                param[3] = new SqlParameter("@HeadOfficeStateCode", obj.HeadOfficeStateCode);
                param[4] = new SqlParameter("@HeadOfficeCityCode", obj.HeadOfficeCityCode);
                param[5] = new SqlParameter("@HeadOfficeZipCode", obj.HeadOfficeZipCode);
                param[6] = new SqlParameter("@BillingAddress1", obj.BillingAddress1);
                param[7] = new SqlParameter("@BillingAddress2", obj.BillingAddress2);
                param[8] = new SqlParameter("@BillingCountryID", obj.BillingCountryID);
                param[9] = new SqlParameter("@BillingStateCode", obj.BillingStateCode);
                param[10] = new SqlParameter("@BillingCityCode", obj.BillingCityCode);
                param[11] = new SqlParameter("@BillingZipCode", obj.BillingZipCode);
                param[12] = new SqlParameter("@InchargeFirstName", obj.InchargeFirstName);
                param[13] = new SqlParameter("@InchargeLastName", obj.InchargeLastName);
                param[14] = new SqlParameter("@InchargeEmail", obj.InchargeEmail);
                param[15] = new SqlParameter("@InchargeDesignation", obj.InchargeDesignation);
                param[16] = new SqlParameter("@InchargePhone", obj.InchargePhone);
                param[17] = new SqlParameter("@ContactPersonFirstName", obj.ContactPersonFirstName);
                param[18] = new SqlParameter("@ContactPersonLastName", obj.ContactPersonLastName);
                param[19] = new SqlParameter("@CompanyLogo", obj.CompanyLogoFileName);
                param[20] = new SqlParameter("@UpdatedBy", obj.UpdatedBy);
                param[21] = new SqlParameter("@CompanyID", obj.CompanyID);
                param[22] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[23] = new SqlParameter("@SessionID", obj.SessionID);
                param[24] = new SqlParameter("@OfflineAccessDuration", obj.OfflineAccessDuration);
                param[25] = new SqlParameter("@CCEmailAddress", obj.CCEmailAddress);
                param[26] = new SqlParameter("@BillingPhoneNumber", obj.BillingPhoneNumber);
                param[27] = new SqlParameter("@HeadOfficePhoneNumber", obj.HeadOfficePhoneNumber);
                param[28] = new SqlParameter("@ACCTManagerEmail", obj.ACCTManagerEmail);
                param[29] = new SqlParameter("@ACCTManagerPhone", obj.ACCTManagerPhone);
                param[30] = new SqlParameter("@ACCTManagerName", obj.ACCTManagerName);
                param[31] = new SqlParameter("@PrimaryPhoneNumber", obj.PrimaryPhoneNumber);
                param[32] = new SqlParameter("@ApplicationOperation", obj.ApplicationOperation);
                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateInCompanyMaster, param);
                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }

        public bool DeleteCompanyMaster(CommonAuditModel obj)
        {

            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[4];
            int result;

            try
            {
                param[0] = new SqlParameter("@CompanyID", obj.Id);
                param[1] = new SqlParameter("@UpdatedBy", obj.LoggedInUserid);
                param[2] = new SqlParameter("@appAuditID", obj.AppAuditID);
                param[3] = new SqlParameter("@SessionID", obj.SessionID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.DeleteFromCompanyMaster, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            finally
            {
                dal.Close();
            }


        }

        public DataSet CheckDelete(int CompanyID)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyID", CompanyID);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CheckCompanyBeforeDelete, param);

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            return ds;
        }

        public bool DeleteMultipleCompany(CompanyEntity obj)
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UpdatedBy", obj.UpdatedBy);
                param[1] = new SqlParameter("@appAuditID", obj.appAuditID);
                param[2] = new SqlParameter("@SessionID", obj.SessionID);
                param[3] = new SqlParameter("@ids", obj.IdList);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanyMultipleDelete, param);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
        }

        public List<DataRow> GetCompanyByCustomer(int companyId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@CompanyID", companyId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.CompanySelectByCustomer, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;
            }
            var companyList = ds.Tables[0].AsEnumerable().ToList();
            return companyList;

        }
    }
}