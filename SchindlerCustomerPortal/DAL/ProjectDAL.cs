﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class ProjectDAL
    {
        DataAccessLayer dal = new DataAccessLayer();


        public List<DataRow> GetAllProjectList(DataTableModel model, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[12];
            try
            {

                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);

                param[3] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                param[4] = new SqlParameter("@PageNumber", model.PageNumber);
                param[5] = new SqlParameter("@PageSize", model.PageSize);
                param[6] = new SqlParameter("@SortColumn", model.SortColumn ?? string.Empty);
                param[7] = new SqlParameter("@SortDirection", model.SortDirection ?? string.Empty);
                param[8] = new SqlParameter("@TotalCount", totalcount)
                {
                    Direction = ParameterDirection.Output
                };

                //For Audit Trail
                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.SessionID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);



                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectMasterList_SelectByPage, param);
                ds.Tables[0].TableName = "ProjectMasterList";
                totalcount = Convert.ToInt32(param[8].Value);
                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProjectList");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public List<DataRow> GetProjectMasterRecordByProjectID(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                param[0] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[1] = new SqlParameter("@UserId", model.UserID);
                param[2] = new SqlParameter("@RoleId", model.RoleID);
                param[3] = new SqlParameter("@CompanyId", model.CompanyID);


                //For Audit Trail
                param[4] = new SqlParameter("@appAuditID", model.appAuditID);
                param[5] = new SqlParameter("@SessionID", model.SessionID);
                param[6] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectMaster_SelectByProjectID, param);
                ds.Tables[0].TableName = "ProjectMasterRecords";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectMasterRecordByProjectID");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public List<DataRow> GetAllProjectJobList(DataTableModel model, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[11];
            try
            {

                param[0] = new SqlParameter("@ProjectNo", model.ProjectNo);

                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                param[2] = new SqlParameter("@PageNumber", model.PageNumber);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn ?? string.Empty);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection ?? string.Empty);
                param[6] = new SqlParameter("@TotalCount", totalcount)
                {
                    Direction = ParameterDirection.Output
                };
                param[7] = new SqlParameter("@UserId", model.UserID);

                //For Audit Trail
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectJobList_SelectByPage, param);
                ds.Tables[0].TableName = "ProjectJobList";
                totalcount = Convert.ToInt32(param[6].Value);
                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProjectJobList");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }


        public List<DataRow> GetAllProject(ProjectSearchInfoViewModel obj, ref int totalcount)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                param[0] = new SqlParameter("@ParentProjectId", obj.ParentProjectId);
                param[1] = new SqlParameter("@SearchTerm", obj.SearchCriteria.SearchString ?? string.Empty);
                param[2] = new SqlParameter("@PageNumber", obj.PageNo);
                param[3] = new SqlParameter("@PageSize", obj.PageSize);
                param[4] = new SqlParameter("@SortColumn", obj.SearchCriteria.SortColumn ?? string.Empty);
                param[5] = new SqlParameter("@SortDirection", obj.SearchCriteria.SortOrder ?? string.Empty);
                param[6] = new SqlParameter("@TotalCount", obj.TotalCount)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectMaster_SelectByPage, param);
                ds.Tables[0].TableName = "CompanyMasterDetails";
                totalcount = Convert.ToInt32(param[6].Value);
                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProject");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;

        }

        public bool UpdateProjectMaster(ProjectMasterEntity projectMaster)
        {

            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[12];
            int result;

            try
            {

                param[0] = new SqlParameter("@ProjectNo", projectMaster.ProjectNo);
                param[1] = new SqlParameter("@ContractorSuper", projectMaster.ContractorSuper);
                param[2] = new SqlParameter("@ContractorProjMgr", projectMaster.ContractorProjMgr);
                param[3] = new SqlParameter("@ContractorContactPerson", projectMaster.ContractorContactPerson);
                param[4] = new SqlParameter("@SchindlerSuper", projectMaster.SchindlerSuperintendent);
                param[5] = new SqlParameter("@SchindlerSales", projectMaster.SchindlerSalesExecutive);
                param[6] = new SqlParameter("@SchindlerProjMgr", projectMaster.SchindlerProjMgr);
                param[7] = new SqlParameter("@TurnOverDate", projectMaster.TurnOverDate);
                param[8] = new SqlParameter("@Status", projectMaster.Status);
                param[9] = new SqlParameter("@UpdatedBy", projectMaster.UpdatedBy);
                param[10] = new SqlParameter("@appAuditID", projectMaster.appAuditID);
                param[11] = new SqlParameter("@SessionID", projectMaster.SessionID);


                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateProjectMasterByID, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectMaster");
                throw ex;
            }
            finally
            {
                dal.Close();
            }


        }

        public bool CheckDocumentName(ProjectDocumentEntity checkdata)
        {
            DataAccessLayer dal = new DataAccessLayer();
            SqlParameter[] param = new SqlParameter[5];

            try
            {

                param[0] = new SqlParameter("@ProjectID", checkdata.ProjectID);
                param[1] = new SqlParameter("@JobID", checkdata.JobId);
                param[2] = new SqlParameter("@DocumentName", checkdata.DocumentName);
                param[3] = new SqlParameter("@DocumentTypeID", checkdata.DocumentTypeID);
                param[4] = new SqlParameter("@DocumentID", checkdata.DocumentID);

                var result = dal.ExecuteScalar(CommandType.StoredProcedure, StoredProcedure.CheckDocumentName, param);

                if (Convert.ToInt16(result) >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectMaster");
                throw ex;
            }
            finally
            {
                dal.Close();
            }

        }

        public List<DataRow> GetProjectJobRecordByJobNo(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            try
            {
                param[0] = new SqlParameter("@JobNo", model.JobNo);
                //param[1] = new SqlParameter("@UserId", model.UserID);
                //param[2] = new SqlParameter("@RoleId", model.RoleID);
                //param[3] = new SqlParameter("@CompanyId", model.CompanyID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectJob_SelectByJobNo, param);
                ds.Tables[0].TableName = "ProjectJobRecord";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectJobRecordByJobNo");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public List<DataRow> GetProjectDocumentList(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[15];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@RoleId", model.RoleID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);
                param[3] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[4] = new SqlParameter("@JobNo", model.JobNo);
                param[5] = new SqlParameter("@DocumentType", model.DocumentType);
                param[6] = new SqlParameter("@PageNumber", model.PageNumber);
                param[7] = new SqlParameter("@PageSize", model.PageSize);
                param[8] = new SqlParameter("@SortColumn", model.SortColumn);
                param[9] = new SqlParameter("@SortDirection", model.SortDirection);
                param[10] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[11] = new SqlParameter("@UserId", model.UserID);

                //For Audit Trail
                param[12] = new SqlParameter("@appAuditID", model.appAuditID);
                param[13] = new SqlParameter("@SessionID", model.SessionID);
                param[14] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectDocumentSelectbyType_CompanywiseSP, param);
                ds.Tables[0].TableName = "ProjectDocumentDetails";

                count = Convert.ToInt32(param[10].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;

        }

        public List<DataRow> AddProjectDocument(ProjectDocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[1] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[2] = new SqlParameter("@Description", doc.Description);
                param[3] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[4] = new SqlParameter("@Expireddate", doc.ExpiredDate);
                param[5] = new SqlParameter("@PublishedBy", doc.PublishedBy);
                param[6] = new SqlParameter("@IsDeleted", doc.IsDeleted);
                param[7] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[8] = new SqlParameter("@SessionID", doc.SessionID);
                param[9] = new SqlParameter("@FileType", doc.FileType);
                param[10] = new SqlParameter("@StatusId", doc.StatusId);
                param[11] = new SqlParameter("@Projectname", HttpUtility.HtmlDecode(doc.ProjectName));
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@Jobname", HttpUtility.HtmlDecode(doc.JobName));
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);
                //{
                //    Direction = ParameterDirection.Output
                //};

                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentInsert, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DocumentMannage");
                throw ex;
            }

            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public List<DataRow> GetProjectDocumentRecordByDocID(int DocID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", DocID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentSelectById, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetProjectDocumentRecordByDocID");
                throw ex;
            }

            List<DataRow> DocumentData = ds.Tables[0].AsEnumerable().ToList();
            return DocumentData;
        }

        public List<DataRow> UpdateProjectDocument(ProjectDocumentEntity doc)
        {
            if (doc.DocumentPath == null && doc.FileType == null)
            {
                doc.DocumentPath = "-1";
                doc.FileType = "-1";
            }
            SqlParameter[] param = new SqlParameter[17];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DocumentName", doc.DocumentName);
                param[2] = new SqlParameter("@DocumentTypeID", doc.DocumentTypeID);
                param[3] = new SqlParameter("@Description", doc.Description);
                param[4] = new SqlParameter("@DocumentPath", doc.DocumentPath);
                param[5] = new SqlParameter("@Expireddate", doc.ExpiredDate);
                param[6] = new SqlParameter("@UpdatedBy", doc.UpdatedBy);
                param[7] = new SqlParameter("@FileType", doc.FileType);
                param[8] = new SqlParameter("@StatusId", doc.StatusId);
                param[9] = new SqlParameter("@Projectname", doc.ProjectName);
                param[10] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[11] = new SqlParameter("@SessionID", doc.SessionID);
                param[12] = new SqlParameter("@Companyname", doc.CompanyName);
                param[13] = new SqlParameter("@Jobname", doc.JobName);
                param[14] = new SqlParameter("@IsPublished", doc.IsPublished);
                param[15] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[16] = new SqlParameter("@UserId", doc.UserId);

                //{
                //    Direction = ParameterDirection.Output
                //};

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentUpdateSP, param);
                ds.Tables[0].TableName = "DocumentDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectDocument");
                throw ex;
            }

            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public bool DeleteProjectDocument(ProjectDocumentEntity doc)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@DocumentID", doc.DocumentID);
                param[1] = new SqlParameter("@DeletedBy", doc.DeletedBy);
                param[2] = new SqlParameter("@appAuditID", doc.appAuditID);
                param[3] = new SqlParameter("@SessionID", doc.SessionID);
                param[4] = new SqlParameter("@ApplicationOperation", doc.ApplicationOperation);
                param[5] = new SqlParameter("@UserId", doc.UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DocumentDeleteSP, param);
                return true;


            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }
        }

        public Tuple<List<DataRow>, List<DataRow>, List<DataRow>> GetProjectMilestoneChart(DataTableModel model)
        {
            DataSet ds = new DataSet();
            DataTable tmp = new DataTable();
            tmp.Columns.Add("Data");
            SqlParameter[] param = new SqlParameter[3];
            try
            {

                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectMilestoneProgress, param);
                ds.Tables[0].TableName = "ProjectMasterList";

                dal.Close();

                List<DataRow> lstDrMilestorne = new List<DataRow>();

                if (ds.Tables.Count >= 3)
                {


                    string data = "";
                    data = "Results";
                    foreach (DataColumn dc in ds.Tables[2].Columns)
                    {
                        switch (dc.ColumnName.ToUpper())
                        {
                            case "PROJECTNO":
                            case "PROJECTNAME":
                            case "MILESTONECOUNT":
                            case "PERCENTAGECOMPLETE":
                                {
                                    break;
                                }
                            default:
                                {
                                    if (data.Length > 0) { data = data + ","; }
                                    data = data + "" + dc.ColumnName + "";
                                    break;
                                }
                        }
                    }
                    DataRow dr = tmp.NewRow();
                    dr["Data"] = data;
                    tmp.Rows.Add(dr);
                    data = "";

                    foreach (DataRow dr1 in ds.Tables[2].Rows)
                    {
                        data = "";
                        DataRow dr2 = tmp.NewRow();
                        foreach (DataColumn dc in ds.Tables[2].Columns)
                        {
                            switch (dc.ColumnName.ToUpper())
                            {
                                case "PROJECTNO":
                                    {
                                        if (data.Length > 0) { data = data + ","; }
                                        data = data + "" + dr1[dc.ColumnName] + "";
                                        break;
                                    }
                                case "MILESTONECOUNT":
                                case "PROJECTNAME":
                                case "PERCENTAGECOMPLETE":
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        if (data.Length > 0) { data = data + ","; }
                                        data = data + dr1[dc.ColumnName];
                                        break;
                                    }
                            }
                        }

                        dr2["Data"] = data;
                        tmp.Rows.Add(dr2);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetProjectMilestoneChart");
                throw ex;
            }
            Tuple<List<DataRow>, List<DataRow>, List<DataRow>> postobj = new Tuple<List<DataRow>, List<DataRow>, List<DataRow>>(ds.Tables[0].AsEnumerable().ToList(), ds.Tables[1].AsEnumerable().ToList(), tmp.AsEnumerable().ToList());
            return postobj;

        }

        public List<DataRow> DashboardProjectListingMilestone(DataTableModel model)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DashboardProjectListingSP, param);
                ds.Tables[0].TableName = "ProjectJobsMilestone";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DashboardProjectListingMilestone");
                throw ex;
            }

            List<DataRow> projectData = ds.Tables[0].AsEnumerable().ToList();
            return projectData;
        }


        public List<DataRow> GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model)
        {
            SqlParameter[] param = new SqlParameter[3];
            DataSet ds = new DataSet();
            try
            {
                model.Milestone = model.Milestone.Replace("’", "'");

                param[0] = new SqlParameter("@JobNo", model.JobNo);
                param[1] = new SqlParameter("@ProjectNo", model.ProjectNo);
                param[2] = new SqlParameter("@MilestoneName", model.Milestone);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DueDateStatusSP, param);
                ds.Tables[0].TableName = "DueDateStatus";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetDueDateAndStatusOfMilestone");
                throw ex;
            }

            List<DataRow> projectData = ds.Tables[0].AsEnumerable().ToList();
            return projectData;
        }


        public bool AddProjectBankMilestoneComment(string comment, string projectBankTitle, int userID)
        {
            SqlParameter[] param = new SqlParameter[3];
            try
            {
                param[0] = new SqlParameter("@note", comment);
                param[1] = new SqlParameter("@projobmile", projectBankTitle);
                param[2] = new SqlParameter("@UserId", userID);

                dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.ProjectMilestoneNoteInsertSP, param);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectBankMilestoneComment");
                throw ex;
            }

            return true;
        }

        public bool AddProjectBankMilestoneCommentDueDate(string comment, string projectBankTitle, int userID, string statusid, string duedate, string completeddate)
        {

            projectBankTitle = projectBankTitle.Replace("'", "'");
            projectBankTitle = projectBankTitle.Replace("’", "'");

            SqlParameter[] param = new SqlParameter[6];
            try
            {
                
                param[0] = new SqlParameter("@note", comment);
                param[1] = new SqlParameter("@projobmile", projectBankTitle);
                param[2] = new SqlParameter("@UserId", userID);
                param[3] = new SqlParameter("@status", Convert.ToInt32(statusid));
           

                if (!string.IsNullOrEmpty(duedate))
                    param[4] = new SqlParameter("@duedate", Convert.ToDateTime(duedate));
                else
                    param[4] = new SqlParameter("@duedate", DBNull.Value);


                if (!string.IsNullOrEmpty(completeddate))
                    param[5] = new SqlParameter("@completeddate", Convert.ToDateTime(completeddate));
                else
                    param[5] = new SqlParameter("@completeddate", DBNull.Value);

                int count = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.ProjectMilestonDueDateInsertSP, param);

                dal.Close();

                if (count >= 1)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectBankMilestoneComment");
                throw ex;
            }

            return false;
        }

        public List<DataRow> GetJobData(DataTableModel model)
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@DateFrom", model.LogDateFrom);
                param[3] = new SqlParameter("@DateTo", model.LogDateTo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobMasterListing, param);
                ds.Tables[0].TableName = "GetJobMasterListing";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobData");
                throw ex;
            }
            List<DataRow> JobData = ds.Tables[0].AsEnumerable().ToList();
            return JobData;
        }

        public string GetProjectData(string ProjectNo, string JobNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", ProjectNo);
                param[1] = new SqlParameter("@JobNo", JobNo);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectandJobData, param);
                ds.Tables[0].TableName = "GetProjectName";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobData");
                throw ex;
            }
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public DataTable GetBankListWithDetail(DataTableModel model, ref int totalcount, ref int projectCompanyId)
        {

            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[13];
            try
            {

                param[0] = new SqlParameter("@UserId", model.UserID);
                param[1] = new SqlParameter("@RoleId", model.RoleID);
                param[2] = new SqlParameter("@CompanyId", model.CompanyID);

                param[3] = new SqlParameter("@ProjectNumber", model.ProjectNumber);
                param[4] = new SqlParameter("@JobNo", model.JobNo);

                param[5] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                param[6] = new SqlParameter("@PageNumber", model.PageNumber);
                param[7] = new SqlParameter("@PageSize", model.PageSize);
                param[8] = new SqlParameter("@TotalCount", totalcount)
                {
                    Direction = ParameterDirection.Output
                };

                //For Audit Trail
                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.SessionID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[12] = new SqlParameter("@ProjectCompanyId", projectCompanyId)
                {
                    Direction = ParameterDirection.Output
                };


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.JobList_GetJobPivotTable, param);
                ds.Tables[0].TableName = "ProjectMasterList";
                totalcount = Convert.ToInt32(param[8].Value);
                projectCompanyId = Convert.ToInt32(param[12].Value);
                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProjectList");
                throw ex;
            }

            // List<DataRow> postobj = ds.Tables[0].AsEnumerable().ToList();
            //var postobj = ds.Tables[0].Rows;
            return ds.Tables[0];

        }

        public List<DataRow> GetDashboardPieDocInvoiceAndContact(DataTableModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[5];
            try
            {
                param[0] = new SqlParameter("@ProjectNumber", model.ProjectNumber.Trim());
                param[1] = new SqlParameter("@UserId", model.UserID);
                param[2] = new SqlParameter("@RoleId", model.RoleID);
                param[3] = new SqlParameter("@CompanyId", model.CompanyID);
                param[4] = new SqlParameter("@ViewType", model.ModuleName.Trim());

                //param[5] = new SqlParameter("@SearchTerm", model.SearchTerm ?? string.Empty);
                //param[6] = new SqlParameter("@appAuditID", model.appAuditID);
                //param[7] = new SqlParameter("@SessionID", model.SessionID);
                //param[8] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetPaymentDocInvoiceAndContact, param);
                ds.Tables[0].TableName = "GraphAndDOcData";
                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectList - GetAllProjectList");
                throw ex;
            }
            var postobj = ds.Tables[0].AsEnumerable().ToList();
            return postobj;
        }

        public bool UpdateBankDescByJobNo(string Description, string UpdatedBy, int appAuditID, string SessionID, string JobNo, string ApplicationOperation)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@BankDesc", Description);

                param[1] = new SqlParameter("@UpdatedBy", UpdatedBy);

                param[2] = new SqlParameter("@appAuditID", appAuditID);
                param[3] = new SqlParameter("@SessionID", SessionID);

                param[4] = new SqlParameter("@JobNo", JobNo);
                param[5] = new SqlParameter("@ApplicationOperation", ApplicationOperation);

                int result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.JobMaster_UpdateBankDescByJobNo, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateBankDescByJobNo");
                throw ex;
            }

            return false;
        }

        public bool updateEmailAndPhoneData(DataTableModel projectMaster)
        {
            DataAccessLayer dal = new DataAccessLayer();
            //SqlParameter[] param = new SqlParameter[9];
            SqlParameter[] param = new SqlParameter[10];
            int result;

            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectMaster.ProjectNo);
                param[1] = new SqlParameter("@CompanyID", projectMaster.CompanyID);
                param[2] = new SqlParameter("@Email", projectMaster.UserEmail);
                param[3] = new SqlParameter("@Phone", projectMaster.Phone);
                param[4] = new SqlParameter("@Role", projectMaster.RoleName);
                param[5] = new SqlParameter("@UpdatedBy", projectMaster.UserID);
                param[6] = new SqlParameter("@appAuditID", projectMaster.appAuditID);
                param[7] = new SqlParameter("@SessionID", projectMaster.SessionID);
                param[8] = new SqlParameter("@ApplicationOperation", projectMaster.ApplicationOperation);
                param[9] = new SqlParameter("@Name", projectMaster.Name);
                result = dal.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedure.UpdateEmailAndPhoneData, param);

                if (result >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - updateEmailAndPhoneData");
                throw ex;
            }
            finally
            {
                dal.Close();
            }
        }
    }
}