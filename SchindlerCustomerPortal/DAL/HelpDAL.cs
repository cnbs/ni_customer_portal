﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Help;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class HelpDAL
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> HelpInsert(HelpEntity help)
        {
            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@ModuleId", help.ModuleId );
                param[1] = new SqlParameter("@Title",help.Title );
                param[2] = new SqlParameter("@Description", help.Description);
                param[3] = new SqlParameter("@PublishedStatus", help.PublishedStatus );
                param[4] = new SqlParameter("@CreatedBy",help.CreatedBy);
                param[5] = new SqlParameter("@appAuditID", help.appAuditID);
                param[6] = new SqlParameter("@SessionID", help.SessionID);
                param[7] = new SqlParameter("@ApplicationOperation", help.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.HelpInsert , param);
                ds.Tables[0].TableName = "HelpDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - HelpInsert");
                throw ex;
            }

            List<DataRow> HelpData = ds.Tables[0].AsEnumerable().ToList();
            return HelpData;
        }

        public List<DataRow> HelpSelect(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[6] = new SqlParameter("@UserID", model.UserID);
                param[7] = new SqlParameter("@appAuditID", model.appAuditID);
                param[8] = new SqlParameter("@SessionID", model.SessionID);
                param[9] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.HelpSelect, param);
                ds.Tables[0].TableName = "HelpDetails";

                count = Convert.ToInt32(param[5].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - HelpSelect");
                throw ex;
            }

            List<DataRow> helpData = ds.Tables[0].AsEnumerable().ToList();
            return helpData;

        }

        public List<DataRow> HelpSelectById(int HelpId)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@HelpId", HelpId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.HelpSelectById, param);
                ds.Tables[0].TableName = "HelpDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - HelpSelectById");
                throw ex;
            }

            List<DataRow> HelpData = ds.Tables[0].AsEnumerable().ToList();
            return HelpData;
        }

        public List<DataRow> HelpUpdate(HelpEntity help)
        {
            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@HelpId", help.HelpId);
                param[1] = new SqlParameter("@ModuleId", help.ModuleId);
                param[2] = new SqlParameter("@Title", help.Title);
                param[3] = new SqlParameter("@Description", help.Description);
                param[4] = new SqlParameter("@PublishedStatus", help.PublishedStatus);
                param[5] = new SqlParameter("@appAuditID", help.appAuditID);
                param[6] = new SqlParameter("@SessionID", help.SessionID);
                param[7] = new SqlParameter("@UpdatedBy", help.UpdatedBy);
                param[8] = new SqlParameter("@ApplicationOperation", help.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.HelpUpdateSP, param);
                ds.Tables[0].TableName = "HelpDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - HelpUpdate");
                throw ex;
            }

            List<DataRow> DocData = ds.Tables[0].AsEnumerable().ToList();
            return DocData;
        }

        public bool DeleteHelp(HelpEntity help)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@HelpId", help.HelpId);
                param[1] = new SqlParameter("@deletedby", help.DeletedBy);
                param[2] = new SqlParameter("@appauditid", help.appAuditID);
                param[3] = new SqlParameter("@sessionid", help.SessionID);
                param[4] = new SqlParameter("@ApplicationOperation", help.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.HelpDeleteSP, param);
                return true;


            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteHelp");
                throw ex;
            }
        }

        public List<DataRow> HelpSelectByModule(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[13];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@ModuleId ", model.ModuleId);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection);
                param[6] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[7] = new SqlParameter("@RoleId", model.RoleID);
                param[8] = new SqlParameter("@Flag", model.FlagValue);
                param[9] = new SqlParameter("@UserID", model.UserID);
                param[10] = new SqlParameter("@appAuditID", model.appAuditID);
                param[11] = new SqlParameter("@SessionID", model.SessionID);
                param[12] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ModuleHelp_SelectAll, param);
                ds.Tables[0].TableName = "HelpDetails";

                count = Convert.ToInt32(param[6].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - HelpSelectByModule");
                throw ex;
            }

            List<DataRow> helpData = ds.Tables[0].AsEnumerable().ToList();
            return helpData;

        }
    }
}