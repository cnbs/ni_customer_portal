﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class ModuleDAL
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> GetAllModules()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = dal.ExecuteDataSetWithoutParameter(CommandType.StoredProcedure, StoredProcedure.GetAllModules);
                ds.Tables[0].TableName = "CompanyMasterDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Module");
                throw ex;
            }
            var moduleList = ds.Tables[0].AsEnumerable().ToList();
            return moduleList;

        }

        public List<DataRow> GetModuleByName(string ModuleName)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@modulename", ModuleName);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetModuleByName, param);
                ds.Tables[0].TableName = "ModuleDetails";

                dal.Close();
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Help");
                throw ex;
            }
            var moduleList = ds.Tables[0].AsEnumerable().ToList();
            return moduleList;

        }

    }
}