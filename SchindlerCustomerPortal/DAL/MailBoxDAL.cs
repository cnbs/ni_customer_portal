﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class MailBoxDAL
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> GetUserByCompanyIdForMailBox(int companyId, string searchTerm)
        {

            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@CompanyId", companyId);
                param[1] = new SqlParameter("@SearchTerm", searchTerm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetUserByCompanyForMailBox, param);
                ds.Tables[0].TableName = "UserMaster";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserByCompanyIdForMailBox");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> SendNewMail(MailBoxEntity user)
        {
            SqlParameter[] param = new SqlParameter[15];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyId", user.CompanyId);
                param[1] = new SqlParameter("@SenderId", user.Sender);
                //param[2] = new SqlParameter("@ReceiverId", user.Receiver);
                param[2] = new SqlParameter("@Tolist", user.ToList);
                param[3] = new SqlParameter("@CC", user.CC);
                param[4] = new SqlParameter("@BCC", user.BCC);
                param[5] = new SqlParameter("@Subject", user.Subject);
                param[6] = new SqlParameter("@Message", user.Message);
                param[7] = new SqlParameter("@HasAttachment", user.HasAttachment);
                param[8] = new SqlParameter("@View_Status", user.ViewStatus);
                param[9] = new SqlParameter("@Status", user.Status);
                param[10] = new SqlParameter("@appAuditID", user.appAuditID);
                param[11] = new SqlParameter("@SessionID", user.SessionID);
                param[12] = new SqlParameter("@AttachmentPath", user.AttachmentPath);

                param[13] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[14] = new SqlParameter("@UserID", user.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MailBoxInsert, param);
                //ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMailByUserID, param);
                ds.Tables[0].TableName = "MailBox";

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SendNewMail");
                throw ex;
            }

            //return true;
            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public int SaveDraftMail(MailBoxEntity user)
        {
            SqlParameter[] param = new SqlParameter[15];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@CompanyId", user.CompanyId);
                param[1] = new SqlParameter("@SenderId", user.Sender);
                //param[2] = new SqlParameter("@ReceiverId", user.Receiver);
                param[2] = new SqlParameter("@Tolist", user.ToList);
                param[3] = new SqlParameter("@CC", user.CC);
                param[4] = new SqlParameter("@BCC", user.BCC);
                param[5] = new SqlParameter("@Subject", user.Subject);
                param[6] = new SqlParameter("@Message", user.Message);
                param[7] = new SqlParameter("@HasAttachment", user.HasAttachment);
                param[8] = new SqlParameter("@View_Status", user.ViewStatus);
                param[9] = new SqlParameter("@Status", user.Status);
                param[10] = new SqlParameter("@appAuditID", user.appAuditID);
                param[11] = new SqlParameter("@SessionID", user.SessionID);
                param[12] = new SqlParameter("@AttachmentPath", user.AttachmentPath);

                param[13] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
                param[14] = new SqlParameter("@UserID", user.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SaveDraftData, param);
                //ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMailByUserID, param);
                ds.Tables[0].TableName = "SaveDraftResult";

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - SaveDraftMail");
                throw ex;
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0]["Result"].ToString());
        }

        public List<DataRow> GetUserMailByEmail(DataTableModel model, ref int count)
        {

            SqlParameter[] param = new SqlParameter[13];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@companyid", model.CompanyID);
                param[1] = new SqlParameter("@emailid", model.UserEmail);
                param[2] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[8] = new SqlParameter("@statusId", model.StatusValue);

                param[9] = new SqlParameter("@appAuditID", model.appAuditID);
                param[10] = new SqlParameter("@SessionID", model.SessionID);
                param[11] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);
                param[12] = new SqlParameter("@UserID", model.UserID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMailByUserID, param);
                ds.Tables[0].TableName = "MailBox";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailByEmail");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> GetUserMailByMailId(int mailId, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {

            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@MailId", mailId);
                param[1] = new SqlParameter("@appAuditID", appAuditID);
                param[2] = new SqlParameter("@SessionID", SessionID);
                param[3] = new SqlParameter("@ApplicationOperation", ApplicationOperation);
                param[4] = new SqlParameter("@UserID", UserId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMailByMailId, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailByMailId");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> GetUserMailCount(string mail)
        {

            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ReceiverId", mail);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetMailCount, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailCount");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public bool UpdateStatusOfUserMail(string mailID, string viewStatus, int status, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {
            SqlParameter[] param = new SqlParameter[7];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@mailids", mailID);
                param[1] = new SqlParameter("@view_status", viewStatus);
                param[2] = new SqlParameter("@status", status);

                param[3] = new SqlParameter("@appAuditID", appAuditID);
                param[4] = new SqlParameter("@SessionID", SessionID);
                param[5] = new SqlParameter("@ApplicationOperation", ApplicationOperation);
                param[6] = new SqlParameter("@UserID", UserId);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateMailSattus, param);

                return true;


            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateStatusOfUserMail");
                throw ex;
            }
        }

        public bool GetUserMailLogicalDelete(string mailID, int appAuditID, string sessionID, int UserId, string ApplicationOperation)
        {

            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                //bool delete = false;
                param[0] = new SqlParameter("@mailid", mailID);
                param[1] = new SqlParameter("@appauditid", appAuditID);
                param[2] = new SqlParameter("@sessionid", sessionID);
                param[3] = new SqlParameter("@UpdatedBy", UserId);
                param[4] = new SqlParameter("@ApplicationOperation", ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MailDeleteforLogicalDelete, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailLogicalDelete");
                throw ex;
            }

            //List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            //return userData;

            return true;

        }

        public List<DataRow> GetUserMailforHeaderMenu(string mail)
        {

            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@mailId", mail);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MailListForHeaderMenu, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailforHeaderMenu");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> GetUserMailforDashboardWidget(string mail)
        {

            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@mailId", mail);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.MailListForDashboardWidget, param);
                ds.Tables[0].TableName = "MailBox";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetUserMailforDashboardWidget");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }
    }
}