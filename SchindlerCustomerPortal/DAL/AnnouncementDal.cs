﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchindlerCustomerPortal.Models;
using System.Data.SqlClient;
using System.Data;
using SchindlerCustomerPortal.Common;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class AnnouncementDal
    {
        DataAccessLayer dal = new DataAccessLayer();
        public List<DataRow> AnnouncementSelect(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@PageNumber", model.PageNumber);
                param[2] = new SqlParameter("@PageSize", model.PageSize);
                param[3] = new SqlParameter("@SortColumn", model.SortColumn);
                param[4] = new SqlParameter("@SortDirection", model.SortDirection);
                param[5] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[6] = new SqlParameter("@UserID", model.UserID);
                param[7] = new SqlParameter("@appAuditID", model.appAuditID);
                param[8] = new SqlParameter("@SessionID", model.SessionID);
                param[9] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectAnnouncementMessage, param);
                ds.Tables[0].TableName = "Announcement";

                count = Convert.ToInt32(param[5].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AnnouncementSelect");
                throw ex;
            }

            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;

        }
        public List<DataRow> InsertAnnouncement(AnnouncementEntity data)
        {
            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@MessageText", data.MessageText);
                param[1] = new SqlParameter("@Frequency", data.Frequency);
                param[2] = new SqlParameter("@DisplayOn", data.DisplayOn);
                param[3] = new SqlParameter("@ValidDate", data.ValidDate);
                param[4] = new SqlParameter("@CreatedBy", data.CreatedBy);
                param[5] = new SqlParameter("@appAuditID", data.appAuditID);
                param[6] = new SqlParameter("@SessionID", data.SessionID);
                param[7] = new SqlParameter("@ApplicationOperation", data.ApplicationOperation);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.InsertAnnouncementMessage, param);
                ds.Tables[0].TableName = "AnnouncementDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - InsertAnnouncement");
                throw ex;
                
            }
            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;

        }
        public List<DataRow> AnnouncementSelectById(int AnnouncementID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@AnnMsgId", AnnouncementID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectAnnouncementMessagebyID, param);
                ds.Tables[0].TableName = "AnnouncementDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AnnouncementSelectById");
                throw ex;
            }

            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;
        }
        public List<DataRow> UpdateAnnouncement(AnnouncementEntity data)
        {
            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@AnnMsgId", data.AnnMsgId);
                param[1] = new SqlParameter("@MessageText", data.MessageText);
                param[2] = new SqlParameter("@Frequency", data.Frequency);
                param[3] = new SqlParameter("@DisplayOn", data.DisplayOn);
                param[4] = new SqlParameter("@ValidDate", data.ValidDate);
                param[5] = new SqlParameter("@UpdatedBy", data.UpdatedBy);
                param[6] = new SqlParameter("@appAuditID", data.appAuditID);
                param[7] = new SqlParameter("@SessionID", data.SessionID);
                param[8] = new SqlParameter("@ApplicationOperation", data.ApplicationOperation);


                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateAnnouncementMessage, param);
                ds.Tables[0].TableName = "AnnouncementDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateAnnouncement");
                throw ex;
                
            }

            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;
        }

        public bool DeleteAnnouncement(AnnouncementEntity data)
        {
            SqlParameter[] param = new SqlParameter[5];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@AnnMsgId", data.AnnMsgId);
                param[1] = new SqlParameter("@DeletedBy", data.DeletedBy);
                param[2] = new SqlParameter("@appAuditID", data.appAuditID);
                param[3] = new SqlParameter("@SessionID", data.SessionID);
                param[4] = new SqlParameter("@ApplicationOperation", data.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DeleteAnnouncementMessage, param);
                return true;


            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteHelp");
                throw ex;
            }
        }

        public List<DataRow> AllAnnouncementSelect(string DisplayOn)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {

                param[0] = new SqlParameter("@DisplayOn", DisplayOn);
               

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectAllAnnouncementMessage, param);
                ds.Tables[0].TableName = "Announcement";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AnnouncementSelect");
                throw ex;
            }

            List<DataRow> AnnouncementData = ds.Tables[0].AsEnumerable().ToList();
            return AnnouncementData;

        }

    }
}