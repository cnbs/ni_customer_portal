﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class ProjectDocumentDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public List<DataRow> DocumentSelectByProject(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[8];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[1] = new SqlParameter("@ProjectId ",model.ProjectID);
                param[2] = new SqlParameter("@DocumentType", model.DocumentType);
                param[3] = new SqlParameter("@PageNumber", model.PageNumber);
                param[4] = new SqlParameter("@PageSize", model.PageSize);
                param[5] = new SqlParameter("@SortColumn", model.SortColumn);
                param[6] = new SqlParameter("@SortDirection", model.SortDirection);
                param[7] = new SqlParameter("@TotalCount", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ContractDocument_SelectAll, param);
                ds.Tables[0].TableName = "DocumentDetails";

                count = Convert.ToInt32(param[7].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Document");
                throw ex;
            }

            List<DataRow> docData = ds.Tables[0].AsEnumerable().ToList();
            return docData;

        }

        public List<DataRow> ProjectSelectById(int ProjectID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@Projectid", ProjectID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectSelectById, param);
                ds.Tables[0].TableName = "ProjectDetails";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Project");
                throw ex;
            }

            List<DataRow> DocumentData = ds.Tables[0].AsEnumerable().ToList();
            return DocumentData;
        }
    }
}