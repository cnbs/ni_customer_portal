﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.DAL
{
    public class ProjectContactDAL
    {
        DataAccessLayer dal = new DataAccessLayer();

        public bool AddProjectContact(ProjectContactEntity user)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ProjectNo", user.ProjectNo);
                param[1] = new SqlParameter("@FirstName", user.FirstName);
     
                param[2] = new SqlParameter("@Designation", user.Designation);
                param[3] = new SqlParameter("@email", user.Email);
                param[4] = new SqlParameter("@phoneno", user.ContactNumber);
               
                param[5] = new SqlParameter("@CreatedBy", user.CreatedBy);
                param[6] = new SqlParameter("@appAuditID", user.appAuditID);
                param[7] = new SqlParameter("@SessionID", user.SessionID);
                param[8] = new SqlParameter("@JobNo", user.JobNo);

                param[9] = new SqlParameter("@UserId", user.UserID);
                param[10] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);
               

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.AddProjectContactSP, param);

                var result = ds.Tables[0].Rows[0]["success"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - AddProjectContact");
                throw ex;
            }
        }

        public List<DataRow> ProjectContactSelectById(int contactID)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ContactID", contactID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.ProjectContactByIdSP, param);
                ds.Tables[0].TableName = "Project_Contacts";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - ProjectContactSelectById");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public bool UpdateProjectContact(ProjectContactEntity user)
        {
            SqlParameter[] param = new SqlParameter[10];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@ContactID", user.ContactID);
                param[1] = new SqlParameter("@FirstName", user.FirstName);
                
                param[2] = new SqlParameter("@Designation", user.Designation);
               
                param[3] = new SqlParameter("@phoneno", user.ContactNumber);
               
                param[4] = new SqlParameter("@UpdatedBy", user.UpdatedBy);
                param[5] = new SqlParameter("@appAuditID", user.appAuditID);
                param[6] = new SqlParameter("@SessionID", user.SessionID);
                param[7] = new SqlParameter("@JobNo", user.JobNo);

                param[8] = new SqlParameter("@UserId", user.UserID);
                param[9] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.UpdateProjectContactSP, param);

                var result = ds.Tables[0].Rows[0]["success"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - UpdateProjectContact");
                throw ex;
            }
        }

        public bool DeleteProjectContact(ProjectContactEntity user)
        {
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@Deletedby", user.UpdatedBy);
                param[1] = new SqlParameter("@appAuditID", user.appAuditID);
                param[2] = new SqlParameter("@SessionID", user.SessionID);
                param[3] = new SqlParameter("@ids", user.ContactIDList);

                param[4] = new SqlParameter("@UserId", user.UserID);
                param[5] = new SqlParameter("@ApplicationOperation", user.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.DeleteProjectContact, param);
                var result = ds.Tables[0].Rows[0]["success"].ToString();
                if (result == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - DeleteProjectContact");
                throw ex;
            }
        }

        public List<DataRow> ProjectContactList(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[11];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", model.ProjectNo);
                //param[1] = new SqlParameter("@UserID", model.UserID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@PageNumber", model.PageNumber);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection);
                param[6] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };

                param[7] = new SqlParameter("@UserId", model.UserID);

                //For Audit Trail
                param[8] = new SqlParameter("@appAuditID", model.appAuditID);
                param[9] = new SqlParameter("@SessionID", model.SessionID);
                param[10] = new SqlParameter("@ApplicationOperation", model.ApplicationOperation);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.SelectAllContactsByProjectId, param);
                ds.Tables[0].TableName = "UserProject_ContactsDetails";

                count = Convert.ToInt32(param[6].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }

        public List<DataRow> GetJobListByProjectNoandSearchTerm(int projectNo, string searchTerm)
        {

            SqlParameter[] param = new SqlParameter[2];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", projectNo);
                param[1] = new SqlParameter("@SearchTerm", searchTerm);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetJobBySearchTermandprojectNo, param);
                ds.Tables[0].TableName = "JobMaster";

                dal.Close();

            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetJobListByProjectNoandSearchTerm");
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;

        }

        public List<DataRow> GetProjectListByCompanyIdandSearchTerm(int CompanyId, string searchTerm, int RoleId, int UserId)
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();

            try
            {
                param[0] = new SqlParameter("@SearchTerm", searchTerm);
                param[1] = new SqlParameter("@companyid", CompanyId);
                param[2] = new SqlParameter("@RoleId", RoleId);
                param[3] = new SqlParameter("@UserId", UserId);
                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetProjectByCompanyIdList, param);
                ds.Tables[0].TableName = "ProjectList";

                dal.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> projectList = ds.Tables[0].AsEnumerable().ToList();

            return projectList;
        }

        public List<DataRow> ProjectContactListByCompanyId(DataTableModel model, ref int count)
        {
            SqlParameter[] param = new SqlParameter[9];
            DataSet ds = new DataSet();
            try
            {
                param[0] = new SqlParameter("@ProjectNo", model.ProjectNo);
                //param[1] = new SqlParameter("@UserID", model.UserID);
                param[1] = new SqlParameter("@SearchTerm", model.SearchTerm);
                param[2] = new SqlParameter("@PageNumber", model.PageNumber);
                param[3] = new SqlParameter("@PageSize", model.PageSize);
                param[4] = new SqlParameter("@SortColumn", model.SortColumn);
                param[5] = new SqlParameter("@SortDirection", model.SortDirection);
                param[6] = new SqlParameter("@TotalUser", SqlDbType.Int, 4)
                {
                    Direction = ParameterDirection.Output
                };
                param[7] = new SqlParameter("@CompanyId", model.CompanyID);
                param[8] = new SqlParameter("@RoleId", model.RoleID);

                ds = dal.ExecuteDataSet(CommandType.StoredProcedure, StoredProcedure.GetAllContactsByCompanyId, param);
                ds.Tables[0].TableName = "UserProject_ContactsDetails";

                count = Convert.ToInt32(param[6].Value);

                dal.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            List<DataRow> userData = ds.Tables[0].AsEnumerable().ToList();
            return userData;
        }
    }
}