﻿using SchindlerCustomerPortal.Models.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    /// <summary>
    /// Base Controller for accessing Current User accessing your User data in your all controller. 
    /// Inherit, your all controller from this base controller to access user information from the UserContext.
    /// </summary>
    public class BaseController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }

        //protected override void ExecuteCore()
        //{

        //    //var controllerName = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[1];

        //    //var action = "";
        //    //if (controllerName != "")
        //    //{
        //    //    action = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[2];
        //    //}

        //    if (User == null)
        //    {
        //        Redirect("~/Login/UserLogin");
        //        //if ((controllerName == "Login" || controllerName == "") && action != "Logout")
        //        //{
        //        //    var url = new UrlHelper(filterContext.RequestContext);
        //        //    var redirectURL = url.Content("~/Dashboard/Index");
        //        //    filterContext.HttpContext.Response.Redirect(redirectURL);
        //        //}
        //    }

        //    base.ExecuteCore();
        //}


    }
}