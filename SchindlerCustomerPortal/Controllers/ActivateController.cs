﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    public class ActivateController : Controller
    {
        private readonly IUserService _userService;

        public ActivateController(IUserService userService)
        {
            this._userService = userService;
        }

        /// <summary>
        /// return Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// User account verify based on TokenId
        /// </summary>
        /// <param name="TokenID"></param>
        /// <returns></returns>
        public ActionResult VerifyAccount(string TokenID)
        {
            // clear existing user cookie
            if (User != null)
            {
                if (User.GetType().Module.Name == "SchindlerCustomerPortal.dll")
                {
                    //Fetch the Cookie using its Key.
                    HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                    //Set the Expiry date to past date.
                    nameCookie.Expires = DateTime.Now.AddDays(-1);
                    //Update the Cookie in Browser.
                    Response.Cookies.Add(nameCookie);
                }
            }

            var CurrentTime = DateTime.Now;
            UserEntity TokenExpireDate = _userService.GetTokenExpiration(TokenID);
            if (CurrentTime > TokenExpireDate.TokenExpireDate)
            {
                ViewBag.Message = "Verification link is expired";
                //return View("VerifyAccount");

                TempData["msgdisplay"] = ViewBag.Message;
                return RedirectToAction("UserLogin", "Login", null);
            }
            else
            {

                var objUser = _userService.GetUserEmailFromToken(TokenID);

                int superAdminCompanyId = 0;

                if (objUser!=null)
                {

                    #region Superadmin Email Send

                    var superAdminList = _userService.CheckPermissionForMailNotification(superAdminCompanyId);

                    var model = _userService.Find(Convert.ToString(objUser.Email.ToLower()));

                    foreach (var item in superAdminList)
                    {
                        string Subject1 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                        string ActivationUrl1 = string.Empty;

                        string headurl1 = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        ActivationUrl1 = Server.HtmlEncode("" + headurl1 + "/Dashboard/Index");

                        string Sbody1 = null;

                        Dictionary<string, string> dic1 = new Dictionary<string, string>();

                        dic1.Add("PersonName", item.FirstName);

                        dic1.Add("NewRegisteredUser", model.FirstName);

                        dic1.Add("CompanyName", model.CompanyName);

                        dic1.Add("MessageURL", ActivationUrl1);

                        Sbody1 = StaticUtilities.getEmailBody("User", "NewUserRegisteredAdmin.html", dic1);

                        SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject1, Sbody1);
                    }

                    #region Push Notification
                    foreach (var item in superAdminList)
                    {

                        

                        string ActivationUrl1 = string.Empty;

                        string headurl1 = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        ActivationUrl1 = Server.HtmlEncode("" + headurl1 + "/Dashboard/Index");

                       

                        Dictionary<string, string> dic1 = new Dictionary<string, string>();

                        dic1.Add("PersonName", item.FirstName);

                        dic1.Add("NewRegisteredUser", model.FirstName);

                        dic1.Add("CompanyName", model.CompanyName);

                        dic1.Add("MessageURL", ActivationUrl1);


                        string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "New User Registered";
                                string NotificationBody = StaticUtilities.getNotificationBody("User", "NewUserRegisteredAdmin.txt", dic1);

                                try
                                {

                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {
                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }


                                //Insert into NotificationLogId table

                                data.CompanyID = superAdminCompanyId;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(item.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                               // data.CreatedBy = getUserCompanyName.FirstName + " " + getUserCompanyName.FirstName;
                                data.DeviceType = item.DeviceType;
                                var Notify = _userService.AddNotification(data);

                            }
                        }


                    }
                    #endregion

                    #endregion

                    #region companyadmin Email Send

                    var getUserCompanyName = _userService.Find(Convert.ToString(objUser.Email.ToLower()));

                    if (getUserCompanyName.CompanyID != 0) // for schindler employee
                    {
                        var companyAdminList = _userService.CheckPermissionForMailNotification(getUserCompanyName.CompanyID);

                        foreach (var item in companyAdminList)
                        {
                            string Subject2 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                            string ActivationUrl2 = string.Empty;

                            string headurl2 = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                            ActivationUrl2 = Server.HtmlEncode("" + headurl2 + "/Dashboard/Index");

                            string Sbody2 = null;

                            Dictionary<string, string> dic2 = new Dictionary<string, string>();

                            dic2.Add("PersonName", item.FirstName);

                            dic2.Add("NewRegisteredUser", model.FirstName);

                            //dic2.Add("CompanyName", user.CompanyName);

                            dic2.Add("MessageURL", ActivationUrl2);

                            Sbody2 = StaticUtilities.getEmailBody("User", "NewUserRegisteredCompanyAdmin.html", dic2);

                            SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject2, Sbody2);

                           
                        }

                        #region Push Notification
                        foreach (var item in companyAdminList)
                        {
                          

                            string ActivationUrl2 = string.Empty;

                            string headurl2 = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                            ActivationUrl2 = Server.HtmlEncode("" + headurl2 + "/Dashboard/Index");

                          

                            Dictionary<string, string> dic2 = new Dictionary<string, string>();

                            dic2.Add("PersonName", item.FirstName);

                            dic2.Add("NewRegisteredUser", model.FirstName);

                            //dic2.Add("CompanyName", user.CompanyName);

                            dic2.Add("MessageURL", ActivationUrl2);
                                                       
                           
                            string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                            var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                            int success = 0;
                            bool Isnotify = false;
                            NotificationEntity data = new NotificationEntity();
                            if (IsNotificationEnable.ToUpper() == "YES")
                            {
                                if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                                {
                                    string NotificationSubject = "New User Registered";
                                    string NotificationBody = StaticUtilities.getNotificationBody("User", "NewUserRegisteredCompanyAdmin.txt", dic2);

                                    try
                                    {

                                        if (item.DeviceType.ToUpper() == "ANDROID")
                                        {
                                            success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                            if (success < 1)
                                            {
                                                data.StatusID = 30;
                                            }
                                            else
                                            {
                                                data.StatusID = 29;
                                            }
                                        }
                                        else
                                        {
                                            Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                            if (Isnotify)
                                            {
                                                data.StatusID = 29;
                                            }
                                            else
                                            {
                                                data.StatusID = 30;
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        data.StatusID = 30;
                                    }


                                    //Insert into NotificationLogId table

                                    data.CompanyID = getUserCompanyName.CompanyID;
                                    data.DeviceID = item.DeviceID;
                                    data.SenderID = SENDER_ID.ToString();
                                    data.ReceiverID = Convert.ToString(item.UserID);
                                    data.Subject = NotificationSubject;
                                    data.Message = NotificationBody;
                                    data.DateSent = DateTime.Now;
                                    //data.StatusID = 29;
                                    data.CreatedBy = getUserCompanyName.FirstName + " " + getUserCompanyName.FirstName;
                                    data.DeviceType = item.DeviceType;
                                    var Notify = _userService.AddNotification(data);

                                }
                            }

                           
                        }
                        #endregion
                    }

                    #endregion

                    ViewBag.Message = "Your email account has been verified successfully! Administrator will notify you once your myschindlerprojects account is approved.";
                }
                else
                {
                    ViewBag.Message = "You are register with the system, please contact to adminstrator.";
                }

                TempData["msgdisplay"] = ViewBag.Message;
                return RedirectToAction("UserLogin", "Login", null);

                //return View("VerifyAccount");
            }



        }

        /// <summary>
        /// User account activated based on TokenId
        /// </summary>
        /// <param name="TokenID"></param>
        /// <returns></returns>
        public ActionResult ActiveAccount(string TokenID)
        {
            // clear existing user cookie
            if (User != null)
            {
                if (User.GetType().Module.Name == "SchindlerCustomerPortal.dll")
                {
                    //Fetch the Cookie using its Key.
                    HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                    //Set the Expiry date to past date.
                    nameCookie.Expires = DateTime.Now.AddDays(-1);
                    //Update the Cookie in Browser.
                    Response.Cookies.Add(nameCookie);
                }
            }

            var CurrentTime = DateTime.Now;
            UserEntity TokenExpireDate = _userService.GetTokenExpiration(TokenID);
            if (CurrentTime > TokenExpireDate.TokenExpireDate)
            {
                ViewBag.Message = "Activation link is expired";
                //return RedirectToAction("UserLogin", "Login");
                //return View();

                TempData["msgdisplay"] = ViewBag.Message;
                return RedirectToAction("UserLogin", "Login", null);
            }
            else
            {
                var actionreturn = RedirectToAction("");
                if (TokenID == null || TokenID == "")
                {
                    actionreturn = RedirectToAction("UserLogin", "Login");
                    return actionreturn;
                }
                else
                {
                    ViewBag.TokenID = HttpUtility.HtmlEncode(TokenID);
                    return View();
                }
            }



        }

        /// <summary>
        /// Add New password for User after his/her activation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUserPassword(UserEntity model)
        {
            var token = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(model.Token));

            string saltKey = EncryptionService.CreateSaltKey(5);
            model.PasswordSalt = saltKey;
            model.NewPassword = EncryptionService.CreatePasswordHash(model.Password, saltKey, "");
            model.AccountStatus = Convert.ToInt32(Status.Active);
            var insertUser = _userService.UpdatePwdService(token, model.NewPassword,saltKey, model.AccountStatus);
            
            if (insertUser == true)
            {
                return Json("Success");
            }
            else
            {
                return Json("Invalid");
            }
        }


	}
}