﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Project;
using SchindlerCustomerPortal.Services.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ProjectMasterController : BaseController
    {
        private readonly IProjectMasterService _projectMasterService;

        public ProjectMasterController(IProjectMasterService projectMasterService)
        {
            this._projectMasterService = projectMasterService;
        }

        // GET: /ProjectMaster/
        public ActionResult List()
        { 
            return View("List");
        } 
        [HttpPost]
        public JsonResult GetAllProject(ProjectSearchInfoViewModel obj)
        {
            List<ProjectEntity> lst = new List<ProjectEntity>(); 
               int totalcount = 0;
               var data = _projectMasterService.GetAllProject(obj, ref totalcount);
               if (!string.IsNullOrEmpty(obj.SearchCriteria.SearchString))
               {
                   totalcount = data.Count;
               }
                
               PagedResult<ProjectEntity> resultlst = new PagedResult<ProjectEntity>();
               resultlst.Result = data;
               resultlst.TotalRecordCount = totalcount;
               

               return Json(new { data= resultlst, success = true });
        }
    }
}