﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Project;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.Services.User;
using SchindlerCustomerPortal.Services.Communication;

using System.Globalization;

using SchindlerCustomerPortal.Services.ProjectMilestone;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using Newtonsoft.Json;
using SchindlerCustomerPortal.Models.Authentication;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ManageProjectController : BaseController
    {
        public string strModuleName;


        private readonly IProjectService _projectService;
        private readonly ICommonService _commonService;
        private readonly IProjectMasterService _projectMasterService;
        private readonly IUserService _userService;
        private readonly ICommunicationService _communicationService;
        private readonly IProjectMilestoneService _projectMilestoneService;
        //for Audit Trail
        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;



        public ManageProjectController(IProjectService projectService,
            ICommonService commonService, IProjectMasterService projectMasterService,
            IUserService userService,
            ICommunicationService communicationService, IProjectMilestoneService projectMilestoneService)
        {
            this._projectService = projectService;
            this._commonService = commonService;
            this._projectMasterService = projectMasterService;
            this._userService = userService;
            this._communicationService = communicationService;
            this._projectMilestoneService = projectMilestoneService;
            strModuleName = "Manage Project";

        }

        [Route("ManageProject/ProjectMasterList/{MilestoneStatusValue?}")]
        public ActionResult ProjectMasterList(string MilestoneStatusValue)
        {
            #region "Audit Trail"
            //For Audit Trail
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //Audit Log entry in database
            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.ProjectManagement, "Project Master List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.ProjectManagement_MasterList.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);
            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }
            #endregion
            //To get the Id of Module
            ModuleEntity module = _commonService.ModuleByName(ModuleName.MyProjects);
            ViewBag.ModuleId = module.ModuleId;

            if (string.IsNullOrEmpty(MilestoneStatusValue))
            {
                MilestoneStatusValue = string.Empty;
            }
            ViewBag.MilestoneStatusValue = MilestoneStatusValue;

            TempData["MilestoneStatusValue"] = MilestoneStatusValue;

            return View();
        }


        public ActionResult ProjectMasterView(int id, string comeFrom)
        {

            #region "Audit Trail"
            //For Audit Trail
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //Audit Log entry in database
            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.ProjectManagement, "Project Master View " + comeFrom, Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.ProjectManagement_MasterView.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);
            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }
            #endregion
            //To get the Id of Module
            ModuleEntity module = _commonService.ModuleByName(ModuleName.MyProjects);
            ViewBag.ModuleId = module.ModuleId;

            ViewBag.ProjectNo = id;
            ViewBag.ComeFrom = comeFrom;
            return View();
        }


        public ActionResult ProjectJobMainView(string id, string ProjectNo, string CompanyId, string comeFrom)
        {

            #region "Audit Trail"
            //For Audit Trail
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //Audit Log entry in database
            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.ProjectManagement, "Project Job View", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.ProjectManagement_JobView.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);
            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }
            #endregion

            DataTableModel model = new DataTableModel();
            model.ProjectNo = Convert.ToInt32(ProjectNo);
            model.CompanyID = Convert.ToInt32(CompanyId);
            model.UserID = User.UserId;
            model.SessionID = User.SessionId;
            model.appAuditID = AppAuditID;
            model.ApplicationOperation = "IsUserValid";

            bool flag = _commonService.IsValidUserForProject(model);

            if (flag)
            {
                //To get the Id of Module
                ModuleEntity module = _commonService.ModuleByName(ModuleName.MyProjects);
                ViewBag.ModuleId = module.ModuleId;

                ViewBag.ProjectNo = ProjectNo;
                ViewBag.JobNo = id;
                ViewBag.CompanyId = CompanyId;
                ViewBag.comeFrom = comeFrom;
                return View();
            }
            else
            {
                ViewBag.AppAuditID = AppAuditID;
                ViewBag.SessionID = User.SessionId;
                ViewBag.CompanyID = User.CompanyID;
                return RedirectToAction("UnauthorizedAccess", "Common", null);
            }
        }

        //public ActionResult ProjectBankView(int? id)
        //{
        //    //ViewBag.ProjectNo = id;
        //    return PartialView();
        //}


        // display all contact for perticular project
        [HttpPost]
        public JsonResult GetProjectMasterRecordByProjectID(int ProjectNo)
        {
            int totalcount = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_ViewProjectRecord.ToString();
                model.SessionID = User.SessionId;


                #endregion

                model.ProjectNo = ProjectNo;
                model.CompanyID = User.CompanyID;
                model.UserID = User.UserId;
                model.RoleID = User.RoleId;

                var projectMasterRecord = _projectMasterService.GetProjectMasterRecordByProjectID(model);
                if(projectMasterRecord != null)
                    ViewBag.ProjectName = Convert.ToString(projectMasterRecord.ProjectName);

                return Json(new { data = projectMasterRecord }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
        //check unique document name
        [HttpPost]
        public JsonResult CheckDocumentName(ProjectDocumentEntity checkdata)
        {

            var IsDocname = _projectMasterService.CheckDocumentName(checkdata);
            return Json(IsDocname, JsonRequestBehavior.AllowGet);
        }

        // display all contact for perticular project
        [HttpPost]
        public JsonResult GetAllProjectMasterRecords(string MilestoneStatusValue)
        {
            int totalcount = 0;
            try
            {

                DataTableModel model = new DataTableModel();

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_MasterList.ToString();
                model.SessionID = User.SessionId;


                #endregion

                if (string.IsNullOrEmpty(MilestoneStatusValue))
                    MilestoneStatusValue = string.Empty;

                //model.ProjectID = 1; // static
                model.CompanyID = User.CompanyID;
                model.UserID = User.UserId;
                model.RoleID = User.RoleId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];

                //if (TempData["MilestoneStatusValue"] != null)
                //{
                //    if (string.IsNullOrEmpty(model.SearchTerm))
                //        model.SearchTerm = Convert.ToString(TempData["MilestoneStatusValue"]);
                //}
                //else if(TempData["MilestoneStatusValue"] == null && string.IsNullOrEmpty(model.SearchTerm))
                //{
                //    model.SearchTerm = MilestoneStatusValue;
                //}

                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);


                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var projectMasterList = _projectMasterService.GetAllProjectMasterList(model, ref totalcount);

                //if (!string.IsNullOrEmpty(model.SearchTerm))
                //{
                //    totalcount = projectMasterList.Count;
                //}

                //return Json(new { data = user }, JsonRequestBehavior.AllowGet);
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = projectMasterList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        // display all contact for perticular project
        [HttpPost]
        public JsonResult GetAllProjectJobRecords(int ProjectNo)
        {
            int totalcount = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_MasterJobList.ToString();
                model.SessionID = User.SessionId;


                #endregion


                model.ProjectNo = ProjectNo;
                model.UserID = User.UserId;
                model.RoleID = User.RoleId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);


                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var projectMasterList = _projectMasterService.GetAllProjectJobList(model, ref totalcount);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = projectMasterList.Count;
                }
                //return Json(new { data = user }, JsonRequestBehavior.AllowGet);
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = projectMasterList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }


        [HttpPost]
        public JsonResult UpdateProjectMaster(ProjectMasterEntity projectMaster)
        {
            projectMaster.appAuditID = 123;
            projectMaster.SessionID = User.SessionId;
            projectMaster.UpdatedBy = User.UserId.ToString();

            try
            {
                var user = _projectMasterService.UpdateProjectMaster(projectMaster);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }



        [HttpPost]
        public JsonResult GetProjectJobRecordByJobNo(string JobNo)
        {

            try
            {
                DataTableModel model = new DataTableModel();

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_ViewProjectJobRecord.ToString();
                model.SessionID = User.SessionId;


                #endregion

                model.JobNo = JobNo;
                model.CompanyID = User.CompanyID;
                model.UserID = User.UserId;
                model.RoleID = User.RoleId;

                var projectMasterRecord = _projectMasterService.GetProjectJobRecordByJobNo(model);
                //ViewBag.ProjectName = projectMasterRecord.ProjectName;
                return Json(new { data = projectMasterRecord }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        // GET: /ManageProject/
        public ActionResult ContactList()
        {
            #region "Audit Trail"
            //For Audit Trail
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //Audit Log entry in database
            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.Contacts, "Contact List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.ProjectManagement_ContactList.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);
            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }
            #endregion
            //To get the Id of Module
            ModuleEntity module = _commonService.ModuleByName(ModuleName.Contacts);
            ViewBag.ModuleId = module.ModuleId;

            return View();
        }

        // add new contact to project
        [HttpPost]
        public JsonResult AddProjectContact(ProjectContactEntity model)
        {
            try
            {

                if (string.IsNullOrEmpty(model.JobNo))
                {
                    model.JobNo = string.Empty;
                }


                // model.ProjectID = 1; // static value
                model.CreatedBy = User.UserId.ToString();


                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_AddProjectContact.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;


                #endregion

                var user = _projectService.AddProjectContact(model);

                string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
                string SuperAdmin = System.Configuration.ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();

                #region Email Send

                //var userResult = _userService.Find(model.Email);

                //string Subject = "Schindler - Please verfiy account";

                //string token = (Guid.NewGuid().ToString("N"));

                //int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                //_userService.SetTokenService(token, userResult.UserID, ExpToken);

                //string ActivationUrl = string.Empty;

                //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                //ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                //string Sbody = null;

                //Sbody += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi," + model.FirstName.Trim() + "," + "<br/><br/>";
                //Sbody += "You have successfully registered on Schindler Customer Portal<br/><br/>";
                //Sbody += "Please  <a href=" + ActivationUrl + ">" + ActivationUrl + "</a>  to verify your email<br/><br/>";
                //Sbody += "Sincerely,<br/>The Schindler Support Team</p>";
                //Sbody += "</div></body></html>";

                //SendEmail.Send(SFrom, model.Email, Subject, Sbody);

                #endregion

                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        // display all contact for perticular project
        [HttpPost]
        public JsonResult GetAllContactByProjectID(int ProjectNo)
        {
            int count = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                model.ProjectNo = ProjectNo; // 1; // static

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);


                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_ProjectContactList.ToString();
                model.SessionID = User.SessionId;


                #endregion


                var user = _projectService.GetProjectContactList(model, ref count);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    count = user.Count;
                }
                //return Json(new { data = user }, JsonRequestBehavior.AllowGet);
                return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        // select perticular contact detail
        [HttpGet]
        public JsonResult GetProjectContactByID(int contactID)
        {
            try
            {
                var user = _projectService.ProjectContactSelectById(contactID);

                var designation = _commonService.designationList();

                //int countryId = 1; // for usa
                //var state = _commonService.stateList(countryId);
                //var city = _commonService.cityList(countryId, user.State);

                return Json(new { user = user, designation = designation }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        // update contact detail
        [HttpPost]
        public JsonResult UpdateProjectContact(ProjectContactEntity model)
        {
            if (!string.IsNullOrEmpty(model.JobName))
            {
                if (string.IsNullOrEmpty(model.JobNo))
                {
                    model.JobNo = "-1";
                }
            }
            else
            {
                model.JobNo = string.Empty;
            }


            model.UpdatedBy = User.UserId.ToString();


            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.ProjectManagement_UpdateProjectContact.ToString();
            model.SessionID = User.SessionId;
            model.UserID = User.UserId;

            #endregion

            try
            {
                var user = _projectService.UpdateProjectContact(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        // soft delete from database
        [HttpPost]
        public JsonResult DeleteProjectContact(string contactID)
        {
            ProjectContactEntity model = new ProjectContactEntity();
            model.ContactIDList = contactID;

            model.UpdatedBy = User.UserId.ToString();

            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.ProjectManagement_DeleteProjectContact.ToString();
            model.SessionID = User.SessionId;
            model.UserID = User.UserId;

            #endregion

            try
            {
                var user = _projectService.DeleteProjectContact(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        // select designation and state
        [HttpGet]
        public JsonResult GetDesignationandState()
        {
            try
            {
                var designation = _commonService.designationList();

                int countryId = 1; // for usa
                var state = _commonService.stateList(countryId);

                return Json(new { designation = designation, state = state }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        // select designation and state
        [HttpGet]
        public JsonResult GetCityList(string stateCode)
        {
            try
            {
                int countryId = 1; // for usa
                var city = _commonService.cityList(countryId, stateCode);

                return Json(city, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }


        //Doc Type

        [HttpPost]
        public JsonResult GetProjectDocumentList(DataTableModel model)
        {

            try
            {
                int totalcount = 0;
                List<ProjectDocumentEntity> projectDocuments = new List<ProjectDocumentEntity>();

                //model.CompanyID = User.CompanyID;

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_ProjectDocumentList.ToString() + "-" + model.DocumentType;
                model.SessionID = User.SessionId;


                #endregion

                model.UserID = User.UserId;
                model.RoleID = User.RoleId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);
                if (String.IsNullOrEmpty(model.JobNo)) { model.JobNo = String.Empty; }
                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                //if (model.CompanyID == 0)
                //{
                projectDocuments = _projectMasterService.GetProjectDocumentList(model, ref totalcount);
                //}
                //else
                //{
                //    projectDocuments = _projectMasterService.GetProjectDocumentList(model, ref totalcount);
                //}

                //totalcount = projectDocuments.Count;

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = projectDocuments.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = projectDocuments }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public JsonResult AddProjectDocument(ProjectDocumentEntity projectDocument)
        {
            //string Docuploadpath = System.Configuration.ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = projectDocument.FileName;
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            try
            {
                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);
                    //string[] words = filename.Split('.');

                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);
                    System.IO.File.Copy(sourceFile, destFile, true);
                }
                if (string.IsNullOrEmpty(projectDocument.CompanyName))
                {
                    projectDocument.CompanyName = "NONE";
                }
                projectDocument.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                string fileExtension = System.IO.Path.GetExtension(docpath);
                projectDocument.FileType = fileExtension;
                projectDocument.PublishedBy = User.FirstName + " " + User.LastName;


                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                projectDocument.appAuditID = appAuditID;
                projectDocument.ApplicationOperation = ApplicationOperation.ProjectManagement_AddProjectDocument.ToString() + "-" + projectDocument.DocumentType;
                projectDocument.SessionID = User.SessionId;
                projectDocument.UserId = User.UserId;

                #endregion

                var documentdetails = _projectMasterService.AddProjectDocument(projectDocument);

                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpPost]
        public JsonResult UpdateProjectDocument(ProjectDocumentEntity projectDocument)
        {

            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = projectDocument.FileName;
            try
            {

                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);
                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);

                    System.IO.File.Copy(sourceFile, destFile, true);

                    projectDocument.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                    string fileExtension = System.IO.Path.GetExtension(projectDocument.DocumentPath);
                    projectDocument.FileType = fileExtension;
                }
                if (string.IsNullOrEmpty(projectDocument.CompanyName))
                {
                    projectDocument.CompanyName = "NONE";
                }
                projectDocument.UpdatedBy = User.FirstName + " " + User.LastName;

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                projectDocument.appAuditID = appAuditID;
                projectDocument.ApplicationOperation = ApplicationOperation.ProjectManagement_UpdateProjectDocument.ToString() + "-" + projectDocument.DocumentType;
                projectDocument.SessionID = User.SessionId;
                projectDocument.UserId = User.UserId;
                #endregion

                //if (!string.IsNullOrEmpty(Convert.ToString(projectDocument.ExpiredDate)))
                //{
                //    DateTime dt = DateTime.ParseExact(projectDocument.ExpiredDate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                //    projectDocument.ExpiredDate = dt;
                //}


                var documentdetails = _projectMasterService.UpdateProjectDocument(projectDocument);
                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpPost]
        public JsonResult DeleteProjectDocument(int DocID)
        {
            ProjectDocumentEntity projectDocument = new ProjectDocumentEntity();
            //model.UserID = userID;
            projectDocument.DocumentID = DocID;
            projectDocument.DeletedBy = User.UserId.ToString();



            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            projectDocument.appAuditID = appAuditID;
            projectDocument.ApplicationOperation = ApplicationOperation.ProjectManagement_DeleteProjectDocument.ToString() + "-" + projectDocument.DocumentType;
            projectDocument.SessionID = User.SessionId;
            projectDocument.UserId = User.UserId;
            #endregion
            try
            {
                var user = _projectMasterService.DeleteProjectDocument(projectDocument);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        [HttpGet]
        public JsonResult GetProjectDocumentByID(int docId)
        {
            try
            {
                var documentdetails = _projectMasterService.GetProjectDocumentByID(docId);
                return Json(documentdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }


        //Doc Type

        [HttpPost]
        public JsonResult JobNameAutoCompleteByProjectNo(string searchTerm, int ProjectNo)
        {
            int companyId = User.CompanyID;

            if (searchTerm.Contains(","))
            {
                string result = searchTerm.Split(',')[searchTerm.Split(',').Length - 1];
                if (!string.IsNullOrEmpty(result))
                {
                    searchTerm = result.Trim();
                }
            }

            try
            {
                var jobList = _projectService.GetAllJobByProjectNoandSearchTerm(ProjectNo, searchTerm);
                return Json(jobList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        [HttpPost]
        public JsonResult ProjectNameAutoCompleteByCompanyId(string searchTerm)
        {
            int companyId = User.CompanyID; //1; // User.CompanyID;
            int RoleId = User.RoleId;

            if (searchTerm.Contains(","))
            {
                string result = searchTerm.Split(',')[searchTerm.Split(',').Length - 1];
                if (!string.IsNullOrEmpty(result))
                {
                    searchTerm = result.Trim();
                }
            }

            try
            {
                var projectList = _projectService.GetAllProjectByCompanyIdandSearchTerm(companyId, searchTerm, RoleId, User.UserId);
                return Json(projectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        [HttpPost]
        public JsonResult GetAllContactListByCompanyId()
        {
            int count = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                model.CompanyID = User.CompanyID;  // static // for common contact list
                //model.FlagValue = 1; // for common contact list

                model.RoleID = User.RoleId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);


                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var user = _projectService.GetProjectContactListByCompanyId(model, ref count);

                //if (!string.IsNullOrEmpty(model.SearchTerm))
                //{
                //    count = user.Count;
                //}
                //return Json(new { data = user }, JsonRequestBehavior.AllowGet);
                return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        [HttpPost]
        public JsonResult AddProjectCommunication(CommunicationEntity model)
        {
            try
            {
                string ActivationUrl = string.Empty;
                string ProjectNoProjectNameBankNoBankDesc = string.Empty;

                //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                if (string.IsNullOrEmpty(model.JobNo) || model.JobNo == "undefined")
                {
                    model.JobNo = string.Empty;

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/ManageProject/ProjectMasterView/" + model.ProjectNo + "/Messages");
                    ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName;
                }
                else
                {
                    //ActivationUrl = Server.HtmlEncode("" + headurl + "/ManageProject/ProjectJobMainView/" + model.JobNo + "/" + model.JobNo + "/" + model.CompanyId + "/Messages");
                    ActivationUrl = Server.HtmlEncode("" + headurl + "/ManageProject/ProjectJobMainView/" + model.JobNo + "/" + model.ProjectNo + "/" + model.CompanyId + "/Messages");
                    //ProjectNoProjectNameBankNoBankDesc = "Project # " + model.ProjectNo + " - " + model.ProjectName + " and Bank " + model.JobName + " [" + model.JobNo + "]";
                    ProjectNoProjectNameBankNoBankDesc = "Bank " + model.JobName + " [" + model.JobNo + "]" + " of Project # " + model.ProjectNo + " - " + model.ProjectName;
                }

                if (string.IsNullOrEmpty(model.NotifyToEmail))
                    model.NotifyToEmail = string.Empty;

                if (string.IsNullOrEmpty(model.AttachmentPath))
                    model.AttachmentPath = string.Empty;

                model.UserId = User.UserId;
                model.CreatedBy = Convert.ToString(User.UserId);



                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_AddCommunication.ToString();
                model.SessionID = User.SessionId;


                #endregion

                var communication = _communicationService.AddProjectCommunication(model);

                if (!string.IsNullOrEmpty(model.NotifyToEmail))
                {
                    foreach (var item in model.NotifyToEmail.Split(','))
                    {
                        UserEntity userResult = _userService.Find(item);

                        #region Email Send

                        string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
                        string Subject = "myschindlerprojects - notification";

                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", userResult.FirstName);
                        dic.Add("SenderName", User.FirstName);

                        dic.Add("MessageURL", ActivationUrl);
                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        Sbody = StaticUtilities.getEmailBody("User", "ProjectJobCommunication.html", dic);

                        //SendEmail.SendMailtoMultiple(SFrom, item.Receiver, model.CC, model.BCC, Subject, Sbody);

                        SendEmail.Send(CommonConfiguration.NoReplyEmail, item, Subject, Sbody);

                        #endregion

                        #region Push Notification
                        string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(userResult.DeviceID) && !string.IsNullOrEmpty(userResult.DeviceType))
                            {
                                string NotificationSubject = "New Email Notification";
                                string NotificationBody = StaticUtilities.getNotificationBody("User", "EmailNotificationNew.txt", dic);

                                try
                                {
                                    if (userResult.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(userResult.DeviceID, NotificationSubject, NotificationBody);
                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }

                                    }
                                    else
                                    {
                                        Isnotify = SendNotification.NotifyIOSDevice(userResult.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }


                                //Insert into NotificationLogId table

                                data.CompanyID = User.CompanyID;
                                data.DeviceID = userResult.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(userResult.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                // data.StatusID = 29;
                                data.CreatedBy = User.FirstName + " " + User.LastName;
                                data.DeviceType = userResult.DeviceType;
                                var Notify = _userService.AddNotification(data);

                            }
                        }

                        #endregion

                    }
                }

                return Json(communication, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        [HttpPost]
        public JsonResult GetAllCommunicationList(int ProjectNo, string JobNo)
        {



            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            #endregion
            if (string.IsNullOrEmpty(JobNo))
                JobNo = string.Empty;
            var communication = _communicationService.GetAllCommunication(ProjectNo, JobNo, appAuditID, ApplicationOperation.ProjectManagement_CommunicationList.ToString(), User.SessionId, User.UserId);
            return Json(communication, JsonRequestBehavior.AllowGet);
        }

        public ActionResult JobMasterView(string Id)
        {
            ViewBag.JobNo = Id;
            return View();
        }

        [HttpPost]
        public JsonResult GetProjectChart(DataTableModel model)
        {
            model.UserID = User.UserId;
            model.RoleID = User.RoleId;
            model.CompanyID = User.CompanyID;

            if (model.ProjectNumber == null) { model.ProjectNumber = ""; }
            if (model.JobNo == null) { model.JobNo = ""; }

            var chart = _projectMasterService.GetProjectChart(model);
            List<ProjectChartEntity> chartData = new List<ProjectChartEntity>();

            chartData.Add(new ProjectChartEntity { StatusName = "StatusName", ProjectCount = "ProjectCount" });
            foreach (var item in chart.lstProjectChartEntity)
            {
                chartData.Add(new ProjectChartEntity { StatusName = item.StatusName, ProjectCount = item.ProjectCount }
                    );
            }


            return Json(new { projectChart = chartData, stackedChart = chart.lstProjectChartMilestone, ProjectCount = chart.lstProjectCount }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public List<object> GetChartDataDashboard(DataTableModel model)
        {

            int userid = User.UserId;
            int roleId = User.RoleId;
            int companyId = User.CompanyID;

            var chart = _projectMasterService.GetProjectChart(model);

            List<object> chartData = new List<object>();
            chartData.Add(new object[]
            {
                    "StatusName", "ProjectCount"
            });

            //foreach (var item in chart)
            //{
            //    chartData.Add(new object[]
            //        {
            //            item.StatusName, item.ProjectCount
            //        });
            //}

            return chartData;
        }

        public ActionResult _ReadyToPullTemplate(string ProjectNo, string JobNo)
        {
            DataTableModel model = new DataTableModel();
            model.UserID = User.UserId;
            model.RoleID = User.RoleId;
            model.CompanyID = User.CompanyID;
            model.JobNo = JobNo;
            model.ProjectNumber = ProjectNo;


            var data = _projectMilestoneService.GetAllReadyToPullTemplateMasterList(model);

            return PartialView(data);
        }

        //public JsonResult SaveReadyToPullTemplateData(List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails)
        [HttpPost]
        public JsonResult SaveReadyToPullTemplateData(FormCollection obj)
        {
            ProjectMilestoneEntity projectMilestoneEntity = new ProjectMilestoneEntity();

            List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails = new List<ReadyToPullTemplateDetails>();
            int ProjectNo = 0;
            string JobNo = "";

            projectMilestoneEntity.Note = Convert.ToString(System.Web.HttpContext.Current.Request.Form["Note"]);
            projectMilestoneEntity.MilestoneName = Convert.ToString(System.Web.HttpContext.Current.Request.Form["MilestoneName"]);
            projectMilestoneEntity.Status = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["Status"]);
            projectMilestoneEntity.StatusName = Convert.ToString(System.Web.HttpContext.Current.Request.Form["StatusName"]);


            if (System.Web.HttpContext.Current.Request.Form["DueDate"] == null || System.Web.HttpContext.Current.Request.Form["DueDate"] == "")
            {
                projectMilestoneEntity.DueDate = null;
            }
            else
            {
                projectMilestoneEntity.DueDate = Convert.ToDateTime(System.Web.HttpContext.Current.Request.Form["DueDate"]);
            }


            if (System.Web.HttpContext.Current.Request.Form["CompletedDate"] == "" || System.Web.HttpContext.Current.Request.Form["CompletedDate"] == "")
            {
                projectMilestoneEntity.CompletedDate = null;
            }
            else { 
                projectMilestoneEntity.CompletedDate = Convert.ToDateTime(System.Web.HttpContext.Current.Request.Form["CompletedDate"]);
            }
            projectMilestoneEntity.PreviousStatusName = Convert.ToString(System.Web.HttpContext.Current.Request.Form["PreviousStatusName"]);
            projectMilestoneEntity.ProjectNo = Convert.ToString(System.Web.HttpContext.Current.Request.Form["ProjectNo"]);
            projectMilestoneEntity.JobNo= Convert.ToString(System.Web.HttpContext.Current.Request.Form["JobNo"]);

            AddProjectBankMilestoneCommentandEmail(projectMilestoneEntity);

            if (System.Web.HttpContext.Current.Request.Form.AllKeys.Length > 0)
            {
                foreach (string key in System.Web.HttpContext.Current.Request.Form.AllKeys)
                {
                    switch (key)
                    {
                        case "lstReadyToPullTemplateDetails":
                            {
                                var xyz = System.Web.HttpContext.Current.Request.Form[key];
                                lstReadyToPullTemplateDetails = JsonConvert.DeserializeObject<List<ReadyToPullTemplateDetails>>(xyz);
                                break;
                            }
                        case "ProjectNo":
                            {
                                ProjectNo = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form[key]);
                                break;
                            }
                        case "JobNo":
                            {
                                JobNo = System.Web.HttpContext.Current.Request.Form[key];
                                break;
                            }
                        default:
                            {
                                int TempId = 0;

                                //if (key.Contains("textarea_") == true)
                                //{
                                //    TempId = Convert.ToInt32(key.Replace("textarea_", ""));
                                //}

                                //ReadyToPullTemplateDetails readyToPull = new ReadyToPullTemplateDetails();
                                //readyToPull.TemplateId = TempId;
                                //readyToPull.ProjectNo = ProjectNo;
                                //readyToPull.JobNo = JobNo;
                                //readyToPull.Comment = System.Web.HttpContext.Current.Request.Form[key];
                                //readyToPull.CreatedBy = User.UserId.ToString();
                                //readyToPull.CreatedDate = DateTime.Now.ToString("M/d/yyyy"); 
                                //readyToPull.appAuditID = 123;
                                //readyToPull.SessionID = User.SessionId;
                                //if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                                //{

                                //    if (System.Web.HttpContext.Current.Request.Files.Count > 0)
                                //    {

                                //        foreach (string attachment in System.Web.HttpContext.Current.Request.Files)
                                //        {
                                //            if (attachment.IndexOf('~')>0)
                                //            {

                                //                string[] strFileinfo = attachment.Split('~');
                                //                string originalFileName=strFileinfo[1];
                                //                string newfilename = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + strFileinfo[1];

                                //                if ("filename_" + TempId.ToString().Trim() ==strFileinfo[0])
                                //                {
                                //                    HttpPostedFileBase file = Request.Files[attachment];
                                //                    var comPath = CommonConfiguration.readyToPullAttachmentsPath + newfilename;
                                //                    var imgSave = Server.MapPath(comPath);
                                //                    file.SaveAs(imgSave);

                                //                    readyToPull.Attachment = newfilename;
                                //                    readyToPull.RealAttachment = originalFileName;
                                //                }
                                //            }

                                //        }

                                //    }
                                //}

                                //lstReadyToPullTemplateDetails.Add(readyToPull);

                                break;
                            }


                    }
                }

            }

            if (lstReadyToPullTemplateDetails.Count > 0)
            {

                foreach (ReadyToPullTemplateDetails rd in lstReadyToPullTemplateDetails)
                {
                    rd.CreatedBy = User.UserId.ToString();
                    rd.CreatedDate = DateTime.Now.ToString("M/d/yyyy");


                    #region "Audit Trail"
                    int appAuditID = 0;
                    if (Request.Cookies["AppAuditCookie"] != null)
                    {
                        appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                    }
                    rd.appAuditID = appAuditID;
                    rd.ApplicationOperation = ApplicationOperation.ProjectManagement_SaveReadyToPullMilestone.ToString();
                    rd.SessionID = User.SessionId;


                    #endregion


                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {

                        if (System.Web.HttpContext.Current.Request.Files.Count > 0)
                        {

                            foreach (string attachment in System.Web.HttpContext.Current.Request.Files)
                            {
                                if (attachment.IndexOf('~') > 0)
                                {

                                    string[] strFileinfo = attachment.Split('~');
                                    string originalFileName = strFileinfo[1];
                                    string newfilename = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + strFileinfo[1];

                                    if ("filename_" + rd.TemplateId.ToString().Trim() == strFileinfo[0])
                                    {
                                        HttpPostedFileBase file = Request.Files[attachment];
                                        var comPath = CommonConfiguration.readyToPullAttachmentsPath + newfilename;
                                        var imgSave = Server.MapPath(comPath);
                                        file.SaveAs(imgSave);

                                        rd.Attachment = newfilename;
                                        rd.RealAttachment = originalFileName;
                                    }
                                }

                            }

                        }
                    }

                    if (string.IsNullOrEmpty(rd.RealAttachment))
                    {

                        rd.RealAttachment = rd.AlreadyRealAttachment;
                    }
                }

            }





            //model.CreatedBy = User.UserId.ToString();
            //model.appAuditID = 123;
            //model.SessionID = User.SessionId;

            var readyToPullDetails = _projectMilestoneService.AddReadyToPullTemplateDetails(lstReadyToPullTemplateDetails, ProjectNo, JobNo);

            return Json(CommonMessage.Success.ToString());
        }

        [HttpPost]
        public JsonResult AlertForProjectManagement(string moduleName)
        {
            var messageList = _commonService.AlertMessgaeByModule(moduleName);
            return Json(messageList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DashboardProjectListingMilestone(DataTableModel model)
        {
            model.UserID = User.UserId;
            model.RoleID = User.RoleId;

            if (string.IsNullOrEmpty(model.SearchTerm))
            {
                model.SearchTerm = string.Empty;
            }   

            var projectResult = _projectMasterService.DashboardProjectListingMilestone(model);

            return Json(projectResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model)
        {
            var projectResult = _projectMasterService.GetDueDateAndStatusOfMilestone(model);

            return Json(projectResult, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AddProjectBankMilestoneComment(string comment, string projectBankTitle, string status, string duedate)
        {
            if (string.IsNullOrEmpty(comment))
            {
                comment = string.Empty;
            }
            int userId = User.UserId;
            var result = _projectMasterService.AddProjectBankMilestoneCommentDueDate(comment, projectBankTitle, userId, status, duedate,"");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddProjectBankMilestoneCommentandEmail(ProjectMilestoneEntity milestoneobj)
        {
            if (string.IsNullOrEmpty(milestoneobj.Note))
            {
                milestoneobj.Note = string.Empty;
            }

            string Value = _projectMilestoneService.GetProjectandJobDetail(milestoneobj.ProjectNo, milestoneobj.JobNo);

            milestoneobj.ProjectName = Value.Split('|')[0].ToString().Trim();

            milestoneobj.JobDesc = string.IsNullOrEmpty(milestoneobj.JobNo) ? "" : Value.Split('|')[1].ToString().Trim();

            int userId = User.UserId;
            var result = _projectMasterService.AddProjectBankMilestoneCommentDueDate(milestoneobj.Note, milestoneobj.MilestoneName, userId, milestoneobj.Status.ToString(), milestoneobj.DueDate.ToString(), milestoneobj.CompletedDate.ToString());

            milestoneobj.MilestoneName = milestoneobj.MilestoneName.Split('_')[2].ToString().Trim();

            #region Email Send

            var mailNotificationUserList = _projectMilestoneService.GetProjectAssignedUserListForMilestoneEmail(milestoneobj.ProjectNo);

            if (milestoneobj.StatusName.ToUpper() != milestoneobj.PreviousStatusName.ToUpper())
            {
                foreach (var item in mailNotificationUserList)
                {

                    string Subject = "myschindlerprojects – [[ProjectNoProjectNameBankNoBankDesc]] status has been updated to [[StatusName]].";
                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"] + "/ManageProject/ProjectJobMainView/" +
                                     milestoneobj.JobNo + "/" + milestoneobj.ProjectNo + "/" + User.CompanyID + "/Milestone ";
                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                    dic.Add("PersonName", item.FirstName);
                    dic.Add("MilestoneName", milestoneobj.MilestoneName);
                    dic.Add("ProjectNo", milestoneobj.ProjectNo + (milestoneobj.JobNo != null ? " - " + milestoneobj.JobNo : ""));
                    dic.Add("StatusName", milestoneobj.StatusName);
                    dic.Add("PreviousStatusName", milestoneobj.PreviousStatusName);
                    dic.Add("UserName", User.FirstName);
                    string ProjectNoProjectNameBankNoBankDesc = "";
                    if (milestoneobj.JobNo != null)
                    {
                        ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName + " and Bank " + milestoneobj.JobDesc + " [" + milestoneobj.JobNo + "]";
                    }
                    else
                    {
                        ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName;
                    }

                    dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                    Subject = Subject.Replace("[[ProjectNoProjectNameBankNoBankDesc]]", ProjectNoProjectNameBankNoBankDesc).Replace("[[StatusName]]", milestoneobj.StatusName);

                    dic.Add("MessageURL", headurl);

                    Sbody = StaticUtilities.getEmailBody("ProjectMilestone", "MilestoneStatusChangeNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject, Sbody);

                }

                #region Push Notification
                foreach (var item in mailNotificationUserList)
                {


                    string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    //string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];


                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                    dic.Add("PersonName", item.FirstName);
                    dic.Add("MilestoneName", milestoneobj.MilestoneName);
                    dic.Add("ProjectNo", milestoneobj.ProjectNo + (milestoneobj.JobNo != null ? " - " + milestoneobj.JobNo : ""));
                    dic.Add("StatusName", milestoneobj.StatusName);
                    dic.Add("PreviousStatusName", milestoneobj.PreviousStatusName);
                    dic.Add("UserName", User.FirstName);
                    string ProjectNoProjectNameBankNoBankDesc = "";
                    if (milestoneobj.JobNo != null)
                    {
                        ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName + " and Bank " + milestoneobj.JobDesc + " [" + milestoneobj.JobNo + "]";
                    }
                    else
                    {
                        ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName;
                    }

                    dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                    dic.Add("MessageURL", headurl);

                    string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                    var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                    int success = 0;
                    bool Isnotify = false;
                    NotificationEntity data = new NotificationEntity();
                    if (IsNotificationEnable.ToUpper() == "YES")
                    {
                        if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                        {
                            string NotificationSubject = "Milestone Status Changed";
                            string NotificationBody = StaticUtilities.getNotificationBody("ProjectMilestone", "MilestoneStatusChangeNew.txt", dic);

                            try
                            {
                                if (item.DeviceType.ToUpper() == "ANDROID")
                                {
                                    success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                    if (success < 1)
                                    {
                                        data.StatusID = 30;
                                    }
                                    else
                                    {
                                        data.StatusID = 29;
                                    }
                                }
                                else
                                {

                                    Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                    if (Isnotify)
                                    {
                                        data.StatusID = 29;
                                    }
                                    else
                                    {
                                        data.StatusID = 30;
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                data.StatusID = 30;
                            }

                            //Insert into NotificationLogId table

                            data.CompanyID = User.CompanyID;
                            data.DeviceID = item.DeviceID;
                            data.SenderID = SENDER_ID.ToString();
                            data.ReceiverID = Convert.ToString(item.UserID);
                            data.Subject = NotificationSubject;
                            data.Message = NotificationBody;
                            data.DateSent = DateTime.Now;
                            data.CreatedBy = User.FirstName + " " + User.LastName;
                            data.DeviceType = item.DeviceType;
                            var Notify = _userService.AddNotification(data);

                        }
                    }


                }
                #endregion
            }

            #endregion


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllJobListWithParameter(string ProjectNumber, string JobNo,
                 string SearchTerm, int PageNumber, int PageSize)
        {
            DataTableModel dtm = new DataTableModel();
            int totalcount = 0;
            int projectCompanyId = 0;
            dtm.SearchTerm = SearchTerm;
            if (string.IsNullOrEmpty(SearchTerm))
            {
                dtm.SearchTerm = string.Empty;
            }
            dtm.PageNumber = PageNumber;
            dtm.PageSize = PageSize;
            dtm.CompanyID = User.CompanyID;
            dtm.UserID = User.UserId;
            dtm.RoleID = User.RoleId;
            dtm.ProjectNumber = ProjectNumber;

            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            dtm.appAuditID = appAuditID;
            dtm.ApplicationOperation = ApplicationOperation.ProjectManagement_ViewProjectRecord.ToString();
            dtm.SessionID = User.SessionId;

            #endregion


            var projectResult = _projectMasterService.GetAllBankListWithDetail(dtm,ref totalcount ,ref projectCompanyId);
            string[] columnNames = projectResult.Columns.Cast<DataColumn>()
                     .Select(x => x.ColumnName)
                     .ToArray();
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(projectResult);
            return Json(new {tbl= output,col= JsonConvert.SerializeObject(columnNames), totalCount= totalcount, projectCompanyId= projectCompanyId }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadDocumentLink(string path)
        {
            byte[] fileBytes;
            path = System.Web.HttpUtility.UrlDecode(path);
            var comPath =  path;
            try
            {
                if (System.IO.File.Exists(Server.MapPath(comPath)))
                {
                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(comPath));
                    string fileName = Path.GetFileName(Server.MapPath(comPath));
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                fileBytes = null;
                GC.Collect();
            }
        }

        [HttpPost]
        public JsonResult GetPaymentDocumentContactData(DataTableModel model)
        {
            model.ApplicationOperation = "GetDashboardPieDocInvoiceAndContactInfo";
            model.UserID = User.UserId;
            model.RoleID = User.RoleId;
            model.CompanyID = User.CompanyID;

            var projectResult = _projectMasterService.DashboardPaymentDocAndContactData(model);
            return Json(projectResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update_BankDesc_ByJobNo(string Description, string SessionID, string JobNo)
        {

            #region "Audit Trail"
            string UpdatedBy = User.UserId.ToString();
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            string applicationOperation = ApplicationOperation.ProjectManagement_UpdateProjectDocument.ToString();
            #endregion
            if (string.IsNullOrEmpty(JobNo))
                JobNo = string.Empty;
            var data = _projectMasterService.Update_BankDescByJobNo(Description,UpdatedBy,  appAuditID,  SessionID,  JobNo, applicationOperation);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEmailAndPhoneData(DataTableModel model)
        {
            model.ApplicationOperation = "EditEmailAndPhone";
            model.UserID = User.UserId;
            model.CompanyID = model.CompanyID;
            model.appAuditID = AppAuditID;
            model.SessionID = User.SessionId;

            var projectResult = _projectMasterService.editEmailAndPhoneData(model);
            return Json(projectResult, JsonRequestBehavior.AllowGet);
        }

    }
}