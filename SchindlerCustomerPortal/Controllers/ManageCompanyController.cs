﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.Company;
using System.Web.Mvc;
using System.Linq;
using System.IO;
using System.Drawing;
using System;
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.User;
using SchindlerCustomerPortal.Services.Common;
using System.Net;
using System.Web;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ManageCompanyController : BaseController
    {
        private readonly ICompanyService _iCompanyService;
        private readonly IUserService _userService;
        private readonly ICommonService _iCommonService;

        public ManageCompanyController(ICompanyService iCompanyService,
            IUserService userService, ICommonService iCommonService)
        {
            this._iCompanyService = iCompanyService;
            this._userService = userService;
            this._iCommonService = iCommonService;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// return list of companies
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            int userId = User.UserId;
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            var manageAccount = new UserPermissionViewModel();
            manageAccount = _userService.CheckUserModulePermission(userId, ModuleName.AccountManagement); // check account management permission

            if (manageAccount == null)
            {
                manageAccount = new UserPermissionViewModel();
            }

            var permission = new UserPermissionViewModel();

            if (manageAccount.HasViewPermission)
            {
                permission = _userService.CheckUserModulePermission(userId, ModuleName.UserManagement); // check user management permission

                if (permission == null)
                {
                    permission = new UserPermissionViewModel();
                }

                ViewBag.CheckPermission = permission.HasViewPermission;
            }
            else
            {
                return RedirectToAction("UnauthorizedAccess", "Common", null);
            }

            //To get the Id of Module
            ModuleEntity module = _iCommonService.ModuleByName(ModuleName.AccountManagement);
            ViewBag.ModuleId = module.ModuleId;

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.AccountManagement, "Account Management List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.AccountManagement_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View(permission);
        }

        /// <summary>
        /// get all companies records from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCompanyAll(SearchInfoCompanyViewModel model)
        {
            int companyId = User.CompanyID;

            model.UserID = User.UserId;
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.AccountManagement_List.ToString();
            model.SessionID = User.SessionId;

            if (companyId == 0)
            {
                int totalcount = 0;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var Companies = _iCompanyService.GetAllCompany(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //totalcount = Companies.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Companies }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var Companies = _iCompanyService.GetCompanyByCustomer(companyId);

                return Json(new { draw = 1, recordsFiltered = 1, recordsTotal = 1, data = Companies }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// change company logo
        /// </summary>
        [HttpPost]
        public void UploadCompanyLogo()
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedFile"];
                if (httpPostedFile != null)
                {
                    // Get the complete file path 
                    var fileSavePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/images/CompanyLogo/"), httpPostedFile.FileName);
                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    //if (httpPostedFile.FileName.StartsWith("BUSINESSCARD"))
                    //{
                    Image image = Image.FromFile(fileSavePath);
                    Size thumbnailSize = SchindlerCustomerPortal.Common.Utility.GetThumbnailSize(image);
                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);
                    var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/images/CompanyLogo/"), "Thumb_" + httpPostedFile.FileName);
                    // Save thumbnail.
                    thumbnail.Save(thumbpath);
                }
            }
        }

        /// <summary>
        /// add new company record to table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertCompany(CompanyEntity model)
        {

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(logo.FileName);
                    string comPath = Server.MapPath(CommonConfiguration.companyLogoPath) + "" + fileName;
                    logo.SaveAs(comPath);
                    //user.UserProfilePic = ImageName;
                }
            }

            var exist = string.Empty;
            // AppAuditID = Convert.ToInt32(Session["appAuditID"]);
            model.appAuditID = 1;
            model.SessionID = User.SessionId;
            model.CreatedBy = User.UserId.ToString();
            model.CreatedDate = DateTime.UtcNow;

            var Result = _iCompanyService.AddCompany(model);

            if (Result == 3)
            {
                return Json(new { isexist = true, success = true, message = CommonMessage.AlreadyExistsCompany.ToString() });

            }
            else if (Result == 4)
            {
                return Json(new { success = true, message = CommonMessage.CompanyAccountNumberDuplicate.ToString() });
            }
            else if (Result != 3 && Result >= 1)
            {
                //Result == Last Inserted Company ID
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Assets/" + Result));

                return Json(new { success = true, message = CommonMessage.AddedCompany.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }

        }
        /// <summary>
        /// update comapny record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateCompany(CompanyEntity model)
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(logo.FileName);
                    string comPath = Server.MapPath(CommonConfiguration.companyLogoPath) + fileName;
                    logo.SaveAs(comPath);
                    model.CompanyLogo = fileName;
                    model.CompanyLogoFileName = fileName;
                }
                else
                {
                    model.CompanyLogoFileName = "-1";
                }
            }
            else
            {
                model.CompanyLogoFileName = "-1";
            }

            if (model.IsDeleted == true)
            {
                model.CompanyLogo = "12.png";
                model.CompanyLogoFileName = "12.png";
            }

            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.AccountManagement_Update.ToString();
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();
            model.UpdatedDate = DateTime.UtcNow;

            if (string.IsNullOrEmpty(model.HeadOfficeAddress1) || model.HeadOfficeAddress1 == "null")
                model.HeadOfficeAddress1 = string.Empty;
            if (string.IsNullOrEmpty(model.HeadOfficeAddress2) || model.HeadOfficeAddress2 == "null")
                model.HeadOfficeAddress2 = string.Empty;
            if (string.IsNullOrEmpty(model.HeadOfficeStateCode) || model.HeadOfficeStateCode == "null")
                model.HeadOfficeStateCode = string.Empty;
            if (string.IsNullOrEmpty(model.HeadOfficeCityCode) || model.HeadOfficeCityCode == "null")
                model.HeadOfficeCityCode = string.Empty;
            if (string.IsNullOrEmpty(model.HeadOfficeZipCode) || model.HeadOfficeZipCode == "null")
                model.HeadOfficeZipCode = string.Empty;

            if (string.IsNullOrEmpty(model.BillingAddress1) || model.BillingAddress1 == "null")
                model.BillingAddress1 = string.Empty;
            if (string.IsNullOrEmpty(model.BillingAddress2) || model.BillingAddress2 == "null")
                model.BillingAddress2 = string.Empty;
            if (string.IsNullOrEmpty(model.BillingStateCode) || model.BillingStateCode == "null")
                model.BillingStateCode = string.Empty;
            if (string.IsNullOrEmpty(model.BillingCityCode) || model.BillingCityCode == "null")
                model.BillingCityCode = string.Empty;
            if (string.IsNullOrEmpty(model.BillingZipCode) || model.BillingZipCode == "null")
                model.BillingZipCode = string.Empty;

            if (string.IsNullOrEmpty(model.InchargeFirstName) || model.InchargeFirstName == "null")
                model.InchargeFirstName = string.Empty;
            if (string.IsNullOrEmpty(model.InchargeLastName) || model.InchargeLastName == "null")
                model.InchargeLastName = string.Empty;
            if (string.IsNullOrEmpty(model.InchargeEmail) || model.InchargeEmail == "null")
               model.InchargeEmail = string.Empty;
            if (string.IsNullOrEmpty(model.InchargeDesignation) || model.InchargeDesignation == "null")
                model.InchargeDesignation = string.Empty;
            if (string.IsNullOrEmpty(model.InchargePhone) || model.InchargePhone == "null")
                model.InchargePhone = string.Empty;
            if (string.IsNullOrEmpty(model.ContactPersonFirstName) || model.ContactPersonFirstName == "null")
                model.ContactPersonFirstName = string.Empty;
            if (string.IsNullOrEmpty(model.ContactPersonLastName) || model.ContactPersonLastName == "null")
                model.ContactPersonLastName = string.Empty;

            if (string.IsNullOrEmpty(model.CompanyLogoFileName) || model.CompanyLogoFileName == "null")
                model.CompanyLogoFileName = string.Empty;
            if (string.IsNullOrEmpty(model.CCEmailAddress) || model.CCEmailAddress == "null")
                model.CCEmailAddress = string.Empty;
            if (string.IsNullOrEmpty(model.BillingPhoneNumber) || model.BillingPhoneNumber == "null")
                model.BillingPhoneNumber = string.Empty;
            if (string.IsNullOrEmpty(model.HeadOfficePhoneNumber) || model.HeadOfficePhoneNumber == "null")
                model.HeadOfficePhoneNumber = string.Empty;
            if (string.IsNullOrEmpty(model.ACCTManagerEmail) || model.ACCTManagerEmail == "null")
                model.ACCTManagerEmail = string.Empty;
            if (string.IsNullOrEmpty(model.ACCTManagerPhone) || model.ACCTManagerPhone == "null")
                model.ACCTManagerPhone = string.Empty;
            if (string.IsNullOrEmpty(model.ACCTManagerName) || model.ACCTManagerName == "null")
                model.ACCTManagerName = string.Empty;
            if (string.IsNullOrEmpty(model.PrimaryPhoneNumber) || model.PrimaryPhoneNumber == "null")
                model.PrimaryPhoneNumber = string.Empty;

            if (_iCompanyService.UpdateCompany(model))
            {
                return Json(new { success = true, message = CommonMessage.UpdatedCompany.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedUpdate.ToString() });
            }
        }

        /// <summary>
        /// delete company record from database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteCompany(CommonAuditModel obj)
        {
            obj.LoggedInUserid = User.UserId;
            obj.AppAuditID = 1;
            obj.SessionID = User.SessionId;

            if (_iCompanyService.DeleteCompany(obj))
            {
                return Json(new { success = true, message = CommonMessage.DeletedCompany.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedDelete.ToString() });
            }
        }

        /// <summary>
        /// get company detail based on companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCompany(int companyId)
        {
            //AppAuditID = Convert.ToInt32(Session["appAuditID"]); 
            var data = _iCompanyService.GetCompanyByID(companyId);
            if (data == null)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// check companyname if already exist or not
        /// </summary>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckCompanyID(int companyid)
        {
            string jsondata = string.Empty;
            var result = _iCompanyService.CheckDelete(companyid);
            jsondata = "SimpleDelete";
            return Json(jsondata);
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MultipleCompanyDelete(string companyID)
        {
            CompanyEntity model = new CompanyEntity();
            model.IdList = companyID;
            model.appAuditID = 123;
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();
            try
            {
                var company = _iCompanyService.DeleteMultipleCompany(model);
                return Json(company, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// redirect to edit page based on companyId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int? id)
        {
            int userId = User.UserId;
            var manageAccount = new UserPermissionViewModel();
            manageAccount = _userService.CheckUserModulePermission(userId, ModuleName.AccountManagement); // check account management permission

            if (manageAccount == null)
            {
                manageAccount = new UserPermissionViewModel();
            }

            if (manageAccount.HasViewPermission)
            {
                if (User.RoleId == 4) // check for company customer
                {
                    if (User.CompanyID != id)
                    {
                        return RedirectToAction("UnauthorizedAccess", "Common", null);
                    }
                }

                ViewBag.Editid = id;

                var companyName = _iCompanyService.GetCompanyByID((int)id).CompanyName;
                ViewBag.companyName = companyName;

                //To get the Id of Module
                ModuleEntity module = _iCommonService.ModuleByName(ModuleName.AccountManagement);
                ViewBag.ModuleId = module.ModuleId;

                return View();
            }
            else
            {
                return RedirectToAction("UnauthorizedAccess", "Common", null);
            }
        }

        /// <summary>
        /// check associated account based on company
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AssociatedAccounts(int CompanyId)
        {
            ViewBag.CompanyId = CompanyId;
            return View();
        }
    }
}