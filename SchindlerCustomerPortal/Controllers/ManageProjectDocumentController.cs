﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Project;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.ProjectDocument;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    //TODO: Remove this controler
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ManageProjectDocumentController : Controller
    {
        private readonly IContractDocumentService _IContractDocumentService;
        private readonly ICommonService _ICommonService;

        public ManageProjectDocumentController(IContractDocumentService iContractDocumentService, ICommonService iCommonService)
        {
            this._IContractDocumentService = iContractDocumentService;
            this._ICommonService = iCommonService;
        }
        
        /// <summary>
        /// return index view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// retrun document based on projectId and document type
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <param name="DocumentType"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult List(int ProjectID,string DocumentType)
        {
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.ProjectDocumentManagement);
            ViewBag.ModuleId = module.ModuleId;
            ViewBag.ProjectID = ProjectID;
            ProjectEntity projectobj = _IContractDocumentService.GetProjectById(ProjectID);
            ViewBag.ProjectName = projectobj.ProjectName;
            ViewBag.DocType = DocumentType;
            return View("List");
        }

        /// <summary>
        /// return all document from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetContractDocumentAll(DataTableModel model)
        {
            
            try
            {
                int totalcount = 0;
                // SearchInfoCompanyViewModel model = new SearchInfoCompanyViewModel();
                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);
                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
               
                var Documents = _IContractDocumentService.GetAllDocumentByProject(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Documents.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Documents }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
	}
}