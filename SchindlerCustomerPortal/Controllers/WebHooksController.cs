﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.Models;
using Newtonsoft.Json.Linq;
using SchindlerCustomerPortal.Common;

namespace SchindlerCustomerPortal.Controllers
{
    public class WebHooksController : Controller
    {
        ErrorLogClass objErrLog = new ErrorLogClass();
        string strErrorPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ErrorPath"]);
        string strWebHooksPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WebHooks"]);
        // GET: WebHooks
        public void Index()
        {
            try
            {
                //Request.InputStream.Position = 0;
                var result = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                //   var result=System.IO.File.ReadAllText("C:\\Users\\srbhatt\\Desktop\\Webhooks\\WebHooks20171122092719732.txt");
                               
                string strSessionId = System.DateTime.Now.ToString("yyyyMMddHHmmssFFF");

                objErrLog.ErrorLog(strWebHooksPath + "Log " + strSessionId + ".txt", "In Result");

                System.IO.StreamWriter swTest = new System.IO.StreamWriter(strWebHooksPath + "WebHooks" + strSessionId + ".json");

                swTest.Write(result);
                swTest.Close();
                               
                // jsonString is your JSON-formatted string
                JObject jsonObj = JObject.Parse(result);               

                var results = JsonConvert.DeserializeObject<dynamic>(result);
                //var subscriptionId = results.subscriptionId;
                //var notificationId = results.notificationId;
                objErrLog.ErrorLog(strWebHooksPath + "Log " + strSessionId + ".txt", "After DeserializeObject");
            }
            catch (Exception ex)
            {               
                string strErrorDetail = ex.Message;

                if (!string.IsNullOrEmpty(Convert.ToString(ex.InnerException)))
                {
                    strErrorDetail += "\n\n Inner Exception : \n" + Convert.ToString(ex.InnerException);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ex.StackTrace)))
                {
                    strErrorDetail += "\n\n Stack Trace : \n" + Convert.ToString(ex.StackTrace);
                }

                string strSessionId = System.DateTime.Now.ToString("yyyyMMddHHmmssFFF");

                objErrLog.ErrorLog(strErrorPath + "Error " + strSessionId + ".txt", strErrorDetail);                              
                
                //System.IO.StreamWriter swErr = new System.IO.StreamWriter(strErrorPath + "Error" + strSessionId + ".txt");

                //swErr.Write("Error " + strErrorDetail);
                //swErr.Close();
            }                   
        }

        //public string WriteErrorLog()
        //{ }
    }
}