﻿
using System;
using System.Web.Mvc;
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Services.Document;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Models.Authentication;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ManageDocumentController : BaseController
    {
        private readonly IDocumentService _IDocumentService;
        private readonly ICommonService _ICommonService;

        public ManageDocumentController(IDocumentService iDocumentService, ICommonService iCommonService)
        {
            this._IDocumentService = iDocumentService;
            this._ICommonService = iCommonService;
        }

        /// <summary>
        /// return index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// return document list based on documentType
        /// </summary>
        /// <param name="DocType"></param>
        /// <returns></returns>
        public ActionResult List(string DocType)
        {
            //To get the Id of Module
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.DocManagement);
            ViewBag.ModuleId = module.ModuleId;

            //ViewBag.DocumentType = DocType;
            return View("List");
        }

        /// <summary>
        /// get all documents from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetDocumentAll(DataTableModel model)
        {

            try
            {
                int totalcount = 0;
                var Documents = new List<DocumentEntity>();
                // SearchInfoCompanyViewModel model = new SearchInfoCompanyViewModel();
                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                if (model.CompanyID == 0)
                {
                    Documents = _IDocumentService.GetAllDocument(model, ref totalcount);
                }
                else
                {
                    Documents = _IDocumentService.GetAllDocumentByCompanyId(model, ref totalcount);
                }

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Documents.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Documents }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// add new document record to database
        /// </summary>
        /// <param name="docobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddDocument(DocumentEntity docobj)
        {
            //string Docuploadpath = System.Configuration.ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = docobj.FileName;
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            //var aa = System.Web.HttpContext.Current.Request.Files.Count;
            //foreach (string fileName in System.Web.HttpContext.Current.Request.Files)
            //{
            //    HttpPostedFileBase file = Request.Files[filename];
            //}

            try
            {
                //if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                //{
                //    var document = System.Web.HttpContext.Current.Request.Files["docs"];
                //    if (document.ContentLength > 0)
                //    {
                //        docpath = CommonConfiguration.docuploadPath + document.FileName;
                //        var docsave = Server.MapPath(docpath);
                //        document.SaveAs(docsave);

                //    }

                //}

                // docobj.DocumentName = filename + "_" + DatewithSeconds;
                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);
                    //string[] words = filename.Split('.');

                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);
                    System.IO.File.Copy(sourceFile, destFile, true);
                }
                if (string.IsNullOrEmpty(docobj.CompanyName))
                {
                    docobj.CompanyName = "NONE";
                }
                docobj.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                string fileExtension = System.IO.Path.GetExtension(docpath);
                docobj.FileType = fileExtension;
                docobj.PublishedBy = User.FirstName + " " + User.LastName;
                docobj.appAuditID = 123;
                docobj.SessionID = User.SessionId;

                var documentdetails = _IDocumentService.DocumentInsert(docobj);

                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// upload new document to server
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            string TempPath = "";
            string doctype = "";

            if (Request.Files == null || Request.Files.Count == 0)
            {
                return Json(new { Message = "No file's were selected..!" });
            }

            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        if (file.ContentLength > 0)
                        {
                            TempPath = CommonConfiguration.TempDocPath + file.FileName;
                            var docsave = Server.MapPath(TempPath);
                            file.SaveAs(docsave);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        /// <summary>
        /// download document from server
        /// </summary>
        /// <param name="DocPath"></param>
        /// <returns></returns>
        public ActionResult DownloadDocument(string DocPath)
        {
            byte[] fileBytes;
            try
            {
                if (System.IO.File.Exists(Server.MapPath(DocPath)))
                {
                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(DocPath));
                    string fileName = Path.GetFileName(Server.MapPath(DocPath));
                    string RealName = fileName.Substring(fileName.IndexOf("_") + 1);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, RealName);
                }
                else
                {
                    return RedirectToAction("List");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                fileBytes = null;
                GC.Collect();
            }


        }

        /// <summary>
        /// view document on other tab in browser
        /// </summary>
        /// <param name="DocPath"></param>
        /// <returns></returns>
        public ActionResult ViewDocumentName(string DocPath)
        {
            //var fileContents = ;
            try
            {

                DocPath = System.Web.HttpUtility.UrlDecode(DocPath);
                //byte[] fileBytes = System.IO.File.ReadAllBytes(DocPath);
                string fileName = Path.GetFileName(Server.MapPath(DocPath));
                string RealName = fileName.Substring(fileName.IndexOf("_") + 1);

                //fileName = System.IO.Path.GetFileName(fileName);
                //string file = Server.MapPath(DocPath);
                byte[] filedata = System.IO.File.ReadAllBytes(Server.MapPath(DocPath));
                string contentType = MimeMapping.GetMimeMapping(Server.MapPath(DocPath));
                //if (!File.Exists(file))
                //{
                //    return HttpNotFound();
                //}
                // return File(file, fileName, "application/octet-stream");
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = RealName,
                    Inline = true,
                };

                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //finally
            //{

            //    fileBytes = null;
            //    GC.Collect();
            //}


        }

        /// <summary>
        /// edit document based on documentId
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditDocument(int docId)
        {
            try
            {
                var documentdetails = _IDocumentService.GetDocById(docId);
                return Json(documentdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update document data to database
        /// </summary>
        /// <param name="docobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateDocument(DocumentEntity docobj)
        {

            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = docobj.FileName;
            try
            {
                //if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                //{
                //    var document = System.Web.HttpContext.Current.Request.Files["docs"];
                //    if (document.ContentLength > 0)
                //    {
                //        docpath = CommonConfiguration.docuploadPath + document.FileName;
                //        var docsave = Server.MapPath(docpath);
                //        document.SaveAs(docsave);
                //        docobj.DocumentPath = docpath + "_" + DatewithSeconds;
                //        string fileExtension = System.IO.Path.GetExtension(docpath);
                //        docobj.FileType = fileExtension;
                //    }

                //}

                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);
                    //string[] words = filename.Split('.');

                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);
                    //string destFile = System.IO.Path.Combine(Server.MapPath(docpath), filename + "_" + DatewithSeconds);
                    System.IO.File.Copy(sourceFile, destFile, true);

                    docobj.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                    string fileExtension = System.IO.Path.GetExtension(docobj.DocumentPath);
                    docobj.FileType = fileExtension;
                }
                if (string.IsNullOrEmpty(docobj.CompanyName))
                {
                    docobj.CompanyName = "NONE";
                }
                docobj.UpdatedBy = User.FirstName + " " + User.LastName;


                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                docobj.appAuditID = appAuditID;
                docobj.ApplicationOperation = ApplicationOperation.ProjectManagement_AddProjectMilestone.ToString();
                docobj.SessionID = User.SessionId;
                docobj.UserId = User.UserId;


                #endregion

                var documentdetails = _IDocumentService.DocumentUpdateService(docobj);
                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// delete document record from databse
        /// </summary>
        /// <param name="DocID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteDocument(int DocID)
        {
            DocumentEntity doc = new DocumentEntity();
            //model.UserID = userID;
            doc.DocumentID = DocID;
            doc.DeletedBy = User.UserId.ToString();




            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            doc.appAuditID = appAuditID;
            doc.ApplicationOperation = ApplicationOperation.ProjectManagement_AddProjectMilestone.ToString();
            doc.SessionID = User.SessionId;
            doc.UserId = User.UserId;


            #endregion
            try
            {
                var user = _IDocumentService.DeleteDocumentService(doc);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// to apply permission to perticular document
        /// </summary>
        /// <param name="docID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult UserDocumentPermission(int docID)
        {
            List<DocumentPermission> objlst = new List<DocumentPermission>();
            objlst = _IDocumentService.GetUserDocumentPermission(docID);
            return Json(new { data = objlst }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// update user permission for perticular document
        /// </summary>
        /// <param name="DocID"></param>
        /// <param name="UserID"></param>
        /// <param name="HasPermission"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateUserAccessPermission(int DocID, int UserID, bool HasPermission, string Type)
        {


            UserPermissionDocumentModel obj = new UserPermissionDocumentModel();
            obj.appAuditID = 1;
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.UpdatedBy = User.UserId.ToString();

            obj.UserID = UserID;
            obj.DocumentID = DocID;
            obj.ColumnValue = HasPermission;

            //var ColumnName = (Type == "view" ? ModulePermissionDbColumn.View_Permission.ToString() : (Type == "upload" ? ModulePermissionDbColumn.Upload_Permission.ToString() : (Type == "delete" ? ModulePermissionDbColumn.Delete_Permission.ToString() : string.Empty)));

            var ColumnName = "AllColumn";
            if (string.IsNullOrEmpty(ColumnName))
            {
                return Json(new { success = false, message = CommonMessage.InvalidError.ToString() });
            }

            obj.ColumnName = ColumnName;

            var result = _IDocumentService.InsertUpdateUserDocumentAccessPermissionByModule(obj);
            if (result >= 1)
            {
                return Json(new { success = true, message = CommonMessage.AccessUpdatedSuccessfully.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }

        }

        /// <summary>
        /// delete multiple user permission for document
        /// </summary>
        /// <param name="docid"></param>
        /// <param name="userids"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MultipleUserPermissionDelete(int docid, string userids)
        {
            UserPermissionDocumentMulti obj = new UserPermissionDocumentMulti();
            obj.appAuditID = 1;
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.UpdatedBy = User.UserId.ToString();

            obj.DocumentID = docid;
            obj.UserIds = userids;
            obj.View_Permission = false;
            obj.Upload_Permission = false;
            obj.Delete_Permission = false;
            //obj.Edit_Permission = false;
            //obj.Print_Permission = false;


            var result = _IDocumentService.UserDocumentAccessService_UpdateAll(obj);
            if (result)
            {
                return Json(new { success = true, message = CommonMessage.AccessUpdatedSuccessfully.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }
        }
    }
}