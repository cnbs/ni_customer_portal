﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Company;
using Newtonsoft.Json;
using System.Web.Security;
using SchindlerCustomerPortal.Models.Login;
using SchindlerCustomerPortal.Services.Project;
using System.Drawing;
using System.IO;

namespace SchindlerCustomerPortal.Controllers
{
    //[CustomAuthorize]
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ManageUserController : BaseController
    {
        public string strModuleName;

        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly ICompanyService _companyService;
        private readonly IProjectService _projectService;

        public ManageUserController(IUserService userService,
            ICommonService commonService,
            ICompanyService companyService,
            IProjectService projectService)
        {
            this._userService = userService;
            this._commonService = commonService;
            this._companyService = companyService;
            this._projectService = projectService;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// return userList view
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserList(int companyId)
        {
            int userId = User.UserId;
            var permission = new UserPermissionViewModel();
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            ViewBag.companyId = companyId;
            if (companyId != 0)
            {

                AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.EmployeeManagement, "Employee List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.EmployeeManagement_ListEmployee.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

                var companyName = _companyService.GetCompanyByID(companyId).CompanyName;
                ViewBag.companyName = companyName;
                ViewBag.userManagement = "User Management";

                permission = _userService.CheckUserModulePermission(userId, ModuleName.UserManagement);

                //To get the Id of Module
                ModuleEntity module = _commonService.ModuleByName(ModuleName.UserManagement);
                ViewBag.ModuleId = module.ModuleId;

                if (permission == null)
                {
                    permission = new UserPermissionViewModel();
                }

                if (User.RoleId != 1)
                {
                    if (!permission.HasViewPermission)
                    {
                        return RedirectToAction("UnauthorizedAccess", "Common", null);
                    }
                }

            }
            else
            {

                AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.UserManagement, "User List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.UserManagement_ListUser.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

                ViewBag.companyName = "Schindler";
                ViewBag.userManagement = "Employee Management";

                permission = _userService.CheckUserModulePermission(userId, ModuleName.EmployeeManagement);

                //To get the Id of Module
                ModuleEntity module = _commonService.ModuleByName(ModuleName.EmployeeManagement);
                ViewBag.ModuleId = module.ModuleId;

                if (permission == null)
                {
                    permission = new UserPermissionViewModel();
                }

                if (User.RoleId != 1)
                {
                    if (!permission.HasViewPermission)
                    {
                        return RedirectToAction("UnauthorizedAccess", "Common", null);
                    }
                }
            }


            ViewBag.Designation = _commonService.designationList();

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View(permission);
        }

        /// <summary>
        /// add new user to UserMaster table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddUser(UserEntity model)
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    //var fileName = Path.GetFileName(logo.FileName);
                    //var ext = Path.GetExtension(logo.FileName);

                    //ImageName = fileName;
                    var comPath = CommonConfiguration.userProfilePicPath + model.ProfilePic;

                    var imgSave = Server.MapPath(comPath);
                    logo.SaveAs(imgSave);
                    //user.UserProfilePic = ImageName;

                    // Start : Thumbnail code added by Vivek
                    Image image = Image.FromFile(imgSave);
                    Size thumbnailSize = StaticUtilities.GetThumbnailSize(image);
                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);
                    var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath), "Thumb_" + model.ProfilePic);
                    // Save thumbnail.
                    thumbnail.Save(thumbpath);
                    //user.UserProfilePic = ImageName;
                    // End : Thumbnail code added by Vivek
                }
            }

            try
            {
                //model.CompanyID = User.CompanyID;
                model.CreatedBy = User.UserId.ToString();
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;

                model.SessionID = User.SessionId;
                model.Status = 4; // 4 means pending status
                model.Token = (Guid.NewGuid().ToString("N"));
                model.LastName = string.Empty;



                if (string.IsNullOrEmpty(model.Address1))
                    model.Address1 = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.PostalCode))
                    model.PostalCode = string.Empty;

                // for schindler company's employee insert
                if (model.CompanyID == 0)
                {
                    model.RoleID = 3; // if Is Admin checkbox is not checked then he is employee
                    model.ApplicationOperation = ApplicationOperation.EmployeeManagement_AddEmployee.ToString();
                }
                else
                {
                    model.RoleID = 4; // it is for Customer
                    model.ApplicationOperation = ApplicationOperation.UserManagement_AddUser.ToString();
                }

                model.TokenExpire = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
                string SuperAdmin = System.Configuration.ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();
                //SendVerificationEmailToUser(userResult.UserID, userResult.Email, "Schindler - Please verfiy account", SFrom, SuperAdmin);

                model.Email = model.Email.ToLower();

                //added by Arpit patel(30-Mar-2018)
                var address = "";
                address = Convert.ToString(model.City) + " " + Convert.ToString(model.StateText) + " " + Convert.ToString(model.PostalCode);

                Tuple<double, double> abc = _commonService.GenerateLatLong(address);
                model.Latitude = abc.Item1;
                model.Longitude = abc.Item2;

                var user = _userService.AddUser(model);

                if (user == 1)
                {

                }
                else
                {
                    #region Email Send

                    var userResult = _userService.Find(model.Email);

                    string Subject = "myschindlerprojects – confirm your email";

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                    _userService.SetTokenService(token, userResult.UserID, ExpToken);

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", userResult.FirstName);
                    //dic.Add("UserName", userResult.FirstName);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "VerifyAccountAdmin.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, model.Email, Subject, Sbody);

                    #endregion
                }


                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// display all user for perticular company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SelectUser(int companyId)
        {
            int count = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;

                if (companyId == 0)
                {
                    model.ApplicationOperation = ApplicationOperation.EmployeeManagement_ListEmployee.ToString();
                    model.ModuleName = ModuleName.EmployeeManagement;
                }
                else
                {
                    model.ApplicationOperation = ApplicationOperation.UserManagement_ListUser.ToString();
                    model.ModuleName = ModuleName.UserManagement;
                }

                model.SessionID = User.SessionId;

                model.CompanyID = companyId;
                model.UserID = User.UserId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var user = _userService.GetAllUserByCompanyId(model, ref count);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //count = user.Count;
                }

                return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        /// <summary>
        /// select perticular user detail
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult UserEdit(int userId)
        {
            try
            {
                var user = _userService.GetUserById(userId);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update user detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateUser(UserEntity model)
        {
            int appAuditID = 0;
            string CurrentDateTime = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    var fileName = CurrentDateTime + "_" + (logo.FileName);

                    var comPath = CommonConfiguration.userProfilePicPath + fileName;

                    var imgSave = Server.MapPath(comPath);

                    logo.SaveAs(imgSave);
                    model.ProfilePic = fileName;

                    // Start : Thumbnail code added by Vivek
                    Image image = Image.FromFile(imgSave);
                    Size thumbnailSize = StaticUtilities.GetThumbnailSize(image);
                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);
                    var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath), "Thumb_" + fileName);
                    // Save thumbnail.
                    thumbnail.Save(thumbpath);
                    // End : Thumbnail code added by Vivek
                }
                else
                {
                    model.ProfilePic = "-1";
                }
            }
            else
            {
                model.ProfilePic = "-1";
            }

            if (model.IsDeleted == 1)
            {
                model.ProfilePic = "no_images.png";

            }

            model.LastName = string.Empty;

            if (string.IsNullOrEmpty(model.Address1))
                model.Address1 = string.Empty;

            if (string.IsNullOrEmpty(model.State))
                model.State = string.Empty;

            if (string.IsNullOrEmpty(model.City))
                model.City = string.Empty;

            if (string.IsNullOrEmpty(model.PostalCode))
                model.PostalCode = string.Empty;

            string token = (Guid.NewGuid().ToString("N"));
            model.Token = token;

            try
            {

                int superAdminCompanyId = 0;

                var getUserCompanyName = _userService.GetUserById(model.UserID);

                if (getUserCompanyName.CompanyID == 0)
                {
                    model.ApplicationOperation = ApplicationOperation.EmployeeManagement_UpdateEmployee.ToString();
                }
                else
                {
                    model.ApplicationOperation = ApplicationOperation.UserManagement_UpdateUser.ToString();
                }
                var address = "";
                address = Convert.ToString(model.City) + " " + Convert.ToString(model.StateText) + " " + Convert.ToString(model.PostalCode);
                Tuple<double, double> abc = _commonService.GenerateLatLong(address);
                model.Latitude = abc.Item1;
                model.Longitude = abc.Item2;
                //   _commonService

                var user = _userService.UpdateUser(model); // update user detail

                if (model.AccountStatus == 18)
                {
                    if (getUserCompanyName.AccountStatus != 18)
                    {
                        int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                        _userService.SetTokenService(token, model.UserID, ExpToken);

                        #region Email Send

                        string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                        string Subject = "myschindlerprojects – your account has been approved";

                        string ActivationUrl = string.Empty;

                        //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/ActiveAccount?TokenID=" + model.Token + "");

                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", model.FirstName);
                        //dic.Add("UserName", model.FirstName);

                        dic.Add("MessageURL", ActivationUrl);

                        Sbody = StaticUtilities.getEmailBody("User", "ActivateAccountNew.html", dic);

                        SendEmail.Send(CommonConfiguration.NoReplyEmail, model.Email, Subject, Sbody);




                        #endregion

                        #region Superadmin Email Send

                        //var superAdminList = _userService.CheckPermissionForMailNotification(superAdminCompanyId);

                        //foreach (var item in superAdminList)
                        //{
                        //    string Subject1 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                        //    string ActivationUrl1 = string.Empty;

                        //    string headurl1 = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                        //    ActivationUrl1 = Server.HtmlEncode("" + headurl + "/Dashboard/Index");

                        //    string Sbody1 = null;

                        //    Dictionary<string, string> dic1 = new Dictionary<string, string>();

                        //    dic1.Add("PersonName", item.FirstName);

                        //    dic1.Add("NewRegisteredUser", model.FirstName);

                        //    dic1.Add("CompanyName", getUserCompanyName.CompanyName);

                        //    dic1.Add("MessageURL", ActivationUrl1);

                        //    Sbody1 = StaticUtilities.getEmailBody("User", "NewUserRegisteredAdmin.html", dic1);

                        //    SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject1, Sbody1);
                        //}

                        #endregion

                        #region companyadmin Email Send

                        //var companyAdminList = _userService.CheckPermissionForMailNotification(getUserCompanyName.CompanyID);

                        //if (getUserCompanyName.CompanyID != 0) // for schindler employee
                        //{
                        //    foreach (var item in companyAdminList)
                        //    {
                        //        string Subject2 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                        //        string ActivationUrl2 = string.Empty;

                        //        string headurl2 = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                        //        ActivationUrl2 = Server.HtmlEncode("" + headurl + "/Dashboard/Index");

                        //        string Sbody2 = null;

                        //        Dictionary<string, string> dic2 = new Dictionary<string, string>();

                        //        dic2.Add("PersonName", item.FirstName);

                        //        dic2.Add("NewRegisteredUser", model.FirstName);

                        //        //dic2.Add("CompanyName", user.CompanyName);

                        //        dic2.Add("MessageURL", ActivationUrl2);

                        //        Sbody2 = StaticUtilities.getEmailBody("User", "NewUserRegisteredCompanyAdmin.html", dic2);

                        //        SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject2, Sbody2);
                        //    }
                        //}

                        #endregion
                    }

                }


                if (user)
                {
                    if (model.UserID == User.UserId)
                    {
                        var userresult = _userService.GetUserById(model.UserID);

                        #region Login FormAthentication
                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                        serializeModel.UserId = userresult.UserID;
                        //  serializeModel.UserName = userresult.Username;
                        serializeModel.FirstName = userresult.FirstName;
                        serializeModel.LastName = userresult.LastName;
                        // serializeModel.roles = roles;
                        serializeModel.Email = userresult.Email;
                        serializeModel.RoleId = userresult.RoleID;
                        serializeModel.SessionID = Session.SessionID;
                        serializeModel.CompanyID = userresult.CompanyID;
                        serializeModel.ProfilePic = userresult.ProfilePic;
                        serializeModel.CompanyName = userresult.CompanyName;

                        string userData = JsonConvert.SerializeObject(serializeModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                        1,
                        userresult.Email,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(15),
                        false, //pass here true, if you want to implement remember me functionality
                        userData);

                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(faCookie);

                        #endregion
                    }

                }

                // permission update
                List<PermissionList> lstPermissionList = new List<PermissionList>();

                var permissionList = System.Web.HttpContext.Current.Request.Form["permissionList"];
                if (permissionList != null)
                {
                    lstPermissionList = JsonConvert.DeserializeObject<List<PermissionList>>(permissionList);

                    UpdateUserAccessPermissionMethod(lstPermissionList);
                }


                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UserDelete(int userID)
        {
            UserEntity model = new UserEntity();
            model.UserID = userID;
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;

            var getUserCompanyName = _userService.GetUserById(model.UserID);

            if (getUserCompanyName.CompanyID == 0)
            {
                model.ApplicationOperation = ApplicationOperation.EmployeeManagement_DeleteEmployee.ToString();
            }
            else
            {
                model.ApplicationOperation = ApplicationOperation.UserManagement_DeleteUser.ToString();
            }

            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();
            try
            {
                var user = _userService.DeleteUser(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// select login user detail
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SelectProfile()
        {
            int userID = User.UserId;

            try
            {
                var user = _userService.GetUserById(userID);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// change password, update UserMaster table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePassword(ChangePassword model)
        {
            model.UserID = User.UserId;
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;

            model.ApplicationOperation = ApplicationOperation.ChangePassword.ToString();

            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();

            string saltKey = EncryptionService.CreateSaltKey(5);
            model.PasswordSalt = saltKey;
            model.NewPassword = EncryptionService.CreatePasswordHash(model.NewPassword, saltKey, "");

            var userresult = _userService.Find(User.Email.ToString());
            model.OldPassword = EncryptionService.CreatePasswordHash(model.OldPassword, userresult.PasswordSalt, "");

            var user = _userService.changePasswordForUser(model);
            if (!user)
            {
                return Json(new { success = true, message = "Old password does not match" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, message = "changed password successfully" }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MultipleUserDelete(string userID)
        {
            UserEntity model = new UserEntity();
            model.IdList = userID;
            model.appAuditID = 123;
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();
            try
            {
                var user = _userService.DeleteMultipleUser(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// to select user module permission
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult UserModulePermission(int userID)
        {
            //UserEntity model = new UserEntity();
            //model.IdList = userID;
            //model.appAuditID = 123;
            //model.SessionID = User.SessionId;
            //model.UpdatedBy = User.UserId.ToString();
            //try
            //{
            //    var user = _userService.DeleteMultipleUser(model);
            //    return Json(user, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception ex)
            //{
            //    string error = ex.Message;
            //    throw;
            //}

            List<UserPermissionViewModel> objlst = new List<UserPermissionViewModel>();
            objlst = _userService.GetUserModulePermission(userID);
            return Json(new { data = objlst }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// to update user module permission
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="ModuleId"></param>
        /// <param name="HasPermission"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateUserAccessPermission(int UserID, int ModuleId, bool HasPermission, string Type)
        {

            UserPermissionModuleModel obj = new UserPermissionModuleModel();
            //obj.appAuditID = 1;

            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            obj.appAuditID = appAuditID;

            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.UpdatedBy = User.UserId.ToString();

            obj.UserID = UserID;
            obj.ModuleId = ModuleId;
            obj.ColumnValue = HasPermission;

            //var ColumnName = (Type == "view" ? ModulePermissionDbColumn.View_Permission.ToString() : (Type == "add" ? ModulePermissionDbColumn.Add_Permission.ToString() : (Type == "edit" ? ModulePermissionDbColumn.Edit_Permission.ToString() : (Type == "delete" ? ModulePermissionDbColumn.Delete_Permission.ToString() : string.Empty))));

            var ColumnName = "AllColumn"; // set all column for permission
            if (string.IsNullOrEmpty(ColumnName))
            {
                return Json(new { success = false, message = CommonMessage.InvalidError.ToString() });
            }

            obj.ColumnName = ColumnName;

            var result = _userService.InsertUpdateUserModuleAccessPermissionByModule(obj);
            if (result >= 1)
            {
                return Json(new { success = true, message = CommonMessage.AccessUpdatedSuccessfully.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }

        }

        /// <summary>
        /// to update user module permission
        /// </summary>
        /// <param name="model"></param>
        [NonAction]
        public void UpdateUserAccessPermissionMethod(List<PermissionList> model)
        {
            UserPermissionModuleModel obj = new UserPermissionModuleModel();
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            obj.appAuditID = appAuditID;
            obj.ApplicationOperation = ApplicationOperation.EmployeeUserModulePermission.ToString();
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.UpdatedBy = User.UserId.ToString();

            foreach (var item in model)
            {
                obj.UserID = item.UserID;
                obj.ModuleId = item.ModuleId;
                obj.ColumnValue = item.HasPermission;

                var ColumnName = "AllColumn"; // set all column for permission

                obj.ColumnName = ColumnName;

                var result = _userService.InsertUpdateUserModuleAccessPermissionByModule(obj);
            }
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="moduleids"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MultipleUserPermissionDelete(int userid, string moduleids)
        {
            UserPermissionModuleModelMulti obj = new UserPermissionModuleModelMulti();
            obj.appAuditID = 1;
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.UpdatedBy = User.UserId.ToString();


            obj.UserID = userid;
            obj.ModuleIds = moduleids;
            obj.View_Permission = false;
            obj.Add_Permission = false;
            obj.Delete_Permission = false;
            obj.Edit_Permission = false;
            obj.Print_Permission = false;

            // var ColumnName = (Type == "view" ? ModulePermissionDbColumn.View_Permission.ToString() : (Type == "add" ? ModulePermissionDbColumn.Add_Permission.ToString() : (Type == "edit" ? ModulePermissionDbColumn.Edit_Permission.ToString() : (Type == "delete" ? ModulePermissionDbColumn.Delete_Permission.ToString() : string.Empty))));
            //if (string.IsNullOrEmpty(ColumnName))
            //{
            //    return Json(new { success = false, message = CommonMessage.InvalidError.ToString() });
            //}

            //obj.ColumnName = ColumnName;

            var result = _userService.UserModuleAccess_UpdateAll(obj);
            if (result)
            {
                return Json(new { success = true, message = CommonMessage.AccessUpdatedSuccessfully.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }
        }

        /// <summary>
        /// update user account status
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="StatusID"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateUserAccountStatus(int UserID, int StatusID, string Email)
        {
            UserEntity model = new UserEntity();
            model.UserID = UserID;
            model.AccountStatus = StatusID;

            model.appAuditID = 123;
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();

            var userFirstName = _userService.Find(Email.Trim()).FirstName;

            if (StatusID == (int)Status.Approved)
            {
                string token = (Guid.NewGuid().ToString("N"));
                model.Token = token;

                int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                _userService.SetTokenService(token, UserID, ExpToken);

                string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                string Subject = "myschindlerprojects – your account has been approved";

                string ActivationUrl = string.Empty;

                //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/ActiveAccount?TokenID=" + model.Token + "");

                #region Email Send


                string Sbody = null;

                Dictionary<string, string> dic = new Dictionary<string, string>();

                dic.Add("PersonName", userFirstName);
                //dic.Add("UserName", model.FirstName);

                dic.Add("MessageURL", ActivationUrl);

                Sbody = StaticUtilities.getEmailBody("User", "ActivateAccountNew.html", dic);

                SendEmail.Send(CommonConfiguration.NoReplyEmail, Email.Trim(), Subject, Sbody);




                #endregion

                //string body = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head><body><div style='background-color:whitesmoke;padding:10px 12px;'><span style='background-color:whitesmoke;'><h2 style='color:#24425E;font-family:Calibri;margin-left:5px;'>Welcome To Schindler!</h2></span></div>";
                //body += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi " + userFirstName.Trim() + "," + "<br/><br/>";

                //body += "Your Profile has been created for Schindler!<br/><br/>To activate please click the link below and follow the direction :<br/><br/>";
                //body += "<a href=" + ActivationUrl + ">" + ActivationUrl + "</a><br/><br/>";

                //body += "If clicking the link above doesn't work, please copy and paste the URL in a new browser window instead.<br/><br/>";

                //body += "If you've received this mail in error, it's likely that another user entered<br/> your email address by mistake. If you didn't initiate the request,<br/> you don't need to take any further action and can safely<br/> disregard this email.<br/><br/>";

                //body += "Sincerely,<br/>The Schindler Support Team</p></div></body></html>";

                //SendEmail.Send(From, Email.Trim(), Subject, body);

            }

            try
            {
                var user = _userService.UpdateUserStatus(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// display newly registered all user for dashboard
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult NewlyRegisterUserList()
        {
            int count = 0;
            var permission = new UserPermissionViewModel();

            permission = _userService.CheckUserModulePermission(User.UserId, ModuleName.UserManagement);

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            try
            {
                DataTableModel model = new DataTableModel();

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.UserManagement_ListUser.ToString();
                model.SessionID = User.SessionId;

                if (User.RoleId != 1)
                {
                    // user don't have permission to access User management module
                    if (!permission.HasViewPermission)
                    {
                        var userList = new List<UserEntity>();

                        return Json(new { draw = count, recordsFiltered = count, recordsTotal = count, data = userList }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.CompanyID = User.CompanyID;
                        model.UserID = User.UserId;

                        model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                        var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                        model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                        model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                        model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                        model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                        var user = _userService.GetAllNewRegisteredUser(model, ref count);

                        if (!string.IsNullOrEmpty(model.SearchTerm))
                        {
                            count = user.Count;
                        }
                        return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    model.CompanyID = User.CompanyID;
                    model.UserID = User.UserId;

                    model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                    var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                    model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                    model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                    model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                    var user = _userService.GetAllNewRegisteredUser(model, ref count);

                    if (!string.IsNullOrEmpty(model.SearchTerm))
                    {
                        count = user.Count;
                    }
                    return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }


        [HttpPost]
        public JsonResult NewlyRegisterUserCount()
        {
            int count = 0;
            int CompanyID = 0;
            var permission = new UserPermissionViewModel();

            permission = _userService.CheckUserModulePermission(User.UserId, ModuleName.UserManagement);

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            try
            {
                DataTableModel model = new DataTableModel();

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.UserManagement_ListUser.ToString();
                model.SessionID = User.SessionId;

                if (User.RoleId != 1)
                {
                    // user don't have permission to access User management module
                    if (!permission.HasViewPermission)
                    {
                        var userList = new List<UserEntity>();

                        return Json(new { draw = count, recordsFiltered = count, recordsTotal = count, data = userList }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.CompanyID = User.CompanyID;
                        model.UserID = User.UserId;
                        CompanyID = model.CompanyID;
                        //model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                        //var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                        //model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                        //model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                        //model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                        //model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                        var user = _userService.GetAllNewRegisteredUsersCount(CompanyID, ref count);

                        //if (!string.IsNullOrEmpty(model.SearchTerm))
                        //{
                        //    count = user.Count;
                        //}
                        return Json(new { usercount = user }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    model.CompanyID = User.CompanyID;
                    model.UserID = User.UserId;
                    CompanyID = model.CompanyID;

                    //model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                    //var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                    //model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                    //model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                    //model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    //model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                    var user = _userService.GetAllNewRegisteredUsersCount(CompanyID, ref count);

                    //if (!string.IsNullOrEmpty(model.SearchTerm))
                    //{
                    //    count = user.Count;
                    //}
                    return Json(new { usercount = user }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        /// <summary>
        /// select newly registered  user detail
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditNewlyRegisteredUser(int userId)
        {
            try
            {
                var user = _userService.GetNewRegisteredUserById(userId);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update newly registered user detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateNewlyRegisteredUser(UserEntity model)
        {
            int appAuditID = 0;
            var companyNameCheck = true;

            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.UserManagement_UpdateUser.ToString();
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();

            companyNameCheck = _userService.CheckCompanyforEditNewRegisterUser(model.CompanyName);

            if (Convert.ToString(model.Token) == "1")
            {
                companyNameCheck = true;
            }

            if (!companyNameCheck)
            {
                var returnResult = "Company not exist";
                return Json(returnResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.LastName = string.Empty;

                if (string.IsNullOrEmpty(model.Address1))
                    model.Address1 = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.PostalCode))
                    model.PostalCode = string.Empty;

                string token = (Guid.NewGuid().ToString("N"));
                model.Token = token;

                int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                _userService.SetTokenService(token, model.UserID, ExpToken);

                var address = "";
                address = Convert.ToString(model.City) + " " + Convert.ToString(model.StateText) + " " + Convert.ToString(model.PostalCode);

                Tuple<double, double> abc = _commonService.GenerateLatLong(address);
                model.Latitude = abc.Item1;
                model.Longitude = abc.Item2;

                var user = _userService.UpdateNewRegisteredUser(model);
                int superAdminCompanyId = 0;

                if (model.AccountStatus == 18)
                {
                    #region Email

                    string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                    string Subject = "myschindlerprojects – your account has been approved";

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/ActiveAccount?TokenID=" + model.Token + "");

                    //string body = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head><body><div style='background-color:whitesmoke;padding:10px 12px;'><span style='background-color:whitesmoke;'><h2 style='color:#24425E;font-family:Calibri;margin-left:5px;'>Welcome To Schindler!</h2></span></div>";
                    //body += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi " + model.FirstName.Trim() + "," + "<br/><br/>";

                    //body += "Your Profile has been created for Schindler!<br/><br/>To activate please click the link below and follow the direction :<br/><br/>";
                    //body += "<a href=" + ActivationUrl + ">" + ActivationUrl + "</a><br/><br/>";

                    //body += "If clicking the link above doesn't work, please copy and paste the URL in a new browser window instead.<br/><br/>";

                    //body += "If you've received this mail in error, it's likely that another user entered<br/> your email address by mistake. If you didn't initiate the request,<br/> you don't need to take any further action and can safely<br/> disregard this email.<br/><br/>";

                    //body += "Sincerely,<br/>The Schindler Support Team</p></div></body></html>";

                    //SendEmail.Send(From, model.Email, Subject, body);

                    #region Email Send


                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", model.FirstName);
                    //dic.Add("UserName", model.FirstName);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "ActivateAccountNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, model.Email, Subject, Sbody);




                    #endregion

                    #endregion

                    #region Superadmin Email Send

                    //var superAdminList = _userService.CheckPermissionForMailNotification(superAdminCompanyId);

                    //foreach (var item in superAdminList)
                    //{
                    //    string Subject1 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                    //    string ActivationUrl1 = string.Empty;

                    //    string headurl1 = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                    //    ActivationUrl1 = Server.HtmlEncode("" + headurl + "/Dashboard/Index");

                    //    string Sbody1 = null;

                    //    Dictionary<string, string> dic1 = new Dictionary<string, string>();

                    //    dic1.Add("PersonName", item.FirstName);

                    //    dic1.Add("NewRegisteredUser", model.FirstName);

                    //    dic1.Add("CompanyName", model.CompanyName);

                    //    dic1.Add("MessageURL", ActivationUrl1);

                    //    Sbody1 = StaticUtilities.getEmailBody("User", "NewUserRegisteredAdmin.html", dic1);

                    //    SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject1, Sbody1);
                    //}

                    #endregion

                    #region companyadmin Email Send

                    //var getUserCompanyName = _userService.GetUserById(model.UserID);

                    //if (getUserCompanyName.CompanyID != 0) // for schindler employee
                    //{
                    //    var companyAdminList = _userService.CheckPermissionForMailNotification(getUserCompanyName.CompanyID);

                    //    foreach (var item in companyAdminList)
                    //    {
                    //        string Subject2 = "myschindlerprojects – A new user " + model.FirstName + " registered on the portal awaiting your approval.";

                    //        string ActivationUrl2 = string.Empty;

                    //        string headurl2 = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                    //        ActivationUrl2 = Server.HtmlEncode("" + headurl + "/Dashboard/Index");

                    //        string Sbody2 = null;

                    //        Dictionary<string, string> dic2 = new Dictionary<string, string>();

                    //        dic2.Add("PersonName", item.FirstName);

                    //        dic2.Add("NewRegisteredUser", model.FirstName);

                    //        //dic2.Add("CompanyName", user.CompanyName);

                    //        dic2.Add("MessageURL", ActivationUrl2);

                    //        Sbody2 = StaticUtilities.getEmailBody("User", "NewUserRegisteredCompanyAdmin.html", dic2);

                    //        SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject2, Sbody2);
                    //    }
                    //}

                    #endregion
                }

                return Json(user, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// return alert message for user management module
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AlertForUserManagement()
        {
            var messageList = _commonService.AlertMessgaeByModule(ModuleName.UserManagement);
            return Json(messageList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// update user project assign
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns> 
        [HttpPost]
        public JsonResult UserProjectAssign(UserProjectAssignEntity model)
        {
            model.UpdatedBy = Convert.ToString(User.UserId);
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.ProjectMapping.ToString();
            model.SessionID = User.SessionId;

            if (string.IsNullOrEmpty(model.ProjectNo))
            {
                model.ProjectNo = "RemoveAll";
            }
            if (string.IsNullOrEmpty(model.UnsubscribeProjectNo))
            {
                model.UnsubscribeProjectNo = "RemoveAll";
            }

            var user = _userService.UpdateuserProjectAssign(model);
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// select perticular user project assign detail
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditProjectAssign(int userId)
        {
            UserProjectAssignEntity model = new UserProjectAssignEntity();

            model.UserID = userId;

            try
            {
                var user = _userService.GetUserProjectAssign(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of project for perticuler company
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="companyId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ProjectNameAutoCompleteByCompanyId(string searchTerm, int companyId, int RoleId, int UserId)
        {
            //int companyId = User.CompanyID; //1; // User.CompanyID;
            //int RoleId = User.RoleId;

            //if (searchTerm.Contains(","))
            //{
            //    string result = searchTerm.Split(',')[searchTerm.Split(',').Length - 1];
            //    if (!string.IsNullOrEmpty(result))
            //    {
            //        searchTerm = result.Trim();
            //    }
            //}
            if (string.IsNullOrEmpty(searchTerm))
                searchTerm = string.Empty;

            try
            {
                var projectList = _projectService.GetAllProjectByCompanyIdandSearchTerm(companyId, searchTerm, RoleId, UserId);

                if (companyId == 0)
                {
                    return Json(new { data = projectList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(projectList, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public ActionResult LatestRegisteredUsers()
        {
            ViewBag.Designation = _commonService.designationList(); // Designation List
            ModuleEntity module = _commonService.ModuleByName(ModuleName.UserManagement);
            ViewBag.ModuleId = module.ModuleId;
            return View();
        }



    }
}