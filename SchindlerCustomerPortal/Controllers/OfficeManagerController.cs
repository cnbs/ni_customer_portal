﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.Common;
using Newtonsoft.Json;
using SchindlerCustomerPortal.Services.OfficeManager;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Models.Authentication;

namespace SchindlerCustomerPortal.Controllers
{
    //[Authorize]
    //[CustomAuthorize]
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class OfficeManagerController : BaseController
    {
        public string strModuleName;

        private readonly IOfficeManagerService _OfficeManagerService;
        private readonly ICommonService _commonService;

        public OfficeManagerController(IOfficeManagerService officeManagerService, ICommonService commonService
           )
        {
            this._OfficeManagerService = officeManagerService;
            this._commonService = commonService;

        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// return List view
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        /// <summary>
        /// return list of help from database
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            //To get the Id of Module
            //ModuleEntity module = _ICommonService.ModuleByName(ModuleName.AccountManagement);
            // ViewBag.ModuleId = module.ModuleId;
            ModuleEntity module = _commonService.ModuleByName(ModuleName.OfficeManager);
            ViewBag.ModuleId = module.ModuleId;

            var permission = new UserPermissionViewModel();
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //permission = _userService.CheckUserModulePermission(User.UserId, ModuleName.HelpManagement);

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            //if (User.RoleId != 1)
            //{
            //    if (!permission.HasViewPermission)
            //    {
            //        return RedirectToAction("UnauthorizedAccess", "Common", null);
            //    }
            //}

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.OfficeManager, "Office Manager List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.OfficeManager_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View("List");
        }

        /// <summary>
        /// add new office manager to Office Manager table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddOfficeManager(OfficeManagerEntity model)
        {
            try
            {
                model.CreatedBy = User.UserId.ToString();
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.SessionID = User.SessionId;


                if (string.IsNullOrEmpty(model.Street))
                    model.Street = string.Empty;

                if (string.IsNullOrEmpty(model.State))
                    model.State = string.Empty;

                if (string.IsNullOrEmpty(model.City))
                    model.City = string.Empty;

                if (string.IsNullOrEmpty(model.Zip))
                    model.Zip = string.Empty;

                model.Email = model.Email.ToLower();


                var user = _OfficeManagerService.AddOfficeManager(model);


                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// display all user for perticular company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SelectOfficeManager()
        {
            int count = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;


                model.ApplicationOperation = ApplicationOperation.OfficeManager_List.ToString();
                model.ModuleName = ModuleName.OfficeManager;


                model.SessionID = User.SessionId;

                // model.CompanyID = companyId;
                model.UserID = User.UserId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var user = _OfficeManagerService.GetAllOfficeManagers(model, ref count);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //count = user.Count;
                }

                return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        /// <summary>
        /// select perticular user detail
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult OfficeManagerEdit(int userId)
        {
            try
            {
                var user = _OfficeManagerService.GetOfficeManagerById(userId);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update user detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateOfficeManager(OfficeManagerEntity model)
        {
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();

            if (string.IsNullOrEmpty(model.Street))
                model.Street = string.Empty;

            if (string.IsNullOrEmpty(model.State))
                model.State = string.Empty;

            if (string.IsNullOrEmpty(model.City))
                model.City = string.Empty;

            if (string.IsNullOrEmpty(model.Zip))
                model.Zip = string.Empty;

            //string token = (Guid.NewGuid().ToString("N"));
            //model.Token = token;

            try
            {
                //int superAdminCompanyId = 0;
                //  var getUserCompanyName = _OfficeManagerService.GetOfficeManagerById(model.OfficeManagerID);
                //if (getUserCompanyName.CompanyID == 0)
                //{
                //    model.ApplicationOperation = ApplicationOperation.EmployeeManagement_UpdateEmployee.ToString();
                //}
                //else
                //{
                //    model.ApplicationOperation = ApplicationOperation.UserManagement_UpdateUser.ToString();
                //}

                var officemanager = _OfficeManagerService.UpdateOfficeManager(model); // update user detail

                //if (officemanager)
                //{
                //    var userresult = _OfficeManagerService.GetOfficeManagerById(model.OfficeManagerID);
                //}

                // permission update
                //List<PermissionList> lstPermissionList = new List<PermissionList>();

                //var permissionList = System.Web.HttpContext.Current.Request.Form["permissionList"];
                //if (permissionList != null)
                //{
                //    lstPermissionList = JsonConvert.DeserializeObject<List<PermissionList>>(permissionList);

                //    //UpdateUserAccessPermissionMethod(lstPermissionList);
                //}

                return Json(officemanager, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult OfficeManagerDelete(int officeManagerID)
        {
            OfficeManagerEntity model = new OfficeManagerEntity();
            model.OfficeManagerID = officeManagerID;
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;

            //var getUserCompanyName = _OfficeManagerService.GetOfficeManagerById(model.OfficeManagerID);          
            model.ApplicationOperation = ApplicationOperation.OfficeManager_Delete.ToString();         

            model.SessionID = User.SessionId;
            model.UpdatedBy = User.UserId.ToString();
            try
            {
                var user = _OfficeManagerService.DeleteOfficeManager(model);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        [HttpPost]
        public JsonResult AlertForOfficeManager()
        {
            var messageList = _commonService.AlertMessgaeByModule(ModuleName.OfficeManager);
            return Json(messageList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// change password, update UserMaster table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
       
    }
}