﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Help;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Help;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    //[Authorize]
    public class HelpController : BaseController
    {
        private readonly IHelpService _IHelpService;
        private readonly ICommonService _ICommonService;
        private readonly IUserService _userService;

        public HelpController(IHelpService iHelpService, 
            ICommonService iCommonService,
            IUserService userService)
        {
            this._IHelpService = iHelpService;
            this._ICommonService = iCommonService;
            this._userService = userService;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// return help Index page
        /// Here user can add, edit, delete Help
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// return list of help from database
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            //To get the Id of Module
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.HelpManagement);
            ViewBag.ModuleId = module.ModuleId;

            var permission = new UserPermissionViewModel();
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            permission = _userService.CheckUserModulePermission(User.UserId, ModuleName.HelpManagement);

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            if (User.RoleId != 1)
            {
                if (!permission.HasViewPermission)
                {
                    return RedirectToAction("UnauthorizedAccess", "Common", null);
                }
            }

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.HelpManagement, "Help Management List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.HelpManagement_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View("List");
        }

        /// <summary>
        /// add new help to Database
        /// </summary>
        /// <param name="objhelp"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddHelpData(HelpEntity objhelp)
        {
            try
            {
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                objhelp.appAuditID = appAuditID;
                objhelp.ApplicationOperation = ApplicationOperation.HelpManagement_Add.ToString();
                objhelp.SessionID = User.SessionId;
                objhelp.CreatedBy = User.UserId.ToString();
                var documentdetails = _IHelpService.HelpInsert(objhelp);

                return Json(CommonMessage.Success.ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// get help data from database based on HelpId
        /// </summary>
        /// <param name="HelpId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditHelp(int HelpId)
        {
            try
            {
                var documentdetails = _IHelpService.GetHelpById(HelpId);
                return Json(documentdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update help data to database
        /// </summary>
        /// <param name="helpobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateHelp(HelpEntity helpobj)
        {
            try
            {
                helpobj.UpdatedBy = User.UserId.ToString();
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                helpobj.appAuditID = appAuditID;
                helpobj.ApplicationOperation = ApplicationOperation.HelpManagement_Update.ToString();
                helpobj.SessionID = User.SessionId;

                var helpdetails = _IHelpService.HelpUpdateService(helpobj);
                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="HelpId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteHelpData(int HelpId)
        {
            HelpEntity help = new HelpEntity();
            help.HelpId = HelpId;
            help.DeletedBy = User.UserId.ToString();
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            help.appAuditID = appAuditID;
            help.ApplicationOperation = ApplicationOperation.HelpManagement_Delete.ToString();
            help.SessionID = User.SessionId;

            try
            {
                var user = _IHelpService.DeleteHelpService(help);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// get help list from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllHelp(DataTableModel model)
        {
            try
            {
                int totalcount = 0;

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.HelpManagement_List.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var HelpData = _IHelpService.GetAllHelp(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //totalcount = HelpData.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = HelpData }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get module wise help list from database
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ModulewiseHelpList(int ModuleId)
        {
            ViewBag.Module = ModuleId;

            return View("ModulewiseHelpList");
        }

        /// <summary>
        /// get module wise help from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetModuleHelpAll(DataTableModel model)
        {
            try
            {
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.HelpManagement_List.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                int totalcount = 0;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);
                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                model.RoleID = User.RoleId;
                model.FlagValue = 0; // for draft and published

                var Helpdata = _IHelpService.GetAllModuleHelp(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Helpdata.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Helpdata }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get module wise help from database based on ModuleId
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetModulewiseHelp(int ModuleId)
        {
            DataTableModel model = new DataTableModel();
            try
            {
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.HelpManagement_HeaderHelp.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                int totalcount = 0;

                model.SearchTerm = string.Empty;

                model.ModuleId = ModuleId;

                model.PageNumber = 0;
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);
                //Get Sort columns value
                model.SortColumn = "Title";
                model.SortDirection = "ASC";

                model.RoleID = User.RoleId;
                model.FlagValue = 1; // for header help only published

                var Helpdata = _IHelpService.GetAllModuleHelp(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Helpdata.Count;
                }
                return Json(Helpdata, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}