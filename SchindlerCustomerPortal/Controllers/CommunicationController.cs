﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Communication;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class CommunicationController : BaseController
    {

        private readonly ICommunicationService _communicationService;
        private readonly IUserService _userService;

        public CommunicationController(ICommunicationService communicationService, IUserService userService)
        {
            this._communicationService = communicationService;
            this._userService = userService;
        }
        //
        // GET: /Communication/

        public ActionResult Communication(int id)
        {
            return View();
        }


        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllPost(PostEntity obj)
        {
            var data = _communicationService.GetAllPost_ThreadByProjectId(obj).ToList();
            List<PostDetailViewModel> lstPost = new List<PostDetailViewModel>();
            foreach (var item in data)
            {
                if (lstPost.Where(x => x.PostId == item.PostId).Count() == 0)
                {
                    PostDetailViewModel objdata = new PostDetailViewModel();
                    objdata.PostId = item.PostId;
                    objdata.PostTitle = item.PostTitle;
                    objdata.ThreadList = data.Where(x => x.PostId == item.PostId && x.ThreadId != 0).ToList();
                    objdata.ThreadCount = data.Where(x => x.PostId == item.PostId && x.ThreadId != 0).Count();
                    lstPost.Add(objdata);
                }
            }
            var page = obj.PageNo == null ? 1 : (int)obj.PageNo;
            var pageSize = obj.PageSize == null ? 10 : (int)obj.PageSize;
            var skip = pageSize * (page - 1);
            var totalcount = lstPost.Count();
            lstPost = lstPost.Skip(skip).Take(pageSize).ToList();
            PagedResult<PostDetailViewModel> lst = new PagedResult<PostDetailViewModel>();
            lst.Result = lstPost;
            lst.PageNo = page;
            lst.PageSize = pageSize;
            lst.TotalRecordCount = totalcount;

            return Json(new { data = lst, success = true, message = "success" });
        }

        public class CommunicationThread
        {
            public int ParentThreadId { get; set; }
            public int ParentPostid { get; set; }
            public int ThreadId { get; set; }
            public string Title { get; set; }
            public List<CommunicationThread> lstthread { get; set; }
        }

        public static void BuildTree(List<PostDetailViewModel> items)
        {
            items.ForEach(i => i.ThreadList = items.Where(ch => ch.ParentPostid == i.PostId).ToList());
        }

        public JsonResult AddPostByProject(PostEntity obj)
        {
            obj.appAuditID = 1;
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.ParentPostId = 0;
            obj.CreatedOn = DateTime.UtcNow;
            var result = _communicationService.AddPostByProject(obj);
            if (result > 0)
            {
                string Result = string.Empty;
                if(obj.NotifyToEmail!=null && obj.NotifyToEmail.Count()>0)
                {
                     Result = String.Join(";", obj.NotifyToEmail.Select(s => s.Email.ToString()));
                }
             
                string To = string.Empty;
                To += String.IsNullOrEmpty(To) ? Result : ";" + Result;
                string from = User.Email;
                string subject = obj.Title ?? string.Empty;
                string body = obj.Content ?? string.Empty;

                if (!string.IsNullOrEmpty(To))
                    SendEmail.Send(from, To, subject, body);
            }

            return Json(new { success = true, message = result > 0 ? CommonMessage.PostInsertedSuccess.ToString() : CommonMessage.FailedInsert.ToString() });
        }


        public JsonResult PostMessage(ThreadEntity obj)
        {
            obj.appAuditID = 1;
            obj.SessionID = User.SessionId;
            obj.CreatedBy = User.UserId.ToString();
            obj.CreatedOn = DateTime.UtcNow;
            obj.PostBy = User.UserId;
            obj.IsActive = true;
            string objnotify = string.Empty;
            if (obj.NotifyToEmail != null && obj.NotifyToEmail.Count() > 0)
            {
                objnotify = String.Join(";", obj.NotifyToEmail.Select(s => s.Email.ToString()));
            }

            obj.strNotifyToEmail = objnotify;

            if (_communicationService.AddThreadEntity(obj) > 0)
            {

                //Get Post created by id for reply on post email

                var createdbyid = _communicationService.GetPostById(obj.PostId).CreatedBy;
                int intStr;
                bool intResultTryParse = int.TryParse(createdbyid, out intStr);
                string To = string.Empty;
                if (intResultTryParse == true)
                {
                    To = _userService.GetUserById(intStr).Email ?? string.Empty;
                }
                else
                {

                }
                //Get Post created by id for reply on post email

                string Result = String.Join(";", obj.NotifyToEmail.Select(s => s.Email.ToString()));

                To += String.IsNullOrEmpty(To) ? Result : ";" + Result;

                string from = User.Email;
                string subject = obj.Title ?? string.Empty;
                string body = obj.Content ?? string.Empty;
                bool Supdate = false;

                if (!string.IsNullOrEmpty(To))
                    Supdate = SendEmail.Send(from, To, subject, body);

                //}
                return Json(new { success = true, message = obj.IsReply ? CommonMessage.PostReplyMessageSuccess.ToString() : CommonMessage.PostMessageSuccess.ToString() });
            }
            else
            {
                return Json(new { success = false, message = CommonMessage.FailedInsert.ToString() });
            }

        }


        [HttpGet]
        public JsonResult GetProjectUser(int projectid, string query)
        {
            List<UserEntity> lstuser = new List<UserEntity>();
          

            lstuser.Add(new UserEntity { UserID = 6, Email = "dzala@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 12, Email = "vmpatel@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 13, Email = "athakkar@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 14, Email = "arbhatt@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 21, Email = "vparmar@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 35, Email = "amodi@ismnet.com" });
            lstuser.Add(new UserEntity { UserID = 37, Email = "rbmehta@ismnet.com" });
            var lstdata = lstuser.Select(x => new { id = x.UserID, Email = x.Email }).ToList().Where(x => x.Email.Contains(query)).ToList();
            return Json(lstdata, JsonRequestBehavior.AllowGet);
        }
    }
}