﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Company;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class DashboardController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly ICompanyService _companyService;


        public DashboardController(IUserService userService,
            ICommonService commonService,
            ICompanyService companyService)
        {
            this._userService = userService;
            this._commonService = commonService;
            this._companyService = companyService;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// after successful login user redirect to index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                if (User != null)
                {
                    int userId = User.UserId;
                    int AuditID = 0;
                    IpAddress = StaticUtilities.getIP();
                    Clientbrowser = StaticUtilities.getClientbrowser();
                    ClientOS = StaticUtilities.getClientOS();
                    ServerIP = StaticUtilities.getServerIP();
                    ServerName = StaticUtilities.getServerName();

                    AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.DashBoard, "Dashboard Page", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.Dashboard.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

                    if (AppAuditID != 0)
                    {
                        HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                        AppAuditCookie.Value = Convert.ToString(AppAuditID);
                        Response.Cookies.Add(AppAuditCookie);
                    }

                    ViewBag.Designation = _commonService.designationList(); // Designation List

                    ModuleEntity module = _commonService.ModuleByName(ModuleName.DashBoard);
                    ViewBag.ModuleId = module.ModuleId;

                    var permission = new UserPermissionViewModel();
                    permission = _userService.CheckUserModulePermission(userId, ModuleName.UserManagement);

                    if (permission == null)
                    {
                        permission = new UserPermissionViewModel();
                    }

                    return View(permission);
                }
                else
                {
                    return RedirectToAction("UserLogin", "Login", null);
                }
            }
            catch (Exception ex)
            {
                SchindlerCustomerPortal.Common.StaticUtilities.ErrorLogAdditionalinfoEntity errorLogAdditionalinfoEntity = new SchindlerCustomerPortal.Common.StaticUtilities.ErrorLogAdditionalinfoEntity();
                errorLogAdditionalinfoEntity.AdditionalInfo = "Dashbaord - Index";
                errorLogAdditionalinfoEntity.IpAddress = Request.UserHostAddress;
                errorLogAdditionalinfoEntity.WindowUserName = Request.LogonUserIdentity.Name.ToString();
                errorLogAdditionalinfoEntity.AppUserName = User.UserId.ToString();
                errorLogAdditionalinfoEntity.URL = Request.Url.ToString();
                SchindlerCustomerPortal.Common.StaticUtilities.WriteToErrorLogXML(ex, errorLogAdditionalinfoEntity);
                return null;
            }

            finally
            {

            }
           
        }
	}
}