﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Company;
using SchindlerCustomerPortal.Services.Project;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class MajorProjectsController : BaseController
    {
        private readonly IProjectMasterService _projectMasterService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly ICompanyService _companyService;
        // GET: MajorProjects

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        public MajorProjectsController(IUserService userService,
            ICommonService commonService,
            ICompanyService companyService,
            IProjectMasterService projectmasterService)
        {
            this._projectMasterService = projectmasterService;
            this._userService = userService;
            this._commonService = commonService;
            this._companyService = companyService;
        }

        public ActionResult Add()
        {
            return View("Add");
        }

        public ActionResult Edit(int? id)
        {
            ViewBag.Editid = id;
            return View("Edit");
        }

        public ActionResult List()
        {
            return View("List");
        }
     
        public ActionResult Index()
        {
            ModuleEntity module = _commonService.ModuleByName("MajorProjects");
            //ViewBag.ModuleId = module.ModuleId;
            int auditID = 0;

            auditID = GetAuditID(User.CompanyID);

            if (auditID == 0)
            {
                //Fetch the Cookie using its Key.
                HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                //Set the Expiry date to past date.
                nameCookie.Expires = DateTime.Now.AddDays(-1);
                //Update the Cookie in Browser.
                Response.Cookies.Add(nameCookie);

                ControllerContext.HttpContext.Response.Redirect("/Login/UserLogin?returnUrl=/MajorProjects/Index");
                //return RedirectToAction("UnauthorizedAccess", "Common", null);
            }
            else
            {
                ViewBag.AppAuditID = auditID;
                ViewBag.SessionID = User.SessionId;
                ViewBag.CompanyID = User.CompanyID;
                return View("MajorProjects");
            }
            //   }
            return View("MajorProjects");
        }

        private int GetAuditID(int companyId)
        {
            int userId = User.UserId;
            var permission = new UserPermissionViewModel();
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            ViewBag.companyId = companyId;
            if (companyId != 0)
            {
                AppAuditID = StaticUtilities.ApplicationAuditRecord("MajorProjects", "Calendar", Convert.ToString(User.UserId), User.RoleId, "Calendar View", User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

                var companyName = _companyService.GetCompanyByID(companyId).CompanyName;
                ViewBag.companyName = companyName;
                ViewBag.userManagement = "Calendar";

            }
            else
            {
                AppAuditID = StaticUtilities.ApplicationAuditRecord("MajorProjects", "Calendar", Convert.ToString(User.UserId), User.RoleId, "Calendar View", User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

                ViewBag.companyName = "Schindler";
                ViewBag.userManagement = "Calendar";

            }
            ViewBag.Designation = _commonService.designationList();

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }
            return AppAuditID;
        }
    }
}