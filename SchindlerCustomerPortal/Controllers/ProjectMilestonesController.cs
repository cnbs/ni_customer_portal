﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.ProjectMilestone;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ProjectMilestonesController : BaseController
    {
        private readonly ICommonService _ICommonService;
        private readonly IProjectMilestoneService _IProjectMilestoneService;
        private readonly IUserService _userService;

        public ProjectMilestonesController(ICommonService iCommonService, IProjectMilestoneService iProjectMilestoneservice, IUserService userService)
        {
            this._ICommonService = iCommonService;
            this._IProjectMilestoneService = iProjectMilestoneservice;
            this._userService = userService;
        }

        /// <summary>
        /// return index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// return list page based on projectNo or jobNo
        /// </summary>
        /// <param name="ProjectNo"></param>
        /// <param name="JobId"></param>
        /// <returns></returns>
        public ActionResult List(int ProjectNo, int? JobId)
        {
            ViewBag.JobId = JobId;
            ViewBag.ProjectNo = ProjectNo;
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.ProjectDocumentManagement);
            ViewBag.ModuleId = module.ModuleId;

            return View("List");
        }

        /// <summary>
        /// get all milestonerecords from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetMilestoneAll(DataTableModel model)
    {
            if (string.IsNullOrEmpty(model.ProjectNumber))
                model.ProjectNumber = string.Empty;
            
            try
            {
                int totalcount = 0;
                var Milestones = new List<ProjectMilestoneEntity>();
                // SearchInfoCompanyViewModel model = new SearchInfoCompanyViewModel();
                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                if (Request.Form.GetValues("order[0][column]") == null)
                {
                    model.SortColumn = "";
                    model.SortDirection = "";
                }
                else
                {
                    model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                }
                //if (model.CompanyID == 0)
                //{

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ProjectManagement_ProjectMilestoneList.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;


                #endregion

                Milestones = _IProjectMilestoneService.GetAllMilestone(model, ref totalcount);
                //}
                //else
                //{
                // Resources = _IResourceUploadService.GetAllResourcesByCompanyId(model, ref totalcount);
                //}



                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Milestones.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Milestones }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// add new milestone record to database
        /// </summary>
        /// <param name="milestoneobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddMilestone(ProjectMilestoneEntity milestoneobj)
        {
         
            try
            {
                milestoneobj.PublishedBy = User.FirstName + " " + User.LastName;
            

                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                milestoneobj.appAuditID = appAuditID;
                milestoneobj.ApplicationOperation = ApplicationOperation.ProjectManagement_AddProjectMilestone.ToString();
                milestoneobj.SessionID = User.SessionId;
                milestoneobj.UserID = User.UserId;

                if (string.IsNullOrEmpty(milestoneobj.Note))
                {
                    milestoneobj.Note = string.Empty;
                }

                #endregion


                var milestonedetails = _IProjectMilestoneService.MilestoneInsert(milestoneobj);

                return Json(milestonedetails.Exist);

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// get perticular milestone detail based on milestoneId
        /// </summary>
        /// <param name="mileId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditMilestone(int mileId)
        {
            try
            {
                var milestonedetails = _IProjectMilestoneService.GetMileById(mileId);
                return Json(milestonedetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update milestone record to database
        /// </summary>
        /// <param name="milestoneobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateMilestone(ProjectMilestoneEntity milestoneobj)
        {
            try
            {
                milestoneobj.UpdatedBy = User.FirstName + " " + User.LastName;
             


                #region "Audit Trail"
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                milestoneobj.appAuditID = appAuditID;
                milestoneobj.ApplicationOperation = ApplicationOperation.ProjectManagement_UpdateProjectMilestone.ToString();
                milestoneobj.SessionID = User.SessionId;
                milestoneobj.UserID = User.UserId;


                #endregion

                if (!string.IsNullOrEmpty(milestoneobj.NewNote))
                {
                    milestoneobj.Note = DateTime.Now.ToString("MM/dd/yyyy") + " " + milestoneobj.UpdatedBy + "\n" + milestoneobj.Note;
                }

                if (string.IsNullOrEmpty(milestoneobj.Note))
                {
                    milestoneobj.Note = "";
                }

                var documentdetails = _IProjectMilestoneService.MilestoneUpdateService(milestoneobj);

                #region Email Send

                var mailNotificationUserList = _IProjectMilestoneService.GetProjectAssignedUserListForMilestoneEmail(milestoneobj.ProjectNo);

                if (milestoneobj.StatusName!=milestoneobj.PreviousStatusName)
                {
                    foreach (var item in mailNotificationUserList)
                    {

                        string Subject = "myschindlerprojects – [[ProjectNoProjectNameBankNoBankDesc]] status has been updated to [[StatusName]].";
                        string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        //string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];
                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", milestoneobj.MilestoneName);
                        dic.Add("ProjectNo", milestoneobj.ProjectNo + (milestoneobj.JobNo != null ? " - " + milestoneobj.JobNo : ""));
                        dic.Add("StatusName", milestoneobj.StatusName);
                        dic.Add("PreviousStatusName", milestoneobj.PreviousStatusName);
                        dic.Add("UserName", User.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (milestoneobj.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName + " and Bank " + milestoneobj.JobDesc + " [" + milestoneobj.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);

                        Subject = Subject.Replace("[[ProjectNoProjectNameBankNoBankDesc]]", ProjectNoProjectNameBankNoBankDesc).Replace("[[StatusName]]", milestoneobj.StatusName);

                        dic.Add("MessageURL", headurl);

                        Sbody = StaticUtilities.getEmailBody("ProjectMilestone", "MilestoneStatusChangeNew.html", dic);

                        SendEmail.Send(CommonConfiguration.NoReplyEmail, item.Email, Subject, Sbody);

                    }

                    #region Push Notification
                    foreach (var item in mailNotificationUserList)
                    {

                        
                        string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        //string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];
                      

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        //dic.Add("PersonName", CommonConfiguration.MilestoneStatusUpdateEmailToPerson);
                        dic.Add("PersonName", item.FirstName);
                        dic.Add("MilestoneName", milestoneobj.MilestoneName);
                        dic.Add("ProjectNo", milestoneobj.ProjectNo + (milestoneobj.JobNo != null ? " - " + milestoneobj.JobNo : ""));
                        dic.Add("StatusName", milestoneobj.StatusName);
                        dic.Add("PreviousStatusName", milestoneobj.PreviousStatusName);
                        dic.Add("UserName", User.FirstName);
                        string ProjectNoProjectNameBankNoBankDesc = "";
                        if (milestoneobj.JobNo != null)
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName + " and Bank " + milestoneobj.JobDesc + " [" + milestoneobj.JobNo + "]";
                        }
                        else
                        {
                            ProjectNoProjectNameBankNoBankDesc = "Project # " + milestoneobj.ProjectNo + " - " + milestoneobj.ProjectName;
                        }

                        dic.Add("ProjectNoProjectNameBankNoBankDesc", ProjectNoProjectNameBankNoBankDesc);
                        
                        dic.Add("MessageURL", headurl);
                        
                        string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "Milestone Status Changed";
                                string NotificationBody = StaticUtilities.getNotificationBody("ProjectMilestone", "MilestoneStatusChangeNew.txt", dic);

                                try
                                {
                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if(success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {
                                       
                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }


                                //Insert into NotificationLogId table

                                data.CompanyID = User.CompanyID;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(item.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;
                                data.CreatedBy = User.FirstName + " " + User.LastName;
                                data.DeviceType = item.DeviceType;
                                var Notify = _userService.AddNotification(data);

                            }
                        }

                        
                    }
                    #endregion
                }



                #endregion


                return Json(documentdetails.Exist);

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// soft delete from database
        /// </summary>
        /// <param name="MileID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteMilestone(int MileID)
        {
            ProjectMilestoneEntity milestoneobj = new ProjectMilestoneEntity();
            //model.UserID = userID;
            milestoneobj.MilestoneId = MileID;
            milestoneobj.DeletedBy = User.UserId.ToString();
           

            #region "Audit Trail"
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            milestoneobj.appAuditID = appAuditID;
            milestoneobj.ApplicationOperation = ApplicationOperation.ProjectManagement_DeleteProjectMilestone.ToString();
            milestoneobj.SessionID = User.SessionId;
            milestoneobj.UserID = User.UserId;


            #endregion


            try
            {
                var user = _IProjectMilestoneService.DeleteMilestoneService(milestoneobj);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
	}
}