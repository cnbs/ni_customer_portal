﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class MapViewController : Controller
    {
        // GET: MapView
        private readonly ICommonService _commonService;

        public MapViewController(ICommonService commonService)
        {
            this._commonService = commonService;
        }

        public ActionResult Index()
        {
            ModuleEntity module = _commonService.ModuleByName(ModuleName.MapView);
            ViewBag.ModuleId = module.ModuleId;

            ViewBag.MapKey = "https://maps.googleapis.com/maps/api/js?sensor=false&key="+ 
                              WebConfigurationManager.AppSettings["GoogleLocationKey"].ToString();
            return View("MapView");
        }
    }
}