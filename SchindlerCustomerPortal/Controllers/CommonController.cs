﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.User;
using SchindlerCustomerPortal.Models.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    //[UserPermissionAuthorizeAttribute]
    //[UserPermissionAuthorizeAttribute]
    public class CommonController : BaseController
    {

        private readonly ICommonService _commonService;
        private readonly IUserService _userService;

        public CommonController(ICommonService commonService,
            IUserService userService)
        {
            this._commonService = commonService;
            this._userService = userService;
            ViewBag.DesignationHeader = _commonService.designationList();
        }

        public CommonController()
        {
        }

        /// <summary>
        /// return list of country from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult BindCountry()
        {
            try
            {
                var countryList = _commonService.countryList();
                return Json(countryList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult BindDesignationList()
        {
            try
            {
                var designationList = _commonService.designationList();
                return Json(designationList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return status list from datatbase
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>

        public JsonResult BindStatusList(string module)
        {
            try
            {
                var DocStatusList = _commonService.StatusList(module);
                return Json(DocStatusList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of state which country selected by user
        /// </summary>
        /// <param name="countryid"></param>
        /// <returns></returns>
        //[UserPermissionAuthorizeAttribute]
        [AllowAnonymous]
        [HttpGet]
        public JsonResult BindState(int countryid)
        {
            try
            {
                var stateList = _commonService.stateList(countryid);
                return Json(stateList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of city which state selected by user
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public JsonResult BindCity(int countryId, string stateCode)
        {
            try
            {
                var cityList = _commonService.cityList(countryId, stateCode);
                return Json(cityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// suggestive search for city. autocomplete for city name
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="stateCode"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult BindCityAutocomplete(int countryId, string stateCode, string searchTerm)
        {
            try
            {
                var cityList = _commonService.cityListAutoComplete(countryId, stateCode, searchTerm);
                return Json(cityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// suggestive search for city. autocomplete for city name
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="countryId"></param>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public JsonResult BindCityAutoCompleteAddUser(string searchTerm, int countryId, string stateCode)
        {
            try
            {
                var cityList = _commonService.cityListForAutoComplete(searchTerm, countryId, stateCode);
                return Json(cityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of document type
        /// </summary>
        /// <returns></returns>
        public JsonResult BindDocumentType()
        {
            try
            {
                var doctypeList = _commonService.DoctypeList();
                return Json(doctypeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of milestone type
        /// </summary>
        /// <returns></returns>
        public JsonResult BindMilestoneType()
        {
            try
            {
                var miletypeList = _commonService.Miletypelist();
                return Json(miletypeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of document type for resource center
        /// </summary>
        /// <returns></returns>
        public JsonResult BindDocumentTypeForResource()
        {
            try
            {
                var doctypeList = _commonService.ResourceDoctypeList();
                return Json(doctypeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// generate dynamic menu based on user permission
        /// </summary>
        /// <param name="parentCategoryId"></param>
        /// <returns></returns>
        public List<UserPermissionViewModel> GetAllMenuByParentCategoryId(int parentCategoryId)
        {

            int UserID = User.UserId;
            var result = new List<UserPermissionViewModel>();

            var data = _userService.GetUserModulePermissionForMenu(UserID, parentCategoryId);

            foreach (var item in data)
            {
                var menuModel = new UserPermissionViewModel
                {
                    UserID = item.UserID,
                    ModuleId = item.ModuleId,
                    //ModuleName = item.ModuleName,
                    ModuleDisplayName = item.ModuleDisplayName,
                    ModuleURL = item.ModuleURL,
                    ModulePermissionName = item.ModulePermissionName,
                    HasViewPermission = item.HasViewPermission,
                    HasAddPermission = item.HasAddPermission,
                    HasEditPermission = item.HasEditPermission,
                    HasDeletePermission = item.HasDeletePermission,
                    HasPrintPermission = item.HasPrintPermission,
                    SequenceNO = item.SequenceNO,
                    Restricted = item.Restricted,
                    VisibleOnMenu = item.VisibleOnMenu
                };

                if (StripHTML(menuModel.ModuleDisplayName) == "User Management")
                {
                    menuModel.ModuleURL = menuModel.ModuleURL + User.CompanyID;
                }

                var subMenus = GetAllMenuByParentCategoryId(item.ModuleId);
                menuModel.SubMenus.AddRange(subMenus);

                result.Add(menuModel);
            }
            //modulePermission.AddRange(model);

            return result;
        }

        /// <summary>
        /// generate dynamic menu based on user permission
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult HeaderLinks()
        {
            var menuModel = new List<UserPermissionViewModel>();

            // 0 is for root level category
            menuModel = GetAllMenuByParentCategoryId(0);

            return PartialView(menuModel);
        }

        /// <summary>
        /// check company name for uniqueness
        /// </summary>
        /// <param name="companyname"></param>
        /// <returns></returns>
        public JsonResult IsCompanyNameExist(string companyname)
        {
            int result = 0;

            if (!string.IsNullOrEmpty(companyname))
            {
                result = _userService.CheckCompanyNameService(companyname).CheckCompany;
            }

            return Json(new { isexist = result == 1 ? true : false, message = Common.CommonMessage.AlreadyExistsCompany.ToString() }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// return list of module
        /// </summary>
        /// <returns></returns>
        public JsonResult BindModule()
        {
            try
            {
                var moduleList = _commonService.moduleList();
                return Json(moduleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of predecessor for miltstone
        /// </summary>
        /// <param name="JobId"></param>
        /// <returns></returns>
        public JsonResult BindPredecessor(int JobId)
        {
            try
            {
                var milestoneList = _commonService.milestoneListByJob(JobId);
                return Json(milestoneList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return list of project status
        /// </summary>
        /// <returns></returns>
        public JsonResult BindProjectStatus()
        {
            try
            {
                var statusList = _commonService.projectstatusList();
                return Json(statusList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// it display unauthorized access page when user don't have permission to access module
        /// </summary>
        /// <returns></returns>
        public ActionResult UnauthorizedAccess()
        {
            return View();
        }

        /// <summary>
        /// return company name for autocomplete textbox
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public JsonResult BindCompany(string searchTerm)
        {
            try
            {
                var companyList = _commonService.companyList(searchTerm);
                return Json(companyList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return project list for perticuler company for autocomplete textbox
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public JsonResult BindProjectOfCompany(string searchTerm, int CompanyId)
        {
            try
            {
                int RoleId = User.RoleId;
                var companyList = _commonService.GetProjectListbyCompanyId(searchTerm, CompanyId, RoleId);
                return Json(companyList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return job list fro autocomplete textbox based on project no
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="ProjectNo"></param>
        /// <returns></returns>
        public JsonResult BindJobOfProject(string searchTerm, string ProjectNo)
        {
            try
            {
                var companyList = _commonService.GetJobListbyProjectNo(searchTerm, ProjectNo);
                return Json(companyList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// return gc superintendent based on company id
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public JsonResult GetGCSuperintendent(int CompanyId)
        {
            List<LookUpcls> lst = new List<LookUpcls>();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// return alert message for my profile module
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AlertForMyProfile()
        {
            var messageList = _commonService.AlertMessgaeByModule(ModuleName.MyProfile);
            return Json(messageList, JsonRequestBehavior.AllowGet);
        }

        public class LookUpcls
        {
            public int Id { get; set; }
            public string DisplayName { get; set; }

        }

        /// <summary>
        /// return string value from html string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        /// <summary>
        /// return common error page for application
        /// </summary>
        /// <returns></returns>
        
        public ActionResult ErrorPage()
        {
            
            return View();
        }

        [HandleError]
        public ActionResult Error404Page()
        {

            return View();
        }

    }

}




