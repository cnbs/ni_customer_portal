﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Services.Company;

namespace SchindlerCustomerPortal.Controllers
{
    public class HomeController : Controller
    {

        private readonly ICompanyService _companyService;

        public HomeController(ICompanyService companyService)
        {
            this._companyService = companyService;
        }

        public ActionResult Index()
        {
       
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}