﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    //TODO: remove this code
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class DocumentByTypeController : BaseController
    {
        //
        private readonly ICommonService _ICommonService;
        private readonly IDocumentService _IDocumentService;

        public DocumentByTypeController(IDocumentService iDocumentService, ICommonService iCommonService)
        {
            this._IDocumentService = iDocumentService;
            this._ICommonService = iCommonService;
        }
        
        /// <summary>
        /// return document list page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// return document list page
        /// </summary>
        /// <param name="Projectid"></param>
        /// <returns></returns>
        public ActionResult DocumentList(int Projectid)
        {
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.DocManagement);
            ViewBag.ModuleId = module.ModuleId;
            //ViewBag.DocumentType = DocType;
            ViewBag.ProjectId = Projectid;
            
            return View("DocumentList");
        }

        /// <summary>
        /// get all documents from database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetDocumentAll(DataTableModel model)
        {

            try
            {
                int totalcount = 0;
                var Documents = new List<DocumentEntity>();
                // SearchInfoCompanyViewModel model = new SearchInfoCompanyViewModel();
                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                if (model.CompanyID == 0)
                {
                    Documents = _IDocumentService.GetAllDocumentByType(model, ref totalcount);
                }
                else
                {
                    Documents = _IDocumentService.GetAllDocumentBycompany_ByType(model, ref totalcount);
                }

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Documents.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Documents }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// download document from server folder
        /// </summary>
        /// <param name="DocPath"></param>
        /// <returns></returns>
        public ActionResult DownloadDocument(string DocPath)
        {
            byte[] fileBytes;
            try
            {

                DocPath = System.Web.HttpUtility.UrlDecode(DocPath);
                if (System.IO.File.Exists(Server.MapPath(DocPath)))
                {
                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(DocPath));
                    string fileName = Path.GetFileName(Server.MapPath(DocPath));
                    string RealName = fileName.Substring(fileName.IndexOf("_") + 1);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, RealName);
                }
                else
                {
                    return RedirectToAction("List");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                fileBytes = null;
                GC.Collect();
            }


        }
	}
}