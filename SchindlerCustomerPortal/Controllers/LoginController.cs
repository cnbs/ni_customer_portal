﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models.Login;
using Newtonsoft.Json;
using System.Web.Security;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Models.Authentication;
using System.Net;
using System.Configuration;
//using Recaptcha.Web.Mvc;
//using Recaptcha.Web;
//using reCaptcha;
using System.Text;
using System.Drawing;
using System.IO;
using SchindlerCustomerPortal.Services.Announcement;

namespace SchindlerCustomerPortal.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly IAnnouncementService _announcementService;

        public LoginController(IUserService userService,
            ICommonService commonService, IAnnouncementService announcementService)
        {
            this._userService = userService;
            this._commonService = commonService;
            this._announcementService = announcementService;
        }

        public LoginController(IUserService userService,
         ICommonService commonService)
        {
            this._userService = userService;
            this._commonService = commonService;            
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;
        string AnnouncementCookieName = string.Empty;

        public ActionResult Index()
        {
            return View();
        }
      

        /// <summary>
        /// it display login page.
        /// </summary>
        /// <param name="wasRedirected"></param>
        /// <param name="Errormsg"></param>
        /// <param name="returnUrl">for redirection to the page where user came from</param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult UserLogin(bool? wasRedirected, string Errormsg = null, string returnUrl = null)
        {
            if (wasRedirected == true)
            {
                ViewBag.Redirect = "true";
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            if (!string.IsNullOrEmpty(Errormsg))
            {
                ViewBag.Redirect = Errormsg;
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            ViewBag.ReturnUrl = returnUrl;

            if (Convert.ToString(TempData["msgdisplay"]) != null && Convert.ToString(TempData["msgdisplay"]) != "")
            {
                TempData["msgdisplay"] = TempData["msgdisplay"];
            }

            // clear existing user cookie
            if (User != null)
            {
                try
                {
                    if (User.GetType().Module.Name == "SchindlerCustomerPortal.dll")
                    {
                        //Fetch the Cookie using its Key.
                        HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                        //Set the Expiry date to past date.
                        nameCookie.Expires = DateTime.Now.AddDays(-1);
                        //Update the Cookie in Browser.
                        Response.Cookies.Add(nameCookie);
                    }
                }
                catch(Exception ex)
                {
                    // to avod this error// this was created to manage browser tabs 
                }
                
            }

            return View();
        }

        /// <summary>
        /// validate user credentials. if valid then redirect to dashboard page.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [CustomAuthorize]
        [HttpPost]
        public ActionResult UserLogin(UserEntity user)
        {
            var actionreturn = RedirectToAction("");
            if (Convert.ToString(user.Email).Trim() == null)
            {
                ModelState.AddModelError("Email", "Email is required");
            }

            if (Convert.ToString(user.Password).Trim() == null)
            {
                ModelState.AddModelError("Password", "Password is required");
            }

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(user.Email))
                {
                    user.Email = user.Email.Trim();
                }

                if (!string.IsNullOrEmpty(user.Password))
                {
                    user.Password = user.Password.Trim();
                }

                var userresult = _userService.Find(Convert.ToString(user.Email.ToLower()));
                if (userresult != null)
                {
                    if (userresult.AccountStatus == 1)
                    {
                        var IsRememberme = user.Remember == "on" ? true : false;
                        string NewPwd = EncryptionService.CreatePasswordHash(user.Password, userresult.PasswordSalt, "");

                        if (NewPwd == userresult.Password)
                        {

                            //clear any other tickets that are already in the response
                            Response.Cookies.Clear();

                            #region Login FormAthentication
                            CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                            serializeModel.UserId = userresult.UserID;
                            //  serializeModel.UserName = userresult.Username;
                            serializeModel.FirstName = userresult.FirstName;
                            serializeModel.LastName = userresult.LastName;
                            // serializeModel.roles = roles;
                            serializeModel.Email = userresult.Email;
                            serializeModel.RoleId = userresult.RoleID;
                            serializeModel.SessionID = Session.SessionID;
                            serializeModel.CompanyID = userresult.CompanyID;
                            serializeModel.ProfilePic = CommonConfiguration.userProfilePicPath + userresult.ProfilePic;
                            serializeModel.CompanyName = userresult.CompanyName;

                            string userData = JsonConvert.SerializeObject(serializeModel);
                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            1,
                            userresult.Email,
                            DateTime.Now,
                            DateTime.Now.AddHours(8),
                            IsRememberme, //pass here true, if you want to implement remember me functionality
                            userData,
                            "/");

                            string encTicket = FormsAuthentication.Encrypt(authTicket);
                            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                            faCookie.Expires = authTicket.Expiration;
                            Response.Cookies.Add(faCookie);

                            //Cookie for Announcement Flag
                            AnnouncementCookieName = "annflagCookie";
                            HttpCookie annflagCookie = new HttpCookie(AnnouncementCookieName.ToString().Trim(),"0");
                            annflagCookie.Expires = authTicket.Expiration;
                            Response.Cookies.Add(annflagCookie);

                            #endregion

                            if (string.IsNullOrEmpty(user.ReturnURL))
                            {
                                return RedirectToAction("Index", "Dashboard");
                            }
                            else
                            {
                                // redirect to return url
                                return Redirect(user.ReturnURL);
                            }

                        }
                        else
                        {
                            ModelState.AddModelError("", "Invalid LoginID or Password");
                            return RedirectToAction("UserLogin", new { wasRedirected = true });
                        }
                    }
                    else if (userresult.AccountStatus == 4)
                    {
                        //ViewBag.RegisteredMsg = CommonMessage.Registered.ToString(); 

                        //return View();
                        ModelState.AddModelError("", "Your account is not Verified.");
                        return RedirectToAction("UserLogin", new { Errormsg = "Your account is not Verified." });

                    }
                    else if (userresult.AccountStatus == 17)
                    {
                        //ViewBag.RegisteredMsg = CommonMessage.Registered.ToString(); 

                        //return View();
                        ModelState.AddModelError("", "Your account is not active.");
                        return RedirectToAction("UserLogin", new { Errormsg = "Your account is not active." });

                    }
                    //if (user.Remember == "on")
                    //{

                    //    Response.Cookies["SchindlerUserName"].Expires = DateTime.Now.AddMonths(1);
                    //    Response.Cookies["SchindlerPassword"].Expires = DateTime.Now.AddMonths(1);
                    //    Response.Cookies["SchindlerUserName"].Value = user.Email.Trim();
                    //    Response.Cookies["SchindlerPassword"].Value = user.Password.Trim();

                    //}

                    //  if (!string.IsNullOrEmpty(Convert.ToString(Session["SessionID"])))
                    // {
                    //   //  Session["SessionID"] = null;
                    //   //  Session.Abandon();
                    //     Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    // }
                    //// Session["SessionId"] = Session.SessionID;



                }
                else
                {
                    ModelState.AddModelError("", "Invalid LoginID or Password");
                    return RedirectToAction("UserLogin", new { wasRedirected = true });
                }

            }
            return View();
        }

        /// <summary>
        /// email send method
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="SFrom"></param>
        /// <param name="SuperAdmin"></param>
        /// <returns></returns>
        private bool SendVerificationEmailToUser(int userid, string email, string subject, string SFrom, string SuperAdmin)
        {
            var flag = false;

            string token = (Guid.NewGuid().ToString("N"));


            int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
            _userService.SetTokenService(token, userid, ExpToken);

            string ActivationUrl = string.Empty;

            //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
            string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

            ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

            string Sbody = null;

            Sbody += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi," + "<br/><br/>";
            Sbody += "You have successfully registered on Schindler Customer Portal<br/><br/>";
            Sbody += "Please  <a href=" + ActivationUrl + ">" + ActivationUrl + "</a>  to verify your email<br/><br/>";
            Sbody += "Sincerely,<br/>The Schindler Support Team</p>";
            Sbody += "</div></body></html>";

            DataSet Stempds = new DataSet();
            flag = SendEmail.Send(SFrom, email, subject, Sbody);

            return flag;

        }

        private string GetRandomText()
        {
            StringBuilder randomText = new StringBuilder();
            string alphabets = "012345679ACEFGHKLMNPRSWXZabcdefghijkhlmnopqrstuvwxyz";
            Random r = new Random();
            for (int j = 0; j <= 5; j++)
            {
                randomText.Append(alphabets[r.Next(alphabets.Length)]);
            }
            return randomText.ToString();
        }
        public FileResult GetCaptchaImage(string val)
        {
            string text =val;

            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            Font font = new Font("Arial", 15);
            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width + 40, (int)textSize.Height + 20);
            drawing = Graphics.FromImage(img);

            Color backColor = Color.SeaShell;
            Color textColor = Color.Red;
            //paint the background
            drawing.Clear(backColor);

            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);

            drawing.DrawString(text, font, textBrush, 20, 10);

            drawing.Save();

            font.Dispose();
            textBrush.Dispose();
            drawing.Dispose();

            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            img.Dispose();

            return File(ms.ToArray(), "image/png");
        }
        /// <summary>
        /// display user registration page
        /// </summary>
        /// <returns></returns>
        public ActionResult UserRegistration()
        {
            int AuditID = 0;
            int UserId = -1; // for audit trial record for new user
            int RoleId = -1; // for audit trial record for new user
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();


            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.Register, "Register Page", Convert.ToString(UserId), RoleId, ApplicationOperation.RegisterPage.ToString(), Session.SessionID, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            //if (AppAuditID != 0)
            //{
            //    HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
            //    AppAuditCookie.Value = Convert.ToString(AppAuditID);
            //    Response.Cookies.Add(AppAuditCookie);
            //}

            ViewBag.CountryList = new SelectList(_commonService.countryList(), "CountryId", "CountryName");
            ViewBag.Captcha= GetRandomText();

            if (TempData["msgdisplay"] != null)
            {
                ViewBag.Redirect = "true";
            }

            return View();
        }

        /// <summary>
        /// insert newly register data to database. Verification email sent to user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserRegistration(UserEntity user)
        {
            int superAdminCompanyId = 0;
            ViewBag.recaptchaerror = "";

            //string EncodedResponse = Request.Form["g-Recaptcha-Response"];
            //bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "True" ? true : false);
            //if (!IsCaptchaValid)
            //{
            //    ViewBag.recaptchaerror = "Captcha Check box must be selected.";
            //    return View(user);
            //}
            string clientCaptcha = Request.Form["clientCaptcha"].ToString();
            string serverCaptcha = Request.Form["InvalidCaptcha"].ToString();

            if (!clientCaptcha.Equals(serverCaptcha))
            {
                ViewBag.ShowCAPTCHA = serverCaptcha;
                ViewBag.Captcha = serverCaptcha;
                ViewBag.CaptchaError = "Sorry, please write exact text as written above.";
                return View(user);
            }

            if (!string.IsNullOrEmpty(user.CompanyName))
            {
                CompanyEntity company = _userService.CheckCompanyNameServiceNew(user.CompanyName);
                int IsCompany = company.CheckCompany;
                string CompanyName = "Schindler"; // for schindler
                if (IsCompany == 1) //Company Available
                {
                    user.CompanyID = company.CompanyID;

                    user.RoleID = Convert.ToInt32(UserRole.Customer);
                }
                else
                {
                    //if (user.CompanyName.ToLower() == CompanyName.ToLower())
                    //{
                    //    user.RoleID = Convert.ToInt32(UserRole.Employee);
                    //    user.CompanyID = 0;
                    //}
                    //else
                    //{
                        user.RoleID = Convert.ToInt32(UserRole.Customer);
                        user.CompanyID = -1;
                    //}
                }
            }

            user.Email = user.Email.ToLower();

            user.AccountStatus = Convert.ToInt32(Status.Pending);
            user.DesignationId = 4;
            user.ProfilePic = "no_images.png";

            //added by Arpit patel(30-Mar-2018)
            if (!string.IsNullOrEmpty(user.StateText))
            {
                var address = "";
                address = Convert.ToString(user.City) + " " + Convert.ToString(user.StateText) + " " + Convert.ToString(user.PostalCode);

                Tuple<double, double> abc = _commonService.GenerateLatLong(address);
                user.Latitude = abc.Item1;
                user.Longitude = abc.Item2;
            }

            UserEntity userResult = _userService.RegisterUserInsert(user);

            string SuperAdmin = System.Configuration.ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();
            string SuperAdminName = System.Configuration.ConfigurationManager.AppSettings["SuperAdminName"].ToString();

            if (userResult.Done == "1") //To check record inserted or not
            {
                if (userResult.CompanyID == -1)
                {
                    string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                    #region Email Send

                    string Subject = "myschindlerprojects – confirm your email";

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                    _userService.SetTokenService(token, userResult.UserID, ExpToken);

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", userResult.FirstName);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "VerifyAccountNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, userResult.Email, Subject, Sbody);

                    #endregion

                    bool Supdate = true; // check condition

                    if (Supdate == true)
                    {
                        TempData["msgdisplay"] = "User registered successfully";
                    }
                    else
                    {
                        TempData["msgdisplay"] = "Something wrong, Please try again!";
                    }
                }
                #region companyId = 0 for schindler
                else if (userResult.CompanyID == 0)
                {
                    string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                    #region Email Send

                    //var userResult = _userService.Find(model.Email);

                    string Subject = "myschindlerprojects – confirm your email";

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                    _userService.SetTokenService(token, userResult.UserID, ExpToken);

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", userResult.FirstName);
                    //dic.Add("UserName", userResult.FirstName);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "VerifyAccountNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, userResult.Email, Subject, Sbody);





                    #endregion

                    bool update = true;

                    if (update == true)
                    {
                        TempData["msgdisplay"] = "User registered successfully";
                    }
                    else
                    {
                        TempData["msgdisplay"] = "Something wrong, Please try again!";
                    }
                }
                #endregion
                else
                {
                    string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();

                    #region Email Send

                    //var userResult = _userService.Find(model.Email);

                    string Subject = "myschindlerprojects – confirm your email";

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                    _userService.SetTokenService(token, userResult.UserID, ExpToken);

                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Activate/VerifyAccount?TokenID=" + token + "");

                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", userResult.FirstName);
                    //dic.Add("UserName", userResult.FirstName);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "VerifyAccountNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, userResult.Email, Subject, Sbody);

                    #endregion

                    bool update = true;

                    if (update == true)
                    {
                        TempData["msgdisplay"] = "User registered successfully";
                    }
                    else
                    {
                        TempData["msgdisplay"] = "Something wrong, Please try again!";
                    }
                }

                return RedirectToAction("UserRegistration");
            }
            else
            {
                TempData["msgdisplay"] = "User registration failed";
                return RedirectToAction("UserLogin", "Login");
            }
        }

        /// <summary>
        /// check email id in database for uniqueness
        /// </summary>
        /// <param name="EmailTo"></param>
        /// <returns></returns>
         [HttpPost]
        public JsonResult CheckEmailExisting(string EmailTo)
        {
            var CheckEmailExist = _userService.ForgotPwdService(EmailTo);
            if (CheckEmailExist.Email == "0")
            {
                return Json(new { success = true, message = CommonMessage.NotExist.ToString() });

            }
            else
            {
                return Json(new { success = true, message = CommonMessage.Exist.ToString() });
            }
        }

        /// <summary>
        /// it display forgot password page
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorize]
        [HttpPost]
        public ActionResult ForgotPassword(EmailHistoryEntity model)
        {
            string From = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
            string Subject = "Schindler Password Assistance!";

            var exist = string.Empty;

            try
            {

                var CheckEmailExist = _userService.ForgotPwdService(model.EmailTo.ToLower());
                EmailHistoryEntity item = new EmailHistoryEntity();
                item.EmailTo = model.EmailTo;

                if (CheckEmailExist.Email == "0")
                {
                    return Json(new { success = true, message = CommonMessage.NotExist.ToString() });
                }
                else
                {
                    if (CheckEmailExist.AccountStatus == 4 || CheckEmailExist.AccountStatus == 17) // if user status is pending or verified then return
                    {
                        return Json(new { success = true, message = "Pending" });
                    }

                    var FirstName = string.Empty;
                    FirstName = CheckEmailExist.FirstName;

                    string token = (Guid.NewGuid().ToString("N"));

                    int ExpToken = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TokenExpireHours"]);
                    int UserID = CheckEmailExist.UserID;
                    _userService.SetTokenService(token, UserID, ExpToken);
                    string ActivationUrl = string.Empty;

                    //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                    ActivationUrl = Server.HtmlEncode("" + headurl + "/Login/ResetPassword?TokenID=" + token + "");


                    #region Email Send

                    string Sbody = null;

                    Dictionary<string, string> dic = new Dictionary<string, string>();

                    dic.Add("PersonName", FirstName);
                    dic.Add("UserEmailID", item.EmailTo);

                    dic.Add("MessageURL", ActivationUrl);

                    Sbody = StaticUtilities.getEmailBody("User", "ResetPasswordNew.html", dic);

                    SendEmail.Send(CommonConfiguration.NoReplyEmail, item.EmailTo, Subject, Sbody);





                    #endregion


                    //string body = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head><body><div style='background-color:whitesmoke;padding:10px 12px;'><span style='background-color:whitesmoke;'><h2 style='color:#24425E;font-family:Calibri;margin-left:5px;'>Schindler Password Assistance!</h2></span></div>";
                    //body += "<div style='border:1px solid #F0F0F0;' dir='ltr'><br/><p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>Hi " + FirstName + "," + "<br/><br/>";
                    //body += "<p style='color:#24425E;font-family:Calibri;font-size:large;margin-left:20px'>To initiate the password reset process for your<br/>";

                    //body += "<a><b> " + item.EmailTo + " </b></a>Schindler Account,click the link below :<br/><br/>";
                    //body += "<a href=" + ActivationUrl + ">" + ActivationUrl + "</a><br/><br/>";

                    //body += "If clicking the link above doesn't work, please copy and paste the URL in a new browser window instead.<br/><br/>";

                    //body += "If you've received this mail in error, it's likely that another user entered<br/> your email address by mistake while trying to reset a password. If you didn't<br/> initiate the request, you don't need to take any further action and can safely<br/> disregard this email.<br/><br/>";

                    //body += "Sincerely,<br/>The Schindler Support Team</p></div></body></html>";

                    DataSet tempds = new DataSet();
                    //bool update = SendEmail.Send(From, item.EmailTo, Subject, body);

                    bool update = true;

                    if (update == true)
                    {

                        DataTable t = new DataTable();
                        tempds.Tables.Add(t);
                        tempds.Tables[0].TableName = "ForgotPassword";

                        t.Columns.Add("Status");
                        t.Columns.Add("Message");

                        DataRow dr = t.NewRow();

                        dr["Status"] = "1";
                        dr["Message"] = "Your password reset link has been sent to your registered email address!";

                        t.Rows.Add(dr);
                        return Json(new { success = true, message = CommonMessage.Success.ToString() });
                    }
                    else
                    {
                        DataSet tempds1 = new DataSet();
                        DataTable t = new DataTable();
                        tempds1.Tables.Add(t);
                        tempds1.Tables[0].TableName = "ForgotPassword";
                        t.Columns.Add("Status");
                        t.Columns.Add("Message");
                        DataRow dr = t.NewRow();
                        dr["Status"] = "0";
                        dr["Message"] = CommonMessage.EmailSendError.ToString();
                        t.Rows.Add(dr);
                        return Json(new { success = true, message = "EmailError" });
                    }
                }


            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// it display reset password page to user to set new password
        /// </summary>
        /// <param name="TokenID"></param>
        /// <returns></returns>
        public ActionResult ResetPassword(string TokenID)
        {
            try
            {
                var CurrentTime = DateTime.Now;
                UserEntity TokenExpireDate = _userService.GetTokenExpiration(TokenID);
                if (CurrentTime > TokenExpireDate.TokenExpireDate)
                {
                    TempData["Message"] = "Link is expired";
                    return RedirectToAction("UserLogin", "Login");
                    //return View();
                }
                else
                {
                    var actionreturn = RedirectToAction("");
                    if (TokenID == null || TokenID == "")
                    {
                        actionreturn = RedirectToAction("UserLogin", "Login");
                        return actionreturn;
                    }
                    else
                    {
                        ViewBag.TokenID = HttpUtility.HtmlEncode(TokenID);
                        return View("ResetPassword");
                    }
                }
            }
            catch(Exception ex)
            {
                if(ex.Message.Contains("Column 'TokenExpireDate' does not belong to table UserMaster"))
                {
                    ViewBag.Message = "Link is expired";
                    return View();
                }
                else
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// set new password for user and store into database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetPassword(UserEntity user)
        {
            var token = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(user.Token));

            string saltKey = EncryptionService.CreateSaltKey(5);
            user.PasswordSalt = saltKey;
            user.Password = EncryptionService.CreatePasswordHash(user.Password, saltKey, "");
            user.AccountStatus = Convert.ToInt32(Status.Active);
            var updateUser = _userService.UpdatePwdService(token, user.Password, saltKey, user.AccountStatus);

            if (updateUser == true)
            {
                return Json(new { success = true, message = CommonMessage.Success.ToString() });
            }
            else
            {
                return Json(new { success = true, message = CommonMessage.Invalid.ToString() });
            }
        }

        /// <summary>
        /// to logout user. and clear all session
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {

            FormsAuthentication.SignOut();
            Session.Abandon();
            UserEntity objUser = new UserEntity();

            if (!string.IsNullOrEmpty(Request.Cookies[FormsAuthentication.FormsCookieName].Value))
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                if (authTicket.IsPersistent)
                {
                    Session["SessionId"] = null;
                    Session.Abandon();
                    Response.Cookies.Add(new HttpCookie(ConfigurationManager.AppSettings["RememberMeUserName"].ToString(), serializeModel.Email));
                    Response.Cookies[ConfigurationManager.AppSettings["RememberMeUserName"].ToString()].Expires = DateTime.Now.AddDays(15);
                    objUser = null;
                }
                else
                {
                    Response.Cookies[ConfigurationManager.AppSettings["RememberMeUserName"].ToString()].Expires = DateTime.Now.AddDays(-1);
                }
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["annflagCookie"].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction("UserLogin");

        }

        /// <summary>
        /// it display forgot password page
        /// </summary>
        /// <returns></returns>
        public ActionResult UserForgotPassword()
        {
            int AuditID = 0;
            int UserId = -1; // for audit trial record for new user
            int RoleId = -1; // for audit trial record for new user
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.ForgotPassword, "Forgot Password Page", Convert.ToString(UserId), RoleId, ApplicationOperation.ForgotPassword.ToString(), Session.SessionID, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            //if (AppAuditID != 0)
            //{
            //    HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
            //    AppAuditCookie.Value = Convert.ToString(AppAuditID);
            //    Response.Cookies.Add(AppAuditCookie);
            //}

            return View();
        }

        [HttpGet]
        public JsonResult BeforeAfterLoginAnnouncement(string DisplayOn)
        {
            try
            {
                var Announcementdetails = _announcementService.GetAllAnnouncement(DisplayOn);
                return Json(Announcementdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
    }
}