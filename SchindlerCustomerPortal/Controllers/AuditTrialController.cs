﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class AuditTrailController : BaseController
    {
        private readonly ICommonService _commonService;

        public AuditTrailController(ICommonService commonService)
        {
            this._commonService = commonService;
        }

        /// <summary>
        /// return AuditTrail List page
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            ModuleEntity module = _commonService.ModuleByName(ModuleName.AuditTrail);
            ViewBag.ModuleId = module.ModuleId;
            return View();
        }

        /// <summary>
        /// return list of audit data from database based on search criteria
        /// </summary>
        /// <param name="auditModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAuditTrailList(AuditTrailModel auditModel)
        {
            int count = 0;
            try
            {
                DataTableModel model = new DataTableModel();

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(50);

                model.UserID = User.UserId;
                model.RoleID = User.RoleId;
                model.LogDateFrom = Request.Form.GetValues("auditModel[LogDateFrom]")[0];
                model.LogDateTo = Request.Form.GetValues("auditModel[LogDateTo]")[0];

                if(string.IsNullOrEmpty(model.SearchTerm))
                   model.SearchTerm =(!string.IsNullOrEmpty(Request.Form.GetValues("auditModel[SearchCriteria]")[0])) ? Request.Form.GetValues("auditModel[SearchCriteria]")[0] : "";

                //model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                //model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                //if (string.IsNullOrEmpty(model.SortColumn))
                model.SortColumn = "LogDate";

                //if (string.IsNullOrEmpty(model.SortDirection))
                model.SortDirection = "DESC";

                var auditList = _commonService.ApplicationAuditMasterList(model, ref count);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //count = auditList.Count;
                }

                return Json(new { draw = draw, recordsFiltered = count, recordsTotal = count, data = auditList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }
    }
}