﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Calendar;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.MailBox;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class MailBoxController : BaseController
    {
        private readonly ICommonService _commonService;
        private readonly IMailBoxService _mailBoxservice;
        private readonly ICalendarService _calendarservice;
        private readonly IUserService _userService;


        public MailBoxController(
            ICommonService commonService,
            IMailBoxService mailBoxservice,
            IUserService userService,
            ICalendarService calendarservice
            )
        {
            this._commonService = commonService;
            this._mailBoxservice = mailBoxservice;
            this._userService = userService;
            this._calendarservice = calendarservice;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// it display list of mail
        /// </summary>
        /// <returns></returns>
        public ActionResult MailList()
        {
            int userId = User.UserId;
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            //To get the Id of Module
            ModuleEntity module = _commonService.ModuleByName(ModuleName.MailBox);
            ViewBag.ModuleId = module.ModuleId;

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.MailBox, "MailBox List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.MailBox_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View();
        }

        /// <summary>
        /// return partial view for mailList page
        /// </summary>
        /// <returns></returns>
        public ActionResult CompanyMail()
        {
            return PartialView();
        }

        /// <summary>
        /// return partial view for compose new mail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult NewMail(int id = 0)
        {
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            var data = _mailBoxservice.GetUserMailByMailId(id, appAuditID, ApplicationOperation.MailBox_NewMail.ToString(), User.SessionId, User.UserId);

            return PartialView(data);
        }

        /// <summary>
        /// return list of user match with search criteria
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CompanyUserAutoComplete(string searchTerm)
        {
            int companyId = User.CompanyID;

            if (searchTerm.Contains(","))
            {
                string result = searchTerm.Split(',')[searchTerm.Split(',').Length - 1];
                if (!string.IsNullOrEmpty(result))
                {
                    searchTerm = result.Trim();
                }
            }

            try
            {
                var userList = _mailBoxservice.GetAllUserByCompanyId(companyId, searchTerm);
                return Json(userList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// send mail to user with attachment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendMailToUser(MailBoxEntity model)
        {
            int appAuditID = 0;
            string chkMailId = "";
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.MailBox_SendMail.ToString();
            model.SessionID = User.SessionId;
            model.UserID = User.UserId;

            if (model.MailId != 0 || string.IsNullOrEmpty(Convert.ToString(model.MailId))) // for draft mail to send
            {
                var mail = _mailBoxservice.UserMailLogicalDelete(Convert.ToString(model.MailId), model.appAuditID, model.SessionID, User.UserId, ApplicationOperation.MailBox_DeleteMail.ToString());
            }

            model.CompanyId = User.CompanyID;
            model.Sender = User.Email;

            model.ViewStatus = "unread";
            model.Status = (int)MailStatus.Inbox; //9;

            if (string.IsNullOrEmpty(model.ForwardFileList) || model.ForwardFileList == "null")
            {
                if (string.IsNullOrEmpty(model.TotalFileName) || model.TotalFileName == "null")
                {
                    model.HasAttachment = false;
                    model.AttachmentPath = string.Empty;
                }
                else
                {
                    model.HasAttachment = true;
                    model.AttachmentPath = model.TotalFileName;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.TotalFileName) || model.TotalFileName == "null")
                {
                    model.HasAttachment = false;
                    model.AttachmentPath = string.Empty;
                }
                else
                {
                    model.HasAttachment = true;
                    model.AttachmentPath = model.ForwardFileList + "|" + model.TotalFileName;
                }
            }

            if (string.IsNullOrEmpty(model.ToList))
                model.ToList = string.Empty;

            if (string.IsNullOrEmpty(model.CC))
                model.CC = string.Empty;

            if (string.IsNullOrEmpty(model.BCC))
                model.BCC = string.Empty;

            try
            {
                var mailResult = _mailBoxservice.SendMail(model);
                var mailId = 0;

                if (mailResult.Count != 0)
                {

                    foreach (var item in mailResult)
                    {
                        if (mailId == item.MailId)
                            continue;


                        if (!string.IsNullOrEmpty(chkMailId))
                        {
                            if (chkMailId.Contains(item.Receiver))
                                continue;
                        }

                        chkMailId += item.Receiver;

                        //if (mailId == 0)
                        //{
                        mailId = item.MailId;

                        #region Email Send

                        string SFrom = System.Configuration.ConfigurationManager.AppSettings["AccountMail"].ToString();
                        string Subject = "myschindlerprojects - " + model.Subject;

                        string ActivationUrl = string.Empty;

                        //string headurl = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                        string headurl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];

                        ActivationUrl = Server.HtmlEncode("" + headurl + "/mailbox/maillist?mail=" + item.MailId + "");

                        string Sbody = null;

                        Dictionary<string, string> dic = new Dictionary<string, string>();

                        dic.Add("PersonName", item.UserName.Split(' ')[0]);
                        dic.Add("SenderName", User.FirstName);

                        dic.Add("MessageURL", ActivationUrl);

                        Sbody = StaticUtilities.getEmailBody("User", "EmailNotificationNew.html", dic);

                        SendEmail.SendMailtoMultiple(SFrom, item.Receiver, "", "", Subject, Sbody);

                        #region Push Notification
                        string IsNotificationEnable = System.Configuration.ConfigurationManager.AppSettings["IsNotificationEnable"].ToString();
                        var SENDER_ID = System.Configuration.ConfigurationManager.AppSettings["SenderID"].ToString();
                        int success = 0;
                        bool Isnotify = false;
                        NotificationEntity data = new NotificationEntity();
                        if (IsNotificationEnable.ToUpper() == "YES")
                        {
                            if (!string.IsNullOrEmpty(item.DeviceID) && !string.IsNullOrEmpty(item.DeviceType))
                            {
                                string NotificationSubject = "New Email Notification";
                                string NotificationBody = StaticUtilities.getNotificationBody("User", "EmailNotificationNew.txt", dic);

                                try
                                {
                                    if (item.DeviceType.ToUpper() == "ANDROID")
                                    {
                                        success = SendNotification.NotifyAndroidDevice(item.DeviceID, NotificationSubject, NotificationBody);

                                        if (success < 1)
                                        {
                                            data.StatusID = 30;
                                        }
                                        else
                                        {
                                            data.StatusID = 29;
                                        }
                                    }
                                    else
                                    {
                                        Isnotify = SendNotification.NotifyIOSDevice(item.DeviceID, NotificationSubject, NotificationBody);
                                        if (Isnotify)
                                        {
                                            data.StatusID = 29;
                                        }
                                        else
                                        {
                                            data.StatusID = 30;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    data.StatusID = 30;
                                }


                                //Insert into NotificationLogId table

                                data.CompanyID = User.CompanyID;
                                data.DeviceID = item.DeviceID;
                                data.SenderID = SENDER_ID.ToString();
                                data.ReceiverID = Convert.ToString(model.UserID);
                                data.Subject = NotificationSubject;
                                data.Message = NotificationBody;
                                data.DateSent = DateTime.Now;

                                data.CreatedBy = User.FirstName + " " + User.LastName;
                                data.DeviceType = item.DeviceType;
                                var Notify = _userService.AddNotification(data);

                            }
                        }

                        #endregion

                        #endregion

                        //}
                    }
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //throw;
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Display list of mail based on status like Draft, Inbox, Sent
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllMailByUser(string status)
        {
            int count = 0;

            int statusValue = 0;

            MailStatus choice;
            if (Enum.TryParse(status, out choice))
            {
                statusValue = (int)choice;
            }

            try
            {
                DataTableModel model = new DataTableModel();

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.MailBox_List.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                model.CompanyID = User.CompanyID;
                model.UserEmail = User.Email;

                model.StatusValue = statusValue;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);


                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var user = _mailBoxservice.GetAllUserMailByUserMail(model, ref count);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    count = user.Count;
                }

                return Json(new { draw = draw, status = status, recordsFiltered = count, recordsTotal = count, data = user }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }

        }

        /// <summary>
        /// It return partial view fro mailList page
        /// Read mail from Inbox or URL that is received by user
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="statusValue"></param>
        /// <param name="ReadmailId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReadMail(int mail, string statusValue, string ReadmailId = null)
        {
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }

            if (!string.IsNullOrEmpty(statusValue))
            {
                if (statusValue != "Draft" && statusValue != "Sent" && statusValue != "Delete")
                {
                    string viewStatus = "read";
                    int status = (int)MailStatus.Inbox;

                    var updateStatus = _mailBoxservice.UpdateUserMailStatus(Convert.ToString(mail), viewStatus, status, appAuditID, ApplicationOperation.MailBox_ReadMail.ToString(), User.SessionId, User.UserId);
                }
            }         

            var mailData = _mailBoxservice.GetUserMailByMailId(mail, appAuditID, ApplicationOperation.MailBox_ReadMail.ToString(), User.SessionId, User.UserId);
            if (mailData.HasAttachment)
            {
                string[] AttachedFilename = mailData.AttachmentPath.Split('|');
                string path = "";
                try
                {
                    foreach (var item in AttachedFilename)
                    {
                        path = CommonConfiguration.emailAttachmentFullPath + item;
                        mailData.Size += (new FileInfo(path).Length / 1000).ToString() + "|";
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (mailData.Status != 10 && mailData.Status != 11 && mailData.Status != 12)
            {
                if (mailData.Receiver.Trim() == User.Email.Trim())
                {
                    if (!string.IsNullOrEmpty(ReadmailId))
                    {
                        string viewStatus = "read";
                        int status = (int)MailStatus.Inbox;

                        var updateStatus = _mailBoxservice.UpdateUserMailStatus(Convert.ToString(mail), viewStatus, status, appAuditID, ApplicationOperation.MailBox_ReadMail.ToString(), User.SessionId, User.UserId);
                    }

                    return PartialView(mailData);
                }
                else
                {

                    //Fetch the Cookie using its Key.
                    HttpCookie nameCookie = Request.Cookies[".ASPXAUTH"];
                    //Set the Expiry date to past date.
                    nameCookie.Expires = DateTime.Now.AddDays(-1);
                    //Update the Cookie in Browser.
                    Response.Cookies.Add(nameCookie);

                    ControllerContext.HttpContext.Response.Redirect("/Login/UserLogin?returnUrl=/mailbox/maillist?mail=" + ReadmailId);
                }
            }
            

            return PartialView(mailData);
        }

        /// <summary>
        /// it return partial view fro mailList page
        /// it provide sideBar menu for mailList page
        /// </summary>
        /// <returns></returns>
        //[ChildActionOnly]
        public ActionResult MailSideBar()
        {
            string email = User.Email;
            var model = _mailBoxservice.GetMailCountByUserMail(email);
            return PartialView(model);
        }

        /// <summary>
        /// mail status change to delete
        /// </summary>
        /// <param name="mailID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteUserMail(string mailID)
        {
            string viewStatus = "read";
            int status = (int)MailStatus.Delete;

            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }

            try
            {
                var mail = _mailBoxservice.UpdateUserMailStatus(mailID, viewStatus, status, appAuditID, ApplicationOperation.MailBox_DeleteMail.ToString(), User.SessionId, User.UserId);
                return Json(mail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// mail save as draft
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DraftUserMail(MailBoxEntity model)
        {
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            model.appAuditID = appAuditID;
            model.ApplicationOperation = ApplicationOperation.MailBox_DraftMail.ToString();
            model.SessionID = User.SessionId;
            model.UserID = User.UserId;

            if (model.MailId != 0 || string.IsNullOrEmpty(Convert.ToString(model.MailId))) // for draft mail to send
            {
                var mail = _mailBoxservice.UserMailLogicalDelete(Convert.ToString(model.MailId), model.appAuditID, model.SessionID, User.UserId, ApplicationOperation.MailBox_DeleteMail.ToString());
            }

            model.CompanyId = User.CompanyID;
            model.Sender = User.Email;

            model.ViewStatus = "read";
            model.Status = (int)MailStatus.Draft;

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["img"];
                if (logo.ContentLength > 0)
                {
                    var fileName = logo.FileName;

                    string onlyFilename = fileName.Substring(0, fileName.LastIndexOf('.'));

                    var ext = Path.GetExtension(logo.FileName);
                    var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
                    var newFileName = (dateTime + ext).ToString();
                    var comPath = CommonConfiguration.emailAttachmentPath + newFileName;

                    var imgSave = Server.MapPath(comPath);

                    logo.SaveAs(imgSave);
                    model.AttachmentPath = comPath;
                }
                model.HasAttachment = true;
            }
            else
            {
                model.HasAttachment = false;
                model.AttachmentPath = string.Empty;
            }

            if (string.IsNullOrEmpty(model.ToList))
                model.ToList = string.Empty;

            if (string.IsNullOrEmpty(model.CC))
                model.CC = string.Empty;

            if (string.IsNullOrEmpty(model.BCC))
                model.BCC = string.Empty;

            try
            {
                int result = _mailBoxservice.SaveDraft(model);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// mail reply with old mail data
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MailReply(int mail)
        {
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }

            var mailData = _mailBoxservice.GetUserMailByMailId(mail, appAuditID, ApplicationOperation.MailBox_ReplyMail.ToString(), User.SessionId, User.UserId);

            return Json(mailData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// refresh mail box every some time of Interval
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MailCountforRefresh()
        {
            var model = _mailBoxservice.GetMailCountByUserMail(User.Email);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// mail delete from Deleted folder
        /// </summary>
        /// <param name="mailID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteUserMailLogical(string mailID)
        {

            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }

            string SessionID = User.SessionId;

            try
            {
                var mail = _mailBoxservice.UserMailLogicalDelete(mailID, appAuditID, SessionID, User.UserId, ApplicationOperation.MailBox_DeleteMail.ToString());
                return Json(mail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// upload single or multiple file to server
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MultipleFileUpload()
        {
            string returnFileName = string.Empty;
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        var ext = Path.GetExtension(file.FileName);
                        string onlyFilename = file.FileName.Substring(0, file.FileName.LastIndexOf('.'));
                        var dateTime = DateTime.Now.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "FN" + onlyFilename;
                        var newFileName = (dateTime + ext).ToString();
                        var comPath = CommonConfiguration.emailAttachmentPath + newFileName;

                        var imgSave = Server.MapPath(comPath);
                        file.SaveAs(imgSave);

                        if (string.IsNullOrEmpty(returnFileName))
                        {
                            returnFileName = newFileName;
                        }
                        else
                        {
                            returnFileName += "|" + newFileName;
                        }
                    }
                    // Returns message that successfully uploaded  
                    return Json(returnFileName, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json(returnFileName, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// download attachment from mail
        /// </summary>
        /// <param name="attachmentPath"></param>
        /// <returns></returns>
        public ActionResult DownloadAttachement(string attachmentPath)
        {
            byte[] fileBytes;

            attachmentPath = System.Web.HttpUtility.UrlDecode(attachmentPath);

            var comPath = CommonConfiguration.emailAttachmentPath + attachmentPath;

            try
            {
                if (System.IO.File.Exists(Server.MapPath(comPath)))
                {
                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(comPath));
                    string fileName = Path.GetFileName(Server.MapPath(comPath));
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return RedirectToAction("MailList");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                fileBytes = null;
                GC.Collect();
            }
        }

        /// <summary>
        /// return mail list for Page Header 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UnreadMailCountforHeader()
        {
            var model = new List<MailBoxEntity>();
            var calendarEvent = new List<CalendarEventEntity>();

            if (User != null)
            {
                // model = _mailBoxservice.GetAllUserMailforHeadermenu(User.Email);
                calendarEvent = _calendarservice.GetAllUserCalendarData(User.Email);
                // return Json(new { mailList = model, calendarList = calendarEvent }, JsonRequestBehavior.AllowGet);
                return Json(new { calendarList = calendarEvent }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //return Json(new { mailList = model, calendarList = calendarEvent }, JsonRequestBehavior.AllowGet);
                return Json(new { calendarList = calendarEvent }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// return mail list for Dashboard widget
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UnreadMailForDashboardWidget()
        {
            var model = _mailBoxservice.GetAllUserMailforDashboardWidget(User.Email);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}