﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Models.Resource;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.ResourceUpload;
using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class ResourceUploadController : BaseController
    {
        private readonly ICommonService _ICommonService;
        private readonly IResourceUploadService _IResourceUploadService;
     

        public ResourceUploadController(ICommonService iCommonService, IResourceUploadService iResourceUploadService)
        {
            this._ICommonService = iCommonService;
            this._IResourceUploadService = iResourceUploadService;
        }

        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;

        /// <summary>
        /// return Resource Center Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
           
            return View();
        }

        /// <summary>
        /// return Resource center List page
        /// Here user can add, edit, delete Resouce file
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.ResourceCenter);
            ViewBag.ModuleId = module.ModuleId;

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.ResourceCenter, "Resource Center List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.ResourceCenter_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View("List");
        }

        /// <summary>
        /// return list of resource from database
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetResourceAll(DataTableModel model)
        {
            try
            {
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.ResourceCenter_List.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                int totalcount = 0;
                var Resources = new List<ResourceUploadEntity>();

                if (string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = string.Empty;
                }

                model.PageNumber = 0;
                model.PageSize = 1000;
                model.RoleID = User.RoleId;

                //Get Sort columns value
                model.SortColumn = "ResourceName";
                model.SortDirection = "ASC";

                #region Resource center old design code

                //model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                //var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                //model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                //model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //model.RoleID = User.RoleId;

                ////Get Sort columns value
                //model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                //model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                #endregion

                Resources = _IResourceUploadService.GetAllResources(model, ref totalcount);

                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    totalcount = Resources.Count;
                }
                //return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = Resources }, JsonRequestBehavior.AllowGet);

                return Json(Resources, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// upload file to server
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            string TempPath = "";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        if (file.ContentLength > 0)
                        {
                            TempPath = CommonConfiguration.TempDocPath + file.FileName;
                            var docsave = Server.MapPath(TempPath);
                            file.SaveAs(docsave);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        /// <summary>
        /// add new rosource data to database
        /// </summary>
        /// <param name="Resobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddResource(ResourceUploadEntity Resobj)
        {
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = Resobj.FileName;
            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var logo = System.Web.HttpContext.Current.Request.Files["img"];
                    if (logo.ContentLength > 0)
                    {
                        string titleImage = System.IO.Path.GetFileName(Resobj.TitleImage);

                        var comPath = CommonConfiguration.userProfilePicPath + titleImage;

                        var imgSave = Server.MapPath(comPath);
                        logo.SaveAs(imgSave);
                        //user.UserProfilePic = ImageName;

                        //Added by vive nayak for thumbnail
                        Image image = Image.FromFile(imgSave);
                        Size thumbnailSize = StaticUtilities.GetThumbnailSize(image);
                        // Get thumbnail.
                        Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                            thumbnailSize.Height, null, IntPtr.Zero);
                        var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath), "Thumb_" + titleImage);
                        // Save thumbnail.
                        thumbnail.Save(thumbpath);


                    }
                }

                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);

                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);
                    System.IO.File.Copy(sourceFile, destFile, true);

                    Resobj.DocumentPath = docpath + DatewithSeconds + "_" + filename;
                }

                Resobj.PublishedBy = User.FirstName + " " + User.LastName;
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                Resobj.appAuditID = appAuditID;
                Resobj.ApplicationOperation = ApplicationOperation.ResourceCenter_Add.ToString();
                Resobj.SessionID = User.SessionId;

                var documentdetails = _IResourceUploadService.ResourceUploadInsertService(Resobj);

                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// get resource data bsed on resourceId
        /// </summary>
        /// <param name="resId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EditResourceUpload(int resId)
        {
            try
            {
                var documentdetails = _IResourceUploadService.GetREsById(resId);
                return Json(documentdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// update resource data to database
        /// </summary>
        /// <param name="resobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateResource(ResourceUploadEntity resobj)
        {

            var DatewithSeconds = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
            var docpath = CommonConfiguration.docuploadPath;
            var temppath = CommonConfiguration.TempDocPath;
            var filename = resobj.FileName;
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var logo = System.Web.HttpContext.Current.Request.Files["img"];
                    if (logo.ContentLength > 0)
                    {
                        string titleImage = System.IO.Path.GetFileName(resobj.TitleImage);

                        var comPath = CommonConfiguration.userProfilePicPath + titleImage;

                        var imgSave = Server.MapPath(comPath);
                        try
                        {
                            logo.SaveAs(imgSave);
                        }
                        catch(Exception ex)
                        {
                            if(!ex.Message.Contains("The process cannot access the file"))
                            {
                                throw ex;
                            }
                        }
                        //user.UserProfilePic = ImageName;


                        //Added by vive nayak for thumbnail
                        Image image = Image.FromFile(imgSave);
                        Size thumbnailSize = StaticUtilities.GetThumbnailSize(image);
                        // Get thumbnail.
                        Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                            thumbnailSize.Height, null, IntPtr.Zero);
                        var thumbpath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath), "Thumb_" + titleImage);

                        try
                        {
                            // Save thumbnail.
                            thumbnail.Save(thumbpath);
                        }
                        catch (Exception ex)
                        {
                            if (!ex.Message.Contains("The process cannot access the file"))
                            {
                                throw ex;
                            }
                        }

                    }
                }
                else
                {
                    resobj.TitleImage = "-1";
                }

                if(resobj.IsDeleted == 1)
                {
                    resobj.TitleImage = "no_images.png";
                }

                if (filename != null)
                {
                    string sourceFile = System.IO.Path.Combine(Server.MapPath(temppath), filename);

                    string destFile = System.IO.Path.Combine(Server.MapPath(docpath), DatewithSeconds + "_" + filename);

                    System.IO.File.Copy(sourceFile, destFile, true);

                    resobj.DocumentPath = docpath + DatewithSeconds + "_" + filename;

                }

                resobj.UpdatedBy = User.FirstName + " " + User.LastName;
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                resobj.appAuditID = appAuditID;
                resobj.ApplicationOperation = ApplicationOperation.ResourceCenter_Update.ToString();
                resobj.SessionID = User.SessionId;

                var documentdetails = _IResourceUploadService.ResourceUpdateService(resobj);
                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// view resouce file in other tab
        /// </summary>
        /// <param name="DocPath"></param>
        /// <returns></returns>
        //[Route("ResourceUpload/ResourceView/{DocPath}")]
        public ActionResult ViewResourceName(string DocPath)
        {
            try
            {
                string fileName = Path.GetFileName(Server.MapPath(DocPath));

                byte[] filedata = System.IO.File.ReadAllBytes(Server.MapPath(DocPath));
                string contentType = MimeMapping.GetMimeMapping(Server.MapPath(DocPath));

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = fileName,
                    Inline = true,
                };

                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// soft delete resource data from database 
        /// </summary>
        /// <param name="DocID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteResource(int DocID)
        {
            ResourceUploadEntity doc = new ResourceUploadEntity();
            doc.ResourceId = DocID;
            doc.UpdatedBy = User.UserId.ToString();
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            doc.appAuditID = appAuditID;
            doc.ApplicationOperation = ApplicationOperation.ResourceCenter_Delete.ToString();
            doc.SessionID = User.SessionId;

            try
            {
                var user = _IResourceUploadService.DeleteResource(doc);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
    }
}