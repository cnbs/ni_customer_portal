﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Authentication;
using SchindlerCustomerPortal.Services.Announcement;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Controllers
{
    [Authorize]
    [UserPermissionAuthorizeAttribute]
    public class AnnouncementMessageController : BaseController
    {
        private readonly IAnnouncementService _announcementService;
        private readonly ICommonService _ICommonService;
        private readonly IUserService _userService;
        public AnnouncementMessageController(IAnnouncementService announcementService, ICommonService iCommonService,
            IUserService userService)
        {
            this._announcementService = announcementService;
            this._ICommonService = iCommonService;
            this._userService = userService;
        }
        string IpAddress = string.Empty;
        Int32 AppAuditID = 0;
        string Clientbrowser = string.Empty;
        string ClientOS = string.Empty;
        string ServerIP = string.Empty;
        string ServerName = string.Empty;
        // GET: AnnounceMessage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            //To get the Id of Module
            ModuleEntity module = _ICommonService.ModuleByName(ModuleName.Announcement);
            ViewBag.ModuleId = module.ModuleId;

            var permission = new UserPermissionViewModel();
            int AuditID = 0;
            IpAddress = StaticUtilities.getIP();
            Clientbrowser = StaticUtilities.getClientbrowser();
            ClientOS = StaticUtilities.getClientOS();
            ServerIP = StaticUtilities.getServerIP();
            ServerName = StaticUtilities.getServerName();

            permission = _userService.CheckUserModulePermission(User.UserId, ModuleName.Announcement);

            if (permission == null)
            {
                permission = new UserPermissionViewModel();
            }

            if (User.RoleId != 1)
            {
                if (!permission.HasViewPermission)
                {
                    return RedirectToAction("UnauthorizedAccess", "Common", null);
                }
            }

            AppAuditID = StaticUtilities.ApplicationAuditRecord(ModuleName.Announcement, "Announcement Message List", Convert.ToString(User.UserId), User.RoleId, ApplicationOperation.AnnouncementMessage_List.ToString(), User.SessionId, IpAddress.ToString(), Clientbrowser, ClientOS, ServerIP, ServerName, out AuditID);

            if (AppAuditID != 0)
            {
                HttpCookie AppAuditCookie = new HttpCookie("AppAuditCookie");
                AppAuditCookie.Value = Convert.ToString(AppAuditID);
                Response.Cookies.Add(AppAuditCookie);
            }

            return View("List");
        }
        [HttpPost]
        public JsonResult GetAllAnnouncement(DataTableModel model)
        {
            try
            {
                int totalcount = 0;

                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                model.appAuditID = appAuditID;
                model.ApplicationOperation = ApplicationOperation.AnnouncementMessage_List.ToString();
                model.SessionID = User.SessionId;
                model.UserID = User.UserId;

                model.SearchTerm = Request.Form.GetValues("search[value]")[0];
                var draw = Convert.ToInt16(Request.Form.GetValues("draw").FirstOrDefault());
                model.PageNumber = Convert.ToInt16(Request.Form.GetValues("start")[0]);
                model.PageSize = Convert.ToInt32(CommonConfiguration.numberOfRecordsToDisplay);

                //Get Sort columns value
                model.SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
                model.SortDirection = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var AnnouncementData = _announcementService.GetAnnouncement(model, ref totalcount);
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    //totalcount = HelpData.Count;
                }
                return Json(new { draw = draw, recordsFiltered = totalcount, recordsTotal = totalcount, data = AnnouncementData }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult AddAnnouncementData(AnnouncementEntity objann)
        {
            try
            {
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                objann.appAuditID = appAuditID;
                objann.ApplicationOperation = ApplicationOperation.AnnouncementMessage_Add.ToString();
                objann.SessionID = User.SessionId;
                objann.CreatedBy = User.UserId.ToString();
                var announcementdetails = _announcementService.InsertAnnouncement(objann);

                return Json(CommonMessage.Success.ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpGet]
        public JsonResult EditAnnouncement(int AnnMsgId)
        {
            try
            {
                var Announcementdetails = _announcementService.GetAnnouncementbyID(AnnMsgId);
                return Json(Announcementdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpPost]
        public JsonResult UpdateAnnouncement(AnnouncementEntity annobj)
        {
            try
            {
                annobj.UpdatedBy = User.UserId.ToString();
                int appAuditID = 0;
                if (Request.Cookies["AppAuditCookie"] != null)
                {
                    appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
                }
                annobj.appAuditID = appAuditID;
                annobj.ApplicationOperation = ApplicationOperation.AnnouncementMessage_Update.ToString();
                annobj.SessionID = User.SessionId;

                var announcementdetails = _announcementService.UpdateAnnouncement(annobj);
                return Json(CommonMessage.Success.ToString());

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        [HttpPost]
        public JsonResult DeleteAnnouncementData(int AnnMsgId)
        {
            AnnouncementEntity annouce = new AnnouncementEntity();
            annouce.AnnMsgId = AnnMsgId;
            annouce.DeletedBy = User.UserId.ToString();
            int appAuditID = 0;
            if (Request.Cookies["AppAuditCookie"] != null)
            {
                appAuditID = Convert.ToInt32(Request.Cookies["AppAuditCookie"].Value);
            }
            annouce.appAuditID = appAuditID;
            annouce.ApplicationOperation = ApplicationOperation.AnnouncementMessage_Delete.ToString();
            annouce.SessionID = User.SessionId;

            try
            {
                var user = _announcementService.DeleteAnnouncement(annouce);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
    }
}