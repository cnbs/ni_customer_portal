﻿using System.Web;
using System.Web.Optimization;

namespace SchindlerCustomerPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        //"~/js/plugins/jquery/jquery.min.js"
                        //"~/js/plugins/jquery/jquery-ui.min.js"
                        ));


            //bundles.Add(new ScriptBundle("~/bundles/lightbox").Include(
            //           "~/Scripts/lightbox.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootbox.min.js",
                      "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //         "~/Content/bootstrap.min.css",
            //         "~/Content/bootstrap-reset.css",
            //         "~/font-awesome/css/font-awesome.css",
            //         "~/Content/style.css",
            //         "~/Content/style-responsive.css"));

            //bundles.Add(new StyleBundle("~/Content/datatable").Include(
            //          "~/Content/dataTables.bootstrap.min.css"
            //         ));

            //bundles.Add(new StyleBundle("~/Content/jquery-ui").Include(
            //          "~/Content/jquery-ui.css"
            //         ));


            //bundles.Add(new StyleBundle("~/Content/scroll").Include(
            //          "~/Content/jquery.dataTables.min.css",
            //          "~/Content/scroller.dataTables.min.css",
            //          "~/Content/jquery.mCustomScrollbar.css"
            //         ));

            //bundles.Add(new StyleBundle("~/Content/select2css").Include(
            //          "~/Content/select2.css"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
            //         "~/Scripts/jquery.dataTables.min.js",
            //         "~/Scripts/dataTables.bootstrap.min.js",
            //         "~/Scripts/dataTables.buttons.min.js",
            //         "~/Scripts/jszip.min.js",
            //         "~/Scripts/buttons.html5.min.js",
            //         "~/Scripts/ModelInsideModelScroll.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/corejs").Include(
            //         "~/Scripts/jquery.js",
            //         "~/Scripts/jquery-1.11.3.min.js",
            //         "~/Scripts/bootstrap.min.js",
            //         "~/Scripts/hover-dropdown.js",
            //         "~/Scripts/scripts.js"
            //         ));


            bundles.Add(new ScriptBundle("~/bundles/selectjs").Include(
                     "~/Scripts/select-init.js",
                     "~/Scripts/select2.js"
                     ));




            //New Bundles - New Designs
            //bundles.Add(new StyleBundle("~/Content/Bootstrap").Include(
            //          "~/Content/bootstrap.min.css",
            //          "~/Content/main.css"
            //          ));

            //bundles.Add(new StyleBundle("~/Content/Treeview").Include(
            //          "~/Content/treeview.min.css",
            //          "~/Content/jquery.mCustomScrollbar.css"
            //          ));


            //bundles.Add(new StyleBundle("~/Content/Datepicker").Include(
            //         "~/Content/datepicker.css"
            //         ));

            //bundles.Add(new StyleBundle("~/Content/tag").Include(
            //         "~/Content/tagit.ui-zendesk.css",
            //         "~/Content/jquery.tagit.css"
            //         ));


            //bundles.Add(new StyleBundle("~/Content/LoadImage").Include(
            //    "~/Content/LoadingImage.css"

            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
            //         "~/Scripts/jquery.min.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/Bootstrap").Include(
            //         "~/Scripts/bootstrap.min.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/ctnavbar").Include(
            //         "~/Scripts/ct-navbar.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/jquerybdt").Include(
            //         "~/Scripts/jquery.bdt.js"
            //         ));


            //bundles.Add(new ScriptBundle("~/bundles/tag").Include(
            //         "~/Scripts/tag-it.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/Datepicker").Include(
            //         "~/Scripts/bootstrap-datepicker.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/mask").Include(
            //         "~/Scripts/jquery.mask.min.js"
            //         ));

            //bundles.Add(new ScriptBundle("~/bundles/scroll").Include(
            //        "~/Scripts/dataTables.scroller.min.js"

            //        ));

            //bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
            //        "~/Scripts/jquery-ui.min.js"
            //        ));

            //bundles.Add(new ScriptBundle("~/bundles/jstree").Include(
            //       "~/Scripts/jstree.min.js"
            //       ));

            //bundles.Add(new ScriptBundle("~/bundles/scrollbar").Include(
            //       "~/Scripts/jquery.mCustomScrollbar.concat.min.js"
            //       ));

            //bundles.Add(new ScriptBundle("~/bundles/upload").Include(
            //       "~/Scripts/dropzone.js",
            //       "~/Scripts/multiupload.js"

            //       ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapFinal").Include(
                      //"~/Scripts/bootstrap.js",
                      "~/js/plugins/bootstrap/bootstrap.min.js",
                      //"~/Scripts/bootbox.min.js",
                      "~/Scripts/respond.js",
                      "~/js/ct-navbar.js",
                      "~/js/jquery.dataTables.min.js",
                      "~/js/dataTables.bootstrap.min.js",
                      "~/Scripts/jquery.mask.min.js"
                      ));

            //bundles.Add(new StyleBundle("~/Content/cssFinal").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/bootstrap.min.css",
            //          "~/Content/main.css" 
            //          ));

            bundles.Add(new ScriptBundle("~/bundles/commonjs").Include(
                    "~/js/custom/common.js"));

            //bundles.Add(new ScriptBundle("~/bundles/angular").Include(
            //       "~/Scripts/angular.js",
            //       "~/Scripts/angular-route.js",
            //       "~/Scripts/ui-bootstrap-tpls-2.0.0.js",
            //       "~/Scripts/angular-sanitize.js",
            //       "~/js/custom/App.js"));

            //bundles.Add(new StyleBundle("~/Content/ckeditor").Include(
            //       "~/App_Client/Component/ng-ckeditor.css"
            //       ));

            //bundles.Add(new StyleBundle("~/Content/autocompletetagsinput").Include(
            //    "~/App_Client/Component/AutocompleteTagsinput/ng-tags-input.bootstrap.css"
            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/communication").Include(
            //      "~/App_Client/Controller/Communication/Post.Controller.js",
            //      "~/App_Client/Controller/Communication/PostDetail.Controller.js",
            //      "~/App_Client/Controller/Communication/ProjectPost.Controller.js",
            //      "~/App_Client/Service/Communication/post.service.js",
            //      "~/App_Client/View/Communication/Communication.route.js"
            //      ));

            //bundles.Add(new ScriptBundle("~/bundle/projectmaster").Include(
            //    "~/App_Client/Controller/Project/ProjectList.Controller.js",
            //    "~/App_Client/Controller/Project/ProjectEdit.Controller.js",
            //    "~/App_Client/Service/Project/Project.Service.js",
            //    "~/App_Client/View/Project/Project.route.js"
            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/angularcomponent").Include(
            //       "~/App_Client/Component/Spinner/spin.js",
            //           "~/App_Client/Component/Spinner/angular-spinner.js",
            //       "~/App_Client/Component/Common.js",
            //      "~/App_Client/Component/toastr.js",
            //        "~/app_client/service/lookup/lookup.service.js",
            //           "~/App_Client/Component/ng-ckeditor.js",
            //           "~/App_Client/Component/AutocompleteTagsinput/ng-tags-input.js"
                     

            //       ));

            BundleTable.EnableOptimizations = false;


        }
    }
}
