﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SchindlerCustomerPortal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{comeFrom}",
                defaults: new { controller = "Login", action = "UserLogin", id = UrlParameter.Optional, comeFrom=UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "MyProjectJobRoute",
                url: "{controller}/{action}/{id}/{ProjectNo}/{CompanyId}/{comeFrom}",
                defaults: new { controller = "Login", action = "UserLogin", id = UrlParameter.Optional, CompanyId = UrlParameter.Optional, ProjectNo = UrlParameter.Optional, comeFrom = UrlParameter.Optional }
            );
            routes.MapRoute("CatchAllUrls", "{*url}", new { controller = "Common", action = "Error404Page" }, new string[] { "MyApp.Controllers" });
            //routes.MapRoute(
            //    name: "ProjectList",
            //    url: "{controller}/{action}/{Status}",
            //    defaults: new { controller = "ManageProject", action = "ProjectMasterList", Status = UrlParameter.Optional }
            //);
        }
    }
}
