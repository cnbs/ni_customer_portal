using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using SchindlerCustomerPortal.App_Start;
using SchindlerCustomerPortal.Services.Common;
using SchindlerCustomerPortal.Services.Communication;
using SchindlerCustomerPortal.Services.Company;
using SchindlerCustomerPortal.Services.Document;
using SchindlerCustomerPortal.Services.Help;
using SchindlerCustomerPortal.Services.MailBox;
using SchindlerCustomerPortal.Services.Project;
using SchindlerCustomerPortal.Services.ProjectDocument;
using SchindlerCustomerPortal.Services.ProjectMilestone;
using SchindlerCustomerPortal.Services.ResourceUpload;
using SchindlerCustomerPortal.Services.User;
using SchindlerCustomerPortal.Services.OfficeManager;
using WebActivatorEx;
using SchindlerCustomerPortal.Services.Announcement;
using SchindlerCustomerPortal.Services.Calendar;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace SchindlerCustomerPortal.App_Start
{
    // using SchindlerCustomerPortal.Services.Help;


    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        ///     Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        ///     Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        ///     Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        ///     Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICompanyService>().To<CompanyService>().InRequestScope();

            kernel.Bind<ICommonService>().To<CommonService>().InRequestScope();

            kernel.Bind<IUserService>().To<UserService>().InRequestScope();

            kernel.Bind<IDocumentService>().To<DocumentService>().InRequestScope();

            kernel.Bind<ICommunicationService>().To<CommunicationService>().InRequestScope();

            kernel.Bind<IProjectService>().To<ProjectService>().InRequestScope();

            kernel.Bind<IMailBoxService>().To<MailBoxService>().InRequestScope();

            kernel.Bind<ICalendarService>().To<CalendarService>().InRequestScope();

            kernel.Bind<IContractDocumentService>().To<ContractDocumentService>().InRequestScope();

            kernel.Bind<IHelpService>().To<HelpService>().InRequestScope();


            kernel.Bind<IProjectMasterService>().To<ProjectMasterService>().InRequestScope();

            kernel.Bind<IResourceUploadService>().To<ResourceUploadService>().InRequestScope();

            kernel.Bind<IOfficeManagerService>().To<OfficeManagerService>().InRequestScope();

            kernel.Bind<IProjectMilestoneService>().To<ProjectMilestoneService>().InRequestScope();

            kernel.Bind<IAnnouncementService>().To<AnnouncementService>().InRequestScope();

        }
    }
}