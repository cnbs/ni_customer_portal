﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Transports;

[assembly: OwinStartup(typeof(SchindlerCustomerPortal.Hubs.Startup))]

namespace SchindlerCustomerPortal.Hubs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // If using the global dependency resolver
            var hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            app.MapSignalR(hubConfiguration);

            TurnOfForeverFrame(GlobalHost.DependencyResolver);
            //app.MapSignalR();

            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
        }

        public static void TurnOfForeverFrame(IDependencyResolver resolver)
        {
            var transportManager = resolver.Resolve<ITransportManager>() as TransportManager;
            transportManager.Remove("foreverFrame");
        }

        //public static void Configuration(IAppBuilder app)
        //{
        //    // If using a custom dependency resolver
        //    var resolver = GetCustomResolver();
        //    TurnOfForeverFrame(resolver);
        //    app.MapSignalR(new HubConfiguration
        //    {
        //        Resolver = resolver
        //    });
        //}
        //private static IDependencyResolver GetCustomResolver()
        //{
        //    return new DefaultDependencyResolver();
        //}
        //public static void TurnOfForeverFrame(IDependencyResolver resolver)
        //{
        //    var transportManager = resolver.Resolve<ITransportManager>() as TransportManager;
        //    transportManager.Remove("foreverFrame");
        //}
    }
}
