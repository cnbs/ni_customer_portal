﻿using Microsoft.AspNet.SignalR;

namespace SchindlerCustomerPortal.Hubs
{
    public class MyHub : Hub
    {
        [Authorize]
        public void SendMessage(string message)
        {
            Clients.All.addMessage(message);
        }
    }
}