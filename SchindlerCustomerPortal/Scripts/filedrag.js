/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function () {
    
    // getElementById
    function $id(id) {
        return document.getElementById(id);
    }


    // output information
    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = msg + m.innerHTML;
    }

    function Output1(msg) {
        var m = $id("messages");
        $("#messages ul").append(msg);
    }


    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }


    // file selection
    function FileSelectHandler(e) {

        // cancel event and hover styling
        FileDragHover(e);

        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        $('.hideshowss.hide').removeClass("hide");
        

       
        //$('.hideshow').setTimeOut(800000);
        
        setTimeout(function () {
            for (var i = 0, f; f = files[i]; i++) {
                //ParseFile(f);            
                //if (f.size <= $id("MAX_FILE_SIZE").value) {
                //Uploadfile(f);
                Uploadfile(f, i + 1, files.length);
                //}
                //else {
                //alert(f.name + " is too large, maximum file size is 4.0 MB");
                //}
            }
        }, 2000);
        // process all File objects
       

        //setTimeout(function () {
            
        //}, 2000);
        

    }


    // output file information
    function ParseFile(file) {
        var fileSize = 0;
        if (file.size > 1024 * 1024)
            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
        else
            fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';      
        if ($('#messages').find('ul').length == 0) {            
            Output(
                    "<ul><li><strong>" + file.name +
                    "</strong><strong>  " + fileSize +
                    "</li></ul>"
                );
        }
        else {            
            Output1(                    
                    "<li><strong>" + file.name +
                    "</strong><strong>  " + fileSize +
                    "</li>"
                );
        }
    }

    function Uploadfile(file,CurrentFileNumber,totalfiles) {
        
       
        var fd = new FormData();
        fd.append("files", file);
        
        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');
       
        $.ajax({
            beforeSend: function () {
                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
                
                
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function (xhr) {
                status.html(xhr.responseText);
            },
            type: "POST",
            url: '/CompanyAdmin/UploadAssets',
            data: fd,
            dataType: 'json',
            async: false,
            contentType: false,
            processData: false,
            cache: false,
           
            success: function (data) {
                
                
                if (totalfiles == CurrentFileNumber)
                {
                    
                    $('.hideshowss').addClass("hide");
                }
            },
        });
       
    }


    // initialize
    function Init() {
       
        var fileselect = $id("fileupload"),
			filedrag = $id("dragAndDropFiles");//submitbutton = $id("submitbutton");
      
        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);

        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
           
            var trident = !!navigator.userAgent.match(/Trident\/7.0/);          
            var net = !!navigator.userAgent.match(/.NET4.0E/);            
            var IE11 = trident && net
            var IEold = (navigator.userAgent.match(/MSIE/i) ? true : false);        
            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            if (IE11 || IEold) {
                filedrag.addEventListener("dragenter", FileSelectHandler, false);
            }
            filedrag.addEventListener("drop", FileSelectHandler, false);            
            filedrag.style.display = "block";

            // remove submit button
            //submitbutton.style.display = "none";
        }

    }

   
    Init();

})();