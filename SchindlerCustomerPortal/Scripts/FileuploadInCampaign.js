﻿/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function () {

    // getElementById
    function $id(id) {
        return document.getElementById(id);
    }


    // output information
    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = msg + m.innerHTML;
    }

    function Output1(msg) {
        var m = $id("messages");
        $("#messages ul").append(msg);
    }


    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }


    // file selection
    function FileSelectHandler(e) {

        // cancel event and hover styling
        FileDragHover(e);

        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            //ParseFile(f);            
            //if (f.size <= $id("MAX_FILE_SIZE").value) {
            Uploadfile(f);
            //}
            //else {
            //alert(f.name + " is too large, maximum file size is 4.0 MB");
            //}
        }

    }


    // output file information
    function ParseFile(file) {
        var fileSize = 0;
        if (file.size > 1024 * 1024)
            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
        else
            fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
        if ($('#messages').find('ul').length == 0) {
            Output(
                    "<ul><li><strong>" + file.name +
                    "</strong><strong>  " + fileSize +
                    "</li></ul>"
                );
        }
        else {
            Output1(
                    "<li><strong>" + file.name +
                    "</strong><strong>  " + fileSize +
                    "</li>"
                );
        }
    }

    function Uploadfile(file) {


        var fd = new FormData();
        fd.append("files", file);


        $.ajax({
            type: "POST",
            url: '/CompanyAdmin/UploadAssets',
            data: fd,
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {



            }
        });

    }


    // initialize
    function Init() {

        var fileselect = $id("fileupload"),
			filedrag = $id("dragAndDropFiles");//submitbutton = $id("submitbutton");

        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);

        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {

            var trident = !!navigator.userAgent.match(/Trident\/7.0/);
            var net = !!navigator.userAgent.match(/.NET4.0E/);
            var IE11 = trident && net
            var IEold = (navigator.userAgent.match(/MSIE/i) ? true : false);
            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            if (IE11 || IEold) {
                filedrag.addEventListener("dragenter", FileSelectHandler, false);
            }
            filedrag.addEventListener("drop", FileSelectHandler, false);
            filedrag.style.display = "block";

            // remove submit button
            //submitbutton.style.display = "none";
        }

    }


    Init();

})();