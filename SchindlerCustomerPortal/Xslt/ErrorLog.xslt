<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
        <h2>Errors</h2>
        <table border="1">
          <xsl:for-each select="Errors/Error">
            <tr>
              <td colspan="2" bgcolor="#E1DEF1">
                Error #: <xsl:value-of select="position()"/>
              </td>
            </tr>
            <tr>
              <td bgcolor="#C1CDCD">Source:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="Source"/>
              </td>
            </tr>
            <tr>
              <td bgcolor="#C1CDCD">Method:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="Method"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">Date Time:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="DateTime"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">Computer:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="Computer"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">ErrorInfo:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="ErrorInfo"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">StackTrace:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="StackTrace"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">Additional Info:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="AdditionalInfo"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">Application User:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="ApplicationUser"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">Window User Name:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="WindowUserName"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">IP Address:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="IPAddress"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">StoreProcedure:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="StoreProcedure"/>
              </td>
            </tr>

            <tr>
              <td bgcolor="#C1CDCD">URL:</td>
              <td bgcolor="#E0EEEE">
                <xsl:value-of select="URL"/>
              </td>
            </tr>







          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>

