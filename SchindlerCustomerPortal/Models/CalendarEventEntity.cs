﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class CalendarEventEntity
    {
        public int EventId { get; set; }
        public int CompanyId { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        [AllowHtml]
        public string Subject { get; set; }
        public string ProfilePic { get; set; }
        public int UserID { get; set; }

    }
}