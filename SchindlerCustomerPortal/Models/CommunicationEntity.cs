﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class CommunicationEntity
    {
        public int CompanyId { get; set; }
        public int CommunicationId { get; set; }
        public int ProjectNo { get; set; }
        public  string ProjectName { get; set; }
        public string JobName { get; set; }
        public string JobNo { get; set; }
        [AllowHtml]
        public string Message { get; set; }
        public string NotifyToEmail { get; set; }
        public string AttachmentPath { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string ProfilePic { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }

        public string ApplicationOperation { get; set; }
    }
}