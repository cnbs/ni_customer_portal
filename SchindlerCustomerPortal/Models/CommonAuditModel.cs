﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class CommonAuditModel
    {
        /// <summary>
        /// PrimaryKey Of Record
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Currently logged in User
        /// </summary>
        public int LoggedInUserid { get; set; }

        /// <summary>
        /// Audittrial Id
        /// </summary>
        public int AppAuditID{ get; set; }

        /// <summary>
        /// User Login Session
        /// </summary>
        public string SessionID { get; set; }

        

    }
}