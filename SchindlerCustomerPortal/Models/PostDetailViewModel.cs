﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class PostDetailViewModel
    {
        public int PostId { get; set; }
        public string PostTitle { get; set; }
        [AllowHtml]
        public string PostContent { get; set; }
        public int ThreadId { get; set; }
        public string ThreadType { get; set; } 
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string NotifyToEmail { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int ParentThreadId { get; set; }
        public int ThreadCount { get; set; }
        
        public int ParentPostid { get; set; }
        public List<PostDetailViewModel> ThreadList { get; set; }

    }
}