﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Project
{
    public class ProjectSearchInfoViewModel : BaseSearchInfo
    {
        public int ParentProjectId { get; set; }
    }
}