﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Project
{
    public class ProjectEntity 
    {
        public int ProjectID { get; set; }

        public int ParentProjectId { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string ProjectNo { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string AlternetProjectNo { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string ProjectName { get; set; }

        public string Company{ get; set; }
        
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string JobName { get; set; }

        public DateTime? AwardDate { get; set; }
        public bool? IsContractSigned   { get; set; }
        public DateTime? ContractReceivedDate { get; set; }
        public DateTime? BookingComplDate{ get; set; }
        public DateTime TurnOverDate { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string TeamAssigned { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public DateTime? OriginalNODDate { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string Proj_Mgr_Email { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string Job_Address { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string Job_State { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public string Job_City { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? Job_Zip { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? UnitCount { get; set; }

        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? GCSuperintendent { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? GCContactPerson { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? ContractorProjMgr { get; set; }


        public int? GCProjMgr{ get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public int? SchindlerSuperintendent { get; set; }

        public int? SchindlerSalesExecutive { get; set; }

        public int? SchindlerProjMgr{ get; set; }
        public Int64? CustomerNo { get; set; }


        public DateTime? NOD { get; set; }
        public bool? NODConfirm { get; set; }
        /// <summary>
        /// Edit Disabled
        /// </summary>
        public DateTime? NODConfirmDate { get; set; }
        public int  Status { get; set; }
        public string Statusstr { get; set; } 
        public DateTime? InceptionDate { get; set; }
        public string InceptionDatestr { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        //public bool? ProjectStatus { get; set; } 
        public string CheckAssigned { get; set; }
    }
    public class ProjectAssignEntity
    {
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
        public string Company { get; set; }
        public string CheckAssigned { get; set; }
        public string UnSubscribedCheckAssigned { get; set; }
        public bool IsUnSubscribedUserMst { get; set; }
    }

}