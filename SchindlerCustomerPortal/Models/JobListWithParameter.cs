﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class JobListWithParameter
    {
        public string JobNumber { get; set; }
        public string ProjectNumber { get; set; }
        public string Description { get; set; }
        public string ProductType { get; set; }
        public string Units { get; set; }
        public string Capacity { get; set; }
        public string Speed { get; set; }
        public string Voltage { get; set; }
        public string Travel { get; set; }
        public string DoorType { get; set; }
        public string DoorHand { get; set; }
        public string Floors { get; set; }
        public string Openings { get; set; }
        public string DrawingLink { get; set; }
        public string DocumentsLink { get; set; }
        public string PhotoLink { get; set; }

    }
}