﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class AnnouncementEntity
    {
        public int AnnMsgId { get; set; }
        [AllowHtml]
        public string MessageText { get; set; }
        public string Frequency { get; set; }
        public string DisplayOn { get; set; }
        public string ValidDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string ApplicationOperation { get; set; }
        public string Title { get; set; }

    }
}