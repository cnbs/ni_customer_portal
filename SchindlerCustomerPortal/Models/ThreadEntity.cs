﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class ThreadEntity
    {
        public int ThreadId { get; set; }
        public int ParentThreadId { get; set; }
        public int PostId { get; set; }
        public int ThreadType { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<NotifyToEmailcls> NotifyToEmail { get; set; }
        public string strNotifyToEmail { get; set; } 
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        public bool IsReply { get; set; }
        public int PostBy { get; set; }
        public string PostByEmail { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }

    }

    public class NotifyToEmailcls
    {
        public int id { get; set; }
        public string Email { get; set; }
    }
}