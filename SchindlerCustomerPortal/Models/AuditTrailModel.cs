﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class AuditTrailModel
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Datetime { get; set; }

        public string UserRole { get; set; }

        public string ModuleName { get; set; }

        public string Operation { get; set; }

        public string Action { get; set; }

        public string ClientIp { get; set; }

        public string ServerIp { get; set; }

        public string ClientBrowser { get; set; }

        public string ServerName { get; set; }

        public string SearchCriteria { get; set; }

        public string LogDateFrom { get; set; }
        public string LogDateTo { get; set; }
    }
}