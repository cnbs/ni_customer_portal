﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class SearchCompany
    {

        public string GroupID { get; set; }
        public int AppAuditID { get; set; }
        DateTime LogDate { get; set; }
        public string PageName { get; set; }
        public string Message { get; set; }
        public string UserID { get; set; }
        public int RoleID { get; set; }
        public string Operation { get; set; }
        public string SessionID { get; set; }
        public string IPAddress { get; set; }
    }
}