﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class DesignationEntity
    {
        public int DesignationId { get; set; }
        public string Designation { get; set; }
    }
}