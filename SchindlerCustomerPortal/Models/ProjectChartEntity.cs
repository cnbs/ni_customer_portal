﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class ProjectChartResult
    {
        public List<ProjectChartEntity> lstProjectChartEntity { get; set; }
        public List<ProjectChartMilestone> lstProjectChartMilestone { get; set; }
        public List<ProjectCount> lstProjectCount { get; set; }
    }

    public class DashboardChartDocAndContactData
    {
        public ProjectPieChartResult ProjectPieChartData { get; set; }
        public List<ProjectDocumentResult> lstProjectDocumentData { get; set; }
        public List<ProjectDocumentResult> lstProjectInvoiceData { get; set; }
        public List<ProjectContactResult> lstProjectContactData { get; set; }
    }

    public class ProjectPieChartResult
    {
        public int ReceivedDataValue { get; set; }
        public int BalanceOwedDataValue { get; set; }
    }

    public class ProjectDocumentResult
    {
        public int DocID { get; set; }
        public string DocName { get; set; }
        public string DocLink { get; set; }
    }

    public class ProjectContactResult
    {
        public string Name { get; set; }
        public string Email{ get; set; }

    }

    public class ProjectChartEntity
    {
        public string ProjectCount { get; set; }
        public string StatusName { get; set; }
    }
    public class ProjectChartMilestone
    {
        public string KeyData { get; set; }
        public string KeyValue { get; set; }
    }
    public class ProjectCount
    {
        public string SystemDate { get; set; }
        public string ProjCount { get; set; }
    }

    public class ProjectStatusDueDateEntity
    {
        public ProjectStatusDueDateEntity()
        {
            ProjectStatusDueDateList = new List<ProjectStatusDueDateEntity>();
        }

        public string DueDate { get; set; }
        public string ProjectNo { get; set; }
        public string CompletedDate { get; set; }
        public string StatusName { get; set; }
        public string JobNo { get; set; }
        public int Milestoneid { get; set; }
        public int StatusId { get; set; }
        public string Milestone { get; set; }
        public List<ProjectStatusDueDateEntity> ProjectStatusDueDateList { get; set; }
    }

    public class ProjectMilestoneStatusEntity
    {
        public ProjectMilestoneStatusEntity()
        {
            BankList = new List<ProjectMilestoneStatusEntity>();
        }

        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
        public string JobNo { get; set; }
        public string ContractExecuted { get; set; }
        public string DownPaymentRecord { get; set; }
        public string DrawingsApproved { get; set; }
        public string FinishesComplete { get; set; }
        public string InstallationStart { get; set; }
        public string ReadyToPull { get; set; }
        //public string MaterialAtHub { get; set; }
        public string MaterialInStorageHub { get; set; }
        public string MaterialOrdered { get; set; }
        //public string FinalInspection { get; set; }
        public string EquipmentTurnover { get; set; }
        
        public string Title { get; set; }
        public string DataType { get; set; }
        public string DueDate { get; set; }
        public int TotalProjectCount { get; set; }
        public int ProjectCompanyId { get; set; }
        public string PaidPercent { get; set; }
        public string BilledPercent { get; set; }


        public string ContractExecutedDate { get; set; }
        public string DownPaymentRecordDate { get; set; }
        public string DrawingsApprovedDate { get; set; }
        public string FinishesCompleteDate { get; set; }
        public string InstallationStartDate { get; set; }
        public string ReadyToPullDate { get; set; }
        //public string MaterialAtHub { get; set; }
        public string MaterialInStorageHubDate { get; set; }
        public string MaterialOrderedDate { get; set; }
        //public string FinalInspection { get; set; }
        public string EquipmentTurnoverDate { get; set; }

        public List<ProjectMilestoneStatusEntity> BankList { get; set; }
    }

}