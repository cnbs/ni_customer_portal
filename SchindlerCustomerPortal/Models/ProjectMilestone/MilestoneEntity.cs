﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.ProjectMilestone
{
    public class MilestoneEntity
    {
        public int MilestoneId { get; set; }
        public string MilestoneTitle { get; set; }

    }
}