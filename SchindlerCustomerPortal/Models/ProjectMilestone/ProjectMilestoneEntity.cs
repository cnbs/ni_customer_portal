﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.ProjectMilestone
{
    public class ProjectMilestoneEntity
    {
        public int MilestoneId { get; set; }
        public int MilestoneTitle { get; set; }
        public string MilestoneShortName { get; set; }
        public string MilestoneName { get; set; }
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }
        public int Predecessor { get; set; }
        public string PredecessorName { get; set; }
        public int Successor { get; set; }
        public string SuccessorName { get; set; }
        public string WaitingOn { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public Nullable<DateTime> DueDate { get; set; }
        public string Date { get; set; }
        public string DueDateStr { get; set; }
        public Nullable<DateTime> CompletedDate { get; set; }
        public string CompletedDateStr { get; set; }        
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string PublishedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string Exist { get; set; }

        public string PreviousStatusName { get; set; }
        public string ProjectName { get; set; }
        public string JobDesc { get; set; }

        public int UserID { get; set; }

        public string ApplicationOperation { get; set; }

        public string Note { get; set; }
        public string NewNote { get; set; }
        public string Color { get; set; }
    }

    public class ReadyToPullTemplateDetails
    {
        public int TemplateId { get; set; }
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }
        public string Comment { get; set; }
        public string Attachment { get; set; }
        public string RealAttachment { get; set; }
        public string AlreadyRealAttachment { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Boolean IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }

        public string ApplicationOperation { get; set; }
    }


    public class ReadyToPullTemplateMaster
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string ProjectNo { get; set; }
        public string JobNo { get; set; }
        public string Comment { get; set; }
        public string Attachment { get; set; }
        public string RealAttachment { get; set; }
        public Boolean Selected { get; set; }
    }
    public class GetReadyToPull
    {
        public List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails { get; set; }
    }

}