﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class PostEntity : BaseSearchInfo
    {
        public int PostId { get; set; }
        public int ProjectId { get; set; }
        public string PostTitle { get; set; }
         [AllowHtml]
        public string PostContent { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int PostTypeId { get; set; }
        public string PostType { get; set; }
        public int ParentPostId { get; set; }
        public List<NotifyToEmail> NotifyToEmail { get; set; }        
        public string Title { get; set; }
        [AllowHtml]
        public string Content{ get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
    }

    public class NotifyToEmail
    {
        public int id { get; set; }
        public string Email{ get; set; }
    }
}