﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class CompanyEntity
    {
        public int CompanyID { get; set; }
        public string AccountNumber { get; set; }
        public string ExternalAccountNumber { get; set; }
        public string CompanyName { get; set; }


        public string HeadOfficeAddress1 { get; set; }
        public string HeadOfficeAddress2 { get; set; }
        public int    HeadOfficeCountryID { get; set; }
        public string HeadOfficeStateCode { get; set; }
        public string HeadOfficeCityCode { get; set; }
        public string HeadOfficeZipCode { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public int    BillingCountryID { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingCityCode { get; set; }
        public string BillingZipCode { get; set; }
        public string InchargeFirstName { get; set; }
        public string InchargeLastName { get; set; }
        public string InchargeEmail { get; set; }
        public string InchargeDesignation { get; set; }
        public string InchargePhone { get; set; }
      
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonDesignation { get; set; }
        public string ContactPersonPhone { get; set; }
        public string CompanyLogo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool AllDelete { get; set; }
        public int OfflineAccessDuration { get; set; }
        public string CCEmailAddress { get; set; } 
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CompanyLogoFileName { get; set; }
        public int CheckCompany { get; set; }
      

        public string BillingPhoneNumber { get; set; }
        public string HeadOfficePhoneNumber { get; set; }
        public string ACCTManagerEmail { get; set; }
        public string ACCTManagerPhone { get; set; }
        public string ACCTManagerName { get; set; }
        public string PrimaryPhoneNumber { get; set; }

        public string IdList { get; set; }

        public string ApplicationOperation { get; set; }


    }
}