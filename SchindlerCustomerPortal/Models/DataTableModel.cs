﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class DataTableModel
    {
        public int ProjectID { get; set; }
        public int CompanyID { get; set; }
        public int UserID { get; set; }
        public string SearchTerm { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public string ProjectNumber { get; set; }

        //public int ProjectID { get; set; }

        public string UserEmail { get; set; }

        public int StatusValue { get; set; }


        //public int ProjectID { get; set; }

        public string DocumentType { get; set; }
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int RoleID { get; set; }

        public int ProjectNo { get; set; }
        public string JobNo { get; set; }

        public int ResourceId { get; set; }

        public int FlagValue { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string ApplicationOperation { get; set; }

        public string LogDateFrom { get; set; }
        public string LogDateTo { get; set; }

        public string RoleName { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
    }
}