﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Job
{
    public class JobEntity
    {
        public int JobId { get; set; }
        public string JobName { get; set; }
        public string JobNo { get; set; }
    }
}