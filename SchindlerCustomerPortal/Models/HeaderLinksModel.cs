﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class HeaderLinksModel
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleURL { get; set; }
        public int ParentID { get; set; }
        public string ModuleDisplayName { get; set; }
        public int SequenceNO { get; set; }
        public int Restricted { get; set; }
        public string ModulePermissionName { get; set; }
        public int VisibleOnMenu { get; set; }
        public bool hasChildMenu { get; set; }
    }

    public class HerderMenu
    {
        public List<UserPermissionViewModel> MenuItem { get; set; }
    }
}