﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class BaseSearchInfo
    {
        public int? PageNo { get; set; }
        public int? PageSize { get; set; }
        public SearchCriteria SearchCriteria { get; set; }
        public int TotalCount { get; set; }
    }
    public class SearchCriteria
    {
        public string SearchString { get; set; }
        public string SearchColumn { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
    }
}