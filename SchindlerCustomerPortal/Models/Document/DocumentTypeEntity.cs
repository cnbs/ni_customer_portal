﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Document
{
    public class DocumentTypeEntity
    {
        public int DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
    }
}