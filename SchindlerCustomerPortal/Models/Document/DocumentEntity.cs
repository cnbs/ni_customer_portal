﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Document
{
    public class DocumentEntity
    {
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public int DocumentTypeID { get; set; }
        public string DocumentType { get; set; }
        public string Description { get; set; }
        public string DocumentPath { get; set; }
        public string  ExpiredDate { get; set; }
        public string PublishedBy { get; set; }
        public Nullable<DateTime> PublishedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string StatusName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int StatusId { get; set; }
        public string FileType { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int JobId { get; set; }
        public string JobName { get; set; }
        public bool IsPublished { get; set; }
        public string PublishedStatus { get; set; }
        public string ExpiredDateString { get; set; }
        public string ApplicationOperation { get; set; }
        public int UserId { get; set; }
    }
}