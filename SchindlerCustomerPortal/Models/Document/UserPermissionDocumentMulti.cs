﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Document
{
    public class UserPermissionDocumentMulti
    {
        public int DocumentID { get; set; }


        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool View_Permission { get; set; }
        public bool Upload_Permission { get; set; }
        public bool Delete_Permission { get; set; }
        public bool Print_Permission { get; set; }
        public bool Edit_Permission { get; set; }

        public string UserIds { get; set; }
    }
}