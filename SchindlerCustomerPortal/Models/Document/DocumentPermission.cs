﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Document
{
    public class DocumentPermission
    {
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public bool HasViewPermission { get; set; }
        public bool HasUploadPermission { get; set; }
        public bool HasDeletePermission { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool isDeleted { get; set; }
    }
}