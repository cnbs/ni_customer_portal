﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Document
{
    public class UserPermissionDocumentModel
    {
        public int UserID { get; set; }
        public int DocumentID { get; set; }
        public string ColumnName { get; set; }
        public bool ColumnValue { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}