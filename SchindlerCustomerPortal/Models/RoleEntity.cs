﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class RoleEntity
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }
}