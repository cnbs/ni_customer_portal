﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models.Login
{
    public class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        //public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
       // public string[] roles { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public string SessionID { get; set; }
        public int CompanyID { get; set; }
        public string ProfilePic { get; set; }

        public string CompanyName { get; set; }
    }
}