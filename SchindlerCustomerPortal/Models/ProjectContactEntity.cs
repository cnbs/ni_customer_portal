﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/// <summary>
/// Class Name:SchindlerCustomerPortal.Models.ProjectContactEntity
/// Created By:manoj
/// Created On:11/16/2016 8:22 PM
/// Interfaces:None
/// BaseClass:None
/// Purpose:  
/// Version:       Version		When					Who					Why
/// 				1.0			11/16/2016 8:22 PM		manoj				Initial Draft.
/// </summary>
namespace SchindlerCustomerPortal.Models
{
   
    public class ProjectContactEntity
    {
        public int ContactID { get; set; }
        public int ProjectID { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public int DesignationId { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }

        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public int IsDeleted { get; set; }

        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string ContactIDList { get; set; }

        public int ProjectNo { get; set; }

        public string JobNo { get; set; }

        public string JobName { get; set; }

        public string ProjectName { get; set; }

        public string BankDesc { get; set; }

        public int FlagValue { get; set; }

        public string ApplicationOperation { get; set; }

        public int UserID { get; set; }
    }
}