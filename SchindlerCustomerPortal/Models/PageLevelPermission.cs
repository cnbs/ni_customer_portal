﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class PageLevelPermission
    {
        public int UserID { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }


        public bool HasViewPermission { get; set; }
        public bool HasAddPermission { get; set; }
        public bool HasEditPermission { get; set; }
        public bool HasDeletePermission { get; set; }
        public bool HasPrintPermission { get; set; }

        public string ModuleURL { get; set; }
        public string ModulePermissionName { get; set; }

        //public int appAuditID { get; set; }
        //public string SessionID { get; set; }
        //public string CreatedBy { get; set; }
        //public string UpdatedBy { get; set; }

        //public bool IsUpdate { get; set; }

        //public int ModuleId { get; set; }
        //public string ModuleName { get; set; }
        
        //public int ParentID { get; set; }
        //public string ModuleDisplayName { get; set; }
        public int SequenceNO { get; set; }
        public int Restricted { get; set; }
        public int VisibleOnMenu { get; set; }
    }
}