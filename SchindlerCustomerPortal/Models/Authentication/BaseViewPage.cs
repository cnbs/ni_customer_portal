﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models.Authentication
{
    /// <summary>
    /// a base class for all your views for accessing your User data in your all views
    /// </summary>
    public abstract class BaseViewPageBase : WebViewPage
    {

        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }
    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }
}