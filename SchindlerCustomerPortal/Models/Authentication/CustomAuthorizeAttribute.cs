﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SchindlerCustomerPortal.Models.Authentication
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string UsersConfigKey { get; set; }
        public string RolesConfigKey { get; set; }

        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    if (filterContext.HttpContext.Request.IsAuthenticated)
        //    {
        //        //var authorizedUsers = ConfigurationManager.AppSettings[UsersConfigKey];
        //        //var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

        //        //Users = String.IsNullOrEmpty(Users) ? authorizedUsers : Users;
        //        //Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;

        //        //if (!String.IsNullOrEmpty(Roles))
        //        //{
        //        //    if (!CurrentUser.IsInRole(Roles))
        //        //    {  
        //        //        filterContext.Result = new RedirectToRouteResult(new
        //        //        RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));

        //        //        // base.OnAuthorization(filterContext); //returns to login url
        //        //    }
        //        //}

        //        //if (!String.IsNullOrEmpty(Users))
        //        //{
        //        //    if (!Users.Contains(CurrentUser.UserId.ToString()))
        //        //    {
        //        //        filterContext.Result = new RedirectToRouteResult(new
        //        //        RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));

        //        //        // base.OnAuthorization(filterContext); //returns to login url
        //        //    }
        //        //}
        //    }

        //}

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var controllerName = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[1];

            var action = "";
            if (controllerName != "")
            {
                action = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[2];
            }

            if (CurrentUser != null)
            {
                if ((controllerName == "Login" || controllerName == "") && action != "Logout")
                {
                    var url = new UrlHelper(filterContext.RequestContext);
                    var redirectURL = url.Content("~/Dashboard/Index");
                    filterContext.HttpContext.Response.Redirect(redirectURL);
                }
            }
            else
            {
                //if (controllerName != "Login" || controllerName == "")
                //{
                //var url = new UrlHelper(filterContext.RequestContext);
                //var redirectURL = url.Content("~/Login/UserLogin");
                //filterContext.HttpContext.Response.Redirect(redirectURL);
                //}

            }
        }

    }


    public class UserPermissionAuthorizeAttribute : ActionFilterAttribute
    {
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CurrentUser != null)
            {
                //var userId = CurrentUser.UserId;
                if (CurrentUser.GetType().Module.Name != "SchindlerCustomerPortal.dll")
                {
                    HttpContext.Current.Response.Redirect("~/Login/UserLogin");
                }
                //var abc = filterContext;
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login/UserLogin");
            }
            
        }

    }

}