﻿using SchindlerCustomerPortal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace SchindlerCustomerPortal.Models.Authentication
{
    public class CustomPrincipal : IPrincipal
    {

        public IIdentity Identity { get; private set; }


        public bool IsSuperAdmin(string role)
        {
            if (roles.Any(r => role.Contains(r)) && role == UserRole.SuperAdmin.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool IsEmployee(string role)
        {
            if (roles.Any(r => role.Contains(r)) && role == UserRole.Employee.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool CustomerAdmin(string role)
        {
            if (roles.Any(r => role.Contains(r)) && role == UserRole.CustomerAdmin.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public bool Customer(string role)
        {
            if (roles.Any(r => role.Contains(r)) && role == UserRole.Customer.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsInRole(string role)
        {
            if (roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public CustomPrincipal(string Username)
        {
            this.Identity = new GenericIdentity(Username);
        }

        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int CompanyID { get; set; }
        public string ProfilePic { get; set; }
        public string SessionId { get; set; }

        public string CompanyName { get; set; }

        public string[] roles { get; set; }
    }
}