﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class AlertMessageEntity
    {
        public int AlertID { get; set; }
        public string ModuleName { get; set; }
        public string MessageName { get; set; }
        public string Message { get; set; }
        public string ServerSide { get; set; }
        public string ClientSide { get; set; }
        public string DictionaryValue { get; set; }
    }
}