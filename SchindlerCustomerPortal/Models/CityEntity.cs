﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class CityEntity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int CountryId { get; set; }
        public string StateCode { get; set; }
    }
}