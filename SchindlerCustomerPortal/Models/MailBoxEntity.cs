﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models
{
    public class MailBoxEntity
    {
        public int MailId { get; set; }
        public int CompanyId { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        [AllowHtml]
        public string Subject { get; set; }
        [AllowHtml]
        public string Message { get; set; }
        public string ViewStatus { get; set; }
        public int Status { get; set; }
        public bool HasAttachment { get; set; }
        public bool IsDeleted { get; set; }
        public string ToList { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string DateSent { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string UserName { get; set; }
        public int InboxUnreadCount { get; set; }
        public int DraftCount { get; set; }

        public int AttachmentId { get; set; }
        public string AttachmentPath { get; set; }
        public string Size { get; set; }

        public int InboxCount { get; set; }

        public string TotalFileName { get; set; }

        public string ForwardFileList { get; set; }

        public string ProfilePic { get; set; }

        public int ProjectNoCount { get; set; }

        public string ApplicationOperation { get; set; }

        public int UserID { get; set; }
        public string DeviceID { get; set; }
        public string DeviceLastLoginDateTime { get; set; }
        public string DeviceType { get; set; }
    }

    public class MailBoxAttachment
    {
        public int AttachmentId { get; set; }
        public string AttachmentPath { get; set; }
        public int MailId { get; set; }
    }

    public class MailCount
    {
        public int InboxUnreadCount { get; set; }
        public int DraftCount { get; set; }
    }

    public enum MailStatus
    {
        Inbox = 9,
        Draft = 10,
        Sent = 11,
        Delete = 12
    }
}