﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class EmailHistoryEntity
    {
        public int EmailHistoryID { get; set; }
        public string Subject { get; set; }
        public string EmailTo { get; set; }
        public string EmailFrom { get; set; }
        public string SenderFName { get; set; }
        public string SenderLName { get; set; }
        public DateTime DateTime { get; set; }
        public string EmailBody { get; set; }
        public string AttachmentFile { get; set; }
        public string Status { get; set; }
        public bool AllowReminder { get; set; }
        public DateTime ReminderDate { get; set; }
        public int CompanyID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}