﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Models
{
    public class StatusEntity
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string Module { get; set; }
    }
}