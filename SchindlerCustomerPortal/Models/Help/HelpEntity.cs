﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchindlerCustomerPortal.Models.Help
{
    public class HelpEntity
    {
        public int HelpId { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public bool PublishedStatus { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int appAuditID { get; set; }
        public string SessionID { get; set; }
        public string CreatedBy { get; set; }

        public string ApplicationOperation { get; set; }

    }
}