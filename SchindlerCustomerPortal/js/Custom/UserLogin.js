﻿function ValidEmailAddress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};

$(document).ready(function () {
    var cname='';
    var DisplayOn = 'before';
    var CookieName = 'OneTimeAnnouncement';
    $.ajax({
        url: '/Login/BeforeAfterLoginAnnouncement',
        type: 'GET',
        dataType: 'json',
        data: { DisplayOn: DisplayOn },
        async: false,
        success: function (data) {
            if (data.length != 0) {

                $('#headerAnnouncement').html('');

                var renderHTML = '';
                var AnnouncementList = data;
                var AnnMsgIds = '';
                cname = GetCookieValue(CookieName);
                if (cname == "") {

                    $.each(AnnouncementList, function (i, val) {
                        renderHTML += '<i class="fa fa-bullhorn announce-bullet" aria-hidden="true"></i><span>' + val.MessageText + '</span>';
                        renderHTML += '<div class="clearfix"></div><span class="announce-border"></span>';
                        if (val.Frequency == "OneTime")
                        {
                            AnnMsgIds += (AnnMsgIds == '') ? val.AnnMsgId : ',' + val.AnnMsgId;
                        }
                    })

                    document.cookie = $.trim("OneTimeAnnouncement=" + AnnMsgIds +";path=/");
                    if (renderHTML != "") {
                        $('#Announcement').modal('show');
                        $('#myModalLabelAnnouncement').html('Announcement');
                        $('#headerAnnouncement').html(renderHTML);
                        $('#headerAnnouncement').mCustomScrollbar();
                    }
                 
                }
                else
                {
                    $.each(AnnouncementList, function (i, val) {
                        var AnnMsgId = '' + val.AnnMsgId + '';
                        if ($.inArray(AnnMsgId, cname.split(',')) < 0) //doesn't exist
                        {
                            renderHTML += '<i class="fa fa-bullhorn announce-bullet" aria-hidden="true"></i><span>' + val.MessageText + '</span>';
                            renderHTML += '<div class="clearfix"></div><span class="announce-border"></span>';
                            
                        }
                        else
                        {
                            AnnMsgIds += (AnnMsgIds == '') ? val.AnnMsgId : ',' + val.AnnMsgId;
                        }
                    });

                    document.cookie = $.trim("OneTimeAnnouncement=" + AnnMsgIds + ";path=/");
                    if (renderHTML != "") {
                        $('#Announcement').modal('show');
                        $('#myModalLabelAnnouncement').html('Announcement');
                        $('#headerAnnouncement').html(renderHTML);
                        $('#headerAnnouncement').mCustomScrollbar();
                    }
                }
            }
        },
        error: function (data) {

        }

    })

    $('.modal').on('hidden.bs.modal', function () {

    });

    $('.modal').on('shown.bs.modal', function () {

    });
    
    var Redirect = $('#InvalidLoginID').val();

    if (Redirect == 'true'){
        $('#loginError').text('Please Enter Valid Email and Password !');
        $('#myLoginModal').modal('show');
    }

    if (Redirect == 'Your account is not Verified.'){
        $('#loginError').text('Your account is not Verified.');
        $('#myLoginModal').modal('show');
    }

    if (Redirect == 'Your account is not active.'){
        $('#loginError').text('Your account is not active.');
        $('#myLoginModal').modal('show');
    }

    var CheckRegistered = $('#RegisterMesaageId').val();
    if (CheckRegistered == 'Registered'){
        $('#myRegisterOnly').modal('show');
    }
    
    $("#validok").click(function () {
        $('#myLoginModal').modal('hide');
       // window.location.href = '/';
    });

    $("#Regok").click(function () {
        $('#myRegisterOnly').modal('hide');
        window.location.href = '/';
    });

    $("#myNotAllowed").click(function () {
        $('#myNotAllowedModal').modal('hide');
        window.location.href = '/';
    });

    $('#forgotbutton').click(function (e) {
        e.preventDefault(e);
        
        var forgotemail = $("#forgotemail").val().trim();

        if (forgotemail == "" || forgotemail == null) {
            $("#forgotemail").css({
                "border": "1px solid red"
            });
            return false;
        }
        else {
            $("#forgotemail").removeAttr('style');
        }

        if (!ValidEmailAddress($('#forgotemail'))) {
            $('#forgotemail').css('border-color', 'red');
            return false;
        }
        

        var data = new Object();
        data.EmailTo = forgotemail;

        $.ajax({
            url: '/Login/ForgotPassword',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (data)
            {
                
                if (data.message == 'Success') {
                    $("#forgotemail").val('');
                    $('#myModal').modal('hide');
                    $('#forgotPasswordError').text('Your password reset link has been sent to your registered email address!');
                    $('#mySuccessModal').modal('show');
                }
                if (data.message == 'Invalid') {
                    $("#forgotemail").val('');
                    $('#myModal').modal('hide');
                    $('#myInvalidModal').modal('show');
                }

                if (data.message == 'Not Exist') {
                    
                    var er = "we could not find your account with this emailid. Please contact customer administration for more information.";
                    $("#forgotemail").val('');
                    $('#myModal').modal('hide');
                    $('#notExist').text(er);
                    $('#EmailnotExist').modal('show');
                }
                if (data.message == 'EmailError') {

                    var er = "Email not sent due to some technical issue, Please contact system support!";
                    $("#forgotemail").val('');
                    $('#myModal').modal('hide');
                    $('#notExist').text(er);
                    $('#EmailnotExist').modal('show');
                }
                if (data.message == 'Pending') {

                    var er = "Your account is pending for approval. Once approved you will be able to create new password.";
                    $("#forgotemail").val('');
                    $('#myModal').modal('hide');
                    $('#notExist').text(er);
                    $('#EmailnotExist').modal('show');
                }
            },
            error: function (data)
            {
                $("#forgotemail").val('');
                $('#myModal').modal('hide');
                $('#myInvalidModal').modal('show');
                return false;
            }
        });
    })

    function successEmail(data) {
        
        
    };

    function errorEmail(data) {
        

    };

    $("#needLoginHelpTooltip").tooltip({ title: 'Help', placement: 'top' });

    var hdnVerify = $('#hdnVerifiyModal').val();
    if (hdnVerify != null && hdnVerify != "" && hdnVerify != undefined) {
        $('#VerifyModal').modal('show');
    }

    function GetCookieValue(cookiename)
    {
        var cookievalue='';
        var result = document.cookie;
        var cookieArray = result.split(";");
        for (var i = 0; i < cookieArray.length; i++) {
            var Name = $.trim(cookieArray[i].split("=")[0]);
            if (Name == cookiename)
            {
                cookievalue = cookieArray[i].split("=")[1];
            }

            //document.cookie = keyValArr[0] + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
        return cookievalue;
    }
   
});