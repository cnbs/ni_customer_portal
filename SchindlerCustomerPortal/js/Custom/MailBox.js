﻿var ToList = [];
var CcList = [];
var BccList = [];

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

function newMailFromDraft(mailId, statusValue) {
    $('#divPartial').load("/MailBox/NewMail/" + mailId, function () {

        $('#hdnMailId').val(mailId);
        //CKEDITOR.instances['txtMessage'].setData(modifiedMessage);
    });
}

function fillDataTable(val, searchTerm) {
    $('#headerlbl').text(val);
    var table = $('#dt_MailBox').DataTable();
    table.destroy();

    $('#dt_MailBox').empty();

    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var mbTable = $('#dt_MailBox').DataTable({
        "ajax": {
            "url": "/MailBox/GetAllMailByUser",
            "type": "POST",
            "datatype": "JSON",
            "data": {
                status: val
            }
        },

        "columnDefs": [{ 'bSortable': false, 'targets': [0, 3] }],
        'aaSorting': [[3, 'DESC']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        //"filter": true, // this is for disable filter (search box)
        //"orderMulti": false, // for disable multiple column at once
        "oLanguage": {
            "sSearch": "",
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "bInfo": false,
        "bDestroy": true,
        //"iTotalRecords": "3",
        //"iTotalDisplayRecords": "3",
        //"bJQueryUI": true,
        //"sDom": 'lfrtip',
        "columns": [
        {
            "data": "MailId",
            "name": "MailId",
            "render": function (data, type, row) {
                return '<input type="checkbox"  name="chkMail" id="chkMail_' + row.MailId + '">'
            },
            //"sClass": "mail-item"
        },
        {
            "data": "UserName",
            "name": "Sender",
            //"Searchable": true,
            "sClass": "view-message  dont-show",
            "width": "230px",
            //"sClass": "mail-user"
        },
        {
            "data": "Subject",
            "name": "Subject",
            //"Searchable": true,
            //"sClass": "mail-text",
            "width": "370px",
            "render": function (data, type, row) {
                //return '<a href="/mailbox/readmail?mail=' + row.MailId + '" onclick=viewMail(' + row.MailId + ')>' + data + '</a>'
                if (val == 'Draft') {
                    return '<a href="javascript:void(0)" class="mail-text" onclick=newMailFromDraft(' + row.MailId + ',"' + val + '")>' + data + '</a>'
                }
                else if (row.ViewStatus == 'unread') {
                    return '<b><a href="javascript:void(0)" class="mail-text" onclick=viewMail(' + row.MailId + ',"' + val + '")>' + data + '</a></b>'
                }
                else {
                    return '<a href="javascript:void(0)" class="mail-text" onclick=viewMail(' + row.MailId + ',"' + val + '")>' + data + '</a>'
                }
            },
        },
        //{
        //    "data": "HasAttachment",
        //    "name": "HasAttachment",
        //    "render": function (data, type, row) {
        //        if (data == true) {
        //            return '<i class="fa fa-paperclip"></i>'
        //        }
        //        else {
        //            return ''
        //        }
        //    },
        //    "sClass": "view-message  inbox-small-cells"
        //},
        {
            "data": "DateSent",
            "name": "DateSent",
            "sClass": "text-right"
        },


        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var sDirectionClass;
            if (aData[2] != "unread")
                sDirectionClass = "mail-item";
            else
                sDirectionClass = "mail-item  mail-unread mail-info ";

            $(nRow).addClass(sDirectionClass);
            return nRow;
        },
        "initComplete": function (settings, json) {
            dt_MailBoxDatatablePaging();
        },
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }


    })
    if (searchTerm != '' && searchTerm != undefined)
        mbTable.search(searchTerm).draw();
}

function dt_MailBoxDatatablePaging() {
    $('#dt_MailBox_paginate').children('ul').addClass('flatpagi');
    $('#dt_MailBox_previous').children('a').remove();
    $('#dt_MailBox_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#dt_MailBox_next').children('a').remove();
    $('#dt_MailBox_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function viewMail(mailId, statusValue) {
    $('#divPartial').load("/mailbox/readmail?mail=" + mailId + "&statusValue=" + statusValue + "", function () {
        $('#hdnMailId').val(mailId);
        mailboxCount();

    });
}

function ReplyMail(mailId, replyCheck) {

    var UserEmail = $('#hdnUserEmail').val();
    ToList = [];
    CcList = [];

    if (replyCheck == 'Reply') {
        //$('#ccDiv').hide(); 
        // $('#mail-cc').hide(); // Manoj 
        $('#mail-cc').addClass('hidden');
        //$('#bccDiv').hide(); 
        //$('#mail-bcc').hide();// Manoj 
        $('#mail-bcc').addClass('hidden');
    }
    else {
        //$('#bccDiv').hide();
        // $('#mail-bcc').hide();// Manoj 
        $('#mail-bcc').addClass('hidden');
    }

    $('#txtMessage').val('');

    //$('#LoadingImage').css('display', 'block');

    $.ajax({
        url: '/MailBox/MailReply',
        method: 'POST',
        async: false,
        data: { mail: mailId },
        success: function (data) {
            if (replyCheck == 'Reply') {
                $('#txtTo').val(data.Sender);
                ToList[ToList.length] = { Email: data.Sender, UserID: data.Sender }
            }

            if (replyCheck == 'ReplyAll') {

                var toEmailValues = data.Sender;
                ToList[ToList.length] = { Email: toEmailValues, UserID: toEmailValues }
                if (data.ToList != '') {
                    $.grep(data.ToList.split(','), function (e) {
                        if (UserEmail != e) {
                            toEmailValues = toEmailValues == '' ? e : toEmailValues + ',' + e;
                            ToList[ToList.length] = { Email: e, UserID: e }
                            return e;
                        }
                    });
                }

                $('#txtTo').val(toEmailValues);

                $('#txtCc').val(data.CC);
                if (data.CC != '') {
                    $.each(data.CC.split(','), function (key, val) {
                        if (CcList.length == 0) {
                            CcList[0] = { Email: val, UserID: val }
                        }
                        else {
                            CcList[CcList.length] = { Email: val, UserID: val }
                        }
                    })
                }
            }

            $('#txtSubject').val("RE : " + data.Subject);

            var sent = '<b>From: "' + data.Sender + '"</b>'
            var modifiedMessage = '<br><br><hr><b>From: </b>' + data.Sender + '<br><b>Sent: </b>' + data.DateSent + '<br><b>Subject: </b>' + data.Subject + '<br>' + data.Message;

            var message = CKEDITOR.instances['txtMessage'].setData(modifiedMessage);

            // $('#LoadingImage').css('display', 'block');

        },
        error: function (data) {
            //alert('error')
        }
    });
}

function ForwardMail(mailId) {

    $('#txtMessage').val('');
    //$('#ccDiv').hide();
    // $('#mail-cc').hide(); //manoj
    $('#mail-cc').addClass('hidden');
    //$('#bccDiv').hide();
    //$('#mail-bcc').hide();//manoj
    $('#mail-bcc').addClass('hidden');
    ToList = [];

    $.ajax({
        url: '/MailBox/MailReply',
        method: 'POST',
        async: false,
        data: { mail: mailId },
        success: function (data) {

            //$('#txtTo').val(data.ToList);
            //$('#txtCc').val(data.CC);
            //<span class="forPadding"><a href="javascript:void(0)" name="attachment" Fullpath="@item">@item</a></span>
            if (data.HasAttachment) {

                var arr = data.AttachmentPath.split('|');

                $.each(arr, function (i, val) {

                    $("<span class='forPadding'><a href='javascript:void(0)' name='attachment' id='removeFileId_" + i + "' class='removeFile' Fullpath='" + val + "'>" + val + "</a><a href='javascript:void(0)' name='attachmentRemove' Fullpath='" + val + "'><b>X</b></a></span>").appendTo("#attchFilename");
                });

                //$("<span class='forPadding'><a href='javascript:void(0)' name='attachment' Fullpath='" + data.AttachmentPath + "'>" + data.AttachmentPath + "</a></span>").appendTo("#attchFilename");

                $('#attachmentDiv').show();
            }

            $('#txtSubject').val("FW : " + data.Subject);

            var sent = '<b>From: "' + data.Sender + '"</b>'
            var modifiedMessage = '<br><br><hr><b>From: </b>' + data.Sender + '<br><b>Sent: </b>' + data.DateSent + '<br><b>Subject: </b>' + data.Subject + '<br>' + data.Message;

            var message = CKEDITOR.instances['txtMessage'].setData(modifiedMessage);
        },
        error: function (data) {
            //alert('error')
        }
    });
}

function HideElement() {
    $('.dataTables_filter input').hide();

    $('#dt_MailBox_length').hide();

    $('#dt_MailBox > thead').hide();

    //$('#mailBoxSearchbox').addClass('dataTables_filter');
}

function mailboxCount() {
    MailTooltip();
    $.ajax({
        url: '/MailBox/MailCountforRefresh',
        method: 'POST',
        //data: { mail: mailId },
        success: function (data) {

            $('#inboxCount').text(data.InboxUnreadCount);
            $('#draftCount').text(data.DraftCount);

            $('#headMailCount').html('(' + data.InboxUnreadCount + ' unread)');
        },
        error: function (data) {
            //alert('error')
        }

    })
}

Dropzone.autoDiscover = false;

$(document).ready(function () {

    //CKEDITOR.replace('txtMessage');

    var statusValue = '';

    var txtTooptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/MailBox/CompanyUserAutoComplete",
                data: '{ searchTerm:"' + $('#txtTo').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                            value: item.Email,
                            UserID: item.UserID
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {

            // insert userId into array
            if (ToList.length == 0) {
                ToList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
            }
            else {
                ToList[ToList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
            }

            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        },
        //change: function (event, ui) {
        //    if (!ui.item) {
        //        $(this).val("");
        //        //alert('');
        //        //$('#empty-message').show();
        //    } else {
        //        //$('#empty-message').hide();
        //    }
        //},
        minLength: 2,
    };

    var txtCCoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/MailBox/CompanyUserAutoComplete",
                data: '{ searchTerm:"' + $('#txtCc').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                            value: item.Email,
                            UserID: item.UserID
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            // insert userId into array
            if (CcList.length == 0) {
                CcList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
            }
            else {
                CcList[CcList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
            }

            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        },
        //change: function (event, ui) {
        //    if (!ui.item) {
        //        $(this).val("");
        //        //alert('');
        //        //$('#empty-message').show();
        //    } else {
        //        //$('#empty-message').hide();
        //    }
        //},
        minLength: 2,
    };

    var txtBccoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/MailBox/CompanyUserAutoComplete",
                data: '{ searchTerm:"' + $('#txtBcc').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                            value: item.Email,
                            UserID: item.UserID
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            // insert userId into array
            if (BccList.length == 0) {
                BccList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
            }
            else {
                BccList[BccList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
            }

            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        },
        //change: function (event, ui) {
        //    if (!ui.item) {
        //        $(this).val("");
        //        //alert('');
        //        //$('#empty-message').show();
        //    } else {
        //        //$('#empty-message').hide();
        //    }
        //},
        minLength: 2,
    };

    $(document).on('draw.dt', '#dt_MailBox', function () {
        dt_MailBoxDatatablePaging();
        MailTooltip();
    });

    // don't navigate away from the field on tab when selecting an item
    $(document).on("keydown.autocomplete", '#txtTo', function (event) {
        //if (event.keyCode === $.ui.keyCode.TAB &&
        //    $(this).autocomplete("instance").menu.active) {
        //    event.preventDefault();
        //}

        $(this).autocomplete(txtTooptions);

    })

    $(document).on("keydown.autocomplete", '#txtCc', function (event) {

        $(this).autocomplete(txtCCoptions);

    })

    $(document).on("keydown.autocomplete", '#txtBcc', function (event) {

        $(this).autocomplete(txtBccoptions);

    })

    statusValue = "Inbox";

    var searTerm = '';
    fillDataTable(statusValue, searTerm);

    HideElement();

    $('#txtTo').focus();
    if ($('#txtTo').val() != '') {
        ToList[ToList.length] = { Email: $('#txtTo').val(), UserID: $('#txtTo').val() }
    }


    //$(document).on("focus", '#txtTo', function (event) {

    //})

    //$('#txtSearchMail').unbind().keyup(function () {
    $(document).on('keyup', '#txtSearchMail', function (event) {

        var value = $(this).val();
        if (value.length > 2) {
            //$('#divPartial').load("/MailBox/CompanyMail", function () {
            //    fillDataTable(statusValue, value);
            //    HideElement();
            //});
            fillDataTable(statusValue, value);
            HideElement();
        }
        if (value == '') {
            //$('#divPartial').load("/MailBox/CompanyMail", function () {
            //    fillDataTable(statusValue, value);
            //    HideElement();
            //});
            fillDataTable(statusValue, value);
            HideElement();
        }
    });

    //$('.dataTables_filter input').unbind().keyup(function () {
    //    var value = $(this).val();
    //    if (value.length > 3) {
    //        //table.search(value).draw();
    //        table.search(value).draw();
    //    }
    //    if (value == '') {
    //        table.search(value).draw();
    //    }
    //});

    $(document).on('change', '#fileAttachment', function (event) {
        //var files = $("#fileAttachment").get(0).files;
        //var fileName = files[0].name;

        //$('#txtAttachmentValue').val(fileName);
        //$('#attachmentDiv').show();

        var files = event.target.files;

        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }

                $.ajax({
                    url: '/MailBox/MultipleFileUpload',
                    method: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        console.log(result);
                    },
                    error: function (xhr, status, p3, p4) {
                        //var err = "Error " + " " + status + " " + p3 + " " + p4;
                        //if (xhr.responseText && xhr.responseText[0] == "{")
                        //    err = JSON.parse(xhr.responseText).Message;
                        //console.log(err);
                    }
                });
            } else {
                alert("This browser doesn't support HTML5 file uploads!");
            }
        }

    });

    //$('#btnSendMail').click(function (event) {
    $(document).on('click', '#btnSendMail', function (event) {
        event.preventDefault();

        //$('#btnSendMail').attr('disabled', 'disabled');

        var isValid = true;

        var toCommaValue = '';
        var ccCommaValue = '';
        var bccCommaValue = '';
        var subjectValue = $('#txtSubject').val().trim();
        if (subjectValue == '') {
            //bootbox.alert('Please Enter Subject')
            $('#CommonErrorMessage').html('Please Enter Subject');
            $('#CommonErrorModal').modal('show');
            return false;
        }

        var toValue = $('#txtTo').val().trim();
        if (toValue != '') {

            var newToList = toValue.split(',');

            for (i = 0; i <= newToList.length; i++) {
                if (newToList[i] != "" && newToList[i] != " " && newToList[i] != undefined) {
                    toCommaValue = toCommaValue + ',' + newToList[i].trim();
                }
            }

            toCommaValue = toCommaValue.slice(1);
            /*Jigar Code
            var newToList = [];
            $.each(toValue.split(','), function (key, val) {
                newToList[newToList.length] = $.grep(ToList, function (e) {

                    if (val.trim() == e.Email) {
                        toCommaValue = toCommaValue == '' ? e.Email : toCommaValue + ',' + e.Email;
                        return e;
                    }
                });
            })

            */
        }
        else {
            //bootbox.alert('Please Enter Recipient Email')
            $('#CommonErrorMessage').html('Please Enter Recipient Email');
            $('#CommonErrorModal').modal('show');
            return false;
        }

        var ccValue = $('#txtCc').val().trim();
        if (ccValue != '') {

            var newCcList = ccValue.split(',');

            for (i = 0; i <= newCcList.length; i++) {
                if (newCcList[i] != "" && newCcList[i] != " " && newCcList[i] != undefined) {
                    ccCommaValue = ccCommaValue + ',' + newCcList[i].trim();
                }
            }

            ccCommaValue = ccCommaValue.slice(1);

            /*
            var newCcList = [];

            $.each(ccValue.split(','), function (key, val) {
                newCcList[newCcList.length] = $.grep(CcList, function (e) {

                    if (val.trim() == e.Email) {
                        ccCommaValue = ccCommaValue == '' ? e.Email : ccCommaValue + ',' + e.Email;
                        return e;
                    }
                });
            })
            */
        }


        var bccValue = $('#txtBcc').val().trim();
        if (bccValue != '') {

            var newBccList = bccValue.split(',');

            for (i = 0; i <= newBccList.length; i++) {
                if (newBccList[i] != "" && newBccList[i] != " " && newBccList[i] != undefined) {
                    bccCommaValue = bccCommaValue + ',' + newBccList[i].trim();
                }
            }

            bccCommaValue = bccCommaValue.slice(1);

            /*
            var newBccList = [];

            $.each(bccValue.split(','), function (key, val) {
                newBccList[newBccList.length] = $.grep(BccList, function (e) {

                    if (val.trim() == e.Email) {
                        bccCommaValue = bccCommaValue == '' ? e.Email : bccCommaValue + ',' + e.Email;
                        return e;
                    }
                });
            })
            */
        }

        //newToList = newToList.filter(function (e) { return e });
        //newToList = $.unique(newToList.sort()).sort();

        var hdnMailID = $('#hdnMailId').val();


        var formData = new FormData();
        //var files = $("#fileAttachment").get(0).files;
        //if (files[0] != null) {
        //    var fileName = files[0].name;
        //}

        var message = CKEDITOR.instances['txtMessage'].getData();

        if (message == '') {
            //bootbox.alert('Please Enter Message')
            $('#CommonErrorMessage').html('Please Enter Message');
            $('#CommonErrorModal').modal('show');
            return false;
        }

        //if ($('#txtTo').val() != '') {
        //    if (($('#txtTo').val().match(", $") || $('#txtTo').val().match(",$")) == null) {
        //        $('#txtTo').val($('#txtTo').val() + ', ');
        //    }
        //}

        //formData.append("img", files[0]);
        formData.append("Subject", subjectValue);
        formData.append("Message", message);
        formData.append("ToList", toCommaValue);
        formData.append("CC", ccCommaValue);
        formData.append("BCC", bccCommaValue);

        formData.append("MailId", hdnMailID);

        var forwardFileList = null;

        $(".removeFile").each(function (i, val) {

            var Fullpath = $(this).attr('Fullpath');
            if (forwardFileList == null) {
                forwardFileList = Fullpath;
            }
            else {
                forwardFileList += "|" + Fullpath;
            }
        })

        formData.append("ForwardFileList", forwardFileList);


        var fileNameList = null;
        var attachmentValid = false;

        if (myDropzone.getQueuedFiles().length > 0) {
            event.preventDefault();
            myDropzone.processQueue();

            myDropzone.on("successmultiple", function (file, responsenew) {

                //alert(responsenew);
                if (fileNameList == null) {
                    fileNameList = responsenew;
                }
                else {
                    fileNameList += "|" + responsenew;
                }
                attachmentValid = true;

                formData.append("TotalFileName", fileNameList);

                if (isValid) {
                    $('#LoadingImage').css('display', 'block');

                    $.ajax({
                        url: '/MailBox/SendMailToUser',
                        method: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            //alert('success')
                            if (data == "1") {
                                $('#LoadingImage').css('display', 'none');

                                $('#btnSendMail').attr('disabled', '');
                                $('#mailSentSuccess').modal('show');
                                //$('#btnInbox').click();
                                //mailboxCount();
                            } else if (data == "0") {
                                $('#LoadingImage').css('display', 'none');
                                $('#mailSentFailure').modal('show');
                            } else if (data == "-1") {
                                $('#LoadingImage').css('display', 'none');
                                $('#WrongMail').modal('show');
                            }

                        },
                        error: function (data) {
                            //alert('error')
                        }
                    })

                }
                else {
                    $('#btnSendMail').attr('disabled', '');
                    return false;
                }

            });

        }
        else {

            //isValid = false;

            formData.append("TotalFileName", fileNameList);

            if (isValid) {
                $('#LoadingImage').css('display', 'block');

                $.ajax({
                    url: '/MailBox/SendMailToUser',
                    method: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data == "1") {
                            $('#LoadingImage').css('display', 'none');
                            $('#btnSendMail').attr('disabled', '');
                            $('#mailSentSuccess').modal('show');
                            //$('#btnInbox').click();
                            //mailboxCount();
                        } else if (data == "0") {
                            $('#LoadingImage').css('display', 'none');
                            $('#mailSentFailure').modal('show');
                        } else if (data == "-1") {

                            $('#LoadingImage').css('display', 'none');
                            $('#WrongMail').modal('show');
                        }
                    },
                    error: function (data) {
                        //alert('error')
                    }
                })

            }
            else {
                $('#btnSendMail').attr('disabled', '');
                return false;
            }
        }

    })

    //$('#btnDraft').click(function () {
    $(document).on('click', '#btnDraft', function () {
        // $('#divPartial').html('');
        $('#btnDraft').addClass('active');
        $('#btnDelete').removeClass('active');
        $('#btnInbox').removeClass('active');
        $('#btnSent').removeClass('active');
        var value = '';
        statusValue = 'Draft';
        $('#divPartial').load("/MailBox/CompanyMail", function () {
            fillDataTable(statusValue, value);
            HideElement();
            mailboxCount();
        });
    })

    //$('#btnDraft').click(function () {
    $(document).on('click', '#btnDelete', function () {
        // $('#divPartial').html('');
        $('#btnDelete').addClass('active');
        $('#btnInbox').removeClass('active');
        $('#btnDraft').removeClass('active');
        $('#btnSent').removeClass('active');
        var value = '';
        statusValue = 'Delete';
        $('#divPartial').load("/MailBox/CompanyMail", function () {
            fillDataTable(statusValue, value);
            HideElement();
            mailboxCount();
        });
    })


    //$('#btnSent').click(function () {
    $(document).on('click', '#btnSent', function () {
        // $('#divPartial').html('');
        $('#btnSent').addClass('active');
        $('#btnDraft').removeClass('active');
        $('#btnDelete').removeClass('active');
        $('#btnInbox').removeClass('active');

        var value = '';
        statusValue = 'Sent';
        $('#divPartial').load("/MailBox/CompanyMail", function () {
            fillDataTable(statusValue, value);
            HideElement();
            mailboxCount();
        });

    })

    //$('#btnInbox').click(function () {
    $(document).on('click', '#btnInbox', function () {
        // $('#divPartial').html('');
        $('#btnInbox').addClass('active');
        $('#btnSent').removeClass('active');
        $('#btnDraft').removeClass('active');
        $('#btnDelete').removeClass('active');

        var value = '';
        statusValue = 'Inbox';
        $('#divPartial').load("/MailBox/CompanyMail", function () {
            fillDataTable(statusValue, value);
            HideElement();
            mailboxCount();
        });
    })

    //$('#btnNewMail').click(function () {
    $(document).on('click', '#btnNewMail', function () {
        ToList = [];
        $('#divPartial').load("/MailBox/NewMail", function () {
            mailboxCount();

        });
    })

    $(document).on('click', '#btnReply', function () {

        //delete CKEDITOR.instances['txtMessage'];
        $('#divPartial').load("/MailBox/NewMail", function () {
            var iddd = $('#hdnMailId').val();
            ReplyMail(iddd, 'Reply');
            mailboxCount();
        });
    })

    $(document).on('click', '#btnReplyAll', function () {

        //delete CKEDITOR.instances['txtMessage'];
        $('#divPartial').load("/MailBox/NewMail", function () {
            var iddd = $('#hdnMailId').val();
            ReplyMail(iddd, 'ReplyAll');
            mailboxCount();
        });
    })

    $(document).on('click', '#btnForward', function () {

        // CKEDITOR.instances['txtMessage'];
        $('#divPartial').load("/MailBox/NewMail", function () {
            var iddd = $('#hdnMailId').val();
            ForwardMail(iddd);
            mailboxCount();
        });
    })

    $(document).on('click', '#btnUpSend', function () {
        $('#btnSendMail').click();
    })

    var mailIdList = [];

    //$('#chkAllMail').change(function () {
    $(document).on('change', '#chkAllMail', function (event) {
        if ($(this).is(":checked")) {
            $('input[name="chkMail"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                mailIdList.push(UserID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkMail"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];

                mailIdList = jQuery.grep(mailIdList, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $(document).on('change', 'input[name^="chkMail"]', function (event) {

        if ($(this).is(":checked")) {
            //$('input[name="chkCompany"]').each(function () {
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var UserID = result[1];
            mailIdList.push(UserID);
            this.checked = true;
            //});
        }
        else {
            //$('input[name="chkCompany"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var UserID = result[1];

            mailIdList = jQuery.grep(mailIdList, function (value) {
                return value != UserID;
            });
            //});
        }
    })

    //$('#btnAllDelete').click(function (event) {
    $(document).on('click', '#btnAllDelete', function (event) {
        event.preventDefault();


        var IsValiddd = true;
        if (mailIdList.length == 0) {
            IsValiddd = false;
            //alert('please select mail for delete')
            $('#CommonErrorMessage').html('Please select mail for delete');
            $('#CommonErrorModal').modal('show');
            $('#dt_MailBox_filter').hide();

        }

        var result = mailIdList.join(", ")

        if (IsValiddd) {
            if (statusValue != 'Delete') {
                $.ajax({
                    type: "POST",
                    url: "/MailBox/DeleteUserMail",
                    data: { mailID: result },
                    success: function (userdetail) {
                        $('#chkAllMail').prop('checked', false);
                        $('#mailDeleteSuccess').modal('show');
                        //var value = '';
                        //fillDataTable(statusValue, value);
                        //HideElement();
                        //mailboxCount();
                    }
                })
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "/MailBox/DeleteUserMailLogical",
                    data: { mailID: result },
                    success: function (userdetail) {
                        $('#chkAllMail').prop('checked', false);
                        $('#mailDeleteSuccess').modal('show');
                        //var value = '';
                        //fillDataTable(statusValue, value);
                        //HideElement();
                        //mailboxCount();
                    }
                })
            }

        }
    })

    $(document).on('click', '#btnComposeDelete', function (event) {

        event.preventDefault();
        var iddd = $('#hdnMailId').val();

        $.ajax({
            type: "POST",
            url: "/MailBox/DeleteUserMail",
            data: { mailID: iddd },
            success: function (userdetail) {
                //$('#chkAllMail').prop('checked', false);
                $('#btnInbox').click();
            }
        })
    })

    $(document).on('click', '#btnSaveDraft', function (event) {
        event.preventDefault();

        var isValid = true;

        var toCommaValue = '';
        var ccCommaValue = '';
        var bccCommaValue = '';
        var subjectValue = $('#txtSubject').val().trim();
        if (subjectValue == '') {
            //bootbox.alert('Please Enter Subject')
            $('#CommonErrorMessage').html('Please Enter Subject');
            $('#CommonErrorModal').modal('show');
            return false;
        }

        debugger
        var toValue = $('#txtTo').val().trim();
        if (toValue != '') {
            var newToList = toValue.split(',');

            for (i = 0; i <= newToList.length; i++) {
                if (newToList[i] != "" && newToList[i] != " " && newToList[i] != undefined) {
                    toCommaValue = toCommaValue + ',' + newToList[i];
                }
            }

            toCommaValue = toCommaValue.slice(1);

            //$.each(toValue.split(','), function (key, val) {
            //    newToList[newToList.length] = $.grep(ToList, function (e) {

            //        if (val == e.Email) {
            //            toCommaValue = toCommaValue == '' ? e.Email : ',' + e.Email;
            //            return e;
            //        }
            //    })[0];
            //})
        }
        //else {
        //    bootbox.alert('Please Enter Recipient Email')
        //    return false;
        //}


        var ccValue = $('#txtCc').val().trim();
        if (ccValue != '') {

            var newCcList = ccValue.split(',');

            for (i = 0; i <= newCcList.length; i++) {
                if (newCcList[i] != "" && newCcList[i] != " " && newCcList[i] != undefined) {
                    ccCommaValue = ccCommaValue + ',' + newCcList[i];
                }
            }

            ccCommaValue = ccCommaValue.slice(1);

            /*Old Code -- Jigar
            var newCcList = [];

            $.each(ccValue.split(','), function (key, val) {
                newCcList[newCcList.length] = $.grep(CcList, function (e) {

                    if (val == e.Email) {
                        ccCommaValue = ccCommaValue == '' ? e.Email : ',' + e.Email;
                        return e;
                    }
                })[0];
            })
            */
        }


        var bccValue = $('#txtBcc').val().trim();
        if (bccValue != '') {

            var newBccList = bccValue.split(',');

            for (i = 0; i <= newBccList.length; i++) {
                if (newBccList[i] != "" && newBccList[i] != " " && newBccList[i] != undefined) {
                    bccCommaValue = bccCommaValue + ',' + newBccList[i];
                }
            }

            bccCommaValue = bccCommaValue.slice(1);

            /*Old Code -- Jigar
            var newBccList = [];

            $.each(bccValue.split(','), function (key, val) {
                newBccList[newBccList.length] = $.grep(BccList, function (e) {

                    if (val == e.Email) {
                        bccCommaValue = bccCommaValue == '' ? e.Email : ',' + e.Email;
                        return e;
                    }
                })[0];
            })
            */
        }

        //newToList = newToList.filter(function (e) { return e });
        //newToList = $.unique(newToList.sort()).sort();


        var formData = new FormData();
        //var files = $("#addProfile").get(0).files;
        //if (files[0] != null) {
        //    var fileName = files[0].name;
        //}
        //else {
        //    //isValid = false;
        //    var fileName = "no_images.png";
        //}

        var message = CKEDITOR.instances['txtMessage'].getData();

        if (message == '') {
            //bootbox.alert('Please Enter Message')
            $('#CommonErrorMessage').html('Please Enter Message');
            $('#CommonErrorModal').modal('show');
            return false;
        }

        //formData.append("img", files[0]);
        formData.append("Subject", subjectValue);
        formData.append("Message", message);
        formData.append("ToList", toCommaValue);
        formData.append("CC", ccCommaValue);
        formData.append("BCC", bccCommaValue);

        var hdnMailID = $('#hdnMailId').val();
        formData.append("MailId", hdnMailID);
        debugger
        if (isValid) {
            $.ajax({
                url: '/MailBox/DraftUserMail',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data == "1") {
                        $('#LoadingImage').css('display', 'none');
                        $('#btnInbox').click();
                        mailboxCount();
                        //$('#btnInbox').click();
                        //mailboxCount();
                    } else if (data == "0") {
                        $('#LoadingImage').css('display', 'none');
                        $('#mailSentFailure').modal('show');
                    } else if (data == "-1") {
                        $('#LoadingImage').css('display', 'none');
                        $('#WrongMail').modal('show');
                    }

                },
                error: function (data) {
                    //alert('error')
                }
            })
        }
        else {
            return false;
        }


    })

    $(document).on('click', '#btnCancel', function (event) {

        $('#btnInbox').click();

    })

    $('#dt_MailBox_length').hide();


    window.setInterval(function () {
        /// call your function here
        mailboxCount();
    }, 30000);

    $(document).on('click', '#btnAllRefresh', function (event) {
        var value = '';
        fillDataTable(statusValue, value);
        HideElement();
        mailboxCount();
    })

    //var myDropzone = new Dropzone("div#previews", { // Make the whole body a dropzone
    //    url: "/MailBox/MultipleFileUpload", // Set the url
    //    previewsContainer: "#previews", // Define the container to display the previews
    //    clickable: "#btnAttachment", // Define the element that should be used as click trigger to select files.
    //    maxFiles: 5,
    //    acceptedFiles: "image/*,application/pdf",
    //    autoProcessQueue: false
    //});

    $(document).on('click', 'a[name=attachment]', function () {
        var Fullpath = $(this).attr('Fullpath');

        window.location.href = '/MailBox/DownloadAttachement?attachmentPath=' + escape(Fullpath);
    });

    $(document).on('click', 'a[name=attachmentRemove]', function () {

        var Fullpath = $(this).attr('Fullpath');
        //window.location.href = '/MailBox/DownloadAttachement?attachmentPath=' + escape(Fullpath);
        //$(this).closest('.removeFile').attr("Fullpath");
        $("a[Fullpath='" + Fullpath + "']").remove();
    });

    $(document).on('click', '#ccLink', function (event) {
        //mail-cc

        if ($('#mail-cc').hasClass('hidden')) {
            $('#mail-cc').removeClass('hidden');

        }
        else {
            $('#mail-cc').addClass('hidden');
        }
    });

    $(document).on('click', '#bccLink', function (event) {
        //mail-cc

        if ($('#mail-bcc').hasClass('hidden')) {
            $('#mail-bcc').removeClass('hidden');
        }
        else {
            $('#mail-bcc').addClass('hidden');
        }
    });

    $(document).on('click', '#mailSentSuccessOK', function (event) {
        $('#btnInbox').click();
        mailboxCount();
    });

    $(document).on('click', '#mailDeleteSuccessOK', function (event) {
        var value = '';
        fillDataTable(statusValue, value);
        HideElement();
        mailboxCount();
    })


    MailTooltip();
})


function MailTooltip() {

    $("#btnNewMail").tooltip({ title: 'New Mail', placement: 'top' });
    $("button[name=btnSaveDraft]").tooltip({ title: 'Save Draft', placement: 'top' });
    $("#btnUpSend").tooltip({ title: 'Send Message', placement: 'top' });
    $("#btnSendMail").tooltip({ title: 'Send Message', placement: 'top' });

    $("#ccLink").tooltip({ title: 'CC', placement: 'top' });
    $("#bccLink").tooltip({ title: 'BCC', placement: 'top' });
    $("#btnAllDelete").tooltip({ title: 'Delete', placement: 'top' });
    $("#btnComposeDelete").tooltip({ title: 'Delete', placement: 'top' });
    $("#btnReply").tooltip({ title: 'Reply', placement: 'top' });
    $("#btnForward").tooltip({ title: 'Forward', placement: 'top' });

}