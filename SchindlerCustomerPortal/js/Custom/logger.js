﻿
(function ($) {

    
    $.fn.logger = function () {
        window.onerror = function (errorMessage, errorUrl, errorLine) {
            var objdate = new Date();
            jQuery.ajax({
                type: 'POST',
                url: '/Services/LoggerService.asmx/ErrorLogData',
                contentType: "application/json",
                data: JSON.stringify({
                    "errorMessage": errorMessage,
                    "errorUrl": errorUrl,
                    "errorLine": errorLine

                }),
                success: function () {

                    if (console && console.log) {
                        console.log('JS error report successful.');
                    }
                },
                error: function () {
                    if (console && console.error) {
                        console.error('JS error report submission failed!');
                    }
                }
            });

            return true;
        }
    }
} (jQuery));