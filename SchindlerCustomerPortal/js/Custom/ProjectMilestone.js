﻿var MileOldvalue;
var PreOldvalue;
var SucOldValue;
var storedStatus;
function convertjsonOnlydate(anydate) {
    if (anydate != null) {
        var dateString = anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}
function editMilestone(id) {


    $('#EditMilestoneDiv').show();
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");

    //umesh
    $.ajax({
        type: "GET",
        url: "/ProjectMilestones/EditMilestone/",
        data: { mileId: id },
        success: function(milestonedetails) {


            $('#editHiddenMilestoneid').val('');
            $('#editPredecessorid').val('');
            $('#editSuccessorid').val('');

            $("select#editmilestonetype").html(options);
            $("select#editpredecessortype").html(options);
            $("select#editsuccessortype").html(options);


            $('#editpredecessortype').val($("#editpredecessortype option:first").val());
            $('#editsuccessortype').val($("#editsuccessortype option:first").val());

            $('#editmilestonetype').val($("#editmilestonetype option:first").val());
            // $('#editpredecessortype').val($("#editpredecessortype option:first").val());
            //$('#editsuccessortype').val($("#editsuccessortype option:first").val());
            //$('#editstatustype').val($("#editstatustype option:first").val());
            $('#Editduedate').val("");
            $('#editWaitingon').val("");


            //var Duedate = milestonedetails.Duedate;
            //var ddate = eval(Duedate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
            var ddate;
            if (milestonedetails.DueDateStr != null && milestonedetails.DueDateStr != undefined) {
                ddate = milestonedetails.DueDateStr;
                ddate = ddate.split(' ')[0];
            }

            var cdate;
            if (milestonedetails.CompletedDateStr != null && milestonedetails.CompletedDateStr != undefined) {
                cdate = milestonedetails.CompletedDateStr;
                cdate = cdate.split(' ')[0];
            }

            //var Publishdate = convertjsonOnlydate(documentdetails.PublishedOn);
            $('#editMilestoneid').val(milestonedetails.MilestoneId);

            //Remove dropdown value of on Milestone value
            $('#editmilestonetype').val(milestonedetails.MilestoneTitle);
            var editMilevalue = $('#editmilestonetype').val();
            $("#editpredecessortype option[value=" + editMilevalue + "]").remove();
            //$("#predecessortype option[value=" + SucciderValue + "]").remove();
            $("#editsuccessortype option[value=" + editMilevalue + "]").remove();
            //$("#successortype option[value=" + PredValue + "]").remove();

            var editPreValue = 0;
            if (milestonedetails.Predecessor == 0) {
                $('#editpredecessortype').val("NULL");
                editPreValue = $('#editpredecessortype').val();
            } else {

                $('#editpredecessortype').val(milestonedetails.Predecessor);
                editPreValue = $('#editpredecessortype').val();
                $("#editmilestonetype option[value=" + editPreValue + "]").remove();
                $("#editsuccessortype option[value=" + editPreValue + "]").remove();
            }


            var editSuccValue = 0;
            if (milestonedetails.Successor == 0) {
                $('#editsuccessortype').val("NULL");
                editSuccValue = $('#editsuccessortype').val();
            } else {

                $('#editsuccessortype').val(milestonedetails.Successor);
                editSuccValue = $('#editsuccessortype').val();
                $("#editmilestonetype option[value=" + editSuccValue + "]").remove();
                $("#editpredecessortype option[value=" + editSuccValue + "]").remove();
            }


            $('#Editduedate').val(ddate);
            $('#editWaitingon').val(milestonedetails.WaitingOn);
            $('#editstatustype').val(milestonedetails.Status);
            storedStatus = milestonedetails.Status;


            $('#editHiddenMilestoneid').val(editMilevalue);
            $('#editPredecessorid').val(editPreValue);
            $('#editSuccessorid').val(editSuccValue);


            MileOldvalue = $("#editmilestonetype").val();
            PreOldvalue = $('#editpredecessortype').val();
            SucOldValue = $("#editsuccessortype").val();


            $('#editmilestonetype').parents('.form-group').addClass('readonly');
             
            if ($('#editmilestonetype option:selected').text().trim() != 'Job-Site Ready') {
                $('#divReadyToPull').removeClass('input-group');
                $('#readyToPull').hide();
            } else {
                $('#divReadyToPull').addClass('input-group');
                $('#readyToPull').show();
            }

        }
    });
}
$('#btnDeleteMilestone').click(function () {

    $('#EditMilestoneDiv').hide();
    $('#deletesinglemilestone').modal('show');
    


});
function DocumentDatatablePaging() {
    $('#Milestonetable_paginate').children('ul').addClass('flatpagi');
    $('#Milestonetable_previous').children('a').remove();
    $('#Milestonetable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Milestonetable_next').children('a').remove();
    $('#Milestonetable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}
var ProjectNo;
var JobNo="";
var options = null;
var statusoptions = null;
$(document)
    .ready(function() {

        ProjectNo = $('#hdnProjectNo').val();

        $('#EditMilestoneDiv').hide();


        var hdnPageSize = parseInt($('#hdnPageSize').val());

        var RoleId = $('#hdnUserRoleId').val();


        var CompanyId = $('#hdnUserCompanyId').val();

        var model = new Object();

        model.ProjectNumber = ProjectNo;


       
        var table = $('#Milestonetable')
            .DataTable({
                "ajax": {
                    "url": "/ProjectMilestones/GetMilestoneAll",
                    "type": "POST",
                    "datatype": "JSON",
                    "data": model

                },

                "autoWidth": false, // for disabling autowidth
                "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
                "processing": true, // for show progress bar
                "serverSide": true, // for process server side
                "filter": true, // this is for disable filter (search box)
                "orderMulti": false, // for disable multiple column at once
                "iTotalRecords": "3",
                "iTotalDisplayRecords": "3",
                "oLanguage": {
                    "sEmptyTable": "No data available",
                    "sProcessing": "<img src='/img/spinner.gif'> Loading.."
                },
                "columns": [
                    {
                        "data": "MilestoneName",
                        "name": "MilestoneTitle",
                        "Searchable": true,
                    },
                    //{ "data": "PredecessorName", "name": "Predecessor", "Searchable": true },
                    //{ "data": "SuccessorName", "name": "Successor", "Searchable": true },
                    {
                        "data": "DueDate",
                        "name": "DueDate",
                        "sClass": "txtAlignCenter",
                        "render": function(data, type, row) {

                            return convertjsonOnlydate(data)
                        }
                    },
                    { "data": "StatusName", "name": "StatusName", "sClass": "txtAlignCenter" },
                    {
                        "data": "WaitingOn",
                        "name": "WaitingOn",

                    },
                    {
                        "data": "MilestoneId",
                        "sClass": "txtAlignCenter",
                        "render": function(data, type, row) {
                            return '<a id="' +
                                data +
                                '"  name="EditDocumentButtonname" class="btnsml_action e-edit" href="javascript:void(0)" onclick="editMilestone(' +
                                data +
                                ')"> </a>'
                        }
                    }
                ],
                createdRow: function(row, data, index) {
                    $(row).addClass('table-click-ProjectMilestone editData');

                },
                "initComplete": function(settings, json) {
                    DocumentDatatablePaging();
                    ProjectMilestoneTooltip();
                },
                "drawCallback": function(settings) {
                    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                    pagination.toggle(this.api().page.info().pages > 1);
                }
            });

        $('#Milestonetable')
            .on('draw.dt',
                function() {
                    DocumentDatatablePaging();
                    ProjectMilestoneTooltip();
                });


        $('#AddMilestonePopupClose')
            .click(function() {
                $('#EditMilestoneDiv').hide();
                $("select#milestonetype").html(options);
                //$("select#predecessortype").html(options);
                //$("select#successortype").html(options);

                $('#addMilestoneid').val('');
                $('#addPredecessorid').val('');
                $('#addSuccessorid').val('');

                $('#milestonetype').val($("#milestonetype option:first").val());
                //$('#predecessortype').val($("#predecessortype option:first").val());
                //$('#successortype').val($("#successortype option:first").val());
                $('#statustype').val($("#statustype option:first").val());
                $('#addWaitingon').val("");
                $('#Addduedate').val("");
                 $('#Addcompleteddate').val("");
                

            });


        function successdoctype(data) {
            var obj = data;
        
            options = '<option name = "miletypeoption" value="NULL">---Select---</option>';
            for (var i = 0; i < obj.length; i++) {

                options += '<option name = "miletypeoption" value="' +
                    obj[i].MilestoneId +
                    '">' +
                    obj[i].MilestoneTitle +
                    '</option>';
            }

            //Add Milestone
            $("select#milestonetype").html(options);

            //$("select#predecessortype").html(options);

            //$("select#successortype").html(options);

            //Edit Milestone
            $("select#editmilestonetype").html(options);

            $("select#editpredecessortype").html(options);

            $("select#editsuccessortype").html(options);


            //RemoveDropDuplicatevalues();
        };

        function errordoctype(data) {

            return false;

        };

        $.ajax({
            url: '/Common/BindMilestoneType',
            type: 'GET',
            success: successdoctype,
            error: errordoctype
        });

        $.ajax({
            url: '/Common/BindStatusList',
            type: 'GET',
            data: { module: 'Milestone' },

            success: successmiletype,
            error: errormiletype
        });

        function successmiletype(data) {
            var obj = data;

            statusoptions = '<option name = "statustypeoption" value="NULL">---Select---</option>';
            for (var i = 0; i < obj.length; i++) {

                statusoptions += '<option name = "statustypeoption" value="' +
                    obj[i].StatusId +
                    '">' +
                    obj[i].StatusName +
                    '</option>';
            }
            $("select#statustype").html(statusoptions);

            $("select#editstatustype").html(statusoptions);


        };

        function errormiletype(data) {

            return false;

        };

        
        $('#MilestoneDatatableSearch')
            .unbind()
            .keyup(function() {
                var value = $(this).val();
                if (value.length > 2) {

                    table.search(value).draw();
                }
                if (value == '') {
                    table.search(value).draw();
                }
            });


        $('#btnAddMilestone')
            .click(function(event) {
                event.preventDefault();
                $('#btnAddMilestone').attr('disabled', 'disabled');
                var isValid = true;
                var milestone = $('#milestonetype').val();
                //var pred = $('#predecessortype').val();
                //var succ = $('#successortype').val();
                //if (milestone == pred)
                $('select[class="form-control milerequired"]')
                    .each(function() {

                        if ($.trim($(this).val()) == 'NULL') {
                            isValid = false;
                            $(this)
                                .css({
                                    "border": "1px solid red"
                                });

                        } else {

                            $(this)
                                .css({
                                    "border": "",
                                    "background": ""
                                });
                        }
                    });

                if (isValid == false) {
                    $('#btnAddMilestone').removeAttr('disabled');
                    return false;

                }

                var formData = new FormData();

                formData.append("ProjectNo", ProjectNo);
                formData.append("JobNo", JobNo);
                formData.append("MilestoneTitle", $('#milestonetype').val().trim());
                //formData.append("Predecessor", $('#predecessortype').val().trim());
                //formData.append("Successor", $('#successortype').val().trim());
                formData.append("WaitingOn", $('#addWaitingon').val());
                formData.append("Status", $('#statustype').val());
                formData.append("DueDate", $('#Addduedate').val());
                formData.append("CompletedDate", $('#Addcompleteddate').val());

                if (isValid) {
                    $.ajax({
                        url: '/ProjectMilestones/AddMilestone',
                        method: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {

                            if (data == "0") {
                                $('#AddMilestone').modal('hide');

                                var updateMessage = $.grep(ProjectManagementMessageList,
                                    function(e) {
                                        if (e.MessageName == 'PM-Milestone-Insert') {
                                            return e
                                        }
                                    })
                                if (updateMessage != '') {
                                    $('#CommonSuccessMessage').html(updateMessage[0].Message);
                                    $('#CommonSuccessModal').modal('show');

                                }


                                var table = $('#Milestonetable').DataTable();
                                table.ajax.reload();

                                MilestoneChart();
                            }
                            if (data == "1") {


                                $('#CommonSuccessMessage').html('Milestone is already assigned');
                                $('#CommonSuccessModal').modal('show');
                            }
                            $('#btnAddMilestone').removeAttr('disabled');

                        },
                        error: function(data) {
                            //habdle error
                            $('#btnAddMilestone').removeAttr('disabled');
                        }
                    })
                } else {
                    return false;
                }


            });

        //$('#documentok').click(function () {
        //    $('#SuccessAddMilestonePopup').modal('hide');

        //});

        $('#editdocumentok')
            .click(function() {
                $('#SuccessEditDocument').modal('hide');

            });

        $('#milestonedeletebutton')
            .click(function() {

                var Milestoneid = $('#editMilestoneid').val();

                $.ajax({
                    type: "POST",
                    url: "/ProjectMilestones/DeleteMilestone/",
                    data: { MileID: Milestoneid },
                    success: function(userdetail) {
                        $('#deletesinglemilestone').modal('hide');
                        var table = $('#Milestonetable').DataTable();
                        table.ajax.reload();

                    }
                })

            })


        $('#btnUpdateMilestone')
            .click(function(event) {
                $('#btnUpdateMilestone').attr('disabled', 'disabled');
                var isValid = true;

                $('select[class="form-control editmilerequired"]')
                    .each(function() {

                        if ($.trim($(this).val()) == 'NULL') {
                            isValid = false;
                            $(this)
                                .css({
                                    "border": "1px solid red"
                                });

                        } else {

                            $(this)
                                .css({
                                    "border": "",
                                    "background": ""
                                });
                        }
                    });

                if (isValid == false) {
                    $('#btnUpdateMilestone').removeAttr('disabled');
                    return false;

                }

                var formData = new FormData();


                formData.append("MilestoneId", $('#editMilestoneid').val().trim());
                formData.append("ProjectNo", ProjectNo);
                formData.append("JobNo", JobNo);
                formData.append("MilestoneTitle", $('#editmilestonetype').val().trim());
                //formData.append("Predecessor", $('#editpredecessortype').val().trim());
                //formData.append("Successor", $('#editsuccessortype').val().trim());
                formData.append("WaitingOn", $('#editWaitingon').val());
                formData.append("Status", $('#editstatustype').val());
                formData.append("DueDate", $('#Editduedate').val());

                formData.append("MilestoneName", $('#editmilestonetype option:selected').text().trim());
                formData.append("StatusName", $('#editstatustype option:selected').text().trim());
                formData.append("PreviousStatusName", $('#editstatustype option[value=' + storedStatus + ']').text());
                formData.append("ProjectName", $('#breadcrumbProjectName').html());
                formData.append("JobDesc", $('#ViewJobDesc').html());


                $.ajax({
                    url: '/ProjectMilestones/UpdateMilestone',
                    method: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {

                        if (data == "0") {
                            $('#EditMilestoneDiv').hide();

                            var updateMessage = $.grep(ProjectManagementMessageList,
                                function(e) {
                                    if (e.MessageName == 'PM-Milestone-Update') {
                                        return e
                                    }
                                })
                            if (updateMessage != '') {
                                $('#CommonSuccessMessage').html(updateMessage[0].Message);
                                $('#CommonSuccessModal').modal('show');

                            }

                            var table = $('#Milestonetable').DataTable();
                            table.ajax.reload();

                            MilestoneChart();

                        } else {

                            $('#CommonSuccessMessage').html('Milestone is already assigned');
                            $('#CommonSuccessModal').modal('show');
                        }

                        $('#btnUpdateMilestone').removeAttr('disabled');
                    },
                    error: function(data) {
                        //handle error
                        $('#btnUpdateMilestone').removeAttr('disabled');
                    }
                })
            });

        var userIdList = [];


        $('.dataTables_length').hide();

        $('#Milestonetable_filter').hide();


        $('#readyToPull')
            .click(function(e) {
                ProjectNo = $('#hdnProjectNo').val();

                viewReadyToPull(ProjectNo, '');


            });
        $('#btnReadyToPullSave')
            .click(function(e) {

                $('#wait').css('display', 'block'); // for ajax loader

                var lstReadyToPullTemplateDetails = [];


                ProjectNo = $('#hdnProjectNo').val();
                var TempleteId = 0;
                var formData = new FormData();


                var ctrlName = '';
                formData.append("ProjectNo", ProjectNo);
                formData.append("JobNo", JobNo);
                $('input[name^="chk_"]')
                    .each(function() {

                        ctrlName = $(this).attr('name');
                        TempleteId = ctrlName.replace('chk_', '');

                        //formData.append("TempleteId", TempleteId);

                        var vSelected = $(this).iCheck('update')[0].checked
                        if (vSelected == true) {
                            var files = $('#filename_' + TempleteId).get(0).files;
                            ctrlName = $('#filename_' + TempleteId).attr('name');
                            for (var i = 0; i < files.length; i++) {
                                
                                formData.append(ctrlName + '~' + files[i].name, files[i]);
                            }
                            var ctrlName = $('#textarea_' + TempleteId).attr('name');
                            if ($('#' + ctrlName).val() != '') {
                                //formData.append(ctrlName, $(this).val());
                            }
                            var ctrlName = $('#hdnAlreadyAttachedFile_' + TempleteId).attr('name');
                            if ($('#' + ctrlName).val() != '') {
                                //formData.append(ctrlName, $(this).val());
                            }

                            lstReadyToPullTemplateDetails[lstReadyToPullTemplateDetails.length] = {
                                TemplateId: TempleteId,
                                ProjectNo: ProjectNo,
                                JobNo: JobNo,
                                Comment: $('#textarea_' + TempleteId).val(),
                                Attachment: '',
                                RealAttachment: '',
                                AlreadyRealAttachment: $('#hdnAlreadyAttachedFile_' + TempleteId).val(),
                                CreatedDate: '',
                                CreatedBy: '',
                                IsDeleted: false,
                                appAuditID: 0,
                                SessionID: ''

                            };
                        }

                    });

                formData.append('lstReadyToPullTemplateDetails', JSON.stringify(lstReadyToPullTemplateDetails));

                //$('input[name^="filename_"]').each(function () {

                //    var files = $(this).get(0).files;
                //    if (files.length > 0)
                //    {

                //        var ctrlName = $(this).attr('name');
                //        for (var i = 0; i < files.length; i++) {
                //            formData.append(ctrlName + '~' + files[i].name, files[i]);
                //        }
                //    }

                //});

                // var fileUpload = $("#FileUpload1").get(0);  
                // var files = fileUpload.files;  

                // Looping over all files and add it to FormData object  


                //$('textarea[name^="textarea_"]').each(function () {

                //    var ctrlName = $(this).attr('name');
                //    if ($(this).val()!='')
                //    {
                //        
                //        formData.append(ctrlName, $(this).val());
                //    }

                //});


                //formData.append("DocumentTypeID", $('#adddoctype').val().trim());
                //formData.append("Description", $('#addDocumentDesc').val().trim());
                //formData.append("CompanyName", $('#addCompanyName').val());
                //formData.append("ProjectName", $('#addProjectName').val());
                //formData.append("JobName", $('#addJobName').val());
                //formData.append("ExpiredDate", $('#dpExpDate').val());
                //formData.append("IsPublished", Publishedstatus);
                //formData.append("FileName", filename);


                $.ajax({
                    url: '/ManageProject/SaveReadyToPullTemplateData',
                    method: 'POST',
                    //data: { 'lstReadyToPullTemplateDetails': lstReadyToPullTemplateDetails },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        $('#wait').css('display', 'none'); // for ajax loader

                        var milestoneStatus = '';
                        if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length
                        ) {

                            milestoneStatus = 'Job-Site Ready milestone is not completed';

                            $('#editstatustype').val('27'); // In Process
                        }


                        $('#CommonSuccessMessage')
                            .html('Record updated successfully ' + (milestoneStatus != '' ? '<br>' + milestoneStatus : ''));
                        $('#CommonSuccessModal').modal('show');
                        $('#modal_readypull').modal('hide');
                    },
                    error: function(data) {
                        //handle error
                    }
                });


            });


        $('#editstatustype')
            .on('change',
                function(e) {
                    
                    if (($(this).val() == '28') && ($('#editmilestonetype').val() == '1')) {
                        ProjectNo = $('#hdnProjectNo').val();
                        viewReadyToPull(ProjectNo, '');
                    }
                    //if ($(this).val() != '28')
                    //{

                    //    $('#divReadyToPull').removeClass('input-group');
                    //    $('#readyToPull').hide();
                    //}
                    //else
                    //{
                    //    $('#divReadyToPull').addClass('input-group');
                    //    $('#readyToPull').show();
                    //}
                });

        MilestoneChart();
    });

$('#btnReadyToPullTopClosed').click(function (e) {
    $('#btnReadyToPullCancel').click();
});
$('#btnReadyToPullCancel').click(function (e) {

    if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length) {
        $('#editstatustype').val(storedStatus);
    }

});
function viewReadyToPull(ProjectNo, JobNo) {
   
    $('#partialReadyToPull').load("/ManageProject/_ReadyToPullTemplate?ProjectNo=" + ProjectNo + "&JobNo=" + JobNo + "", function () {
        $(".icheckbox,.iradio").iCheck({ checkboxClass: 'icheckbox_minimal-grey', radioClass: 'iradio_minimal-grey' });
        if ($("input.fileinput").length > 0) {
            $("input.fileinput").bootstrapFileInput();
        }

        $('.onactive input').on('ifChanged', function (event) {
            
            var abc = event.target;
            $('#' + abc.id).parentsUntil('.onactive').find('.help-block').toggleClass('hide');
        });
        $('#modal_readypull').modal();
        
    });
}
function ValidateAttachment(ctrlName)
{
        event.preventDefault();
        var files = $('#' + ctrlName).get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/jpg" || files[0].type == "image/png") {
    
        }
        else {
            
            $('#CommonSuccessMessage').html('Only jpg and png format');
            $('#CommonSuccessModal').modal('show');
            return false;
        
        }

}
$(document).on("click", ".table-click-ProjectMilestone", function (e) {
   
    if ($(this).find("td").is(e.target)) {
        
        var targetClass = '.' + e.target.className
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            
            var clickedRow = $(this).find("td").parent('tr')[0];
            var mileStoneId = $(clickedRow).find('td:last').children('.e-edit')[0].id;
            editMilestone(parseInt(mileStoneId));
        }

    }
});
function MilestoneChart() {
    
    var model = new Object();
    model.ProjectNumber = ProjectNo;
    model.JobNo = JobNo;

    var chartData = '';
    google.charts.load("visualization", "1", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Results', 'Contract Executed', 'Drawing Approved', 'Finishes Completed', 'Job-Site Ready', { role: 'annotation' }],
        ['Village At Rivers Edge Norfolk', 25, 25, 25, 25, ''],
        ['Milennium Tower Boston', 35, 25, 35, 5, ''],
        ['Residence Inn by Marriottbridgewate', 25, 25, 25, 25, ''],
        ['Mit Academic BUildings', 35, 25, 35, 5, '']
        ]);

        $.ajax({

            type: "POST",

            url: "/ManageProject/GetProjectChart/",

            data: model,

            //contentType: "application/json; charset=utf-8",

            dataType: "json",

            success: function (r) {
                
                chartData= r.stackedChart;
                var data = new google.visualization.DataTable();
                if (chartData.length > 0) {
                    var ColumnData = chartData[0].KeyData.split(',');
                    for (i = 0; i < ColumnData.length; i++) {
                        if (i == 0) {
                            data.addColumn('string', ColumnData[i]);
                        }
                        else {
                            data.addColumn('number', ColumnData[i]);
                        }

                    }
                }

                for (var i = 1; i < (chartData.length) ; i++) {
                    var row = [];
                    var RowData = chartData[i].KeyData.split(',');
                    for (j = 0; j < RowData.length; j++) {
                        if (j == 0) {
                            row.push(RowData[j]);
                        }
                        else {
                            row.push(RowData[j] == "" ? 0 : parseInt(RowData[j]));
                        }

                    }
                    
                    data.addRow(row);
                }

                var options = {
                    //title: 'Standard',
                    chartArea: { width: "50%", height: "50%" },
                    legend: { position: 'top',  maxLines: 3 },
                    bar: { groupWidth: '80%' },
                    isStacked: true,
                    colors: ['#9e9e9e', '#16a085', '#27ae60', '#f39c12', '#9c27b0', '#ffeb3b', '#e91e63', '#795548', '#263238', '#34495e'],
                    vAxis: { textPosition: 'none'},
                    hAxis: { textPosition: 'none',
                        viewWindow: {
                            min: 0,
                            max: 100
                        },
                        ticks: [0, 20, 40, 60, 80, 100] // display labels every 25,
                        
                    }
                };
                //var chart = new google.visualization.BarChart(document.getElementById('stackedChartProjectMilestone'));
                //chart.draw(data, options);



                
            },

            failure: function (r) {
                
                

            },

            error: function (r) {
                
                

            }

        });




        
    }
}



function ProjectMilestoneTooltip() {
    
    $("#AddMilestonePopupClose").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit', placement: 'top' });
   
}