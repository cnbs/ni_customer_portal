﻿/// <reference path="App.js" />
'use strict';
// Declare app level module which depends on views, and components
var app = angular.module('customerPortal', [
     'ngRoute',
     'ui.bootstrap',
     'angularSpinner',
     'ngCkeditor',
     'ngSanitize',
     'ngTagsInput'
 // 'aa.formExtensions',
 // 'aa.notify'
]);

app.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({ color: 'blue' });
}]);
app.directive('ngEnter', function () {
    return function (scope, elem, attrs) {
        elem.bind("keydown keypress", function (event) {
            // 13 represents enter button
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
function isUndefinedOrNull(val) {
    return angular.isUndefined(val) || val === null
}
function GetDefaultPageObject() {
    var hdnPageSize =document.getElementById('hdnPageSize').value;     
    var PageObj = { 'PageNo': 1, 'PageSize': isUndefinedOrNull(hdnPageSize) ? 10 : hdnPageSize }
    return PageObj;
}
