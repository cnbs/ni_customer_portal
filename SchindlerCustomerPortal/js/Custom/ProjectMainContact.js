﻿function EditProjectContact(id) {

    $.ajax({
        type: "GET",
        url: "/ManageProject/GetProjectContactByID/",
        data: { contactID: id },
        success: function (userdetail) {
            
            //$('#editContactHeaderFirstName').text(userdetail.user.FirstName);

            $('#editContactID').val(userdetail.user.ContactID);
            $('#editContactFirstname').val(userdetail.user.FirstName);

            $("#editProjectNo").val(userdetail.user.ProjectNo)
            $("#editContactProjectName").val(userdetail.user.ProjectName)
            $("#editContactJobName").val(userdetail.user.JobName);

            //$('#editContactLastname').val(userdetail.user.LastName);
            //$('#editContactDesignation').val(user.Designation);
            $("#editContactPhone").val(userdetail.user.ContactNumber)
            $('#editContactEmail').val(userdetail.user.Email);
            //$('#editContactAddress').val(userdetail.user.Address);
            //$('#editContactState').val(userdetail.user.State);
            //$('#editContactCity').val(userdetail.user.City);
            //$('#editContactZipcode').val(userdetail.user.PostalCode);

            $('#editContactDesignation').html("");
            $.each(userdetail.designation, function (i, val) {
                //$('#editContactDesignation option[value=' + userdetail.user.DesignationId + ']').attr('selected', 'selected');
                $('#editContactDesignation').append($("<option/>", ($({ value: val.DesignationId, text: val.Designation }))));
            });

            $('#editContactDesignation').val(userdetail.user.DesignationId);

            //$('#editContactState').html("");
            //$.each(userdetail.state, function (i, val) {
            //    $('#editContactState option[value=' + userdetail.user.State + ']').attr('selected', 'selected');
            //    $('#editContactState').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));
            //});

            //$('#editContactCity').html("");
            //$.each(userdetail.city, function (i, val) {
            //    $('#editContactCity option[value=' + userdetail.user.City + ']').attr('selected', 'selected');
            //    $('#editContactCity').append($("<option/>", ($({ value: val.CityName, text: val.CityName }))));
            //});

        }
    })
}

function DeleteProjectContact(id) {
    $.ajax({
        type: "POST",
        url: "/ManageProject/DeleteProjectContact/",
        data: { contactID: id },
        success: function (userdetail) {
            var table = $('#CompanyContactDatatable').DataTable();
            table.ajax.reload();
        }
    })
}

function ValidateAddProjectContact() {
    var inValid = false;
    var addContactFirstname = $('#addContactFirstname').val().trim();
    var addContactProjectName = $('#addContactProjectName').val().trim();
    var addContactDesignation = $('#addContactDesignation').val().trim();
    var addContactPhone = $('#addContactPhone').val().trim();
    var addContactEmail = $('#addContactEmail').val().trim();
    //var addContactAddress = $('#addContactAddress').val().trim();
    //var addContactState = $('#addContactState').val().trim();
    //var addContactCity = $('#addContactCity').val().trim();
    //var addContactZipcode = $('#addContactZipcode').val().trim();

    if (addContactFirstname == "") {
        inValid = true;
        $('#addContactFirstname').css('border-color', 'red');
    }

    if (addContactProjectName == "") {
        inValid = true;
        $('#addContactProjectName').css('border-color', 'red');
    }

    if (addContactDesignation == "") {
        inValid = true;
        $('#addContactDesignation').css('border-color', 'red');
    }

    if (addContactPhone == "") {
        inValid = true;
        $('#addContactPhone').css('border-color', 'red');
    }

    if (addContactEmail == "") {
        inValid = true;
        $('#addContactEmail').css('border-color', 'red');
    }

    //if (addContactAddress == "") {
    //    inValid = true;
    //    $('#addContactAddress').css('border-color', 'red');
    //}

    //if (addContactState == "") {
    //    inValid = true;
    //    $('#addContactState').css('border-color', 'red');
    //}

    //if (addContactCity == "") {
    //    inValid = true;
    //    $('#addContactCity').css('border-color', 'red');
    //}

    //if (addContactZipcode == "") {
    //    inValid = true;
    //    $('#addContactZipcode').css('border-color', 'red');
    //}

    if (inValid == true)
        return false;
    else {
        return true;
    }
}

function ValidateEditProjectContact() {
    var inValid = false;
    var editContactFirstname = $('#editContactFirstname').val().trim();
    var editContactJobName = $('#editContactJobName').val().trim();
    var editContactDesignation = $('#editContactDesignation').val().trim();
    var editContactPhone = $('#editContactPhone').val().trim();
    //var addContactEmail = $('#addContactEmail').val().trim();
    //var addContactAddress = $('#addContactAddress').val().trim();
    //var addContactState = $('#addContactState').val().trim();
    //var addContactCity = $('#addContactCity').val().trim();
    //var addContactZipcode = $('#addContactZipcode').val().trim();

    if (editContactFirstname == "") {
        inValid = true;
        $('#editContactFirstname').css('border-color', 'red');
    }

    //if (editContactJobName == "") {
    //    inValid = true;
    //    $('#editContactJobName').css('border-color', 'red');
    //}

    if (editContactDesignation == "") {
        inValid = true;
        $('#editContactDesignation').css('border-color', 'red');
    }

    if (editContactPhone == "") {
        inValid = true;
        $('#editContactPhone').css('border-color', 'red');
    }

    //if (addContactEmail == "") {
    //    inValid = true;
    //    $('#addContactEmail').css('border-color', 'red');
    //}

    //if (addContactAddress == "") {
    //    inValid = true;
    //    $('#addContactAddress').css('border-color', 'red');
    //}

    //if (addContactState == "") {
    //    inValid = true;
    //    $('#addContactState').css('border-color', 'red');
    //}

    //if (addContactCity == "") {
    //    inValid = true;
    //    $('#addContactCity').css('border-color', 'red');
    //}

    //if (addContactZipcode == "") {
    //    inValid = true;
    //    $('#addContactZipcode').css('border-color', 'red');
    //}

    if (inValid == true)
        return false;
    else {
        return true;
    }
}

function DesignationAndState() {

    $.ajax({
        type: "GET",
        url: "/ManageProject/GetDesignationandState",
        dataType: "json",
        //data: { countryId: countryId },
        success: function (response) {

            $('#addContactDesignation').html("");
            $('#addContactDesignation').append($("<option/>", ($({ value: '', text: '-- Select --' }))));
            $.each(response.designation, function (i, val) {
                $('#addContactDesignation').append($("<option/>", ($({ value: val.DesignationId, text: val.Designation }))));
            });

            //$('#addContactState').html("");
            //$('#addContactState').append($("<option/>", ($({ value: '', text: '-- Select --' }))));
            //$.each(response.state, function (i, val) {
            //    $('#addContactState').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));
            //});
        },
        failure: function (response) {
            bootbox.alert('oops something went wrong');
        }
    });
}

function CompanyContactDatatablePaging() {
    $('#CompanyContactDatatable_paginate').children('ul').addClass('flatpagi');
    $('#CompanyContactDatatable_previous').children('a').remove();
    $('#CompanyContactDatatable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#CompanyContactDatatable_next').children('a').remove();
    $('#CompanyContactDatatable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function AddNewCompanyContactPopUpOpen() {

    DesignationAndState();

    $("#AddProjectContactModal").modal(); return false;
}

function UserMessageSend(mail) {
    window.location.href = '/mailbox/maillist?to=' + mail;
}

function MainConatctTooltip() {
    $("#btnAddnewCompanyContact").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit Contact', placement: 'top' });
    $(".e-communicate").tooltip({ title: 'Message', placement: 'top' });
}

$(document).ready(function () {
   // $('[data-toggle="tooltip"]').tooltip(); 
    var hdnPageSize = parseInt($('#hdnPageSize').val());

    $('#addContactPhone').mask('000-000-0000');
    $('#editContactPhone').mask('000-000-0000');

    var ProjectNo = '';

    var txtProjectNameoptions = {
        source: function (request, response) {
            //
            $.ajax({
                url: "/ManageProject/ProjectNameAutoCompleteByCompanyId/",
                data: '{ searchTerm:"' + request.term + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    //
                    response($.map(data, function (item) {
                        return {
                            label: item.ProjectName,
                            value: item.ProjectName,
                            ProjectNo: item.ProjectNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addContactProjectName").val(ui.item.label);
            
            ProjectNo = ui.item.ProjectNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                //alert('');
                //$('#empty-message').show();
            } else {
                //$('#empty-message').hide();
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#addContactProjectName', function (event) {
        $(this).autocomplete(txtProjectNameoptions);
    })

    var ProjectJobNo = '';

    var txtJobNameoptions = {
        source: function (request, response) {
            //
            $.ajax({
                url: "/ManageProject/JobNameAutoCompleteByProjectNo/",
                data: '{ searchTerm:"' + request.term + '" , ProjectNo:"' + ProjectNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    //
                    response($.map(data, function (item) {
                        return {
                            label: item.JobName,
                            value: item.JobName,
                            JobNo: item.JobNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addContactJobName").val(ui.item.label);
            ProjectJobNo = ui.item.JobNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                //alert('');
                //$('#empty-message').show();
            } else {
                //$('#empty-message').hide();
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#addContactJobName', function (event) {
        $(this).autocomplete(txtJobNameoptions);
    })

    $('#addContactFirstname,#addContactProjectName,#addContactDesignation,#addContactPhone,#addContactEmail').on('blur', function () {
        ValidateElement($(this));
    });

    $('#btnAddProjectContact').click(function (event) {
        event.preventDefault();

        $('#btnAddProjectContact').attr('disabled', 'disabled');

        if (!ValidateAddProjectContact()) {
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }

        var isValid = true;

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var addContactEmail = $("#addContactEmail").val();
        var echeck = "";
        if (typeof addContactEmail != 'undefined' && addContactEmail != null && addContactEmail.trim() != "") {
            if (!addContactEmail.match(re)) {
                $("#addContactEmail").css({
                    "border": "1px solid red"
                });
                $("#addContactEmail").focus();
                echeck = "a";
                $('#btnAddProjectContact').removeAttr('disabled');
            }
        }
        if (echeck == "a") {
            return false;
        }

        var phonelen = $('#addContactPhone').val().length;
        if (phonelen < 12) {
            $('#addContactPhone').css({
                "border": "1px solid red"
            });
            $('#addContactPhone').focus();
            var isValid = false;
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }

        //validation for Firstname allow only alphabets
        var contactfirstname = $('#addContactFirstname').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof contactfirstname != 'undefined' && contactfirstname != null && contactfirstname.trim() != "") {
            if (!contactfirstname.match(re)) {
                $("#addContactFirstname").css({
                    "border": "1px solid red"
                });
                $("#addContactFirstname").focus();
                fcheck = "a";
                $('#btnAddProjectContact').removeAttr('disabled');
            }
        }
        if (fcheck == "a") {
            return false;
        }
        
        var formData = new FormData();

        var contact = $("#addContactPhone").val().replace('-', '').replace('-', '');

        formData.append("FirstName", $('#addContactFirstname').val().trim());
        formData.append("ProjectNo", ProjectNo);
        formData.append("JobNo", ProjectJobNo);
        //formData.append("LastName", $('#addContactLastname').val().trim());
        formData.append("Designation", $('#addContactDesignation option:selected').val().trim());
        formData.append("ContactNumber", contact);
        formData.append("Email", $('#addContactEmail').val().trim());
        formData.append("FlagValue", 1);
        //formData.append("Address", $('#addContactAddress').val().trim());
        //formData.append("State", $('#addContactState option:selected').val().trim());
        //formData.append("City", $('#addContactCity option:selected').val().trim());
        //formData.append("PostalCode", $('#addContactZipcode').val().trim());

        //isValid = false;

        if (isValid) {
            $.ajax({
                url: '/ManageProject/AddProjectContact',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    //alert('success')
                    //$('#btnAddProjectContact').attr('disabled', '');
                    $('#btnAddProjectContact').removeAttr('disabled');
                    $('#AddProjectContactModal').modal('hide');

                    $('#CommonSuccessMessage').html('Record added successfully');
                    $('#CommonSuccessModal').modal('show');

                    var table = $('#CompanyContactDatatable').DataTable();
                    table.ajax.reload();
                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }


    })

    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var table = $('#CompanyContactDatatable').DataTable({
        "ajax": {
            "url": "/ManageProject/GetAllContactListByCompanyId",
            "type": "POST",
            "datatype": "JSON",
        },
        "autoWidth": false, // for disabling autowidth
        //"columnDefs": [{ 'bSortable': false, 'targets': [0, 2] }],
        'aaSorting': [[3, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "","sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "bInfo": false,
        //"iTotalRecords": "3",
        //"iTotalDisplayRecords": "3",
        "columns": [
        { "data": "ContactID", "name": "ContactID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "FirstName",
            "name": "FirstName",
            "Searchable": true,
        },
        { "data": "ProjectName", "name": "ProjectName", "Searchable": true }, // project name
        { "data": "BankDesc", "name": "BankDesc", "Searchable": true }, // job name
        { "data": "Designation", "name": "Designation", "Searchable": true }, // role
        { "data": "ContactNumber", "name": "PhoneNo", "Searchable": true, "sClass":"txtAlignCenter", },
        { "data": "Email", "name": "Email", "Searchable": true,
                "render": function (data, type, row) {
                    return '<a href="mailto:' + row.Email + '?subject=MySchindlerProjects" >' + data + '</a>'
                    }
        }, // email
        {
            "data": "ContactID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                return '<a class="btnsml_action e-edit" href="javascript:void(0)" id="' + data + '" data-toggle="modal" data-target="#EditProjectContactModal" onclick="EditProjectContact(' + data + ')"> </a>'//<a href="javascript:void(0)" class="btnsml_action e-communicate" onclick="UserMessageSend(\'' + row.Email + '\')"> </a>'
            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            CompanyContactDatatablePaging();
            MainConatctTooltip();
        },
        "drawCallback": function (settings) {
            //
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }
    });

    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {
            
            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#EditProjectContactModal').modal('show');
                EditProjectContact(parseInt(userID));
            }

        }
    });

    $('#CompanyContactDatatable').on('draw.dt', function () {
        CompanyContactDatatablePaging();
    });

    $('.dataTables_length').hide();

    $('#CompanyContactDatatable_filter').hide();

    $('#CompanyContactDatatableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 2) {
            //table.search(value).draw();
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    var txtProjectNameoptions = {
        source: function (request, response) {
            //
            $.ajax({
                url: "/ManageProject/ProjectNameAutoCompleteByCompanyId/",
                data: '{ searchTerm:"' + request.term + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    //
                    response($.map(data, function (item) {
                        return {
                            label: item.ProjectName,
                            value: item.ProjectName,
                            ProjectNo: item.ProjectNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addContactProjectName").val(ui.item.label);
            
            ProjectNo = ui.item.ProjectNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                //alert('');
                //$('#empty-message').show();
            } else {
                //$('#empty-message').hide();
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#editContactProjectName', function (event) {
        $(this).autocomplete(txtProjectNameoptions);
    })

    var txtJobNameEditoptions = {
        source: function (request, response) {
            //
            $.ajax({
                url: "/ManageProject/JobNameAutoCompleteByProjectNo/",
                data: '{ searchTerm:"' + request.term + '" , ProjectNo:"' + $("#editProjectNo").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    //
                    response($.map(data, function (item) {
                        return {
                            label: item.JobName,
                            value: item.JobName,
                            JobNo: item.JobNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editContactJobName").val(ui.item.label);
            ProjectJobNo = ui.item.JobNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                //alert('');
                //$('#empty-message').show();
            } else {
                //$('#empty-message').hide();
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#editContactJobName', function (event) {
        $(this).autocomplete(txtJobNameEditoptions);
    })

    $('#btnUpdateProjectContact').click(function (event) {

        event.preventDefault();

        if (!ValidateEditProjectContact()) {
            return false;
        }

        var isValid = true;

        var phonelen = $('#editContactPhone').val().length;
        if (phonelen < 12) {
            $('#editContactPhone').css({
                "border": "1px solid red"
            });
            $('#editContactPhone').focus();
            var isValid = false;
            return false;
        }

        //validation for Firstname allow only alphabets
        var contactfirstname = $('#editContactFirstname').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof contactfirstname != 'undefined' && contactfirstname != null && contactfirstname.trim() != "") {
            if (!contactfirstname.match(re)) {
                $("#editContactFirstname").css({
                    "border": "1px solid red"
                });
                $("#editContactFirstname").focus();
                fcheck = "a";
                
            }
        }
        if (fcheck == "a") {
            return false;
        }

        var formData = new FormData();

        var contact = $("#editContactPhone").val().replace('-', '').replace('-', '');

        formData.append("ContactID", $('#editContactID').val().trim());
        formData.append("FirstName", $('#editContactFirstname').val().trim());
        //formData.append("LastName", $('#editContactLastname').val().trim());
        formData.append("Designation", $('#editContactDesignation option:selected').val().trim());
        //formData.append("Email", $('#editContactEmail').val().trim());
        formData.append("ContactNumber", contact);
        formData.append("JobNo", ProjectJobNo);
        formData.append("JobName", $('#editContactJobName').val());
        //formData.append("Address", $('#editContactAddress').val().trim());
        //formData.append("State", $('#editContactState option:selected').val().trim());
        //formData.append("City", $('#editContactCity option:selected').val().trim());
        //formData.append("PostalCode", $('#editContactZipcode').val().trim());

        if (isValid) {
            $.ajax({
                url: '/ManageProject/UpdateProjectContact',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    //alert('success')

                    $('#EditProjectContactModal').modal('hide');

                    $('#CommonSuccessMessage').html('Record updated successfully');
                    $('#CommonSuccessModal').modal('show');

                    //$('#editProjectContactForm').hide();

                    var table = $('#CompanyContactDatatable').DataTable();
                    table.ajax.reload();
                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            return false;
        }

    });

    $('#btnDeleteProjectContact').click(function () {
        
        $('#contactDeleteConfirmModel').modal('show');
        //var id = $('#editContactID').val();
        //DeleteProjectContact(id);
        //$('#EditProjectContactModal').modal('hide');
    })

    $('#btnContactDeleteYes').click(function () {
        var id = $('#editContactID').val();
        DeleteProjectContact(id);
        $('#contactDeleteConfirmModel').modal('hide');
        $('#EditProjectContactModal').modal('hide');
    })

    $('#addContactFirstname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    
    $('#editContactFirstname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
})