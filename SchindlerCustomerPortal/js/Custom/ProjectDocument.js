﻿
function convertjsonOnlydate(Anydate) {
    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = year.toString() + "/" + month + "/" + day;
        return date;
    }
    else {
        return null;
    }
}

function editDocument(id) {

    $.ajax({
        type: "GET",
        url: "/ManageDocument/EditDocument/",
        data: { docId: id },
        success: function (documentdetails) {

            //var Expdate = convertjsonOnlydate(documentdetails.ExpiredDate);
            //var Publishdate = convertjsonOnlydate(documentdetails.PublishedOn);

            var Expdate = documentdetails.ExpiredDate;
            var Publishdate = documentdetails.PublishedOn;

            $('#editDocumentid').val(documentdetails.DocumentID);
            $('#editDocumentName').val(documentdetails.DocumentName);
            $('#editdoctype').val(documentdetails.DocumentTypeID);
            $('#editstatustype').val(documentdetails.StatusId);
            $('#editDocumentDesc').val(documentdetails.Description);
            $('#editExpDate').val(Expdate);
            $('#editpublishDate').val(Publishdate);

            //PublishedOn

            //$('#editRoleID').prop('checked', (userdetail.RoleID == 3) ? true : false);

            //$('#editpicFilename').val(userdetail.ProfilePic);

        }
    })
}

$('#btnDeleteDocument').click(function () {

    $('#documentDeleteConfirmModel').modal('show');


});

$('#btnDeleteYes').click(function () {

    var DocID = $('#editDocumentid').val();

    DeleteDocument(DocID);


});

function DeleteDocument(id) {

    $.ajax({
        type: "POST",
        url: "/ManageDocument/DeleteDocument/",
        data: { DocID: id },
        success: function (userdetail) {
            $('#editDocument').modal('hide');
            var table = $('#ContractDocumenttable').DataTable();
            table.ajax.reload();
        }
    })
}

$(function () {

    $("#dpExpDate").datepicker();
});


function updateuserdocumentpermission(docid, userid, HasPermission, Type, rowid) {


    //console.log("Type", Type);
    //var FlagValue = $("#" + rowid).attr("custvalue" + Type);
    $.ajax({
        type: "POST",
        url: "/ManageDocument/UpdateUserAccessPermission/",
        data: { DocID: docid, UserID: userid, HasPermission: HasPermission, Type: Type },
        success: function (response) {
            if (response.success) {
                //var THasPermission = (FlagValue == true || FlagValue == 'true') ? 'false' : 'true';
                //$("#" + rowid).attr("custvalue" + Type, THasPermission);


                //if (THasPermission == 'true' || THasPermission == true) {
                //    $('#' + rowid).removeClass("accessallowed");
                //}
                //else {
                //    $('#' + rowid).addClass("accessallowed");

                //}
                // bootbox.alert(response.message);

                if (HasPermission == 'true' || HasPermission == true) {
                    $('#' + rowid).addClass("accessallowed");
                }
                else {
                    $('#' + rowid).removeClass("accessallowed");
                }
            }
            else {
                bootbox.alert(response.message);
            }
        },
        error: function (err) {
            bootbox.alert(err.message);
        }
    })


}

function BindUserDocumentPermission(id) {



    $('#dt_UserDocumentPermission').DataTable({
        "paging": false,
        "info": false,
        "ajax": {
            "url": "/ManageDocument/UserDocumentPermission",
            "type": "Get",
            "datatype": "JSON",
            'data': {
                docID: id,
            },
        },
        "filter": true, // this is for disable filter (search box) 
        "oLanguage": { "sSearch": "" },
        "columns": [
            {
                "render": function (data, type, row) {
                    return '<div class="checkbox"><label><input type="checkbox" value="" name="chkUserPermission" id="chkUserPermission_' + row.UserID + '_' + row.DocumentID + '" ><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> </label></div>'
                }

            },
        {
            "data": "UserName",

        },

        {
            "data": "HasViewPermission",
            "render": function (data, type, row) {

                //var editResult = (row.HasEditPermission == 1) ? 'accessallowed' : '';
                var viewResult = (row.HasViewPermission == 1) ? 'accessallowed' : '';

                var uploadResult = (row.HasUploadPermission == 1) ? 'accessallowed' : '';
                var deleteResult = (row.HasDeletePermission == 1) ? 'accessallowed' : '';



                var rowViewPermission = (row.HasViewPermission == 1) ? false : true;
                var rowUploadPermission = (row.HasUploadPermisssion == 1) ? false : true;
                var rowDeletePermission = (row.HasDeletePermission == 1) ? false : true;

                return '<a class="btnsml_action e-view round ' + viewResult + '"  name="view" id="view_' + row.DocumentID + '_' + row.UserID + '" custvalueview="' + rowViewPermission + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowViewPermission + ',this.name,this.id)" href="#" data-isedit="' + row.HasViewPermission + '"> </a>' +
                    '<a class="btnsml_action round e-upload ' + uploadResult + '" name="upload" id="upload_' + row.DocumentID + '_' + row.UserID + '" custvalueupload="' + rowUploadPermission + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowUploadPermission + ',this.name,this.id)" href="#" data-isedit="' + row.rowUploadPermission + '" > </a>' +
                '<a href="#" class="btnsml_action e-delete round ' + deleteResult + '" id="delete_' + row.DocumentID + '_' + row.UserID + '" custvaluedelete="' + rowDeletePermission + '" name="delete" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowDeletePermission + ',this.name,this.id)" href="#" data-isedit="' + row.HasDeletePermission + '" > </a>';
            }



        },
         //{ "data": "HasAddPermission" },
         // { "data": "HasEditPermission" },
         //  { "data": "HasDeletePermission" },
         //   { "data": "HasPrintPermission" }

        ]


    });

}



function ViewDocumentDetail(id) {


    $.ajax({
        type: "GET",
        url: "/ManageDocument/EditDocument/",//UserEdit
        data: { docId: id },
        success: function (documentdetails) {

            //var name = userdetail.FirstName + ' ' + userdetail.LastName;
            //var role = (userdetail.RoleID == 3) ? 'Admin User' : 'Normal User';
            $('#DocumentName').html(documentdetails.DocumentName);
            $('#docdescription').html(documentdetails.Description);
            //$('#usercontact').html(userdetail.ContactNumber);
            //$('#useremail').html(userdetail.Email);
            //$('#userimage').attr('src', userdetail.ProfilePic);
            BindUserDocumentPermission(id);

            //var table = $('#UserDatatable').DataTable();
            //table.ajax.reload();
        }
    })
}



$(document).ready(function () {

    //To show project name in pop up
    $('#projname').text($('#projectname').val());
    $('#editprojname').text($('#projectname').val());
    //

    var DocumentTypeName = $('#doctypebypage').val().trim();
    

    $("#adddoctype").prop("disabled", true);
    $("#editdoctype").prop("disabled", true);


    var hdnPageSize = parseInt($('#hdnPageSize').val());
    var ProjectId = $('#projecid').val();
   
    
    var model = new Object();
    model.ProjectID = ProjectId;
    model.DocumentType = DocumentTypeName;

    var table = $('#ContractDocumenttable').DataTable({

        "ajax": {
            "url": "/ManageProjectDocument/GetContractDocumentAll",
            "type": "POST",
            "datatype": "JSON",
            "data": model

        },

        //"columnDefs": [{ 'bSortable': false, 'targets': [0, 2] }],
        //'aaSorting': [[5, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "bInfo": false,
        "oLanguage": {
            "sEmptyTable": "No data available"
        },

        "columns": [

        { "data": "DocumentName", "name": "DocumentName", "Searchable": true },
         { "data": "Description", "name": "Description", "Searchable": true },
         { "data": "StatusName", "name": "StatusName" },
         { "data": "PublishedBy", "name": "PublishedBy" },
         {
             "data": "CreatedDate",
             "name": "CreatedDate",
             //var Expdate =convertjsonOnlydate(data)
             "render": function (data, type, row) {

                 return convertjsonOnlydate(data)
             }
         },
          //{
          //    "data": "PublishedOn",
          //    "name": "PublishedOn",
          //    "render": function (data, type, row) {

          //        return convertjsonOnlydate(data)
          //    }
          //},
        {
            "data": "DocumentID",
            "render": function (data, type, row) {
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var output = d.getFullYear() + '/' +
                    (month < 10 ? '0' : '') + month + '/' +
                    (day < 10 ? '0' : '') + day;


                //alert('Today date ' + output);
                var Expdate = convertjsonOnlydate(row.ExpiredDate);
                //alert('Exp date ' + Expdate);



                //if (row.ExpiredDate != null) {
                if (Expdate > output) {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>     <a href="#" name="DownloadDocumentName" Fullpath=' + row.DocumentPath + ' class="btnsml_action e-edit" > </a>'
                }
                    //}
                    //}
                else {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>  '
                }


            }
        }

        ]
        ,
        "drawCallback": function (settings) {
      
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });

    //$("#ContractDocumenttable").on('click', 'a[name=DownloadDocumentName]', function () {

    //    //var Contentid = $(this).attr('astructid');
    //    var Fullpath = $(this).attr('fullpath');

    //    //window.location.href = '/CompanyAdmin/DownloadAsset?ContentId=' + Contentid + '&AssetPath=' + Fullpath;
    //    window.location.href = '/ManageDocument/DownloadDocument?DocPath=' + escape(Fullpath);

    //});

    function successdoctype(data) {
        var obj = data;
        var options = null;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].DocumentTypeName == DocumentTypeName) {
                options += '<option selected="selected" name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
            }
            else
            {
                options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
            }
            
        }
        $("select#adddoctype").html(options);

        $("select#editdoctype").html(options);


    };

    function errordoctype(data) {

        return false;

    };

    $.ajax({
        url: '/Common/BindDocumentType',
        type: 'GET',
        success: successdoctype,
        error: errordoctype
    });

    

    $.ajax({
        url: '/Common/BindStatusList',
        type: 'GET',
        success: successdocstatustype,
        error: errordocstatustype
    });

    function successdocstatustype(data) {
        var obj = data;
        var options = null;
        for (var i = 0; i < obj.length; i++) {

            options += '<option name = "docstatustypeoption" value="' + obj[i].StatusId + '">' + obj[i].StatusName + '</option>';
        }
        $("select#addstatustype").html(options);

        $("select#editstatustype").html(options);


    };

    function errordocstatustype(data) {

        return false;

    };

    $('.dataTables_filter input').unbind().keyup(function () {
        var value = $(this).val();
        if (value.length > 3) {

            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    var Did = 0;
    var userIdListP = [];

    $('#chkAllUserPermission').change(function () {

        if ($(this).is(":checked")) {
            $('input[name="chkUserPermission"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                var DocID = result[2];
                userIdListP.push(UserID);
                Did = DocID;
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUserPermission"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                Did = DocID;
                var DocID = result[2];

                userIdListP = jQuery.grep(userIdListP, function (value) {
                    return value != UserID;
                });
            });
        }
    });



    $('#btnAllDeletePermission').click(function (event) {
        event.preventDefault();
        var IsValiddd = true;
        if (userIdListP.length == 0) {
            IsValiddd = false;
        }

        var result = userIdListP.join(", ")
        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageDocument/MultipleUserPermissionDelete",
                data: { docid: Did, userids: result },
                success: function (userdetail) {
                    $('#chkAllUserPermission').prop('checked', false);
                    bootbox.alert(userdetail.message);
                    var table = $('#dt_UserDocumentPermission').DataTable();
                    table.ajax.reload();
                }
            })
        }


    })

    $('#addDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#editDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#btnAddDocument').click(function (event) {
        event.preventDefault();

        var isValid = true;

        $('input[class="form-control docsrequired"]').each(function () {

            var a = $('#addDocumentName').val();
            var b = $('#adddoctype').val();
            var c = $("#attachDocument").get(0).files;
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {
                isValid = true;
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });


        var setfocus = "f";
        var ele = $('input[class="form-control docsrequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }

        if (isValid == false)
            return false;

        var formData = new FormData();
        var files = $("#attachDocument").get(0).files;
        var filesize = $("#attachDocument")[0].files[0].size;
        if (filesize > 8388608)   //33554432
        {
            //alert('File size shold be less than 32 mb');
            $('#CommonErrorMessage').html('File size shold be less than 32 mb.');
            $('#CommonErrorModal').modal('show');
            return false;
        }



        formData.append("docs", files[0]);
        formData.append("DocumentName", $('#addDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#adddoctype').val().trim());
        formData.append("ProjectID", ProjectId);
        formData.append("StatusId", $('#addstatustype').val().trim());
        formData.append("Description", $('#addDocumentDesc').val().trim());
        formData.append("ExpiredDate", $('#dpExpDate').val().trim());
        formData.append("PublishedOn", $('#dppublishDate').val().trim());


        if (isValid) {
            $.ajax({
                url: '/ManageDocument/AddDocument',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    $('#AddDocument').modal('hide');
                    $('#SuccessDocument').modal('show');
                    var table = $('#ContractDocumenttable').DataTable();
                    table.ajax.reload();

                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            return false;
        }


    });

    $('#documentok').click(function () {
        $('#SuccessDocument').modal('hide');
        
    });

    $('#editdocumentok').click(function () {
        $('#SuccessEditDocument').modal('hide');
        
    });

    $('#deletedocumentok').click(function () {
        $('#SuccessDeleteDocument').modal('hide');
       
    });

    $('#btnUpdateDocument').click(function (event) {

        //event.preventDefault();
        var formData = new FormData();

        var isValid = true;

        $('input[class="form-control editdocsrequired"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {
                isValid = true;
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });


        var setfocus = "f";
        var ele = $('input[class="form-control editdocsrequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }

        if (isValid == false)
            return false;

        var formData = new FormData();
        var files = $("#editattachDocument").get(0).files;
        var IsFile = $('#editattachDocument')[0].files.length;
        if (IsFile > 0) {
            var filesize = $("#editattachDocument")[0].files[0].size;
            if (filesize > 8388608)   //33554432
            {
                // alert('File size shold be less than 32 mb');
                 $('#CommonErrorMessage').html('File size shold be less than 32 mb.');
            $('#CommonErrorModal').modal('show');
                return false;
            }
        }


        formData.append("docs", files[0]);
        formData.append("DocumentID", $('#editDocumentid').val().trim());
        formData.append("DocumentName", $('#editDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#editdoctype').val().trim());
        formData.append("ProjectID", ProjectId);
        formData.append("StatusId", $('#editstatustype').val().trim());
        formData.append("Description", $('#editDocumentDesc').val().trim());
        formData.append("ExpiredDate", $('#editExpDate').val().trim());
        formData.append("PublishedOn", $('#editpublishDate').val().trim());


        $.ajax({
            url: '/ManageDocument/UpdateDocument',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                $('#editDocument').modal('hide');
                $('#SuccessEditDocument').modal('show');
                var table = $('#ContractDocumenttable').DataTable();
                table.ajax.reload();


            },
            error: function (data) {
                alert('error')
            }
        })
    });

    var userIdList = [];

    $('#chkAllUser').change(function () {
        if ($(this).is(":checked")) {
            $('input[name="chkUser"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                userIdList.push(UserID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUser"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];

                userIdList = jQuery.grep(userIdList, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $('#btnAllDelete').click(function (event) {
        event.preventDefault();
        

        var IsValiddd = true;
        if (userIdList.length == 0) {
            IsValiddd = false;
        }

        var result = userIdList.join(", ")

        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageUser/MultipleUserDelete",
                data: { userID: result },
                success: function (userdetail) {
                    $('#chkAllUser').prop('checked', false);
                    var table = $('#UserDatatable').DataTable();
                    table.ajax.reload();
                }
            })
        }


    })

    $("#tblCategory_paginate").on("click", "a", function () {
        $('#chkAllUser').prop('checked', false);
        $('input[name="chkUser"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var UserID = result[1];

            userIdList = jQuery.grep(userIdList, function (value) {
                return value != UserID;
            });
        });
    });

})

