﻿//Developed By : Manoj Mevada
// 01/13/2017

var SelectedDocType = '';
var ProjectNo = '';
var ComeFrom = '';
var SelectedCompanyId = '';
var ProjectManagementMessageList = '';
var scrolled = 0;

var flag = 0;
//var JobNo = "";

Dropzone.autoDiscover = false;

$(document).ready(function () {

    $('#DisplayDocuments').click(function (event) {

        $('.xn-openable').removeClass('active');
        $(this).parents('.xn-openable').addClass('active');


        SelectedDocType = 'Documents';

        //$('#DisplayProjectEditMode').show();
        $('#DisplayProjectEditMode').hide();
        $("#_ProjectCommunicationView").hide();

        $("#_ProjectMilestoneList").hide();
        $("#EditMilestoneDiv").hide();

        ProjectDocumentInit(SelectedDocType);

        $('html, body').animate({
            scrollTop: $("#ProjectDocumentForm").offset().top
        }, 500);
    });

   
    ProjectNo = $('#hdnProjectNo').val();
    ComeFrom = $('#hdnComeFrom').val();
    var hdnSelectedCompanyId;

    /*For Document Type*/
    hdnPageSize = parseInt($('#hdnPageSize').val());
    /*For Document Type*/


    BindProejctStatus();
    ViewProjectMasterRecord(ProjectNo);

    //Check Querystring Exist or Not
    //if (flag == 0) {

    //    var field = 'ProjectNo';
    //    var url = window.location.href;
    //    if (url.indexOf('?' + field + '=') != -1)
    //        alert('ProjectNo Exist..1');
    //    else if (url.indexOf('&' + field + '=') != -1)
    //        alert('ProjectNo Exist..2');

    //}

    $('#addDocumentName').blur(function () {

        var docname = $(this).val().trim();
        var doctype = $('#adddoctype').val();
        var JobNo = $("#hdJobNo").val();
        if (doctype != 2) {
            doctype = 0
        }
        var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: 0 }
        //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: 0 }

        CheckDocName(checkdata);

        //var docname = $("#addDocumentName").val();

    });
    $('#editDocumentName').blur(function () {

        var docname = $(this).val().trim();
        var doctype = $('#editdoctype').val();
        var docid = $('#editDocumentid').val();
        if (doctype != 2) {
            doctype = 0
        }
        var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: docid }
        //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: docid }

        CheckDocName(checkdata);

        // var docname = $("#editDocumentName").val();

    });

    switch (ComeFrom) {
        case "Summary":
            {
                ViewProjectJobList(ProjectNo);
                //$('#DisplayProjectEditMode').show(); // for all project master data
                $('#DisplayProjectEditMode').hide(); // for all project master data
                $("#_ProjectJobView").show(); // for All Bank list and its details
                $('#_ProjectContactView').hide(); // for Contact Information
                $("#_DocumentByTYpeList").hide(); // for Document management
                $("#_ProjectMilestoneList").hide(); // for project Milestone
                $("#_ProjectCommunicationView").hide();  // For Project Communicaiton management
                break;
            }
        case "Contacts":
            {

                ViewProjectJobList(ProjectNo);

                $.getScript("/js/Custom/ProjectContact.js", function (data, textStatus, jqxhr) {

                });

                //$('#DisplayProjectEditMode').show(); // for all project master data
                $('#DisplayProjectEditMode').hide(); // for all project master data
                $("#_ProjectJobView").hide(); // for All Bank list and its details
                $('#_ProjectContactView').show(); // for Contact Information
                $("#_DocumentByTYpeList").hide(); // for Document management
                $("#_ProjectMilestoneList").hide(); // for project Milestone
                $("#_ProjectCommunicationView").hide();  // For Project Communicaiton management
                $('#ViewProjectContacts').html($('#viewProjectName').html() + ' - ' + 'Contacts');
                break;
            }
        case "Messages":
            {
                ViewProjectJobList(ProjectNo);

                $.getScript("/js/Custom/ProjectCommunication.js", function (data, textStatus, jqxhr) {

                });

                $('#DisplayProjectEditMode').hide(); // for all project master data
                $("#_ProjectJobView").hide(); // for All Bank list and its details
                $('#_ProjectContactView').hide(); // for Contact Information
                $("#_DocumentByTYpeList").hide(); // for Document management
                $("#_ProjectMilestoneList").hide(); // for project Milestone
                $("#_ProjectCommunicationView").show();  // For Project Communicaiton management
                break;
            }

    }

    AlertMessageProjectManagement();


    $(".editDataContainer").hide();
    $(".editData").click(function () {
        //$(".editDataContainer").show();
        $('html,body').animate({ scrollTop: $(".editDataContainer").offset().top }, 'slow');
    });

   

    $('#DisplayDrawingsDocuments').click(function (event) {
        $('.xn-openable').removeClass('active');
        $(this).parents('.xn-openable').addClass('active');

        SelectedDocType = 'Drawing';

        //$('#DisplayProjectEditMode').show();
        $('#DisplayProjectEditMode').hide();
        $("#_ProjectCommunicationView").hide();

        $("#_ProjectMilestoneList").hide();
        $("#EditMilestoneDiv").hide();

        ProjectDocumentInit(SelectedDocType);

        $('html, body').animate({
            scrollTop: $("#ProjectDocumentForm").offset().top
        }, 500);
    });


    $('#DisplayContacts').click(function (event) {

        $('.xn-openable').removeClass('active');
        $(this).parents('.xn-openable').addClass('active');

        $.getScript("/js/Custom/ProjectContact.js", function (data, textStatus, jqxhr) {

        });

        //$('#DisplayProjectEditMode').show();
        $('#DisplayProjectEditMode').hide();
        $('#_ProjectContactView').show();
        $("#editProjectContactForm").hide();
        $("#_ProjectJobView").hide();
        $("#_DocumentByTYpeList").hide();
        $("#_ProjectMilestoneList").hide();
        $("#EditMilestoneDiv").hide();
        $("#_ProjectCommunicationView").hide();


        $('#ViewProjectContacts').html($('#viewProjectName').html() + ' - ' + 'Contacts');

        $('html, body').animate({
            scrollTop: $("#ProjectContactForm").offset().top
        }, 500);
    });


    $('#DisplayMilestones').click(function (event) {
        $('.xn-openable').removeClass('active');
        $(this).parents('.xn-openable').addClass('active');

        //$('#DisplayProjectEditMode').show();
        $('#DisplayProjectEditMode').hide();
        $('#_ProjectContactView').hide();
        $("#_ProjectJobView").hide();
        $("#_DocumentByTYpeList").hide();
        $("#_ProjectMilestoneList").show();
        $("#EditMilestoneDiv").hide();
        $('#ViewProjectMilestone').html($('#viewProjectName').html() + ' - ' + 'Milestones');
        $("#_ProjectCommunicationView").hide();
        $('html, body').animate({
            scrollTop: $("#ProjectMilestoneForm").offset().top
        }, 500);
    });



    $('#DisplayInvociesDocuments').click(function (event) {
        $('.xn-openable').removeClass('active');
        $(this).parents('.xn-openable').addClass('active');

        $('#CommonSuccessMessage').html('Under Development');
        $('#CommonSuccessModal').modal('show');

    });

    $('#btnUpdateProject').click(function (event) {

        $('#btnUpdateProject').attr('disabled', 'disabled');
        var formData = new FormData();

        formData.append("ProjectNo", ProjectNo);

        formData.append("ContractorSuper", $('#editContractorSuper').val().trim());
        formData.append("ContractorContactPerson", $('#editContractorContactPerson').val());
        formData.append("ContractorProjMgr", $('#editContractorProjMgr').val());

        formData.append("SchindlerSuperintendent", $('#editScSuperintendent').val());
        formData.append("SchindlerSalesExecutive", $('#editScSalesExecutive').val().trim());
        formData.append("SchindlerProjMgr", $('#editScProjMgr').val().trim());
        formData.append("TurnOverDate", $('#editTurnOverDate').val().trim());
        formData.append("Status", $('#editStatus option:selected').val().trim());

        $.ajax({
            url: '/ManageProject/UpdateProjectMaster',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                var updateMessage = $.grep(ProjectManagementMessageList, function (e) { if (e.MessageName == 'PM-Update') { return e } })
                if (updateMessage != '') {
                    //$('#DisplayProjectEditMode').show();
                    $('#DisplayProjectEditMode').hide();
                    $('#CommonSuccessMessage').html(updateMessage[0].Message);
                    $('#CommonSuccessModal').modal('show');

                    $('#btnUpdateProject').removeAttr('disabled');
                }

                ViewProjectMasterRecord(ProjectNo);

            },
            error: function (data) {
                //handle error
                $('#btnUpdateProject').removeAttr('disabled');
            }
        })
    });

    $('#btnResetProject').click(function (event) {
        ViewProjectMasterRecord(ProjectNo);
    });


    /* For Docs */

    $('#divdpExpDate').hide();
    $('#diveditdpExpDate').hide();
    $('#AddDocumentClose').click(function () {

        $("#hdJobNo").val("");
        $('#addDocumentName').val("");
        $('#addDocumentDesc').val("");
        $('#adddoctype').val($("#adddoctype option:first").val());

        $('#addProjectName').val("");
        $('#addJobName').val("");
        $('#dpExpDate').val("");
        $('#divdpExpDate').hide();
        $('#AddIsExpiryDate').prop('checked', false);
        $('#AddIsPublished').prop('checked', false);
        //for current company and project

        var SelectedDocTypeId = '';
        $("#adddoctype > option").each(function () {
            if (SelectedDocType == this.text) {
                SelectedDocTypeId = this.value;
            }
        });

        $('#adddoctype option[value="' + SelectedDocTypeId + '"]').attr("selected", "selected");
        if (SelectedDocType == 'Drawing') {
            $("#adddoctype > option").each(function () {
                if (SelectedDocType != this.text) {
                    $(this).remove();
                }

            });
            $('#adddoctype option[value="' + SelectedDocTypeId + '"]').prop('disabled', false);
            $('#adddoctype').prop('disabled', false);
        }
        else if (SelectedDocType != 'Drawing') {
            $("#adddoctype > option").each(function () {
                if ('Drawing' == this.text) {
                    $(this).remove();
                }

            });
        }


        $('#addProjectName').val($('#viewProjectName').html());


        Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
    });
    $('#btnAddDocument').click(function (event) {
        event.preventDefault();
        //$('#btnAddDocument').attr('disabled', 'disabled');
        var isValid = true;

        var doctype = $("#adddoctype option:selected").text();




        if ($('#adddoctype').val() == '') {
            $('#adddoctype').css({ "border": "1px solid red" });
            isValid = false;
        }


        if (doctype != "Other") {

            if ($('#addDocumentName').val() == '') {
                $('#addDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addDocumentName').focus();
            }
            else {
                $('#addDocumentName').css({ "border": "", "background": "" });
            }

            if ($('#addCompanyName').val() == '') {
                $('#addCompanyName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addCompanyName').focus();
            }
            else {
                $('#addCompanyName').css({ "border": "", "background": "" });
            }

        }
        else {

            if ($('#addDocumentName').val() == '') {
                $('#addDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addDocumentName').focus();
            }
            else {
                $('#addDocumentName').css({ "border": "", "background": "" });
            }

            $('#addCompanyName').css({ "border": "", "background": "" });

        }


        var filename = $('div.dz-filename span').text();
        if (filename == null || filename == "" || filename == undefined) {

            $('#dropzoneForm').css({ "border": "1px solid red" });

            isValid = false;
        }
        else {
            $('#dropzoneForm').css({
                "border": "",
                "background": ""
            });
        }

        var fileerrormsg = $('#dropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#dropzoneForm').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {

        }
        //unique doc name start
        if ($('#addDocumentName').val() != "" && isValid == true) {
            var documentname = $('#addDocumentName').val().trim();
            var documenttype = $('#adddoctype').val();
            var JobNo = $("#hdJobNo").val();
            if (documenttype != 2) {
                documenttype = 0
            }
            var checkdata = { JobId: JobNo, DocumentName: documentname, ProjectID: ProjectNo, DocumentTypeID: documenttype, DocumentID: 0 }


            isValid = CheckDocName(checkdata);

        }
        //unique doc name end


        if (isValid == false) {
            $('#btnAddDocument').removeAttr('disabled');
            return false;

        }


        var formData = new FormData();


        var Publishedstatus;





        if ($("#AddIsPublished").is(':checked'))

            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentName", $('#addDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#adddoctype').val().trim());
        formData.append("Description", $('#addDocumentDesc').val().trim());
        formData.append("CompanyName", $('#addCompanyName').val());
        formData.append("ProjectName", $('#addProjectName').val());
        formData.append("JobName", $('#addJobName').val());
        formData.append("ExpiredDate", $('#dpExpDate').val());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);
        formData.append("CompanyId", $('#hdnSelectedCompanyId').val());


        if (isValid) {
            $.ajax({
                url: '/ManageProject/AddProjectDocument',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    var updateMessage = $.grep(ProjectManagementMessageList, function (e) { if (e.MessageName == 'PM-Document-Insert') { return e } })
                    if (updateMessage != '') {
                        $('#CommonSuccessMessage').html(updateMessage[0].Message);
                        $('#CommonSuccessModal').modal('show');

                    }

                    $('#AddDocument').modal('hide');

                    var table = $('#Documenttable').DataTable();
                    table.ajax.reload();
                    $('#btnAddDocument').removeAttr('disabled');

                },
                error: function (data) {
                    //handle error
                    $('#btnAddDocument').removeAttr('disabled');
                }
            })
        }
        else {
            return false;
        }


    });
    $('#btnUpdateDocument').click(function (event) {

        $('#btnUpdateDocument').attr('disabled', 'disabled');
        var formData = new FormData();

        var isValid = true;

        var doctype = $("#editdoctype option:selected").text();

        if (doctype != "Other") {

            if ($('#editDocumentName').val() == '') {
                $('#editDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editDocumentName').focus();
            }
            else {
                $('#editDocumentName').css({ "border": "", "background": "" });
            }

            if ($('#editCompanyName').val() == '') {
                $('#editCompanyName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editCompanyName').focus();
            }
            else {
                $('#editCompanyName').css({ "border": "", "background": "" });
            }

        }
        else {

            if ($('#editDocumentName').val() == '') {
                $('#editDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editDocumentName').focus();
            }
            else {
                $('#editDocumentName').css({ "border": "", "background": "" });
            }

            $('#editCompanyName').css({ "border": "", "background": "" });

        }

        var filename = $('#editdropzoneForm div.dz-filename span').text();


        var fileerrormsg = $('#editdropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#editdropzoneForm').css({ "border": "1px solid red" });
            isValid = false;

        }
        else {

        }

        if ($('#editattchFilename').find('span.forPadding').length != 0) {


        }
        else {

            if (filename == null || filename == "" || filename == undefined) {
                $('#editdropzoneForm').css({ "border": "1px solid red" });


                isValid = false;
            }
            else {
                $('#editdropzoneForm').css({ "border": "" });
            }

        }
        // unique doc name start
        if ($('#editDocumentName').val() != "" && isValid == true) {
            var documentname = $('#editDocumentName').val().trim();
            var documenttype = $('#editdoctype').val();
            var docid = $('#editDocumentid').val();
            var JobNo = $("#edithdJobNo").val();
            if (documenttype != 2) {
                documenttype = 0
            }
            var checkdata = { JobId: JobNo, DocumentName: documentname, ProjectID: ProjectNo, DocumentTypeID: documenttype, DocumentID: docid }

            isValid = CheckDocName(checkdata);

        }
        //unique doc name end

        if (isValid == false) {
            $('#btnUpdateDocument').removeAttr('disabled');
            return false;
        }


        var formData = new FormData();

        var Publishedstatus;


        if ($("#editIsPublished").is(':checked'))
            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentID", $('#editDocumentid').val().trim());
        formData.append("DocumentName", $('#editDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#editdoctype').val().trim());
        formData.append("Description", $('#editDocumentDesc').val().trim());
        formData.append("CompanyName", $('#editCompanyName').val().trim());
        formData.append("ProjectName", $('#editProjectName').val().trim());
        formData.append("JobName", $('#editJobName').val().trim());
        formData.append("ExpiredDate", $('#editdpExpDate').val().trim());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);
        formData.append("CompanyId", $('#hdnSelectedCompanyId').val());
        $.ajax({
            url: '/ManageProject/UpdateProjectDocument',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                var updateMessage = $.grep(ProjectManagementMessageList, function (e) { if (e.MessageName == 'PM-Document-Update') { return e } })
                if (updateMessage != '') {
                    $('#CommonSuccessMessage').html(updateMessage[0].Message);
                    $('#CommonSuccessModal').modal('show');

                }

                $('#editDocument').modal('hide');

                var table = $('#Documenttable').DataTable();
                table.ajax.reload();
                $('#btnUpdateDocument').removeAttr('disabled');

            },
            error: function (data) {
                //handle error
                $('#btnUpdateDocument').removeAttr('disabled');
            }
        })
    });

    $('#documentok').click(function () {
        $('#SuccessDocument').modal('hide');

    });
    $('#editdocumentok').click(function () {
        $('#SuccessEditDocument').modal('hide');

    });

    $('#btnDeleteDocument').click(function () {
        $('#documentDeleteConfirmModel').modal('show');
        $('#btnDeleteYes').click(function () {
            var DocID = $('#editDocumentid').val();
            DeleteDocument(DocID);
        });
    });

    /* For Docs */

    var DocType = GetQueryStringParams('DocType');

    if (DocType != undefined && DocType != '' && DocType != null) {

        if (DocType == "documents") {
            $('#hdnProjectNo').val(GetQueryStringParams('ProjectNo'));
            $('#hdnSelectedCompanyId').val(GetQueryStringParams('CompanyId'));

            $("#DisplayDocuments").trigger("click");
        }
    }


});

function GetQueryStringParams(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {

        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }

}

function ProjectDocumentInit(SelectedDocType) {

    $("#_ProjectJobView").hide();
    $('#_ProjectContactView').hide();
    $("#_ProjectMilestoneList").hide();
    $("#_DocumentByTYpeList").show();

    var ProjectNo = $('#hdnProjectNo').val();

    $('#ViewProjectDocument').html($('#viewProjectName').html() + ' - ' + SelectedDocType);

    $('#SelectDocumentType').val(SelectedDocType);
    SelectedCompanyId = $('#hdnSelectedCompanyId').val();
    ViewProjectDocumentList(ProjectNo, SelectedDocType, SelectedCompanyId, '');
    $('#Documenttable_filter').hide(); // To remove autogenerated server textbox
    $('#Documenttable').on('draw.dt', function () { // To set design again
        ProjectDocumentListDatatablePaging();
    });
    $('.dataTables_length').hide(); // To hide page size selection

    BindDocType();

    //---------------------Document -----------
    // For Add
    var CompId = 0;
    var txtAddCompanyoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCompany/",
                data: '{ searchTerm:"' + $('#addCompanyName').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CompanyName,
                            value: item.CompanyID,
                            CompanyID: item.CompanyID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {

            $("#addCompanyName").val(ui.item.label);
            CompId = ui.item.CompanyID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };
    $(document).on("keydown.autocomplete", '#addCompanyName', function (event) {

        $(this).autocomplete(txtAddCompanyoptions);

    })

    var txtAddProjectoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindProjectOfCompany/",
                data: '{ searchTerm:"' + $('#addProjectName').val() + '", CompanyId:"' + CompId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {

                            label: item.ProjectName,
                            value: item.ProjectID,
                            ProjectID: item.ProjectID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addProjectName").val(ui.item.label);
            Proid = ui.item.ProjectID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#addProjectName', function (event) {

        $(this).autocomplete(txtAddProjectoptions);

    })

    //Add job data for doc
    var AddJobId = 0;
    var EditJobId = 0;
    var txtAddJoboptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindJobOfProject/",
                data: '{ searchTerm:"' + $('#addJobName').val() + '", ProjectNo:"' + $('#hdnProjectNo').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {

                            label: item.JobName,
                            value: item.JobId,
                            JobId: item.JobId,
                            JobNo: item.JobNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addJobName").val(ui.item.label);
            AddJobId = ui.item.JobId
            $("#hdJobNo").val(ui.item.JobNo);

            //var docname = $('#addDocumentName').val().trim();
            //var doctype = $('#adddoctype').val();
            // var JobNo = $("#hdJobNo").val();
            //if (doctype != 2) {
            //    doctype = 0
            //}
            //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: 0 }
            //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: 0 }

            //CheckDocName(checkdata);

            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#addJobName', function (event) {

        $(this).autocomplete(txtAddJoboptions);
    })


    // For Edit

    //Edit Document side
    var EditCompId = 0;
    var txtEditCompanyoptions = {

        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCompany/",
                data: '{ searchTerm:"' + $('#editCompanyName').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CompanyName,
                            value: item.CompanyID,
                            CityID: item.CompanyID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editCompanyName").val(ui.item.label);
            EditCompId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#editCompanyName', function (event) {

        $(this).autocomplete(txtEditCompanyoptions);

    })

    //Edit project data on Edit doc
    var EditProId = 0;

    var txtEditProjectoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindProjectOfCompany/",
                data: '{ searchTerm:"' + $('#editProjectName').val() + '", CompanyId:"' + EditCompId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {

                            label: item.ProjectName,
                            value: item.ProjectID,
                            CityID: item.ProjectID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editProjectName").val(ui.item.label);
            EditProId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#editProjectName', function (event) {

        $(this).autocomplete(txtEditProjectoptions);

    })

    var txtEditJoboptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindJobOfProject/",
                data: '{ searchTerm:"' + $('#editJobName').val() + '", ProjectNo:"' + $('#hdnProjectNo').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {

                            label: item.JobName,
                            value: item.JobId,
                            CityID: item.JobId,
                            JobNo: item.JobNo,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editJobName").val(ui.item.label);
            EditJobId = ui.item.CityID
            $("#edithdJobNo").val(ui.item.JobNo);

            //var docname = $('#editDocumentName').val().trim();
            //var doctype = $('#editdoctype').val();
            //var JobNo = $("#edithdJobNo").val();
            //var docid = $('#editDocumentid').val();
            //if (doctype != 2) {
            //    doctype = 0
            //}
            //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: docid}

            //CheckDocName(checkdata);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };
    $(document).on("keydown.autocomplete", '#editJobName', function (event) {

        $(this).autocomplete(txtEditJoboptions);

    })
    //

    //---------------------Docuemnt -----------

}
function GoToBankSummary(JobNo) {

    ViewProjectJobRecord(JobNo);
    //$(".editDataContainer").show();

    $('html,body').animate({ scrollTop: $(".editDataContainer").offset().top }, 'slow');
}
function ViewProjectMasterRecord(id) {

    $.ajax({
        type: "POST",
        url: "/ManageProject/GetProjectMasterRecordByProjectID/",//UserEdit
        data: { ProjectNo: id },
        async: false,
        success: function (projectMasterRecord) {



            $('#hdnSelectedCompanyId').val(projectMasterRecord.data.CompanyId);
            $('#hdnProjectName').val(projectMasterRecord.data.ProjectName);

            // View Mode
            $('#breadcrumbProjectName').html(projectMasterRecord.data.ProjectName).attr("href", "/ManageProject/ProjectMasterView/" + id + "/Summary");

            $('#projectCommunicationTitle').html(projectMasterRecord.data.ProjectName); // for communication title

            $('#viewProjectName').html(projectMasterRecord.data.ProjectName);
            $('#viewProjectBanksUnitInfo').html('[ Project # ' + projectMasterRecord.data.ProjectNo + ', ' + projectMasterRecord.data.Banks + ' Banks, ' + projectMasterRecord.data.UnitCount + ' Units]');

            // Edit mode initial data

            $('#editAlternateProjectNo').val(projectMasterRecord.data.AlternateProjectNo);
            $('#editProjectAddress').val(projectMasterRecord.data.Job_Address);
            $('#editCompany').val(projectMasterRecord.data.CompanyName);
            $('#editContractorSuper').val(projectMasterRecord.data.ContractorSuper);
            $('#editContractorContactPerson').val(projectMasterRecord.data.ContractorContactPerson);
            $('#editContractorProjMgr').val(projectMasterRecord.data.ContractorProjMgr);
            $('#editScSuperintendent').val(projectMasterRecord.data.SchindlerSuperintendent);
            $('#editScSalesExecutive').val(projectMasterRecord.data.SchindlerSalesExecutive);
            $('#editScProjMgr').val(projectMasterRecord.data.SchindlerProjMgr);

            $('#editScSuperintendentEmail').val(projectMasterRecord.data.SchindlerSuperintendentEmail);
            $('#editScSalesExecutiveEmail').val(projectMasterRecord.data.SchindlerSalesExecutiveEmail);
            $('#editScProjMgrEmail').val(projectMasterRecord.data.SchindlerProjMgrEmail);

            $('#editScSuperintendent_Phone').val(setPhoneMasking(projectMasterRecord.data.SchindlerSuperintendent_Phone));
            $('#editScSalesExecutive_Phone').val(setPhoneMasking(projectMasterRecord.data.SchindlerSalesExecutive_Phone));
            $('#editScProjMgr_Phone').val(setPhoneMasking(projectMasterRecord.data.SchindlerProjMgr_Phone));
           // $('#editScSuperintendent_Phone').mask('000-000-0000');
            $('#editProjectAwardDate').val(projectMasterRecord.data.AwardDate);
           
            $('#editBookingCompleteDate').val(projectMasterRecord.data.BookingComplDate);
            $('#editContractSignedYN').prop('checked', (projectMasterRecord.data.IsContractSigned == 'Yes') ? true : false);
            $('#editContractSignedYN').parent('div').addClass((projectMasterRecord.data.IsContractSigned == 'Yes') ? 'checked' : '');

            $('#editContractSignedDate').val(projectMasterRecord.data.ContractReceivedDate); // as PD's suggestion becuase Contract Signed Date was not there
            $('#editNOD').val(projectMasterRecord.data.NOD);
            $('#editOriginalNOD').val(projectMasterRecord.data.OriginalNODDate);
            $('#editNODConfirmYN').prop('checked', (projectMasterRecord.data.NODConfirm == 'Yes') ? true : false);
            $('#editNODConfirmYN').parent('div').addClass((projectMasterRecord.data.NODConfirm == 'Yes') ? 'checked' : '');
            $('#editNODConfirmDate').val(projectMasterRecord.data.NODConfirmDate);
            $('#editTurnOverDate').val(projectMasterRecord.data.TurnOverDate);

            $('#editStatus option[value="' + projectMasterRecord.data.Status + '"]').attr("selected", "selected");
            $('#hdnProjectStatusForReset').val(projectMasterRecord.data.Status); // set old value to hidden field for reset button
            $('#addCompanyName').val(projectMasterRecord.data.CompanyName); // for document add

        }
    })
}
function setPhoneMasking(phonenNumber) {
    var srphone = '';
    if (phonenNumber != '') {
        var a = phonenNumber;
        srphone = a.substr(0, 3) + '-' + a.substr(3, 3) + '-' + a.substr(6, 10);
    }

    return srphone;
}
function ViewProjectJobList(id) {

    var hdnPageSize = parseInt($('#hdnPageSize').val());
    var hdnUserRoleId = $('#hdnUserRoleId').val();
    var table = $('#ProjectJobListDatatable').DataTable({

        "ajax": {
            "url": "/ManageProject/GetAllProjectJobRecords",
            "type": "POST",
            "datatype": "JSON",
            'data': {
                ProjectNo: id,
            },
        },
        "autoWidth": false, // for disabling autowidth
        'aaSorting': [[1, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "serverSide": true, // for process server side
        "processing": true,
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "bInfo": false,
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },

        "columns": [
        { "data": "JobNo", "name": "JobNo", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        { "data": "JobNo", "name": "JobNo", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "BankDesc", "name": "BankDesc", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Units", "name": "UnitCount", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Capacity", "name": "Capacity", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Speed", "name": "Speed", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Floor", "name": "Floor", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Rear", "name": "Rear", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "ProdType", "name": "ProdType", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "ProdCode", "name": "ProdCode", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "StatusName", "name": "StatusName", "Searchable": true, "sClass": "txtAlignCenter" },
        //{
        //    "data": "StatusName",
        //    "name": "StatusName",
        //    "sClass": "txtAlignCenter",
        //    "render": function (data, type, row) {


        //        switch (data) {
        //            case "Approved":
        //                {
        //                    return '<span class="label s-approved">Approved</span>'
        //                    break;
        //                }
        //            case "Pending":
        //                {
        //                    return '<span class="label s-pending">Pending</span>'
        //                    break;
        //                }
        //            case "Completed":
        //                {
        //                    return '<span class="label s-complete">Completed</span>'
        //                    break;
        //                }
        //            case "Progress":
        //                {
        //                    return '<span class="label s-progress">Progress</span>'
        //                    break;
        //                }
        //            default:
        //                {
        //                    return ''
        //                }
        //        }

        //    }

        //},

        {
            "data": "JobNo",
            "sClass": "txtAlignCenter",

            "render": function (data, type, row) {
                var MsgCnt = "";
                if (row.MessageCount == "0") { MsgCnt = "" } else { MsgCnt = '<span class="infocst">' + row.MessageCount + '</span>'; }
                return '<a href="javascript:void(0)" class="btnsml_action e-contract"   onclick="GoToBankContacts(\'' + data + '\') "> </a> ' +
                       '<a href="javascript:void(0)" class="btnsml_action e-drawing"    onclick="GoToBankDrawing(\'' + data + '\')" > </a> ' +
                       '<a href="javascript:void(0)" class="btnsml_action e-milestone"    onclick="GoToBankMilestone(\'' + data + '\')" > </a> '
                //'<a href="javascript:void(0)" class="btnsml_action e-communicate"    onclick="GoToBankMessage(\'' + data + '\') "> ' + MsgCnt + '</a>'
            }
        }

        ],
        createdRow: function (row, data, index) {
            if (hdnUserRoleId != 2 && hdnUserRoleId != 4) {
                $(row).addClass('table-click-ProjectJobList editData');
            }
        },
        "initComplete": function (settings, json) {
            ProjectJobListDatatablePaging();
        },
        "drawCallback": function (settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });

    $('.dataTables_length').hide(); // To hide page size selection

    $('#ProjectJobListDatatable_filter').hide(); // To remove autogenerated server textbox


    $('#ProjectJobListDatatable').on('draw.dt', function () { // To set design again
        ProjectJobListDatatablePaging();
        ProjectMasterViewTooltip();
    });


    $('#ProjectJobListDatatableSearch').unbind().keyup(function () { // to handle search functionality
        var value = $(this).val();
        if (value.length > 2) {
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    var column = table.column(11);
    if (hdnUserRoleId == 2 || hdnUserRoleId == 4) {
        $('#editStatus').prop('disabled', true);
        column.visible(false);

    }
    else {
        column.visible(true);
        $('#editStatus').prop('disabled', false);
    }
}
function ProjectJobListDatatablePaging() {
    $('#ProjectJobListDatatable_paginate').children('ul').addClass('flatpagi');
    $('#ProjectJobListDatatable_previous').children('a').remove();
    $('#ProjectJobListDatatable_previous').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#ProjectJobListDatatable_next').children('a').remove();
    $('#ProjectJobListDatatable_next').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}
function BindProejctStatus() {

    $.ajax({
        type: "GET",
        url: "/Common/BindStatusList",
        async: false,
        data: { module: 'project' },
        success: function (statusList) {

            $('#editStatus').html("");
            $('#editStatus').append($("<option/>", ($({ value: '', text: '--Select--' }))));

            $.each(statusList, function (i, val) {

                $('#editStatus').append($("<option/>", ($({ value: val.StatusId, text: val.StatusName }))));


            });

            //    $('#editStatus').selectpicker();


        }
    })
}
function BindJobStatus() {

    $.ajax({
        type: "GET",
        url: "/Common/BindStatusList",
        async: false,
        data: { module: 'project' },
        success: function (statusList) {

            $('#editJobStatus').html("");
            $('#editJobStatus').append($("<option/>", ($({ value: '', text: '--Select--' }))));

            $.each(statusList, function (i, val) {

                $('#editJobStatus').append($("<option/>", ($({ value: val.StatusId, text: val.StatusName }))));


            });

            //    $('#editStatus').selectpicker();


        }
    })
}
function BindJobMilestoneStatus() {

    $.ajax({
        url: '/Common/BindMilestoneType',
        type: 'GET',
        async: false,
        success: function (statusList) {

            $('#editJobStatus').html("");
            $('#editJobStatus').append($("<option/>", ($({ value: '0', text: '' }))));

            $.each(statusList, function (i, val) {
                $('#editJobStatus').append($("<option/>", ($({ value: val.MilestoneId, text: val.MilestoneTitle }))));
            });

            //    $('#editStatus').selectpicker();


        }
    })
}
function EditProjectMasterRecord(id) {
    $.ajax({
        type: "GET",
        url: "/ManageProject/UpdateProjectMaster/",
        data: { userId: id },
        success: function (userdetail) {

            $('#edituserid').val(userdetail.UserID);
            $('#editFullname').val(userdetail.FirstName);
            //$('#editlastname').val(userdetail.LastName);
            $('#editphone').val(userdetail.ContactNumber);
            $('#editemail').val(userdetail.Email);
            //$('#editRoleID').prop('checked', (userdetail.RoleID == 3) ? true : false);
            $('#editUserpic').attr('src', userdetail.ProfilePic);
            $('#editpicFilename').val(userdetail.ProfilePic);

            $('#editStreetAddress').val(userdetail.Address1);
            $('#editState').val(userdetail.State);
            $('#editCity').val(userdetail.City);
            $('#editZipCode').val(userdetail.PostalCode);

            $("#editDesignation").val(userdetail.DesignationId);
            $('#editStatus').val(userdetail.AccountStatus);

            BindUserModulePermission(id);
        }
    })
}
function ViewProjectJobRecord(id) {
    //BindJobStatus();
    BindJobMilestoneStatus();
    $.ajax({
        type: "POST",
        url: "/ManageProject/GetProjectJobRecordByJobNo/",//UserEdit
        data: { JobNo: id },
        success: function (projectJobRecord) {

            // View Mode
            //$('#breadcrumbProjectName').html(projectMasterRecord.data.ProjectName + ' - Job Summary').attr("href", "/ManageProject/ProjectMasterView/" + id);

            //$('#viewProjectName').html(projectMasterRecord.data.ProjectName);
            // $('#viewProjectBanksUnitInfo').html('[ Project # ' + projectMasterRecord.data.ProjectNo + ', ' + projectMasterRecord.data.Banks + ' Banks, ' + projectMasterRecord.data.UnitCount + ' Units]');
            $('#ViewJobID').html('[ GO # ' + projectJobRecord.data.JobNo + ']');
            $('#ViewJobDesc').html(projectJobRecord.data.BankDesc);
            $('#viewSerialNumber').html(projectJobRecord.data.Serial_Num);
            $('#viewOffice').html(projectJobRecord.data.Office);
            $('#viewJobDistrict').html(projectJobRecord.data.District);
            $('#viewApprovalstoGC').html(projectJobRecord.data.ApprovalstoGC);
            $('#viewApprovalstoGberg').html(projectJobRecord.data.ApprovalstoGBer);
            $('#ViewNOD').html(projectJobRecord.data.NOD);
            $('#viewOriginalNOD').html(projectJobRecord.data.OriginalNODDate);
            $('#viewNODConfirmYN').html(projectJobRecord.data.NODConfirm);
            $('#viewNODConfirmDate').html(projectJobRecord.data.NODConfirmDate);
            $('#viewForecastCloseDate').html(projectJobRecord.data.ForecastCloseDate);
            $('#viewNOSplanned').html(projectJobRecord.data.NOSPlanned);
            $('#viewNOSactual').html(projectJobRecord.data.NOSActual);
            $('#viewProductCode').html(projectJobRecord.data.ProdCode);
            $('#viewProductType').html(projectJobRecord.data.Prod);
            $('#viewCWTLocation').html(projectJobRecord.data.CwtLoc);
            $('#viewElevatorType').html(projectJobRecord.data.Elev_Type);
            $('#viewVolt').html(projectJobRecord.data.Volt);
            $('#viewTravel').html(projectJobRecord.data.Travel);
            $('#viewDoorType').html(projectJobRecord.data.DoorType);
            //$('#viewDoorHand').html(projectJobRecord.data.);
            $('#viewCapacity').html(projectJobRecord.data.Capacity);
            $('#viewSpeed').html(projectJobRecord.data.Speed);
            $('#viewFloor').html(projectJobRecord.data.Num_Floors);
            $('#viewRearOpening').html(projectJobRecord.data.Rear);
            $('#viewPrice').html(projectJobRecord.data.Price);
            $('#viewPaid').html(projectJobRecord.data.PaidPercent);
            $('#viewBilled').html(projectJobRecord.data.BilledPercent);
            $('#viewBaseHours').html(projectJobRecord.data.BaseHrs);
            $('#viewEstimatedHours').html(projectJobRecord.data.EstHrs);
            $('#viewActualHours').html(projectJobRecord.data.ActHrs);
            $('#viewGCProjMgr').html(projectJobRecord.data.ContractorProjMgr);
            $('#viewGCProjMgremail').html(projectJobRecord.data.Proj_Mgr_Email);
            $('#viewGCProjMgrPhone').html(projectJobRecord.data.ProjectMgrPhone);
            $('#viewGCSuper').html(projectJobRecord.data.ContractorSuper);
            //$('#viewGCSalesRep').html(projectJobRecord.data.);
            //$('#viewSECSuper').html(projectJobRecord.data.);
            //$('#viewSECSalesExe').html(projectJobRecord.data.);
            //$('#viewSECProjMgr').html(projectJobRecord.data.);
            $('#viewTeamAssigned').html(projectJobRecord.data.TeamAssigned);
            $('#viewWhyBumpOut').html(projectJobRecord.data.ActionText);
            $('#viewSupersComments').html(projectJobRecord.data.SuperComments);
            $('#viewSalesComments').html(projectJobRecord.data.SalesComments);
            $('#viewJobStatus').html(projectJobRecord.data.StatusName);

            $('#viewJobCompletion').html(projectJobRecord.data.CompletionPercent);





            // Edit mode initial data



            $('#editSerialNumber').val(projectJobRecord.data.Serial_Num);
            $('#editOffice').val(projectJobRecord.data.Office);
            $('#editJobDistrict').val(projectJobRecord.data.District);
            $('#editApprovalstoGC').val(projectJobRecord.data.ApprovalstoGC);
            $('#editApprovalstoGberg').val(projectJobRecord.data.ApprovalstoGBer);
            $('#editNOD').val(projectJobRecord.data.NOD);
            $('#editOriginalNOD').val(projectJobRecord.data.OriginalNODDate);
            $('#editNODConfirm').val(projectJobRecord.data.NODConfirm);
            $('#editForecastCloseDate').val(projectJobRecord.data.ForecastCloseDate);
            $('#editNOSplanned').val(projectJobRecord.data.NOSPlanned);
            $('#editNOSactual').val(projectJobRecord.data.NOSActual);
            $('#editProductCode').val(projectJobRecord.data.ProdCode);
            $('#editProductType').val(projectJobRecord.data.Prod);
            $('#editCWTLocation').val(projectJobRecord.data.CwtLoc);
            $('#editElevatorType').val(projectJobRecord.data.Elev_Type);
            $('#editVolt').val(projectJobRecord.data.Volt);
            $('#editTravel').val(projectJobRecord.data.Travel);
            $('#editDoorType').val(projectJobRecord.data.DoorType);
            //$('#editDoorHand').val(projectJobRecord.data.);
            $('#editCapacity').val(projectJobRecord.data.Capacity);
            $('#editSpeed').val(projectJobRecord.data.Speed);
            $('#editFloor').val(projectJobRecord.data.Num_Floors);
            $('#editRearOpening').val(projectJobRecord.data.Rear);
            $('#editPrice').val(NumberToCurrencyFormat(projectJobRecord.data.Price));
            $('#editPaid').val(projectJobRecord.data.PaidPercent);
            $('#editBilled').val(projectJobRecord.data.BilledPercent);
            $('#editBaseHours').val(projectJobRecord.data.BaseHrs);
            $('#editEstimatedHours').val(projectJobRecord.data.EstHrs);
            $('#editActualHours').val(projectJobRecord.data.ActHrs);
            $('#editGCProjMgr').val(projectJobRecord.data.ContractorProjMgr);
            $('#editGCProjMgrEmail').val(projectJobRecord.data.Proj_Mgr_Email);
            $('#editGCProjMgrPhone').val(projectJobRecord.data.ProjectMgrPhone);
            $('#editGCSuper').val(projectJobRecord.data.ContractorSuper);
            //$('#editGCSalesRep').val(projectJobRecord.data.);
            //$('#editSECSuper').val(projectJobRecord.data.);
            //$('#editScSalesExecutive').val(projectJobRecord.data.);
            //$('#editScProjMgr').val(projectJobRecord.data.);
            $('#editTeamAssigned').val(projectJobRecord.data.TeamAssigned);
            $('#editWhyBumpOut').val(projectJobRecord.data.ActionText);
            $('#editSupersComments').val(projectJobRecord.data.SuperComments);
            $('#editSalesComments').val(projectJobRecord.data.SalesComments);

            $('#editJobStatus option[value="' + projectJobRecord.data.Status + '"]').attr("selected", "selected");
            $('#editJobCompletion').val(projectJobRecord.data.CompletionPercent);



            //$('#viewProjectName').html(projectMasterRecord.data.ProjectName);
            //$('#viewProjectBanksUnitInfo').html('[ Project # ' + projectMasterRecord.data.ProjectNo + ', ' + projectMasterRecord.data.Banks + ', ' + projectMasterRecord.data.UnitCount + ']');



            //$('#editContractSignedYN').prop('checked', (projectMasterRecord.data.IsContractSigned == 'Yes') ? true : false);
            //$('#editContractSignedYN').parent('div').addClass((projectMasterRecord.data.IsContractSigned == 'Yes') ? 'checked' : '');


            //$('#editStatus option[value="' + projectMasterRecord.data.Status + '"]').attr("selected", "selected");

        }
    })
    ViewProjectJobFieldsShowHide();
}

function ViewProjectJobFieldsShowHide() {
    var RoleId = $('#hdnUserRoleId').val();
    if (RoleId != 1 && RoleId != 3) {
        $('#lblEditJVNOD').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#lblEditJVOriginalNOD').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#lblEditJVNODConfirm').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editForecastCloseDate').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editNOSplanned').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editNOSactual').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editWhyBumpOut').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editApprohidestoGC').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editApprovalstoGberg').parents('.col-md-4.col-sm-6.form-group').hide()
        $('#editSupersComments').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editSalesComments').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editPrice').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editBaseHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editEstimatedHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editActualHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editVolt').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editTravel').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editDoorType').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editCapacity').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editSpeed').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editFloor').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editRearOpening').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editCWTLocation').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editTeamAssigned').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editJobCompletion').parents('.col-md-4.col-sm-6.form-group').hide();



    }


}


/* Code for Document type*/
function convertjsonOnlydate(Anydate) {

    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}
function ViewProjectDocumentList(ProjectNo, docType, SelectedCompanyId, searchTerm) {

    //model.DocumentType = $('#SelectDocumentType').val();

    var model = new Object();
    model.ProjectNo = ProjectNo;
    model.DocumentType = docType;
    model.CompanyId = SelectedCompanyId;
    FinalTable = $('#Documenttable').DataTable({

        "ajax": {
            "url": "/ManageProject/GetProjectDocumentList",
            "type": "POST",
            "datatype": "JSON",
            'data': model,

        },
        destroy: true,
        'aaSorting': [[1, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": {
            "sEmptyTable": "No data available",
            "zeroRecords": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },

        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "columns": [
        { "data": "DocumentID", "name": "DocumentID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "DocumentName", "name": "DocumentName", "Searchable": true, "sClass": "txtAlignCenter",
            "render": function (data, type, row) {

                var xyz = '<a target="_new" href="/ManageDocument/ViewDocumentName?DocPath=' + escape(row.DocumentPath) + '">' + data + '</a>';

                return xyz;
            }

        },
         { "data": "Description", "name": "Description", "Searchable": true },
         { "data": "DocumentType", "name": "DocumentType", "Searchable": true, "sClass": "txtAlignCenter", },
         { "data": "PublishedBy", "name": "PublishedBy", "sClass": "txtAlignCenter" },


            {
                "data": "ExpiredDate",
                "name": "ExpiredDate",
                "sClass": "txtAlignCenter",
            },

            {
                "data": "PublishedStatus",
                "name": "PublishedStatus",
                "sClass": "txtAlignCenter",
                "render": function (data, type, row) {

                    switch (data) {
                        case "Published":
                            {
                                return '<span class="label s-published">Published</span>'
                                break;
                            }
                        case "Draft":
                            {
                                return '<span class="label s-draft">Draft</span>'
                                break;
                            }
                    }

                }
            },
          {
              "data": "DocumentID",
              "sClass": "txtAlignCenter",
              "render": function (data, type, row) {

                  var d = new Date();

                  var month = d.getMonth() + 1;
                  var day = d.getDate();

                  var output =
                      (month < 10 ? '0' : '') + month + '/' +
                      (day < 10 ? '0' : '') + day + '/' +
                      d.getFullYear();
                  var Expdate = convertjsonOnlydate(row.ExpiredDate);


                  if (RoleId == 1 || RoleId == 3) {

                      return '<a id="' + data + '" name="EditDocumentButtonname" class="btnsml_action e-edit" href="javascript:void(0)" data-toggle="modal"  data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>    <a href="javascript:void(0)" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                  }
                  else {
                      if (Expdate != null && Expdate != "" && Expdate != undefined) {
                          if (Expdate > output) {

                              return ' <a href="javascript:void(0)" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                          }
                          else {

                              return '';
                          }
                      }
                      else {
                          return ' <a href="javascript:void(0)" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                      }

                  }


              }
          }
        ]
        ,
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }
        ,
        createdRow: function (row, data, index) {
            $(row).addClass('table-click-ProjectDocumentList editData');

        },
        "initComplete": function (settings, json) {
            ProjectDocumentListDatatablePaging();
            ProjectMasterViewTooltip();
        }
    });

    $('#editIsExpiryDate').on('change', function () {
        if ($(this).is(":checked")) {
            $('#diveditdpExpDate').show();
        }
        else {
            $('#diveditdpExpDate').hide();
            $('#editdpExpDate').val(null);
        }
    });
    $('#AddIsExpiryDate').on('change', function () {

        if ($(this).is(":checked")) {
            $('#divdpExpDate').show();
        }
        else {
            $('#divdpExpDate').hide();
            $('#dpExpDate').val(null);
        }
    });
    //FinalTable.destroy();
    var RoleId = $('#hdnUserRoleId').val();
    if (RoleId != 1 && RoleId != 3) {

        fnShowHide(3);
    }

    if (searchTerm != '' && searchTerm != undefined)
        FinalTable.search(searchTerm).draw();
};

function fnShowHide(iCol) {
    /* Get the DataTables object again - this is not a recreation, just a get of the object */
    var oTable = $('#Documenttable').dataTable();

    var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
    oTable.fnSetColumnVis(iCol, bVis ? false : true);
}
function ProjectDocumentListDatatablePaging() {
    $('#Documenttable_paginate').children('ul').addClass('flatpagi');
    $('#Documenttable_previous').children('a').remove();
    $('#Documenttable_previous').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Documenttable_next').children('a').remove();
    $('#Documenttable_next').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}
function DeleteDocument(id) {

    $.ajax({
        type: "POST",
        url: "/ManageProject/DeleteProjectDocument/",
        data: { DocID: id },
        success: function (userdetail) {
            $('#editDocument').modal('hide');
            var table = $('#Documenttable').DataTable();
            table.ajax.reload();
        }
    })
}
function BindDocType() {


    $.ajax({
        url: '/Common/BindDocumentType',
        type: 'GET',
        async: false,
        success: function successdoctype(data) {
            var obj = data;

            var options = null;
            options += '<option name = "doctypeoption" value="">--Select--</option>';
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].DocumentTypeName == 'Other') {
                    continue;
                }
                else {
                    if (obj[i].DocumentTypeName == 'Drawing') {
                        options += '<option disabled name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
                    }
                    else {
                        options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
                    }

                }

            }

            $("select#adddoctype").html(options);
            $("select#editdoctype").html(options);


        },
        error: function errordoctype(data) {

            return false;

        }
    });
}
function editDocument(id) {
    $('#edithdJobNo').val("");
    $.ajax({
        type: "GET",
        url: "/ManageProject/GetProjectDocumentByID/",
        data: { docId: id },
        success: function (documentdetails) {

            $('#editDocumentName').val("");
            $('#editDocumentDesc').val("");
            $('#editdoctype').val($("#editdoctype option:first").val());
            $('#editCompanyName').val("");
            $('#editProjectName').val("");
            $('#editJobName').val("");
            $('#editdpExpDate').val("");
            $('#editIsExpiryDate').prop('checked', false);
            $('#editIsPublished').prop('checked', false);
            $('#editattchFilename').empty();

            Dropzone.forElement("#editdropzoneForm").removeAllFiles(true);

            var FileName = documentdetails.DocumentPath;
            var myString = FileName.substr(FileName.indexOf("_") + 1);


            var Expdate = documentdetails.ExpiredDate;

            $('#editDocumentid').val(documentdetails.DocumentID);
            $('#editDocumentName').val(documentdetails.DocumentName);
            $('#editdoctype').val(documentdetails.DocumentTypeID);
            $('#editDocumentDesc').val(documentdetails.Description);
            $('#editCompanyName').val(documentdetails.CompanyName);
            $('#editProjectName').val(documentdetails.ProjectName);
            $('#editJobName').val(documentdetails.JobName);

            if (FileName != null && FileName != "" && FileName != undefined) {
                $("<span class='forPadding'><a href='javascript:void(0)' name='attachment' class='removeFile'>" + myString + "</a><a href='javascript:void(0)' name='attachmentRemove'><b> Remove</b></a></span>").appendTo("#editattchFilename");
            }


            if (Expdate != null && Expdate != "" && Expdate != undefined) {
                $('#diveditdpExpDate').show();
                $('#editdpExpDate').val(Expdate);
                $('#editIsExpiryDate').prop('checked', true);
            }
            else {
                $('#diveditdpExpDate').hide();
                $('#editdpExpDate').val("");
                $('#editIsExpiryDate').prop('checked', false);
            }


            if (documentdetails.IsPublished == true) {
                $('#editIsPublished').prop('checked', true);

            }
            else {
                $('#editIsPublished').prop('checked', false);

            }

/*
            var SelectedDocTypeId = '';
            $("#editdoctype > option").each(function () {
                if (SelectedDocType == this.text) {
                    SelectedDocTypeId = this.value;
                }
            });
            if (SelectedDocType == 'Drawing') {
                $('#editdoctype option[value="' + SelectedDocTypeId + '"]').prop('disabled', false);
                $('#editdoctype').prop('disabled', true);
            }
*/

            var SelectedDocTypeId = documentdetails.DocumentTypeID;
            if (SelectedDocTypeId == "2")
            {
                SelectedDocType = "Drawing";
            }
            else
            {
                SelectedDocType = "Documents";
            }

            //SelectedDocType = ComeFrom;

            BindDocType();

            $('#editdoctype option[value="' + SelectedDocTypeId + '"]').attr("selected", "selected");
            if (SelectedDocType == 'Drawing') {
                $("#editdoctype > option").each(function () {
                    if (SelectedDocType != this.text) {
                        $(this).remove();
                    }
                });
                $('#editdoctype option[value="' + SelectedDocTypeId + '"]').prop('disabled', false);
                $('#editdoctype').prop('disabled', false);
            }
            else if (SelectedDocType != 'Drawing') {
                $("#editdoctype > option").each(function () {
                    if ('Drawing' == this.text) {
                        $(this).prop('disabled', false);
                        $(this).remove();
                    }

                });
            }


        }
    })
}

function GoToBankContacts(JobNo) {
    window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + $('#hdnSelectedCompanyId').val() + '/Documents'
}

function GoToBankDrawing(JobNo) {
    window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + $('#hdnSelectedCompanyId').val() + '/Drawing'
}

function GoToBankMessage(JobNo) {
    window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + $('#hdnSelectedCompanyId').val() + '/Messages'
}

function GoToBankMilestone(JobNo) {
    window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + $('#hdnSelectedCompanyId').val() + '/Milestone'
}

$("#Documenttable").on('click', 'a[name=DownloadDocumentName]', function () {
    var Fullpath = $(this).attr('fullpath');
    window.location.href = '/DocumentByType/DownloadDocument?DocPath=' + escape(Fullpath);

});

$(document).on('click', 'a[name=attachmentRemove]', function () {
    $(".forPadding").remove();
});
//======================================//
$('#DocumentDatatableSearch').unbind().keyup(function () {
    var value = $(this).val();
    if (value.length > 1) {

        ViewProjectDocumentList($('#hdnProjectNo').val(), SelectedDocType, $('#hdnSelectedCompanyId').val(), value);
    }
    if (value == '') {
        ViewProjectDocumentList($('#hdnProjectNo').val(), SelectedDocType, $('#hdnSelectedCompanyId').val(), value);
    }
    $('#Documenttable_filter').hide(); // To remove autogenerated server textbox
    $('#Documenttable').on('draw.dt', function () { // To set design again
        ProjectDocumentListDatatablePaging();
        ProjectMasterViewTooltip();
    });
    $('.dataTables_length').hide(); // To hide page size selection
});

// Related to permission

// Related to permission

/* Code for Document type*/

$('#DisplayMessageCommunication').click(function (event) {
    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');

    $.getScript("/js/Custom/ProjectCommunication.js", function (data, textStatus, jqxhr) {

    });

    $("#_ProjectCommunicationView").show();

    $('#DisplayProjectEditMode').hide();
    $('#_ProjectContactView').hide();
    $("#_ProjectJobView").hide();
    $("#_DocumentByTYpeList").hide();
    $("#_ProjectMilestoneList").hide();
});

$(document).on("change", "editstatustype", function (e) {

    viewReadyToPull(ProjectNo, '');

});


function viewReadyToPull(ProjectNo, JobNo) {
    $('#divPartial').load("/ManageProject/_ReadyToPullTemplate?ProjectNo=" + ProjectNo + "&JobNo=" + JobNo + "", function () {

    });
}

function AlertMessageProjectManagement() {

    var module = 'Project Management';

    $.ajax({
        type: "POST",
        url: "/ManageProject/AlertForProjectManagement/",
        data: { moduleName: module },
        success: function (data) {
            ProjectManagementMessageList = data;
        }
    })



}

function ProjectMasterViewTooltip() {
    //$("#btnAddnewUser").tooltip({ title: 'Add New', placement: 'top' });
    //if ($('#hdnCheckUsermanagement').val() == 'User Management') {
    //    $(".e-edit").tooltip({ title: 'Edit User', placement: 'top' });
    //}
    //else {
    //    $(".e-edit").tooltip({ title: 'Edit Employee', placement: 'top' });
    //}
    $(".e-communicate").tooltip({ title: 'Communication', placement: 'top' });
    $(".e-sammery").tooltip({ title: 'View', placement: 'top' });
    $(".e-contact").tooltip({ title: 'Contacts', placement: 'top' });
    $(".e-drawing").tooltip({ title: 'Drawings', placement: 'top' });
    $(".e-milestone").tooltip({ title: 'Milestones', placement: 'top' });
    $(".e-contract").tooltip({ title: 'Documents', placement: 'top' });

    $(".e-edit").tooltip({ title: 'Edit', placement: 'top' });
    $(".e-download").tooltip({ title: 'Download', placement: 'top' });


}

$(document).on("click", ".table-click-ProjectJobList", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className;
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];
            var JobNo = $(clickedRow).find('td:not(:empty):first').text();
            GoToBankSummary(JobNo);
        }

    }
});

$(document).on("click", ".table-click-ProjectDocumentList", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];
            //var JobNo = $(clickedRow).find('td:not(:empty):first').text();
            var DocId = $(clickedRow).find('td:last').children('.e-edit')[0].id;
            $('#editDocument').modal('show');
            editDocument(DocId);
        }

    }
});

function NumberToCurrencyFormat(value) {
    return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
}
function CheckDocName(checkdata) {
    var flag = true;
    //if (checkdata.DocumentName != "" && checkdata.DocumentTypeID != "") {
    if (checkdata.DocumentName != "") {
        $.ajax({
            url: '/ManageProject/CheckDocumentName',
            type: 'POST',
            data: checkdata,
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data == true) {
                    if (checkdata.DocumentID == 0) {
                        $('#addDocumentName').css({ "border": "1px solid red" });
                        $('#CommonErrorMessage').html("Document Name Is Already Exist.");
                        $('#CommonErrorModal').modal('show');

                        $('#addDocumentName').val("");
                        $('#addDocumentName').focus();
                        // $('#addDocumentName').addClass('input-error');
                        $(".dz-hidden-input").prop("disabled", true);
                        // return false
                        flag = false;
                    }
                    else {
                        $('#editDocumentName').css({ "border": "1px solid red" });
                        $('#CommonErrorMessage').html("Document Name Is Already Exist.");
                        $('#CommonErrorModal').modal('show');

                        $('#editDocumentName').val("");
                        $('#editDocumentName').focus();
                        //$('#editDocumentName').addClass('input-error');
                        $(".dz-hidden-input").prop("disabled", true);
                        // return false
                        flag = false;
                    }

                }
                else {
                    $('#addDocumentName').removeClass('input-error');
                    $('#editDocumentName').removeClass('input-error');
                    $('#addDocumentName').css({ "border": "" });
                    $('#editDocumentName').css({ "border": "" });
                    $(".dz-hidden-input").prop("disabled", false);
                }
            },
            error: function (data) { }
        });
    }
    return flag;
}
$(document).on("click", "#btnResetProject", function (e) {

    var hdnProjectStatusForReset = $('#hdnProjectStatusForReset').val(); // get old value from hidden field for reset button
    $('#editStatus').val(hdnProjectStatusForReset);

});
