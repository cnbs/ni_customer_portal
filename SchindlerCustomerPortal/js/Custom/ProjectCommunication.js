﻿var ToList = [];
var CommunicationProjectNo = '';
var CommunicationJobNo = '';

function split(val) {
    return val.split(/,\s*/);
}

function commonJqueryValueSubString(input, length) {

    if (input.length <= length) {
        input = input
    }
    else {
        var startName = input.substring(0, 7) + '...';
        var inputLength = input.length - 8;
        var endName = input.substring(inputLength, input.length);
        //input = input.substring(0, length) + '...'
        input = startName + endName;
    }

    return input
}

function GetAllCommunication(projectNo, jobNo) {

    $('#ProjectCommunicationMessage').code('');
       
    $.ajax({
        url: '/ManageProject/GetAllCommunicationList',
        method: 'POST',
        async: false,
        data: { ProjectNo: projectNo, JobNo: jobNo },
        success: function (data) {
            
            $('#ProjectCommunicationEmail').val('');
            ToList = [];
            $('#previews').html('');
            $('#lastMessage').html('');
            $('#allCommunicationList').html('');

            if (data == null || data == '') {
                var NoMessagerenderHTML = '';
                NoMessagerenderHTML += '<h3> There is no communication made on the project till now. Be the first one to post the message.</h3>'
                $('#lastMessage').append(NoMessagerenderHTML);
            }

            if (data.length > 0) {
                var lastMessagerenderHTML = '';
                if (data[0].AttachmentPath == "" || data[0].AttachmentPath == null) {
                    lastMessagerenderHTML += '<h3> Last message posted by : ' + data[0].UserName + ' <small class="pull-right text-muted"><span class="fa fa-clock-o"></span> ' + data[0].CreatedOn + '</small></h3>'
                }
                else {
                    var filenamePath = data[0].AttachmentPath.split('|');

                    //lastMessagerenderHTML += '<h3> Last message posted by : ' + data[0].UserName + ' <small class="pull-right text-muted">';
                    //lastMessagerenderHTML += '<span class="fa fa-clock-o"></span>' + data[0].CreatedOn + '</small><a type="button" href="javascript:void(0)" name="attachment" Fullpath="' + escape(filenamePath) + '" title="Download file - ' + filenamePath + '" class="attach pull-right"><i class="fa fa-paperclip"></i></a></h3>'

                    lastMessagerenderHTML += '<h3> Last message posted by : ' + data[0].UserName ;
                  
                    lastMessagerenderHTML += '<ul class="messagetools"><li><button type="button" class="mstinfo"><i class="fa fa-info"></i></button></li></ul>';
                    lastMessagerenderHTML += '<div class="attachbox"><div class="btn-group"> <a href="#" data-toggle="dropdown" class="btn mybtn dropdown-toggle" aria-expanded="false"><i class="glyphicon glyphicon-paperclip"></i>';
                    lastMessagerenderHTML += '<span class="caret"></span></a><ul class="dropdown-menu" role="menu">';
                    $.each(filenamePath, function (i, val) {
                        if (val != "") {
                            var fileNameLength = commonJqueryValueSubString(val, 14);
                            var getFileExtension = val.substr(val.lastIndexOf('.') + 1, val.length);
                            lastMessagerenderHTML += '<li><span class="label label-success ">' + getFileExtension.toUpperCase() + '</span>' + fileNameLength + ' <a href="javascript:void(0)" name="attachment" Fullpath="' + escape(val) + '" title="Download file - ' + val + '" class="btnsml_action e-download"></a></li>';
                        }
                    })
                    lastMessagerenderHTML += '</ul></div></div>';
                    lastMessagerenderHTML += '<small style="margin-right:20px;" class="pull-right text-muted"><span class="fa fa-clock-o"></span>' + data[0].CreatedOn + '</small>';
                }


                lastMessagerenderHTML += ' ' + data[0].Message + ''
                //lastMessagerenderHTML += '<p class="text-muted"><strong>Best Regards<br>' + data[0].UserName + '</strong></p>'
                $('#lastMessage').append(lastMessagerenderHTML);
            }

            $.each(data, function (i, val) {

                if (i != 0) {

                    var renderHTML = '';
                    renderHTML += '<div class="item item-visible"><div class="image"><img src="' + val.ProfilePic + '" alt="Dmitry Ivaniuk">'
                    renderHTML += '</div><div class="text"><div class="heading"><a href="javascript:void(0)">' + val.UserName + '</a>'
                    renderHTML += '<ul class="messagetools">'

                    renderHTML += '<li><button type="button" title="Collapse\Expand" class="mst"><i class="fa fa-minus-square-o"></i></button>'
                    renderHTML += '</li><li><button type="button" class="mstinfo"><i class="fa fa-info"></i></button></li></ul>'
                    if (val.AttachmentPath != "" && val.AttachmentPath != null) {
                        var filenamePath = val.AttachmentPath.split('|');
                        //renderHTML += '<li><button type="button" name="attachment" Fullpath="' + escape(filenamePath) + '" title="Download file - ' + filenamePath + '" class="pull-right"><i class="fa fa-paperclip"></i></button></li>'
                        renderHTML += '<div class="attachbox"><div class="btn-group"> <a href="#" data-toggle="dropdown" class="btn mybtn dropdown-toggle" aria-expanded="false"><i class="glyphicon glyphicon-paperclip"></i>';
                        renderHTML += '<span class="caret"></span></a><ul class="dropdown-menu" role="menu">';
                        $.each(filenamePath, function (i, val) {
                            if (val != "") {
                                var fileNameLength = commonJqueryValueSubString(val, 14);
                                var getFileExtension = val.substr(val.lastIndexOf('.') + 1, val.length);
                                renderHTML += '<li><span class="label label-success ">' + getFileExtension.toUpperCase() + '</span>' + fileNameLength + '<a href="javascript:void(0)" name="attachment" Fullpath="' + escape(val) + '" title="Download file - ' + val + '" class="btnsml_action e-download"></a></li>';
                            }
                        })
                        renderHTML += '</ul></div></div>';
                    }
                    renderHTML += '<span class="date">' + val.CreatedOn + '</span></div>'
                    renderHTML += '<div class="commentInfo grey2" style="display: block;"> Comment timestamp: <b>' + val.CreatedOn + '</b><br>'
                    renderHTML += 'Reply about this comment were sent to: <em>' + val.UserName + '</em> </div>'
                    renderHTML += '<div class="commentBody" style="display: block;">' + val.Message + '</div></div></div>'

                    $('#allCommunicationList').append(renderHTML);
                }
            })

            $(".commentInfo").css('display', 'none');
        },
        error: function (data) {
            //alert('error')
        }
    })


}

Dropzone.autoDiscover = false;

$(document)
    .ready(function () {

        $('.note-editable').click();

        var txtEmailoptions = {
            source: function (request, response) {
                //
                $.ajax({
                    url: "/MailBox/CompanyUserAutoComplete",
                    data: '{ searchTerm:"' + $('#ProjectCommunicationEmail').val() + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    type: 'Post',
                    success: function (data) {
                        //
                        response($.map(data,
                            function (item) {
                                return {
                                    label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                                    value: item.Email,
                                    UserID: item.UserID
                                }
                            }));
                    }
                })
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {

                // insert userId into array
                if (ToList.length == 0) {
                    ToList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
                } else {
                    ToList[ToList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
                }

                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(this).val("");
                    //alert('');
                    //$('#empty-message').show();
                } else {
                    //$('#empty-message').hide();
                }
            },
            minLength: 2,
        };

        $(document)
            .on("keydown.autocomplete",
                '#ProjectCommunicationEmail',
                function (event) {
                    $(this).autocomplete(txtEmailoptions);
                })

        $(document)
            .on('click',
                '#btnReplyCommunication',
                function (event) {
                    event.preventDefault();
                    //
                    
                    var isValid = true;

                    var toCommaValue = '';

                    var toValue = $('#ProjectCommunicationEmail').val();
                    if (toValue != '') {
                        var newToList = [];
                        $.each(toValue.split(','),
                            function (key, val) {
                                newToList[newToList.length] = $.grep(ToList,
                                    function (e) {

                                        if (val.trim() == e.Email) {
                                            toCommaValue = toCommaValue == '' ? e.Email : toCommaValue + ',' + e.Email;
                                            return e;
                                        }
                                    });
                            })
                    }

                    var formData = new FormData();
                    var message = $('#ProjectCommunicationMessage').code();
                    var ProjectName = $('#hdnProjectName').val();
                    var JobName = $('#hdnJobName').val();

                    var clearText = $("#ProjectCommunicationMessage").code().replace(/<\/?[^>]+(>|$)/g, "");

                    if (message == '' || message == null || clearText == "" || clearText == null) {
                        isValid = false;
                        //alert('Please enter message');


                        //$('#CommonErrorMessage').html('Please enter message');
                        //$('#CommonErrorModal').modal('show');
                        $(".summernote_lite").focus();
                        $('.note-editable').css('border', 'solid 1px red');

                        return false;
                    }

                    CommunicationProjectNo = $('#hdnProjectNo').val();

                    //ormData.append("Subject", subjectValue);
                    formData.append("Message", message);
                    formData.append("NotifyToEmail", toCommaValue);
                    formData.append("ProjectNo", CommunicationProjectNo);
                    formData.append("JobNo", $('#hdnJobNo').val());
                    formData.append("CompanyId", $('#hdnSelectedCompanyId').val());
                    formData.append("ProjectName", $('#hdnProjectName').val());
                    formData.append("JobName", $('#hdnJobName').val());
                    
                    $('#ProjectCommunicationMessage').css('display', 'none');


                    //
                    var fileNameList = '';
                    var attachmentValid = false;

                    if (myDropzone.getQueuedFiles().length > 0) {
                        event.preventDefault();
                        myDropzone.processQueue();

                        myDropzone.on("successmultiple",
                            function (file, responsenew) {
                                this.removeAllFiles(true);

                                //alert(responsenew);
                                if (fileNameList == null) {
                                    fileNameList = responsenew;
                                } else {
                                    fileNameList += "|" + responsenew;
                                }
                                attachmentValid = true;

                                formData.append("AttachmentPath", fileNameList);

                                if (isValid) {
                                    $.ajax({
                                        url: '/ManageProject/AddProjectCommunication',
                                        method: 'POST',
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (data) {
                                            //alert('success')
                                            $('#CommonSuccessMessage').html('Message sent successfully');
                                            $('#CommonSuccessModal').modal('show');
                                            GetAllCommunication(CommunicationProjectNo, $('#hdnJobNo').val())
                                        },
                                        error: function (data) {
                                            alert('error')
                                        }
                                    })
                                } else {
                                    return false;
                                }

                            });

                    } else {
                        //

                        //isValid = false;

                        formData.append("AttachmentPath", fileNameList);

                        if (isValid) {
                            $.ajax({
                                url: '/ManageProject/AddProjectCommunication',
                                method: 'POST',
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    //alert('success')
                                    GetAllCommunication(CommunicationProjectNo, $('#hdnJobNo').val())
                                },
                                error: function (data) {
                                    alert('error')
                                }
                            })
                        } else {
                            return false;
                        }
                    }

                });

        CommunicationProjectNo = $('#hdnProjectNo').val();

        GetAllCommunication(CommunicationProjectNo, $('#hdnJobNo').val());

        $(document)
            .on('click',
                'button.mst',
                function () {
                    $(this).find('i').toggleClass('fa-minus-square-o').toggleClass('fa-plus-square-o');
                    //
                    //$(this).parentsUntil('div.heading').siblings('.commentBody').slideToggle('fast');

                    $(this).parents().parents('.text').find('.commentBody').slideToggle('fast');
                    //$(this).parents().find('.commentBody').slideToggle('fast');
                });

        $(document)
            .on('click',
                'button.mstinfo',
                function () {
                    //$(this).find('i').toggleClass('fa-minus-square-o').toggleClass('fa-plus-square-o');
                    //
                    //$(this).parentsUntil('div.heading').siblings('.commentBody').slideToggle('fast');
                    $(this).parents().parents('.text').find('.commentInfo').slideToggle('fast')
                    //$(this).parents().find('.commentBody').slideToggle('fast');
                });

        $(document)
            .on('click',
                '#btnCancelCommunication',
                function (event) {
                    $("#ProjectCommunicationEmail").val("");
                    ToList = [];
                    $('#ProjectCommunicationMessage').code('');
                })

        $(document)
            .on('click',
                'a[name=attachment]',
                function () {
             
                    var Fullpath = $(this).attr('Fullpath');
                    window.location.href = '/MailBox/DownloadAttachement?attachmentPath=' + escape(Fullpath);
                });

        $(document)
            .on('click',
                'button[name=attachment]',
                function () {
                    var Fullpath = $(this).attr('Fullpath');
                    window.location.href = '/MailBox/DownloadAttachement?attachmentPath=' + escape(Fullpath);
                });

        $(document)
           .on('keyup',
               '.note-editable',
               function () {

                   $('.note-editable').css('border', 'solid 1px transparent');
               });

    });
