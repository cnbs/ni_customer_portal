﻿function EditProjectContact(id) {

    $('#editProjectContactForm').show();

    $.ajax({
        type: "GET",
        url: "/ManageProject/GetProjectContactByID/",
        data: { contactID: id },
        success: function (userdetail) {
            
            $('#editContactHeaderFirstName').text('Edit Contact');

            $('#editContactID').val(userdetail.user.ContactID);
            $('#editContactFirstname').val(userdetail.user.FirstName);

            $("#editContactProjectName").val(userdetail.user.ProjectName)
            $("#editContactJobName").val(userdetail.user.JobName);

            $("#editContactPhone").val(userdetail.user.ContactNumber)
            $('#editContactEmail').val(userdetail.user.Email);
            
            $('#editContactDesignation').html("");
            $.each(userdetail.designation, function (i, val) {
                $('#editContactDesignation').append($("<option/>", ($({ value: val.DesignationId, text: val.Designation }))));
            });
            
            $('#editContactDesignation').val(userdetail.user.DesignationId);

            $('html, body').animate({
                scrollTop: $("#editProjectContactForm").offset().top
            }, 500);

        }
    })
}

function DeleteProjectContact(id) {
    $.ajax({
        type: "POST",
        url: "/ManageProject/DeleteProjectContact/",
        data: { contactID: id },
        success: function (userdetail) {
            var table = $('#ProjectContactDatatable').DataTable();
            table.ajax.reload();
        }
    })
}

function ValidateAddProjectContact() {
    var inValid = false;
    var addContactFirstname = $('#addContactFirstname').val().trim();
    var addContactJobName = $('#addContactJobName').val().trim();
    var addContactDesignation = $('#addContactDesignation').val().trim();
    var addContactPhone = $('#addContactPhone').val().trim();
    var addContactEmail = $('#addContactEmail').val().trim();
    

    if (addContactFirstname == "") {
        inValid = true;
        $('#addContactFirstname').css('border-color', 'red');
    }

    
    if (addContactDesignation == "") {
        inValid = true;
        $('#addContactDesignation').css('border-color', 'red');
    }

    if (addContactPhone == "") {
        inValid = true;
        $('#addContactPhone').css('border-color', 'red');
    }

    if (addContactEmail == "") {
        inValid = true;
        $('#addContactEmail').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }
}

function ValidateEditProjectContact() {
    var inValid = false;
    var editContactFirstname = $('#editContactFirstname').val().trim();
    var editContactJobName = $('#editContactJobName').val().trim();
    var editContactDesignation = $('#editContactDesignation').val().trim();
    var editContactPhone = $('#editContactPhone').val().trim();
    

    if (editContactFirstname == "") {
        inValid = true;
        $('#editContactFirstname').css('border-color', 'red');
    }

   
    if (editContactDesignation == "") {
        inValid = true;
        $('#editContactDesignation').css('border-color', 'red');
    }

    if (editContactPhone == "") {
        inValid = true;
        $('#editContactPhone').css('border-color', 'red');
    }

    
    if (inValid == true)
        return false;
    else {
        return true;
    }
}

function DesignationAndState() {

    $.ajax({
        type: "GET",
        url: "/ManageProject/GetDesignationandState",
        dataType: "json",
        //data: { countryId: countryId },
        success: function (response) {
            
            $('#addContactDesignation').html("");
            $('#addContactDesignation').append($("<option/>", ($({ value: '', text: '-- Select --' }))));
            $.each(response.designation, function (i, val) {
                $('#addContactDesignation').append($("<option/>", ($({ value: val.DesignationId, text: val.Designation }))));
            });

            
        },
        failure: function (response) {
            bootbox.alert('oops something went wrong');
        }
    });
}

function ProjectContactDatatablePaging() {
    $('#ProjectContactDatatable_paginate').children('ul').addClass('flatpagi');
    $('#ProjectContactDatatable_previous').children('a').remove();
    $('#ProjectContactDatatable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#ProjectContactDatatable_next').children('a').remove();
    $('#ProjectContactDatatable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function ProjectEditFormCancel()
{
    $('#editContactFirstname').attr('style', '');
    $('#editContactDesignation').attr('style', '');
    $("#editContactPhone").attr('style', '');
    $("#editContactJobName").attr('style', '');

    $('#editProjectContactForm').hide();
}

function UserMessageSend(mail) {
    window.location.href = '/mailbox/maillist?to=' + mail;
}

function ProjectContactTooltip() {

    $(".e-communicate").tooltip({ title: 'Communication', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit', placement: 'top' });

}

$(document).ready(function () {

    $('#addContactPhone').mask('000-000-0000');
    $('#editContactPhone').mask('000-000-0000');

    //var hdnProjectId = 1;
    var hdnPageSize = parseInt($('#hdnPageSize').val());
    var ContactProjectNo = $('#hdnProjectNo').val();
    $('#editProjectContactForm').hide();

    var tableDestryed = $('#ProjectContactDatatable').DataTable();
    tableDestryed.destroy();

    var table = $('#ProjectContactDatatable').DataTable({
        "ajax": {
            "url": "/ManageProject/GetAllContactByProjectID",
            "type": "POST",
            "datatype": "JSON",
            'data': {
                ProjectNo: ContactProjectNo,
            },
        },
        "autoWidth": false, // for disabling autowidth
        //"columnDefs": [{ 'bSortable': false, 'targets': [0, 2] }],
        'aaSorting': [[1, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "bInfo": false,
        //"iTotalRecords": "3",
        //"iTotalDisplayRecords": "3",

        "columns": [
            { "data": "ProjectNo", "name": "ProjectNo", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "FirstName",
            "name": "FirstName",
            "Searchable": true, "sClass": "txtAlignCenter"
        },
        { "data": "BankDesc", "name": "BankDesc", "Searchable": true }, // job name
        { "data": "Designation", "name": "Designation", "Searchable": true, "sClass": "txtAlignCenter" }, // role
        { "data": "ContactNumber", "name": "PhoneNo", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Email", "name": "Email", "Searchable": true,
            "render": function (data, type, row) {
                        return '<a href="mailto:' + row.Email + '?subject=MySchindlerProjects" onclick="UserMessageSend(\'' + row.Email + '\')">' + data + '</a>'
                        }
        }, // email
        {
            "data": "ContactID", "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                return '<a class="btnsml_action e-edit" href="javascript:void(0)" id="' + data + '" onclick="EditProjectContact(' + data + ')"> </a>'//<a href="javascript:void(0)" class="btnsml_action e-communicate" onclick="UserMessageSend(\'' + row.Email + '\')"> </a>'
            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            ProjectContactDatatablePaging();
            ProjectContactTooltip();
        },
        "drawCallback": function (settings) {
            
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }
    });

    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {
            
            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#EditProjectContactModal').modal('show');
                EditProjectContact(parseInt(userID));
            }

        }
    });

    var ContactProjectJobNo = '';

    var txtJobNameoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/ManageProject/JobNameAutoCompleteByProjectNo/",
                data: '{ searchTerm:"' + request.term + '" , ProjectNo:"' + ContactProjectNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {
                            label: item.JobName,
                            value: item.JobName,
                            JobNo: item.JobNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addContactJobName").val(ui.item.label);
            ContactProjectJobNo = ui.item.JobNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                
            } else {
                
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#addContactJobName', function (event) {
        $(this).autocomplete(txtJobNameoptions);
    })

    $('#ProjectContactDatatable').on('draw.dt', function () {
        ProjectContactDatatablePaging();
    });

    $('.dataTables_length').hide();

    $('#ProjectContactDatatable_filter').hide();

    $('#ProjectContactDatatableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 2) {
           
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    $('#addContactFirstname,#addContactDesignation,#addContactPhone,#addContactEmail').on('blur', function () {
        ValidateElement($(this));
    });

    $('#btnAddProjectContact').click(function (event) {
        event.preventDefault();
        
        $('#btnAddProjectContact').attr('disabled', 'disabled');

        if (!ValidateAddProjectContact()) {
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }

        var isValid = true;

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var addContactEmail = $("#addContactEmail").val();
        var echeck = "";
        if (typeof addContactEmail != 'undefined' && addContactEmail != null && addContactEmail.trim() != "") {
            if (!addContactEmail.match(re)) {
                $("#addContactEmail").css({
                    "border": "1px solid red"
                });
                $("#addContactEmail").focus();
                echeck = "a";
                $('#btnAddProjectContact').removeAttr('disabled');
            }
        }
        if (echeck == "a") {
            return false;
        }

        var phonelen = $('#addContactPhone').val().length;
        if (phonelen < 12) {
            $('#addContactPhone').css({
                "border": "1px solid red"
            });
            $('#addContactPhone').focus();
            var isValid = false;
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }

        //validation for Firstname allow only alphabets
        var contactfirstname = $('#addContactFirstname').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof contactfirstname != 'undefined' && contactfirstname != null && contactfirstname.trim() != "") {
            if (!contactfirstname.match(re)) {
                $("#addContactFirstname").css({
                    "border": "1px solid red"
                });
                $("#addContactFirstname").focus();
                fcheck = "a";
                $('#btnAddProjectContact').removeAttr('disabled');
            }
        }
        if (fcheck == "a") {
            return false;
        }


        var formData = new FormData();

        var contact = $("#addContactPhone").val().replace('-', '').replace('-', '');

        formData.append("FirstName", $('#addContactFirstname').val().trim());
        formData.append("ProjectNo", ContactProjectNo);
        formData.append("JobNo", ContactProjectJobNo);
       
        formData.append("Designation", $('#addContactDesignation option:selected').val().trim());
        formData.append("ContactNumber", contact);
        formData.append("Email", $('#addContactEmail').val().trim());
       
        if (isValid) {
            $.ajax({
                url: '/ManageProject/AddProjectContact',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
              
                    $('#btnAddProjectContact').removeAttr('disabled');

                    $('#AddProjectContactModal').modal('hide');

                    $('#CommonSuccessMessage').html('Record added successfully');
                    $('#CommonSuccessModal').modal('show');

                    var table = $('#ProjectContactDatatable').DataTable();
                    table.ajax.reload();
                },
                error: function (data) {
                    //Handle Error
                }
            })
        }
        else {
            $('#btnAddProjectContact').removeAttr('disabled');
            return false;
        }


    })

    var txtJobNameEditoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/ManageProject/JobNameAutoCompleteByProjectNo/",
                data: '{ searchTerm:"' + request.term + '" , ProjectNo:"' + ContactProjectNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {
                            label: item.JobName,
                            value: item.JobName,
                            JobNo: item.JobNo
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editContactJobName").val(ui.item.label);
            ContactProjectJobNo = ui.item.JobNo
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
                
            } else {
               
            }
        },
        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#editContactJobName', function (event) {
        $(this).autocomplete(txtJobNameEditoptions);
    })

    $('#btnUpdateProjectContact').click(function (event) {
        
        event.preventDefault();

        if (!ValidateEditProjectContact()) {
            return false;
        }

        var isValid = true;

        var phonelen = $('#editContactPhone').val().length;
        if (phonelen < 12) {
            $('#editContactPhone').css({
                "border": "1px solid red"
            });
            $('#editContactPhone').focus();
            var isValid = false;
            return false;
        }
        //validation for Firstname allow only alphabets
        var contactfirstname = $('#editContactFirstname').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof contactfirstname != 'undefined' && contactfirstname != null && contactfirstname.trim() != "") {
            if (!contactfirstname.match(re)) {
                $("#editContactFirstname").css({
                    "border": "1px solid red"
                });
                $("#editContactFirstname").focus();
                fcheck = "a";

            }
        }
        if (fcheck == "a") {
            return false;
        }

        var formData = new FormData();

        var contact = $("#editContactPhone").val().replace('-', '').replace('-', '');
        
        formData.append("ContactID", $('#editContactID').val().trim());
        formData.append("FirstName", $('#editContactFirstname').val().trim());
        
        formData.append("Designation", $('#editContactDesignation option:selected').val().trim());
        formData.append("ContactNumber", contact);
        formData.append("JobNo", ContactProjectJobNo);
        formData.append("JobName", $('#editContactJobName').val());
        if (isValid)
        {
            $.ajax({
                url: '/ManageProject/UpdateProjectContact',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    

                    $('#CommonSuccessMessage').html('Record updated successfully');
                    $('#CommonSuccessModal').modal('show');

                    $('#editProjectContactForm').hide();
              
                    $('#editContactFirstname').attr('style','');
                    $('#editContactDesignation').removeAttr('style');
                    $("#editContactPhone").removeAttr('style');
                    $("#editContactJobName").removeAttr('style');

                    var table = $('#ProjectContactDatatable').DataTable();
                    table.ajax.reload();
                },
                error: function (data) {
                    //Handle Error
                }
            })
        }
        else {
            return false;
        }

    });

    $('#btnDeleteProjectContact').click(function () {
        
        $('#contactDeleteConfirmModel').modal('show');

        //var id = $('#editContactID').val();
        //DeleteProjectContact(id);
        //$('#editProjectContactForm').hide();

        //$('#editContactFirstname').attr('style', '');
        //$('#editContactDesignation').attr('style', '');
        //$("#editContactPhone").attr('style', '');
        //$("#editContactJobName").attr('style', '');
    })

    $('#btnContactDeleteYes').click(function () {
        var id = $('#editContactID').val();
        DeleteProjectContact(id);
        $('#editProjectContactForm').hide();
        $('#contactDeleteConfirmModel').modal('hide');
        $('#editContactFirstname').attr('style', '');
        $('#editContactDesignation').attr('style', '');
        $("#editContactPhone").attr('style', '');
        $("#editContactJobName").attr('style', '');
    })

    $("#addContactState").on('change', function () {

        var stateid = $('#addContactState').val();
    
        $.ajax({
            type: "GET",
            url: "/ManageProject/GetCityList",
            dataType: "json",
            data: { stateCode: stateid },
            success: function (city) {
                $('#addContactCity').html("");
                $('#addContactCity').append($("<option/>", ($({ value: '', text: '--Select--' }))));
                $.each(city, function (i, val) {
                    $('#addContactCity').append($("<option/>", ($({ value: val.CityName, text: val.CityName }))));
                });
            },
            failure: function (citylist) {
                bootbox.alert('oops something went wrong');
            }
        });
    });

    $("#editContactState").on('change', function () {

        var stateid = $('#editContactState').val();
       
        $.ajax({
            type: "GET",
            url: "/ManageProject/GetCityList",
            dataType: "json",
            data: { stateCode: stateid },
            success: function (city) {
                $('#editContactCity').html("");
                $('#editContactCity').append($("<option/>", ($({ value: '', text: '--Select--' }))));
                $.each(city, function (i, val) {
                    $('#editContactCity').append($("<option/>", ($({ value: val.CityName, text: val.CityName }))));
                });
            },
            failure: function (citylist) {
                bootbox.alert('oops something went wrong');
            }
        });
    });

    var contactIdList = [];

    $('#chkAllContact').change(function () {
        if ($(this).is(":checked")) {
            $('input[name="chkContact"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var contactID = result[1];
                contactIdList.push(contactID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkContact"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var contactID = result[1];

                contactIdList = jQuery.grep(contactIdList, function (value) {
                    return value != contactID;
                });
            });
        }
    });

    $('#btnAllContactDelete').click(function (event) {
        event.preventDefault();
        

        var IsValiddd = true;
        if (contactIdList.length == 0) {
            IsValiddd = false;
        }

        var result = contactIdList.join(", ")

        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageProject/DeleteProjectContact",
                data: { contactID: result },
                success: function (userdetail) {
                    $('#chkAllContact').prop('checked', false);
                    var table = $('#ProjectContactDatatable').DataTable();
                    table.ajax.reload();
                }
            })
        }


    })

    $("#ProjectContactDatatable_paginate").on("click", "a", function () {
        $('#chkAllContact').prop('checked', false);
        $('input[name="chkContact"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var ContactID = result[1];

            contactIdList = jQuery.grep(contactIdList, function (value) {
                return value != ContactID;
            });
        });
    });

    $(document).on('change', 'input[name^="chkContact"]', function (event) {
        
        if ($(this).is(":checked")) {
            
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var ContactID = result[1];
            contactIdList.push(ContactID);
            this.checked = true;
            
        }
        else {
            
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var ContactID = result[1];

            contactIdList = jQuery.grep(contactIdList, function (value) {
                return value != ContactID;
            });
            
        }
    })

    $('#addContactFirstname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });

    $('#editContactFirstname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
})

