﻿var structure = 1;
var IsDeleted = 0;

function clearFileInputField(tagId1, tagId2) {
    
    $('#' + tagId1).attr('src', '/img/no_images.png');
    $('#' + tagId2).val('');
    IsDeleted = 1;

}

function Setvideourl(vurl) {
    var newurl = $('#VideoFrame').prop('src', vurl);

    window.open(vurl, '_new');

    return false;
}

function convertjsonOnlydate(Anydate) {
    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}

function editResource(id) {

    $('#editDocument').modal('show');

    $.ajax({
        type: "GET",
        url: "/ResourceUpload/EditResourceUpload/",
        data: { resId: id },
        success: function (Resourcedetails) {

            $('#editResourceName').val("");
            $('#editProductName').val("");
            $('#editdoctype').val($("#editdoctype option:first").val());
            $('#EditIsPublished').prop('checked', false);
            $('#EditUrlName').val("");
            $('#attchFilename').empty();
            $('#editdropzoneForm').css({
                "border": "",
                "background": ""
            });
            $('#EditUrlName').css({
                "border": "",
                "background": ""
            });

            Dropzone.forElement("#editdropzoneForm").removeAllFiles(true);

            var FileName = Resourcedetails.DocumentPath;
            var myString = FileName.substr(FileName.indexOf("_") + 1);
            var fileType = myString.substr(myString.lastIndexOf(".") + 1);

            $('#editResourceid').val(Resourcedetails.ResourceId);
            $('#editResourceName').val(Resourcedetails.ResourceName);
            $('#editProductName').val(Resourcedetails.ProductName);
            $('#editdoctype').val(Resourcedetails.DocumentTypeId);
            //added by vivek 
            if (Resourcedetails.TitleImage == "/images/ProfilePics/") {
                if (fileType == "jpeg" || fileType == "jpe" || fileType == "jpg") {
                    $('#editUserpic').attr('src', '/images/gallery/jpeg.png');
                }
                else if (fileType == "png") {
                    $('#editUserpic').attr('src', '/images/gallery/png.png');
                }
                else if (fileType == "gif") {
                    $('#editUserpic').attr('src', '/images/gallery/gif.png');
                }
                else if (fileType == "pdf") {
                    $('#editUserpic').attr('src', '/images/gallery/pdf.png');
                }
                else if (fileType == "doc" || fileType == "docx") {
                    $('#editUserpic').attr('src', '/images/gallery/word.png');
                }
                else if (fileType == "ppt" || fileType == "pptx") {
                    $('#editUserpic').attr('src', '/images/gallery/ppt.png');
                }
                else if (fileType == "xls" || fileType == "xlsx") {
                    $('#editUserpic').attr('src', '/images/gallery/xls.png');
                }
                else if (fileType == "txt") {
                    $('#editUserpic').attr('src', '/images/gallery/txt.png');
                }
                else if (fileType == "html") {
                    $('#editUserpic').attr('src', '/images/gallery/html.png');
                }
                else if (fileType == "acd") {
                    $('#editUserpic').attr('src', '/images/gallery/acd.png');
                }
                else if (fileType == "bim") {
                    $('#editUserpic').attr('src', '/images/gallery/bim.png');
                }
                else if (fileType == "cad") {
                    $('#editUserpic').attr('src', '/images/gallery/cad.png');
                }
                else if (fileType == "pps") {
                    $('#editUserpic').attr('src', '/images/gallery/pps.png');
                }
                else if (fileType == "rfa") {
                    $('#editUserpic').attr('src', '/images/gallery/rfa.png');
                }
                else if (fileType == "odt") {
                    $('#editUserpic').attr('src', '/images/gallery/odt.png');
                }
                else {
                    $('#editUserpic').attr('src', '/images/gallery/no-image.png');
                }
            }
            else {
                $('#editUserpic').attr('src', Resourcedetails.TitleImage);
                     
              }

            $('#editpicFilename').val(Resourcedetails.TitleImage);

            if (Resourcedetails.URLName == null || Resourcedetails.URLName == "" || Resourcedetails.URLName == "undefined") {

            }
            else {
                $('#EditUrlName').val(Resourcedetails.URLName);
            }

            if (FileName != null && FileName != "" && FileName != undefined) {
                $("<span class='forPadding'><a href='javascript:void(0)' name='attachment' class='removeFile'>" + myString + "</a><a href='javascript:void(0)' name='attachmentRemove'><b> &nbsp;[Remove]</b></a></span>").appendTo("#attchFilename");
            }

            if (Resourcedetails.IsPublished == true) {
                $('#EditIsPublished').prop('checked', true);

            }
            else {
                $('#EditIsPublished').prop('checked', false);

            }

        }
    })
}

$(document).on('click', 'a[name=attachmentRemove]', function () {
    $(".forPadding").remove();
});

$('#btnDeleteDocument').click(function () {
    $('#documentDeleteConfirmModel').modal('show');
});

$(document).on('click', 'a[name=btnDeleteDocumentDirect]', function () {
    $('#documentDeleteConfirmModel').modal('show');
});

$('#btnDeleteYes').click(function () {
    var DocID = $('#editResourceid').val();
    DeleteResource(DocID);
})

function DeleteResource(id) {

    $.ajax({
        type: "POST",
        url: "/ResourceUpload/DeleteResource/",
        data: { DocID: id },
        success: function (userdetail) {
            $('#editDocument').modal('hide');
            //var table = $('#ResourceTable').DataTable();
            //table.ajax.reload();

            var searchValue = '';
            LoadResourceData(searchValue)
            if (structure == 1) {
                $('#grid').click();
            }
            else {
                $('#list').click();
            }

        }
    })
}

$(function () {
    $("#dpExpDate").datepicker();
});

function updateuserdocumentpermission(docid, userid, HasPermission, Type, rowid) {

    if ($('#' + rowid).is(':checked')) {
        HasPermission = true
    }
    else {
        HasPermission = false
    }

    $.ajax({
        type: "POST",
        url: "/ManageDocument/UpdateUserAccessPermission/",
        data: { DocID: docid, UserID: userid, HasPermission: HasPermission, Type: Type },
        success: function (response) {
            if (response.success) {

                if (HasPermission == 'true' || HasPermission == true) {
                    $('#' + rowid).addClass("accessallowed");
                }
                else {
                    $('#' + rowid).removeClass("accessallowed");
                }
            }
            else {
                alert(response.message);
            }
        },
        error: function (err) {
            alert(err.message);
        }
    })


}

function BindUserDocumentPermission(id) {

    var table = $('#dt_UserDocumentPermission').DataTable();

    table.destroy();

    $('#dt_UserDocumentPermission').DataTable({
        "paging": false,
        "info": false,
        "ajax": {
            "url": "/ManageDocument/UserDocumentPermission",
            "type": "Get",
            "datatype": "JSON",
            'data': {
                docID: id,
            },
        },
        "autoWidth": false, // for disabling autowidth
        "filter": true, // this is for disable filter (search box) 
        "oLanguage": { "sSearch": "" },
        "columns": [
            {
                "render": function (data, type, row) {

                    var viewResult = (row.HasViewPermission == 1) ? 'accessallowed' : '';
                    var uploadResult = (row.HasUploadPermission == 1) ? 'accessallowed' : '';
                    var deleteResult = (row.HasDeletePermission == 1) ? 'accessallowed' : '';

                    var rowViewPermission = (row.HasViewPermission == 1) ? false : true;
                    var rowChecked = (row.HasViewPermission == true) ? 'checked' : 'unchecked';

                    return '<div class="checkbox"><label><input type="checkbox" ' + rowChecked + '  value="" name="chkUserPermission" id="chkUserPermission_' + row.UserID + '_' + row.DocumentID + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowViewPermission + ',this.name,this.id)"><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> </label></div>'
                },
                "sClass": "txtLeftalign"

            },
        {
            "data": "UserName", "sClass": "txtLeftalign"

        },
        ]

    });

}

function ViewDocumentDetail(id) {

    $.ajax({
        type: "GET",
        url: "/ManageDocument/EditDocument/",//UserEdit
        data: { docId: id },
        success: function (documentdetails) {

            $('#DocumentName').html(documentdetails.DocumentName);
            $('#docdescription').html(documentdetails.Description);

            BindUserDocumentPermission(id);

        }
    })
}

function DocumentDatatablePaging() {
    $('#ResourceTable_paginate').children('ul').addClass('flatpagi');
    $('#ResourceTable_previous').children('a').remove();
    $('#ResourceTable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#ResourceTable_next').children('a').remove();
    $('#ResourceTable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function ResourceCenterTooltip() {
    $("#AddResourceClose").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-sammery").tooltip({ title: 'View Resource', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit Resource', placement: 'top' });
    $(".e-delete").tooltip({ title: 'Delete Resource', placement: 'top' });
}

function commonResourceSubString(input, length) {

    if (input.length <= length) {
        input = input
    }
    else {
        input = input.substring(0, length) + '...'
    }

    return input
}

function LoadResourceData(searchTerm) {
    var CompanyId = $('#hdnUserCompanyId').val();
    var RoleId = $('#hdnUserRoleId').val();
    var model = new Object();

    if (CompanyId != 0) {
        model.CompanyID = CompanyId;
    }
    model.SearchTerm = searchTerm;

    $.ajax({
        type: "POST",
        url: "/ResourceUpload/GetResourceAll/",
        data: model,
        async: false,
        success: function (response) {

            var renderHTML = '';
            $('#links').html('');

            if (response.length > 0) {
                if (RoleId == 1 || RoleId == 3) {
                    renderHTML += '<div class="gallery-item-list" id="tblResource" style="display:none;"><span class="gallery-item-list-imghead">Title Image</span><div class="pull-left"><span class="gallery-list-head">Resource Name</span><span class="gallery-list-head">Product Name</span><span class="gallery-list-head">Document Type</span><span class="gallery-list-head">Status</span></div><div class="pull-right"><span class="gallery-list-head">Action</span></div><div class="clearfix"></div></div>';
                }
                else {
                    renderHTML += '<div class="gallery-item-list" id="tblResource" style="display:none; "><span class="gallery-item-list-imghead" style="width:215px;">Title Image</span><div class="pull-left"><span class="gallery-list-head">Resource Name</span><span class="gallery-list-head">Product Name</span><span class="gallery-list-head">Document Type</span><span class="gallery-list-head">Status</span></div><div class="clearfix"></div></div>';
                }
                $.each(response, function (i, val) {

                    var fileType = getFileExtension(val.DocumentPath);
                    var titleValue = val.ResourceName + ' uploaded by ' + val.CreatedBy + 'on ' + convertjsonOnlydate(val.UploadDate) + ' under ' + val.DocumentType + ' for ' + val.ProductName;

                    if (val.URLName != null && val.URLName != "" && val.URLName != undefined) {

                        renderHTML += '<div class="gallery-item" href="javascript:void(0)" data-gallery><a target="_new" title="' + titleValue + '" onclick="Setvideourl(\'' + val.URLName + '\')" href="javascript:void(0)">';

                        if (RoleId == 1 || RoleId == 3) {
                            renderHTML += '<div class="image" align="center">';
                        }
                        else {
                            renderHTML += '<div class="image" style="width:215px;" align="center">';
                        }

                        if (val.TitleImage != null && val.TitleImage != '')
                        {
                            if (val.TitleImage == "no_images.png") {
                                renderHTML += '<img src="/images/ProfilePics/no_images.png" class="fixed-height-width" alt="Title Image" /></div>';
                            }
                            else
                            {
                                renderHTML += '<img class="fixed-height-width" src="/images/ProfilePics/' + val.TitleImage + '" alt="Title Image" /></div>';
                            }
                        }
                        else {
                            renderHTML += '<img src="/images/gallery/www.png" alt="Web URL" /></div>';
                        }
                    }
                    else {

                        renderHTML += '<div class="gallery-item" href="javascript:void(0)" data-gallery><a target="_new" title="' + titleValue + '" href="/ResourceUpload/ViewResourceName?DocPath=' + val.DocumentPath + '">';

                        if (RoleId == 1 || RoleId == 3) {
                            renderHTML += '<div class="image" align="center" >';
                        }
                        else {
                            renderHTML += '<div class="image" style="width:215px;" align="center">';
                        }
                       
                        if (val.TitleImage != null && val.TitleImage != '') {
                            
                            if (val.TitleImage == "no_images.png") {
                                renderHTML += '<img class="img-responsive fixed-height-width" src="/images/ProfilePics/no_images.png" alt="Title Image" /></div>';
                            }
                            else {
                                renderHTML += '<img class="img-responsive fixed-height-width" src="/images/ProfilePics/' + val.TitleImage + '" alt="Title Image" /></div>';
                            }
                        }
                        else {
                            if (fileType == "jpeg" || fileType == "jpe" || fileType == "jpg") {
                                renderHTML += '<img src="/images/gallery/jpeg.png" alt="jpeg image" /></div>';
                            }
                            else if (fileType == "png") {
                                renderHTML += '<img src="/images/gallery/png.png" alt="png image" /></div>';
                            }
                            else if (fileType == "gif") {
                                renderHTML += '<img src="/images/gallery/gif.png" alt="gif image" /></div>';
                            }
                            else if (fileType == "pdf") {
                                renderHTML += '<img src="/images/gallery/pdf.png" alt="gif image" /></div>';
                            }
                            else if (fileType == "doc" || fileType == "docx") {
                                renderHTML += '<img src="/images/gallery/word.png" alt="document file" /></div>';
                            }
                            else if (fileType == "ppt" || fileType == "pptx") {
                                renderHTML += '<img src="/images/gallery/ppt.png" alt="ppt file" /></div>';
                            }
                            else if (fileType == "xls" || fileType == "xlsx") {
                                renderHTML += '<img src="/images/gallery/xls.png" alt="excel file" /></div>';
                            }
                            else if (fileType == "txt") {
                                renderHTML += '<img src="/images/gallery/txt.png" alt="text file" /></div>';
                            }
                            else if (fileType == "html") {
                                renderHTML += '<img src="/images/gallery/www.png" alt="html file" /></div>';
                            }
                            else if (fileType == "acd") {
                                renderHTML += '<img src="/images/gallery/acd.png" alt="acd file" /></div>';
                            }
                            else if (fileType == "bim") {
                                renderHTML += '<img src="/images/gallery/bim.png" alt="bim image" /></div>';
                            }
                            else if (fileType == "cad") {
                                renderHTML += '<img src="/images/gallery/cad.png" alt="cad file" /></div>';
                            }
                            else if (fileType == "pps") {
                                renderHTML += '<img src="/images/gallery/pps.png" alt="pps file" /></div>';
                            }
                            else if (fileType == "rfa") {
                                renderHTML += '<img src="/images/gallery/rfa.png" alt="rfa file" /></div>';
                            }
                            else if (fileType == "odt") {
                                renderHTML += '<img src="/images/gallery/odt.png" alt="document file" /></div>';
                            }
                            else {
                                renderHTML += '<img class="img-responsive" src="/images/gallery/no-image.png" alt="other file" /></div>';

                            }
                        }
                    }

                    //renderHTML += '<div class="gallery-item" href="javascript:void(0)" data-gallery><a target="_new" title="' + titleValue + '" href="/ResourceUpload/ViewResourceName?DocPath=' + val.DocumentPath + '">';



                    renderHTML += '</a><div class="meta"><div class="pull-left"><span>' + commonResourceSubString(val.ResourceName, 40) + '</span>';
                    renderHTML += '<span id="idProductName" style="display:none">' + val.ProductName + '</span><span id="idDocumentType" style="display:none">' + val.DocumentType + '</span>';
                  
                    //renderHTML += '<strong>' + val.PublishedStatus + '</strong>';

                    renderHTML += '</div><div class="col-sm-12 nopad box_statusAction">';
                    if (val.PublishedStatus == "Draft") {
                        renderHTML += '<strong class="label s-draft pull-left label_bordered">Draft</strong>';
                    }
                    else {
                        renderHTML += '<strong class="label s-published pull-left label_bordered">Published</strong>';
                    }
                    if (RoleId == 1 || RoleId == 3) {
                        renderHTML += '<a href="javascript:void(0)" class="btnsml_action e-edit pull-right" onclick="editResource(' + val.ResourceId + ')" title=""></a>';
                    }
                    else {
                        renderHTML += '<a></a>'
                    }
                    //renderHTML += '<a href="javascript:void(0)" name="btnDeleteDocumentDirect" class="btnsml_action e-delete" title=""></a>';
                    renderHTML += '</div><div class="clearfix"></div></div></div>';
                })
            }
            else {
                renderHTML += '<div align="center">No Resource Found..!</div><br/>';
            }

            $('#links').append(renderHTML);
            if (structure == 1) {
                $('#grid').click();
            }
            else {
                $('#list').click();
            }

        },
        error: function (err) {
            alert(err.message);
        }
    })

    ResourceCenterTooltip();
}

function commonSubStringResource(input, length) {

    if (input.length <= length) {
        input = input
    }
    else {
        input = input.substring(0, length) + '...'
    }

    return input
}

function getFileExtension(name) {
    var found = name.lastIndexOf('.') + 1;
    return (found > 0 ? name.substr(found) : "");
}

$(document).ready(function () {

    if (structure == 1) {
        $('#grid').click();
    }
    else {
        $('#list').click();
    }
    $('#dpExpDate').hide();

    $('#editdpExpDate').hide();

    var searchValue = '';
    LoadResourceData(searchValue);

    $('#AddIsExpiryDate').change(function () {
        if ($(this).is(":checked")) {
            $('#dpExpDate').show();
        }
        else {
            $('#dpExpDate').hide();
            $('#dpExpDate').val(null);
        }
    });

    $('#editIsExpiryDate').change(function () {
        if ($(this).is(":checked")) {
            $('#editdpExpDate').show();
        }
        else {
            $('#editdpExpDate').hide();
            $('#editdpExpDate').val(null);
        }
    });

    //Add document side
    var CompId = 0;

    var hdnPageSize = $('#hdnPageSize').val();

    var RoleId = $('#hdnUserRoleId').val();

    var CompanyId = $('#hdnUserCompanyId').val();

    var model = new Object();

    if (CompanyId != 0) {
        model.CompanyID = CompanyId;
    }

    $("#ResourceTable").on('click', 'a[name=DownloadDocumentName]', function () {

        var Fullpath = $(this).attr('fullpath');

        window.location.href = '/ManageDocument/DownloadDocument?DocPath=' + escape(Fullpath);

    });

    function successdoctype(data) {
        var obj = data;

        var options = null;
        options = '<option name = "doctypeoption" value="">--Select--</option>';
        for (var i = 0; i < obj.length; i++) {

            options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
        }
        $("select#adddoctype").html(options);

        $("select#editdoctype").html(options);

    };

    function errordoctype(data) {
        return false;
    };

    $.ajax({
        url: '/Common/BindDocumentTypeForResource',
        type: 'GET',
        success: successdoctype,
        error: errordoctype
    });

    $('#ResourceDatatableSearch').unbind().keyup(function () {
        var value = $(this).val();
        if (value.length > 1) {

            //table.search(value).draw();
            LoadResourceData(value);
        }
        if (value == '') {
            //table.search(value).draw();
            LoadResourceData(value);
        }
    });

    var Did = 0;
    var userIdListP = [];

    $('#chkAllUserPermission').change(function () {

        if ($(this).is(":checked")) {
            $('input[name="chkUserPermission"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                var DocID = result[2];
                userIdListP.push(UserID);
                Did = DocID;
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUserPermission"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                Did = DocID;
                var DocID = result[2];

                userIdListP = jQuery.grep(userIdListP, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $('#addDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#editDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#AddResourceClose').click(function () {

        $('#addResourceName').val("");
        $('#addProductName').val("");
        $('#adddoctype').val($("#adddoctype option:first").val());
        $('#AddIsPublished').prop('checked', false);
        $('#UrlName').val("");

        Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
    });

    $('#btnAddResource').click(function (event) {
        IsDeleted = 0;
            
        event.preventDefault();

        var isValid = true;

        $('input[class="form-control resourcerequired"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {
                isValid = true;
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        var setfocus = "f";
        var ele = $('input[class="form-control resourcerequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }

        var Urlname = $('#UrlName').val();

        var filename = $('div.dz-filename span').text();

        if (filename == null || filename == "" || filename == undefined) {
            if ((filename == null || filename == "" || filename == undefined) && (Urlname == null || Urlname == "" || Urlname == undefined)) {

                $('#dropzoneForm').css({ "border": "1px solid red" });
                $('#UrlName').css({ "border": "1px solid red" });

                isValid = false;
            }
            else {

                $('#dropzoneForm').css({
                    "border": "",
                    "background": ""
                });
                $('#UrlName').css({
                    "border": "",
                    "background": ""
                });
            }
        }
        else {
            if ((filename != null && filename != "" && filename != undefined) && (Urlname != null && Urlname != "" && Urlname != undefined)) {

                $('#dropzoneForm').css({ "border": "1px solid red" });
                $('#UrlName').css({ "border": "1px solid red" });

                isValid = false;
            }
            else {

                $('#dropzoneForm').css({
                    "border": "",
                    "background": ""
                });
                $('#UrlName').css({
                    "border": "",
                    "background": ""
                });
            }
        }

        var fileerrormsg = $('#dropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#dropzoneForm').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {

        }

        if (Urlname != null && Urlname != "" && Urlname != undefined) {
            if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(Urlname)) {

            } else {

                $('#UrlName').tooltip({ title: 'URL, Follow this format https://google.co.in', placement: 'top' });
                $('#UrlName').focus();
                $('#UrlName').css({ "border": "1px solid red" });
                isValid = false;

            }

        }

        if ($('#adddoctype').val() == "") {
            $('#adddoctype').css({ "border": "1px solid red" });
            isValid = false;
        }

        if (isValid == false)
            return false;

        var formData = new FormData();


        var Publishedstatus = false;
        var ResourceType;
        if ($("#AddIsPublished").is(':checked'))
            // checked
            Publishedstatus = true;
        else
            Publishedstatus = false;

        if (Urlname == null || Urlname == "" || Urlname == undefined) {
            ResourceType = "Document";
        }
        else {
            ResourceType = "URL"
        }

        var files = $("#addProfile").get(0).files;
        if (files[0] != null) {
            var thumbfileName = files[0].name;
        }
        else {
            var thumbfileName = "no_images.png";
        }
        formData.append("img", files[0]);
        formData.append("ResourceName", $('#addResourceName').val().trim());
        formData.append("ProductName", $('#addProductName').val().trim());
        formData.append("DocumentTypeId", $('#adddoctype').val().trim());
        formData.append("URLName", Urlname);
        formData.append("PublishedStatus", Publishedstatus);
        formData.append("ResourceType", ResourceType);
        formData.append("FileName", filename);
        formData.append("TitleImage", thumbfileName);

        if (isValid) {
            $.ajax({
                url: '/ResourceUpload/AddResource',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    $('#AddResource').modal('hide');
                    $('#SuccessDocument').modal('show');
                    var table = $('#ResourceTable').DataTable();
                    table.ajax.reload();
                    if (structure == 1) {
                        $('#grid').click();
                    }
                    else {
                        $('#list').click();
                    }

                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            return false;
        }


    });

    $('#documentok').click(function () {
        $('#SuccessDocument').modal('hide');
        var searchValue = '';
        LoadResourceData(searchValue);

    });

    $('#editdocumentok').click(function () {
        $('#SuccessEditDocument').modal('hide');
        var searchValue = '';
        LoadResourceData(searchValue);
    });

    $('#btnUpdateResource').click(function (event) {

        event.preventDefault();

        var isValid = true;

        $('input[class="form-control editresourcerequired"]').each(function () {
            if ($.trim($(this).val()) == '') {

                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });
            }
        });

        var setfocus = "f";
        var ele = $('input[class="form-control editresourcerequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }

        var Urlname = "";
        if ($('#EditUrlName').val() != null && $('#EditUrlName').val() != "" && $('#EditUrlName').val() != undefined) {
            Urlname = $('#EditUrlName').val().trim();
        }

        var filename = $('#editdropzoneForm div.dz-filename span').text();

        if (filename == null || filename == "" || filename == undefined) {
            if ((filename == null || filename == "" || filename == undefined) && (Urlname == null || Urlname == "" || Urlname == undefined)) {
                if ($('#attchFilename').find('span.forPadding').length != 0) {
                }
                else {
                    $('#editdropzoneForm').css({ "border": "1px solid red" });
                    $('#EditUrlName').css({ "border": "1px solid red" });
                    isValid = false;
                }
            }
            else {
                $('#editdropzoneForm').css({
                    "border": "",
                    "background": ""
                });
                $('#EditUrlName').css({
                    "border": "",
                    "background": ""
                });
            }
        }
        else {

            if ($('#attchFilename').find('span.forPadding').length != 0) {

                if (Urlname != null && Urlname != "" && Urlname != undefined) {
                    $('#editdropzoneForm').css({ "border": "1px solid red" });
                    $('#EditUrlName').css({ "border": "1px solid red" });

                    isValid = false;
                }
                else {
                    $('#editdropzoneForm').css({
                        "border": "",
                        "background": ""
                    });
                    $('#EditUrlName').css({
                        "border": "",
                        "background": ""
                    });
                }
            }
            else {

            }
        }
        var fileerrormsg = $('#editdropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#editdropzoneForm').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {

        }

        if (Urlname != null && Urlname != "" && Urlname != undefined) {
            if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(Urlname)) {

            } else {

                $('#EditUrlName').tooltip({ title: 'URL, Follow this format https://google.co.in', placement: 'top' });
                $('#EditUrlName').focus();
                $('#EditUrlName').css({ "border": "1px solid red" });
                isValid = false;

            }

        }

        if (isValid == false)
            return false;

        var formData = new FormData();
        var picfiles = $("#editpic").get(0).files;
        if (picfiles[0] != null) {
            var picfileName = picfiles[0].name;
            IsDeleted = 0;
        }
        //else {
        //    var picfileName = "no_images.png";
        //}

        //$('#editUserpic').attr('src', Resourcedetails.TitleImage);
        //         $('#editpicFilename').val(Resourcedetails.TitleImage);

        formData.append("img", picfiles[0]);
        var Publishedstatus = false;
        var ResourceType;

        if ($("#EditIsPublished").is(':checked'))
            // checked
            Publishedstatus = true;
        else
            Publishedstatus = false;

        if (Urlname == null || Urlname == "" || Urlname == undefined) {
            ResourceType = "Document";
        }
        else {
            ResourceType = "URL"
        }
        //var files = $("#addProfile").get(0).files;
        //if (files[0] != null) {
        //    var fileName = files[0].name;
        //}
        //else {
        //    var fileName = "no_images.png";
        //}

        formData.append("ResourceId", $('#editResourceid').val().trim());
        formData.append("ResourceName", $('#editResourceName').val().trim());
        formData.append("ProductName", $('#editProductName').val().trim());
        formData.append("DocumentTypeID", $('#editdoctype').val().trim());
        formData.append("URLName", Urlname);
        formData.append("PublishedStatus", Publishedstatus);
        formData.append("ResourceType", ResourceType);
        formData.append("FileName", filename);
        formData.append("TitleImage", picfileName);
        formData.append("IsDeleted", IsDeleted);

        $.ajax({
            url: '/ResourceUpload/UpdateResource',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                IsDeleted = 0;
                $('#editDocument').modal('hide');
                $('#SuccessEditDocument').modal('show');
                var table = $('#ResourceTable').DataTable();
                table.ajax.reload();
                if (structure == 1) {
                    $('#grid').click();
                }
                else {
                    $('#list').click();
                }

            },
            error: function (data) {
                alert('error')
            }
        })
        //}
    });

    var userIdList = [];

    $('#chkAllUser').change(function () {
        if ($(this).is(":checked")) {
            $('input[name="chkUser"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                userIdList.push(UserID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUser"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];

                userIdList = jQuery.grep(userIdList, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $('#btnAllDelete').click(function (event) {
        event.preventDefault();

        var IsValiddd = true;
        if (userIdList.length == 0) {
            IsValiddd = false;
        }

        var result = userIdList.join(", ")

        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageUser/MultipleUserDelete",
                data: { userID: result },
                success: function (userdetail) {
                    $('#chkAllUser').prop('checked', false);
                    var table = $('#UserDatatable').DataTable();
                    table.ajax.reload();
                }
            })
        }


    })

    $("#tblCategory_paginate").on("click", "a", function () {
        $('#chkAllUser').prop('checked', false);
        $('input[name="chkUser"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var UserID = result[1];

            userIdList = jQuery.grep(userIdList, function (value) {
                return value != UserID;
            });
        });
    });

    //$('.dataTables_length').hide();

    //$('#ResourceTable_filter').hide();

    $('#addProfile').change(function (event) {
        event.preventDefault();
        var files = $("#addProfile").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

            var image = new Image();
            image.src = window.URL.createObjectURL($('#addProfile')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 60 && width < 60) {
                    //alert("Height and Width should be at least 60px.");
                    $('#CommonErrorMessage').html('Height and Width should be at least 60px.');
                    $('#CommonErrorModal').modal('show');
                    return false;
                }
                    //else if (height > 300) {
                    //     alert("Image Height should not be greater than 300px.");
                    //     return false;
                    // }
                else {
                    DisplayImage($('#addProfile')[0].files, $('#addlogo').attr('id'));
                }
            }
        }
        else {
            //alert('Only jpg and png format');
            var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'ProfilePic') { return e } })
            if (updateMessage != '') {
                $('#CommonErrorMessage').html(updateMessage[0].Message);
                $('#CommonErrorModal').modal('show');
            }
            //$('#btnAddUser').prop('disabled', true)
        }
    });

    $("#editpic").change(function (event) {
        event.preventDefault();
        var files = $("#editpic").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

            var image = new Image();
            image.src = window.URL.createObjectURL($('#editpic')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 60 && width < 60) {
                    alert("Height and Width should be at least 60px.");
                    return false;
                }
                    //else if (height > 300) {
                    //    alert("Image Height should not be greater than 300px.");
                    //    return false;
                    //}
                else {
                    DisplayImage($('#editpic')[0].files, $('#editUserpic').attr('id'));
                }
            }

        }
        else {
            var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'ProfilePic') { return e } })
            if (updateMessage != '') {
                $('#CommonErrorMessage').html(updateMessage[0].Message);
                $('#CommonErrorModal').modal('show');
            }

        }
    });
    $('#list').click(function (event) {
        event.preventDefault();
        $('#links .gallery-item').addClass('gallery-item-list');
        $('#links .gallery-item-list').removeClass('gallery-item');
        $('#links .gallery-item-list .pull-left span').css('display', 'block');
        $('#tblResource').css('display', 'block');
        $('img').removeClass('fixed-height-width');
        structure = 0;
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#links .gallery-item-list').addClass('gallery-item');
        $('#links .gallery-item').removeClass('gallery-item-list');
        $('#links #idProductName').css('display', 'none');
        $('#links #idDocumentType').css('display', 'none');
        $('#tblResource').css('display', 'none');
        $('#links img').addClass('fixed-height-width');

        structure = 1;
    });

});
