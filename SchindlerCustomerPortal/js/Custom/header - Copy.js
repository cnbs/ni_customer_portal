﻿function unreadMailForHeader() {
    $.ajax({
        url: '/MailBox/UnreadMailCountforHeader',
        method: 'POST',
        //data: { mail: mailId },
        success: function (data) {
            //debugger
            //$('#dashboardAllmsg').html('&nbsp;');

            //if (data.mailList.length > 0) {
            //    $('#headerUnreadCount').html(data.mailList.length);
            //    $('#dashboardAllmsg').html(data.mailList.length);
            //}

            if (data.calendarList.length > 0) {
                $('#headerTodayEventCount').html(data.calendarList.length);
                $('#calendarCount').show();
            }

            //$('#headerMessageCount').text(data.mailList.length + ' new');
            $('#headerEventCount').text(data.calendarList.length + ' new');


            //$('#headerMailBox .mCSB_container').html('');
            $('#headerCalendarBox .mCSB_container').html('');

            var renderHTML = '';

            //$.each(data.mailList, function (i, val) {

            //    renderHTML += '<a href="/mailbox/maillist?mail=' + val.MailId + '" class="list-group-item">';
            //    renderHTML += '<div class="list-group-status status-online"></div>';
            //    renderHTML += '<img src=' + val.ProfilePic + ' class="pull-left mCS_img_loaded" alt="' + val.UserName + '"> <span class="contacts-title">' + val.UserName + '</span>';
            //    renderHTML += '<p>' + val.Subject + '</p></a>';

            //})

            //$('#headerMailBox').mCustomScrollbar();
            //$('#headerMailBox .mCSB_container').append(renderHTML);

            renderHTML = '';

            $.each(data.calendarList, function (i, val) {

                renderHTML += '<a href="/CalendarView/Index" class="list-group-item">';
                renderHTML += '<div class="list-group-status status-online"></div>';
                renderHTML += '<img src=' + val.ProfilePic + ' class="pull-left mCS_img_loaded" alt="' + val.Sender + '"> <span class="contacts-title">' + val.Sender + '</span>';
                renderHTML += '<p>' + val.Subject + '</p></a>';

            })

            $('#headerCalendarBox').mCustomScrollbar();
            $('#headerCalendarBox .mCSB_container').append(renderHTML);

        },
        error: function (data) {
            //alert('The Requested URL Cannot be Found..!');
        }

    })
}

function HelpModule(moduleId) {

    if (moduleId == null || moduleId == "" || moduleId == undefined) {
        moduleId = 0
    }
    if ($.trim($('#headerHelp').text()) == "") {
        $.ajax({
            url: '/Help/GetModulewiseHelp',
            method: 'POST',
            data: { ModuleId: moduleId },
            success: function (data) {

                $('#headerHelp').html('');

                var renderHTML = '';

                if (data.length == 0) {
                    renderHTML += '<h4 class="cstm-title">No Help</h4>'
                }

                $.each(data, function (i, val) {

                    if (i == 0) {
                        renderHTML += '<h4 class="cstm-title">' + val.ModuleName + '</h4>'
                    }

                    renderHTML += '<p><strong>' + val.Title + '</strong></p><p>' + val.Description + '</p>'

                })

                $('#headerHelp').append(renderHTML);

                $('#headerHelp').mCustomScrollbar();

            },
            error: function (data) {
                //alert('error')
            }

        })
    }
}

function HeaderTooltip() {
    $("#contextHelp").tooltip({ title: 'Help', placement: 'left' });
    $("#messageHeaderTooltip").tooltip({ title: 'New Messages', placement: 'left' });
    $("#profileHeaderTooltip").tooltip({ title: 'Profile Info', placement: 'left' });
}

$(document).ready(function () {
    //debugger
    unreadMailForHeader();

    window.setInterval(function () {
        /// call your function here
        unreadMailForHeader();
    }, 10000);

    $('#contextHelp').click(function () {
        var val = $('#hdnContextHelp').val();
        HelpModule(val);
    })

    HeaderTooltip();

    $('.HeaderDivision#messageHeaderTooltip').click(function () {
        var elementId = $(this).attr('id');
        $('.HeaderDivision').each(function (i, val) {
            if ($(this).attr('id') != elementId) {
                $(this).removeClass('active');
            }
        })
    })

    var closeNestedDropdown = function () {
        $('#CalendarDropdown > ul').hide();
    }

    var showNestedDropdown = function () {
        $('#CalendarDropdown > ul').show();
    }

    $('#CalendarDropdown').on('mouseover', function (e) {
        //timeout = setTimeout(showNestedDropdown, 500);
        //$('#CalendarDropdown > ul').show();
        $('#CalendarDropdown > ul').stop(true, true).delay(200).fadeIn(0);
    });

    $('#CalendarDropdown,#CalendarDropdown > ul').on('mouseout', function (e) {
        //timeout = setTimeout(closeNestedDropdown, 500);
        //clearTimeout(timeout);
        $('#CalendarDropdown > ul').stop(true, true).delay(1000).fadeOut(500);
    });


})