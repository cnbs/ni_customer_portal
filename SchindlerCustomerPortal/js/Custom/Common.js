﻿var MyProfileMessageList = '';
var IsDeleted = 0;

function DisplayImage(files, imageHolderId) {
    /// <summary>
    /// Displays the image.
    /// </summary>
    /// <param name="files">The files collecitons.</param>
    /// <param name="imageHolderId">The image holder identifier.</param>
    /// <returns></returns>
    for (var i = 0, f; f = files[i]; i++) {
        var render = new FileReader();
        render.onload = function (evt) {
            $('#' + imageHolderId).removeAttr('src');
            $('#' + imageHolderId).attr('src', evt.target.result + '');
        }
        render.readAsDataURL(f);
    }
}

function clearFileInputField(tagId) {

    $('#'+ tagId).attr('src', '/images/ProfilePics/no_images.png');
    IsDeleted = 1;

}

function SetDesignation() {

    $.ajax({
        type: "POST",
        url: "/Common/BindDesignationList/",
        //data: { userId: id },
        success: function (data) {

            $('#profileDesignation').append($("<option/>", ($({ value: '', text: '--Select--' }))));

            $.each(data, function (i, val) {
                $('#profileDesignation').append($("<option/>", ($({ value: val.DesignationId, text: val.Designation }))));
            })
        }
    });
}

function ValidateChangePassword() {
    /// <summary>
    /// Validates the change password.
    /// </summary>
    /// <returns></returns>
    var inValid = false;
    var oldpassword = $('#oldpassword').val().trim();
    var newpassword = $('#newpassword').val().trim();
    var cnfnewpassword = $('#cnfnewpassword').val().trim();

    if (oldpassword == "") {
        inValid = true;
        $('#oldpassword').css('border-color', 'red');
    }

    if (newpassword == "") {
        inValid = true;
        $('#newpassword').css('border-color', 'red');
    }

    if (cnfnewpassword == "") {
        inValid = true;
        $('#cnfnewpassword').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function ValidateUserProfile() {
    /// <summary>
    /// Validates the user profile.
    /// </summary>
    /// <returns></returns>
    var inValid = false;
    var profileFirstName = $('#profileFullName').val().trim();

    var profilePhoneNo = $('#profilePhoneNo').val().trim();
    var addDesignation = $('#profileDesignation').val().trim();

    if (profileFirstName == "") {
        inValid = true;
        $('#profileFullName').css('border-color', 'red');
    }

    if (profilePhoneNo == "") {
        inValid = true;
        $('#profilePhoneNo').css('border-color', 'red');
    }

    if (addDesignation == "") {
        inValid = true;
        $('#profileDesignation').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function ValidateElement(element) {

    if (element.val() == "")
        element.css('border-color', 'red');
    else
        element.css('border-color', '');

}

function PasswordPopUpOpen() {
    $("#mypassword").modal(); return false;
}

function UserProfilePopUpOpen() {
    $("#myprofile").modal(); return false;
}

function BindStateForUser() {
    /// <summary>
    /// Binds the state for user.
    /// </summary>
    /// <returns></returns>
    var usaCountry = 1;
    $.ajax({
        type: "GET",
        url: "/Common/BindState/",
        async:false,
        data: { countryid: usaCountry },
        success: function(userdetail) {

            $('#addState').html("");
            $('#addState').append($("<option/>", ($({ value: '0', text: '--Select--' }))));

            $('#editState').html("");
            $('#editState').append($("<option/>", ($({ value: '0', text: '--Select--' }))));

            $('#profileState').html("");
            $('#profileState').append($("<option/>", ($({ value: '0', text: '--Select--' }))));

            $.each(userdetail,
                function(i, val) {

                    $('#addState').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));

                    $('#editState').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));
                    $('#profileState').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));
                });

        }
    });
}

function logOut() {
    window.location.href = '/login/logout';
}

function AlertMessageMyProfile() {

    $.ajax({
        type: "POST",
        url: "/Common/AlertForMyProfile/",
        //data: { userId: id },
        success: function(data) {
            MyProfileMessageList = data;
        }
    });
}

$(document).ready(function () {

    //BindStateForUser();
    SetDesignation();
    //AlertMessageMyProfile();
    $('#profilePhoneNo').mask('000-000-0000');

    var usaCountry = 1;
    var cityIdSelected = 0;

    var txtProfileCityoptions = {
        source: function (request, response) {
           $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' +
                    $('#profileCity').val() +
                    '" , countryid:"' +
                    usaCountry +
                    '" , stateCode:"' +
                    $('#profileState').val() +
                    '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function(data) {

                    response($.map(data,
                        function(item) {
                            return {
                                label: item.CityName,
                                value: item.CityName,
                                CityID: item.CityId
                            }
                        }));
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#profileCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2
    };

    $("#profileState").change(function (event) {
        $('#profileCity').val('');
    });

    $(document).on("keydown.autocomplete", '#profileCity', function (event) {
        $(this).autocomplete(txtProfileCityoptions);
    })

    $('#btneditProfile').click(function (event) {
        event.preventDefault();

        BindStateForUser();
        AlertMessageMyProfile();

        UserProfilePopUpOpen();

        $.ajax({
            url: '/ManageUser/SelectProfile',
            method: 'GET',
            dataType: 'json',
            success: function(userDetail) {

                if (userDetail.ProfilePic == '/images/ProfilePics/') {
                    $('#profileuserimage').attr('src', '/img/no_images.png');
                } else {
                    $('#profileuserimage').attr('src', userDetail.ProfilePic);
                }
                $('#editprofileuserid').val(userDetail.UserID);
                $('#profileFullName').val(userDetail.FirstName);

                $('#profilePhoneNo').val(userDetail.ContactNumber);
                $('#profileEmail').val(userDetail.Email);
                $('#editprofileRoleID').val(userDetail.RoleID);

                $('#profileStreetAddress').val(userDetail.Address1);
                $('#profileState').val(userDetail.State);

                $('#profileCity').val(userDetail.City);
                $('#profileZipCode').val(userDetail.PostalCode);

                $("#profileDesignation").val(userDetail.DesignationId);

                $("#editprofileAccountStatus").val(userDetail.AccountStatus);
                $('#profileUnsubscribe').prop('checked', userDetail.Unsubscribe);

            },
            error: function(userDetail) {
                alert('error');
            }
        });

    });

    $("#profileImageUpload").change(function (event) {
        event.preventDefault();
        IsDeleted = 0;
        var files = $("#profileImageUpload").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

            var image = new Image();
            image.src = window.URL.createObjectURL($('#profileImageUpload')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 100 && width < 100) {
                    //alert("Height and Width must not exceed 100px.");
                    $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                    $('#CommonErrorModal').modal('show');
                    return false;
                }
                else {
                    DisplayImage($('#profileImageUpload')[0].files, $('#profileuserimage').attr('id'));
                    DisplayImage($('#profileImageUpload')[0].files, $('#profile-pic').attr('id'));
                    return true;
                }
                
            }

        }
        else {
            //alert('Only jpg and png format');
            $('#CommonErrorMessage').html('Only jpg and png format.');
            $('#CommonErrorModal').modal('show');
        }
    });

    $('#profileFullName,#profilePhoneNo,#profileDesignation').on('blur', function () {
        ValidateElement($(this));
    });

    $('#saveProfile').click(function(event) {

            if (!ValidateUserProfile()) {
                return false;
            }
        var isValid = true;

        var phonelen = $('#profilePhoneNo').val().length;
        if (phonelen < 12) {
            $('#profilePhoneNo').css({
                "border": "1px solid red"
            });
            $('#profilePhoneNo').focus();
            var isValid = false;
            $('#saveProfile').removeAttr('disabled');
            return false;
        }
        //validation for name allow only alphabets
        var fullname = $('#profileFullName').val();
        var re = /^[a-zA-Z ]*$/;
        var namecheck = "";
        if (typeof fullname != 'undefined' && fullname != null && fullname.trim() != "") {
            if (!fullname.match(re)) {
                $("#profileFullName").css({
                    "border": "1px solid red"
                });
                $("#profileFullName").focus();
                namecheck = "a";
                $('#saveProfile').removeAttr('disabled');
            }
        }
        if (namecheck == "a") {
            return false;
        }

            event.preventDefault();
            var formData = new FormData();
            var files = $("#profileImageUpload").get(0).files;

            var contact = $("#profilePhoneNo").val().replace('-', '').replace('-', '');
            formData.append("img", files[0]);
            formData.append("IsDeleted", IsDeleted);
            formData.append("UserID", $('#editprofileuserid').val().trim());
            formData.append("FirstName", $('#profileFullName').val().trim());

            formData.append("ContactNumber", contact);

            formData.append("Designation", $('#profileDesignation option:selected').val().trim());

            formData.append("Address1", $('#profileStreetAddress').val().trim());
            formData.append("State", $('#profileState').val());
            formData.append("StateText", $('#profileState option:selected').text());
            formData.append("City", $('#profileCity').val());
            formData.append("PostalCode", $('#profileZipCode').val());

            formData.append("AccountStatus", $('#editprofileAccountStatus').val());
            if ($('#profileUnsubscribe').is(":checked")) {
                formData.append("Unsubscribe", true);
            }
            else
            {
                formData.append("Unsubscribe", false);
            }
           

            $.ajax({
                url: '/ManageUser/UpdateUser',
                method: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(userDetail) {
                    var updateMessage = $.grep(MyProfileMessageList,
                        function(e) {
                            if (e.MessageName == 'Update') {
                                return e;
                            }
                        });
                    if (updateMessage != '') {
                        $('#CommonSuccessMessage').html(updateMessage[0].Message);
                        $('#CommonSuccessModal').modal('show');
                        $('#CommonSuccessModal .mybtn').click(function () {
                            $('#CommonSuccessModal').modal('hide');
                            $('#myprofile').modal('hide');
                            window.location.reload();
                        })
                    }
                    IsDeleted = 0;
                },
                error: function(userDetail) {
                    alert('error');
                }
            });

        });

    var passwordValid = true;

    $('input[name=newpassword]').popover({
        trigger: 'click focus',
        html: true,
        content: function () {
            return $('#pswd_info').html();
        }
    });

    $("#newpassword").focusout(function () {
        $('input[name=newpassword]').popover('hide');
    });

    $('input[name=password]').blur(function () {
        $('input[name=newpassword]').popover('hide');
    });

    $('#oldpassword,#newpassword,#cnfnewpassword').on('blur', function () {
        ValidateElement($(this));
    });

    $('input[name=newpassword]').on('click focus keyup blur', function () {

        passwordValid = true;
        var pswd = $(this).val();

        //validate total count
        if (pswd.length < 8) {
            $('#length').removeClass('valid').addClass('invalid');
            passwordValid = false;
        } else {
            $('#length').removeClass('invalid').addClass('valid');
        }

        //validate letter
        if (pswd.match(/[A-z]/)) {
            $('#letter').removeClass('invalid').addClass('valid');
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
            passwordValid = false;
        }

        //validate capital letter
        if (pswd.match(/[A-Z]/)) {
            $('#capital').removeClass('invalid').addClass('valid');
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
            passwordValid = false;
        }

        //validate number
        if (pswd.match(/\d/)) {
            $('#number').removeClass('invalid').addClass('valid');
        } else {
            $('#number').removeClass('valid').addClass('invalid');
            passwordValid = false;
        }

    });

    $('#btnChangePassword').click(function (event) {

        if (!ValidateChangePassword()) {
            return false;
        }

        event.preventDefault();
        var oldpass = $('#oldpassword').val();
        var newpass = $('#newpassword').val();
        var cnfnewpass = $('#cnfnewpassword').val();

        if (oldpass == '' && newpass == '' && cnfnewpass == '') {
            passwordValid = false;
        }
   

        if (newpass != cnfnewpass && newpass.trim() != null) {
            $("#cnfnewpassword").val('');

            $('#CommonErrorMessage').html('new password and confirm password does not match.');
            $('#CommonErrorModal').modal('show');
            passwordValid = false;
            return false;
        }

        if (!passwordValid) {

            $('input[name=newpassword]').focus();
        }

        if (passwordValid) {
            event.preventDefault();
            var user = new Object();

            user.OldPassword = oldpass;
            user.NewPassword = newpass;

            $.ajax({
                url: '/ManageUser/ChangePassword',
                method: 'POST',
                dataType: 'json',
                data: user,
                success: function (data) {

                    $('#mypassword').modal('hide');
                    $('#userPasswordChanged').modal('show');
                },
                error: function (data) {
                    alert('error')
                }
            })
        }

    })

    //$('.modal').on('hidden.bs.modal', function () {
        
    //    var formValue = $(this).find('form').length;
    //    var formId = $(this).find('form')[0].id;
    //    if (formValue > 0) {
    //        $(this).find('form')[0].reset();
    //    }
    //    if (formId != "mailSentSuccessForm" && formId != "mailDeleteSuccessForm") {
    //        $('.form-control').removeAttr('style');
    //    }

    //    $('button').removeAttr('style');

    //    $('#ProjectCommunicationMessage').css('display', 'none');
    //   // $('.form-control').not('.input-error').removeAttr('style');
    //    //$(".input-error").css("border-color", "red").removeClass("input-error");
    //});

    $('.readonly').find('input, textarea, button, select').attr('disabled', 'disabled');
    $('#profileFullName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });

});