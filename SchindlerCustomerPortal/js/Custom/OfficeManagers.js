﻿var EditUserCompanyId = '';
var EditUserRoleId = '';
var subPermissionCount = 0;

BindStateForUser();

function EditOfficeManager(id) {

    $.ajax({
        type: "GET",
        url: "/OfficeManager/OfficeManagerEdit/",
        data: { userId: id },
        async: false,
        success: function (userdetail) {

            $('#editofficemanagerid').val(userdetail.OfficeManagerID);
            $('#editFirstName').val(userdetail.FirstName);
            $('#editLastName').val(userdetail.LastName);
            $('#editPhone').val(userdetail.Phone);
            $('#editOfficeID').val(userdetail.OfficeID);
            $('#editSapID').val(userdetail.SapID);
            $('#editEmail').val(userdetail.Email);
            $('#editOfficeDesc').val(userdetail.Office_Desc);
            $('#editStreetAddress').val(userdetail.Street);
            $('#editState').val(userdetail.State);
            $('#editCity').val(userdetail.City);
            $('#editZipCode').val(userdetail.Zip);
            $("#editDesignation").val(userdetail.Title);
        }
    })
}

function ViewOfficeManager(id) {

    $.ajax({
        type: "GET",
        url: "/OfficeManager/OfficeManagerEdit/",
        data: { userId: id },
        async: false,
        success: function (userdetail) {

            $('#viewofficemanagerid').val(userdetail.OfficeManagerID);
            $('#viewFirstName').text(userdetail.FirstName);
            $('#viewLastName').text(userdetail.LastName);
            $('#viewPhone').text(userdetail.Phone);
            $('#viewOfficeID').text(userdetail.OfficeID);
            if (userdetail.SapID != null && userdetail.SapID != '') {
                $('#viewSapID').text(userdetail.SapID);
            }
            else {
                $('#viewSapID').text("N/A");
            }
            $('#viewEmail').text(userdetail.Email);
            $('#viewOfficeDesc').text(userdetail.Office_Desc);

            if (userdetail.Street != null && userdetail.Street != '') {
                $('#viewStreetAddress').text(userdetail.Street);
            }
            else {
                $('#viewStreetAddress').text("N/A");
            }

            if (userdetail.State != null && userdetail.State != '') {
                $('#viewState').text(userdetail.StateName);
            }
            else {
                $('#viewState').text("N/A");
            }

            if (userdetail.City != null && userdetail.City != '') {
                $('#viewCity').text(userdetail.City);
            }
            else {
                $('#viewCity').text("N/A");
            }

            if (userdetail.Zip != null && userdetail.Zip != '') {
                $('#viewZipCode').text(userdetail.Zip);
            }
            else {
                $('#viewZipCode').text("N/A");
            }

            if (userdetail.Title != null && userdetail.Title != '') {
                $('#viewDesignation').text(userdetail.Title);
            }
            else {
                $('#viewDesignation').text("N/A");
            }
        }
    })
}

function DeleteOfficeManager(id) {

    $.ajax({
        type: "POST",
        url: "/OfficeManager/OfficeManagerDelete/",
        data: { officeManagerID: id },
        success: function (userdetail) {

            var table = $('#UserDatatable').DataTable();
            table.ajax.reload();
        }
    })
}

function ValidateAddOfficeManager() {
    var inValid = false;
    var addFirstname = $('#addFirstName').val().trim();
    var addLastName = $('#addLastName').val().trim();
    var addOfficeID = $('#addOfficeID').val().trim();
    var addPhone = $('#addPhone').val().trim();
    var addEmail = $('#addEmail').val().trim();
    var addOfficeDesc = $('#addOfficeDesc').val().trim();

    var addDesignation = $('#addDesignation').val().trim();

    if (addFirstname == "") {
        inValid = true;
        $('#addFirstName').css('border-color', 'red');
    }

    if (addLastName == "") {
        inValid = true;
        $('#addLastName').css('border-color', 'red');
    }

    if (addOfficeID == "") {
        inValid = true;
        $('#addOfficeID').css('border-color', 'red');
    }

    if (addPhone == "") {
        inValid = true;
        $('#addPhone').css('border-color', 'red');
    }


    if (addOfficeDesc == "") {
        inValid = true;
        $('#addOfficeDesc').css('border-color', 'red');
    }

    if (addEmail == "") {
        inValid = true;
        $('#addEmail').css('border-color', 'red');
    }

    if (!ValidEmailAddress($('#addEmail'))) {
        inValid = true;
        $('#addEmail').css('border-color', 'red');
    }

    if (addDesignation == "") {
        inValid = true;
        $('#addDesignation').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function ValidateEditOfficeManager() {
    var inValid = false;
    var editFirstname = $('#editFirstName').val().trim();
    var editLastName = $('#editLastName').val().trim();
    var editOfficeID = $('#editOfficeID').val().trim();
    var editPhone = $('#editPhone').val().trim();

    var editEmail = $('#editEmail').val().trim();
    var editOfficeDesc = $('#editOfficeDesc').val().trim();

    var editDesignation = $('#editDesignation').val().trim();

    if (editFirstname == "") {
        inValid = true;
        $('#editFirstName').css('border-color', 'red');
    }

    if (editLastName == "") {
        inValid = true;
        $('#editLastName').css('border-color', 'red');
    }

    if (editOfficeID == "") {
        inValid = true;
        $('#editOfficeID').css('border-color', 'red');
    }

    if (editPhone == "") {
        inValid = true;
        $('#editPhone').css('border-color', 'red');
    }


    if (editOfficeDesc == "") {
        inValid = true;
        $('#editOfficeDesc').css('border-color', 'red');
    }

    if (editEmail == "") {
        inValid = true;
        $('#editEmail').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}


function OfficeManagerDatatablePaging() {
    $('#UserDatatable_paginate').children('ul').addClass('flatpagi');
    $('#UserDatatable_previous').children('a').remove();
    $('#UserDatatable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#UserDatatable_next').children('a').remove();
    $('#UserDatatable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function AddNewOfficeManagerPopUpOpen() {
    $("#AddUser").modal(); return false;
}

var OfficeManagerMessageList = '';


function AlertForOfficeManager() {

    $.ajax({
        type: "POST",
        url: "/OfficeManager/AlertForOfficeManager/",
        //data: { userId: id },
        success: function (data) {
            OfficeManagerMessageList = data;
        }
    })
}

function ValidEmailAddress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};


function OfficeManagerTooltip() {
    $("#btnAddnewUser").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit Office Manager', placement: 'top' });
    $(".e-view").tooltip({ title: 'View Office Manager', placement: 'top' });
}

$(document).ready(function () {



    var cId = $('#companyId').val();
    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var table = $('#UserDatatable').DataTable({
        "ajax": {
            "url": "/OfficeManager/SelectOfficeManager",
            "type": "POST",
            "datatype": "JSON",
            'data': {
            },
        },
        'aaSorting': [[1, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        //"iDisplayLength": parseInt( hdnPageSize,10),
        "bInfo": false,
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "columns": [
        { "data": "OfficeManagerID", "name": "OfficeManagerID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "FullName",
            "name": "FirstName",
            "Searchable": true,
            "sClass": "txtAlignCenter"
            //"render": function (data, type, row) {
            //    return '<div class="user ">' + data + '</div>'
            //}
        },
        { "data": "Phone", "name": "Contact Number", "Searchable": true, "sClass": "txtAlignCenter" },
        {
            "data": "Email", "name": "Email", "Searchable": true, "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                return '<a href="javascript:void(0)" onclick="UserMessageSend(\'' + row.Email + '\')">' + data + '</a>'
            }
        },
          { "data": "Office_Desc", "name": "Office_Desc", "Searchable": true, "sClass": "txtAlignCenter", },
        { "data": "Title", "name": "Title", "Searchable": true, "sClass": "txtAlignCenter", },

        {
            "data": "OfficeManagerID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                var roleId = $('#hdnUserRoleId').val();
                if (roleId == 1 || roleId == 3) {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" id="' + data + '" data-target="#editUser" onclick="EditOfficeManager(' + data + ')"></a>' +
                        '<a class="btnsml_action e-view" href="#" data-toggle="modal" id="' + data + '" data-target="#viewUser" onclick="ViewOfficeManager(' + data + ')"></a>'
                }
                return '<a class="btnsml_action e-view" href="#" data-toggle="modal" id="' + data + '" data-target="#viewUser" onclick="ViewOfficeManager(' + data + ')"></a>'


            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            OfficeManagerDatatablePaging();
            OfficeManagerTooltip();
        },
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });

    $('#UserDatatable').on('draw.dt', function () {
        OfficeManagerDatatablePaging();
        OfficeManagerTooltip();
    });

    $('#UserDatatableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 1) {
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {

            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#editUser').modal('show');
                EditOfficeManager(parseInt(userID));
            }

        }
    });

    $('#addPhone').mask('000-000-0000');
    $('#editPhone').mask('000-000-0000');

    $('#addFirstName,#addPhone,#addEmail,#addDesignation, #addLastName, #addOfficeDesc, #addOfficeID').on('blur', function () {
        ValidateElement($(this));
    });

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtCityoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' + $('#addCity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#addState').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    var txtEditCityoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' + $('#editCity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#editState').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $("#addState").change(function (event) {
        $('#addCity').val('');
    });

    $(document).on("keydown.autocomplete", '#addCity', function (event) {
        $(this).autocomplete(txtCityoptions);
    })

    AlertForOfficeManager();

    $('#btnAddUser').click(function (event) {
        event.preventDefault();

        $('#btnAddUser').attr('disabled', 'disabled');

        if (!ValidateAddOfficeManager()) {
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }

        var isValid = true;

        var phonelen = $('#addPhone').val().length;
        if (phonelen < 12) {
            $('#addPhone').css({
                "border": "1px solid red"
            });
            $('#addPhone').focus();
            var isValid = false;
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }
        //validation for Firstname allow only alphabets
        var officefirstname = $('#addFirstName').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof officefirstname != 'undefined' && officefirstname != null && officefirstname.trim() != "") {
            if (!officefirstname.match(re)) {
                $("#addFirstName").css({
                    "border": "1px solid red"
                });
                $("#addFirstName").focus();
                fcheck = "a";
                $('#btnAddUser').removeAttr('disabled');
            }
        }
        if (fcheck == "a") {
            return false;
        }
        //validation for Lastname allow only alphabets
        var officelastname = $('#addLastName').val();
        var re = /^[a-zA-Z ]*$/;
        var lcheck = "";
        if (typeof officelastname != 'undefined' && officelastname != null && officelastname.trim() != "") {
            if (!officelastname.match(re)) {
                $("#addLastName").css({
                    "border": "1px solid red"
                });
                $("#addLastName").focus();
                lcheck = "a";
                $('#btnAddUser').removeAttr('disabled');
            }
        }
        if (lcheck == "a") {
            return false;
        }

        var formData = new FormData();

        var contact = $("#addPhone").val().replace('-', '').replace('-', '');

        formData.append("FirstName", $('#addFirstName').val().trim());
        formData.append("LastName", $('#addLastName').val().trim());
        formData.append("OfficeID", $('#addOfficeID').val().trim());
        formData.append("Phone", contact);
        formData.append("SapID", $('#addSapID').val().trim());
        formData.append("Title", $('#addDesignation option:selected').val().trim());
        formData.append("Office_Desc", $('#addOfficeDesc').val().trim());
        formData.append("Email", $('#addEmail').val().trim());
        formData.append("Street", $('#addStreetAddress').val().trim());
        formData.append("City", $('#addCity').val());
        formData.append("State", $('#addState').val());
        formData.append("Zip", $('#addZipCode').val());


        if (isValid) {

            $.ajax({
                url: '/OfficeManager/AddOfficeManager',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#btnAddUser').removeAttr('disabled');
                    if (data == 1) {

                        var addMessage = $.grep(OfficeManagerMessageList, function (e) { if (e.MessageName == 'Insert') { return e } })
                        if (addMessage != '') {
                            $('#CommonSuccessMessage').html(addMessage[0].Message);
                            $('#CommonSuccessModal').modal('show');
                        }

                        $('#AddUser').modal('hide');
                        var table = $('#UserDatatable').DataTable();
                        table.ajax.reload();
                    }

                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }


    })

    $("#editState").change(function (event) {
        $('#editCity').val('');
    });

    $(document).on("keydown.autocomplete", '#editCity', function (event) {
        $(this).autocomplete(txtEditCityoptions);
    })

    $(document).on('change', 'input[name="chkUserPermission"]', function () {
        //
        var Parentiddd = $(this).attr('parentid').split('_')[0];
        var parentidddchecked = $(this).is(":checked");


        $('input[name="chkUserPermission"]').each(function () {

            var iddd = $(this).attr('parentid').split('_');

            if (iddd[1] == Parentiddd) {
                this.checked = parentidddchecked;
            }
        });

        var Childiddd = $(this).attr('parentid').split('_')[1];
        var childidddchecked = $(this).is(":checked");
        var anysiblingsChecked = 0;
        var anysiblingsUnChecked = 0;
        var totalsiblings = 0;
        $('input[name="chkUserPermission"]').each(function () {
            var iddd = $(this).attr('parentid').split('_');
            if ((iddd[1] == Childiddd) && ($(this).is(":checked") == true)) {
                anysiblingsChecked++;
                totalsiblings++;
            }
            else if ((iddd[1] == Childiddd) && ($(this).is(":checked") == false)) {
                totalsiblings++;
                anysiblingsUnChecked++;
            }


        });


        if (childidddchecked == true) {
            if (anysiblingsChecked <= totalsiblings) {
                $('input[name="chkUserPermission"]').each(function () {
                    var parentid = $(this).attr('parentid').split('_');
                    if (Childiddd == parentid[0]) {
                        this.checked = true;
                    }

                });
            }
        }
        else {
            if (anysiblingsUnChecked == totalsiblings) {
                $('input[name="chkUserPermission"]').each(function () {
                    var parentid = $(this).attr('parentid').split('_');
                    if (Childiddd == parentid[0]) {
                        this.checked = false;
                    }

                });
            }
        }


        //$('input[name="chkUserPermission"]').each(function () {

        //    var iddd = $(this).attr('parentid').split('_');

        //    if (iddd[0] == Childiddd) {
        //        if ((anysiblingsChecked != totalsiblings) && (childidddchecked == false)) {

        //        }
        //        else if ((anysiblingsChecked == totalsiblings) && (childidddchecked == false)) {
        //            
        //            this.checked = childidddchecked;
        //        }
        //        else {
        //            
        //            this.checked = childidddchecked;
        //        }
        //    }
        //});


    });

    $('#btnUpdateUser').click(function (event) {

        event.preventDefault();

        if (!ValidateEditOfficeManager()) {
            return false;
        }

        var isValid = true;

        var phonelen = $('#editPhone').val().length;
        if (phonelen < 12) {
            $('#editPhone').css({
                "border": "1px solid red"
            });
            $('#editPhone').focus();
            var isValid = false;
            return false;
        }

        //validation for Firstname allow only alphabets
        var officefirstname = $('#editFirstName').val();
        var re = /^[a-zA-Z ]*$/;
        var fcheck = "";
        if (typeof officefirstname != 'undefined' && officefirstname != null && officefirstname.trim() != "") {
            if (!officefirstname.match(re)) {
                $("#editFirstName").css({
                    "border": "1px solid red"
                });
                $("#editFirstName").focus();
                fcheck = "a";
            }
        }
        if (fcheck == "a") {
            return false;
        }
        //validation for Lastname allow only alphabets
        var officelastname = $('#editLastName').val();
        var re = /^[a-zA-Z ]*$/;
        var lcheck = "";
        if (typeof officelastname != 'undefined' && officelastname != null && officelastname.trim() != "") {
            if (!officelastname.match(re)) {
                $("#editLastName").css({
                    "border": "1px solid red"
                });
                $("#editLastName").focus();
                lcheck = "a";
            }
        }
        if (lcheck == "a") {
            return false;
        }
        var formData = new FormData();

        var contact = $("#editPhone").val().replace('-', '').replace('-', '');

        formData.append("OfficeManagerID", $('#editofficemanagerid').val().trim());
        formData.append("FirstName", $('#editFirstName').val().trim());
        formData.append("LastName", $('#editLastName').val().trim());
        formData.append("OfficeID", $('#editOfficeID').val().trim());
        formData.append("Phone", contact);
        formData.append("SapID", $('#editSapID').val().trim());
        formData.append("Title", $('#editDesignation option:selected').val().trim());
        formData.append("Office_Desc", $('#editOfficeDesc').val().trim());
        formData.append("Email", $('#editEmail').val().trim());
        formData.append("Street", $('#editStreetAddress').val().trim());
        formData.append("City", $('#editCity').val());
        formData.append("State", $('#editState').val());
        formData.append("Zip", $('#editZipCode').val());

        $.ajax({
            url: '/OfficeManager/UpdateOfficeManager',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                var updateMessage = $.grep(OfficeManagerMessageList, function (e) { if (e.MessageName == 'Update') { return e } })
                if (updateMessage != '') {
                    $('#CommonSuccessMessage').html(updateMessage[0].Message);
                    $('#CommonSuccessModal').modal('show');
                }

                $('#editUser').modal('hide');
                var table = $('#UserDatatable').DataTable();
                table.ajax.reload();
            },
            error: function (data) {
                alert('error')
            }
        })
    });

    var userIdList = [];

    $('#btnStatusYes').click(function () {

        var UserID = $('#hdnStatusForUserID').val();
        var statusID = $('#hdnStatusValue').val();
        var UserEmail = $('#hdnStatusForUserEmail').val();

        $.ajax({
            type: "POST",
            url: "/ManageUser/UpdateUserAccountStatus",
            data: { UserID: UserID, StatusID: statusID, Email: UserEmail },
            success: function (userdetail) {
                var table = $('#UserDatatable').DataTable();
                table.ajax.reload();
            }
        })
    })

    $('#btnDeleteUser').click(function () {
        var deleteMessage = $.grep(OfficeManagerMessageList, function (e) { if (e.MessageName == 'Delete') { return e } })
        if (deleteMessage != '') {
            $('#userDeleteMessage').html(deleteMessage[0].Message);
        }
        $('#userDeleteConfirmModel').modal('show');
    })

    $('#btnDeleteYes').click(function () {

        var OfficeManagerID = $('#editofficemanagerid').val();
        DeleteOfficeManager(OfficeManagerID);

        $('#editUser').modal('hide');
        $('#userDeleteConfirmModel').modal('hide');
    })

    $('#editUserclickfromModal').click(function () {

        $('#ViewDetail').modal('hide');
        var OfficeManagerID = $('#editofficemanagerid').val();

        EditOfficeManager(OfficeManagerID);
        $('#editUser').modal('show');
    })

    $('.dataTables_length').hide();

    $('#UserDatatable_filter').hide();

    $('#addFirstName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $('#addLastName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });

    $('#editFirstName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $('#editLastName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });



})