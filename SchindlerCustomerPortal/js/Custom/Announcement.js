﻿function editAnnouncement(id) {
    $('#editdisplayon option').removeAttr('selected');
    $('#editfrequency option').removeAttr('selected');
   
    $.ajax({
        type: "GET",
        url: "/AnnouncementMessage/EditAnnouncement/",
        data: { AnnMsgId: id },
        success: function (Announcementdetails) {

            $('#AnnouncementId').val(Announcementdetails.AnnMsgId);
            //$('#edittitle').val(Announcementdetails.Title);
            CKEDITOR.instances['editannouncementmsg'].setData(Announcementdetails.MessageText);
            $('#editfrequency option[value="' + Announcementdetails.Frequency + '"]').prop('selected', true);
            $('#editdisplayon option[value="' + Announcementdetails.DisplayOn + '"]').prop('selected', true);
            $('#edituptodate').val(Announcementdetails.ValidDate);

        }
    })
    $('#cke_2_contents').removeAttr('style');
    $('#cke_2_contents').attr('style', 'height: 165px;');
}
function viewBroadcast(id)
{
    $("#viewBroadcast").attr("disabled", "disabled");
    $.ajax({
        type: "GET",
        url: "/AnnouncementMessage/EditAnnouncement/",
        data: { AnnMsgId: id },
        success: function (Announcementdetails) {
            $('#broadcastId').val(Announcementdetails.AnnMsgId);
            var tmp = document.createElement("DIV");
            tmp.innerHTML = Announcementdetails.MessageText;
            $('#viewBroadcast').val(tmp.innerText);
            $('#Broadcastfrequency option[value="' + Announcementdetails.Frequency + '"]').prop('selected', true);
        }
    })
}
function AnnouncementDatatablePaging() {
    $('#Announcementtable_paginate').children('ul').addClass('flatpagi');
    $('#Announcementtable_previous').children('a').remove();
    $('#Announcementtable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Announcementtable_next').children('a').remove();
    $('#Announcementtable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

$('#btndeleteAnnouncement').click(function () {
    $('#announcementDeleteConfirmModel').modal('show');
});

$('#btnDeleteYes').click(function () {

    var AnnouncementID = $('#AnnouncementId').val();

    $.ajax({
        type: "POST",
        url: "/AnnouncementMessage/DeleteAnnouncementData/",
        data: { AnnMsgId: AnnouncementID },
        success: function (Announcementdetails) {

            $('#editAnnouncement').modal('hide');
            var table = $('#Announcementtable').DataTable();
            table.ajax.reload();
        }
    })

})

function AnnouncementTooltip() {
    $("#addannouncementTooltip").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit Announcement', placement: 'top' });
}

$(function () {
    

    CKEDITOR.replace('addannouncementmsg');
    CKEDITOR.replace('editannouncementmsg');

    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var table = $('#Announcementtable').DataTable({

        "ajax": {
            "url": "/AnnouncementMessage/GetAllAnnouncement",
            "type": "POST",
            "datatype": "JSON",
            "async":"false"

        },

        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        'aaSorting': [[1, 'asc']],
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "columns": [
      { "data": "AnnMsgId", "name": "AnnMsgId", "Searchable": true, "sClass": "colHide txtAlignCenter" },
         {
             "data": "MessageText", "name": "MessageText",
             "render": function (data, type, row) {
                 return data.substr(0, 50)
             }
         },
        {
            "data": "Frequency", "name": "Frequency", "Searchable": true, "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                if(data.toLowerCase() == "multipletime"){
                    return "Every Time";
                } else if(data.toLowerCase() == "onetime"){
                    return "One Time";
                } else {
                    return data;
                }
            }
        },
         {
             "data": "DisplayOn", "name": "DisplayOn", "Searchable": true, "sClass": "txtAlignCenter",
             "render": function (data, type, row) {
                 if (data.toLowerCase() == "beforelogin") {
                     return "Before Login";
                 } else if (data.toLowerCase() == "afterlogin") {
                     return "After Login";
                 } else {
                     return data;
                 }
             }
         },
          { "data": "ValidDate", "name": "ValidDate", "Searchable": true, "sClass": "txtAlignCenter" },
        {
            "data": "AnnMsgId",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {

                if (row.Frequency == 'Broadcast') {
                    return '<a href="#" class="btnsml_action e-edit" data-toggle="modal"  data-target="#editBroadcast"  id="' + data + '" onclick="viewBroadcast(' + data + ')" ></a>';
                }
                else {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" id="' + data + '" data-target="#editAnnouncement" onclick="editAnnouncement(' + data + ')"> </a> '
                }

            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            AnnouncementDatatablePaging();
            AnnouncementTooltip();
        },
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }


    });

    $('#Announcementtable').on('draw.dt', function () {
        AnnouncementDatatablePaging();
        AnnouncementTooltip();
    });

    $('.dataTables_length').hide();

    $('#Announcementtable_filter').hide();

    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {

            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var AnnouncementID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                if ($(clickedRow).find('td')[2].innerText == 'Broadcast')
                {
                    $('#editBroadcast').modal('show');
                    viewBroadcast(parseInt(AnnouncementID));
                }
                else
                {
                    $('#editAnnouncement').modal('show');
                    editAnnouncement(parseInt(AnnouncementID));
                }

            }

        }
    });

    $('#AnnouncementtableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 2) {

            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    $('#btnAddAnnouncement').click(function (event) {
        event.preventDefault();

        var isValid = true;

        if (!ValidateAddAnnouncement()) {
            $('#btnAddAnnouncement').removeAttr('disabled');
            return false;
        }
        var AddDescData = CKEDITOR.instances['addannouncementmsg'].getData().trim();

        if ($.trim(AddDescData) == '') {
            isValid = false;
            $('#cke_1_contents').css({
                "border": "1px solid red"
            });
            $('#btnAddAnnouncement').removeAttr('disabled');
        }
        else {

            $('#cke_1_contents').css({
                "border": "",
                "background": ""
            });
        }

        var ele = $('input[class="form-control required"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();

            }
            else {
            }
        }

        if (isValid == false) {
            $('#btnAddAnnouncement').removeAttr('disabled');
            return false;
        }

        var obj = new Object;
        
        obj.MessageText = AddDescData;
        obj.Frequency = $('#addfrequency').val();
        obj.DisplayOn = $('#adddisplayon').val();
        obj.ValidDate = $('#Adduptodate').val();
        
        if ($('#addfrequency').val() == 'Broadcast') {
            $('#announcementSubmitConfirmModel').modal('show');
            return;
        }
      
        if (isValid) {
            AnnouncementMessage(obj);
        }
        else {
            $('#btnAddAnnouncement').removeAttr('disabled');
            return false;
        }

    });

    $('#btnUpdateAnnouncement').click(function (event) {
        event.preventDefault();

        $('#btnUpdateAnnouncement').attr('disabled', 'disabled');
        var isValid = true;

        //$('input[class="form-control edithelprequired"]').each(function () {

        //    if ($.trim($(this).val()) == '') {
        //        isValid = false;
        //        $(this).css({
        //            "border": "1px solid red"
        //        });

        //    }
        //    else {

        //        $(this).css({
        //            "border": "",
        //            "background": ""
        //        });
        //    }
        //});
        if (!ValidateEditAnnouncement()) {
            $('#btnUpdateAnnouncement').removeAttr('disabled');
            return false;
        }
        var EditDescData = CKEDITOR.instances['editannouncementmsg'].getData().trim();
        if ($.trim(EditDescData) == '') {
            isValid = false;
            $('#cke_2_contents').css({
                "border": "1px solid red"
            });
        }
        else {

            $('#cke_2_contents').css({
                "border": "",
                "background": ""
            });
        }
        var ele = $('input[class="form-control required"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();

            }
            else {
            }
        }
        if (isValid == false) {
            $('#btnUpdateAnnouncement').removeAttr('disabled');
            return false;
        }
       

        var obj = new Object;
        obj.AnnMsgId = $('#AnnouncementId').val();
        obj.MessageText = EditDescData;
        obj.Frequency = $('#editfrequency').val();
        obj.DisplayOn = $('#editdisplayon').val();
        obj.ValidDate = $('#edituptodate').val();


        $.ajax({
            url: '/AnnouncementMessage/UpdateAnnouncement',
            method: 'POST',
            data: obj,
            //cache: false,
            //contentType: false,
            //processData: false,
            success: function (data) {

                $('#editAnnouncement').modal('hide');

                $('#CommonSuccessMessage').html('Record updated successfully');
                $('#CommonSuccessModal').modal('show');

                //$('#SuccessEditHelp').modal('show');
                var table = $('#Announcementtable').DataTable();
                table.ajax.reload();
                $('#btnUpdateAnnouncement').removeAttr('disabled');

            },
            error: function (data) {
                $('#btnUpdateAnnouncement').removeAttr('disabled');
            }
        })
    });

    $('#addannouncementTooltip').click(function (event) {
        $('#cke_1_contents').removeAttr('style');
        $('#cke_1_contents').attr('style', 'height: 165px;');
    });

    $('#AddAnnouncement .close').click(function () {
       
        CKEDITOR.instances['addannouncementmsg'].setData("");

    })
    $('#btnaddcancleAnnouncement').click(function () {
       
        CKEDITOR.instances['addannouncementmsg'].setData("");
    })

    $('#btnBroadcastYes').click(function () {
        event.preventDefault();

        var isValid = true;

        if (!ValidateAddAnnouncement()) {
            $('#btnAddAnnouncement').removeAttr('disabled');
            return false;
        }
        var AddDescData = CKEDITOR.instances['addannouncementmsg'].getData().trim();

        if ($.trim(AddDescData) == '') {
            isValid = false;
            $('#cke_1_contents').css({
                "border": "1px solid red"
            });
            $('#btnAddAnnouncement').removeAttr('disabled');
        }
        else {

            $('#cke_1_contents').css({
                "border": "",
                "background": ""
            });
        }

        var ele = $('input[class="form-control required"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();

            }
            else {
            }
        }

        if (isValid == false) {
            $('#btnAddAnnouncement').removeAttr('disabled');
            return false;
        }

        var obj = new Object;

        obj.MessageText = AddDescData;
        obj.Frequency = $('#addfrequency').val();
        obj.DisplayOn = $('#adddisplayon').val();
        obj.ValidDate = $('#Adduptodate').val();
        AnnouncementMessage(obj);

        var conn = $.connection.myHub;
        $.connection.hub.start().done(function () {
            conn.server.sendMessage(CKEDITOR.instances['addannouncementmsg'].getData().trim());
        });
    })

    $('#btnbroadcastDeleteYes').click(function () {
        var annMsgId = $('#broadcastId').val();
        $.ajax({
            type: "POST",
            url: "/AnnouncementMessage/DeleteAnnouncementData/",
            data: { AnnMsgId: annMsgId },
            success: function (Announcementdetails) {
                var table = $('#Announcementtable').DataTable();
                table.ajax.reload();
            }
        });
        $('#editBroadcast').modal('hide');
    })
});

function ValidateAddAnnouncement() {
    var inValid = false;
    var addMessage = CKEDITOR.instances['addannouncementmsg'].getData().trim();

    var frequency = $('#addfrequency').val().trim();
    var displayon = $('#adddisplayon').val().trim();
    var validdate = $('#Adduptodate').val().trim();
    

    if (addMessage == "") {
        inValid = true;
        $('#cke_1_contents').css({
            "border": "1px solid red"
        });
    }
    else {
      
        $('#cke_1_contents').css({
            "border": ""
        });
    }
    if (frequency == "") {
        inValid = true;
        $('#addfrequency').css('border-color', 'red');
    }
    else {
        
        $('#addfrequency').css('border-color', '');
    }
    if (displayon == "") {
        inValid = true;
        $('#adddisplayon').css('border-color', 'red');
    }
    else {
        
        $('#adddisplayon').css('border-color', '');
    }

    if (validdate == "") {
        inValid = true;
        $('#Adduptodate').css('border-color', 'red');
    }
    else {
        
        $('#Adduptodate').css('border-color', '');
    }
    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function ValidateEditAnnouncement() {
    var inValid = false;
    var editMessage = CKEDITOR.instances['editannouncementmsg'].getData().trim();

    var editfrequency = $('#editfrequency').val().trim();
    var editdisplayon = $('#editdisplayon').val().trim();
    var editvaliddate = $('#edituptodate').val().trim();


    if (editMessage == "") {
        inValid = true;
        $('#cke_2_contents').css({
            "border": "1px solid red"
        });
    }
    else {
        $('#cke_2_contents').css({
            "border": "",
            "background": ""
        });
    }
    if (editfrequency == "") {
        inValid = true;
        $('#editfrequency').css('border-color', 'red');
    }
    else {
       
        $('#editfrequency').css('border-color', '');
    }
    if (editdisplayon == "") {
        inValid = true;
        $('#editdisplayon').css('border-color', 'red');
    }
    else {
        
        $('#editdisplayon').css('border-color', '');
    }

    if (editvaliddate == "") {
        inValid = true;
        $('#edituptodate').css('border-color', 'red');
    }
    else {
        
        $('#edituptodate').css('border-color', '');
    }
    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function changeaddfrequency()
{
    if ($('#addfrequency').val() == 'Broadcast') {
        $('#adddisplayon').val('AfterLogin');
        $('#divDisplayOn,#divValidTillDate').hide();
        $('#Adduptodate').datepicker("setDate", new Date());
    }
    else {
        $('#adddisplayon').val('');
        $('#divDisplayOn,#divValidTillDate').show();
        $('#Adduptodate').datepicker("setDate", "");
    }
}

function AnnouncementMessage(obj)
{
    $.ajax({
        url: '/AnnouncementMessage/AddAnnouncementData',
        method: 'POST',
        data: obj,
        dataType: 'json',
        success: function (data) {
            $('#btnAddAnnouncement').removeAttr('disabled');
            $('#AddAnnouncement').modal('hide');

            $('#CommonSuccessMessage').html('Record added successfully');
            $('#CommonSuccessModal').modal('show');

            var table = $('#Announcementtable').DataTable();
            table.ajax.reload();
            CKEDITOR.instances['addannouncementmsg'].setData("");
            $('#btnAddAnnouncement').removeAttr('disabled');
        },
        error: function (data) {
            $('#btnAddAnnouncement').removeAttr('disabled');
        }
    })
}

function DeleteBroadCastMessage()
{
    $('#broadcastId').val($('#broadcastId').val());
    $('#broadcastDeleteConfirmModel').modal('show');
}

