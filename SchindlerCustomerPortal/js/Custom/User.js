﻿var EditUserCompanyId = '';
var EditUserRoleId = '';
var subPermissionCount = 0;
var IsDeleted = 0;

BindStateForUser();

function SetNoImage(tagId1, tagId2, Operation) {
    $('#' + tagId1).val('');
    $('#' + tagId2).attr('src', '/images/ProfilePics/no_images.png');

    if(Operation == "Edit"){
        IsDeleted = 1;
    }
    else {
        IsDeleted = 0;
    }
}

function EditUser(id) {

    $.ajax({
        type: "GET",
        url: "/ManageUser/UserEdit/",
        data: { userId: id },
        async: false,
        success: function (userdetail) {

            $('#edituserid').val(userdetail.UserID);
            $('#editFullname').val(userdetail.FirstName);

            $('.hdnUserFisrtName').html(userdetail.FirstName);

            $('#editphone').val(userdetail.ContactNumber);
            
            $('#editrequestedprojects').val(userdetail.Requested_Projects);
            $('#editemail').val(userdetail.Email);

            $('#editUserpic').attr('src', userdetail.ProfilePic);
            $('#editpicFilename').val(userdetail.ProfilePic);

            $('#editStreetAddress').val(userdetail.Address1);
            $('#editState').val(userdetail.State);

            $('#editCity').val(userdetail.City);
            $('#editZipCode').val(userdetail.PostalCode);

            $("#editDesignation").val(userdetail.DesignationId);

            $('#editStatus').val(userdetail.AccountStatus);

            //if ($('#hdnCheckUsermanagement').val() == 'Employee Management') {
            //    $('#divProfileUnsubscribeEdit').css('display', 'none');
            //}
            //else {
            //    $('#divProfileUnsubscribeEdit').css('display', 'block');
            //    $('#profileUnsubscribeEdit').prop('checked', userdetail.Unsubscribe);
            //}

           
            var verify = 17;
            var approved = 18;
            var pending = 4;
            var active = 1;
            var disapproved = 19;

            if (userdetail.AccountStatus == 4) {
                $('#editStatus option[value="' + verify + '"]').prop('disabled', true);
                $('#editStatus option[value="' + approved + '"]').prop('disabled', true);
                $('#editStatus option[value="' + active + '"]').prop('disabled', true);
            }
            //else {
            //    $('#editStatus option[value="' + verify + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + approved + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + active + '"]').prop('disabled', false);
            //}

            if (userdetail.AccountStatus == 17) {
                $('#editStatus option[value="' + pending + '"]').prop('disabled', true);
                $('#editStatus option[value="' + active + '"]').prop('disabled', true);
            }
            //else {
            //    $('#editStatus option[value="' + pending + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + active + '"]').prop('disabled', false);
            //}

            if (userdetail.AccountStatus == 1) {
                $('#editStatus option[value="' + pending + '"]').prop('disabled', true);
                $('#editStatus option[value="' + verify + '"]').prop('disabled', true);
                $('#editStatus option[value="' + approved + '"]').prop('disabled', true);
                $('#editStatus option[value="' + disapproved + '"]').prop('disabled', true);
            }
            //else {
            //    $('#editStatus option[value="' + pending + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + verify + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + approved + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + disapproved + '"]').prop('disabled', false);
            //}

            EditUserCompanyId = userdetail.CompanyID;
            EditUserRoleId = userdetail.RoleID;

            BindUserModulePermission(id);
        }
    })
}

function EditUserEmployee(id) {

    $.ajax({
        type: "GET",
        url: "/ManageUser/UserEdit/",
        data: { userId: id },
        //async: false,
        success: function (userdetail) {

            $('#edituserid').val(userdetail.UserID);
            $('#editFullname').val(userdetail.FirstName);

            $('.hdnUserFisrtName').html(userdetail.FirstName);

            $('#editphone').val(userdetail.ContactNumber);
            $('#editemail').val(userdetail.Email);

            $('#editUserpic').attr('src', userdetail.ProfilePic);
            $('#editpicFilename').val(userdetail.ProfilePic);

            $('#editStreetAddress').val(userdetail.Address1);
            $('#editState').val(userdetail.State);

            $('#editCity').val(userdetail.City);
            $('#editZipCode').val(userdetail.PostalCode);

            $("#editDesignation").val(userdetail.DesignationId);

            $('#editStatus').val(userdetail.AccountStatus);

            EditUserCompanyId = userdetail.CompanyID;
            EditUserRoleId = userdetail.RoleID;

        }
    })
}

function DeleteUser(id) {

    $.ajax({
        type: "POST",
        url: "/ManageUser/UserDelete/",
        data: { userID: id },
        success: function (userdetail) {

            var table = $('#UserDatatable').DataTable();
            table.ajax.reload();
        }
    })
}

function updateusermodulepermission(userid, moduleid, HasPermission, Type, rowid) {

    if ($('#' + rowid).is(':checked')) {
        HasPermission = true
    }
    else {
        HasPermission = false
    }


    // for sub child
    //if (HasPermission) {
    //    $('input[name="chkUserPermission"]').each(function () {
    //        var iddd = $(this).attr('parentid').split('_');

    //        if (iddd[1] == moduleid) {
    //            this.checked = true;
    //        }

    //    });
    //}

    $.ajax({
        type: "POST",
        url: "/ManageUser/UpdateUserAccessPermission/",
        data: { UserID: userid, ModuleId: moduleid, HasPermission: HasPermission, Type: Type },
        success: function (response) {

            var hdnloginUserid = $('#hdnLoggedUserId').val();
            if (hdnloginUserid == userid) {
                //alert('you have updated your own permission. so please login again!')
                $('#CommonSuccessMessage').html('you have updated your own permission. so please login again!');
                $('#CommonSuccessModal').modal('show');
                window.location.href = '/login/Logout'
            }

        },
        error: function (err) {
            alert(err.message);
        }
    })


}

function BindUserModulePermission(id) {

    $.ajax({
        type: "Get",
        url: "/ManageUser/UserModulePermission",
        data: { userID: id },
        success: function (userdetail) {

            $('#tree1').html('');

            var renderHTML = '';
            var moduleId = 0;

            $.each(userdetail.data, function (i, val) {

                moduleId = val.ModuleId;

                if (val.ParentID == 0) {
                    var rowEditPermission = (val.HasEditPermission == 1) ? false : true;
                    var rowChecked = (val.HasViewPermission == true) ? 'checked' : 'unchecked';

                    //renderHTML += '<li><input type="checkbox" ' + rowChecked + ' parentID = ' + val.ModuleId + '_' + 0 + ' class="icheckbox" name="chkUserPermission" id="chkUserPermission_' + val.UserID + '_' + val.ModuleId + '" onclick="updateusermodulepermission(' + val.UserID + ',' + val.ModuleId + ',' + rowEditPermission + ',this.name,this.id)"/> <a href="#">' + val.ModuleName + '</a>';
                    renderHTML += '<li><input type="checkbox" ' + rowChecked + ' parentID = ' + val.ModuleId + '_' + 0 + ' class="icheckbox" name="chkUserPermission" id="chkUserPermission_' + val.UserID + '_' + val.ModuleId + '" /> <a href="#">' + val.ModuleName + '</a>';

                    var returnResult = GenerateUserPermission(userdetail.data, moduleId);

                    if (subPermissionCount == 1) {
                        renderHTML += returnResult;
                    }

                    renderHTML += '</li>';
                }

            })

            $('#tree1').append(renderHTML);


        },
        error: function (data) {

        }
    })

}

function GenerateUserPermission(data, id) {
    subPermissionCount = 0;

    var renderHTML = '';

    renderHTML += '<ul>';

    $.each(data, function (i, val) {
        if (val.ParentID == id) {

            subPermissionCount = 1;
            var rowEditPermission = (val.HasEditPermission == 1) ? false : true;
            var rowChecked = (val.HasViewPermission == true) ? 'checked' : 'unchecked';

            //renderHTML += '<li><input type="checkbox" ' + rowChecked + ' parentID = ' + val.ModuleId + '_' + id + ' class="icheckbox" name="chkUserPermission" id="chkUserPermission_' + val.UserID + '_' + val.ModuleId + '" onclick="updateusermodulepermission(' + val.UserID + ',' + val.ModuleId + ',' + rowEditPermission + ',this.name,this.id)"/> <a href="#">' + val.ModuleName + '</a>';
            renderHTML += '<li><input type="checkbox" ' + rowChecked + ' parentID = ' + val.ModuleId + '_' + id + ' class="icheckbox" name="chkUserPermission" id="chkUserPermission_' + val.UserID + '_' + val.ModuleId + '" /> <a href="#">' + val.ModuleName + '</a>';
        }
    })

    renderHTML += '</ul>';

    return renderHTML;
}

function ViewUserDetail(id) {

    $.ajax({
        type: "GET",
        url: "/ManageUser/UserEdit/",
        data: { userId: id },
        success: function (userdetail) {

            var name = userdetail.FirstName + ' ' + userdetail.LastName;
            var role = (userdetail.RoleID == 3) ? 'Admin User' : 'Normal User';

            $('#edituserid').val(userdetail.UserID);

            $('#userfullname').html(name);
            $('#userrole').html(role);
            $('#usercontact').html(userdetail.ContactNumber);
            $('#useremail').html(userdetail.Email);
            $('#userimage').attr('src', userdetail.ProfilePic);
            BindUserModulePermission(id);

        }
    })
}

function ValidateAddUser() {
    var inValid = false;
    var addFirstname = $('#addFullname').val().trim();

    var addPhone = $('#addPhone').val().trim();
    var addEmail = $('#addEmail').val().trim();
    var addProfile = $('#addProfile').val().trim();
    var addDesignation = $('#addDesignation').val().trim();

    if (addFirstname == "") {
        inValid = true;
        $('#addFullname').css('border-color', 'red');
    }

    if (addPhone == "") {
        inValid = true;
        $('#addPhone').css('border-color', 'red');
    }

    if (addEmail == "") {
        inValid = true;
        $('#addEmail').css('border-color', 'red');
    }

    if (!ValidEmailAddress($('#addEmail'))) {
        inValid = true;
        $('#addEmail').css('border-color', 'red');
    }

    if (addDesignation == "") {
        inValid = true;
        $('#addDesignation').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function ValidateEditUser() {
    var inValid = false;
    var editFirstname = $('#editFullname').val().trim();

    var editPhone = $('#editphone').val().trim();
    var editEmail = $('#editemail').val().trim();

    var editDesignation = $('#editDesignation').val().trim();

    if (editFirstname == "") {
        inValid = true;
        $('#editFullname').css('border-color', 'red');
    }

    if (editPhone == "") {
        inValid = true;
        $('#editphone').css('border-color', 'red');
    }

    if (editDesignation == "") {
        inValid = true;
        $('#editDesignation').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function UserStatusChange(statusID, UserID, userEmail) {
    $('#hdnStatusForUserID').val(UserID);

    if (statusID == 1) {
        // statusID 1 = Active or Approved
        $('#statusText').html('Are you sure you want to deactive this user?')
        $('#hdnStatusValue').val('2');
        $('#userStatusModel').modal('show');
    }
    if (statusID == 2) {
        // statusID 2 = InActive
        $('#statusText').html('Are you sure you want to active this user?')
        $('#hdnStatusValue').val('1');
        $('#hdnStatusForUserEmail').val(userEmail);
        $('#userStatusModel').modal('show');
    }
    if (statusID == 4) {
        // statusID 4 = pending
    }
    if (statusID == 17) {
        // statusID 17 = Verify
        $('#statusText').html('Are you sure you want to approved this user?')
        $('#hdnStatusValue').val('18'); // statusID 18 = Approved
        $('#hdnStatusForUserEmail').val(userEmail);
        $('#userStatusModel').modal('show');
    }
    if (statusID == 18) {
        // statusID 18 = Approved
    }
    if (statusID == 19) {
        // statusID 19 = Disapproved
    }
}

function UserDatatablePaging() {
    $('#UserDatatable_paginate').children('ul').addClass('flatpagi');
    $('#UserDatatable_previous').children('a').remove();
    $('#UserDatatable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#UserDatatable_next').children('a').remove();
    $('#UserDatatable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function UserMessageSend(mail) {
    window.location.href = '/mailbox/maillist?to=' + mail;
}

function AddNewUserPopUpOpen() {
    //if ($('#hdnCheckUsermanagement').val() == 'Employee Management') {
    //    $('#divProfileUnsubscribeAdd').css('display','none');
    //}
    //else {
    //    $('#divProfileUnsubscribeAdd').css('display', 'block');
    //}
    $("#AddUser").modal(); return false;
}

var UserManagementMessageList = '';

function AlertMessageUserManagement() {

    $.ajax({
        type: "POST",
        url: "/ManageUser/AlertForUserManagement/",
        //data: { userId: id },
        success: function (data) {
            UserManagementMessageList = data;
        }
    })
}

function ValidEmailAddress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};

function EditProjectAssign(id) {

    $('#edituserid').val(id);

    EditUser(id);
    var term = "";
    GetAllProjectAssign(term,id);

    //var isValid = true;
    //if (isValid) {
    //    $.ajax({
    //        type: "GET",
    //        url: "/ManageUser/EditProjectAssign/",
    //        data: { userId: id },
    //        success: function (userdetail) {

    //            //$('#updateProjectAssignListbox').html("");

    //            $.each(userdetail, function (i, val) {
    //                var setCheckbox = "chkProjectAssign_" + val.ProjectNo;
    //                $("#" + setCheckbox).attr('checked', 'checked');
    //            });

    //            //$.each(userdetail, function (i, val) {
    //            //    var concateProjectNo = val.ProjectNo + " - " + val.ProjectName;
    //            //    $('#updateProjectAssignListbox').append($("<option/>", ($({ value: val.ProjectNo, text: concateProjectNo }))));
    //            //});

    //        }
    //    })
    //}
    //else {
    //    return false;
    //}

}

function UserTooltip() {
    $("#btnAddnewUser").tooltip({ title: 'Add New', placement: 'top' });
    if ($('#hdnCheckUsermanagement').val() == 'User Management') {
        $(".e-edit").tooltip({ title: 'Edit User', placement: 'top' });
    }
    else {
        $(".e-edit").tooltip({ title: 'Edit Employee', placement: 'top' });
    }
    $(".e-communicate").tooltip({ title: 'Message', placement: 'top' });
    $(".e-projectassign").tooltip({ title: 'Project Assign', placement: 'top' });
}

function GetAllProjectAssign(term,id) {

    //var term = "";

    var isValid = true;
    if (isValid) {
        $.ajax({
            type: "Post",
            url: "/ManageUser/ProjectNameAutoCompleteByCompanyId/",
            data: { searchTerm: term, companyId: EditUserCompanyId, RoleId: EditUserRoleId, UserId: id, },
            //contentType: "application/json; charset=utf-8",
            //dataType: 'json',
            async: false,
            success: function (userdetail) {

                $('#tblProjectAssign').html("")
                var renderHTML = '';
                $.each(userdetail, function (i, val) {

                    var concateProjectNo = val.ProjectNo + " - " + val.ProjectName;
                    renderHTML += "<tr>";
                    renderHTML += "<td class='text-left'><input type='checkbox' " + val.CheckAssigned + " id=chkProjectAssign_" + val.ProjectNo + " value=" + val.ProjectNo + "></td>";
                    if ((val.IsUnSubscribedUserMst)) {
                        renderHTML += "<td style='text-align:center;'><input type='checkbox'  disabled   id=chkUnsubscribe_" + val.ProjectNo + " value=" + val.ProjectNo + "></td>";
                    }
                    else {
                        renderHTML += "<td style='text-align:center;'><input type='checkbox'  "+ val.UnSubscribedCheckAssigned+ " id=chkUnsubscribe_" + val.ProjectNo + " value=" + val.ProjectNo + "></td>";
                    }
                   
                    renderHTML += "<td>" + concateProjectNo + "</td></tr>";
                });

                $('#tblProjectAssign').append(renderHTML);

                //$('#tblProjectAssign').mCustomScrollbar();
                //$('#updateProjectAssignListbox').html("");

                //$.each(userdetail, function (i, val) {
                //    var concateProjectNo = val.ProjectNo + " - " + val.ProjectName;
                //    $('#updateProjectAssignListbox').append($("<option/>", ($({ value: val.ProjectNo, text: concateProjectNo }))));
                //});

            }
        })
    }
    else {
        return false;
    }

}

function EditProjectAssignForEmployee(id,companyId,roleId) {

    $('#edituserid').val(id);

    //EditUser(id);
    EditUserEmployee(id);
    var term = "";
    //GetAllProjectAssign(term);

    var table = $('#dt_tblProjectAssign').DataTable();
    table.destroy();

    var table = $('#dt_tblProjectAssign').DataTable({
        "ajax": {
            "url": "/ManageUser/ProjectNameAutoCompleteByCompanyId",
            "type": "POST",
            "datatype": "JSON",
            //"async": false,
            'data': {
                searchTerm: term,
                companyId: companyId,
                RoleId: roleId,
                UserId: id,
            },
        },
        "bPaginate": false,
        "autoWidth": false, // for disabling autowidth
        "columnDefs": [
            { width: "0px", targets: "_all", className: "dt-center" }
        ],
        "filter": true, // this is for disable filter (search box)
        "bInfo": false,
        "processing": true,
        'aaSorting': [[3, 'asc']],
        "oLanguage": {
            "sSearch": "",
            "sSearchPlaceholder": "Search",
            "sEmptyTable": "No data available"
        },
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "columns": [

            {
                "data": "ProjectNo", "name": "ProjectNo",
                "render": function (data, type, row) {
                    
                    return "<input type='checkbox' " + row.CheckAssigned + "  id=chkProjectAssign_" + row.ProjectNo + " value=" + row.ProjectNo + ">"
                }
            },
            {
                "data": "ProjectNo", "name": "IsUnsubscribe",
                "render": function (data, type, row) {
                    if ((row.IsUnSubscribedUserMst)) {
                        return "<input type='checkbox' disabled >"
                    }
                    else {
                        return "<input type='checkbox' " + row.UnSubscribedCheckAssigned + "   id=chkUnsubscribe_" + row.ProjectNo + " value=" + row.ProjectNo + ">"
                    }
                },
                "bSortable": false,
             },
        { "data": "Company", "name": "CompanyName", "Searchable": true },
        { "data": "ProjectNo", "name": "ProjectNo", "Searchable": true },
        { "data": "ProjectName", "name": "ProjectName", "Searchable": true },
        {
            "data": "ProjectNo",
            "bSortable": false,
            "render": function (data, type, row) {
                return ""
            }
        },
        ],
    });

    $('.dataTables_length').hide();
    var column = table.column(1);
   
    if ($('#hdnCheckUsermanagement').val() == 'Employee Management') {
        column.visible(false);
    }
    else {
        //$('#divProfileUnsubscribeEdit').css('display', 'block');
        column.visible(true);
    }
    //$('#dt_tblProjectAssign_filter').hide();

    //$.ajax({
    //    type: "GET",
    //    url: "/ManageUser/EditProjectAssign/",
    //    data: { userId: id },
    //    success: function (userdetail) {

    //        $.each(userdetail, function (i, val) {
    //            var setCheckbox = "chkProjectAssign_" + val.ProjectNo;
    //            $("#" + setCheckbox).attr('checked', 'checked');
    //        });

    //    }
    //})


}

$(document).ready(function () {

    var cId = $('#companyId').val();
    var hdnPageSize = parseInt($('#hdnPageSize').val());
    //$('#wait').css('display', 'block'); // for ajax loader
    var table = $('#UserDatatable').DataTable({
        "ajax": {
            "url": "/ManageUser/SelectUser",
            "type": "POST",
            "datatype": "JSON",
            'data': {
                companyId: cId,
            },
        },
        'aaSorting': [[1, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "bInfo": false,
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        
        "columns": [
        { "data": "UserID", "name": "UserID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "FirstName",
            "name": "FirstName",
            "Searchable": true,
            "render": function (data, type, row) {
                 return '<div class="user"><img src="' + row.ProfilePic + '">' + data + '</div>'
               //return '<div class="user "><img src="/images/ProfilePics/ahghg.jpg">' + data + '</div>'
            }
        },
        { "data": "ContactNumber", "name": "ContactNumber", "Searchable": true, "sClass": "txtAlignCenter" },
        {
            "data": "Email", "name": "Email", "Searchable": true,
            "render": function (data, type, row) {
                // return '<a href="javascript:void(0)" onclick="UserMessageSend(\'' + row.Email + '\')">' + data + '</a>'
                return '<a href="mailto:' + data + '?subject=MySchindlerProjects" >' + data + '</a>'
               
            }
        },
        { "data": "Designation", "name": "DesignationName", "Searchable": true },
        {
            "data": "AccountStatus",
            "name": "AccountStatus",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {

                if (data == 4) {
                    return '<span class="label pending">Pending</span>'
                }
                if (data == 1) {
                    return '<span class="label complete">Active</span>'
                }
                if (data == 2) {
                    return '<span class="custombtn progresss">InActive</span>'
                }
                if (data == 17) {
                    return '<span class="label progresss">Verified</span>'
                }
                if (data == 18) {
                    return '<span class="label progresss">Approved</span>'
                }
                if (data == 19) {
                    return '<span class="label progresss">Disapproved</span>'
                }
            }
        },
        {
            "data": "UserID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {

                var rowViewPermission = (row.HasViewPermission == 1) ? false : true;

                var checkUsermanagement = $('#hdnCheckUsermanagement').val();

                if (checkUsermanagement == "User Management") {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" id="' + data + '" data-target="#editUser" onclick="EditUser(' + data + ')"></a>' +
               // '<a href="#" class="btnsml_action e-communicate" data-toggle="modal" data-target="#ViewDetail" onclick="UserMessageSend(\'' + row.Email + '\')"></a>' +
                    '<a class="btnsml_action e-projectassign" href="#" data-toggle="modal" data-target="#ProjectAssignModal" onclick="EditProjectAssign(' + data + ')"></a>'
                }
                else {
                    return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" id="' + data + '" data-target="#editUser" onclick="EditUser(' + data + ')"></a>' +
                //'<a href="#" class="btnsml_action e-communicate" data-toggle="modal" data-target="#ViewDetail" onclick="UserMessageSend(\'' + row.Email + '\')"></a>' +
                        '<a class="btnsml_action e-projectassign" href="#" data-toggle="modal" data-target="#ProjectAssignModalForEmployee" onclick="EditProjectAssignForEmployee(' + data + ',' + row.CompanyID + ','+row.RoleID+')"></a>'
                }

            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            UserDatatablePaging();
            UserTooltip();
        },
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });

    $('#UserDatatable').on('draw.dt', function () {
        UserDatatablePaging();
        UserTooltip();
    });

    $('#UserDatatableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 2) {
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {

            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#editUser').modal('show');
                EditUser(parseInt(userID));
            }

        }
    });

    $('#addPhone').mask('000-000-0000');
    $('#editphone').mask('000-000-0000');

    $('#addFullname,#addPhone,#addEmail,#addDesignation').on('blur', function () {
        ValidateElement($(this));
    });

    $('#addProfile').change(function (event) {
        event.preventDefault();
        var files = $("#addProfile").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

            var image = new Image();
            image.src = window.URL.createObjectURL($('#addProfile')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 100 && width < 100) {
                   // alert("Height and Width must not exceed 100px.");
                    $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                    $('#CommonErrorModal').modal('show');

                    return false;
                }
                else {
                    DisplayImage($('#addProfile')[0].files, $('#addlogo').attr('id'));
                }
            }
        }
        else {
            //alert('Only jpg and png format');
            var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'ProfilePic') { return e } })
            if (updateMessage != '') {
                $('#CommonErrorMessage').html(updateMessage[0].Message);
                $('#CommonErrorModal').modal('show');
            }
            //$('#btnAddUser').prop('disabled', true)
        }
    });

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtCityoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' + $('#addCity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#addState').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    var txtEditCityoptions = {
        source: function (request, response) {

            $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' + $('#editCity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#editState').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $("#addState").change(function (event) {
        $('#addCity').val('');
    });

    $(document).on("keydown.autocomplete", '#addCity', function (event) {
        $(this).autocomplete(txtCityoptions);
    })

    AlertMessageUserManagement();

    $('#btnAddUser').click(function (event) {
       
        event.preventDefault();

        $('#btnAddUser').attr('disabled', 'disabled');

        if (!ValidateAddUser()) {
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }

        var isValid = true;

        var phonelen = $('#addPhone').val().length;
        if (phonelen < 12) {
            $('#addPhone').css({
                "border": "1px solid red"
            });
            $('#addPhone').focus();
            var isValid = false;
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }
        //validation for name allow only alphabets
        var fullname = $('#addFullname').val();
        var re = /^[a-zA-Z ]*$/;
        var namecheck = "";
        if (typeof fullname != 'undefined' && fullname != null && fullname.trim() != "") {
            if (!fullname.match(re)) {
                $("#addFullname").css({
                    "border": "1px solid red"
                });
                $("#addFullname").focus();
                namecheck = "a";
                $('#btnAddUser').removeAttr('disabled');
            }
        }
        if (namecheck == "a") {
            return false;
        }

        var formData = new FormData();
        var files = $("#addProfile").get(0).files;
        if (files[0] != null) {
            var fileName = files[0].name;
        }
        else {
            var fileName = "no_images.png";
        }

        var contact = $("#addPhone").val().replace('-', '').replace('-', '');
        var companyId = $('#companyId').val();

        formData.append("img", files[0]);
        formData.append("IsDeleted", IsDeleted);
        formData.append("FirstName", $('#addFullname').val().trim());

        formData.append("Designation", $('#addDesignation option:selected').val().trim());
        formData.append("ContactNumber", contact);
        formData.append("Email", $('#addEmail').val().trim());
        formData.append("ProfilePic", fileName);

        formData.append("Address1", $('#addStreetAddress').val().trim());
        formData.append("State", $('#addState').val());
        formData.append("StateText", $('#addState option:selected').text());
        formData.append("City", $('#addCity').val());
        formData.append("PostalCode", $('#addZipCode').val());

        formData.append("CompanyID", companyId);
        if ($('#profileUnsubscribeAdd').is(":checked")) {
            formData.append("Unsubscribe", true);
        }
        else {
            formData.append("Unsubscribe", false);
        }
        if (isValid) {

            $.ajax({
                url: '/ManageUser/AddUser',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    IsDeleted= 0;
                    $('#btnAddUser').removeAttr('disabled');
                    if (data == 1) {

                        var EmailMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'EmailExist') { return e } })
                        if (EmailMessage != '') {
                            $('#CommonErrorMessage').html(EmailMessage[0].Message);
                            $('#CommonErrorModal').modal('show');
                        }

                    }
                    else {
                        $('#btnAddUser').removeAttr('disabled');
                        var addMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'Insert') { return e } })
                        if (addMessage != '') {
                            $('#CommonSuccessMessage').html(addMessage[0].Message);
                            $('#CommonSuccessModal').modal('show');
                        }

                        $('#AddUser').modal('hide');
                        var table = $('#UserDatatable').DataTable();
                        table.ajax.reload();
                    }
                },
                error: function (data) {
                    $('#btnAddUser').removeAttr('disabled');
                    alert('error')
                }
            })
        }
        else {
            $('#btnAddUser').removeAttr('disabled');
            return false;
        }


    })

    $("#editpic").change(function (event) {
        event.preventDefault();
        var files = $("#editpic").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

            var image = new Image();
            image.src = window.URL.createObjectURL($('#editpic')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 100 && width < 100) {
                    // alert("Height and Width must not exceed 100px.");
                    $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                    $('#CommonErrorModal').modal('show');
                    return false;
                }
                else {
                    DisplayImage($('#editpic')[0].files, $('#editUserpic').attr('id'));
                }
            }

        }
        else {
            var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'ProfilePic') { return e } })
            if (updateMessage != '') {
                $('#CommonErrorMessage').html(updateMessage[0].Message);
                $('#CommonErrorModal').modal('show');
            }

        }
    });

    $("#editState").change(function (event) {
        $('#editCity').val('');
    });

    $(document).on("keydown.autocomplete", '#editCity', function (event) {
        $(this).autocomplete(txtEditCityoptions);
    })

    $(document).on('change', 'input[name="chkUserPermission"]', function () {
        //
        var Parentiddd = $(this).attr('parentid').split('_')[0];
        var parentidddchecked = $(this).is(":checked");


        $('input[name="chkUserPermission"]').each(function () {

            var iddd = $(this).attr('parentid').split('_');

            if (iddd[1] == Parentiddd) {
                this.checked = parentidddchecked;
            }
        });

        var Childiddd = $(this).attr('parentid').split('_')[1];
        var childidddchecked = $(this).is(":checked");
        var anysiblingsChecked = 0;
        var anysiblingsUnChecked = 0;
        var totalsiblings = 0;
        $('input[name="chkUserPermission"]').each(function () {
            var iddd = $(this).attr('parentid').split('_');
            if ((iddd[1] == Childiddd) && ($(this).is(":checked") == true)) {
                anysiblingsChecked++;
                totalsiblings++;
            }
            else if ((iddd[1] == Childiddd) && ($(this).is(":checked") == false)) {
                totalsiblings++;
                anysiblingsUnChecked++;
            }


        });


        if (childidddchecked == true) {
            if (anysiblingsChecked <= totalsiblings) {
                $('input[name="chkUserPermission"]').each(function () {
                    var parentid = $(this).attr('parentid').split('_');
                    if (Childiddd == parentid[0]) {
                        this.checked = true;
                    }

                });
            }
        }
        else {
            if (anysiblingsUnChecked == totalsiblings) {
                $('input[name="chkUserPermission"]').each(function () {
                    var parentid = $(this).attr('parentid').split('_');
                    if (Childiddd == parentid[0]) {
                        this.checked = false;
                    }

                });
            }
        }


        //$('input[name="chkUserPermission"]').each(function () {

        //    var iddd = $(this).attr('parentid').split('_');

        //    if (iddd[0] == Childiddd) {
        //        if ((anysiblingsChecked != totalsiblings) && (childidddchecked == false)) {

        //        }
        //        else if ((anysiblingsChecked == totalsiblings) && (childidddchecked == false)) {
        //            
        //            this.checked = childidddchecked;
        //        }
        //        else {
        //            
        //            this.checked = childidddchecked;
        //        }
        //    }
        //});


    });

    $('#btnUpdateUser').click(function (event) {

        event.preventDefault();

        if (!ValidateEditUser()) {
            return false;
        }
        var isValid = true;

        var phonelen = $('#editphone').val().length;
        if (phonelen < 12) {
            $('#editphone').css({
                "border": "1px solid red"
            });
            $('#editphone').focus();
            var isValid = false;
            $('#btnUpdateUser').removeAttr('disabled');
            return false;
        }

        //validation for name allow only alphabets
        var fullname = $('#editFullname').val();
        var re = /^[a-zA-Z ]*$/;
        var namecheck = "";
        if (typeof fullname != 'undefined' && fullname != null && fullname.trim() != "") {
            if (!fullname.match(re)) {
                $("#editFullname").css({
                    "border": "1px solid red"
                });
                $("#editFullname").focus();
                namecheck = "a";
                $('#btnUpdateUser').removeAttr('disabled');
            }
        }
        if (namecheck == "a") {
            return false;
        }

        var formData = new FormData();
        var files = $("#editpic").get(0).files;
        if (files[0] != null) {
            var fileName = files[0].name;
            IsDeleted = 0;
        }
        formData.append("img", files[0]);
        formData.append("IsDeleted", IsDeleted);
        var editContact = $("#editphone").val().replace('-', '').replace('-', '');
        formData.append("UserID", $('#edituserid').val().trim());
        formData.append("FirstName", $('#editFullname').val().trim());

        formData.append("ContactNumber", editContact);
        formData.append("Designation", $('#editDesignation option:selected').val().trim());
        formData.append("Email", $('#editemail').val());

        formData.append("Requested_Projects", $('#editrequestedprojects').val());

        formData.append("Address1", $('#editStreetAddress').val().trim());
        formData.append("State", $('#editState').val());
        formData.append("StateText", $('#editState option:selected').text());
        formData.append("City", $('#editCity').val());
        formData.append("PostalCode", $('#editZipCode').val());

        formData.append("AccountStatus", $('#editStatus option:selected').val());
        if ($('#profileUnsubscribeEdit').is(":checked")) {
            formData.append("Unsubscribe", true);
        }
        else {
            formData.append("Unsubscribe", false);
        }
        var permissionArray = [];
        $('input[name="chkUserPermission"]').each(function () {
            if ($(this).is(":checked")) {
                permissionArray[permissionArray.length] = { UserID: $('#edituserid').val(), ModuleId: $(this).attr('parentid').split('_')[0], HasPermission: true }
            }
            else {
                permissionArray[permissionArray.length] = { UserID: $('#edituserid').val(), ModuleId: $(this).attr('parentid').split('_')[0], HasPermission: false }
            }
        })

        formData.append("permissionList", JSON.stringify(permissionArray));
        if ($('#profileUnsubscribeAdd').is(":checked")) {
            formData.append("Unsubscribe", true);
        }
        else {
            formData.append("Unsubscribe", false);
        }
        $.ajax({
            url: '/ManageUser/UpdateUser',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                IsDeleted = 0;
                var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'Update') { return e } })
                if (updateMessage != '') {
                    $('#CommonSuccessMessage').html(updateMessage[0].Message);
                    $('#CommonSuccessModal').modal('show');
                }

                $('#editUser').modal('hide');
                var table = $('#UserDatatable').DataTable();
                table.ajax.reload();
            },
            error: function (data) {
                alert('error')
            }
        })
    });

    var userIdList = [];

    $('#chkAllUser').change(function () {
        if ($(this).is(":checked")) {
            $('input[name="chkUser"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                userIdList.push(UserID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUser"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];

                userIdList = jQuery.grep(userIdList, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $('#btnAllDelete').click(function (event) {
        event.preventDefault();

        var IsValiddd = true;
        if (userIdList.length == 0) {
            IsValiddd = false;
        }

        var result = userIdList.join(", ")

        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageUser/MultipleUserDelete",
                data: { userID: result },
                success: function (userdetail) {
                    $('#chkAllUser').prop('checked', false);
                    var table = $('#UserDatatable').DataTable();
                    table.ajax.reload();
                }
            })
        }
    })

    $('#btnStatusYes').click(function () {

        var UserID = $('#hdnStatusForUserID').val();
        var statusID = $('#hdnStatusValue').val();
        var UserEmail = $('#hdnStatusForUserEmail').val();

        $.ajax({
            type: "POST",
            url: "/ManageUser/UpdateUserAccountStatus",
            data: { UserID: UserID, StatusID: statusID, Email: UserEmail },
            success: function (userdetail) {
                var table = $('#UserDatatable').DataTable();
                table.ajax.reload();
            }
        })
    })

    $('#btnDeleteUser').click(function () {
        var deleteMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'Delete') { return e } })
        if (deleteMessage != '') {
            $('#userDeleteMessage').html(deleteMessage[0].Message);
        }
        $('#userDeleteConfirmModel').modal('show');
    })

    $('#btnDeleteYes').click(function () {

        var UserID = $('#edituserid').val();

        DeleteUser(UserID);

        $('#editUser').modal('hide');
        $('#userDeleteConfirmModel').modal('hide');
    })

    $('#editUserclickfromModal').click(function () {

        $('#ViewDetail').modal('hide');
        var UserID = $('#edituserid').val();

        EditUser(UserID);
        $('#editUser').modal('show');
    })

    $('.dataTables_length').hide();

    $('#UserDatatable_filter').hide();

    var ProjectNo = '';

    //var txtProjectNameoptions = {
    //    source: function (request, response) {

    //        $.ajax({
    //            url: "/ManageUser/ProjectNameAutoCompleteByCompanyId/",
    //            data: '{ searchTerm:"' + request.term + '", companyId:"' + EditUserCompanyId + '", RoleId:"' + EditUserRoleId + '"}',
    //            contentType: "application/json; charset=utf-8",
    //            dataType: 'json',
    //            type: 'Post',
    //            success: function (data) {

    //                response($.map(data, function (item) {
    //                    return {
    //                        label: item.ProjectNo + " - " +item.ProjectName,
    //                        value: item.ProjectName,
    //                        ProjectNo: item.ProjectNo
    //                    }
    //                }));
    //            }
    //        })
    //    },
    //    focus: function () {
    //        // prevent value inserted on focus
    //        return false;
    //    },
    //    select: function (event, ui) {
    //        $("#updateProjectAssign").val('');

    //        ProjectNo = ui.item.ProjectNo

    //        if ($("#updateProjectAssignListbox option[value='" + ProjectNo + "']").length > 0) {
    //            // already in listbox
    //        }
    //        else {
    //            var concateProjectNo = ui.item.label;
    //            $('#updateProjectAssignListbox').append('<option value="' + ProjectNo + '">' + concateProjectNo + '</option>'); // adds item 5 at the end
    //        }

    //        return false;
    //    },
    //    change: function (event, ui) {
    //        if (!ui.item) {
    //            $(this).val("");
    //        } else {
    //        }
    //    },
    //    minLength: 1,
    //};

    //$(document).on("keydown.autocomplete", '#updateProjectAssign', function (event) {
    //    $(this).autocomplete(txtProjectNameoptions);
    //})

    $(document).on("click", '#btnRemoveAssignProject', function (event) {
        $("[id*=updateProjectAssignListbox] option:selected").remove();
        return false;
    })

    $('button[name=btnUpdateUserProject]').click(function (event) {

        event.preventDefault();

        $(this).prop("disabled", true);
        var inValid = true;

        var projectNoCommaValue = '';
        var unsubscribecheck = '';

        //if ($('#updateProjectAssignListbox').has('option').length == 0) {
        //    inValid = false;
        //    $('#updateProjectAssign').css('border-color', 'red');
        //}

        //$("#updateProjectAssignListbox > option").each(function () {
        //    projectNoCommaValue = projectNoCommaValue == '' ? this.value : projectNoCommaValue + ',' + this.value;
        //});

        var oTable = $('#dt_tblProjectAssign').DataTable();
        oTable.search('').columns().search('').draw();

        $("input[id*=chkProjectAssign_").each(function () {
 
            if ($(this).is(":checked")) {
                projectNoCommaValue = projectNoCommaValue == '' ? this.value : projectNoCommaValue + ',' + this.value;
            }

        });
       
            $("input[id*=chkUnsubscribe_").each(function () {

                if ($(this).is(":checked")) {
                    unsubscribecheck = unsubscribecheck == '' ? this.value : unsubscribecheck + ',' + this.value;
                }
            });
       
        var formData = new FormData();
        
        formData.append("UserID", $('#edituserid').val().trim());
        formData.append("ProjectNo", projectNoCommaValue);
        formData.append("UnSubscribeProjectNo", unsubscribecheck);
        if (inValid) {
            $.ajax({
                url: '/ManageUser/UserProjectAssign',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $("#LoadingImage").css("display", "block");
                },
                complete: function () {
                    $("#LoadingImage").css("display", "none");
                },
                success: function (data) {
                    $('button[name=btnUpdateUserProject]').prop("disabled", false);

                    var updateMessage = $.grep(UserManagementMessageList, function (e) { if (e.MessageName == 'ProjectAssign') { return e } })
                    if (updateMessage != '') {
                        $('#CommonSuccessMessage').html(updateMessage[0].Message);
                        $('#CommonSuccessModal').modal('show');
                    }
                    $('#ProjectAssignModal').modal('hide');
                    $('#ProjectAssignModalForEmployee').modal('hide');
                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            return false;
        }

    });

    /*tree css and js*/
    $.fn.extend({
        treed: function (o) {

            var openedClass = 'glyphicon-minus-sign';
            var closedClass = 'glyphicon-plus-sign';

            if (typeof o != 'undefined') {
                if (typeof o.openedClass != 'undefined') {
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined') {
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicatord glyphicon " + closedClass + "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicatord').each(function () {
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });

    //Initialization of treeviews

    $('#tree1').treed();

    $('#tree2').treed({ openedClass: 'glyphicon-folder-open', closedClass: 'glyphicon-folder-close' });

    $('#tree3').treed({ openedClass: 'glyphicon-chevron-right', closedClass: 'glyphicon-chevron-down' });

    $(document).on("change", '#chkAllProjectAssign', function (event) {
        
        if ($(this).is(":checked")) {
            //$('input[id^=chkProjectAssign_]').attr('checked', true);
            $('input[id^=chkProjectAssign_]').each(function () {
                this.checked = true;
            });
        }
        else {
            //$('input[id^=chkProjectAssign_]').attr('checked', false);
            $('input[id^=chkProjectAssign_]').each(function () {
                this.checked = false;
            });
        }
    });
    
    $(document).on("change", '#chkAllUnsubscribe', function (event) {

        if ($(this).is(":checked")) {
            //$('input[id^=chkProjectAssign_]').attr('checked', true);
            $('input[id^=chkUnsubscribe_]').each(function () {
                this.checked = true;
            });
        }
        else {
            //$('input[id^=chkProjectAssign_]').attr('checked', false);
            $('input[id^=chkUnsubscribe_]').each(function () {
                this.checked = false;
            });
        }
    });

    $('#addFullname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $('#editFullname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    //$(document).on("keyup", '#txtProjectAssignSearch', function (event) {
    //    var searchTerm = $(this).val();
    //    var userId = $('#edituserid').val();

    //    if (searchTerm.length > 2) {
    //        GetAllProjectAssign(searchTerm);

    //        $.ajax({
    //            type: "GET",
    //            url: "/ManageUser/EditProjectAssign/",
    //            data: { userId: userId },
    //            success: function (userdetail) {
    //                $.each(userdetail, function (i, val) {
    //                    var setCheckbox = "chkProjectAssign_" + val.ProjectNo;
    //                    $("#" + setCheckbox).attr('checked', 'checked');
    //                });
    //            }
    //        })

    //    }
    //    if (searchTerm == '') {
    //        GetAllProjectAssign(searchTerm);

    //        $.ajax({
    //            type: "GET",
    //            url: "/ManageUser/EditProjectAssign/",
    //            data: { userId: userId },
    //            success: function (userdetail) {
    //                $.each(userdetail, function (i, val) {
    //                    var setCheckbox = "chkProjectAssign_" + val.ProjectNo;
    //                    $("#" + setCheckbox).attr('checked', 'checked');
    //                });
    //            }
    //        })
    //    }
    //})

})