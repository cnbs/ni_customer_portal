﻿function AuditTrailDataTablePaging() {
    $('#AuditTrailDataTable_paginate').children('ul').addClass('flatpagi');
    $('#AuditTrailDataTable_previous').children('a').remove();
    $('#AuditTrailDataTable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#AuditTrailDataTable_next').children('a').remove();
    $('#AuditTrailDataTable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

$(document)
    .ready(function() {
        var currentDate = new Date();
        $("#txtLogDateTo").datepicker("setDate", currentDate);

        var someDate = new Date();
        var numberOfDaysToAdd = -1;
        someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
        $("#txtLogDateFrom").datepicker("setDate", someDate);

        fillDatatable();
        $("#btnAuditTrailSearch").click(function () {
            var startDate = new Date($('#txtLogDateFrom').val());
            var endDate = new Date($('#txtLogDateTo').val());

            
            if (startDate > endDate) {
              $('#CommonSuccessMessage').html('Invalid Date range!!');
              $('#CommonSuccessModal').modal('show');
                return false;
            }

            fillDatatable();
        });

    });

function fillDatatable() {

    var hdnPageSize = 50;
    var auditModel = new Object();
    auditModel.SearchCriteria = "";

    if($('#AuditTrailCustomSearch').val().length > 2) {
        auditModel.SearchCriteria = $('#AuditTrailCustomSearch').val();
    }
    else {
        auditModel.SearchCriteria = "";
    }

    auditModel.LogDateFrom = $('#txtLogDateFrom').val();
    auditModel.LogDateTo = $('#txtLogDateTo').val();

    var formData = new FormData();
    var files = $("#profileImageUpload").get(0).files;

    var contact = $("#profilePhoneNo").val().replace('-', '').replace('-', '');
    formData.append("img", files[0]);


    $('#AuditTrailDataTable').DataTable().destroy();
    var table = $('#AuditTrailDataTable')
        .DataTable({
            "ajax": {
                "url": "/AuditTrail/GetAuditTrailList",
                "type": "POST",
                "datatype": "JSON",
                'data': {
                    auditModel: auditModel
                }
            },
            "fnServerParams": function (aoData) {
                //aoData.columns.push({ "name": "more_data", "value": "my_value" });
                aoData.columns.push({ "name": "LogDateFrom", "value": auditModel.LogDateFrom });
                aoData.columns.push({ "name": "LogDateTo", "value": auditModel.LogDateTo });
                aoData.columns.push({ "name": "SearchCriteria", "value": auditModel.SearchCriteria });

            },
            //'aaSorting': [[0, 'asc']],
            "autoWidth": false, // for disabling autowidth
            "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
            "processing": true, // for show progress bar
            "serverSide": true, // for process server side
            "filter": true, // this is for disable filter (search box)
            "orderMulti": false, // for disable multiple column at once
            "bInfo": false,
            "bSort": false,
            "oLanguage": {
                "sEmptyTable": "No data available",
                "sProcessing": "<img src='/img/spinner.gif'> Loading.."
            },
            "columns": [
                { "data": "Datetime", "name": "Datetime", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "UserName", "name": "UserName", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "UserRole", "name": "UserRole", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "ModuleName", "name": "ModuleName", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "Operation", "name": "Operation", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "Action", "name": "Action", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "ClientIp", "name": "ClientIp", "Searchable": true, "sClass": "txtAlignCenter" },
                { "data": "ServerIp", "name": "ServerIp", "Searchable": true, "sClass": "txtAlignCenter" },
                {
                    "data": "ClientBrowser",
                    "name": "ClientBrowser",
                    "Searchable": true,
                    "sClass":
                        "txtAlignCenter"
                },
                { "data": "ServerName", "name": "ServerName", "Searchable": true, "sClass": "txtAlignCenter" },
                {
                    "data": "SearchCriteria",
                    "name": "SearchCriteria",
                    "Searchable": true,
                    "sClass": "txtAlignCenter"
                },
            ],
            createdRow: function (row, data, index) {
                $(row).addClass('table-click editData');

            },
            "initComplete": function (settings, json) {
                AuditTrailDataTablePaging();
            },
            "drawCallback": function (settings) {
            
                var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            }

        });

    $('.dataTables_length').hide(); // To hide page size selection

    $('#AuditTrailDataTable_filter').hide(); // To remove autogenerated server textbox


    $('#AuditTrailDataTable')
        .on('draw.dt',
            function () {
                AuditTrailDataTablePaging();
                //AuditTrailTooltip();
            });

    //$('#AuditTrailCustomSearch')
    //    .unbind()
    //    .keyup(function () {
    //        auditModel.SearchCriteria = "";

    //        var value = $(this).val();
    //        if (value.length > 2) {
    //            auditModel.SearchCriteria = $('#AuditTrailCustomSearch').val();
    //            table.search(value).draw();
    //        }
    //        if (value == '') {
    //            auditModel.SearchCriteria = $('#AuditTrailCustomSearch').val();
    //            table.search(value).draw();
    //        }
    //    });

    $('#AuditTrailDataTableSearch').hide();

}
