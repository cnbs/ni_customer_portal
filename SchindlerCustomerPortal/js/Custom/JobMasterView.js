﻿var MileOldvalue;
var PreOldvalue;
var SucOldValue;
var storedStatus;
var ComeFrom = '';

var CompanyId = '';
var JobNo = '';
var ProjectNo = '';


function convertjsonOnlydate(Anydate) {
    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}

function editMilestone(id) {



    $('#EditMilestoneDiv').show();

    $.ajax({
        type: "GET",
        url: "/ProjectMilestones/EditMilestone/",
        data: { mileId: id },
        success: function (milestonedetails) {

            $('#hdnMilestoneNoteValue').val(milestonedetails.MilestoneName);
            $('#editHiddenMilestoneid').val('');
            $('#editPredecessorid').val('');
            $('#editSuccessorid').val('');

            $("select#editmilestonetype").html(options);
            $("select#editpredecessortype").html(options);
            $("select#editsuccessortype").html(options);


            $('#editpredecessortype').val($("#editpredecessortype option:first").val());
            $('#editsuccessortype').val($("#editsuccessortype option:first").val());

            $('#editmilestonetype').val($("#editmilestonetype option:first").val());

            $('#Editduedate').val("");
            $('#EditCompleteddate').val("");

            $('#EditMilestoneNote').val("");
            $('#editWaitingon').val("");

            if (milestonedetails.DueDateStr != null && milestonedetails.DueDateStr != undefined) {
                var ddate = milestonedetails.DueDateStr;
                ddate = ddate.split(' ')[0];
            }


            if (milestonedetails.CompletedDateStr != null && milestonedetails.CompletedDateStr != undefined) {
                var cdate = milestonedetails.CompletedDateStr;
                cdate = cdate.split(' ')[0];
            }

            $('#editMilestoneid').val(milestonedetails.MilestoneId);

            //Remove dropdown value of on Milestone value
            $('#editmilestonetype').val(milestonedetails.MilestoneTitle);

            var EditMilevalue = $('#editmilestonetype').val();
            $("#editpredecessortype option[value=" + EditMilevalue + "]").remove();

            $("#editsuccessortype option[value=" + EditMilevalue + "]").remove();


            var EditPreValue = 0;
            if (milestonedetails.Predecessor == 0) {
                $('#editpredecessortype').val("NULL");
                EditPreValue = $('#editpredecessortype').val();
            }
            else {

                $('#editpredecessortype').val(milestonedetails.Predecessor);
                EditPreValue = $('#editpredecessortype').val();
                $("#editmilestonetype option[value=" + EditPreValue + "]").remove();
                $("#editsuccessortype option[value=" + EditPreValue + "]").remove();
            }




            var EditSuccValue = 0;
            if (milestonedetails.Successor == 0) {
                $('#editsuccessortype').val("NULL");
                EditSuccValue = $('#editsuccessortype').val();
            }
            else {

                $('#editsuccessortype').val(milestonedetails.Successor);
                EditSuccValue = $('#editsuccessortype').val();
                $("#editmilestonetype option[value=" + EditSuccValue + "]").remove();
                $("#editpredecessortype option[value=" + EditSuccValue + "]").remove();
            }

            $('#Editduedate').val(ddate);

            $('#EditCompleteddate').val(cdate);

            $('#editWaitingon').val(milestonedetails.WaitingOn);
            $('#editstatustype').val(milestonedetails.Status);
            $('#hdnstatustype').val($('#editstatustype option:selected').text());

            var noteValue = milestonedetails.Note;
            //noteValue = noteValue.replace(/\r?\n/g, '<br />');

            //$('#EditMilestoneNote').html(noteValue);
            if (noteValue != null & noteValue != '')
                $('#ViewMilestoneNote').html('<pre>' + noteValue + '</pre>');
            else
                $('#ViewMilestoneNote').html("N/A");

            // alert($('#ViewMilestoneNote').text());
            storedStatus = milestonedetails.Status;

            $('#editHiddenMilestoneid').val(EditMilevalue);
            $('#editPredecessorid').val(EditPreValue);
            $('#editSuccessorid').val(EditSuccValue);


            MileOldvalue = $("#editmilestonetype").val();
            PreOldvalue = $('#editpredecessortype').val();
            SucOldValue = $("#editsuccessortype").val();

            $('#editmilestonetype').parents('.form-group').addClass('readonly');

            if ($('#editmilestonetype option:selected').text().trim() != 'Job-Site Ready') {
                $('#divReadyToPull').removeClass('input-group');
                $('#readyToPull').hide();
            } else {
                $('#divReadyToPull').addClass('input-group');
                $('#readyToPull').show();
            }

        }
    });
    $('html, body').animate({
        scrollTop: $("#EditMilestoneDiv").offset().top
    }, 500);
}



//$('#btnDeleteMilestone').click(function () {
$(document).on("click", "#btnDeleteMilestone", function () {

    $('#EditMilestoneDiv').hide();
    $('#deletesinglemilestone').modal('show');


});

function commonMilestoneSubString(input, length) {

    if (input.length <= length) {
        input = input
    }
    else {
        input = input.substring(0, length) + '...'
    }

    return input
}


function DocumentDatatablePaging() {
    $('#Milestonetable_paginate').children('ul').addClass('flatpagi');
    $('#Milestonetable_previous').children('a').remove();
    $('#Milestonetable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Milestonetable_next').children('a').remove();
    $('#Milestonetable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}
var ProjectNo = "";

ProjectNo = $('#hdnProjectNo').val();
var JobNo = "";
var options = null;
var statusoptions = null;
$(document).ready(function () {


    JobNo = $('#hdnJobNo').val();

    $('#EditMilestoneDiv').hide();


    $('#ViewBankMilestone').html($('#ViewJobDesc').html() + ' - Milestones');


    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var roleId = $('#hdnUserRoleId').val();
    var model = new Object();

    model.ProjectNumber = $('#hdnProjectNo').val();

    model.ProjectNumber = ProjectNo;


    model.JobNo = JobNo;


    var table = $('#Milestonetable')
        .DataTable({

            "ajax": {
                "url": "/ProjectMilestones/GetMilestoneAll",
                "type": "POST",
                "datatype": "JSON",
                "data": model

            },

            "autoWidth": false, // for disabling autowidth
            //'aaSorting': [[1, 'asc']],
            'aaSorting': [],
            "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
            "processing": true, // for show progress bar
            "serverSide": true, // for process server side
            "filter": true, // this is for disable filter (search box)
            "orderMulti": false, // for disable multiple column at once
            "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
            "iTotalRecords": "3",
            "iTotalDisplayRecords": "3",


            "columns": [
                { "data": "MilestoneId", "name": "MilestoneId", "Searchable": true, "sClass": "colHide txtAlignCenter" },
                {
                    "data": "MilestoneName",
                    "name": "MilestoneTitle",
                    "Searchable": true, "sClass": "txtAlignCenter"
                },
                //{ "data": "PredecessorName", "name": "Predecessor", "Searchable": true },
                //{ "data": "SuccessorName", "name": "Successor", "Searchable": true },
                {
                    "data": "DueDate",
                    "name": "DueDate",
                    "sClass": "txtAlignCenter",
                    "render": function (data, type, row) {

                        return convertjsonOnlydate(data)
                    }
                },
                 {
                     "data": "CompletedDate",
                     "name": "CompletedDate",
                     "sClass": "txtAlignCenter",
                     "render": function (data, type, row) {

                         return convertjsonOnlydate(data)
                     }
                 },
                { "data": "StatusName", "name": "StatusName", "sClass": "txtAlignCenter", },
                {
                    "data": "WaitingOn",
                    "name": "WaitingOn",
                    "sClass": "txtAlignCenter",
                },
                {
                    "data": "Note", "name": "Note", "sClass": "txtAlignCenter",
                    "render": function (data, type, row) {
                        if (data != null || data != "") {
                            return commonMilestoneSubString(data, 20);
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    "data": "MilestoneId",
                    "sClass": "txtAlignCenter",
                    "render": function (data, type, row) {

                        return '<a id="' +
                            data +
                            '"  name="EditDocumentButtonname" class="btnsml_action e-edit" href="#" onclick="editMilestone(' +
                            data +
                            ')"> </a>'


                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).addClass('table-click-JMV editData');

            },
            "initComplete": function (settings, json) {
                DocumentDatatablePaging();
                JobMasterViewTooltip();
            },
            "drawCallback": function (settings) {
                var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            }
        });

    $('#Milestonetable')
        .on('draw.dt',
            function () {
                DocumentDatatablePaging();
                JobMasterViewTooltip();
            });


    $('#AddMilestonePopupClose')
        .click(function () {
            $('#EditMilestoneDiv').hide();
            $("select#milestonetype").html(options);
            //$("select#predecessortype").html(options);
            //$("select#successortype").html(options);

            $('#addMilestoneid').val('');
            $('#addPredecessorid').val('');
            $('#addSuccessorid').val('');

            $('#milestonetype').val($("#milestonetype option:first").val());
            //$('#predecessortype').val($("#predecessortype option:first").val());
            //$('#successortype').val($("#successortype option:first").val());
            $('#statustype').val($("#statustype option:first").val());
            $('#addWaitingon').val("");
            $('#Addduedate').val("");
            $('#Addcompleteddate').val("");


        });


    function successdoctype(data) {
        var obj = data;

        options = '<option name = "miletypeoption" value="NULL">---Select---</option>';
        for (var i = 0; i < obj.length; i++) {

            if (obj[i].MilestoneId == 6) {
                options += '<option name = "miletypeoption" value="' + obj[i].MilestoneId + '">Material in Storage Hub</option>';
            } else if (obj[i].MilestoneId == 8) {
                options += '<option name = "miletypeoption" value="' + obj[i].MilestoneId + '">Equipment Turnover</option>';
            } else if (obj[i].MilestoneId != 5) {
                options += '<option name = "miletypeoption" value="' + obj[i].MilestoneId + '">' + obj[i].MilestoneTitle + '</option>';
            }
        }

        //Add Milestone
        $("select#milestonetype").html(options);

        //$("select#predecessortype").html(options);

        //$("select#successortype").html(options);

        //Edit Milestone
        $("select#editmilestonetype").html(options);

        //$("select#editpredecessortype").html(options);

        //$("select#editsuccessortype").html(options);


    };

    function errordoctype(data) {

        return false;

    };

    $.ajax({
        url: '/Common/BindMilestoneType',
        type: 'GET',
        success: successdoctype,
        error: errordoctype
    });

    $.ajax({
        url: '/Common/BindStatusList',
        type: 'GET',
        data: { module: 'Milestone' },

        success: successmiletype,
        error: errormiletype
    });

    function successmiletype(data) {
        var obj = data;

        statusoptions = '<option name = "statustypeoption" value="NULL">---Select---</option>';
        for (var i = 0; i < obj.length; i++) {

            statusoptions += '<option name = "statustypeoption" value="' +
                obj[i].StatusId +
                '">' +
                obj[i].StatusName +
                '</option>';
        }
        $("select#statustype").html(statusoptions);

        $("select#editstatustype").html(statusoptions);


    };

    function errormiletype(data) {

        return false;

    };


    $('#MilestoneDatatableSearch')
        .unbind()
        .keyup(function () {
            var value = $(this).val();
            if (value.length > 2) {

                table.search(value).draw();
            }
            if (value == '') {
                table.search(value).draw();
            }
        });


    $('#btnAddMilestone')
        .click(function (event) {
            event.preventDefault();
            $('#btnAddMilestone').attr('disabled', 'disabled');
            var isValid = true;
            var milestone = $('#milestonetype').val();
            //var pred = $('#predecessortype').val();
            //var succ = $('#successortype').val();
            //if (milestone == pred)
            $('select[class="form-control milerequired"]')
                .each(function () {

                    if ($.trim($(this).val()) == 'NULL') {
                        isValid = false;
                        $(this)
                            .css({
                                "border": "1px solid red"
                            });

                    } else {

                        $(this)
                            .css({
                                "border": "",
                                "background": ""
                            });
                    }
                });



            if (isValid == false) {
                $('#btnAddMilestone').removeAttr('disabled');
                return false;

            }

            debugger
            CompanyId = $('#hdnSelectedCompanyId').val();
            JobNo = $('#hdnJobNo').val();
            ProjectNo = $('#hdnProjectNo').val();
            ComeFrom = $('#hdnComeFrom').val();

            var formData = new FormData();

            formData.append("ProjectNo", ProjectNo);
            formData.append("JobNo", JobNo);
            formData.append("MilestoneTitle", $('#milestonetype').val().trim());
            //formData.append("Predecessor", $('#predecessortype').val().trim());
            //formData.append("Successor", $('#successortype').val().trim());
            formData.append("WaitingOn", $('#addWaitingon').val());
            formData.append("Status", $('#statustype').val());
            formData.append("DueDate", $('#Addduedate').val());
            formData.append("CompletedDate", $('#Addcompleteddate').val());

            var mileStoneNote = $('#addMilestoneNote').val();
            formData.append("Note", mileStoneNote);

            if (isValid) {
                $.ajax({
                    url: '/ProjectMilestones/AddMilestone',
                    method: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data == "0") {
                            $('#AddMilestone').modal('hide');
                            $('#SuccessJobAddMilestonePopup').modal('show');
                            $('#SuccessJobAddMilestonePopup .mybtn').click(function () {
                                $('#SuccessJobAddMilestonePopup').modal('hide');
                                window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + CompanyId + '/' + ComeFrom;
                                //window.location.reload();
                            })

                            var table = $('#Milestonetable').DataTable();
                            table.ajax.reload();
                            MilestoneChart();
                        }
                        if (data == "1") {

                            $('#CommonSuccessMessage').html('Milestone is already assigned');
                            $('#CommonSuccessModal').modal('show');
                        }
                        $('#btnAddMilestone').removeAttr('disabled');

                    },
                    error: function (data) {
                        //Handle errors
                        $('#btnAddMilestone').removeAttr('disabled');
                    }
                })
            } else {
                return false;
            }

            

        });

    //$('#documentok').click(function () {
    //    $('#SuccessDocument').modal('hide');

    //});

    $('#editdocumentok')
        .click(function () {
            $('#SuccessEditDocument').modal('hide');

        });

    $('#milestonedeletebutton')
        .click(function () {

            var Milestoneid = $('#editMilestoneid').val();

            $.ajax({
                type: "POST",
                url: "/ProjectMilestones/DeleteMilestone/",
                data: { MileID: Milestoneid },
                success: function (userdetail) {
                    $('#deletesinglemilestone').modal('hide');
                    var table = $('#Milestonetable').DataTable();
                    table.ajax.reload();

                }
            })

        })


    $('#btnUpdateMilestone')
        .click(function (event) {
            $('#btnUpdateMilestone').attr('disabled', 'disabled');
            $('#wait').css('display', 'block');
            var isValid = true;

            $('select[class="form-control editmilerequired"]')
                .each(function () {

                    if ($.trim($(this).val()) == 'NULL') {
                        isValid = false;
                        $(this)
                            .css({
                                "border": "1px solid red"
                            });

                    } else {

                        $(this)
                            .css({
                                "border": "",
                                "background": ""
                            });
                    }
                });

            if (isValid == false) {
                $('#btnUpdateMilestone').removeAttr('disabled');
                $('#wait').css('display', 'none');
                return false;

            }

            debugger
            CompanyId = $('#hdnSelectedCompanyId').val();
            JobNo = $('#hdnJobNo').val();
            ProjectNo = $('#hdnProjectNo').val();
            ComeFrom = $('#hdnComeFrom').val();

            var formData = new FormData();


            formData.append("MilestoneId", $('#editMilestoneid').val().trim());
            formData.append("ProjectNo", ProjectNo);
            formData.append("JobNo", JobNo);
            formData.append("MilestoneTitle", $('#editmilestonetype').val().trim());
            //formData.append("Predecessor", $('#editpredecessortype').val().trim());
            //formData.append("Successor", $('#editsuccessortype').val().trim());
            formData.append("WaitingOn", $('#editWaitingon').val());
            formData.append("Status", $('#editstatustype').val());
            formData.append("DueDate", $('#Editduedate').val());
            formData.append("CompletedDate", $('#EditCompleteddate').val());
            formData.append("MilestoneName", $('#editmilestonetype option:selected').text().trim());
            formData.append("StatusName", $('#editstatustype option:selected').text().trim());
            formData.append("PreviousStatusName", $('#editstatustype option[value=' + storedStatus + ']').text());
            formData.append("ProjectName", $('#breadcrumbProjectName').html());
            formData.append("JobDesc", $('#ViewJobDesc').html());


            var previousnotes = $('#ViewMilestoneNote').text();

            if ($('#EditMilestoneNote').val() != '') {
                var mileStoneNote = $('#EditMilestoneNote').val() + '\r \n' + previousnotes.replace("N/A", "");
            }
            else {
                var mileStoneNote = previousnotes.replace("N/A", "");
            }
            formData.append("Note", mileStoneNote);
            formData.append("NewNote", $('#EditMilestoneNote').val());


            $.ajax({
                url: '/ProjectMilestones/UpdateMilestone',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#wait').css('display', 'none');
                    if (data == "0") {
                        var table = $('#Milestonetable').DataTable();
                        table.ajax.reload();
                        MilestoneChart();
                        $('#SuccessJobeditMilestonePopup').modal('show');
                        $('#EditMilestoneDiv').hide();

                        $('#SuccessJobeditMilestonePopup .mybtn').click(function () {
                            $('#SuccessJobeditMilestonePopup').modal('hide');
                            //window.location.reload();
                            window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + CompanyId + '/' + ComeFrom;
                        })

                    } else {

                        $('#CommonSuccessMessage').html('Milestone is already assigned');
                        $('#CommonSuccessModal').modal('show');
                    }

                    $('#btnUpdateMilestone').removeAttr('disabled');
                },
                error: function (data) {
                    //Handle errors
                    $('#btnUpdateMilestone').removeAttr('disabled');
                    $('#wait').css('display', 'none');
                }
            })
        });

    var userIdList = [];


    $('.dataTables_length').hide();

    $('#Milestonetable_filter').hide();

    $('#readyToPull')
        .click(function (e) {
            ProjectNo = $('#hdnProjectNo').val();

            viewReadyToPull(ProjectNo, JobNo);
        });
    //Replace Code Here
    $('#btnReadyToPullSave')
           .click(function (e) {

               $('#wait').css('display', 'block');

               var lstReadyToPullTemplateDetails = [];

               var ProjectNo = $('#hdnProjectNo').val();
               var JobNo = $('#hdnJobNo').val();

               var TempleteId = 0;
               var formData = new FormData();
               var ctrlName = '';

               $('#modal_readypull').modal('hide');
               //$("#loadermodal").show();

               var formData = new FormData();

               formData.append("Note", $('#addMilestoneNote').val().trim());
               formData.append("MilestoneName", $("#hdnMilestoneNoteValue").val());
               formData.append("Status", $('#editstatustype').val());
               formData.append("StatusName", $('#editstatustype option:selected').text().trim());

               formData.append("DueDate", $('#Editduedate').val().trim());
               formData.append("CompletedDate", $('#EditCompleteddate').val());
               formData.append("PreviousStatusName", $('#hdnstatustype').val());

               formData.append("ProjectNo", ProjectNo);
               formData.append("JobNo", JobNo);


               //formData.append("ProjectNo", ProjectNo);
               //formData.append("JobNo", JobNo);

               $('input[name^="chk_"]')
                   .each(function () {

                       ctrlName = $(this).attr('name');
                       TempleteId = ctrlName.replace('chk_', '');

                       //formData.append("TempleteId", TempleteId);

                       var vSelected = $(this).iCheck('update')[0].checked
                       if (vSelected == true) {
                           var files = $('#filename_' + TempleteId).get(0).files;
                           ctrlName = $('#filename_' + TempleteId).attr('name');
                           for (var i = 0; i < files.length; i++) {

                               formData.append(ctrlName + '~' + files[i].name, files[i]);
                           }
                           var ctrlName = $('#textarea_' + TempleteId).attr('name');
                           if ($('#' + ctrlName).val() != '') {
                               //formData.append(ctrlName, $(this).val());
                           }
                           var ctrlName = $('#hdnAlreadyAttachedFile_' + TempleteId).attr('name');
                           if ($('#' + ctrlName).val() != '') {
                               //formData.append(ctrlName, $(this).val());
                           }

                           lstReadyToPullTemplateDetails[lstReadyToPullTemplateDetails.length] = {
                               TemplateId: TempleteId,
                               ProjectNo: ProjectNo,
                               JobNo: JobNo,
                               Comment: $('#textarea_' + TempleteId).val(),
                               Attachment: '',
                               RealAttachment: '',
                               AlreadyRealAttachment: $('#hdnAlreadyAttachedFile_' + TempleteId).val(),
                               CreatedDate: '',
                               CreatedBy: '',
                               IsDeleted: false,
                               appAuditID: 0,
                               SessionID: ''

                           };
                       }

                   });

               formData.append('lstReadyToPullTemplateDetails', JSON.stringify(lstReadyToPullTemplateDetails));


               $.ajax({
                   url: '/ManageProject/SaveReadyToPullTemplateData',
                   method: 'POST',
                   //data: { 'lstReadyToPullTemplateDetails': lstReadyToPullTemplateDetails },
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {

                       //$('#wait').css('display', 'none'); // for ajax loader
                       $('#wait').css('display', 'none');

                       var milestoneStatus = '';
                       if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length
                       ) {

                           milestoneStatus = 'Job-Site Ready milestone is not completed';
                           $('#editstatustype').val('27'); // In Process
                           storedStatus = 27;
                       }

                       $('#CommonSuccessMessage')
                           .html('Record updated successfully ' + (milestoneStatus != '' ? '<br>' + milestoneStatus : ''));
                       $('#CommonSuccessModal').modal('show');
                       //location.reload();

                       //$('#modal_readypull').modal('hide');
                   },
                   error: function (data) {
                       $('#wait').css('display', 'none');
                       //Handle errors
                   }
               });
           });
    //Replace Code Here


    $('#editstatustype')
        .on('change',
            function (e) {

                if (($(this).val() == '28') && ($('#editmilestonetype').val() == '1')) {
                    ProjectNo = $('#hdnProjectNo').val();
                    viewReadyToPull(ProjectNo, JobNo);
                }

                //if ($(this).val() != '28')
                //{

                //    $('#divReadyToPull').removeClass('input-group');
                //    $('#readyToPull').hide();
                //}
                //else
                //{
                //    $('#divReadyToPull').addClass('input-group');
                //    $('#readyToPull').show();
                //}
            });

    MilestoneChart();
});


$('#btnReadyToPullTopClosed').click(function (e) {
    $('#btnReadyToPullCancel').click();
});
$('#btnReadyToPullCancel').click(function (e) {

    if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length) {
        $('#editstatustype').val(storedStatus);
    }

});
function viewReadyToPull(ProjectNo, JobNo) {

    $('#partialReadyToPull').load("/ManageProject/_ReadyToPullTemplate?ProjectNo=" + ProjectNo + "&JobNo=" + JobNo + "", function () {
        $(".icheckbox,.iradio").iCheck({ checkboxClass: 'icheckbox_minimal-grey', radioClass: 'iradio_minimal-grey' });
        if ($("input.fileinput").length > 0) {
            $("input.fileinput").bootstrapFileInput();
        }

        $('.onactive input').on('ifChanged', function (event) {

            var abc = event.target;
            $('#' + abc.id).parentsUntil('.onactive').find('.help-block').toggleClass('hide')
        });
        $('#modal_readypull').modal();

    });
}
function ValidateAttachment(ctrlName) {
    event.preventDefault();
    var files = $('#' + ctrlName).get(0).files;
    if (files[0].type == "image/jpeg" || files[0].type == "image/jpg" || files[0].type == "image/png") {

    }
    else {

        $('#CommonSuccessMessage').html('Only jpg and png format');
        $('#CommonSuccessModal').modal('show');
        return false;

    }
}



function JobMasterViewTooltip() {
    $("#AddDocumentClose").tooltip({ title: 'Add New', placement: 'top' });
    $("#AddMilestone").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit', placement: 'top' });
    $(".e-communicate").tooltip({ title: 'Communication', placement: 'top' });
    $(".e-sammery").tooltip({ title: 'View', placement: 'top' });
    $(".e-contact").tooltip({ title: 'Contacts', placement: 'top' });
    $(".e-download").tooltip({ title: 'Download', placement: 'top' });
}

$(document).on("click", ".table-click-JMV", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];

            var mileStoneId = $(clickedRow).find('td:last').children('.e-edit')[0].id;

            editMilestone(parseInt(mileStoneId));
        }

    }
});


function MilestoneChart() {

    var model = new Object();
    model.ProjectNumber = ProjectNo;
    model.JobNo = JobNo;

    var chartData = '';
    google.charts.load("visualization", "1", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Results', 'Contract Executed', 'Drawing Approved', 'Finishes Completed', 'Job-Site Ready', { role: 'annotation' }],
        ['Village At Rivers Edge Norfolk', 25, 25, 25, 25, ''],
        ['Milennium Tower Boston', 35, 25, 35, 5, ''],
        ['Residence Inn by Marriottbridgewate', 25, 25, 25, 25, ''],
        ['Mit Academic BUildings', 35, 25, 35, 5, '']
        ]);

        $.ajax({

            type: "POST",

            url: "/ManageProject/GetProjectChart/",

            data: model,

            //contentType: "application/json; charset=utf-8",

            dataType: "json",

            success: function (r) {

                chartData = r.stackedChart;
                var data = new google.visualization.DataTable();
                if (chartData.length > 0) {
                    var ColumnData = chartData[0].KeyData.split(',');
                    for (i = 0; i < ColumnData.length; i++) {
                        if (i == 0) {
                            data.addColumn('string', ColumnData[i]);
                        }
                        else {
                            data.addColumn('number', ColumnData[i]);
                        }

                    }
                }

                for (var i = 1; i < chartData.length; i++) {
                    var row = [];
                    var RowData = chartData[i].KeyData.split(',');
                    for (j = 0; j < RowData.length; j++) {
                        if (j == 0) {
                            row.push(RowData[j]);
                        }
                        else {
                            row.push(RowData[j] == "" ? 0 : parseInt(RowData[j]));
                        }

                    }

                    data.addRow(row);
                }

                var options = {
                    //title: 'Standard',
                    chartArea: { width: "50%", height: "50%" },
                    legend: { position: 'right', maxLines: 3 },
                    bar: { groupWidth: '80%' },
                    isStacked: true,
                    colors: ['#9e9e9e', '#16a085', '#27ae60', '#f39c12', '#9c27b0', '#ffeb3b', '#e91e63', '#795548', '#263238', '#34495e'],

                    hAxis: {
                        viewWindow: {
                            min: 0,
                            max: 100
                        },
                        ticks: [0, 20, 40, 60, 80, 100] // display labels every 25
                    }
                };
                //var chart = new google.visualization.BarChart(document.getElementById('stackedChartProjectMilestone'));
                //chart.draw(data, options);




            },

            failure: function (r) {

                //Handle error

            },

            error: function (r) {

                //handle errors

            }

        });

    }
}