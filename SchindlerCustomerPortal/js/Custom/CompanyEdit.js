﻿
BindStateForUser();
var IsDeleted = false;

function clearFileInputField(tagId1, tagId2) {

    $('#' + tagId1).attr('src', '/img/12.png');
    $('#'+ tagId2).val('');
    IsDeleted = true;

}


function EditHeadCountry(countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindCountry",
        dataType: "json",
        success: function (data) {
            $('#heditcountry').html("");

            $.each(data, function (i, val) {
                $('#heditcountry option[value=' + countryId + ']').attr('selected', 'selected');
                $('#heditcountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));

            });
        },
        failure: function (data) {
            alert('oops something went wrong');
        }
    });

}



function EditBillingCountry(countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindCountry",
        dataType: "json",
        success: function (response) {
            $('#beditcountry').html("");

            $.each(response, function (i, val) {
                $('#beditcountry option[value=' + countryId + ']').attr('selected', 'selected');
                $('#beditcountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function EditHeadState(stateid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindState",
        dataType: "json",
        data: { countryId: countryId },
        success: function (response) {
            $('#editstate').html("");
            $('#editstate').append($("<option/>", ($({ value: '', text: '--Select State--' }))));

            $.each(response, function (i, val) {

                $('#editstate option[value=' + stateid + ']').attr('selected', 'selected');

                $('#editstate').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function EditBillingState(stateid, countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindState",
        dataType: "json",
        data: { countryId: countryId },
        success: function (response) {
            $('#editbillingstate').html("");
            $('#editbillingstate').append($("<option/>", ($({ value: '', text: '--Select State--' }))));

            $.each(response, function (i, val) {
                $('#editbillingstate option[value=' + stateid + ']').attr('selected', 'selected');
                $('#editbillingstate').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));

            });
        },
        failure: function (response) {
            bootbox.alert('oops something went wrong');
        }
    });
}

function EditHeadCity(stateid, cityid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindCity",
        dataType: "json",
        data: { countryId: countryId, stateCode: stateid },
        success: function (response) {
            $('#editcity').html("");

            $.each(response, function (i, val) {
                $('#editcity option[value=' + cityid + ']').attr('selected', 'selected');
                $('#editcity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
                $('#billingofficeCity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function EditBillingCity(stateid, cityid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindCity",
        dataType: "json",
        data: { countryId: countryId, stateCode: stateid },
        success: function (response) {
            $('#editbillingcity').html("");

            $.each(response, function (i, val) {

                $('#editbillingcity option[value=' + cityid + ']').attr('selected', 'selected');
                $('#editbillingcity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

$("#heditcountry").on('change', function () {

    var countryid = $('#heditcountry').val();

    $.ajax({
        type: "GET",
        url: "/Common/BindCountry",
        dataType: "json",
        data: { countryid: countryid },
        success: function (citylist) {
            $('#editstate').html("");

            $('#editstate').append($("<option/>", ($({ value: '', text: '--State--' }))));

            $.each(citylist, function (i, val) {
                $('#editstate').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));

            });
        },
        failure: function (citylist) {
            alert('oops something went wrong');
        }
    });
});

$("#beditcountry").on('change', function () {

    var countryid = $('#beditcountry').val();

    $.ajax({
        type: "GET",
        url: "/Common/BindState",
        dataType: "json",
        data: { countryid: countryid },
        success: function (citylist) {

            $('#editbillingstate').html("");
            $('#editbillingstate').append($("<option/>", ($({ value: '', text: '--State--' }))));

            $.each(citylist, function (i, val) {
                $('#editbillingstate').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));

            });
        },
        failure: function (citylist) {
            alert('oops something went wrong');
        }
    });
});

$("#editcompanylogo").change(function (event) {


    event.preventDefault();
    var files = $("#editcompanylogo").get(0).files;
    if (files[0].type == "image/jpeg" || files[0].type == "image/png") {


        var image = new Image();
        image.src = window.URL.createObjectURL($('#editcompanylogo')[0].files[0]);
        image.onload = function () {
            var height = this.height;
            var width = this.width;
            if (height < 100 && width < 100) {
                //alert("Height and Width must not exceed 100px.");
                $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                $('#CommonErrorModal').modal('show');
                return false;
            }
            else {
                DisplayImage($('#editcompanylogo')[0].files, $('#editlogo').attr('id'));
            }
        }
    }
    else {
        //alert('Only jpg and png format');
        $('#CommonErrorMessage').html('Only jpg and png format.');
        $('#CommonErrorModal').modal('show');
    }

});

function EditData(id) {

    $('#editinchargephone').mask('000-000-0000');
    $('#editprimaryphonenumber').mask('000-000-0000');
    $('#editacctmanagerphone').mask('000-000-0000');
    $('#editheadofficephonenumber').mask('000-000-0000');
    $('#editbillingphonenumber').mask('000-000-0000');

    $('#editzipcode').mask('00000');
    $('#editbillingzipcode').mask('00000');

    $(document).on("click", "a[name='disCompanyId']", function (e) {
        $("#editDetail :input").prop("disabled", true);
        $("#editDetail #btnclose").prop("disabled", false);
    });

    $("#editDetail :input").prop("disabled", false);
    $("#editDetail #editcompanyname").prop("disabled", true);
    $("#editDetail #editaccountnumber").prop("disabled", true);
    $("#editDetail #editexternalaccountnumber").prop("disabled", true);

    $.ajax({
        type: "GET",
        url: "/ManageCompany/GetCompany/",
        data: { companyid: id },
        async: false,
        success: function (data) {

            if (data.success == 'false' || data.success == false) {
                window.location.href = "/managecompany/list";
                return;
            }

            if (data.HeadOfficeCityCode == data.BillingCityCode) {
                $('#editCheckbox').prop('checked', true);
            }

            $('#editcompanyid').val(data.CompanyID);
            $('#Viewcompanyid').val(data.CompanyID);
            $('#editcompanyname').val(data.CompanyName);
            $('#editaccountnumber').val(data.AccountNumber);
            $('#editexternalaccountnumber').val(data.ExternalAccountNumber);
            $('#editaddress1').val(data.HeadOfficeAddress1);
            $('#editaddress2').val(data.HeadOfficeAddress2);

            EditHeadCountry(data.HeadOfficeCountryID);
            EditHeadState(data.HeadOfficeStateCode, data.HeadOfficeCountryID);
            $('#editcity').val(data.HeadOfficeCityCode);

            $('#editstate').removeProp("selected");
            $('#editstate').find('option[value="' + data.HeadOfficeStateCode + '"]').prop("selected", true);
            $('#editcompanylogo').attr('src', data.CompanyLogo);

            $('#editzipcode').val(data.HeadOfficeZipCode);
            $('#editbillingstate').removeProp("selected");
            $('#editbillingA1').val(data.BillingAddress1);
            $('#editbillingA2').val(data.BillingAddress2);

            EditBillingCountry(data.BillingCountryID);
            EditBillingState(data.BillingStateCode, data.BillingCountryID);
            $('#editbillingcity').val(data.BillingCityCode);

            $('#editbillingstate option[value=' + data.BillingStateCode + ']').prop('selected', true);
            $('#editbillingzipcode').val(data.BillingZipCode);

            $('#editinchargefname').val(data.InchargeFirstName);
            $('#editacctmanagername').val(data.ACCTManagerName);
            $('#editinchargedes').val(data.InchargeDesignation);
            $('#editinchargephone').val(data.InchargePhone);
            $('#editprimaryphonenumber').val(data.PrimaryPhoneNumber);

            $('#editacctmanagerphone').val(data.ACCTManagerPhone);
            $('#editheadofficephonenumber').val(data.HeadOfficePhoneNumber);
            $('#editbillingphonenumber').val(data.BillingPhoneNumber);

            $('#editinchargeemail').val(data.InchargeEmail);
            $('#editacctmanageremail').val(data.ACCTManagerEmail);

            $("#editlogo").attr("src", data.CompanyLogo);
            $('#editfileName').val(data.CompanyLogoFileName);

        },
        failure: function (data) {
            alert('oops something went wrong');
        }
    });
}

function ValidEmailAddress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};

$("#editCompany").click(function (e) {
    e.preventDefault();
    var isValid = true;


    if ($('#editprimaryphonenumber').val().trim() != null && $('#editprimaryphonenumber').val().trim() != undefined && $('#editprimaryphonenumber').val().trim() != '') {
        var phonelen = $('#editprimaryphonenumber').val().length;
        if (phonelen < 12) {
            $('#editprimaryphonenumber').css({
                "border": "1px solid red"
            });
            $('#editprimaryphonenumber').focus();
            return false;
        }
    }

    if ($('#editinchargephone').val().trim() != null && $('#editinchargephone').val().trim() != undefined && $('#editinchargephone').val().trim() != '') {
        var phonelen = $('#editinchargephone').val().length;
        if (phonelen < 12) {
            $('#editinchargephone').css({
                "border": "1px solid red"
            });
            $('#editinchargephone').focus();
            return false;
        }
    }

    if ($('#editacctmanagerphone').val().trim() != null && $('#editacctmanagerphone').val().trim() != undefined && $('#editacctmanagerphone').val().trim() != '') {
        var phonelen = $('#editacctmanagerphone').val().length;
        if (phonelen < 12) {
            $('#editacctmanagerphone').css({
                "border": "1px solid red"
            });
            $('#editacctmanagerphone').focus();
            return false;
        }
    }

    if ($('#editheadofficephonenumber').val().trim() != null && $('#editheadofficephonenumber').val().trim() != undefined && $('#editheadofficephonenumber').val().trim() != '') {
        var phonelen = $('#editheadofficephonenumber').val().length;
        if (phonelen < 12) {
            $('#editheadofficephonenumber').css({
                "border": "1px solid red"
            });
            $('#editheadofficephonenumber').focus();
            return false;
        }
    }

    if ($('#editbillingphonenumber').val().trim() != null && $('#editbillingphonenumber').val().trim() != undefined && $('#editbillingphonenumber').val().trim() != '') {
        var phonelen = $('#editbillingphonenumber').val().length;
        if (phonelen < 12) {
            $('#editbillingphonenumber').css({
                "border": "1px solid red"
            });
            $('#editbillingphonenumber').focus();
            return false;
        }
    }

    var ziplen = $('#editzipcode').val();
    if (ziplen.trim() != null && ziplen.trim() != undefined && ziplen.trim() != '') {
        if (ziplen.length < 5) {
            $('#editzipcode').css({
                "border": "1px solid red"

            });
            $('#editzipcode').focus();
            return false;

        }
    }

    var bziplen = $('#editbillingzipcode').val();
    if (bziplen.trim() != null && bziplen.trim() != undefined && bziplen.trim() != '') {
        if (bziplen.length < 5) {
            $('#editbillingzipcode').css({
                "border": "1px solid red"

            });
            $('#editbillingzipcode').focus();
            return false;

        }
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var addinchargeemail = $("#editinchargeemail").val();
    var echeck = "";
    if (typeof addinchargeemail != 'undefined' && addinchargeemail != null && addinchargeemail.trim() != "") {
        if (!addinchargeemail.match(re)) {

            $("#Inchargeemail").css({
                "border": "1px solid red"
            });
            $("#Inchargeemail").focus();
            echeck = "a";
        }

    }

    var editacctmanageremail = $("#editacctmanageremail").val();
    var echeck = "";
    if (typeof editacctmanageremail != 'undefined' && editacctmanageremail != null && editacctmanageremail.trim() != "") {
        if (!editacctmanageremail.match(re)) {

            $("#editacctmanageremail").css({
                "border": "1px solid red"
            });
            $("#editacctmanageremail").focus();
            echeck = "a";
        }

    }

    var propic = $('#editfileName').val() != '' || $('#editcompanylogo')[0].files.length > 0 ? 1 : 0;
    if (propic == 0) {
        $('#editlogo').css({
            "border": "1px solid red"

        });

        $('html, body').animate({
            scrollTop: $("#Divlogo").offset().top
        }, 2000);

        return false;
    }

    if (typeof editacctmanageremail != 'undefined' && editacctmanageremail != null && editacctmanageremail.trim() != "") {
        if (!ValidEmailAddress($('#editacctmanageremail'))) {
            isValid = false;
            $('#editacctmanageremail').css('border-color', 'red');
        }
    }

    if (typeof addinchargeemail != 'undefined' && addinchargeemail != null && addinchargeemail.trim() != "") {
        if (!ValidEmailAddress($('#editinchargeemail'))) {
            isValid = false;
            $('#editinchargeemail').css('border-color', 'red');
        }
    }

    var re = /^[a-zA-Z ]*$/;
    //validation for Incharge name allow only alphabets
    var inchargename = $('#editinchargefname').val();
    var fcheck = "";
    if (typeof inchargename != 'undefined' && inchargename != null && inchargename.trim() != "") {
        if (!inchargename.match(re)) {
            $("#editinchargefname").css({
                "border": "1px solid red"
            });
            $("#editinchargefname").focus();
            fcheck = "a";
        }
    }
    if (fcheck == "a") {
        return false;
    }
    //validation for SEC name allow only alphabets
    var secname = $('#editacctmanagername').val();
    var scheck = "";
    if (typeof secname != 'undefined' && secname != null && secname.trim() != "") {
        if (!secname.match(re)) {
            $("#editacctmanagername").css({
                "border": "1px solid red"
            });
            $("#editacctmanagername").focus();
            scheck = "a";
        }
    }
    if (scheck == "a") {
        return false;
    }

    if (isValid) {

        $('#editinchargephone').mask('000-000-0000');
        $('#editprimaryphonenumber').mask('000-000-0000');

        $('#editacctmanagerphone').mask('000-000-0000');
        $('#editheadofficephonenumber').mask('000-000-0000');
        $('#editbillingphonenumber').mask('000-000-0000');

        var TInchargePhone = $('#editinchargephone').val().replace('-', '').replace('-', '');
        var Tprimaryphonenumber = $('#editprimaryphonenumber').val().replace('-', '').replace('-', '');

        var Tacctmanagerphone = $('#editacctmanagerphone').val().replace('-', '').replace('-', '');
        var Theadofficephonenumber = $('#editheadofficephonenumber').val().replace('-', '').replace('-', '');
        var Tbillingphonenumber = $('#editbillingphonenumber').val().replace('-', '').replace('-', '');

        var formData = new FormData();
        var files = $("#editcompanylogo").get(0).files;
        if (files[0] != null) {
            $('#editfileName').val(files[0].name);
        }
        formData.append("img", files[0]);
        formData.append("IsDeleted", IsDeleted);

        formData.append("CompanyId", $('#editcompanyid').val().trim());
        formData.append("CompanyName", $('#editcompanyname').val().trim());
        formData.append("HeadOfficeAddress1", $('#editaddress1').val().trim());
        formData.append("HeadOfficeAddress2", $('#editaddress2').val().trim());

        formData.append("HeadOfficeCountryID", $('#heditcountry option:selected').val().trim());
        formData.append("HeadOfficeStateCode", $('#editstate option:selected').val().trim());

        formData.append("HeadOfficeCityCode", $('#editcity').val().trim());

        formData.append("HeadOfficeZipCode", $('#editzipcode').val().trim());
        formData.append("BillingAddress1", $('#editbillingA1').val().trim());
        formData.append("BillingAddress2", $('#editbillingA2').val().trim());

        formData.append("BillingCountryID", $('#beditcountry option:selected').val().trim());
        formData.append("BillingStateCode", $('#editbillingstate option:selected').val().trim());

        formData.append("BillingCityCode", $('#editbillingcity').val().trim());
        formData.append("BillingZipCode", $('#editbillingzipcode').val().trim());
        formData.append("InchargeFirstName", $('#editinchargefname').val().trim());
        formData.append("ACCTManagerName", $('#editacctmanagername').val().trim());
        formData.append("InchargeDesignation", $('#editinchargedes').val().trim());
        formData.append("InchargePhone", TInchargePhone);
        formData.append("InchargeEmail", $('#editinchargeemail').val().trim());
        formData.append("ACCTManagerEmail", $('#editacctmanageremail').val().trim());

        formData.append("PrimaryPhoneNumber", Tprimaryphonenumber);

        formData.append("ACCTManagerPhone", Tacctmanagerphone);
        formData.append("HeadOfficePhoneNumber", Theadofficephonenumber);
        formData.append("BillingPhoneNumber", Tbillingphonenumber);

        formData.append("CompanyLogo", $('#editfileName').val().trim());
        formData.append("CompanyLogoFileName", $('#editfileName').val().trim());
        $.ajax({
            url: '/ManageCompany/UpdateCompany',
            method: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                IsDeleted = false;
                $('#btneditcompany').removeAttr('disabled');
                $('#btneditcompany').removeClass('disabled');

                $('#CompanyEditSuccessMessage').html('Record updated successfully');
                $('#CompanyEditSuccessModal').modal('show');

            },
            error: function (data) {

                $('#btneditcompany').removeAttr('disabled');
                $('#btneditcompany').removeClass('disabled');
            }
        });
    }
});

function CompanyEditSuccessModalOk() {
    window.location.href = "/managecompany/list";
}

$(document).ready(function () {

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtCityoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutocomplete/",
                data: '{ searchTerm:"' + $('#editcity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#editstate').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editcity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $(document).on("keydown.autocomplete", '#editcity', function (event) {
        $(this).autocomplete(txtCityoptions);
    })

    var TUSACountry = 1;
    var TcityIdSelected = 0;

    var txtCityoptions1 = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutocomplete/",
                data: '{ searchTerm:"' + $('#editbillingcity').val() + '" , countryid:"' + TUSACountry + '" , stateCode:"' + $('#editbillingstate').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editbillingcity").val(ui.item.label);
            TcityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $(document).on("keydown.autocomplete", '#editbillingcity', function (event) {
        $(this).autocomplete(txtCityoptions1);
    })

    $('#IsBillingedit').on('ifChecked', function () {
        $('#editbillingA1').val($('#editaddress1').val());
        $('#editbillingA2').val($('#editaddress2').val());

        $('#beditcountry').val($('#heditcountry').val());

        $('#editbillingstate').val($('#editstate').val());
        $('#editbillingcity').val($('#editcity').val());
        $('#editbillingzipcode').val($('#editzipcode').val());

        $('#editbillingA1').attr('disabled', true)
        $('#editbillingA2').attr('disabled', true)
        $('#editbillingstate').attr('disabled', true)
        $('#editbillingcity').attr('disabled', true)
        $('#editbillingzipcode').attr('disabled', true)

    });

    $('#IsBillingedit').on('ifUnchecked', function () {

        $('#editbillingA1').val("");
        $('#editbillingA2').val("");
        $('#editbillingstate').val("");
        $('#editbillingcity').val("");
        $('#editbillingzipcode').val("");
        $('#editbillingcity option[value="0"]').attr('selected', 'selected');
        $('#editbillingA1').attr('disabled', false)
        $('#editbillingA2').attr('disabled', false)
        $('#editbillingstate').attr('disabled', false)
        $('#editbillingcity').attr('disabled', false)
        $('#editbillingzipcode').attr('disabled', false)

    });

    EditData($("#editid").val());

    $("#editreset").click(function () {
        window.location.href = "/managecompany/list";
    });
    $('#editacctmanagername').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $('#editinchargefname').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
});

