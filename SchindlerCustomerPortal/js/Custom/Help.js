﻿function editHelp(id) {
    
    $.ajax({
        type: "GET",
        url: "/Help/EditHelp/",
        data: { HelpId: id },
        success: function (Helpdetails) {

            $('#HelpId').val(Helpdetails.HelpId);
            $('#editmoduletype').val(Helpdetails.ModuleId);
            $('#edittitle').val(Helpdetails.Title);
            CKEDITOR.instances['edithelpdesc'].setData(Helpdetails.Description);
            $('#editpublished').attr('checked', Helpdetails.PublishedStatus);

        }
    })

    $('#cke_2_contents').removeAttr('style');
    $('#cke_2_contents').attr('style', 'height: 165px;');
}

function HelpDatatablePaging() {
    $('#Helptable_paginate').children('ul').addClass('flatpagi');
    $('#Helptable_previous').children('a').remove();
    $('#Helptable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Helptable_next').children('a').remove();
    $('#Helptable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

$('#btndeletehelp').click(function () {
    $('#helpDeleteConfirmModel').modal('show');
});

$('#btnDeleteYes').click(function () {

    var HelpID = $('#HelpId').val();

    $.ajax({
        type: "POST",
        url: "/Help/DeleteHelpData/",
        data: { HelpId: HelpID },
        success: function (helpdetail) {
           
            $('#editHelp').modal('hide');
            var table = $('#Helptable').DataTable();
            table.ajax.reload();
        }
    })

})

function HelpTooltip() {
    $("#addhelpTooltip").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit Help', placement: 'top' });
}

$(function () {

    CKEDITOR.replace('addhelpdesc');
    CKEDITOR.replace('edithelpdesc');

    $.ajax({
        url: '/Common/BindModule',
        type: 'GET',
        success: successmoduletype,
        error: errormoduletype
    });

    function successmoduletype(data) {
        var obj = data;
        var options = null;
        options = '<option name = "moduleoption" value="">--Select--</option>';
        for (var i = 0; i < obj.length; i++) {
            options += '<option name = "moduleoption" value="' + obj[i].ModuleId + '">' + obj[i].ModuleName + '</option>';
        }
        $("select#addmoduletype").html(options);

        $("select#editmoduletype").html(options);

    };

    function errormoduletype(data) {
        return false;
    };

    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var table = $('#Helptable').DataTable({

        "ajax": {
            "url": "/Help/GetAllHelp",
            "type": "POST",
            "datatype": "JSON"

        },

        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "columns": [
      { "data": "HelpId", "name": "HelpId", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        { "data": "ModuleName", "name": "ModuleName", "Searchable": true },
         { "data": "Title", "name": "Title" },
         {
             "data": "Description", "name": "Description",
             "render": function (data, type, row) {
                 return data.substr(0,50)
             }
         },  
         {
             "data": "PublishedStatus", "name": "PublishedStatus",
             "sClass": "txtAlignCenter",
             "render": function (data, type, row) {
                 if (data == true) {
                     return '<span class="label s-published">Published</span>'
                 }
                 else {
                     return '<span class="label s-draft">Draft</span>'
                 }
             }
         },
        {
            "data": "HelpId",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                
                return '<a class="btnsml_action e-edit" href="#" data-toggle="modal" id="' + data + '" data-target="#editHelp" onclick="editHelp(' + data + ')"> </a> '
               
            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            HelpDatatablePaging();
            HelpTooltip();
        },
        "drawCallback": function (settings) {
            
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }


    });

    $('#Helptable').on('draw.dt', function () {
        HelpDatatablePaging();
        HelpTooltip();
    });

    $('.dataTables_length').hide();

    $('#Helptable_filter').hide();
    
    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {
            
            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#editHelp').modal('show');
                editHelp(parseInt(userID));
            }

        }
    });

    $('#HelptableSearch').unbind().keyup(function () {

        var value = $(this).val();
        if (value.length > 2) {

            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    $('#btnAddHelp').click(function (event) {
        event.preventDefault();

        $('#btnAddHelp').attr('disabled', 'disabled');

        var isValid = true;

        $('input[class="form-control helprequired"]').each(function () {

            
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });
                $('#btnAddHelp').removeAttr('disabled');
            }
            else {
                isValid = true;
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        $('select[class="form-control helprequired"]').each(function () {


            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });
                $('#btnAddHelp').removeAttr('disabled');
            }
            else {
                isValid = true;
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        var AddDescData = CKEDITOR.instances['addhelpdesc'].getData().trim();
       
        if ($.trim(AddDescData) == '')
        {
            isValid = false;
            $('#cke_1_contents').css({
                "border": "1px solid red"
            });
            $('#btnAddHelp').removeAttr('disabled');
        }
        else
        {
           
            $('#cke_1_contents').css({
                "border": "",
                "background": ""
            });
        }
                   
        var ele = $('input[class="form-control helprequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                
            }
            else {
            }
        }

        if (isValid == false)
            return false;

        var publishstatus;
       
        if ($('#addpublished').is(":checked"))
        {
            publishstatus = true;
        }
        else
        {
            publishstatus = false;
        }

        var obj = new Object;
        obj.ModuleId = $('#addmoduletype').val();
        obj.Title = $('#addtitle').val().trim();
        obj.Description = AddDescData;
        obj.PublishedStatus = publishstatus;
        
        if (isValid) {
            $.ajax({
                url: '/Help/AddHelpData',
                method: 'POST',
                data: obj,
                dataType : 'json',
                success: function (data) {
                    $('#btnAddHelp').removeAttr('disabled');
                    $('#AddHelp').modal('hide');

                    $('#CommonSuccessMessage').html('Record added successfully');
                    $('#CommonSuccessModal').modal('show');

                    var table = $('#Helptable').DataTable();
                    table.ajax.reload();

                },
                error: function (data) {
                    alert('error')
                    $('#btnAddHelp').removeAttr('disabled');
                }
            })
        }
        else {
            $('#btnAddHelp').removeAttr('disabled');
            return false;
        }

      

    });

    $('#btnUpdateHelp').click(function (event) {

        var isValid = true;

        $('input[class="form-control edithelprequired"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {
                
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        var ele = $('input[class="form-control edithelprequired"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();

            }
            else {
            }
        }
        var EditDescData = CKEDITOR.instances['edithelpdesc'].getData().trim();
        if ($.trim(EditDescData) == '') {
            isValid = false;
            $('#cke_2_contents').css({
                "border": "1px solid red"
            });
        }
        else {
           
            $('#cke_2_contents').css({
                "border": "",
                "background": ""
            });
        }
       
       

        if (isValid == false)
            return false;

        var publishstatus;

        if ($('#editpublished').is(":checked")) {
            publishstatus = true;
        }
        else {
            publishstatus = false;
        }


        var obj = new Object;
        obj.HelpId = $('#HelpId').val();
        obj.ModuleId = $('#editmoduletype').val();
        obj.Title = $('#edittitle').val().trim();
        obj.Description = EditDescData;
        obj.PublishedStatus = publishstatus;


        $.ajax({
            url: '/Help/UpdateHelp',
            method: 'POST',
            data: obj,
            //cache: false,
            //contentType: false,
            //processData: false,
            success: function (data) {

                $('#editHelp').modal('hide');

                $('#CommonSuccessMessage').html('Record updated successfully');
                $('#CommonSuccessModal').modal('show');

                //$('#SuccessEditHelp').modal('show');
                var table = $('#Helptable').DataTable();
                table.ajax.reload();


            },
            error: function (data) {
                alert('error')
            }
        })
    });

    $('#addhelpTooltip').click(function (event)
    {
        $('#cke_1_contents').removeAttr('style');
        $('#cke_1_contents').attr('style', 'height: 165px;');
    }); 
});

