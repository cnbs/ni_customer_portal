﻿
var markers = [];
var User_Latitude = 0.0;
var User_Longitude = 0.0;

$(document).ready(function () {
    var id = $('#hdnLoggedUserId').val().trim();
    var SearchTearm = $('#ProjectMapSearch').val().trim();

    var model = { 'UserID': parseInt(id), 'SearchTerm': SearchTearm };
    setMapPinValues(model);

    initialize();

    $('#ProjectMapSearch').unbind().keyup(function () {
        var value = $(this).val();

        if (value.length >= 2) {
            markers = [];
            initialize();

            model = { 'UserID': parseInt(id), 'SearchTerm': value };
            setMapPinValues(model);
            initialize();
        }
        if (value == '') {
            model = { 'UserID': parseInt(id) };
            setMapPinValues(model);
            initialize();
        }
    });
})

function setMapPinValues(model) {
    $.ajax({
        type: "POST",
        url: apiLink + "GetLatLongForProjectMaster",
        data: model,
        async: false,
        datatype: "JSON",
        contentType: "application/x-www-form-urlencoded",
        success: function (data) {
            //debugger
            //alert(data.Data.length);
            $.each(data.Data, function (index, value) {
                User_Latitude = value.User_Latitude;
                User_Longitude = value.User_Longitude;
                markers[markers.length] = [value.Latitude, value.Longitude, value.ProjectName, value.StatusName, value.ProjectNo];
            });

        }
    });
}

var map;
var global_markers = [];

//markers = [
//                [39.1259149, -77.2325207, 'Mediumme Parking Garage  Gaithersbu'],
//                [38.8911469, -77.0867431, '2311 Wilson              Arlington'],
//                [38.8911469, -77.0867431, 'Dc Bar Association       Washington'],
//                [38.9015273, -77.0159991, 'East Market West Podium  Philadelph'],
//                [39.9513154, -75.1606304, 'I-97 Business Park Phase Millersvil']
//];

var infowindow = new google.maps.InfoWindow({});

function initialize() {
    if (markers.length >= 1) {
        //var DefaultLat = markers[0][0];
        //var DefaultLang = markers[0][1];
        var DefaultLat = User_Latitude;
        var DefaultLang = User_Longitude;
    }
    else {
        var DefaultLat = 40.77627;
        var DefaultLang = -73.910965;
    }

    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(DefaultLat, DefaultLang);
    var myOptions = {
        zoom: 7,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    addMarker();
}

function addMarker() {

    for (var i = 0; i < markers.length; i++) {
        // obtain the attribues of each marker
        var lat = parseFloat(markers[i][0]);
        var lng = parseFloat(markers[i][1]);
        var trailhead_name = markers[i][2];
        var StatusName = markers[i][3];
        var ProjectNo = markers[i][4];

        var myLatlng = new google.maps.LatLng(lat, lng);

        var contentString = "<html><body><div><p><h5><a href='/ManageProject/ProjectMasterView/"+ ProjectNo + "/Summary' >" + trailhead_name + "</a></h5></p></div></body></html>";

         var image = {
            url:'',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
        };

        if (StatusName == "P") {
            image.url= '../../images/Map/Gray-16.png';
        } else if (StatusName == "O") {
            image.url= '../../images/Map/red-16.png';
        } else if (StatusName == "C") {
            image.url= '../../images/Map/Green-16.png';
        } else if (StatusName == "D") {
            image.url= '../../images/Map/Yellow-16.png';
        }

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: image,
            title: "Coordinates: " + lat + " , " + lng + " | Trailhead name: " + trailhead_name
        });

        marker['infowindow'] = contentString;

        global_markers[i] = marker;

        google.maps.event.addListener(global_markers[i], 'click', function () {
            infowindow.setContent(this['infowindow']);
            infowindow.open(map, this);
        });
    }
}
