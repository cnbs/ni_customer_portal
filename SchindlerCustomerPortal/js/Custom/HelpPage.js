﻿$(document).ready(function(){
   
    $.ajax({
        url: apiLink + 'GetAllHelp',
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (data) {
            
            $('#divHelpPage').html('');

            var renderHTML = '';

            //if (data.length == 0) {
            //    renderHTML += '<h4 class="cstm-title">No Help</h4>'
            //}
            var HelpList = data.Data;
            var ModuleID = 0;
            $.each(HelpList, function (i, val) {
                if (ModuleID != val.ModuleId)
                {
                    ModuleID = val.ModuleId;
                    renderHTML += '<br/><h4 class="cstm-title">' + val.ModuleName + '</h4>'
                }
                renderHTML += '<p><strong>' + val.Title + '</strong></p><p>' + val.Description + '</p>'

            })
            $('#divHelpPage').html(renderHTML);

             $('#divHelpPage').mCustomScrollbar();

        },
        error: function (data) {
            //alert('error')
        }

    })

})