﻿var SelectedDocType = '';
var ProjectNo = '';
var ComeFrom = '';
var SelectedCompanyId = '';
var ProjectName = '';
var CompanyName = '';
var JobName = '';
var ProjectManagementMessageList = '';
var JobNo ='';
var CompanyId ='';

$(document).ready(function () {

    CompanyId = $('#hdnSelectedCompanyId').val();
    JobNo = $('#hdnJobNo').val();
    ProjectNo = $('#hdnProjectNo').val();
    ComeFrom = $('#hdnComeFrom').val();

    $('#BankHeader').html('Bank Profile' + ' - ' + ComeFrom);


    AlertMessageProjectManagement();
    //BindDocType();

    $('#addDocumentName').on("blur focusout", function () {

        var docname = $(this).val().trim();
        var doctype = $('#adddoctype').val();
        var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: 0 }
        // var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: 0 }

        CheckDocName(checkdata);

        //var docname = $("#addDocumentName").val();

    });
    $('#editDocumentName').on("blur focusout", function () {

        var docname = $(this).val().trim();
        var doctype = $('#editdoctype').val();
        var docid = $('#editDocumentid').val();
        if (doctype != 2) {
            doctype = 0
        }
        var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentTypeID: doctype, DocumentID: docid }
        //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: docid }

        CheckDocName(checkdata);

        //var docname = $("#editDocumentName").val();

    });

    switch (ComeFrom) {
        case "Documents":
            {

                ViewProjectJobRecord(JobNo);
                $('#ViewProjectMilestone').html(JobName + ' - ' + 'Milestones');

                SelectedDocType = 'Documents';

                //$("#_ProjectJobView").show();
                $('#_ProjectJobContact').hide();
                $("#_DocumentByTYpeList").show();
                $("#_ProjectMilestoneList").hide
                $("#EditMilestoneDiv").hide();

                $('#JobMilestoneList').hide();
                $("#_ProjectJobCommunicationView").hide();

                $('.xn-openable').removeClass('active');
                $('#DisplayJobContractDocuments').parents('.xn-openable').addClass('active');
              
                break;
            }
        case "Drawing":
            {

                ViewProjectJobRecord(JobNo);
                $('#ViewProjectMilestone').html(JobName + ' - ' + 'Milestones');

                SelectedDocType = 'Drawing';

                $("#_ProjectJobCommunicationView").hide();

                //$("#_ProjectJobView").show();
                $("#_DocumentByTYpeList").hide();
                $('#DisplayJobDrawingsDocuments').click();
                $('#JobMilestoneList').hide();
                $("#EditMilestoneDiv").hide();

                $('.xn-openable').removeClass('active');
                $('#DisplayJobDrawingsDocuments').parents('.xn-openable').addClass('active');
                break;
            }
        case "Milestone":
            {


                ViewProjectJobRecord(JobNo);

                $('#ViewProjectMilestone').html(JobName + ' - ' + 'Milestones');
                $("#EditMilestoneDiv").hide();
                $('#_ProjectJobMilestone').show();

                $("#_ProjectJobCommunicationView").hide();
                //$("#_ProjectJobView").show();
                $("#_DocumentByTYpeList").hide();
                $('#JobMilestoneList').show();
                $('.xn-openable').removeClass('active');
                $('#DisplayJobMilestones').parents('.xn-openable').addClass('active');
                break;
            }
        case "Messages":
            {


                $("#_ProjectJobView").hide();
                $("#_ProjectJobCommunicationView").show();
                ViewProjectJobRecord(JobNo);

                $.getScript("/js/Custom/ProjectCommunication.js", function (data, textStatus, jqxhr) {

                });


                // $('#ViewProjectMilestone').html(JobName + ' - ' + 'Milestones');
                $('#JobMilestoneList').hide();
                $('#_ProjectJobContact').hide();

                $("#_DocumentByTYpeList").hide();
                $('.xn-openable').removeClass('active');
                $('#DisplayJobMessageCommunication').parents('.xn-openable').addClass('active');
                break;
            }

    }

  

    /* For Docs */

    $('#divdpExpDate').hide();
    $('#diveditdpExpDate').hide();

    $('#btnAddDocument').click(function (event) {
        event.preventDefault();
        $('#btnAddDocument').attr('disabled', 'disabled');
        var isValid = true;

        if ($('#addDocumentName').val() == '') {
            $('#addDocumentName').css({ "border": "1px solid red" });
            isValid = false;
            $('#addDocumentName').focus();
        }
        else {
            $('#addDocumentName').css({ "border": "", "background": "" });
        }


        var doctype = $("#adddoctype option:selected").text();

        if ($('#adddoctype').val() == '') {
            $('#adddoctype').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {
            $('#adddoctype').css({ "border": "", "background": "" });
        }


        if (doctype != "Other") {

            if ($('#addDocumentName').val() == '') {
                $('#addDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addDocumentName').focus();
            }
            else {
                $('#addDocumentName').css({ "border": "", "background": "" });
            }

            if ($('#addCompanyName').val() == '') {
                $('#addCompanyName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addCompanyName').focus();
            }
            else {
                $('#addCompanyName').css({ "border": "", "background": "" });
            }

        }
        else {

            if ($('#addDocumentName').val() == '') {
                $('#addDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#addDocumentName').focus();
            }
            else {
                $('#addDocumentName').css({ "border": "", "background": "" });
            }

            //$('#addCompanyName').css({ "border": "", "background": "" });

        }


        var filename = $('div.dz-filename span').text();
        if (filename == null || filename == "" || filename == undefined) {

            $('#dropzoneForm').css({ "border": "1px solid red" });

            isValid = false;
        }
        else {
            $('#dropzoneForm').css({
                "border": "",
                "background": ""
            });
        }

        var fileerrormsg = $('#dropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#dropzoneForm').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {

        }
        //unique doc name start
        if ($('#addDocumentName').val() != "" && isValid == true) {
            var documentname = $('#addDocumentName').val().trim();
            var documenttype = $('#adddoctype').val();
            if (documenttype != 2) {
                documenttype = 0
            }
            var checkdata = { JobId: JobNo, DocumentName: documentname, ProjectID: ProjectNo, DocumentTypeID: documenttype, DocumentID: 0 }


            isValid = CheckDocName(checkdata);

        }
        //unique doc name end


        if (isValid == false) {
            $('#btnAddDocument').removeAttr('disabled');
            return false;

        }

        ComeFrom = $('#hdnComeFrom').val();

        var formData = new FormData();

        var Publishedstatus;

        if ($("#AddIsPublished").is(':checked'))

            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentName", $('#addDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#adddoctype').val().trim());
        formData.append("Description", $('#addDocumentDesc').val().trim());
        formData.append("CompanyName", $('#addCompanyName').val());
        formData.append("ProjectName", $('#addProjectName').val());
        formData.append("JobName", $('#addJobName').val());
        formData.append("ExpiredDate", $('#dpExpDate').val());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);
        formData.append("CompanyId", $('#hdnSelectedCompanyId').val());


        if (isValid) {
            $.ajax({
                url: '/ManageProject/AddProjectDocument',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    var updateMessage = $.grep(ProjectManagementMessageList, function (e) { if (e.MessageName == 'Insert') { return e } })
                    if (updateMessage != '') {
                        $('#CommonSuccessMessage').html(updateMessage[0].Message);
                        $('#CommonSuccessModal').modal('show');

                    }

                    $('#AddDocument').modal('hide');
                    //$('#SuccessDocument').modal('show');
                    var table = $('#Documenttable').DataTable();
                    table.ajax.reload();
                    $('#btnAddDocument').removeAttr('disabled');
                    //ViewProjectDocumentList(ProjectNo, SelectedDocType,SelectedCompanyId)

                },
                error: function (data) {
                    //handle error
                    $('#btnAddDocument').removeAttr('disabled');
                }
            })
        }
        else {
            return false;
        }
       
       // window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + CompanyId + '/'+ ComeFrom;
    });
    $('#btnUpdateDocument').click(function (event) {
        $('#btnUpdateDocument').attr('disabled', 'disabled');
        //event.preventDefault();
        var formData = new FormData();

        var isValid = true;

        var doctype = $("#editdoctype option:selected").text();

        if (doctype != "Other") {

            if ($('#editDocumentName').val() == '') {
                $('#editDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editDocumentName').focus();
            }
            else {
                $('#editDocumentName').css({ "border": "", "background": "" });
            }

            if ($('#editCompanyName').val() == '') {
                $('#editCompanyName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editCompanyName').focus();
            }
            else {
                $('#editCompanyName').css({ "border": "", "background": "" });
            }

        }
        else {

            if ($('#editDocumentName').val() == '') {
                $('#editDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editDocumentName').focus();
            }
            else {
                $('#editDocumentName').css({ "border": "", "background": "" });
            }

            $('#editCompanyName').css({ "border": "", "background": "" });

        }

        var filename = $('#editdropzoneForm div.dz-filename span').text();

        var fileerrormsg = $('#editdropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {

            $('#editdropzoneForm').css({ "border": "1px solid red" });
            isValid = false;

        }
        else {

        }

        if ($('#editattchFilename').find('span.forPadding').length != 0) {


        }
        else {

            if (filename == null || filename == "" || filename == undefined) {
                $('#editdropzoneForm').css({ "border": "1px solid red" });


                isValid = false;
            }
            else {
                $('#editdropzoneForm').css({ "border": "" });
            }

        }

        // unique doc name start
        if ($('#editDocumentName').val() != "" && isValid == true) {
            var documentname = $('#editDocumentName').val().trim();
            var documenttype = $('#editdoctype').val();
            var docid = $('#editDocumentid').val();
            if (documenttype != 2) {
                documenttype = 0
            }
            var checkdata = { JobId: JobNo, DocumentName: documentname, ProjectID: ProjectNo, DocumentTypeID: documenttype, DocumentID: docid }
            //var checkdata = { JobId: JobNo, DocumentName: docname, ProjectID: ProjectNo, DocumentID: docid }

            isValid = CheckDocName(checkdata);

            //docname = $("#editDocumentName").val();
            //isValid == false
        }
        //unique doc name end

        if (isValid == false) {
            $('#btnUpdateDocument').removeAttr('disabled');
            return false;
        }


        var formData = new FormData();

        var Publishedstatus;

        //formData.append("docs", files[0]);
        //var filename = $('div.dz-filename span').text();
        if ($("#editIsPublished").is(':checked'))
            // checked
            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentID", $('#editDocumentid').val().trim());
        formData.append("DocumentName", $('#editDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#editdoctype').val().trim());
        formData.append("Description", $('#editDocumentDesc').val().trim());
        formData.append("CompanyName", $('#editCompanyName').val().trim());
        formData.append("ProjectName", $('#editProjectName').val().trim());
        formData.append("JobName", $('#editJobName').val().trim());
        formData.append("ExpiredDate", $('#editdpExpDate').val().trim());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);
        formData.append("CompanyId", $('#hdnSelectedCompanyId').val());
        $.ajax({
            url: '/ManageProject/UpdateProjectDocument',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                $('#editDocument').modal('hide');
                $('#SuccessEditDocument').modal('show');
                var table = $('#Documenttable').DataTable();
                table.ajax.reload();
                $('#btnUpdateDocument').removeAttr('disabled');

            },
            error: function (data) {
                //handle error
                $('#btnUpdateDocument').removeAttr('disabled');
            }
        })
    });
    $('#documentok').click(function () {
        $('#SuccessDocument').modal('hide');
        //var table = $('#Documenttable').DataTable();
        //table.ajax.reload();
    });
    $('#editdocumentok').click(function () {
        $('#SuccessEditDocument').modal('hide');
        //var table = $('#Documenttable').DataTable();
        //table.ajax.reload();
    });

    $('#btnDeleteDocument').click(function () {
        $('#documentDeleteConfirmModel').modal('show');
        $('#btnDeleteYes').click(function () {
            var DocID = $('#editDocumentid').val();
            DeleteDocument(DocID);
        });
    });

    $('#CommonSuccessOK').click(function () {
        //window.location.href = '/ManageProject/ProjectJobMainView/' + JobNo + '/' + ProjectNo + '/' + CompanyId + '/' + ComeFrom;
    })

    /* For Docs */

    var RoleId = $('#hdnUserRoleId').val();
    if (RoleId == "2" || RoleId == "4") {
        $('#AddMilestonePopupClose').hide();
        $('#editWaitingon').prop('disabled', true);
        $('#btnDeleteMilestone').remove();
        $('#editApprovalstoGC').parents().eq(1).hide();
    }
    else {
        $('#AddMilestonePopupClose').show();
        $('#editWaitingon').prop('disabled', false);
        // $('#btnDeleteMilestone').show();
        $('.mieditMode1').append('<button class="mybtn btnclass" type="button" id="btnDeleteMilestone">Delete Milestone</button>')
        $('#editApprovalstoGC').parents().eq(1).show();
    }

     
});
var hdnPageSize = parseInt($('#hdnPageSize').val());
$(document).on('click', '#AddDocumentClose', function () {

    $('#addDocumentName').val("");
    $('#addDocumentDesc').val("");
    $('#adddoctype').val($("#adddoctype option:first").val());
    $('#addCompanyName').val("");
    $('#addProjectName').val("");
    $('#addJobName').val("");
    $('#dpExpDate').val("");
    $('#divdpExpDate').hide();
    $('#AddIsExpiryDate').prop('checked', false);
    $('#AddIsPublished').prop('checked', false);
    //for current company and project
    var SelectedDocTypeId = '';
    SelectedDocType = ComeFrom;

    BindDocType();
    $("#adddoctype > option").each(function () {
        if (SelectedDocType == this.text) {
            SelectedDocTypeId = this.value;
        }
    });

    $('#adddoctype option[value="' + SelectedDocTypeId + '"]').attr("selected", "selected");
    if (SelectedDocType == 'Drawing') {
        $("#adddoctype > option").each(function () {
            if (SelectedDocType != this.text) {
                $(this).remove();
            }

        });
        $('#adddoctype option[value="' + SelectedDocTypeId + '"]').prop('disabled', false);
        $('#adddoctype').prop('disabled', false);
    }
    else if (SelectedDocType != 'Drawing') {
        $("#adddoctype > option").each(function () {
            if ('Drawing' == this.text) {
                $(this).prop('disabled', false);
                $(this).remove();
            }

        });
    }
    $('#addCompanyName').val(CompanyName);
    $('#addProjectName').val(ProjectName);
    $('#addJobName').val(JobName);
    $('#addJobName').attr('disabled', 'disabled');


    Dropzone.forElement("#dropzoneForm").removeAllFiles(true);

    
});

$('#DisplayJobDocuments').click(function (event) {

    ComeFrom = 'Documents';

    SelectedDocType = 'Documents';

    $('#BankHeader').html('Bank Profile' + ' - ' + ComeFrom);
    SelectedCompanyId = $('#hdnSelectedCompanyId').val();
    ViewProjectDocumentList(ProjectNo, ComeFrom, SelectedCompanyId, '', $('#hdnJobNo').val());
    $('#BankHeader').html('Bank Profile' + ' - ' + ComeFrom);
    $('#JobMilestoneList').hide();
    $("#_ProjectJobCommunicationView").hide();

    $('#_ProjectJobMilestone').hide();
    $("#EditMilestoneDiv").hide();
    
    //$("#_ProjectJobView").show();


    $("#_DocumentByTYpeList").show();

    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');

    $('html, body').animate({
        scrollTop: $("#ProjectDocumentForm").offset().top
    }, 500);

    $('#hdnComeFrom').val('Documents');

});
$('#DisplayJobChangeNoteDocuments').click(function (event) {

    ComeFrom = 'ChangeNote';
    $('#BankHeader').html('Bank Profile' + ' - ' + ComeFrom);
    ViewProjectDocumentList(ProjectNo, ComeFrom, SelectedCompanyId, '', $('#hdnJobNo').val());
    $('#BankHeader').html('Bank Profile' + ' - ' + 'ChangeNotes');

    $("#_ProjectJobCommunicationView").hide();
    $('#JobMilestoneList').hide();
    $('#_ProjectJobMilestone').hide();
    //$("#_ProjectJobView").show();
    $("#_DocumentByTYpeList").show();

    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');
    $('html, body').animate({
        scrollTop: $("#ProjectDocumentForm").offset().top
    }, 500);
});
$('#DisplayJobDrawingsDocuments').click(function (event) {

    ComeFrom = 'Drawing';

    SelectedDocType = 'Drawing';

    $('#BankHeader').html('Bank Profile' + ' - ' + ComeFrom);
    ViewProjectDocumentList(ProjectNo, ComeFrom, SelectedCompanyId, '', $('#hdnJobNo').val());
    $('#BankHeader').html('Bank Profile' + ' - ' + 'Drawings');

    $("#_ProjectJobCommunicationView").hide();
    $('#JobMilestoneList').hide();
    $('#_ProjectJobMilestone').hide();
    $("#EditMilestoneDiv").hide();
    //$("#_ProjectJobView").show();
    $("#_DocumentByTYpeList").show();

    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');
    $('html, body').animate({
        scrollTop: $("#ProjectDocumentForm").offset().top
    }, 500);

    $('#hdnComeFrom').val('Drawing');
});
$('#DisplayJobMilestones').click(function (event) {

    $('#BankHeader').html('Bank Profile' + ' - ' + 'Milestones');
    $('#JobMilestoneList').show();
    $('#_ProjectJobMilestone').show();
    $("#EditMilestoneDiv").hide();

    $("#_ProjectJobCommunicationView").hide();
    //$("#_ProjectJobView").show();
    $("#_DocumentByTYpeList").hide();

    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');

    $('html, body').animate({
        scrollTop: $("#ProjectJobMilestoneForm").offset().top
    }, 1000);

    $('#hdnComeFrom').val('Milestone');

});
$('#DisplayJobMessageCommunication').click(function (event) {

    $.getScript("/js/Custom/ProjectCommunication.js", function (data, textStatus, jqxhr) {

    });

    $('#BankHeader').html('Bank Profile' + ' - ' + 'Messages');

    $("#_ProjectJobCommunicationView").show();
    $('#JobMilestoneList').hide();
    $('#_ProjectJobMilestone').hide();
    $("#EditMilestoneDiv").hide();
    $("#_ProjectJobView").hide();
    $("#_DocumentByTYpeList").hide();

    $('.xn-openable').removeClass('active');
    $(this).parents('.xn-openable').addClass('active');

});
function ViewProjectJobRecord(id) {
    //BindJobStatus();
    BindJobMilestoneStatus();
    $.ajax({
        type: "POST",
        url: "/ManageProject/GetProjectJobRecordByJobNo/",//UserEdit
        data: { JobNo: id },
        async: false,
        success: function (projectJobRecord) {


            // Edit mode initial data
            $('#breadcrumbProjectName').html(projectJobRecord.data.ProjectName).attr("href", "/ManageProject/ProjectMasterView/" + projectJobRecord.data.ProjectNo + "/Summary");
            ProjectNo = projectJobRecord.data.ProjectNo;
            ProjectName = projectJobRecord.data.ProjectName;
            SelectedCompanyId = $('#hdnSelectedCompanyId').val();//projectJobRecord.data.CompanyId;

            CompanyName = projectJobRecord.data.CompanyName;
            JobName = projectJobRecord.data.JobName;

            $('#hdnJobName').val(projectJobRecord.data.BankDesc);
            $('#hdnProjectName').val(projectJobRecord.data.ProjectName);

            $('#ViewJobID').html('[ GO # ' + projectJobRecord.data.JobNo + ']');

            $('#ViewBankMilestoneGoId').html('[ GO # ' + projectJobRecord.data.JobNo + ']');

            $('#ViewProjectDocumentGoId').html('[ GO # ' + projectJobRecord.data.JobNo + ']');


            $('#projectCommunicationTitle').html('[ GO # ' + projectJobRecord.data.JobNo + ']'); // for communication title

            $('#ViewJobDesc').html(projectJobRecord.data.BankDesc);

            $('#editSerialNumber').val(projectJobRecord.data.Serial_Num);
            $('#editOffice').val(projectJobRecord.data.Office);
            $('#editJobDistrict').val(projectJobRecord.data.District);
            $('#editApprovalstoGC').val(projectJobRecord.data.ApprovalstoGC);
            $('#editApprovalstoGberg').val(projectJobRecord.data.ApprovalstoGBer);
            $('#editNOD').val(projectJobRecord.data.NOD);
            $('#editOriginalNOD').val(projectJobRecord.data.OriginalNODDate);
            $('#editNODConfirm').val(projectJobRecord.data.NODConfirm);
            $('#editForecastCloseDate').val(projectJobRecord.data.ForecastCloseDate);
            $('#editNOSplanned').val(projectJobRecord.data.NOSPlanned);
            $('#editNOSactual').val(projectJobRecord.data.NOSActual);
            $('#editProductCode').val(projectJobRecord.data.ProdCode);
            $('#editProductType').val(projectJobRecord.data.Prod);
            $('#editCWTLocation').val(projectJobRecord.data.CwtLoc);
            $('#editElevatorType').val(projectJobRecord.data.Elev_Type);
            $('#editVolt').val(projectJobRecord.data.Volt);
            $('#editTravel').val(projectJobRecord.data.Travel);
            $('#editDoorType').val(projectJobRecord.data.DoorType);
            //$('#editDoorHand').val(projectJobRecord.data.);
            $('#editCapacity').val(projectJobRecord.data.Capacity);
            $('#editSpeed').val(projectJobRecord.data.Speed);
            $('#editFloor').val(projectJobRecord.data.Num_Floors);
            $('#editRearOpening').val(projectJobRecord.data.Rear);
            $('#editPrice').val(NumberToCurrencyFormat(projectJobRecord.data.Price));
            $('#editPaid').val(projectJobRecord.data.PaidPercent);
            $('#editBilled').val(projectJobRecord.data.BilledPercent);
            $('#editBaseHours').val(projectJobRecord.data.BaseHrs);
            $('#editEstimatedHours').val(projectJobRecord.data.EstHrs);
            $('#editActualHours').val(projectJobRecord.data.ActHrs);
            $('#editGCProjMgr').val(projectJobRecord.data.ContractorProjMgr);
            $('#editGCProjMgrEmail').val(projectJobRecord.data.Proj_Mgr_Email);
            $('#editGCProjMgrPhone').val(projectJobRecord.data.ProjectMgrPhone);
            $('#editGCSuper').val(projectJobRecord.data.ContractorSuper);

            $('#editTeamAssigned').val(projectJobRecord.data.TeamAssigned);
            $('#editWhyBumpOut').val(projectJobRecord.data.ActionText);
            $('#editSupersComments').val(projectJobRecord.data.SuperComments);
            $('#editSalesComments').val(projectJobRecord.data.SalesComments);

            $('#editJobStatus option[value="' + projectJobRecord.data.Status + '"]').attr("selected", "selected");
            $('#editJobCompletion').val(projectJobRecord.data.CompletionPercent);

        }
    });
    ViewProjectJobFieldsShowHide();
    ViewProjectDocumentList(ProjectNo, 'Documents', SelectedCompanyId, '', id);

}
function BindJobStatus() {

    $.ajax({
        type: "GET",
        url: "/Common/BindStatusList",
        async: false,
        data: { module: 'project' },
        success: function (statusList) {

            $('#editJobStatus').html("");
            $('#editJobStatus').append($("<option/>", ($({ value: '', text: '--Select--' }))));

            $.each(statusList, function (i, val) {
                $('#editJobStatus').append($("<option/>", ($({ value: val.StatusId, text: val.StatusName }))));
            });

            //    $('#editStatus').selectpicker();


        }
    })
}
function BindJobMilestoneStatus() {

    $.ajax({
        url: '/Common/BindMilestoneType',
        type: 'GET',
        async: false,
        success: function (statusList) {
            
            $('#editJobStatus').html("");
            $('#editJobStatus').append($("<option/>", ($({ value: '0', text: '' }))));

            $.each(statusList, function (i, val) {
                if (val.MilestoneId == 6) {
                    $('#editJobStatus').append($("<option/>", ($({ value: val.MilestoneId, text: 'Material in Storage Hub' }))));
                } else if (val.MilestoneId == 8) {
                    $('#editJobStatus').append($("<option/>", ($({ value: val.MilestoneId, text: 'Equipment Turnover' }))));
                }
                else if (val.MilestoneId != 5) {
                    $('#editJobStatus').append($("<option/>", ($({ value: val.MilestoneId, text: val.MilestoneTitle }))));
                }

            });

            //    $('#editStatus').selectpicker();


        }
    })
}
function convertjsonOnlydate(Anydate) {
    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}
function ViewProjectDocumentList(ProjectNo, docType, SelectedCompanyId, searchTerm, JobNo) {
  
    $('#ViewProjectDocument').html($('#ViewJobDesc').html() + ' - ' + docType);
    var model = new Object();
    model.ProjectNo = ProjectNo;
    model.JobNo = JobNo;
    model.DocumentType = docType;
    model.CompanyId = SelectedCompanyId;



    FinalTable = $('#Documenttable').DataTable({

        "ajax": {
            "url": "/ManageProject/GetProjectDocumentList",
            "type": "POST",
            "datatype": "JSON",
            'data': model,

        },
        destroy: true,
        'aaSorting': [[1, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "columns": [
        { "data": "DocumentID", "name": "DocumentID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        {
            "data": "DocumentName", "Searchable": true, "sClass": "txtAlignCenter",
            "render": function (data, type, row) {

                var xyz = '<a target="_new" href="/ManageDocument/ViewDocumentName?DocPath=' + escape(row.DocumentPath) + '">' + data + '</a>';

                return xyz;

                // return '<a target="_new" href="/ManageDocument/ViewDocumentName?DocPath=' + row.DocumentPath + '">' + data + '</a>'

            }

        },

         { "data": "Description", "name": "Description", "Searchable": true, "sClass": "txtAlignCenter" },
         { "data": "DocumentType", "name": "DocumentType", "Searchable": true, "sClass": "txtAlignCenter", },
         { "data": "PublishedBy", "name": "PublishedBy", "sClass": "txtAlignCenter" },
         {
             "data": "ExpiredDate",
             "name": "ExpiredDate",
             "sClass": "txtAlignCenter",

         },
         {
             "data": "PublishedStatus",
             "sClass": "txtAlignCenter",
             "render": function (data, type, row) {


                 switch (data) {
                     case "Published":
                         {
                             return '<span class="label s-published">Published</span>'
                             break;
                         }
                     case "Draft":
                         {
                             return '<span class="label s-draft">Draft</span>'
                             break;
                         }
                 }

             }
         },

          {
              "data": "DocumentID",
              "sClass": "txtAlignCenter",
              "render": function (data, type, row) {

                  var d = new Date();

                  var month = d.getMonth() + 1;
                  var day = d.getDate();

                  var output =
                      (month < 10 ? '0' : '') + month + '/' +
                      (day < 10 ? '0' : '') + day + '/' +
                      d.getFullYear();
                  var Expdate = convertjsonOnlydate(row.ExpiredDate);


                  if (RoleId == 1 || RoleId == 3) {

                      return '<a id="' + data + '" name="EditDocumentButtonname" class="btnsml_action e-edit" href="#" data-toggle="modal"  data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>    <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                  }
                  else {
                      if (Expdate != null && Expdate != "" && Expdate != undefined) {
                          if (Expdate > output) {

                              return ' <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                          }
                          else {

                              return '';
                          }
                      }
                      else {
                          return ' <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                      }

                  }


              }
          }

        ]

        ,
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        createdRow: function (row, data, index) {
            $(row).addClass('table-click-ProjectJobDocumentList editData');

        },
        "initComplete": function (settings, json) {
            ProjectDocumentListDatatablePaging();
            ProjectJobMainViewTooltip();
        }
        ,
        "drawCallback": function (settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }


    });

    $('#editIsExpiryDate').on('change', function () {

        if ($(this).is(":checked")) {
            $('#diveditdpExpDate').show();
        }
        else {
            $('#diveditdpExpDate').hide();
            $('#editdpExpDate').val(null);
        }
    });
    $('#AddIsExpiryDate').on('change', function () {

        if ($(this).is(":checked")) {
            $('#divdpExpDate').show();
        }
        else {
            $('#divdpExpDate').hide();
            $('#dpExpDate').val(null);
        }
    });

    //FinalTable.destroy();
    var RoleId = $('#hdnUserRoleId').val();
    if (RoleId != 1 && RoleId != 3) {

        fnShowHide(3);
    }

    function fnShowHide(iCol) {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var oTable = $('#Documenttable').dataTable();

        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, bVis ? false : true);
    }

    ProjectDocumentListDatatablePaging();
    $('#Documenttable_filter').hide(); // To remove autogenerated server textbox
    $('#Documenttable').on('draw.dt', function () { // To set design again
        ProjectDocumentListDatatablePaging();
        ProjectJobMainViewTooltip();
    });
    $('.dataTables_length').hide(); // To hide page size selection

    if (searchTerm != '' && searchTerm != undefined) {
        FinalTable.search(searchTerm).draw();
    }
    
   
}
function ProjectDocumentListDatatablePaging() {
    $('#Documenttable_paginate').children('ul').addClass('flatpagi');
    $('#Documenttable_previous').children('a').remove();
    $('#Documenttable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Documenttable_next').children('a').remove();
    $('#Documenttable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');

    $('html, body').animate({
        scrollTop: $("#ProjectDocumentForm").offset().top
    }, 500);
}
function DeleteDocument(id) {

    $.ajax({
        type: "POST",
        url: "/ManageProject/DeleteProjectDocument/",
        data: { DocID: id },
        success: function (userdetail) {
            $('#editDocument').modal('hide');
            var table = $('#Documenttable').DataTable();
            table.ajax.reload();
        }
    })
}
function BindDocType() {


    $.ajax({
        url: '/Common/BindDocumentType',
        type: 'GET',
        async: false,
        success: function successdoctype(data) {
            var obj = data;

            var options = null;
            options += '<option name = "doctypeoption" value="">--Select--</option>';
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].DocumentTypeName == 'Other') {
                    continue;
                }
                else {
                    if (obj[i].DocumentTypeName == 'Drawing') {
                        options += '<option disabled name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
                    }
                    else {
                        options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
                    }

                }

            }

            $("select#adddoctype").html(options);
            $("select#editdoctype").html(options);


        },
        error: function errordoctype(data) {

            return false;

        }
    });
}
function editDocument(id) {

    $.ajax({
        type: "GET",
        url: "/ManageProject/GetProjectDocumentByID/",
        data: { docId: id },
        success: function (documentdetails) {

            $('#editDocumentName').val("");
            $('#editDocumentDesc').val("");
            $('#editdoctype').val($("#editdoctype option:first").val());
            $('#editCompanyName').val("");
            $('#editProjectName').val("");
            $('#editJobName').val("");
            $('#editdpExpDate').val("");
            $('#editIsExpiryDate').prop('checked', false);
            $('#editIsPublished').prop('checked', false);
            $('#editattchFilename').empty();

            Dropzone.forElement("#editdropzoneForm").removeAllFiles(true);

            var FileName = documentdetails.DocumentPath;
            var myString = FileName.substr(FileName.indexOf("_") + 1);

            //var Expdate = convertjsonOnlydate(documentdetails.ExpiredDate);
            var Expdate = documentdetails.ExpiredDate;

            //var Publishdate = convertjsonOnlydate(documentdetails.PublishedOn);
            $('#editDocumentid').val(documentdetails.DocumentID);
            $('#editDocumentName').val(documentdetails.DocumentName);
            $('#editdoctype').val(documentdetails.DocumentTypeID);
            $('#editDocumentDesc').val(documentdetails.Description);
            $('#editCompanyName').val(documentdetails.CompanyName);
            $('#editProjectName').val(documentdetails.ProjectName);
            $('#editJobName').val(documentdetails.JobName);

            if (FileName != null && FileName != "" && FileName != undefined) {
                $("<span class='forPadding'><a href='javascript:void(0)' name='attachment' class='removeFile'>" + myString + "</a><a href='javascript:void(0)' name='attachmentRemove'><b> Remove</b></a></span>").appendTo("#editattchFilename");
            }

            //$('#editpublishDate').val(Publishdate);
            if (Expdate != null && Expdate != "" && Expdate != undefined) {
                $('#diveditdpExpDate').show();
                $('#editdpExpDate').val(Expdate);
                $('#editIsExpiryDate').prop('checked', true);
            }
            else {
                $('#diveditdpExpDate').hide();
                $('#editdpExpDate').val("");
                $('#editIsExpiryDate').prop('checked', false);
            }


            if (documentdetails.IsPublished == true) {
                $('#editIsPublished').prop('checked', true);
                //$('#editdpExpDate').show();
            }
            else {
                $('#editIsPublished').prop('checked', false);
                //$('#editdpExpDate').show();
            }

       
            var SelectedDocTypeId = documentdetails.DocumentTypeID;
            SelectedDocType = ComeFrom;

            BindDocType();
                      
            $('#editdoctype option[value="' + SelectedDocTypeId + '"]').attr("selected", "selected");
            if (SelectedDocType == 'Drawing')
            {
                $("#editdoctype > option").each(function ()
                {
                    if (SelectedDocType != this.text) 
                    {
                        $(this).remove();
                    }
                });
                $('#editdoctype option[value="' + SelectedDocTypeId + '"]').prop('disabled', false);
                $('#editdoctype').prop('disabled', false);
            }
            else if (SelectedDocType != 'Drawing') {
                $("#editdoctype > option").each(function () {
                    if ('Drawing' == this.text) {
                        $(this).prop('disabled', false);
                        $(this).remove();
                    }

                });
            }         

            //$('#editRoleID').prop('checked', (userdetail.RoleID == 3) ? true : false);

            //$('#editpicFilename').val(userdetail.ProfilePic);
            changeDocTypeedit();
        }
    })
}
function ViewProjectJobFieldsShowHide() {
    var RoleId = $('#hdnUserRoleId').val();
    if (RoleId != 1 && RoleId != 3) {
        $('#lblEditJMVNOD').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#lblEditJMVOriginalNOD').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#lblEditJMVNODConfirm').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editForecastCloseDate').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editNOSplanned').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editNOSactual').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editWhyBumpOut').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editApprohidestoGC').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editApprovalstoGberg').parents('.col-md-4.col-sm-6.form-group').hide()
        $('#editSupersComments').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editSalesComments').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editPrice').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editBaseHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editEstimatedHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editActualHours').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editVolt').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editTravel').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editDoorType').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editCapacity').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editSpeed').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editFloor').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editRearOpening').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editCWTLocation').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editTeamAssigned').parents('.col-md-4.col-sm-6.form-group').hide();
        $('#editJobCompletion').parents('.col-md-4.col-sm-6.form-group').hide();
    }


}
function AlertMessageProjectManagement() {

    var module = 'Job Management';

    $.ajax({
        type: "POST",
        url: "/ManageProject/AlertForProjectManagement/",
        data: { moduleName: module },
        success: function (data) {
            ProjectManagementMessageList = data;
        }
    })



}
function ProjectJobMainViewTooltip() {
    $("#AddDocumentClose").tooltip({ title: 'Add New', placement: 'top' });
    $("#AddMilestone").tooltip({ title: 'Add New', placement: 'top' });
    $(".e-edit").tooltip({ title: 'Edit', placement: 'top' });
    $(".e-communicate").tooltip({ title: 'Communication', placement: 'top' });
    $(".e-sammery").tooltip({ title: 'View', placement: 'top' });
    $(".e-contact").tooltip({ title: 'Contacts', placement: 'top' });
    $(".e-download").tooltip({ title: 'Download', placement: 'top' });
}
$(document).on("click", ".table-click-ProjectJobDocumentList", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];
            var DocId = $(clickedRow).find('td:last').children('.e-edit')[0].id;
            $('#editDocument').modal('show');
            editDocument(parseInt(DocId));
        }

    }
});

$("#Documenttable").on('click', 'a[name=DownloadDocumentName]', function () {
    var Fullpath = $(this).attr('fullpath');
    window.location.href = '/DocumentByType/DownloadDocument?DocPath=' + escape(Fullpath);
    return false;

});

function NumberToCurrencyFormat(value) {
    return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
}

function CheckDocName(checkdata) {
    var flag = true;
    //if (checkdata.DocumentName != "" && checkdata.DocumentTypeID != "") {
    if (checkdata.DocumentName != "") {
        $.ajax({
            url: '/ManageProject/CheckDocumentName',
            type: 'POST',
            data: checkdata,
            dataType: 'json',
            async: false,
            success: function (data) {

                if (data == true) {
                    if (checkdata.DocumentID == 0) {
                        $('#addDocumentName').css({ "border": "1px solid red" });
                        $('#CommonErrorMessage').html("Document Name Is Already Exist.");
                        $('#CommonErrorModal').modal('show');

                        $('#addDocumentName').val("");
                        $('#addDocumentName').focus();
                        //$('#addDocumentName').addClass('input-error');
                        $(".dz-hidden-input").prop("disabled", true);
                        flag = false;
                        // return false
                    }
                    else {
                        $('#editDocumentName').css({ "border": "1px solid red" });
                        $('#CommonErrorMessage').html("Document Name Is Already Exist.");
                        $('#CommonErrorModal').modal('show');

                        $('#editDocumentName').val("");
                        $('#editDocumentName').focus();
                        //$('#editDocumentName').addClass('input-error');
                        $(".dz-hidden-input").prop("disabled", true);
                        flag = false;
                        //return false
                    }

                }
                else {
                    $('#addDocumentName').removeClass('input-error');
                    $('#editDocumentName').removeClass('input-error');
                    $('#addDocumentName').css({ "border": "" });
                    $('#editDocumentName').css({ "border": "" });
                    $(".dz-hidden-input").prop("disabled", false);

                }
            },
            error: function (data) { }
        });
    }

    return flag;
}

$('#DocumentDatatableSearch').unbind().keyup(function () {
    var value = $(this).val();
    if (value.length > 1) {

        ViewProjectDocumentList($('#hdnProjectNo').val(), SelectedDocType, $('#hdnSelectedCompanyId').val(), value, JobNo);
    }
    if (value == '') {
        ViewProjectDocumentList($('#hdnProjectNo').val(), SelectedDocType, $('#hdnSelectedCompanyId').val(), value, JobNo);
    }
    $('#Documenttable_filter').hide(); // To remove autogenerated server textbox
    $('#Documenttable').on('draw.dt', function () { // To set design again
        ProjectDocumentListDatatablePaging();
        ProjectJobMainViewTooltip();
    });
    $('.dataTables_length').hide(); // To hide page size selection
});

function changeDocType()
{
    $('#hdnDocType').val($('#adddoctype').val());
    if ($('#adddoctype').val() == '15') {
        $('#filetypecontent').html('These file types are only allowed:- jpeg, jpg, png, gif');
    }
    else {
        $('#filetypecontent').html('These file types are only allowed:- jpeg, jpg, png, gif, txt, pdf, doc, docx, ppt, pptx, pps, ppsx, odt,x ls,<br /> xlsx, bim, rfa, cad, acd </em>');
    }
}
function changeDocTypeedit()
{
    if ($('#editdoctype').val() == '15') {
        $('#filetypecontentedit').html('These file types are only allowed:- jpeg, jpg, png, gif');
    }
    else {
        $('#filetypecontentedit').html('These file types are only allowed:- jpeg, jpg, png, gif, txt, pdf, doc, docx, ppt, pptx, pps, ppsx, odt,x ls,<br /> xlsx, bim, rfa, cad, acd </em>');
    }
}
$(document).on('click', 'a[name=attachmentRemove]', function () {
    $(".forPadding").remove();
});