﻿
BindStateForUser();
var IsDeleted = false;

function clearFileInputField(tagId1, tagId2) {

    $('#' + tagId1).attr('src', '/img/12.png');
    $('#'+ tagId2).val('');
    IsDeleted = true;

}


function editHeadCountry(countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindCountry",
        dataType: "json",
        success: function (data) {
            $('#heditcountry').html("");

            $.each(data, function (i, val) {
                $('#heditcountry option[value=' + countryId + ']').attr('selected', 'selected');
                $('#heditcountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));

            });
        },
        failure: function (data) {
            alert('oops something went wrong');
        }
    });

}



function editBillingCountry(countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindCountry",
        dataType: "json",
        success: function (response) {
            $('#beditcountry').html("");

            $.each(response, function (i, val) {
                $('#beditcountry option[value=' + countryId + ']').attr('selected', 'selected');
                $('#beditcountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function editHeadState(stateid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindState",
        dataType: "json",
        data: { countryId: countryId },
        success: function (response) {
            $('#editstate').html("");
            $('#editstate').append($("<option/>", ($({ value: '', text: '--Select State--' }))));

            $.each(response, function (i, val) {

                $('#editstate option[value=' + stateid + ']').attr('selected', 'selected');

                $('#editstate').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function editBillingState(stateid, countryId) {
    $.ajax({
        type: "GET",
        url: "/common/BindState",
        dataType: "json",
        data: { countryId: countryId },
        success: function (response) {
            $('#editbillingstate').html("");
            $('#editbillingstate').append($("<option/>", ($({ value: '', text: '--Select State--' }))));

            $.each(response, function (i, val) {
                $('#editbillingstate option[value=' + stateid + ']').attr('selected', 'selected');
                $('#editbillingstate').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));

            });
        },
        failure: function (response) {
            bootbox.alert('oops something went wrong');
        }
    });
}

function editHeadCity(stateid, cityid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindCity",
        dataType: "json",
        data: { countryId: countryId, stateCode: stateid },
        success: function (response) {
            $('#editcity').html("");

            $.each(response, function (i, val) {
                $('#editcity option[value=' + cityid + ']').attr('selected', 'selected');
                $('#editcity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
                $('#billingofficeCity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

function editBillingCity(stateid, cityid, countryId) {

    $.ajax({
        type: "GET",
        url: "/common/BindCity",
        dataType: "json",
        data: { countryId: countryId, stateCode: stateid },
        success: function (response) {
            $('#editbillingcity').html("");

            $.each(response, function (i, val) {

                $('#editbillingcity option[value=' + cityid + ']').attr('selected', 'selected');
                $('#editbillingcity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));

            });
        },
        failure: function (response) {
            alert('oops something went wrong');
        }
    });
}

$("#heditcountry").on('change', function () {

    var countryid = $('#heditcountry').val();

    $.ajax({
        type: "GET",
        url: "/Common/BindCountry",
        dataType: "json",
        data: { countryid: countryid },
        success: function (citylist) {
            $('#editstate').html("");

            $('#editstate').append($("<option/>", ($({ value: '', text: '--State--' }))));

            $.each(citylist, function (i, val) {
                $('#editstate').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));

            });
        },
        failure: function (citylist) {
            alert('oops something went wrong');
        }
    });
});

$("#beditcountry").on('change', function () {

    var countryid = $('#beditcountry').val();

    $.ajax({
        type: "GET",
        url: "/Common/BindState",
        dataType: "json",
        data: { countryid: countryid },
        success: function (citylist) {

            $('#editbillingstate').html("");
            $('#editbillingstate').append($("<option/>", ($({ value: '', text: '--State--' }))));

            $.each(citylist, function (i, val) {
                $('#editbillingstate').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));

            });
        },
        failure: function (citylist) {
            alert('oops something went wrong');
        }
    });
});

$("#editcompanylogo").change(function (event) {


    event.preventDefault();
    var files = $("#editcompanylogo").get(0).files;
    if (files[0].type == "image/jpeg" || files[0].type == "image/png") {

        var image = new Image();
        image.src = window.URL.createObjectURL($('#editcompanylogo')[0].files[0]);
        image.onload = function () {
            var height = this.height;
            var width = this.width;
            if (height < 100 && width < 100) {
                //alert("Height and Width must not exceed 100px.");
                $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                $('#CommonErrorModal').modal('show');
                return false;
            }
            else {
                DisplayImage($('#editcompanylogo')[0].files, $('#editlogo').attr('id'));
            }
        }
    }
    else {
        //alert('Only jpg and png format');
        $('#CommonErrorMessage').html('Only jpg and png format.');
        $('#CommonErrorModal').modal('show');
    }

});

function editData(id) {

    $('#editProjectPhone').mask('000-000-0000');
    $('#editContact1Phone').mask('000-000-0000');
    $('#editContact2Phone').mask('000-000-0000');
    //$('#editheadofficephonenumber').mask('000-000-0000');
    //$('#editbillingphonenumber').mask('000-000-0000');

    //$('#editzipcode').mask('00000');
    //$('#editbillingzipcode').mask('00000');

    //$(document).on("click", "a[name='disCompanyId']", function (e) {
    //    $("#editDetail :input").prop("disabled", true);
    //    $("#editDetail #btnclose").prop("disabled", false);
    //});

    //$("#editDetail :input").prop("disabled", false);
    //$("#editDetail #editcompanyname").prop("disabled", true);
    //$("#editDetail #editaccountnumber").prop("disabled", true);
    //$("#editDetail #editexternalaccountnumber").prop("disabled", true);

    $.ajax({
        type: "GET",
        url: apiLink + "MajorProjects/GeMajorProjectsByID/",
        data: { MajorProjectID: id },
        async: false,
        success: function (data) {

            if (data.success == 'false' || data.success == false) {
                window.location.href = "/majorprojects/list";
                return;
            }

            if (data.HeadOfficeCityCode == data.BillingCityCode) {
                //$('#editCheckbox').prop('checked', true);
                $('#editCheckbox').iCheck('check');
            }

            $('#editcompanyid').val(data.CompanyID);
            $('#Viewcompanyid').val(data.CompanyID);
            $('#editProjectName').val(data.Data.ProjectName);
            $('#editProjectShortName').val(data.Data.ProjectShortName);
            $('#editProjAdditionaInfo').val(data.Data.ProjectAdditionalInfo);
            $('#editConfidentiality').val(data.Data.ProjectConfidentialInfo);
            $('#editProjectStreet').val(data.Data.StreetAddress);

            editHeadCountry(data.Data.HeadOfficeCountryID);
            editHeadState(data.Data.HeadOfficeStateCode, data.Data.HeadOfficeCountryID);
            $('#editProjectCity').val(data.Data.City);

            $('#editstate').removeProp("selected");
            $('#editstate').find('option[value="' + data.Data.StateCode + '"]').prop("selected", true);
            $('#editcompanylogo').attr('src', data.Data.CompanyLogo);

            $('#editProjectZip').val(data.Data.ZipCode);
            $('#editProjectPhone').removeProp("selected");

            $('#editContact1Name').val(data.Data.Contact1Name);
            $('#editContact1Designation').val(data.Data.Contact1Designation);
            $('#editContact1Phone').val(data.Data.Contact1Phone);
            $('#editContact1Email').val(data.Data.Contact1Email);

            $('#editContact2Name').val(data.Data.Contact2Name);
            $('#editContact2Designation').val(data.Data.Contact2Designation);
            $('#editContact2Phone').val(data.Data.Contact2Phone);
            $('#editContact2Email').val(data.Data.Contact2Email);
                   
            $("#editlogo").attr("src", data.Data.CompanyLogo);
            $('#editfileName').val(data.Data.CompanyLogoFileName);
        },
        failure: function (data) {
            alert('oops something went wrong');
        }
    });
}

function ValidEmaileditress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};

$("#btneditMajorProjects").click(function (e) {
    e.preventDefault();
    var isValid = true;


    if ($('#editProjectPhone').val().trim() != null && $('#editProjectPhone').val().trim() != undefined && $('#editProjectPhone').val().trim() != '') {
        var phonelen = $('#editProjectPhone').val().length;
        if (phonelen < 12) {
            $('#editProjectPhone').css({
                "border": "1px solid red"
            });
            $('#editProjectPhone').focus();
            return false;
        }
    }

    if ($('#editContact1Phone').val().trim() != null && $('#editContact1Phone').val().trim() != undefined && $('#editContact1Phone').val().trim() != '') {
        var phonelen = $('#editContact1Phone').val().length;
        if (phonelen < 12) {
            $('#editContact1Phone').css({
                "border": "1px solid red"
            });
            $('#editContact1Phone').focus();
            return false;
        }
    }

    if ($('#editContact2Phone').val().trim() != null && $('#editContact2Phone').val().trim() != undefined && $('#editContact2Phone').val().trim() != '')
    {
        var phonelen = $('#editContact2Phone').val().length;
        if (phonelen < 12) {
            $('#editContact2Phone').css({
                "border": "1px solid red"
            });
            $('#editContact2Phone').focus();
            return false;
        }
    }

    //if ($('#editheadofficephonenumber').val().trim() != null && $('#editheadofficephonenumber').val().trim() != undefined && $('#editheadofficephonenumber').val().trim() != '') {
    //    var phonelen = $('#editheadofficephonenumber').val().length;
    //    if (phonelen < 12) {
    //        $('#editheadofficephonenumber').css({
    //            "border": "1px solid red"
    //        });
    //        $('#editheadofficephonenumber').focus();
    //        return false;
    //    }
    //}

    //if ($('#editbillingphonenumber').val().trim() != null && $('#editbillingphonenumber').val().trim() != undefined && $('#editbillingphonenumber').val().trim() != '') {
    //    var phonelen = $('#editbillingphonenumber').val().length;
    //    if (phonelen < 12) {
    //        $('#editbillingphonenumber').css({
    //            "border": "1px solid red"
    //        });
    //        $('#editbillingphonenumber').focus();
    //        return false;
    //    }
    //}

    //var ziplen = $('#editzipcode').val();
    //if (ziplen.trim() != null && ziplen.trim() != undefined && ziplen.trim() != '') {
    //    if (ziplen.length < 5) {
    //        $('#editzipcode').css({
    //            "border": "1px solid red"

    //        });
    //        $('#editzipcode').focus();
    //        return false;

    //    }
    //}

    //var bziplen = $('#editbillingzipcode').val();
    //if (bziplen.trim() != null && bziplen.trim() != undefined && bziplen.trim() != '') {
    //    if (bziplen.length < 5) {
    //        $('#editbillingzipcode').css({
    //            "border": "1px solid red"

    //        });
    //        $('#editbillingzipcode').focus();
    //        return false;

    //    }
    //}

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var editContact1Email = $("#editContact1Email").val();
    var echeck = "";
    if (typeof editContact1Email != 'undefined' && editContact1Email != null && editContact1Email.trim() != "") {
        if (!editContact1Email.match(re)) {

            $("#editContact1Email").css({
                "border": "1px solid red"
            });
            $("#editContact1Email").focus();
            echeck = "a";
        }

    }

    var editContact2Email = $("#editContact2Email").val();
    var echeck = "";
    if (typeof editContact2Email != 'undefined' && editContact2Email != null && editContact2Email.trim() != "") {
        if (!editContact2Email.match(re)) {

            $("#editContact2Email").css({
                "border": "1px solid red"
            });
            $("#editContact2Email").focus();
            echeck = "a";
        }

    }

    //var propic = $('#editfileName').val() != '' || $('#editcompanylogo')[0].files.length > 0 ? 1 : 0;
    //if (propic == 0) {
    //    $('#editlogo').css({
    //        "border": "1px solid red"

    //    });

    //    $('html, body').animate({
    //        scrollTop: $("#Divlogo").offset().top
    //    }, 2000);

    //    return false;
    //}

    if (typeof editacctmanageremail != 'undefined' && editacctmanageremail != null && editacctmanageremail.trim() != "") {
        if (!ValidEmaileditress($('#editacctmanageremail'))) {
            isValid = false;
            $('#editacctmanageremail').css('border-color', 'red');
        }
    }

    if (typeof editinchargeemail != 'undefined' && editinchargeemail != null && editinchargeemail.trim() != "") {
        if (!ValidEmaileditress($('#editinchargeemail'))) {
            isValid = false;
            $('#editinchargeemail').css('border-color', 'red');
        }
    }

    ////validation for SEC name allow only alphabets
    //var secname = $('#editacctmanagername').val();
    //var scheck = "";
    //if (typeof secname != 'undefined' && secname != null && secname.trim() != "") {
    //    if (!secname.match(re)) {
    //        $("#editacctmanagername").css({
    //            "border": "1px solid red"
    //        });
    //        $("#editacctmanagername").focus();
    //        scheck = "a";
    //    }
    //}
    //if (scheck == "a") {
    //    return false;
    //}

    if (isValid) {
        $('#editProjectPhone').mask('000-000-0000');
        $('#editContact1Phone').mask('000-000-0000');
        $('#editContact2Phone').mask('000-000-0000');

        //var TInchargePhone = $('#editinchargephone').val().replace('-', '').replace('-', '');
        //var Tprimaryphonenumber = $('#editprimaryphonenumber').val().replace('-', '').replace('-', '');

        //var Tacctmanagerphone = $('#editacctmanagerphone').val().replace('-', '').replace('-', '');
        //var Theadofficephonenumber = $('#editheadofficephonenumber').val().replace('-', '').replace('-', '');
        //var Tbillingphonenumber = $('#editbillingphonenumber').val().replace('-', '').replace('-', '');

        //var formData = new FormData();
        var model = new Object();
        var files = $("#editcompanylogo").get(0).files;
        if (files[0] != null)
        {
            $('#editfileName').val(files[0].name);
        }
        //formData.append("img", files[0]);
        //formData.append("IsDeleted", IsDeleted);

        //formData.append("ProjectName", $('#editProjectName').val().trim());
        //formData.append("ProjectShortName", $('#editProjectShortName').val().trim());
        //formData.append("ProjectedititionalInfo", $('#editProjeditInfo').val().trim());
        //formData.append("ProjectConfidentialInfo", $('#editConfidentiality').val().trim());

        ////formData.append("HeadOfficeCountryID", $('#heditcountry option:selected').val().trim());
        ////formData.append("StateCode", $('#editstate option:selected').val().trim());

        //formData.append("Streeteditress", $('#editProjectStreet').val().trim());
        //formData.append("City", $('#editProjectCity').val().trim());
        //formData.append("ProjectMgrPhone", $('#editProjectPhone').val().trim());

        ////formData.append("HeadOfficeZipCode", $('#editzipcode').val().trim());
        ////formData.append("Billingeditress1", $('#editbillingA1').val().trim());
        ////formData.append("Billingeditress2", $('#editbillingA2').val().trim());
        
        //formData.append("Contact1Name", $('#editContact1Name').val().trim());
        //formData.append("Contact1Designation", $('#editContact1Designation').val().trim());
        //formData.append("Contact1Phone", $('#editContact1Phone').val().trim());
        //formData.append("Contact1Email", $('#editContact1Email').val().trim());

        //formData.append("Contact2Name", $('#editContact2Name').val().trim());
        //formData.append("Contact2Designation", $('#editContact2Designation').val().trim());
        //formData.append("Contact2Phone", $('#editContact2Phone').val().trim());
        //formData.append("Contact2Email", $('#editContact2Email').val().trim());
               
        //formData.append("ProjectLogo", $('#editlogo').val().trim());
        //formData.append("CompanyLogoFileName", $('#editfileName').val().trim());
        model.img = files[0];
        model.IsDeleted = IsDeleted;

        model.ProjectName = $('#editProjectName').val().trim();
        model.ProjectShortName = $('#editProjectShortName').val().trim();
        model.ProjectedititionalInfo = $('#editProjAdditionaInfo').val().trim();
        model.ProjectConfidentialInfo = $('#editConfidentiality').val().trim();

        //model.HeadOfficeCountryID= $('#heditcountry option:selected').val().trim();
        model.StateCode= $('#editstate option:selected').val().trim();

        model.StreetAddress = $('#editProjectStreet').val().trim();
        model.City = $('#editProjectCity').val().trim();
        model.ProjectMgrPhone = $('#editProjectPhone').val().trim();
        model.Zip = $('#editProjectZip').val().trim();

        //model.HeadOfficeZipCode= $('#editzipcode').val().trim();
        //model.Billingeditress1= $('#editbillingA1').val().trim();
        //model.Billingeditress2= $('#editbillingA2').val().trim();

        model.Contact1Name = $('#editContact1Name').val().trim();
        model.Contact1Designation = $('#editContact1Designation').val().trim();
        model.Contact1Phone = $('#editContact1Phone').val().trim();
        model.Contact1Email = $('#editContact1Email').val().trim();

        model.Contact2Name = $('#editContact2Name').val().trim();
        model.Contact2Designation = $('#editContact2Designation').val().trim();
        model.Contact2Phone = $('#editContact2Phone').val().trim();
        model.Contact2Email = $('#editContact2Email').val().trim();

        model.ProjectLogo = $('#editlogo').val().trim();
      
        $.ajax({
            url: apiLink + 'MajorProjects/UpdateMajorProjects',
            method: 'POST',
            dataType: 'json',
            data: model,
            //cache: false,
            //contentType: false,
            //processData: false,
            success: function (data) {
                IsDeleted = false;
                $('#btneditCompany').removeAttr('disabled');
                $('#btneditCompany').removeClass('disabled');

                $('#CompanyeditSuccessMessage').html('Record udpated successfully');
                $('#CompanyeditSuccessModal').modal('show');
            },
            error: function (data)
            {
                $('#btneditCompany').removeAttr('disabled');
                $('#btneditCompany').removeClass('disabled');
            }
        });
    }
});

function CompanyeditSuccessModalOk() {
    window.location.href = "/managecompany/list";
}

$(document).ready(function () {

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtCityoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutocomplete/",
                data: '{ searchTerm:"' + $('#editcity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#editstate').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editcity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $(document).on("keydown.autocomplete", '#editcity', function (event) {
        $(this).autocomplete(txtCityoptions);
    })

    var TUSACountry = 1;
    var TcityIdSelected = 0;

    var txtCityoptions1 = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutocomplete/",
                data: '{ searchTerm:"' + $('#editbillingcity').val() + '" , countryid:"' + TUSACountry + '" , stateCode:"' + $('#editbillingstate').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editbillingcity").val(ui.item.label);
            TcityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $(document).on("keydown.autocomplete", '#editbillingcity', function (event) {
        $(this).autocomplete(txtCityoptions1);
    })

    //$('#IsBillingedit').on('ifChecked', function () {
    //    $('#editbillingA1').val($('#editeditress1').val());
    //    $('#editbillingA2').val($('#editeditress2').val());

    //    $('#beditcountry').val($('#heditcountry').val());

    //    $('#editbillingstate').val($('#editstate').val());
    //    $('#editbillingcity').val($('#editcity').val());
    //    $('#editbillingzipcode').val($('#editzipcode').val());

    //    $('#editbillingA1').attr('disabled', true)
    //    $('#editbillingA2').attr('disabled', true)
    //    $('#editbillingstate').attr('disabled', true)
    //    $('#editbillingcity').attr('disabled', true)
    //    $('#editbillingzipcode').attr('disabled', true)

    //});

    //$('#IsBillingedit').on('ifUnchecked', function () {

    //    $('#editbillingA1').val("");
    //    $('#editbillingA2').val("");
    //    $('#editbillingstate').val("");
    //    $('#editbillingcity').val("");
    //    $('#editbillingzipcode').val("");
    //    $('#editbillingcity option[value="0"]').attr('selected', 'selected');
    //    $('#editbillingA1').attr('disabled', false)
    //    $('#editbillingA2').attr('disabled', false)
    //    $('#editbillingstate').attr('disabled', false)
    //    $('#editbillingcity').attr('disabled', false)
    //    $('#editbillingzipcode').attr('disabled', false)

    //});

    editData($("#editid").val());
    

    $("#editreset").click(function () {
        window.location.href = "/majorprojects/list";
    });
    //$('#editacctmanagername').keypress(function (event) {
    //    var inputValue = event.which;
    //    // allow letters and whitespaces only.
    //    if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
    //        event.preventDefault();
    //    }
    //});
    //$('#editinchargefname').keypress(function (event) {
    //    var inputValue = event.which;
    //    // allow letters and whitespaces only.
    //    if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
    //        event.preventDefault();
    //    }
    //});
});

