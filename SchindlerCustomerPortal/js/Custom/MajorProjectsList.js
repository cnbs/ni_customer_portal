﻿$(document).ready(function () {
    var apiLink1 = "http://localhost:55242/api/";
    debugger;

    //var table = $('#MajorProjectMasterListDatatable').DataTable({
  
        //"ajax": {
        //    "url": apiLink + "MajorProjects/GetMajorProjects",
        //    "type": "GET"
         
        //},
        //"searching": false,
        //"ordering": true,
        //"Paging": true,
        //"lengthMenu": [[hdnPageSize, 25, 50], [hdnPageSize, 25, 50]],
        ////"processing": true, // for show progress bar
        ////"serverSide": true, // for process server side
        //"filter": false, // this is for disable filter (search box)
        //"orderMulti": false, // for disable multiple column at once
        //"oLanguage": {
        //    "sEmptyTable": "No data available",
        //    "sProcessing": '<img src="/New Content/New Script/images/loader.png" height="48" width="48" /> Loading... '
        //},
        //"iTotalRecords": "3",
        //"iTotalDisplayRecords": "3",
    var id = $('#hdnLoggedUserId').val().trim();
    var companyId = $('#hdnCompanyID').val();
    var calarray = [];
    var userdata = { UserId: id };


    var table = $('#MajorProjectMasterListDatatable').DataTable({
        "ajax": {
            "url": apiLink + "MajorProjects/PostMajorProjects",
         //   "url": ManageProject/GetAllProjectMasterRecords",
            "type": "POST",
            "datatype": "JSON",
            "data": userdata,
            //"url": "/ManageProject/GetAllProjectMasterRecords",
         //   "method": "Get",
            //async: false,
           //"type": "GET",
           // "datatype": "JSON"
          //  "data": { "MilestoneStatusValue": hdnMilestoneStatusValue }
        },
        "autoWidth": false, // for disabling autowidth
        'aaSorting': [[2, 'asc']],
        "Paging": true,
        "lengthMenu": [[1, 10, 25, 50], [1, 10, 25, 50]],
        "serverSide": true, // for process server side
        "processing": true,
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "bInfo": false,
        "iTotalRecords": "3",
      
        "iTotalDisplayRecords": "3",
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        //"oSearch": { "sSearch": hdnMilestoneStatusValue }    ,
        "columns": [
        { "data": "CompanyID", "name": "CompanyID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        { "data": "MajorProjectID", "name": "MajorProjectID", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "ProjectName", "name": "ProjectName", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "StreetAddress", "name": "StreetAddress", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "CompanyName", "name": "CompanyName", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "ProjectShortName", "name": "ProjectShortName", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Contact1Name", "name": "Contact1Name", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Contact2Name", "name": "Contact2Name", "Searchable": true, "sClass": "txtAlignCenter" },
        //{ "data": "Address", "name": "Job_Address", "Searchable": true },
        //{ "data": "CompanyName", "name": "CompanyName", "Searchable": true },
        //{ "data": "Banks", "name": "Banks", "Searchable": true, "sClass": "txtAlignCenter" },
        //{ "data": "Units", "name": "UnitCount", "Searchable": true, "sClass": "txtAlignCenter" },

        //{
        //    "data": "ContractorSuper", "name": "ContractorSuper", "Searchable": true
        //    ,
        //    "render": function (data, type, row) {

        //        if ((hdnUserRoleId == 1) || (hdnUserRoleId == 3)) {
        //            return '<Span>' + data + '</span>';
        //        }
        //        else {
        //            return '<Span>' + row.SchindlerSuperintendent + '</span>';
        //        }
        //    }
        //},
 


        {
            "data": "MajorProjectID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                var msgCnt = "";
                //if (row.MessageCount == "0") { msgCnt = "" } else { msgCnt = '<span class="infocst">' + row.MessageCount + '</span>'; }
                var links = '';
                links += '';
             //   <a id="1" href="editProject.html" class="btnsml_action e-edit" name="useredit" campid="1" data-original-title="" title=""></a>

                links += '<a class="btnsml_action e-edit" name="projectedit"  href="javascript:void(0)" onclick="GoToEditProject(' + data + ')"></a>'
                links += '<a class="btnsml_action e-projectassign href="javascript:void(0)"  onclick="GoToEditProject(' + data + ')"></a>'
                links += '<a class="btnsml_action e-communicate" href="javascript:void(0)" onclick="GoToEditProject(' + data + ')"></a>'
                links += '<a class="btn btn-config" href="javascript:void(0)"  onclick="GoToEditProject(' + data + ')"><i class=" fa fa-cogs"></i></a>'
                links += '<a class="btn btn-config" href="javascript:void(0)"  onclick="GoToEditProject(' + data + ')"><i class=" fa fa-cloud-upload"></i></a>  </a>'
                return links
                // + '<a class="btnsml_action e-contact" href="javascript:void(0)" data-toggle="modal" data-target="#gotoContact" onclick="GoToContacts(' + data + ')"></a>' 
                // '<a href="javascript:void(0)" class="btnsml_action e-communicate" data-toggle="modal" data-target="#gotoMessage" onclick="GoToMessages(\'' + data + '\')">' + msgCnt + '</a>'

            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click-ProjectMasterList editData');

        },
        "initComplete": function (settings, json) {
            //ProjectMasterListDatatablePaging();
            //ProjectTooltip();
        },
        "drawCallback": function (settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });
    
  //  $('#MajorProjectMasterListDatatable_filter').hide(); // To remove autogenerated server textbox
   // $('.dataTables_length').hide(); // To hide page size selection
    
    $('#MajorProjectMasterListDatatable_paginate').css("display", "block"); // To show pagination

    $('#MajorProjectMasterListDatatableSearch').unbind().keyup(function () { // to handle search functionality

        var value = $(this).val();
        if (value.length > 2) {
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });
});

    function GoToSummary(projectId) {
        window.location.href = '/ManageProject/Edit/' + projectId;
    }
    function GoToContacts(projectId) {
        window.location.href = '/ManageProject/ProjectMasterView/' + projectId + '/Contacts';
    }
    function GoToMessages(projectId) {
        window.location.href = '/ManageProject/ProjectMasterView/' + projectId + '/Messages';
    }
    function GoToEditProject(projectId) {
        window.location.href = '/MajorProjects/Edit/' + projectId;
    }