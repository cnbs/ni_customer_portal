﻿$(document).ready(function () {
    var hdnPageSize = parseInt($('#hdnPageSize').val());
    var hdnUserRoleId = $('#hdnUserRoleId').val();
    var hdnMilestoneStatusValue = $('#hdnMilestoneStatusValue').val();
    var table = $('#ProjectMasterListDatatable').DataTable({
        "ajax": {
            "url": "/ManageProject/GetAllProjectMasterRecords",
            "type": "POST",
            "datatype": "JSON",
            "data": { "MilestoneStatusValue": hdnMilestoneStatusValue }
        },
        "autoWidth": false, // for disabling autowidth
        'aaSorting': [[2, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "serverSide": true, // for process server side
        "processing": true,
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "bInfo": false,
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "oSearch": { "sSearch": hdnMilestoneStatusValue }
        ,
        "columns": [        
        { "data": "ProjectID", "name": "ProjectID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
        { "data": "ProjectNo", "name": "ProjectNo", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "ProjectName", "name": "ProjectName", "Searchable": true },
        { "data": "Address", "name": "Job_Address", "Searchable": true },
        { "data": "CompanyName", "name": "CompanyName", "Searchable": true },
        { "data": "Banks", "name": "Banks", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "Units", "name": "UnitCount", "Searchable": true, "sClass": "txtAlignCenter" },

        {
            "data": "ContractorSuper", "name": "ContractorSuper", "Searchable": true
            ,
            "render": function (data, type, row) {

                if ((hdnUserRoleId == 1) || (hdnUserRoleId == 3)) {
                    return '<Span>' + data + '</span>';
                }
                else {
                    return '<Span>' + row.SchindlerSuperintendent + '</span>';
                }
            }
        },
        {
            "data": "ContractorProjMgr", "name": "ContractorProjMgr", "Searchable": true
            ,
            "render": function (data, type, row) {

                if ((hdnUserRoleId == 1) || (hdnUserRoleId == 3)) {
                    return '<Span>' + data + '</span>';
                }
                else {
                    return '<Span>' + row.SchindlerProjMgr + '</span>';
                }

            }
        },
        
        {
            "data": "Status" ,
            "name": "Status", "Searchable": true,
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                switch (data)
                {
                    case "Booked":
                    {
                        return '<span class="label s-approved">Booked</span>';
                            
                        }
                    case "In Approval Stage":
                    {
                        return '<span class="label s-pending">In Approval Stage</span>';
                            
                        }
                    case "Closed":
                    {
                        return '<span class="label s-completed">Closed</span>';
                            
                        }
                    case "Released to Manufacturing":
                    {
                        return '<span class="label s-progress">Released to Manufacturing</span>';
                            
                        }
                    case "Material Delivered":
                    {
                        return '<span class="label s-progress">Material Delivered</span>';
                            
                        }
                    case "Installation Started":
                    {
                        return '<span class="label s-progress">Installation Started</span>';
                            
                        }

                    case "Installation Complete":
                    {
                        return '<span class="label s-progress">Installation Complete</span>';
                            
                        }
                        
                    default:
                    {
                        return data;
                    }
                }

            }

        },

        {
            "data": "ProjectNo",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                var msgCnt = "";
                if (row.MessageCount == "0") { msgCnt = "" } else { msgCnt = '<span class="infocst">' + row.MessageCount + '</span>'; }
                return '<a class="btnsml_action e-sammery" href="javascript:void(0)" data-toggle="modal" data-target="#gotoSummary" onclick="GoToSummary(' + data + ')"></a>'
                    // + '<a class="btnsml_action e-contact" href="javascript:void(0)" data-toggle="modal" data-target="#gotoContact" onclick="GoToContacts(' + data + ')"></a>' 
                   // '<a href="javascript:void(0)" class="btnsml_action e-communicate" data-toggle="modal" data-target="#gotoMessage" onclick="GoToMessages(\'' + data + '\')">' + msgCnt + '</a>'

            }
        }

        ],
            createdRow: function (row, data, index) {
                $(row).addClass('table-click-ProjectMasterList editData');
           
            },
        "initComplete": function (settings, json) {
            ProjectMasterListDatatablePaging();
            ProjectTooltip();
        },
        "drawCallback": function (settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });

    $('.dataTables_length').hide(); // To hide page size selection

    if($('#hdnMilestoneStatusValue').val() != '' || $('#hdnMilestoneStatusValue').val() != null){
        $('#ProjectMasterListDatatableSearch').val($('#hdnMilestoneStatusValue').val());
    }

    $('#ProjectMasterListDatatable_filter').hide(); // To remove autogenerated server textbox
    $('#ProjectMasterListDatatable').on('draw.dt', function () { // To set design again
        ProjectMasterListDatatablePaging();
        ProjectTooltip();
    });
    
    $('#ProjectMasterListDatatableSearch').val(hdnMilestoneStatusValue).trigger($.Event("keyup", { keyCode: 13 }));
    $('#ProjectMasterListDatatableSearch').unbind().keyup(function () { // to handle search functionality
        
        var value = $(this).val();
        if (value.length > 2) {
            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });
});
function ProjectMasterListDatatablePaging() {
    $('#ProjectMasterListDatatable_paginate').children('ul').addClass('flatpagi');
    $('#ProjectMasterListDatatable_previous').children('a').remove();
    $('#ProjectMasterListDatatable_previous').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#ProjectMasterListDatatable_next').children('a').remove();
    $('#ProjectMasterListDatatable_next').append('<a href="javascript:void(0)"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}
function GoToSummary(projectId) {
    window.location.href = '/ManageProject/ProjectMasterView/' + projectId + '/Summary';
}
function GoToContacts(projectId) {
    window.location.href = '/ManageProject/ProjectMasterView/' + projectId + '/Contacts';
}
function GoToMessages(projectId) {
    window.location.href = '/ManageProject/ProjectMasterView/' + projectId + '/Messages';
}
function GoToEditProject(projectId) {
  
}

function ProjectTooltip() {
    $(".e-communicate").tooltip({ title: 'Communication', placement: 'top' });
    $(".e-sammery").tooltip({ title: 'View', placement: 'top' });
    $(".e-contact").tooltip({ title: 'Contacts', placement: 'top' });
}
$(document).on("click", ".table-click-ProjectMasterList", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className;
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];
            //var projectNo = $(clickedRow).find('td:not(:empty):first').text();
            var projectNo = $(clickedRow).find('td:not(:empty):first').siblings().eq(0).text();
            GoToSummary(projectNo);
        }

    }
});