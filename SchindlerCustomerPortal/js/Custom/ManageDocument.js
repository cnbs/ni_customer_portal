﻿
function convertjsonOnlydate(Anydate) {
    if (Anydate != null) {
        var dateString = Anydate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var hours = currentTime.getHours();
        var minute = currentTime.getMinutes();
        if (hours < 10) hours = "0" + hours;
        if (minute < 10) minute = "0" + minute;
        var date = month + "/" + day + "/" + year.toString();
        return date;
    }
    else {
        return null;
    }
}

function editDocument(id) {
    //umesh
    $.ajax({
        type: "GET",
        url: "/ManageDocument/EditDocument/",
        data: { docId: id },
        success: function (documentdetails) {

            $('#editDocumentName').val("");
            $('#editDocumentDesc').val("");
            $('#editdoctype').val($("#editdoctype option:first").val());
            $('#editCompanyName').val("");
            $('#editProjectName').val("");
            $('#editJobName').val("");
            $('#editdpExpDate').val("");
            $('#editIsExpiryDate').prop('checked', false);
            $('#editIsPublished').prop('checked', false);
            $('#editattchFilename').empty();

            Dropzone.forElement("#editdropzoneForm").removeAllFiles(true);


            //if (documentdetails.ExpiredDate != null)
            //{
            //    $('#editdpExpDate').val(Expdate);
            //    $('#editIsExpiryDate').prop('checked', true);
            //}
            var FileName = documentdetails.DocumentPath;
            var myString = FileName.substr(FileName.indexOf("_") + 1);

           // var Expdate = convertjsonOnlydate(documentdetails.ExpiredDate);
            var Expdate = documentdetails.ExpiredDate;

            //var Publishdate = convertjsonOnlydate(documentdetails.PublishedOn);
            $('#editDocumentid').val(documentdetails.DocumentID);
            $('#editDocumentName').val(documentdetails.DocumentName);
            $('#editdoctype').val(documentdetails.DocumentTypeID);
            $('#editDocumentDesc').val(documentdetails.Description);
            $('#editCompanyName').val(documentdetails.CompanyName);
            $('#editProjectName').val(documentdetails.ProjectName);
            $('#editJobName').val(documentdetails.JobName);

            if (FileName != null && FileName != "" && FileName != undefined) {
                $("<span class='forPadding'><a href='javascript:void(0)' name='attachment' class='removeFile'>" + myString + "</a><a href='javascript:void(0)' name='attachmentRemove'><b> Remove</b></a></span>").appendTo("#editattchFilename");
            }

            //$('#editpublishDate').val(Publishdate);
            if (Expdate != null && Expdate != "" && Expdate != undefined) {
                $('#editdpExpDate').show();
                $('#editdpExpDate').val(Expdate);
                $('#editIsExpiryDate').prop('checked', true);
            }
            else {
                $('#editdpExpDate').hide();
                $('#editdpExpDate').val("");
                $('#editIsExpiryDate').prop('checked', false);
            }


            if (documentdetails.IsPublished == true) {
                $('#editIsPublished').prop('checked', true);
                //$('#editdpExpDate').show();
            }
            else {
                $('#editIsPublished').prop('checked', false);
                //$('#editdpExpDate').show();
            }


            //PublishedOn

            //$('#editRoleID').prop('checked', (userdetail.RoleID == 3) ? true : false);

            //$('#editpicFilename').val(userdetail.ProfilePic);

        }
    })
}

$(document).on('click', 'a[name=attachmentRemove]', function () {
    

    //window.location.href = '/MailBox/DownloadAttachement?attachmentPath=' + escape(Fullpath);
    //$(this).closest('.removeFile').attr("Fullpath");
    $(".forPadding").remove();
});



$('#btnDeleteDocument').click(function () {

    $('#documentDeleteConfirmModel').modal('show');


});

$('#btnDeleteYes').click(function () {



    var DocID = $('#editDocumentid').val();

    DeleteDocument(DocID);


})

function DeleteDocument(id) {

    $.ajax({
        type: "POST",
        url: "/ManageDocument/DeleteDocument/",
        data: { DocID: id },
        success: function (userdetail) {
            $('#editDocument').modal('hide');
            var table = $('#Documenttable').DataTable();
            table.ajax.reload();
        }
    })
}

$(function () {

    $("#dpExpDate").datepicker();
});


function updateuserdocumentpermission(docid, userid, HasPermission, Type, rowid) {



    if ($('#' + rowid).is(':checked')) {
        HasPermission = true
    }
    else {
        HasPermission = false
    }
    //console.log("Type", Type);
    //var FlagValue = $("#" + rowid).attr("custvalue" + Type);
    $.ajax({
        type: "POST",
        url: "/ManageDocument/UpdateUserAccessPermission/",
        data: { DocID: docid, UserID: userid, HasPermission: HasPermission, Type: Type },
        success: function (response) {
            if (response.success) {
                //var THasPermission = (FlagValue == true || FlagValue == 'true') ? 'false' : 'true';
                //$("#" + rowid).attr("custvalue" + Type, THasPermission);


                //if (THasPermission == 'true' || THasPermission == true) {
                //    $('#' + rowid).removeClass("accessallowed");
                //}
                //else {
                //    $('#' + rowid).addClass("accessallowed");

                //}
                // bootbox.alert(response.message);

                if (HasPermission == 'true' || HasPermission == true) {
                    $('#' + rowid).addClass("accessallowed");
                }
                else {
                    $('#' + rowid).removeClass("accessallowed");
                }
            }
            else {
                bootbox.alert(response.message);
            }
        },
        error: function (err) {
            bootbox.alert(err.message);
        }
    })


}

function BindUserDocumentPermission(id) {

    var table = $('#dt_UserDocumentPermission').DataTable();

    table.destroy();

    $('#dt_UserDocumentPermission').DataTable({
        "paging": false,
        "info": false,
        "ajax": {
            "url": "/ManageDocument/UserDocumentPermission",
            "type": "Get",
            "datatype": "JSON",
            'data': {
                docID: id,
            },
        },
        "filter": true, // this is for disable filter (search box) 
        "oLanguage": { "sSearch": "" },
        "columns": [
            {
                "render": function (data, type, row) {

                    var viewResult = (row.HasViewPermission == 1) ? 'accessallowed' : '';
                    var uploadResult = (row.HasUploadPermission == 1) ? 'accessallowed' : '';
                    var deleteResult = (row.HasDeletePermission == 1) ? 'accessallowed' : '';

                    var rowViewPermission = (row.HasViewPermission == 1) ? false : true;
                    var rowChecked = (row.HasViewPermission == true) ? 'checked' : 'unchecked';

                    return '<div class="checkbox"><label><input type="checkbox" ' + rowChecked + '  value="" name="chkUserPermission" id="chkUserPermission_' + row.UserID + '_' + row.DocumentID + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowViewPermission + ',this.name,this.id)"><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> </label></div>'
                },
                "sClass": "txtLeftalign"

            },
        {
            "data": "UserName", "sClass": "txtLeftalign"

        },

        //{
        //    "data": "HasViewPermission",
        //    "render": function (data, type, row) {

        //        //var editResult = (row.HasEditPermission == 1) ? 'accessallowed' : '';
        //        var viewResult = (row.HasViewPermission == 1) ? 'accessallowed' : '';

        //        var uploadResult = (row.HasUploadPermission == 1) ? 'accessallowed' : '';
        //        var deleteResult = (row.HasDeletePermission == 1) ? 'accessallowed' : '';



        //        var rowViewPermission = (row.HasViewPermission == 1) ? false : true;
        //        var rowUploadPermission = (row.HasUploadPermisssion == 1) ? false : true;
        //        var rowDeletePermission = (row.HasDeletePermission == 1) ? false : true;

        //        return '<a class="btnsml_action e-view round ' + viewResult + '"  name="view" id="view_' + row.DocumentID + '_' + row.UserID + '" custvalueview="' + rowViewPermission + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowViewPermission + ',this.name,this.id)" href="#" data-isedit="' + row.HasViewPermission + '"> </a>' +
        //            '<a class="btnsml_action round e-upload ' + uploadResult + '" name="upload" id="upload_' + row.DocumentID + '_' + row.UserID + '" custvalueupload="' + rowUploadPermission + '" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowUploadPermission + ',this.name,this.id)" href="#" data-isedit="' + row.rowUploadPermission + '" > </a>' +
        //        '<a href="#" class="btnsml_action e-delete round ' + deleteResult + '" id="delete_' + row.DocumentID + '_' + row.UserID + '" custvaluedelete="' + rowDeletePermission + '" name="delete" onclick="updateuserdocumentpermission(' + row.DocumentID + ',' + row.UserID + ',' + rowDeletePermission + ',this.name,this.id)" href="#" data-isedit="' + row.HasDeletePermission + '" > </a>';
        //    }



        //},
         //{ "data": "HasAddPermission" },
         // { "data": "HasEditPermission" },
         //  { "data": "HasDeletePermission" },
         //   { "data": "HasPrintPermission" }

        ]


    });

}



function ViewDocumentDetail(id) {


    $.ajax({
        type: "GET",
        url: "/ManageDocument/EditDocument/",//UserEdit
        data: { docId: id },
        success: function (documentdetails) {

            //var name = userdetail.FirstName + ' ' + userdetail.LastName;
            //var role = (userdetail.RoleID == 3) ? 'Admin User' : 'Normal User';
            $('#DocumentName').html(documentdetails.DocumentName);
            $('#docdescription').html(documentdetails.Description);
            //$('#usercontact').html(userdetail.ContactNumber);
            //$('#useremail').html(userdetail.Email);
            //$('#userimage').attr('src', userdetail.ProfilePic);
            BindUserDocumentPermission(id);

            //var table = $('#UserDatatable').DataTable();
            //table.ajax.reload();
        }
    })
}

function DocumentDatatablePaging() {
    $('#Documenttable_paginate').children('ul').addClass('flatpagi');
    $('#Documenttable_previous').children('a').remove();
    $('#Documenttable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#Documenttable_next').children('a').remove();
    $('#Documenttable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}


$(document).ready(function () {



    $('#dpExpDate').hide();
    //$(function () {
    //    var availableTags = [
    //      "ActionScript",
    //      "AppleScript",
    //      "Asp",
    //      "BASIC",
    //      "C",
    //      "C++",
    //      "Clojure",
    //      "COBOL",
    //      "ColdFusion",
    //      "Erlang",
    //      "Fortran",
    //      "Groovy",
    //      "Haskell",
    //      "Java",
    //      "JavaScript",
    //      "Lisp",
    //      "Perl",
    //      "PHP",
    //      "Python",
    //      "Ruby",
    //      "Scala",
    //      "Scheme"
    //    ];
    //    $("#tags1").autocomplete({
    //        source: availableTags
    //    });
    //    $("#tags2").autocomplete({
    //        source: availableTags
    //    });
    //    $("#tags3").autocomplete({
    //        source: availableTags
    //    });
    //});
    $('#dpExpDate').hide();
    $('#editdpExpDate').hide();

    $('#AddIsExpiryDate').change(function () {
        if ($(this).is(":checked")) {
            $('#dpExpDate').show();
        }
        else {
            $('#dpExpDate').hide();
            $('#dpExpDate').val(null);
        }
    });

    $('#editIsExpiryDate').change(function () {
        if ($(this).is(":checked")) {
            $('#editdpExpDate').show();
        }
        else {
            $('#editdpExpDate').hide();
            $('#editdpExpDate').val(null);
        }
    });
    //Add document side
    var CompId = 0;

    var txtAddCompanyoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCompany/",
                data: '{ searchTerm:"' + $('#addCompanyName').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {
                            label: item.CompanyName,
                            value: item.CompanyID,
                            CityID: item.CompanyID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {

            $("#addCompanyName").val(ui.item.label);
            CompId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },

        minLength: 1,
    };



    $(document).on("keydown.autocomplete", '#addCompanyName', function (event) {

        $(this).autocomplete(txtAddCompanyoptions);

    })


    //Edit Document side
    var EditCompId = 0;
    var txtEditCompanyoptions = {

        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCompany/",
                data: '{ searchTerm:"' + $('#editCompanyName').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {
                            label: item.CompanyName,
                            value: item.CompanyID,
                            CityID: item.CompanyID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editCompanyName").val(ui.item.label);
            EditCompId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#editCompanyName', function (event) {

        $(this).autocomplete(txtEditCompanyoptions);

    })

    var Proid = 0;
    //Add project data of company id in add document
    //var Pname = $('#addProjectName').val();
    var txtAddProjectoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindProjectOfCompany/",
                data: '{ searchTerm:"' + $('#addProjectName').val() + '", CompanyId:"' + CompId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {

                            label: item.ProjectName,
                            value: item.ProjectNo,
                            CityID: item.ProjectNo,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addProjectName").val(ui.item.label);
            Proid = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#addProjectName', function (event) {

        $(this).autocomplete(txtAddProjectoptions);

    })

    //

    //Edit project data on Edit doc
    var EditProId = 0;

    var txtEditProjectoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindProjectOfCompany/",
                data: '{ searchTerm:"' + $('#editProjectName').val() + '", CompanyId:"' + EditCompId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {

                            label: item.ProjectName,
                            value: item.ProjectNo,
                            CityID: item.ProjectNo,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editProjectName").val(ui.item.label);
            EditProId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#editProjectName', function (event) {

        $(this).autocomplete(txtEditProjectoptions);

    })

    //Add job data for doc
    var AddJobId = 0;
    var EditJobId = 0;
    var txtAddJoboptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindJobOfProject/",
                data: '{ searchTerm:"' + $('#addJobName').val() + '", ProjectNo:"' + Proid + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {

                            label: item.JobName,
                            value: item.JobId,
                            CityID: item.JobId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#addJobName").val(ui.item.label);
            AddJobId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#addJobName', function (event) {

        $(this).autocomplete(txtAddJoboptions);

    })


    var txtEditJoboptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindJobOfProject/",
                data: '{ searchTerm:"' + $('#editJobName').val() + '", ProjectNo:"' + EditProId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {

                            label: item.JobName,
                            value: item.JobId,
                            CityID: item.JobId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editJobName").val(ui.item.label);
            EditJobId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");


            } else {

            }
        },
        minLength: 1,
    };


    $(document).on("keydown.autocomplete", '#editJobName', function (event) {

        $(this).autocomplete(txtEditJoboptions);

    })
    //


    var hdnPageSize = parseInt($('#hdnPageSize').val());

    var RoleId = $('#hdnUserRoleId').val();

    //var Doctypename = $('#SelectDocumentType').val();
    var CompanyId = $('#hdnUserCompanyId').val();

    var model = new Object();
    //model.DocumentType = Doctypename;
    if (CompanyId != 0) {
        model.CompanyID = CompanyId;
    }

    var table = $('#Documenttable').DataTable({

        "ajax": {
            "url": "/ManageDocument/GetDocumentAll",
            "type": "POST",
            "datatype": "JSON",
            "data": model

        },

        //"columnDefs": [{ 'bSortable': false, 'targets': [0, 2] }],
        //'aaSorting': [[5, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",



        "columns": [

        {
            "data": "DocumentName", "Searchable": true,
            "render": function (data, type, row) {
                
                var xyz = '<a target="_new" href="/ManageDocument/ViewDocumentName?DocPath=' + escape(row.DocumentPath) + '">' + data + '</a>';

                return xyz;

                //return '<a target="_new" href="/ManageDocument/ViewDocumentName?DocPath=' + row.DocumentPath + '">' + data + '</a>'
                //return '<a href="#", name="ViewDocumentName" Fullpath=' + row.DocumentPath + '> ' + data + ' </a>'
                //return '<a href="#" onclick="location.href="@Url.Action("ViewDocumentName")"">Something</a>'
                //return '<a href=@Url.Action("ViewDocumentName", new { DocPath = ' + row.DocumentPath + ' })> ' + data + ' </a>'
            }

        },

         {
             "data": "Description", "name": "Description", "Searchable": true,

             "render": function (data, type, row) {
                 return data.substr(0, 50)
             }
         },
         {
             "data": "ExpiredDate",
             "name": "ExpiredDate",
             "sClass": "txtAlignCenter",
             //var Expdate =convertjsonOnlydate(data)
             "render": function (data, type, row) {

                 return convertjsonOnlydate(data)
             }
         },

          { "data": "CompanyName", "name": "CompanyName" },
          {
              "data": "PublishedStatus",
              "name": "Published",
              "sClass": "txtAlignCenter",
              //"render": function (data, type, row) {

              //    return convertjsonOnlydate(data)
              //}
          },
        {
            "data": "DocumentID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var output =
                    (month < 10 ? '0' : '') + month + '/' +
                    (day < 10 ? '0' : '') + day + '/' +
                    d.getFullYear();



                var Expdate = convertjsonOnlydate(row.ExpiredDate);





                if (Expdate > output) {
                    if (RoleId == 1 || RoleId == 3) {

                        return '<a name="EditDocumentButtonname" class="btnsml_action e-edit" href="#" data-toggle="modal"  data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>    <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                    }
                    else {

                        return ' <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                    }


                }
                else {
                    if (RoleId == 1 || RoleId == 3) {
                        return '<a name="EditDocumentButtonname" class="btnsml_action e-edit" href="#" data-toggle="modal" data-target="#editDocument" onclick="editDocument(' + data + ')"> </a>  <a href="#" name="DownloadDocumentName" Fullpath=' + escape(row.DocumentPath) + ' class="btnsml_action e-download" > </a>'
                    }
                    else {
                        return ''
                    }



                }


            }
        }

        ],
        "initComplete": function (settings, json) {
            DocumentDatatablePaging();
        }


    });

    $('#Documenttable').on('draw.dt', function () {
        DocumentDatatablePaging();
    });

    if (RoleId != 1 && RoleId != 3) {

        fnShowHide(3);
    }

    function fnShowHide(iCol) {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var oTable = $('#Documenttable').dataTable();

        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, bVis ? false : true);
    }

    $("#Documenttable").on('click', 'a[name=DownloadDocumentName]', function () {

        //var Contentid = $(this).attr('astructid');
        var Fullpath = $(this).attr('fullpath');

        //window.location.href = '/CompanyAdmin/DownloadAsset?ContentId=' + Contentid + '&AssetPath=' + Fullpath;
        window.location.href = '/ManageDocument/DownloadDocument?DocPath=' + escape(Fullpath);

    });

    $('#AddDocumentClose').click(function () {

        $('#addDocumentName').val("");
        $('#addDocumentDesc').val("");
        $('#adddoctype').val($("#adddoctype option:first").val());
        $('#addCompanyName').val("");
        $('#addProjectName').val("");
        $('#addJobName').val("");
        $('#dpExpDate').val("");
        $('#AddIsExpiryDate').prop('checked', false);
        $('#AddIsPublished').prop('checked', false);
        $('#dpExpDate').hide();

        Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
    });
    //$("#Documenttable").on('click', 'a[name=ViewDocumentName]', function () {

    //    //var Contentid = $(this).attr('astructid');
    //    var Fullpath = $(this).attr('fullpath');

    //    //window.location.href = '/CompanyAdmin/DownloadAsset?ContentId=' + Contentid + '&AssetPath=' + Fullpath;
    //    // window.location.href = '/ManageDocument/ViewDocumentName?DocPath=' + escape(Fullpath);
    //    $.ajax({
    //        url: '/ManageDocument/ViewDocumentName/',
    //        data: '{ DocPath: "' + Fullpath + '"}',
    //        dataType: 'json',
    //        type: 'POST',
    //        success: function (data) {
    //        },
    //        error: false
    //    });

    //});

    function successdoctype(data) {
        var obj = data;
        //'<option value="">-- select please --</option>'
        var options = null;
        for (var i = 0; i < obj.length; i++) {
            //options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
            options += '<option name = "doctypeoption" value="' + obj[i].DocumentTypeID + '">' + obj[i].DocumentTypeName + '</option>';
        }
        $("select#adddoctype").html(options);

        $("select#editdoctype").html(options);


    };

    function errordoctype(data) {

        return false;

    };

    $.ajax({
        url: '/Common/BindDocumentType',
        type: 'GET',
        success: successdoctype,
        error: errordoctype
    });

    //$.ajax({
    //    url: '/Common/BindCompany',
    //    type: 'GET',
    //    success: successdoctype,
    //    error: errordoctype
    //});


    //var USACountry = 1;
    //var cityIdSelected = 0;






    $('#DocumentDatatableSearch').unbind().keyup(function () {
        var value = $(this).val();
        if (value.length > 2) {

            table.search(value).draw();
        }
        if (value == '') {
            table.search(value).draw();
        }
    });

    var Did = 0;
    var userIdListP = [];

    $('#chkAllUserPermission').change(function () {

        if ($(this).is(":checked")) {
            $('input[name="chkUserPermission"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                var DocID = result[2];
                userIdListP.push(UserID);
                Did = DocID;
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUserPermission"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                Did = DocID;
                var DocID = result[2];

                userIdListP = jQuery.grep(userIdListP, function (value) {
                    return value != UserID;
                });
            });
        }
    });



    //$('#btnAllDeletePermission').click(function (event) {
    //    event.preventDefault();
    //    var IsValiddd = true;
    //    if (userIdListP.length == 0) {
    //        IsValiddd = false;
    //    }

    //    var result = userIdListP.join(", ")
    //    if (IsValiddd) {
    //        $.ajax({
    //            type: "POST",
    //            url: "/ManageDocument/MultipleUserPermissionDelete",
    //            data: { docid: Did, userids: result },
    //            success: function (userdetail) {
    //                $('#chkAllUserPermission').prop('checked', false);
    //                bootbox.alert(userdetail.message);
    //                var table = $('#dt_UserDocumentPermission').DataTable();
    //                table.ajax.reload();
    //            }
    //        })
    //    }


    //})

    $('#addDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#editDocumentName').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#btnAddDocument').click(function (event) {
        event.preventDefault();

        var isValid = true;

        var doctype = $("#adddoctype option:selected").text();

        //if (doctype != "Other") {

        //    if ($('#addDocumentName').val() == '') {
        //        $('#addDocumentName').css({ "border": "1px solid red" });
        //        isValid = false;
        //        $('#addDocumentName').focus();
        //    }
        //    else {
        //        $('#addDocumentName').css({ "border": "", "background": "" });
        //    }

        //    if ($('#addCompanyName').val() == '') {
        //        $('#addCompanyName').css({ "border": "1px solid red" });
        //        isValid = false;
        //        $('#addCompanyName').focus();
        //    }
        //    else {
        //        $('#addCompanyName').css({ "border": "", "background": "" });
        //    }

        //}
        //else {

        if ($('#addCompanyName').val() == '') {
            $('#addCompanyName').css({ "border": "1px solid red" });
            isValid = false;
            $('#addCompanyName').focus();
        }
        else {
            $('#addCompanyName').css({ "border": "", "background": "" });
        }

        if ($('#addDocumentName').val() == '') {
            $('#addDocumentName').css({ "border": "1px solid red" });
            isValid = false;
            $('#addDocumentName').focus();
        }
        else {
            $('#addDocumentName').css({ "border": "", "background": "" });
        }

        //$('#addCompanyName').css({ "border": "", "background": "" });

        //}


        var filename = $('div.dz-filename span').text();
        if (filename == null || filename == "" || filename == undefined) {

            $('#dropzoneForm').css({ "border": "1px solid red" });
            //alert('Please Upload File');
            isValid = false;
        }
        else {
            $('#dropzoneForm').css({
                "border": "",
                "background": ""
            });
        }

        var fileerrormsg = $('#dropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {
            //alert('Msg-' + fileerrormsg);
            $('#dropzoneForm').css({ "border": "1px solid red" });
            isValid = false;
        }
        else {
            //alert('Msg- Null');
        }



        if (isValid == false)
            return false;

        var formData = new FormData();
        //var files = $("#attachDocument").get(0).files;
        //var filesize = $("#attachDocument")[0].files[0].size;
        //if (filesize > 8388608)   //33554432
        //{
        //    alert('File size shold be less than 32 mb');
        //    return false;
        //}

        var Publishedstatus;





        if ($("#AddIsPublished").is(':checked'))
            // checked
            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentName", $('#addDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#adddoctype').val().trim());
        formData.append("Description", $('#addDocumentDesc').val().trim());
        formData.append("CompanyName", $('#addCompanyName').val());
        formData.append("ProjectName", $('#addProjectName').val());
        formData.append("JobName", $('#addJobName').val());
        formData.append("ExpiredDate", $('#dpExpDate').val());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);


        if (isValid) {
            $.ajax({
                url: '/ManageDocument/AddDocument',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    $('#AddDocument').modal('hide');
                    $('#SuccessDocument').modal('show');
                    var table = $('#Documenttable').DataTable();
                    table.ajax.reload();

                },
                error: function (data) {
                    alert('error')
                }
            })
        }
        else {
            return false;
        }


    });

    $('#documentok').click(function () {
        $('#SuccessDocument').modal('hide');
        //var table = $('#Documenttable').DataTable();
        //table.ajax.reload();
    });

    $('#editdocumentok').click(function () {
        $('#SuccessEditDocument').modal('hide');
        //var table = $('#Documenttable').DataTable();
        //table.ajax.reload();
    });

    $('#btnUpdateDocument').click(function (event) {

        //event.preventDefault();
        var formData = new FormData();

        var isValid = true;

        var doctype = $("#editdoctype option:selected").text();

        //if (doctype != "Other") {

        //    if ($('#editDocumentName').val() == '') {
        //        $('#editDocumentName').css({ "border": "1px solid red" });
        //        isValid = false;
        //        $('#editDocumentName').focus();
        //    }
        //    else {
        //        $('#editDocumentName').css({ "border": "", "background": "" });
        //    }

        //    if ($('#editCompanyName').val() == '') {
        //        $('#editCompanyName').css({ "border": "1px solid red" });
        //        isValid = false;
        //        $('#editCompanyName').focus();
        //    }
        //    else {
        //        $('#editCompanyName').css({ "border": "", "background": "" });
        //    }

        //}
        //else {

            if ($('#editCompanyName').val() == '') {
                $('#editCompanyName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editCompanyName').focus();
            }
            else {
                $('#editCompanyName').css({ "border": "", "background": "" });
            }

            if ($('#editDocumentName').val() == '') {
                $('#editDocumentName').css({ "border": "1px solid red" });
                isValid = false;
                $('#editDocumentName').focus();
            }
            else {
                $('#editDocumentName').css({ "border": "", "background": "" });
            }

            //$('#editCompanyName').css({ "border": "", "background": "" });

        //}

        var filename = $('#editdropzoneForm div.dz-filename span').text();
        //if (filename == null || filename == "" || filename == undefined) {

        //    $('#editdropzoneForm').css({ "border": "1px solid red" });
        //    //alert('Please Upload File');
        //    isValid = false;
        //}
        //else {
        //    $('#editdropzoneForm').css({
        //        "border": "",
        //        "background": ""
        //    });
        //}

        var fileerrormsg = $('#editdropzoneForm div.dz-error-message').text();
        if (fileerrormsg != null && fileerrormsg != "" && fileerrormsg) {
            //alert('Msg-' + fileerrormsg);
            $('#editdropzoneForm').css({ "border": "1px solid red" });
            isValid = false;

        }
        else {

        }

        if (isValid == false)
            return false;

        var formData = new FormData();

        var Publishedstatus;

        //formData.append("docs", files[0]);
        //var filename = $('div.dz-filename span').text();
        if ($("#editIsPublished").is(':checked'))
            // checked
            Publishedstatus = true;
        else
            Publishedstatus = false;

        formData.append("DocumentID", $('#editDocumentid').val().trim());
        formData.append("DocumentName", $('#editDocumentName').val().trim());
        formData.append("DocumentTypeID", $('#editdoctype').val().trim());
        formData.append("Description", $('#editDocumentDesc').val().trim());
        formData.append("CompanyName", $('#editCompanyName').val().trim());
        formData.append("ProjectName", $('#editProjectName').val().trim());
        formData.append("JobName", $('#editJobName').val().trim());
        formData.append("ExpiredDate", $('#editdpExpDate').val().trim());
        formData.append("IsPublished", Publishedstatus);
        formData.append("FileName", filename);

        $.ajax({
            url: '/ManageDocument/UpdateDocument',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                $('#editDocument').modal('hide');
                $('#SuccessEditDocument').modal('show');
                var table = $('#Documenttable').DataTable();
                table.ajax.reload();


            },
            error: function (data) {
                alert('error')
            }
        })
    });

    var userIdList = [];

    $('#chkAllUser').change(function () {
        if ($(this).is(":checked")) {
            $('input[name="chkUser"]').each(function () {
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];
                userIdList.push(UserID);
                this.checked = true;
            });
        }
        else {
            $('input[name="chkUser"]').each(function () {
                this.checked = false;
                var iddd = $(this).attr('id');
                var result = iddd.split('_');
                var UserID = result[1];

                userIdList = jQuery.grep(userIdList, function (value) {
                    return value != UserID;
                });
            });
        }
    });

    $('#btnAllDelete').click(function (event) {
        event.preventDefault();
        

        var IsValiddd = true;
        if (userIdList.length == 0) {
            IsValiddd = false;
        }

        var result = userIdList.join(", ")

        if (IsValiddd) {
            $.ajax({
                type: "POST",
                url: "/ManageUser/MultipleUserDelete",
                data: { userID: result },
                success: function (userdetail) {
                    $('#chkAllUser').prop('checked', false);
                    var table = $('#UserDatatable').DataTable();
                    table.ajax.reload();
                }
            })
        }


    })

    $("#tblCategory_paginate").on("click", "a", function () {
        $('#chkAllUser').prop('checked', false);
        $('input[name="chkUser"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var UserID = result[1];

            userIdList = jQuery.grep(userIdList, function (value) {
                return value != UserID;
            });
        });
    });


    $('.dataTables_length').hide();

    $('#Documenttable_filter').hide();
})

