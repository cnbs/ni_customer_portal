﻿
var PageName = window.location.pathname;

if (!PageName.match("^/Calendar")) {
    BindStateForUser();
}


var pageNumber = 0;
var refCount = 0;
var updateCounterValue = 0;
var storedStatus;
var job = '';
var project = '';
var projectName = '';

var joblistpageNumber = 0;
var joblistrefCount = 0;
var pageajaxcall = '0';
var pieChartHTML = '<div class="box-piechart" id="show-PieChart"><div id="piechart" style="overflow:hidden;"></div></div> ';
// <div class="box-piechart" id="show-PieChart"></div>  
function NewlyRegisterUSerDatatablePaging() {
    $('#newlyRegisteredUserDataTable_paginate').children('ul').addClass('flatpagi');
    $('#newlyRegisteredUserDataTable_previous').children('a').remove();
    $('#newlyRegisteredUserDataTable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#newlyRegisteredUserDataTable_next').children('a').remove();
    $('#newlyRegisteredUserDataTable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function toDate(dateStr) {
    var parts = dateStr.split("-");
    return (parts[1] + '-' + parts[2] + '-' + parts[0]);
}

function EditNewlyRegisteredUser(id) {

    $.ajax({
        type: "GET",
        url: "/ManageUser/EditNewlyRegisteredUser/",
        data: { userId: id },
        success: function (userdetail) {

            $('#editUserId').val(userdetail.UserID);
            $('#editFullname').val(userdetail.FirstName);

            $('#editPhone').val(userdetail.ContactNumber);

            $('#editrequestedprojects').val(userdetail.Requested_Projects);
            $('#editEmail').val(userdetail.Email);
            $('#editCompanyName').val(userdetail.CompanyName);

            $('#editStreetAddress').val(userdetail.Address1);
            $('#editState').val(userdetail.State);

            $('#editCity').val(userdetail.City);
            $('#editZipCode').val(userdetail.PostalCode);

            $("#editDesignation").val(userdetail.DesignationId);

            $('#editStatus').val(userdetail.AccountStatus);


            var verify = 17;
            var approved = 18;
            var pending = 4;
            var active = 1;

            if (userdetail.AccountStatus == 4) {
                $('#editStatus option[value="' + verify + '"]').prop('disabled', true);
                $('#editStatus option[value="' + approved + '"]').prop('disabled', true);
                $('#editStatus option[value="' + active + '"]').prop('disabled', true);
            }
            //else {
            //    $('#editStatus option[value="' + verify + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + approved + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + active + '"]').prop('disabled', false);
            //}

            if (userdetail.AccountStatus == 17) {
                $('#editStatus option[value="' + pending + '"]').prop('disabled', true);
                $('#editStatus option[value="' + active + '"]').prop('disabled', true);
            }
            //else {
            //    $('#editStatus option[value="' + pending + '"]').prop('disabled', false);
            //    $('#editStatus option[value="' + active + '"]').prop('disabled', false);
            //}


        }
    })
}

function GetCountOfLatestRegisteredUsers() {

    $.ajax({
        type: "POST",
        url: "/ManageUser/NewlyRegisterUserCount/",
        data: {},
        success: function (data) {
            $('#dashboardAllNewUser').text(data.usercount);
        }
    })
}

function ValidateEditUser() {
    var inValid = false;
    var editCompanyName = $('#editCompanyName').val().trim();

    var editPhone = $('#editPhone').val().trim();

    var editDesignation = $('#editDesignation').val().trim();


    if (editCompanyName == "") {
        inValid = true;
        $('#editCompanyName').css('border-color', 'red');
    }

    if (editPhone == "") {
        inValid = true;
        $('#editPhone').css('border-color', 'red');
    }

    if (editDesignation == "") {
        inValid = true;
        $('#editDesignation').css('border-color', 'red');
    }

    if (inValid == true)
        return false;
    else {
        return true;
    }

}

function UserStatusChange(statusID, UserID, userEmail) {
    $('#hdnStatusForUserID').val(UserID);

    if (statusID == 4) {
        // statusID 4 = Pending
    }
    if (statusID == 17) {
        // statusID 17 = Verify
        $('#statusText').html('Are you sure you want to approved this user?')
        $('#hdnStatusValue').val('18'); // statusID 18 = Approved
        $('#hdnStatusForUserEmail').val(userEmail);
        $('#userStatusModel').modal('show');
    }
}

function DisplayAllMessages() {
    window.location.href = '/mailbox/maillist';
}

function DisplayAllProjects() {
    window.location.href = '/ManageProject/ProjectMasterList';
}

function DashboardMsgWidget() {
    $.ajax({
        type: "POST",
        url: "/MailBox/UnreadMailForDashboardWidget/",
        //data: { userId: id },
        success: function (data) {

            var renderHTML = '';

            if (data.length == 0) {
                renderHTML += '<div class="mail-item  mail-read mail-info">';
                renderHTML += '<div class="mail-user">&nbsp;</div>';
                renderHTML += '<a href="javascript:void(0)" class="mail-text">No new messages</a>';
            }

            $.each(data, function (i, val) {

                var subjectName = commonSubString(val.Subject, 20);

                if (val.HasAttachment) {


                    if (val.ViewStatus == 'unread') {
                        renderHTML += '<div class="mail-item  mail-unread mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<strong><a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a></strong>';
                    }
                    else {
                        renderHTML += '<div class="mail-item  mail-read mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a>';
                    }
                    renderHTML += '<div class="mail-date">' + val.DateSent + '</div>';
                    renderHTML += '<div class="mail-attachments"><span class="fa fa-paperclip"></span></div></div>';

                }
                else {

                    if (val.ViewStatus == 'unread') {
                        renderHTML += '<div class="mail-item  mail-unread mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<strong><a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a></strong>';
                    }
                    else {
                        renderHTML += '<div class="mail-item  mail-read mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a>';
                    }
                    renderHTML += '<div class="mail-date">' + val.DateSent + '</div></div>';
                }

            })

            $('#allMsgList').append(renderHTML);

            $('#allMsgList').mCustomScrollbar();
        }
    })
}

function DashboardMsgWidget() {
    $.ajax({
        type: "POST",
        url: "/MailBox/UnreadMailForDashboardWidget/",
        //data: { userId: id },
        success: function (data) {

            var renderHTML = '';

            if (data.length == 0) {
                renderHTML += '<div class="mail-item  mail-read mail-info">';
                renderHTML += '<div class="mail-user">&nbsp;</div>';
                renderHTML += '<a href="javascript:void(0)" class="mail-text">No new messages</a>';
            }

            $.each(data, function (i, val) {

                var subjectName = commonSubString(val.Subject, 20);

                if (val.HasAttachment) {


                    if (val.ViewStatus == 'unread') {
                        renderHTML += '<div class="mail-item  mail-unread mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<strong><a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a></strong>';
                    }
                    else {
                        renderHTML += '<div class="mail-item  mail-read mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a>';
                    }
                    renderHTML += '<div class="mail-date">' + val.DateSent + '</div>';
                    renderHTML += '<div class="mail-attachments"><span class="fa fa-paperclip"></span></div></div>';

                }
                else {

                    if (val.ViewStatus == 'unread') {
                        renderHTML += '<div class="mail-item  mail-unread mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<strong><a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a></strong>';
                    }
                    else {
                        renderHTML += '<div class="mail-item  mail-read mail-info">';
                        renderHTML += '<div class="mail-user">' + val.UserName + '</div>';
                        renderHTML += '<a href="/mailbox/maillist?mail=' + val.MailId + '" class="mail-text">' + subjectName + '</a>';
                    }
                    renderHTML += '<div class="mail-date">' + val.DateSent + '</div></div>';
                }

            })

            $('#allMsgList').append(renderHTML);

            $('#allMsgList').mCustomScrollbar();
        }
    })
}

function MilestoneChart(chartData) {
    google.charts.load("visualization", "1", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Results', 'Contract Executed', 'Drawing Approved', 'Finishes Completed', 'Job-Site Ready', { role: 'annotation' }],
        ['Village At Rivers Edge Norfolk', 25, 25, 25, 25, ''],
        ['Milennium Tower Boston', 35, 25, 35, 5, ''],
        ['Residence Inn by Marriottbridgewate', 25, 25, 25, 25, ''],
        ['Mit Academic BUildings', 35, 25, 35, 5, '']
        ]);

        var data = new google.visualization.DataTable();

        if (chartData.length > 0) {
            var ColumnData = chartData[0].KeyData.split(',');
            for (i = 0; i < ColumnData.length; i++) {
                if (i == 0) {
                    data.addColumn('string', ColumnData[i]);
                }
                else {
                    data.addColumn('number', ColumnData[i]);
                }
                data.addColumn({ type: 'string', role: 'tooltip' });
            }
        }

        var concateValue = chartData[0].KeyData.split(',');

        for (var i = 1; i < chartData.length; i++) {
            var row = [];
            var RowData = chartData[i].KeyData.split(',');
            for (j = 0; j < RowData.length; j++) {
                if (j == 0) {
                    row.push(RowData[j]);
                }
                else {
                    row.push(RowData[j] == "" ? 0 : parseInt(RowData[j]));
                }
                row.push(RowData[0] + " " + concateValue[j]);
            }
            data.addRow(row);
        }

        var options = {
            //title: 'Standard',
            chartArea: { width: "50%", height: "70%" },
            legend: { position: 'right', maxLines: 5 },
            bar: { groupWidth: '80%' },
            isStacked: true,
            colors: ['#9e9e9e', '#16a085', '#27ae60', '#f39c12', '#9c27b0', '#ffeb3b', '#e91e63', '#795548', '#263238', '#34495e'],
            hAxis: {
                viewWindow: {
                    min: 0,
                    max: 100
                },
                title: 'Milestone Attianment',
                ticks: [0, 20, 40, 60, 80, 100] // display labels every 25
            },
            vAxis: {
                title: 'Project'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('barchart_material'));
        chart.draw(data, options);

        // Add our selection handler.
        google.visualization.events.addListener(chart, 'select', selectHandler);

        // The selection handler.
        // Loop through all items in the selection and concatenate
        // a single message from all of them.
        function selectHandler() {
            var selection = chart.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                    var projectNo = data.getValue(item.row, 0);
                } else if (item.row != null) {
                    var projectNo = data.getValue(item.row, 0);
                } else if (item.column != null) {
                    var projectNo = data.getValue(item.row, 0);
                }
            }
            //alert('You selected ' + message);
            if (projectNo != "") {
                window.location.href = '/ManageProject/ProjectMasterView/' + projectNo + '/Summary';
            }
        }

    }
}

function ProjectPieChart() {
    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'xxx'],
          ['Compeleted', 11],
          ['Progress', 2],
          ['Pending', 2]
        ]);

        $.ajax({

            type: "POST",

            url: "/ManageProject/GetProjectChart/",

            data: '{}',

            contentType: "application/json; charset=utf-8",

            dataType: "json",

            success: function (r) {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'StatusName');
                data.addColumn('number', 'ProjectCount');

                for (var i = 0; i < r.ProjectCount.length; i++) {
                    if ((parseInt(r.ProjectCount[i].ProjCount)) > 0) {
                        $('#dashboardAllProjectCount').html(r.ProjectCount[i].ProjCount); // set project count in widget
                        break;
                    }

                }


                for (var i = 0; i < r.projectChart.length; i++) {
                    data.addRow([r.projectChart[i].StatusName, parseInt(r.projectChart[i].ProjectCount)]);
                }

                var options = {
                    chartArea: { width: "50%", height: "70%" },
                    title: '',
                    pieHole: 0.65,
                    tooltip: { trigger: 'none' },
                    legend: { position: 'right', verticalAlign: 'middle' },
                    chartArea: { 'width': '100%', 'height': '80%' },
                    pieSliceText: 'value',
                    slices: { 0: { color: '#9e9e9e' }, 1: { color: '#16a085' }, 2: { color: '#27ae60' }, 3: { color: '#f39c12' }, 4: { color: '#9c27b0' }, 5: { color: '#ffeb3b' }, 6: { color: '#e91e63' }, 7: { color: '#795548' }, 8: { color: '#263238' }, 9: { color: '#34495e' } }
                };


                var chart = new google.visualization.PieChart(document.getElementById('ProjectPieChart'));
                chart.draw(data, options);

                MilestoneChart(r.stackedChart);

                // Add our selection handler.
                google.visualization.events.addListener(chart, 'select', selectHandler);

                // The selection handler.
                // Loop through all items in the selection and concatenate
                // a single message from all of them.
                function selectHandler() {
                    var selection = chart.getSelection();
                    var message = '';
                    for (var i = 0; i < selection.length; i++) {
                        var item = selection[i];
                        if (item.row != null && item.column != null) {
                            var str = data.getFormattedValue(item.row, item.column);
                            message += '{row:' + item.row + ',column:' + item.column + '} = ' + str + '\n';
                        } else if (item.row != null) {
                            var str = data.getFormattedValue(item.row, 0);
                            message += '{row:' + item.row + ', column:none}; value (col 0) = ' + str + '\n';
                        } else if (item.column != null) {
                            var str = data.getFormattedValue(0, item.column);
                            message += '{row:none, column:' + item.column + '}; value (row 0) = ' + str + '\n';
                        }
                    }
                    if (message == '') {
                        message = 'nothing';
                    }
                    //alert('You selected ' + message);
                    if (str != "") {
                        window.location.href = '/ManageProject/ProjectMasterList/' + str;
                    }
                }

            },

            failure: function (r) {
                alert(r.d);

            },

            error: function (r) {
                alert(r.d);
            }
        });
    }

}

function commonSubString(input, length) {

    if (input.length <= length) {
        input = input
    }
    else {
        input = input.substring(0, length) + '...'
    }

    return input
}

function DashboardTooltip() {
    $(".e-edit").tooltip({ title: 'Edit User', placement: 'top' });
}

function DashboardProjectStatusChart(searchTerm, pageNo) {
    var model = new Object();

    model.SearchTerm = searchTerm;
    model.PageNumber = pageNo;
    model.PageSize = 10;

    $.ajax({
        type: "POST",
        url: "/ManageProject/DashboardProjectListingMilestone/",
        data: model,
        async: false,
        success: function (data) {
            var renderHTML = '';
            //$('#tblBodyProjectJobMilestone').html('');
            pageNumber++;

            if (data.length <= 0) {
                renderHTML += '<center><h3>No assigned project found.</h3></center>';
                $('#dashboardAllProjectCount').html(0); // set 0 value
                $('#tblBodyProjectJobMilestone').append(renderHTML);
            }
            else {

                $.each(data, function (i, val) {
                    if (i == 0) {
                        $('#dashboardAllProjectCount').html(val.TotalProjectCount);
                    }

                    renderHTML += '<div class="row nomar"><div class="col-sm-12 wrapperBox"><div class="section_head">';
                    renderHTML += '<h2 class="project_title">PROJECT | ' + val.Title + '</h2>';
                    renderHTML += '<ul class="pull-right action_listing"><li>';

                    renderHTML += '<a class="btn-link-chart label label-info" href="javascript:void(0);" title="Payment" data-toggle="popoversmall" data-html="true" data-placement="top" data-container="body" data-content="" id="' + val.ProjectNo + 'Payment" onclick="showTinyPopup(\'Payment\', \' ' + val.ProjectNo + '\')">Billed: ' + val.BilledPercent + '| Paid: ' + val.PaidPercent + '</a></li>';

                    renderHTML += '<li><a href="javascript:void(0);" title="Invoice" onmouseover=\"mouseoverfun(this,1);\" onmouseout=\"mouseoutfun(this,1);\" data-toggle="popoversmall" data-trigger="click" data-html="true" data-placement="top" data-container="body" data-container="body"  data-content="" id="' + val.ProjectNo + 'Invoice" onclick="showTinyPopup(\'Invoice\', \' ' + val.ProjectNo + '\');" class="btn btn-info-chart">';
                    renderHTML += "<img src=\"/images/img/invoice-icon.png\"  class=\"img-responsive set-icon\" data-alt-image=\"/images/img/invoice-icon-roll.png\" />Invoice</a></li>";

                    renderHTML += '<li><a href="javascript:void(0);" title="Contract Docs" onmouseover=\"mouseoverfun(this,2);\" onmouseout=\"mouseoutfun(this,2);\" data-toggle="popoversmall" data-trigger="click" data-html="true" data-placement="top" data-container="body" data-container="body" data-content="" id="' + val.ProjectNo + 'Contract" onclick="showTinyPopup(\'Contract\', \' ' + val.ProjectNo + '\');" class="btn btn-info-chart">';
                    renderHTML += '<img src="/images/img/contract-icon.png" class="img-responsive set-icon" data-alt-image="/images/img/contract-icon-roll.png" />Contract Docs</a></li>';

                    renderHTML += '<li><a href="javascript:void(0);" title="Contact" onmouseover=\"mouseoverfun(this,3);\" onmouseout=\"mouseoutfun(this,3);\" data-toggle="popover" data-trigger="click" data-html="true" data-placement="top" data-container="body" data-container="body" data-content="" id="' + val.ProjectNo + 'Contact" onclick="showTinyPopup(\'Contact\', \' ' + val.ProjectNo + '\');" class="btn btn-info-chart">';
                    renderHTML += '<img src="/images/img/contact-icon.png" class="img-responsive set-icon" data-alt-image="/images/img/contact-icon-roll.png" />Contact</a></li></ul></div>';

                    renderHTML += '<div class="col-sm-12 nopad"><table class="table work_list_table tasklisting_table"><thead><tr>';
                    renderHTML += '<th class="project_block">&nbsp;</th>';
                    renderHTML += '<th class="task_block">Contract Executed</th><th class="task_block">Down Payment Rec’d</th>';
                    renderHTML += '<th class="task_block">Drawings Approved</th><th class="task_block">Finishes Complete</th>';
                    renderHTML += '<th class="task_block">Material in Storage Hub</th><th class="task_block" style="width:150px;">Job-Site Ready</th>';
                    renderHTML += '<th class="task_block">Installation Start</th><th class="task_block">Equipment Turnover</th>';
                    renderHTML += ' </tr></thead><tbody><tr><td><span>';

                    renderHTML += '<a href="/ManageProject/ProjectMasterView/' + val.ProjectNo + '/Summary" data-tooltip="tooltip" data-placement="right" class="txt_red" title="" data-original-title="' + val.ProjectName + '">Overall</a></span></td>';

                    //renderHTML += '<td align="center">Payments</td><td align="center">Invoice</td><td align="center">Documents</td>';
                    //renderHTML += '<td align="center">Contacts</td></tr>';
                    renderHTML += '<input type="hidden" id="hdnProjectCompanyId' + val.ProjectNo + '" value="' + val.ProjectCompanyId + '" />';


                    //if (val.BankList[i] != null && val.BankList[i].DueDate != undefined && val.BankList[i].DueDate != null) {
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.ContractExecuted, val.ProjectNo, val.JobNo, "Contract Executed", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.DrawingsApproved, val.ProjectNo, val.JobNo, "Drawings Approved", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.FinishesComplete, val.ProjectNo, val.JobNo, "Finishes Complete", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.MaterialOrdered, val.ProjectNo, val.JobNo, "Material Ordered", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.MaterialAtHub, val.ProjectNo, val.JobNo, "Material At Hub", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.ReadyToPull, val.ProjectNo, val.JobNo, "Ready To Pull", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.InstallationStart, val.ProjectNo, val.JobNo, "Installation Start", val.BankList[i].DueDate) + '</td>';
                    //    renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.FinalInspection, val.ProjectNo, val.JobNo, "Final Inspection", val.BankList[i].DueDate) + '</td></tr>';
                    //}
                    //else {
                    renderHTML += milestoneColorCodeReturn(val.ContractExecuted, val.ProjectNo, val.JobNo, "Contract Executed");
                    renderHTML += milestoneColorCodeReturn(val.DownPaymentRecord, val.ProjectNo, val.JobNo, "Down Payment Rec’d");
                    renderHTML += milestoneColorCodeReturn(val.DrawingsApproved, val.ProjectNo, val.JobNo, "Drawings Approved");
                    renderHTML += milestoneColorCodeReturn(val.FinishesComplete, val.ProjectNo, val.JobNo, "Finishes Complete");
                    //renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.MaterialOrdered, val.ProjectNo, val.JobNo, "Material Ordered") + '</td>';
                    renderHTML += milestoneColorCodeReturn(val.MaterialInStorageHub, val.ProjectNo, val.JobNo, "Material in Storage Hub");
                    //renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.MaterialAtHub, val.ProjectNo, val.JobNo, "Material in Storage Hub") + '</td>';
                    renderHTML += milestoneColorCodeReturn(val.ReadyToPull, val.ProjectNo, val.JobNo, "Job-Site Ready");
                    renderHTML += milestoneColorCodeReturn(val.InstallationStart, val.ProjectNo, val.JobNo, "Installation Start");
                    renderHTML += milestoneColorCodeReturn(val.EquipmentTurnover, val.ProjectNo, val.JobNo, "Equipment Turnover") + '</tr>';
                    //renderHTML += '<td align="center">' + milestoneColorCodeReturn(val.FinalInspection, val.ProjectNo, val.JobNo, "Equipment Turnover") + '</td></tr>';

                    //renderHTML += '<td align="center" class="set-box-width"><a href="javascript:void(0);" title="Payment" data-toggle="popover" data-html="true" data-placement="top" data-container="body" data-content="" id="' + val.ProjectNo + 'projectNo" onclick="showTinyPopup(\'Payment\', \' ' + val.ProjectNo + 'projectNo \')">Billed:' + val.PaidPercent + ' Paid:' + val.BilledPercent + '</a></td>';
                    //renderHTML += '<td align="center" class="set-box-width"><a href="javascript:void(0);" title="Invoice" data-toggle="popover" data-html="true" data-placement="top" data-container="body" data-content="" id="' + val.ProjectNo + 'invoice" onclick="showTinyPopup(\'Invoice\');"><img src="/images/img/invoice-icon.png" width="25px" height="25px" class="img-responsive" /></a></td>';
                    //renderHTML += '<td align="center" class="set-box-width"><a href="javascript:void(0);" title="Document" data-toggle="popover" data-html="true" data-placement="top" data-container="body" data-content="" id="' + val.ProjectNo + 'document" onclick="showTinyPopup(\'Document\');"><img src="/images/img/contract-icon.png" width="25px" height="25px" class="img-responsive" /></a></td>';
                    //renderHTML += '<td align="center" class="set-box-width"><a href="javascript:void(0);" title="Contact" data-toggle="popover" data-html="true" data-placement="top" data-container="body" data-content="" id="' + val.ProjectNo + 'contact" onclick="showTinyPopup(\'Contact\');"><img src="/images/img/contact-icon.png" width="25px" height="25px" class="img-responsive" /></a></td></tr>';

                    //}

                    $.each(val.BankList, function (i, model) {
                        renderHTML += '<tr><td align="center"><span onclick="popupForJobListWithParameter(this)"  data-job="' + model.JobNo + '" data-project-name="' + val.Title + '" data-project="' + model.ProjectNo + '"  style="cursor:pointer" data-tooltip="tooltip" data-placement="top" title="">' + model.JobNo + '</span></td>';
                        //renderHTML += '<tr><td align="center"><span  data-tooltip="tooltip" data-placement="top" title="" data-original-title="">' + model.JobNo + '</span></td>';
                        renderHTML += milestoneColorCodeReturn(model.ContractExecuted, model.ProjectNo, model.JobNo, "Contract Executed", model.ContractExecutedDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.DownPaymentRecord, model.ProjectNo, model.JobNo, "Down Payment Rec’d", model.DownPaymentRecordDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.DrawingsApproved, model.ProjectNo, model.JobNo, "Drawings Approved", model.DrawingsApprovedDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.FinishesComplete, model.ProjectNo, model.JobNo, "Finishes Complete", model.FinishesCompleteDate.substr(0, 10));
                        //renderHTML += '<td align="center">' + milestoneColorCodeReturn(model.MaterialOrdered, model.ProjectNo, model.JobNo, "Material Ordered") + '</td>';
                        renderHTML += milestoneColorCodeReturn(model.MaterialInStorageHub, model.ProjectNo, model.JobNo, "Material in Storage Hub", model.MaterialInStorageHubDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.ReadyToPull, model.ProjectNo, model.JobNo, "Job-Site Ready", model.ReadyToPullDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.InstallationStart, model.ProjectNo, model.JobNo, "Installation Start", model.InstallationStartDate.substr(0, 10));
                        renderHTML += milestoneColorCodeReturn(model.EquipmentTurnover, model.ProjectNo, model.JobNo, "Equipment Turnover", model.EquipmentTurnoverDate.substr(0, 10));

                        renderHTML += '</tr>';
                        //renderHTML += '<td align="center">' + milestoneColorCodeReturn(model.FinalInspection, model.ProjectNo, model.JobNo, "Equipment Turnover") + '</td></tr>';
                        // renderHTML += '<td align="center"></td>';
                        // renderHTML += '<td align="center"></td>';
                        // renderHTML += '<td align="center"></td>';
                        // renderHTML += '<td align="center"></td></tr>';
                    })
                    renderHTML += '</tbody></table></div></div></div>';
                })

                $('#tblBodyProjectJobMilestone').append(renderHTML);
                $('[data-toggle="popover"]').popover({ template: '<div style=\"max-width:600px;width:600px;\" class=\"popover\" role=\"tooltip\"><div class=\"arrow\"></div><h3 class=\"popover-title\"></h3><div class=\"popover-content\"></div></div>', trigger: 'manual' });
                $('[data-toggle="popoversmall"]').popover({ template: '<div style=\"max-width:360px;width:300px;\" class=\"popover\" role=\"tooltip\"><div class=\"arrow\"></div><h3 class=\"popover-title\"></h3><div class=\"popover-content\"></div></div>', trigger: 'manual' });

                $('#projectJobMilestoneStatusDIV').mCustomScrollbar();
            }
        },
        error: function (data) {

        }
    })
}
function mouseoverfun(ths, img) {
    if (img == 1) {   //--- 1 For Invoice
        $(ths).find('img').attr('src', '/images/img/invoice-icon-roll.png')
    }
    else if (img == 2) {   //--- 2 For Contracts
        $(ths).find('img').attr('src', '/images/img/contract-icon-roll.png')
    }
    else if (img == 3) {   //--- 3 For Contact
        $(ths).find('img').attr('src', '/images/img/contact-icon-roll.png')
    }


}
function mouseoutfun(ths, img) {
    if (img == 1) {
        $(ths).find('img').attr('src', '/images/img/invoice-icon.png')
    }
    else if (img == 2) {   //--- 2 For Contracts
        $(ths).find('img').attr('src', '/images/img/contract-icon.png')
    }
    else if (img == 3) {   //--- 3 For Contact
        $(ths).find('img').attr('src', '/images/img/contact-icon.png')
    }

}
function milestoneColorCodeReturn(milestoneValue, ProjectNo, JobNo, MilestoneName, DueDate) {
    // 0 means not applicable
    var dt = (DueDate != "" && DueDate != undefined) ? 'Dt.: ' + toDate(DueDate) : '';

    if (milestoneValue == "0" || milestoneValue == "N") {
        return '<td><a title="Not applicable"><span></span></a>';
    }
        // 1 means completed
    else if (milestoneValue == "1" || milestoneValue == "C") {

        if (milestoneValue == "1") {
            return '<td class="completed_bg"><a title="Completed" href="javascript:void(0);" style="cursor:default;"><span class="completed_txt">Completed</span><img src="/img/milestoneicon/Completed-24x24.png" /></a></td>';
        }
        else {
            return '<td class="completed_bg"><a title="Completed" href="javascript:void(0);" style="cursor:default;"><span class="completed_txt">' + dt + '</span><img src="/img/milestoneicon/Completed-24x24.png" /></a></td>';
        }
    }
        // 2 means pending
    else if (milestoneValue == "2" || milestoneValue == "P") {

        if (milestoneValue == "2") {
            return '<td class="inprogress_bg"><a title="Pending/In Progress" href="javascript:void(0);" style="cursor:default;"><span class="inprogress_txt">In-Progress</span><img src="/img/milestoneicon/Pending-24x24.png"></img></a></td>';
        }
        else {
            return '<td class="inprogress_bg" title="Pending/In Progress" name="overdueMilestoneClick" overdue="' + ProjectNo + '_' + JobNo + '_' + MilestoneName + '" style="cursor:pointer;"><span class="inprogress_txt">' + dt + '</span><img src="/img/milestoneicon/Pending-24x24.png"></img></td>';
        }
    }
        // 3 means overdue
    else if (milestoneValue == "3" || milestoneValue == "O") {

        if (milestoneValue == "3") {
            return '<td class="overdue_bg"><a title="Completed" href="javascript:void(0);" style="cursor:default;"><span class="overdue_txt">Overdue</span><img src="/img/milestoneicon/Overdue-24x24.png"></img></a></td>';
        }
        else {
            return '<td class="overdue_bg" title="Overdue" name="overdueMilestoneClick" overdue="' + ProjectNo + '_' + JobNo + '_' + MilestoneName + '" style="cursor:pointer;"><span class="overdue_txt">' + dt + '</span><img src="/img/milestoneicon/Overdue-24x24.png"></img></td>';
        }
    }
        // 4 means Due in 7 days
    else if (milestoneValue == "D" || milestoneValue == "4") {
        if (milestoneValue == "4") {
            return '<td class="duedays_bg"><a title="Completed" href="javascript:void(0);" style="cursor:default;"><span class="duedays_txt">Due in 7 days</span><img src="/img/milestoneicon/Due-24x24.png"></img></a></td>';
        }
        else {
            return '<td class="duedays_bg" title="Due in 7 Days" style="cursor: pointer;" name="overdueMilestoneClick" overdue="' + ProjectNo + '_' + JobNo + '_' + MilestoneName + '"><span class="duedays_txt">' + dt + '</span><img src="/img/milestoneicon/Due-24x24.png"></img></td>';
        }
    }
}

function GetDueDateAndStatusOfMilestone(ProjectNo, JobNo, MilestoneTitle) {

    var model = new Object();

    model.ProjectNo = ProjectNo;
    model.JobNo = JobNo;
    model.Milestone = MilestoneTitle;


    $.ajax({
        type: "POST",
        url: "/ManageProject/GetDueDateAndStatusOfMilestone/",
        data: model,
        async: false,
        success: function (data) {

            //$('#dashboardAllNewUser').text(data.usercount);
            if (data.length > 0) {
                var DueDate = data[0].DueDate.replace("12:00:00 AM", "");
                var CompletedDate = data[0].CompletedDate.replace("12:00:00 AM", "");
                $('#Editduedate').val(DueDate);
                $('#Editcompleteddate').val(CompletedDate);
                $('#editstatustype').val(data[0].StatusId);
                storedStatus = data[0].StatusId;
            }
        }

    })
}

function drawChart() {

    var data = new google.visualization.DataTable();
    //$('#piechart')[0]

    data.addColumn('string', 'Jobs payment');
    data.addColumn('number', 'payment');
    //data.addColumn('number', 'Balance Owed');

    data.addRow(['Rec\'d', parseFloat(recData)]);
    data.addRow(['Balance Owed', parseFloat(BalData)]);

    var options = {
        legend: { position: 'bottom', alignment: 'start' },
        width: 260,
        height: 140
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}

//$(window).resize(function () {
//    drawChart();
//});

function ShowChartDocInvoiceAndContact(projectNumber, modelName) {

    var model = new Object();
    var projectNo = projectNumber;
    var dynamicHTML = "";
    var renderID = "";

    model.ProjectNumber = projectNo;//'578459';
    model.ModuleName = modelName;

    $.ajax({
        type: "POST",
        url: "/ManageProject/GetPaymentDocumentContactData/",
        data: model,
        //async: false,
        dataType: 'json',
        success: function (data) {

            var renderID = '#' + projectNo.trim() + modelName.trim();
            var ProjectCompanyId = $('#hdnProjectCompanyId' + projectNo.trim()).val();


            if (data == "" || data == null || data == undefined) {
                //dynamicHTML = '<h3> No Data </h3>';
                //added by pradip on 14-08-2017 
                if (modelName == 'Payment') {

                    dynamicHTML = '<h3> No Payment details found !! </h3>';
                }
                else if (modelName == 'Contract') {

                    //Send ProjectNo and CompanyId   ///$('#hdnSelectedCompanyId').val()

                    var docType = "documents";
                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        dynamicHTML = '<h3> No Contract(s) found !! </h3><br/><div class="col-sm-12 fixed-foot"><a target="_blank" class="btn btn-red" href="/ManageProject/ProjectMasterView/' + projectNo.trim() + '/Summary?DocType=' + docType + '&ProjectNo=' + projectNo.trim() + '&CompanyId=' + ProjectCompanyId.trim() + '">Upload Contract Doc</a></div>';
                    }
                    else {
                        dynamicHTML = '<h3> No Contract(s) found !! </h3>';
                    }
                }
                else if (modelName == 'Invoice') {
                    var docType = "documents";
                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        dynamicHTML = '<h3> No Invoice(s) details found !! </h3><br/><div class="col-sm-12 fixed-foot"><a target="_blank" class="btn btn-red" href="/ManageProject/ProjectMasterView/' + projectNo.trim() + '/Summary?DocType=' + docType + '&ProjectNo=' + projectNo.trim() + '&CompanyId=' + ProjectCompanyId.trim() + '">Upload Invoice</a></div>';
                    }
                    else {
                        dynamicHTML = '<h3> No Invoice(s) details found !! </h3>';
                    }
                }
                else {
                    dynamicHTML = '<h3> No details found !! </h3>';
                }

                $(renderID).attr('data-content', dynamicHTML).popover('show');
                //return false;
            }
            else {

                data = $.parseJSON(data);
                //debugger
                if (modelName == 'Payment') {

                    if (data.Payment[0].ReceivedPercent == undefined || data.Payment[0].Balance_Owed_Percent == undefined) {
                        dynamicHTML = '';
                        dynamicHTML = '<h3> No Payment details found !! </h3>';
                        $(renderID).attr('data-content', dynamicHTML).popover('show');

                    }
                    else {
                        //Draw pie chart
                        recData = data.Payment[0].ReceivedPercent;
                        BalData = data.Payment[0].Balance_Owed_Percent;
                        google.charts.load('current', { 'packages': ['corechart'] });
                        google.charts.setOnLoadCallback(drawChart);
                    }
                } else if (modelName == 'Invoice') {
                    $(renderID).attr('data-content', '');
                    //Invoice Listing
                    dynamicHTML = '';
                    var docType = "documents";

                    for (i = 0; i < data.Invoice.length; i++) {
                        var imgPath = getExtension(data.Invoice[i].DocumentPath);

                        dynamicHTML += '<div class="row mrg-LR15B5"><div class="col-sm-1 set-padding-zero"><img src="' + imgPath + '" width="16px" height="18px" alt="icon" /></div><div class="col-sm-11 set-padding-zero set-top"><a target="_blank" href="' + data.Invoice[i].DocumentPath + '"><strong>' + data.Invoice[i].DocumentName + '</strong></a></div></div>';
                    }

                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {

                        dynamicHTML += '<div class="col-sm-12 fixed-foot"><a target="_blank" class="btn btn-red" href="/ManageProject/ProjectMasterView/' + projectNo.trim() + '/Summary?DocType=' + docType + '&ProjectNo=' + projectNo.trim() + '&CompanyId=' + ProjectCompanyId.trim() + '">Upload Invoice</a></div>';
                    }

                    // if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                    $(renderID).attr('data-content', dynamicHTML).popover('show');

                } else if (modelName == 'Contract') {
                    $(renderID).attr('data-content', '');

                    //Contract Listing
                    dynamicHTML = '';
                    var docType = "documents";

                    for (i = 0; i < data.Contract.length; i++) {
                        var imgPath = getExtension(data.Contract[i].DocumentPath);
                        dynamicHTML += '<div class="row mrg-LR15B5"><div class="col-sm-1 set-padding-zero"><img src="' + imgPath + '" width="16px" height="18px" alt="icon" /></div><div class="col-sm-11 set-padding-zero set-top"><a target="_blank" href="' + data.Contract[i].DocumentPath + '"><strong>' + data.Contract[i].DocumentName + '</strong></a></div></div>';
                    }
                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        dynamicHTML += '<div class="col-sm-12 fixed-foot"><a target="_blank" class="btn btn-red" href="/ManageProject/ProjectMasterView/' + projectNo.trim() + '/Summary?DocType=' + docType + '&ProjectNo=' + projectNo.trim() + '&CompanyId=' + ProjectCompanyId.trim() + '">Upload Contract Doc</a></div>';
                    }


                    $(renderID).attr('data-content', dynamicHTML).popover('show');
                } else if (modelName == 'Contact') {
                    dynamicHTML = "";
                    //$('.popover').css({ "width": "450px" });

                    $(renderID).attr('data-content', '');

                    var tempData = data.Contact[0];
                    //Contact Listing

                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        //<i class="fa fa-floppy-o" aria-hidden="true"></i>
                        debugger
                        dynamicHTML = '<table id="tblPopoverContract" class="contactInfo"><tr><th>Role</th> <th>Name</th><th>Email</th> <th>Phone</th><th></th></tr>';

                        //Sales Representative
                        dynamicHTML += '<tr><td>Sales Rep </td>';


                        dynamicHTML += '<td ><a id="lblSEName' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerSalesExecutive_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSalesExecutive + '</a>';
                        dynamicHTML += '<input type="text" placeholder="Name" style="display:none;" class="edit-textbox-size" id="editSEName' + tempData.ProjectNo.trim() + '"/></td>';



                        dynamicHTML += '<td><a id="lblSEEmail' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerSalesExecutive_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSalesExecutive_Email + '</a>';
                        dynamicHTML += '<input type="text" placeholder="Email-Id" style="display:none;" class="edit-textbox-size" id="editSEEmail' + tempData.ProjectNo.trim() + '"/></td>';



                        dynamicHTML += '<td style><a href="tel:+' + tempData.SchindlerSalesExecutive_Phone + '" id="lblSEPhone' + tempData.ProjectNo + '">' + setPhoneMasking(tempData.SchindlerSalesExecutive_Phone) + '</a>';//$(txtPhoneId).mask('000-000-0000');
                        dynamicHTML += '<input type="text" maxlength="12" placeholder="Phone Number" style="display:none;" class="edit-textbox-size" id="editSEPhone' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><div class="" id="editIconSE' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=editData("editSE","lblSE","hdnSE","SE","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-pencil-square-o edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<div class="hide-show-btn" id="saveIconSE' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=saveData("editSE","' + tempData.ProjectNo.trim() + '","SR","lblSE","hdnSE","SE");>';
                        dynamicHTML += '<i class="fa fa-floppy-o edit-btn-icon"></i></a> &nbsp;&nbsp;&nbsp;';

                        dynamicHTML += '<a href="javascript:void(0);" onclick=cancelEdit("editSE","lblSE","SE","hdnSE","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-close edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<input type="hidden" id="hdnSEName' + tempData.ProjectNo + '" value="' + tempData.SchindlerSalesExecutive + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSEEmail' + tempData.ProjectNo + '" value="' + tempData.SchindlerSalesExecutive_Email + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSEPhone' + tempData.ProjectNo + '" value="' + tempData.SchindlerSalesExecutive_Phone + '" /></td></tr>';



                        //Supretendant
                        dynamicHTML += '<tr><td>Sup </td>';

                        dynamicHTML += '<td ><a id="lblSupName' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerSuperintendent_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSuperintendent + '</a>';
                        dynamicHTML += '<input type="text" placeholder="Name" style="display:none;" class="edit-textbox-size" id="editSupName' + tempData.ProjectNo.trim() + '"/></td>';



                        //dynamicHTML += '<td><a id="lblSupEmail' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerSuperintendent_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSuperintendent_Email + '</a>';
                        //dynamicHTML += '<input type="text" placeholder="Email-Id" style="display:none;" class="edit-textbox-size" id="editSupEmail' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><a id="lblSupEmail' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerSuperintendent_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSuperintendent_Email + '</a>';
                        dynamicHTML += '<input type="text" placeholder="Email-Id" style="display:none;" class="edit-textbox-size" id="editSupEmail' + tempData.ProjectNo.trim() + '"/></td>';


                        dynamicHTML += '<td><a href="tel:+' + tempData.SchindlerSuperintendent_Phone + '" id="lblSupPhone' + tempData.ProjectNo + '">' + setPhoneMasking(tempData.SchindlerSuperintendent_Phone) + '</a>';
                        dynamicHTML += '<input type="text" maxlength="12" placeholder="Phone Number" style="display:none;" class="edit-textbox-size" id="editSupPhone' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><div class="" id="editIconSup' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=editData("editSup","lblSup","hdnSup","Sup","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-pencil-square-o edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<div class="hide-show-btn" id="saveIconSup' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=saveData("editSup","' + tempData.ProjectNo.trim() + '","SUP","lblSup","hdnSup","Sup");>';
                        dynamicHTML += '<i class="fa fa-floppy-o edit-btn-icon"></i></a> &nbsp;&nbsp;&nbsp;';

                        dynamicHTML += '<a href="javascript:void(0);" onclick=cancelEdit("editSup","lblSup","Sup","hdnSup","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-close edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<input type="hidden" id="hdnSupName' + tempData.ProjectNo + '" value="' + tempData.SchindlerSuperintendent + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSupEmail' + tempData.ProjectNo + '" value="' + tempData.SchindlerSuperintendent_Email + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSupPhone' + tempData.ProjectNo + '" value="' + tempData.SchindlerSuperintendent_Phone + '" /></td></tr>';





                        //SchindlerProjMgr --SPM
                        dynamicHTML += '<tr><td>Proj Mgr </td>';

                        dynamicHTML += '<td ><a id="lblSPMName' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerProjMgr_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerProjMgr + '</a>';
                        dynamicHTML += '<input type="text" placeholder="Name" style="display:none;" class="edit-textbox-size" id="editSPMName' + tempData.ProjectNo.trim() + '"/></td>';


                        //dynamicHTML += '<td><a id="lblSPMEmail' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerProjMgr_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerProjMgr + '</a>';
                        //dynamicHTML += '<input type="text" style="display:none;" class="edit-textbox-size" id="editSPMEmail' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><a id="lblSPMEmail' + tempData.ProjectNo + '" href="mailto:' + tempData.SchindlerProjMgr_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerProjMgr_Email + '</a>';
                        dynamicHTML += '<input type="text" style="display:none;" placeholder="Email-Id" class="edit-textbox-size" id="editSPMEmail' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><a href="tel:+' + tempData.SchindlerProjMgr_Phone + '" id="lblSPMPhone' + tempData.ProjectNo + '">' + setPhoneMasking(tempData.SchindlerProjMgr_Phone) + '</a>';
                        dynamicHTML += '<input type="text" maxlength="12" style="display:none;" placeholder="Phone Number" class="edit-textbox-size" id="editSPMPhone' + tempData.ProjectNo.trim() + '"/></td>';

                        dynamicHTML += '<td><div class="" id="editIconSPM' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=editData("editSPM","lblSPM","hdnSPM","SPM","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-pencil-square-o edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<div class="hide-show-btn" id="saveIconSPM' + tempData.ProjectNo.trim() + '"><a href="javascript:void(0);" onclick=saveData("editSPM","' + tempData.ProjectNo.trim() + '","PM","lblSPM","hdnSPM","SPM");>';
                        dynamicHTML += '<i class="fa fa-floppy-o edit-btn-icon"></i></a> &nbsp;&nbsp;&nbsp;';

                        dynamicHTML += '<a href="javascript:void(0);" onclick=cancelEdit("editSPM","lblSPM","SPM","hdnSPM","' + tempData.ProjectNo.trim() + '");>';
                        dynamicHTML += '<i class="fa fa-close edit-btn-icon"></i></a></div>';

                        dynamicHTML += '<input type="hidden" id="hdnSPMName' + tempData.ProjectNo + '" value="' + tempData.SchindlerProjMgr + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSPMEmail' + tempData.ProjectNo + '" value="' + tempData.SchindlerProjMgr_Email + '" />';
                        dynamicHTML += '<input type="hidden" id="hdnSPMPhone' + tempData.ProjectNo + '" value="' + tempData.SchindlerProjMgr_Phone + '" /></td></tr></table>';


                    }
                    else {
                        //Sales Rep:
                        dynamicHTML = '<table id="tblPopoverContract" class="contactInfo"><tr><th>Role</th> <th>Name</th> <th>Email</th><th>Phone</th></tr><tr><td>Sales Rep </td>';
                        dynamicHTML += '<td><a href="#">' + tempData.SchindlerSalesExecutive + '</a></td>';
                        dynamicHTML += '<td><a href="mailto:' + tempData.SchindlerSalesExecutive_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSalesExecutive_Email + '</a>';
                        dynamicHTML += '</td><td><a href="tel:+' + tempData.SchindlerSalesExecutive_Phone + '">' + setPhoneMasking(tempData.SchindlerSalesExecutive_Phone) + '</a></td></tr>';


                        //supretendant
                        dynamicHTML += '<tr><td>Sup </td><td><a href="#">' + tempData.SchindlerSuperintendent + '</a></td>';
                        dynamicHTML += '<td><a href="mailto:' + tempData.SchindlerSuperintendent_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerSuperintendent_Email + '</a>';
                        dynamicHTML += '</td><td><a href="tel:+' + tempData.SchindlerSuperintendent_Phone + '">' + setPhoneMasking(tempData.SchindlerSuperintendent_Phone) + '</a></td></tr>';


                        //Project mngr
                        dynamicHTML += '<tr><td>Proj Mgr </td><td><a href="#">' + tempData.SchindlerProjMgr + '</a></td>';
                        dynamicHTML += '<td><a href="mailto:' + tempData.SchindlerProjMgr_Email + '?subject=MySchindlerProjects - ' + tempData.ProjectNo + ' - ' + tempData.ProjectName + '">' + tempData.SchindlerProjMgr_Email + '</a>';
                        dynamicHTML += '</td><td><a href="tel:+' + tempData.SchindlerProjMgr_Phone + '">' + setPhoneMasking(tempData.SchindlerProjMgr_Phone) + '</a></td></tr></table>';
                    }

                    $(renderID).attr('data-content', dynamicHTML).popover('show');
                    //$('[data-toggle="popover"]').popover({ template: '<div style=\"max-width:200px;width:200px;\" class=\"popover\" role=\"tooltip\"><div class=\"arrow\"></div><h3 class=\"popover-title\"></h3><div class=\"popover-content\"></div></div>' });

                }
            }
            // $('[data-toggle="popover"]').popover();
        }

    })
}

function setPhoneMasking(phonenNumber) {
    var srphone = '';
    if (phonenNumber != '') {
        var a = phonenNumber;
        srphone = a.substr(0, 3) + '-' + a.substr(3, 3) + '-' + a.substr(6, 10);
    }

    return srphone;
}

function cancelEdit(txtboxName, lblName, role, hdnName, projectNo) {

    var lblEName = '#' + lblName + 'Name' + projectNo;
    var lblEmailName = '#' + lblName + 'Email' + projectNo;
    var lblPhoneName = '#' + lblName + 'Phone' + projectNo;

    var editIcon = '#' + 'editIcon' + role + projectNo;
    var saveIcon = '#' + 'saveIcon' + role + projectNo;

    //var hdnName = '#' + hdnName + 'Name' + projectNo;
    //var hdnEmail = '#' + hdnName + 'Email' + projectNo;
    var hdnPhone = '#' + hdnName + 'Phone' + projectNo;

    $(lblEName).css('display', 'block');
    $(lblEmailName).css('display', 'block');
    $(lblPhoneName).css('display', 'block');

    var txtENameId = '#' + txtboxName + "Name" + projectNo;
    var txtEmailId = '#' + txtboxName + "Email" + projectNo;
    var txtPhoneId = '#' + txtboxName + "Phone" + projectNo;

    $(txtENameId).css('display', 'none');
    $(txtEmailId).css('display', 'none');
    $(txtPhoneId).css('display', 'none');

    //$(lblEmailName).text($(hdnName).val());
    $(lblPhoneName).text($(hdnPhone).val());

    $(editIcon).removeClass('hide-show-btn');
    $(saveIcon).addClass('hide-show-btn');

    $(txtENameId).css({
        "border": "1px solid #a9a9a9"
    });

    $(txtEmailId).css({
        "border": "1px solid #a9a9a9"
    });

    $(txtPhoneId).css({
        "border": "1px solid #a9a9a9"
    });
}

function editData(txtboxName, lblName, hdnName, role, projectNo) {
    debugger
    //var hdnName = '#' + hdnName + 'Name' + projectNo;
    var hdnEName = '#' + hdnName + 'Name' + projectNo;
    var hdnEmail = '#' + hdnName + 'Email' + projectNo;
    var hdnPhone = '#' + hdnName + 'Phone' + projectNo;

    var lblEName = '#' + lblName + 'Name' + projectNo;
    var lblEmailName = '#' + lblName + 'Email' + projectNo;
    var lblPhoneName = '#' + lblName + 'Phone' + projectNo;

    var editIcon = '#' + 'editIcon' + role + projectNo;
    var saveIcon = '#' + 'saveIcon' + role + projectNo;

    $(lblEName).css('display', 'none');
    $(lblEmailName).css('display', 'none');
    $(lblPhoneName).css('display', 'none');

    var txtNameId = '#' + txtboxName + "Name" + projectNo;
    var txtEmailId = '#' + txtboxName + "Email" + projectNo;
    var txtPhoneId = '#' + txtboxName + "Phone" + projectNo;

    $(txtNameId).css('display', 'block');
    $(txtEmailId).css('display', 'block');
    $(txtPhoneId).css('display', 'block');

    $(txtNameId).val($(hdnEName).val());
    $(txtEmailId).val($(hdnEmail).val());
    $(txtPhoneId).val($(hdnPhone).val());

    $(editIcon).addClass('hide-show-btn');
    $(saveIcon).removeClass('hide-show-btn');

    $(txtPhoneId).mask('000-000-0000');

}


function ValidEmailAddress(element) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(element.val());
};


function saveData(txtboxName, projectNo, role, lblName, hdnName, RoleName) {

    debugger
    var isValid = true;
    var txtEName = '#' + txtboxName + 'Name' + projectNo;
    var txtEmail = '#' + txtboxName + 'Email' + projectNo;
    var txtPhone = '#' + txtboxName + 'Phone' + projectNo;
    var prjctCompanyID = '#hdnProjectCompanyId' + projectNo;

    if ($(txtEName).val() == null || $(txtEName).val() == '' || $(txtEName).val() == undefined) {
        $(txtEName).css({
            "border": "1px solid red"
        });
        $(txtEName).focus();
        var isValid = false;
        //$('#btnUpdateUser').removeAttr('disabled');
        return false;

    }

    if ($(txtEmail).val() != null && $(txtEmail).val() != '' && $(txtEmail).val() != undefined) {
        if (!ValidEmailAddress($(txtEmail))) {
            $(txtEmail).css({
                "border": "1px solid red"
            });
            $(txtEmail).focus();
            var isValid = false;
            //$('#btnUpdateUser').removeAttr('disabled');
            return false;
        }
    }

    var phonelen = $(txtPhone).val().length;
    if (phonelen != 0) {
        if (phonelen != 12) {
            $(txtPhone).css({
                "border": "1px solid red"
            });
            $(txtPhone).focus();
            var isValid = false;
            //$('#btnUpdateUser').removeAttr('disabled');
            return false;
        }
    }


    var model = new Object();
    // debugger;
    model.ProjectNo = projectNo;
    model.UserEmail = $(txtEmail).val();
    model.Name = $(txtEName).val();
    model.Phone = $(txtPhone).val().replace('-', '').replace('-', '');;
    model.RoleName = role;
    model.CompanyID = $(prjctCompanyID).val();

    $.ajax({
        type: "POST",
        url: "/ManageProject/SaveEmailAndPhoneData",
        data: model,
        dataType: 'json',
        success: function (data) {
            debugger
            var hdnEName = '#' + hdnName + 'Name' + projectNo;
            var hdnEmail = '#' + hdnName + 'Email' + projectNo;
            var hdnPhone = '#' + hdnName + 'Phone' + projectNo;

            $(hdnEName).val(model.Name);
            $(hdnEmail).val(model.UserEmail);
            $(hdnPhone).val($(txtPhone).val());

            var lblEName = '#' + lblName + 'Name' + projectNo;
            var lblEmail = '#' + lblName + 'Email' + projectNo;
            var lblPhone = '#' + lblName + 'Phone' + projectNo;
            $(lblEName).html(model.Name);
            $(lblEmail).html(model.UserEmail);
            $(lblPhone).html($(txtPhone).val());

            cancelEdit(txtboxName, lblName, RoleName, hdnName, projectNo);
        },
        error: function (d) {
            //alert(d.data);
        }
    })

}

function getExtension(fileName) {

    var imgType = fileName.split('.').pop().toLowerCase();

    if (imgType == "png" || imgType == "jpeg" || imgType == "jpg" || imgType == "jpe" || imgType == "gif" || imgType == "psd")
        imgType = "psd";
    else if (imgType == "xls" || imgType == "xlsx")
        imgType = "xls";
    else if (imgType == "ppt" || imgType == "pptx")
        imgType = "ppt";
    else if (imgType == "doc" || imgType == "docx")
        imgType = "doc";
    else if (imgType == "html" || imgType == "htm")
        imgType = "html";
    else if (imgType == "txt")
        imgType = "txt";
    else if (imgType == "pdf")
        imgType = "pdf";
    else
        imgType = "file"

    imgType = "/img/filetree/" + imgType + ".png";

    return imgType;
}

function showTinyPopup(modelName, ProjectNo) {

    var UID = '#' + ProjectNo.trim() + modelName;

    //Uncomment Code
    //ProjectNo = ProjectNo.replace("projectNo", "").trim();
    //or
    //ProjectNo = ProjectNo.trim();

    //ProjectNo = 578459;

    if (modelName == 'Payment') {

        $('#piechart').remove();
        $(UID).attr('data-content', pieChartHTML).popover('show');
        ShowChartDocInvoiceAndContact(ProjectNo, modelName);

    } else if (modelName == 'Invoice') {
        ShowChartDocInvoiceAndContact(ProjectNo, modelName);

    } else if (modelName == 'Contract') {
        ShowChartDocInvoiceAndContact(ProjectNo, modelName);

    } else if (modelName == 'Contact') {
        ShowChartDocInvoiceAndContact(ProjectNo, modelName);
    }
}


$(document).ready(function () {

    //To show Chart-Doc-Invoice-Contact---Ankit Code
    //ShowChartDocInvoiceAndContact(578459, 'Payment');


    var CompId = 0;
    $("#hdnstatustype").val($('#editstatustype').val());

    var txtAddCompanyoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCompany/",
                data: '{ searchTerm:"' + $('#editCompanyName').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CompanyName,
                            value: item.CompanyID,
                            CityID: item.CompanyID,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {

            $("#editCompanyName").val(ui.item.label);
            CompId = ui.item.CityID
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            } else {

            }
        },

        minLength: 1,
    };

    $(document).on("keydown.autocomplete", '#editCompanyName', function (event) {
        $(this).autocomplete(txtAddCompanyoptions);
    })

    $('#editPhone').mask('000-000-0000');

    var hdnPageSize = parseInt($('#hdnPageSize').val());

    $('#dashboardAllNewUser').text('0');

    var table = $('#newlyRegisteredUserDataTable').DataTable({
        "ajax": {
            "url": "/ManageUser/NewlyRegisterUserList",
            "type": "POST",
            "datatype": "JSON",
            "async": false,
        },
        'aaSorting': [[1, 'asc']],
        "autoWidth": false, // for disabling autowidth
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "processing": true, // for show progress bar
        "orderMulti": false, // for disable multiple column at once
        //"bInfo": false,
        "oLanguage": {
            "sEmptyTable": "No data available",
            "sSearch": "",
            "sProcessing": "<img src='/img/spinner.gif'> Loading.."
        },
        "oSearch": { "sSearch": "" },
        "iTotalRecords": "3",
        "iTotalDisplayRecords": "3",
        "columns": [
        { "data": "UserID", "name": "UserID", "Searchable": true, "sClass": "colHide" },
        { "data": "FirstName", "name": "FirstName", "Searchable": true, "sClass": "txtAlignCenter" },
        //{ "data": "Email", "name": "Email", "Searchable": true, "sClass": "txtAlignCenter" },
         {
             "data": "Email",
             "name": "Email",
             "Searchable": true, "sClass": "txtAlignCenter",
             "render": function (data, type, row) {
                 return '<a href="mailto:' + data + '?subject=MySchindlerProjects" >' + data + '</a>'

             }
         },
        { "data": "CompanyName", "name": "CompanyName", "Searchable": true, "sClass": "txtAlignCenter" },
        { "data": "RegisteredDate", "name": "RegisteredDate", "Searchable": true, "sClass": "txtAlignCenter" },
        {
            "data": "AccountStatus",
            "name": "AccountStatus",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row, meta) {
                $('#dashboardAllNewUser').text(meta.settings._iRecordsTotal);
                if (data == 4) {
                    return '<span class="label pending" >Pending</span>'
                }
                if (data == 1) {
                    return '<span class="label complete" >Active</span>'
                }
                if (data == 2) {
                    return '<span class="label complete" >InActive</span>'
                }
                if (data == 17) {
                    return '<span class="label progresss" >Verified</span>'
                }
                if (data == 18) {
                    return '<span class="label complete" >Approved</span>'
                }
                if (data == 19) {
                    return '<span class="label complete" >Disapproved</span>'
                }
            }
        },
        {
            "data": "UserID",
            "sClass": "txtAlignCenter",
            "render": function (data, type, row) {
                return '<a class="btnsml_action e-edit" id="' + data + '"  href="#" data-toggle="modal" data-target="#editUser" onclick="EditNewlyRegisteredUser(' + data + ')"></a>'
            }
        }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');

        },
        "initComplete": function (settings, json) {
            NewlyRegisterUSerDatatablePaging();
            DashboardTooltip();
        },
        "drawCallback": function (settings) {

            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }

    });
    $(document).on("click", ".table-click", function (e) {

        if ($(this).find("td").is(e.target)) {

            var targetClass = '.' + e.target.className
            targetClass = targetClass.replace(" ", ".");
            var beforeColor = '';
            try {
                beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
            }
            catch (err) {

            }

            if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

            }
            else {
                var clickedRow = $(this).find("td").parent('tr')[0];
                var userID = $(clickedRow).find('td:last').children('.e-edit')[0].id;
                $('#editUser').modal('show');
                EditNewlyRegisteredUser(parseInt(userID));
            }

        }
    });
    $('#newlyRegisteredUserDataTable').on('draw.dt', function () {
        NewlyRegisterUSerDatatablePaging();
    });

    $('.dataTables_length').hide();

    //$('#newlyRegisteredUserDataTable_filter').hide();

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtUserCityoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutoCompleteAddUser/",
                data: '{ searchTerm:"' + $('#editCity').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#editState').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#editCity").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $("#editState").change(function (event) {
        $('#editCity').val('');
    });

    $(document).on("keydown.autocomplete", '#editCity', function (event) {
        $(this).autocomplete(txtUserCityoptions);
    })

    $('#btnUpdateUser').click(function (event) {

        event.preventDefault();
        var token = "0";

        if ($('#editStatus option:selected').val() != 19) {
            if (!ValidateEditUser()) {
                return false;
            }
        }
        else {
            token = "1";
        }

        var isValid = true;

        var phonelen = $('#editPhone').val().length;
        if (phonelen < 12) {
            $('#editPhone').css({
                "border": "1px solid red"
            });
            $('#editPhone').focus();
            var isValid = false;
            $('#btnUpdateUser').removeAttr('disabled');
            return false;
        }
        var formData = new FormData();

        var editContact = $("#editPhone").val().replace('-', '').replace('-', '');
        formData.append("UserID", $('#editUserId').val().trim());
        formData.append("FirstName", $('#editFullname').val().trim());
        formData.append("CompanyName", $('#editCompanyName').val().trim());
        formData.append("ContactNumber", editContact);
        formData.append("Designation", $('#editDesignation option:selected').val().trim());
        formData.append("Email", $('#editEmail').val());
        formData.append("Address1", $('#editStreetAddress').val().trim());
        formData.append("State", $('#editState').val());
        formData.append("StateText", $('#editState option:selected').text());
        formData.append("City", $('#editCity').val());
        formData.append("PostalCode", $('#editZipCode').val());
        formData.append("Requested_Projects", $('#editrequestedprojects').val());
        formData.append("AccountStatus", $('#editStatus option:selected').val());
        formData.append("Token", token);

        $.ajax({
            url: '/ManageUser/UpdateNewlyRegisteredUser',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                if (data == "Company not exist") {
                    $('#editCompanyName').css('border-color', 'red');
                    $('#CommonSuccessMessage').html('Please enter valid company');
                    $('#CommonSuccessModal').modal('show');
                }
                else {
                    $('#editUser').modal('hide');
                    $('#CommonSuccessMessage').html('Record updated successfully');
                    $('#CommonSuccessModal').modal('show');

                    var table = $('#newlyRegisteredUserDataTable').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (data) {
                alert('error')
            }
        })
    });

    $('#btnStatusYes').click(function () {

        var UserID = $('#hdnStatusForUserID').val();
        var statusID = $('#hdnStatusValue').val();
        var UserEmail = $('#hdnStatusForUserEmail').val();

        $.ajax({
            type: "POST",
            url: "/ManageUser/UpdateUserAccountStatus",
            data: { UserID: UserID, StatusID: statusID, Email: UserEmail },
            success: function (userdetail) {
                var table = $('#newlyRegisteredUserDataTable_filter').DataTable();
                table.ajax.reload();
            }
        })
    })


    //DashboardMsgWidget();

    //$('#msgWidgetRefresh').click(function () {
    //    DashboardMsgWidget();
    //})

    // old project status chart and milestone chart
    //ProjectPieChart();

    var searchTerm = '';

    if (PageName.match("^/Dashboard")) {
        DashboardProjectStatusChart(searchTerm, pageNumber); // for new dashboard project milestone chart
    }

    $("#projectJobMilestoneStatusDIV").mCustomScrollbar({

        callbacks: {
            whileScrolling: function (event) {

                if (this.mcs.topPct == "95") {
                    refCount++;
                    if ((refCount % 2) == 0) {
                        //alert('call here');
                        var pages = Math.floor((parseInt($('#dashboardAllProjectCount').html()) / 10));
                        pages = pages + ((parseInt($('#dashboardAllProjectCount').html()) % 10) > 0 ? 1 : 0);
                        if (pageNumber < pages) {
                            //alert('hi')
                            DashboardProjectStatusChart(searchTerm, pageNumber);
                        }
                    }
                }
            },
        }
    });



    //$('.set-icon').hover(function () {
    //    var tempImg = this.getAttribute('src');
    //    this.src = this.getAttribute('data-alt-image');
    //    this.setAttribute('data-alt-image', tempImg);
    //})

    //$('.set-icon').mouseout(function () {
    //    var tempImg = this.getAttribute('src');
    //    this.src = this.getAttribute('data-alt-image');
    //    this.setAttribute('data-alt-image', tempImg);
    //})


    var checkNewUserPermission = $('#hdnCheckNewlyUserPermission').val();

    if (checkNewUserPermission == false) {
        $('#newlyRegisteredUserDIV').hide();
    }

    $('#dashboardAllNewUser').click(function () {
        //window.open('/ManageUser/LatestRegisteredUsers', '_blank');
        window.location.href = '/ManageUser/LatestRegisteredUsers'
    })

    $(document).on("click", 'td[name="overdueMilestoneClick"]', function (event) {

        $('#addMilestoneNote').val('');
        $('#Editduedate').val('');
        $('#Editcompleteddate').val('');
        $('#editstatustype').val('NULL');

        $('#editstatustype').css('border-color', '#D5D5D5');

        var titleValue = $(this).attr('overdue');
        $('#hdnMilestoneNoteValue').val(titleValue);
        var titleArray = titleValue.split('_');

        GetDueDateAndStatusOfMilestone(titleArray[0], titleArray[1], titleArray[2]);
        $('#hdnProjectNo').val(titleArray[0]);
        $('#hdnJobNo').val(titleArray[1]);
        $('#hdnMilestoneName').val(titleArray[2]);

        if (titleArray[2] != 'Job-Site Ready') {
            //$('#divReadyToPull').removeClass('input-group');
            $('#readyToPull').hide();
            $('#job-site-miletstone-btn').addClass('hide-show-btn');
            $('#job-site-question').addClass('hide-show-btn');
            $('#simple-miletstone-btn').removeClass('hide-show-btn');
        }
        else {
            $('#job-site-question').removeClass('hide-show-btn');
            $('#job-site-miletstone-btn').removeClass('hide-show-btn');
            $('#simple-miletstone-btn').addClass('hide-show-btn');

            $('#readyToPull').hide();
            viewReadyToPull($('#hdnProjectNo').val(), $('#hdnJobNo').val());
            //$('#divReadyToPull').addClass('input-group');
            //$('#readyToPull').show();
        }

        if (titleArray[2] == "Final Inspection") {
            titleArray[2] = "Equipment Turnover";
        } else if (titleArray[2] == "Material At Hub") {
            titleArray[2] = "Material in Storage Hub";
        }

        var result = "Add new note for <b>" + titleArray[2] + "</b> milestone of Bank <b>" + titleArray[1] + "</b> under Project No. <b>" + titleArray[0] + "</b> which has Planned Date <b>" + $('#Editduedate').val() + "</b>";

        $('#milestoneNoteTitle').html('');
        $('#milestoneNoteTitle').html(result);
        $('#milestoneNoteModal').modal('show');
        $('#hdnstatustype').val($('#editstatustype option:selected').text());
        $('#spanMilestoneName').html('<b>' + titleArray[2] + '<b>');
        if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
            $("#Editduedate").prop('disabled', false);
            $("#Editcompleteddate").prop('disabled', false);
            $("#editstatustype").prop('disabled', false);
        }
        else {
            $("#Editduedate").prop('disabled', true);
            $("#Editcompleteddate").prop('disabled', true);
            $("#editstatustype").prop('disabled', true);
        }
    })

    $(document).on("click", '#btnMilestoneNoteYes', function (event) {

        if ($('#editstatustype').val() == "NULL") {
            $('#editstatustype').css('border-color', 'red');
            return false;
        }
        else {
            $('#editstatustype').css('border-color', '#D5D5D5');
        }

        $('#milestoneNoteModal').modal('hide');
        //$("#loadermodal").show();
        $('#LoadingImage').css('display', 'block');

        var formData = new FormData();

        formData.append("Note", $('#addMilestoneNote').val().trim());
        formData.append("MilestoneName", $('#hdnMilestoneNoteValue').val());
        formData.append("Status", $('#editstatustype').val());
        formData.append("StatusName", $('#editstatustype option:selected').text().trim());

        formData.append("DueDate", $('#Editduedate').val().trim());
        formData.append("CompletedDate", $('#Editcompleteddate').val().trim());
        formData.append("PreviousStatusName", $('#hdnstatustype').val());

        formData.append("ProjectNo", $('#hdnProjectNo').val());
        formData.append("JobNo", $('#hdnJobNo').val());

        $.ajax({
            url: "/ManageProject/AddProjectBankMilestoneCommentandEmail",
            method: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#LoadingImage').css('display', 'none');
                $('#addMilestoneNote').val('');
                $('#milestoneNoteSuccessModal').modal('show');
                //location.reload();
            },
            error: function (data) {
                $('#LoadingImage').css('display', 'none');
            }
        })
    })

    $('#ProjectJobMilestoneSearch').unbind().keyup(function () {
        var value = $(this).val();
        var pNo = 0;
        if (value.length > 2) {
            $('#tblBodyProjectJobMilestone').html('');
            DashboardProjectStatusChart(value, pNo);
        }
        if (value == '') {
            $('#tblBodyProjectJobMilestone').html('');
            DashboardProjectStatusChart(value, pNo);
        }
    });

    GetCountOfLatestRegisteredUsers();

    $.ajax({
        url: '/Common/BindStatusList',
        type: 'GET',
        data: { module: 'Milestone' },

        success: successmiletype,
        error: errormiletype
    });

    function successmiletype(data) {
        var obj = data;

        statusoptions = '<option name = "statustypeoption" value="NULL">---Select---</option>';
        for (var i = 0; i < obj.length; i++) {

            statusoptions += '<option name = "statustypeoption" value="' +
                obj[i].StatusId +
                '">' +
                obj[i].StatusName +
                '</option>';
        }
        $("select#statustype").html(statusoptions);
        $("select#editstatustype").html(statusoptions);
    };

    function errormiletype(data) {
        return false;
    };

    /* //Ankit Comment
  $('#readyToPull').click(function (e) {
      ProjectNo = $('#hdnProjectNo').val();
      JobNo = $('#hdnJobNo').val();

      viewReadyToPull(ProjectNo, JobNo);
  });
  */

    $('body').on('click', function (e) {
        $('[data-toggle="popover"], [data-toggle="popoversmall"]').each(function () {

            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                //if ($(this).attr('data-content') != '' || $(this).attr('data-content') != null || $(this).attr('data-content') != undefined)
                $(this).popover('hide');
                //$('#piechart').remove();
            }
        });

    });

    $('#btnReadyToPullSave')
           .click(function (e) {

               if ($('#editstatustype').val() == "NULL") {
                   $('#editstatustype').css('border-color', 'red');
                   return false;
               }
               else {
                   $('#editstatustype').css('border-color', '#D5D5D5');
               }

               $('#LoadingImage').css('display', 'block');

               var lstReadyToPullTemplateDetails = [];

               var ProjectNo = $('#hdnProjectNo').val();
               var JobNo = $('#hdnJobNo').val();

               var TempleteId = 0;
               var formData = new FormData();
               var ctrlName = '';

               $('#milestoneNoteModal').modal('hide');
               //$("#loadermodal").show();

               var formData = new FormData();

               formData.append("Note", $('#addMilestoneNote').val().trim());
               formData.append("MilestoneName", $('#hdnMilestoneNoteValue').val());
               formData.append("Status", $('#editstatustype').val());
               formData.append("StatusName", $('#editstatustype option:selected').text().trim());

               formData.append("DueDate", $('#Editduedate').val().trim());
               formData.append("CompletedDate", $('#Editcompleteddate').val().trim());
               formData.append("PreviousStatusName", $('#hdnstatustype').val());

               formData.append("ProjectNo", ProjectNo);
               formData.append("JobNo", JobNo);


               //formData.append("ProjectNo", ProjectNo);
               //formData.append("JobNo", JobNo);

               $('input[name^="chk_"]')
                   .each(function () {

                       ctrlName = $(this).attr('name');
                       TempleteId = ctrlName.replace('chk_', '');

                       //formData.append("TempleteId", TempleteId);

                       var vSelected = $(this).iCheck('update')[0].checked
                       if (vSelected == true) {
                           var files = $('#filename_' + TempleteId).get(0).files;
                           ctrlName = $('#filename_' + TempleteId).attr('name');
                           for (var i = 0; i < files.length; i++) {

                               formData.append(ctrlName + '~' + files[i].name, files[i]);
                           }
                           var ctrlName = $('#textarea_' + TempleteId).attr('name');
                           if ($('#' + ctrlName).val() != '') {
                               //formData.append(ctrlName, $(this).val());
                           }
                           var ctrlName = $('#hdnAlreadyAttachedFile_' + TempleteId).attr('name');
                           if ($('#' + ctrlName).val() != '') {
                               //formData.append(ctrlName, $(this).val());
                           }

                           lstReadyToPullTemplateDetails[lstReadyToPullTemplateDetails.length] = {
                               TemplateId: TempleteId,
                               ProjectNo: ProjectNo,
                               JobNo: JobNo,
                               Comment: $('#textarea_' + TempleteId).val(),
                               Attachment: '',
                               RealAttachment: '',
                               AlreadyRealAttachment: $('#hdnAlreadyAttachedFile_' + TempleteId).val(),
                               CreatedDate: '',
                               CreatedBy: '',
                               IsDeleted: false,
                               appAuditID: 0,
                               SessionID: ''

                           };
                       }

                   });

               formData.append('lstReadyToPullTemplateDetails', JSON.stringify(lstReadyToPullTemplateDetails));


               $.ajax({
                   url: '/ManageProject/SaveReadyToPullTemplateData',
                   method: 'POST',
                   //data: { 'lstReadyToPullTemplateDetails': lstReadyToPullTemplateDetails },
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {

                       //$('#wait').css('display', 'none'); // for ajax loader
                       $('#LoadingImage').css('display', 'none');

                       var milestoneStatus = '';
                       if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length
                       ) {

                           milestoneStatus = 'Job-Site Ready milestone is not completed';
                           $('#editstatustype').val('27'); // In Process
                           storedStatus = 27;
                       }


                       $('#CommonSuccessMessage')
                           .html('Record updated successfully ' + (milestoneStatus != '' ? '<br>' + milestoneStatus : ''));
                       $('#CommonSuccessModal').modal('show');

                       //location.reload();
                       //$('#modal_readypull').modal('hide');
                   },
                   error: function (data) {
                       $('#LoadingImage').css('display', 'none');
                       //Handle errors
                   }
               });
           });

    $('#btnReadyToPullCancel').click(function (e) {
        if ($('input:checkbox[name^="chk_"]:checked').length != $('input:checkbox[name^="chk_"]').length) {
            $('#editstatustype').val(storedStatus);
        }
        //$('#editstatustype').val(storedStatus);
    })
    //#lnkok
    $('#MessageOkbtn, #CommonSuccessOK').click(function (e) {
        location.reload();
    })

    $('#editstatustype')
           .on('change',
               function (e) {

                   if (($(this).val() == '28') && ($('#hdnMilestoneName').val() == 'Job-Site Ready')) {
                       ProjectNo = $('#hdnProjectNo').val();
                       JobNo = $('#hdnJobNo').val();
                       viewReadyToPull(ProjectNo, JobNo);
                   }

               });



    //Announcement Model 
    var cname = '';
    var LoginUser = $('#hdnLoggedUserId').val();
    var DisplayOn = 'after';
    var CookieeName = $.trim('AfterLogin_' + LoginUser);
    var annflagCookiename = "annflagCookie";

    var annflagCookie = GetCookieValue(annflagCookiename);
    if (annflagCookie == "0") {
        document.cookie = "annflagCookie=1;path=/";
        $.ajax({
            url: '/Login/BeforeAfterLoginAnnouncement',
            type: 'GET',
            dataType: 'json',
            data: { DisplayOn: DisplayOn },
            async: false,
            success: function (data) {
                if (data.length != 0) {

                    $('#headerAnnouncement').html('');

                    var renderHTML = '';
                    var AnnouncementList = data;
                    var AnnMsgIds = '';
                    cname = GetCookieValue(CookieeName);
                    if (cname == "") {

                        $.each(AnnouncementList, function (i, val) {
                            renderHTML += '<i class="fa fa-bullhorn announce-bullet" aria-hidden="true"></i><span>' + val.MessageText + '</span>';
                            renderHTML += '<div class="clearfix"></div><span class="announce-border"></span>';
                            if (val.Frequency == "OneTime") {
                                AnnMsgIds += (AnnMsgIds == '') ? val.AnnMsgId : ',' + val.AnnMsgId;
                            }
                        })

                        var Cookie = "AfterLogin_" + LoginUser + "=" + AnnMsgIds;// + "; path=/Dashboard;";
                        document.cookie = $.trim(Cookie)
                        if (renderHTML != "") {
                            $('#Announcement').modal('show');
                            $('#myModalLabelAnnouncement').html('Announcement');
                            $('#headerAnnouncement').html(renderHTML);
                            $('#headerAnnouncement').mCustomScrollbar();
                        }

                    }
                    else {
                        $.each(AnnouncementList, function (i, val) {
                            var AnnMsgId = '' + val.AnnMsgId + '';
                            if ($.inArray(AnnMsgId, cname.split(',')) < 0) //doesn't exist
                            {
                                renderHTML += '<i class="fa fa-bullhorn announce-bullet" aria-hidden="true"></i><span>' + val.MessageText + '</span>';
                                renderHTML += '<div class="clearfix"></div><span class="announce-border"></span>';

                            }
                            else {
                                AnnMsgIds += (AnnMsgIds == '') ? val.AnnMsgId : ',' + val.AnnMsgId;
                            }
                        });

                        var Cookie = "AfterLogin_" + LoginUser + "=" + AnnMsgIds;// + "; path=/Dashboard;";
                        document.cookie = $.trim(Cookie)
                        if (renderHTML != "") {
                            $('#Announcement').modal('show');
                            $('#myModalLabelAnnouncement').html('Announcement');
                            $('#headerAnnouncement').html(renderHTML);
                            $('#headerAnnouncement').mCustomScrollbar();
                        }
                    }
                }
            },
            error: function (data) {

            }

        })
    }


    $('#divjoblist tbody').scroll(function (e) { //detect a scroll event on the tbody
        $('#divjoblist thead').css("left", -$("#divjoblist tbody").scrollLeft()); //fix the thead relative to the body scrolling
        $('#divjoblist thead th:nth-child(1)').css("left", $("#divjoblist tbody").scrollLeft()); //fix the first cell of the header
        $('#divjoblist tbody td:nth-child(1)').css("left", $("#divjoblist tbody").scrollLeft()); //fix the first column of tdbody

        if (($("#divjoblist tbody").scrollLeft() + $("#divjoblist tbody").outerWidth()) > $("#divjoblist tbody")[0].scrollWidth) {
            var w = $("#divjoblist tbody").scrollLeft() + $("#divjoblist tbody").outerWidth();
            var pages = Math.round((parseInt($('#hdnJobListCount').val()) / 6));
            pages = pages + ((parseInt($('#hdnJobListCount').val()) % 6) > 0 ? 1 : 0);
            if (joblistpageNumber <= pages) {

                if (pageajaxcall.indexOf(joblistpageNumber) > 0)
                { }
                else
                {
                    pageajaxcall = pageajaxcall + ',' + joblistpageNumber;
                    popupForJobListWithParameterAjaxCall(job, project, joblistpageNumber, 6, projectName);
                    $("#divjoblist tbody").scrollLeft(w);

                    $('#divjoblist thead').css("left", -$("#divjoblist tbody").scrollLeft()); //fix the thead relative to the body scrolling
                    $('#divjoblist thead th:nth-child(1)').css("left", $("#divjoblist tbody").scrollLeft()); //fix the first cell of the header
                    $('#divjoblist tbody td:nth-child(1)').css("left", $("#divjoblist tbody").scrollLeft()); //fix the first column of tdbody
                }
            }
        }

    });
});

function viewReadyToPull(ProjectNo, JobNo) {

    $('#partialReadyToPull').load("/ManageProject/_ReadyToPullTemplate?ProjectNo=" + ProjectNo + "&JobNo=" + JobNo + "", function () {
        $(".icheckbox,.iradio").iCheck({ checkboxClass: 'icheckbox_minimal-grey', radioClass: 'iradio_minimal-grey' });
        if ($("input.fileinput").length > 0) {
            $("input.fileinput").bootstrapFileInput();
        }

        $('.onactive input').on('ifChanged', function (event) {
            var abc = event.target;
            $('#' + abc.id).parentsUntil('.onactive').find('.help-block').toggleClass('hide')
        });
    });
}

function GetCookieValue(cookiename) {
    var cookievalue = '';
    var result = document.cookie;
    var cookieArray = result.split("; ");
    for (var i = 0; i < cookieArray.length; i++) {
        var Name = $.trim(cookieArray[i].split("=")[0]);
        if (Name == $.trim(cookiename)) {
            cookievalue = cookieArray[i].split("=")[1];
        }
    }
    return cookievalue;
}

function popupForJobListWithParameter(ths) {
    pageajaxcall = '0';
    job = $(ths).attr("data-job");
    project = $(ths).attr("data-project");
    projectName = $(ths).attr("data-project-name");

    joblistpageNumber = 1;
    popupForJobListWithParameterAjaxCall(job, project, joblistpageNumber, 6, projectName);

}
var storejoblistheader = [];
var storejoblistbody = [];
var tbllist = [];
var col = [];
function popupForJobListWithParameterAjaxCall(job, project, pageNumber, PageSize, projectName) {
    $.ajax({
        type: "POST",
        url: "/ManageProject/GetAllJobListWithParameter/",
        data: {
            'ProjectNumber': project, 'JobNo': job, 'SearchTerm': '', 'PageNumber': pageNumber, 'PageSize': PageSize,
        },
        success: function (data) {
            joblistpageNumber = joblistpageNumber + 1;
            if (pageNumber == 1) {
                $('#thjoblist').html('');
                $('#tdjoblist').html('');
                $('.job-head').html(projectName);
                $('#hdnJobListCount').val(data.totalCount);
                bindjobListWithParameter(data, project, job, data.projectCompanyId);
            }
            else {
                bindjobListWithParameterOnScroll(data, project, job, data.projectCompanyId);
            }

        }
    })
}

function bindjobListWithParameter(data, project, job, projectCompanyId) {
    var tbllist = JSON.parse(data.tbl);
    var col = JSON.parse(data.col);


    $('#joblistwithparameter').modal('show');

    var theaddata = '<tr class="joblistinsidetr">';

    if (col.length > 5) {
        $('#tdjoblist').removeClass('not-show-scroll');
    }
    else {
        $('#tdjoblist').addClass('not-show-scroll');
    }
    for (var i = 0; i < col.length; i++) {
        theaddata += '<th>' + col[i] + '</th>';
    }

    $('#joblistwithparameter > .modal-dialog.modal-box').removeClass('modal-box-2 modal-box-3 modal-box-4 modal-box-5 modal-box-6');
    var className = 'modal-box-' + col.length;
    $('#joblistwithparameter > .modal-dialog.modal-box').addClass(className);

    //for (var i = 0; i < (6 - col.length) ; i++) {
    //    theaddata += '<th></th>';
    //}
    theaddata += '</tr>';

    $('#thjoblist').append(theaddata);

    var tbodydata = '<tr>';
    for (var j = 0; j < tbllist.length; j++) {

        var tbodydata = '<tr class="joblistinsidebodytr">';
        for (var key in tbllist[j]) {
            if (key != 'Type') {
                if (j == 0) {
                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        tbodydata += '<td><input type="text" class="txt-box-description" name="txtDesc" style="margin: 0px; width: 148px; height: 31px;" Value="' + tbllist[j][key] + '"/><button type="submit" style="padding: 2px 4px 0px 4px;" data-jobno="' + key + '" onclick="saveDescriptionOfJob(this)" class="save-btn"><i class="fa fa-floppy-o setfont-size"></i></button></td>';
                    }
                    else {
                        tbodydata += '<td>' + tbllist[j][key] + '</td>';
                    }
                }

                else if (tbllist[j].Type == 'Drawings Link' || tbllist[j].Type == 'Documents Link' || tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Milestone Link') {
                    if (tbllist[j][key] != null && tbllist[j][key] != '') {
                        var doc = JSON.parse(tbllist[j][key]);
                        tbodydata += '<td>';
                        var tda = '';
                        //for (var k = 0; k < doc.length; k++) {
                        var docanddownllink = "";
                        var imgpath = "";
                        var imgName = "";
                        for (var k = 0; k < doc.length; k++) {
                            docanddownllink += "<a target='_blank' href='/ManageProject/DownloadDocumentLink?path=" + escape(doc[k].DocumentPath) + "'>" + (k + 1) + ". " + doc[k].DocumentName + "</a><br/>";
                            imgpath += escape(doc[k].DocumentPath) + ',';
                            imgName += escape(doc[k].DocumentName) + ',';
                        }
                        if (doc.length > 0) {
                            // debugger;
                            if (tbllist[j].Type == 'Drawings Link' || tbllist[j].Type == 'Documents Link') {
                                //tda += '<a target="_blank" href="/ManageProject/DownloadDocumentLink?path=' + escape(doc[0].DocumentPath) + '">' + doc[0].DocumentName + '</a><br/>';
                                tda += "<a  href=\"javascript:void(0);\" title=\"Download Link\" data-toggle=\"popover\" data-placement=\"right\" data-html=\"true\" data-content=\"" + docanddownllink + "\"><img src=\"/img/view-icon-red.png\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"View\" /></a>";
                            }

                            else if (tbllist[j].Type == 'Photos Link') {
                                var captionJobPopup = 'Project # ' + project + ', Bank # ' + job;
                                tda += "<a  href=\"javascript:void(0);\" onclick=\"openPhotoLink('" + imgpath + "','" + captionJobPopup + "', '" + imgName + "')\"><img src=\"/img/view-icon-red.png\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"View\" /></a>";
                            }

                        }
                        //tbodydata = tbodydata + tda + '<a style="float: right;"  href="#" title="Upload" data-toggle="tooltip"  onclick="GoToBankContacts("' + job + '", "' + project + '", "' + projectCompanyId + '")" data-placement="top">Upload</a></td>';
                        if (tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Documents Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata = tbodydata + tda + "  <a style=\"float: right;\" onclick=\"GoToBankContacts(this,' " + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0);\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1 setfont-size\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i> </a></td>";
                            }
                            else {
                                tbodydata = tbodydata + tda + "</td>";
                            }

                        }
                        else if (tbllist[j].Type == 'Drawings Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata = tbodydata + tda + "  <a style=\"float: right;\" onclick=\"GoToBankDrawing(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0);\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1 setfont-size\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata = tbodydata + tda + "</td>";
                            }
                        }
                    }
                    else {
                        if (tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Documents Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata += "<td>N/A  <a style=\"float: right;\" onclick=\"GoToBankContacts(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1 setfont-size\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata = tbodydata + tda + "<td>N/A  </td>";
                            }
                        }
                        else if (tbllist[j].Type == 'Drawings Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata += "<td>N/A  <a style=\"float: right;\" onclick=\"GoToBankDrawing(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1 setfont-size\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata = tbodydata + tda + "<td>N/A  </td>";
                            }
                        }
                        else if (tbllist[j].Type == 'Milestone Link') {
                            //if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                            tbodydata += "<td> <a style=\"float: left;\" onclick=\"GoToBankMilestone(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Milestone\" data-toggle=\"tooltip\" data-placement=\"top\"><img id='imgMilestonelogo' title='Milestone Logo' src='/images/img/milestone.png' /></a></td>";
                            //}
                            //else {
                            //    tbodydata = tbodydata + tda + "<td> </td>";
                            //}
                        }
                    }
                }
                else {
                    if (tbllist[j][key] != '' && tbllist[j][key].indexOf("SSSO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("2SSO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="2 speed side opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("BIPA") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Biparting Freight">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("C2") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Center Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("SSCO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Center Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("TL") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening - Left ">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("TR") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening – Right">' + tbllist[j][key] + '</td>';
                    }
                    else {
                        tbodydata += '<td>' + tbllist[j][key] + '</td>';
                    }
                }
            }
            else {
                tbodydata += '<td>' + tbllist[j][key] + '</td>';
            }

        }
        //for (var i = 0; i < (6 - col.length) ; i++) {
        //    tbodydata += '<td></td>';
        //}
        tbodydata += '</tr>';
        $('#tdjoblist').append(tbodydata);

        $('[data-toggle="popover"]').popover();

    }
    //$('#manualinputlabel').click(function (e) {
    //    $('.popover-title').append('<button id="popovercloseid" onclick="popoverclose()" type="button" class="close">&times;</button>');
    //});

}
function GoToBankContacts(ths, ProjectNo, CompanyId) {
    window.open(
  '/ManageProject/ProjectJobMainView/' + $('#tbljoblist th').eq($(ths).parent().index()).text() + '/' + ProjectNo + '/' + CompanyId + '/Documents',
  '_blank' // <- This is what makes it open in a new window.
);
}

function GoToBankDrawing(ths, ProjectNo, CompanyId) {
    window.open(
'/ManageProject/ProjectJobMainView/' + $('#tbljoblist th').eq($(ths).parent().index()).text() + '/' + ProjectNo + '/' + CompanyId + '/Drawing',
'_blank' // <- This is what makes it open in a new window.
);
}

function GoToBankMilestone(ths, ProjectNo, CompanyId) {
    window.open(
'/ManageProject/ProjectJobMainView/' + $('#tbljoblist th').eq($(ths).parent().index()).text() + '/' + ProjectNo + '/' + CompanyId + '/Milestone',
'_blank' // <- This is what makes it open in a new window.
);
}

function popoverclose() {
    $('#manualinputlabel').popover('hide');
}
function bindjobListWithParameterOnScroll(data, project, job, projectCompanyId) {
    var tbllist = JSON.parse(data.tbl);
    var col = JSON.parse(data.col);
    var theaddata = '';

    for (var i = 0; i < col.length; i++) {
        if (col[i] != 'Type') {
            theaddata += '<th>' + col[i] + '</th>';
        }
    }

    $('.joblistinsidetr').append(theaddata);


    for (var j = 0; j < tbllist.length; j++) {
        var tbodydata = '';
        for (var key in tbllist[j]) {
            if (key != 'Type') {
                if (j == 0) {
                    if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                        tbodydata += '<td><textarea class="txt-box-description">' + tbllist[j][key] + '</textarea><button type="submit" data-jobno="' + key + '" onclick="saveDescriptionOfJob(this);" class="save-btn"><i class="fa fa-floppy-o"></i></button></td>';
                    }
                    else {
                        tbodydata += '<td>' + tbllist[j][key] + '</td>';
                    }
                }

                else if (tbllist[j].Type == 'Drawings Link' || tbllist[j].Type == 'Documents Link' || tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Milestone Link') {
                    //debugger;
                    if (tbllist[j][key] != null && tbllist[j][key] != '') {
                        var doc = JSON.parse(tbllist[j][key]);
                        tbodydata += '<td>';
                        var tda = '';
                        var docanddownllink = "";
                        var imgpath = "";
                        var imgName = "";
                        for (var k = 0; k < doc.length; k++) {
                            docanddownllink += "<a target='_blank' href='/ManageProject/DownloadDocumentLink?path=" + escape(doc[k].DocumentPath) + "'>" + (k + 1) + ". " + doc[k].DocumentName + "</a><br/>";
                            imgpath += escape(doc[k].DocumentPath) + ',';
                            imgName += escape(doc[k].DocumentName) + ',';
                        }
                        if (doc.length > 0) {
                            //debugger;
                            if (tbllist[j].Type == 'Drawings Link' || tbllist[j].Type == 'Documents Link') {
                                tda += "<a  href=\"javascript:void(0)\" title=\"Download Link\" data-toggle=\"popover\" data-placement=\"right\" data-html=\"true\" data-content=\"" + docanddownllink + "\"><img src=\"/img/view-icon-red.png\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"View\" /></a>";
                            }
                            else if (tbllist[j].Type == 'Photos Link') {
                                var captionJobPopup = 'Project # ' + project + ', Bank # ' + job;
                                tda += "<a  href=\"javascript:void(0);\" onclick=\"openPhotoLink('" + imgpath + "','" + captionJobPopup + "','" + imgName + "')\"><img src=\"/img/view-icon-red.png\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"View\" /></a>";
                            }


                        }
                        if (tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Documents Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata = tbodydata + tda + "<a style=\"float: right;\" onclick=\"GoToBankContacts(this,' " + project + "',' " + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata = tbodydata + tda + "</td>";
                            }
                        }
                        else if (tbllist[j].Type == 'Drawings Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata = tbodydata + tda + "<a style=\"float: right;\" onclick=\"GoToBankDrawing(this, '" + project + "',' " + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            } else {
                                tbodydata = tbodydata + tda + "</td>";
                            }
                        }


                    }
                    else {
                        if (tbllist[j].Type == 'Photos Link' || tbllist[j].Type == 'Documents Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata += "<td>N/A  <a style=\"float: right;\" onclick=\"GoToBankContacts(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata += "<td>N/A </td>";
                            }
                        }
                        else if (tbllist[j].Type == 'Drawings Link') {
                            if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                                tbodydata += "<td>N/A  <a style=\"float: right;\" onclick=\"GoToBankDrawing(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Upload\" data-toggle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-upload fa-1\" aria-hidden=\"true\" style=\"color: #dc0000;\"></i></a></td>";
                            }
                            else {
                                tbodydata += "<td>N/A </td>";
                            }
                        }

                        else if (tbllist[j].Type == 'Milestone Link') {
                            //if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
                            tbodydata += "<td><a style=\"float: left;\" onclick=\"GoToBankMilestone(this, '" + project + "', '" + projectCompanyId + "')\"   href=\"javascript:void(0)\" title=\"Milestone\" data-toggle=\"tooltip\" data-placement=\"top\"><img id='imgMilestonelogo' title='Milestone Logo' src='/images/img/milestone.png' /></a></td>";
                            //}
                            //else {
                            //    tbodydata += "<td></td>";
                            //}
                        }
                    }
                }
                else {
                    if (tbllist[j][key] != '' && tbllist[j][key].indexOf("SSSO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("2SSO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="2 speed side opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("BIPA") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Biparting Freight">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("C2") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Center Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("SSCO") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Center Opening">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("TL") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening - Left ">' + tbllist[j][key] + '</td>';
                    }
                    else if (tbllist[j][key] != '' && tbllist[j][key].indexOf("TR") > -1) {
                        tbodydata += '<td data-toggle="tooltip" data-placement="top" title="Single Speed Side Opening – Right">' + tbllist[j][key] + '</td>';
                    }
                    else {
                        tbodydata += '<td>' + tbllist[j][key] + '</td>';
                    }
                }
            }

        }
        $('#tdjoblist').find('tr')[j].innerHTML = $('#tdjoblist').find('tr')[j].innerHTML + tbodydata;
        $('[data-toggle="popover"]').popover();
    }
    //$('#manualinputlabel').click(function (e) {
    //    $('.popover-title').append('<button id="popovercloseid" onclick="popoverclose()" type="button" class="close">&times;</button>');
    //});
}

function openPhotoLink(data, caption, imgName) {
    $('#divphotolinkgrid').modal('show');
    $('#divbodyphotolinkgrid').html('');

    var griddata = "<div class=\"row\">";
    var imageName = "";

    $.each(data.split(','), function (index, value) {
        if (value != "") {
            imageName = decodeURIComponent(imgName.split(',')[index]);
            griddata += "<div class=\"lightboxcolumn\"><h3 class=\"text-align-center title-light-box\">" + imageName + "</h3><img alt=\"" + imageName + "\" src=\"" + value + "\" style=\"width:100%\" onclick=\"openModal('" + data + "','" + caption + "','" + imageName + "');currentSlide(" + (index + 1) + ")\" class=\"hover-shadow demo cursor\"></div>";
        }
    });

    griddata += "</div>";

    $('#divbodyphotolinkgrid').append(griddata);
    // alert(data);
}

function saveDescriptionOfJob(ths) {

    var jobNumber = $(ths).attr("data-jobno");
    $.ajax({
        type: "POST",
        url: "/ManageProject/Update_BankDesc_ByJobNo",
        data: { Description: $(ths).parent().find('input[name="txtDesc"]').val(), SessionID: '', JobNo: jobNumber },
        success: function (data) {
            if (data) {
                $('#CommonSuccessMessage').html('Successfully updated.');
                $('#CommonSuccessModal').modal('show');
            }
        }
    })
}