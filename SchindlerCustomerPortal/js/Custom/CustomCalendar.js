﻿
var storedStatus;
var isValid = false;
//var apiLink = "http://192.168.100.133/api/";
var addEventFlag = 0;
var VersionNumber = 0;

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    // If IE, return version number.
    if (Idx > 0)
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));

        // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./))
        return 11;

    else
        return 0; //It is not IE
}

function addEvent(StartDate, TodayDate) {

    if (VersionNumber > 0) {
        
        if (new Date(StartDate).getTime() >= new Date(TodayDate).getTime() || StartDate.toLocaleDateString() == TodayDate.toLocaleDateString()) {
            //if (StartDate.toLocaleDateString() >= TodayDate.toLocaleDateString()) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (Date.parse(StartDate.toLocaleDateString()) >= Date.parse(TodayDate.toLocaleDateString())) {
            return true;
        }
        else {
            return false;
        }
    }
}

$(document).ready(function () {


    ToList = [];
    var id = $('#hdnLoggedUserId').val().trim();
    var companyId = $('#hdnCompanyID').val();
    var calarray = [];
    var model = { Id: id, SearchTearm: '', StartDate: '' };
    var urlLink = apiLink + "GetEventsByUserID";
    var flag = true;

    VersionNumber = GetIEVersion();

    $("#hdnstatustype").val($('#editstatustype').val());

    $.ajax({
        type: "POST",
        url: urlLink,
        data: JSON.stringify(model),
        datatype: "JSON",
      //  contentType: "application/json; charset=utf-8",
        async: false,
        beforeSend: function () {
            $("#LoadingImage").css("display", "block");
        },
        complete: function () {
            $("#LoadingImage").css("display", "none");
        },
        success: function (data) {
            debugger
            var defaultdate = new Date();

            
            $.each(data.Data.jobMasterList.Data, function (i, val) {
                calarray.push({
                    title: val.ProjectNo + " - " + val.JobNo + " - " + val.MilestoneShortName,
                    start: val.Date, //$("#addFolUpMethod option:selected").text(),
                    end: "",
                    id: i,
                    color: val.Color,
                    textColor: 'black',
                    description: val.JobDesc,
                    data: { Idfor: 'job', Status: val.StatusName, Overdue: val.ProjectNo + '_' + val.JobNo + '_' + val.MilestoneName, FullDesc: val.ProjectNo + " - " + val.JobNo + " - " + val.MilestoneName }
                });
            });

                $.each(data.Data.eventList, function (j, val) {
                    calarray.push({
                        title: val.Subject,
                        start: val.StartDate, //$("#addFolUpMethod option:selected").text(),
                        end: val.EndDate + ' 23:59:00',
                        id: j,
                        color: val.CategoryData.ColorCode,
                        textColor: 'black',
                        description: val.MessageBody,
                        data: { Idfor: 'event', Location: val.Location, SendTo: val.SendTo, Uid: val.Id }
                    });
                });

                /*
                $.each(data.Data.jobMasterList.Data, function (i, val) {
                    calarray[calarray.length] = {
                        "title": val.ProjectNo + " - " + val.JobNo + " - " + val.MilestoneShortName,
                        "start": val.Date, //$("#addFolUpMethod option:selected").text(),
                        "end": "",
                        "id": i,
                        "color": val.Color,
                        "textColor": 'black',
                        "description": val.JobDesc,
                        "data": { Idfor: 'job', Status: val.StatusName, Overdue: val.ProjectNo + '_' + val.JobNo + '_' + val.MilestoneName, FullDesc: val.ProjectNo + " - " + val.JobNo + " - " + val.MilestoneName }
                    }
                })
    
                $.each(data.Data.eventList, function (j, val) {
                    calarray[calarray.length] = {
                        "title": val.Subject,
                        "start": val.StartDate, //$("#addFolUpMethod option:selected").text(),
                        "end": val.EndDate + ' 23:59:00',
                        "id": j,
                        "color": val.CategoryData.ColorCode,
                        "textColor": 'black',
                        "description": val.MessageBody,
                        "data": { Idfor: 'event', Location: val.Location, SendTo: val.SendTo, Uid: val.Id }
                    }
    
                    $('#calendarCount').val(data.Data.eventList.Count);
                })
    */
                var jsoncalarray = JSON.stringify(calarray);

                $('#calendarid').fullCalendar({

                    theme: true,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    defaultDate: defaultdate,
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    displayEventTime: false,
                    select: function (start, end) {

                        var startdate = moment();
                        startdate = startdate.subtract(1, "days");

                        if (start.isBefore(startdate)) {
                            $('.fc-highlight').css('background-color', '#ffffff');
                            return false;
                        } else {
                            $('.fc-highlight').css('background-color', '#bce8f1');

                            $.ajax({
                                url: apiLink + 'GetCalendarCategoryList',
                                type: 'GET',
                                async: false,
                                success: function (data) {

                                    successcategory(data.Data);
                                },
                                error: errormiletype
                            });

                            //$("#AddFollowUpCalendarModal").modal();
                            $("#startdate").val(start._d.toLocaleDateString());
                            $('#TodoModal').modal('show');
                        }
                    },
                    eventRender: function (event, element) {

                        if (event.data.Idfor.indexOf("job") != -1) {

                            var tooltip = event.data.FullDesc + ", Bank Desc: " + event.description;

                            $(element).attr("data-original-title", tooltip);
                            $(element).tooltip({ container: "body" });
                            element.attr('href', 'javascript:void(0);');

                            if (event.data.Status == 'O') {
                                element.attr('name', 'overdueMilestoneClick');
                                element.attr('title', 'Overdue');
                                element.attr('overdue', event.data.Overdue);

                            } else if (event.data.Status == 'C') {
                                element.attr('title', 'Completed');

                            } else if (event.data.Status == 'P') {
                                element.attr('title', 'Pending/In Progress');

                            } else if (event.data.Status == 'D') {
                                element.attr('title', 'Due in 7 Days');
                                element.attr('name', 'overdueMilestoneClick');
                                element.attr('overdue', event.data.Overdue);

                            } else if (event.data.Status == 'N') {
                                element.attr('title', 'Not applicable');

                            }
                        }
                        else {

                            var tooltip = "Subject: " + event.title + "<br/>Location: " + ((event.data.Location == '') ? '-' : event.data.Location) + "<br/>Start Date: " + event.start._i + "<br/>End Date: " + (event.end._i.replace(" 23:59:00", ""));
                            element.attr('name', 'eventDetail');
                            $(element).attr("data-html", "true");
                            $(element).attr("data-original-title", tooltip);
                            element.attr('href', 'javascript:void(0);');
                            element.attr('data-value', event.data.Uid);

                            $(element).tooltip({ container: "body" });
                            element.attr('title', 'EventList');
                        }

                        element.click(function () {
                            //debugger
                            //EditCalendarFolUp(event);
                        });

                    },
                    viewRender: function (view, element) {
                        var startdate = moment();
                        startdate = startdate.subtract(1, "days");
                        if (view.name == 'month') {
                            $('.fc-day-top').each(function () {

                                var abc = new Date($(this).attr('data-date'));
                                var def = new Date();
                                //var month = abc.getMonth()+1;
                                //var month2 = def.getMonth()+1;
                                //var date = abc.getDate();

                                //var date2 = def.getDate();

                                if (addEvent(abc, def)) {

                                    $(this).css('position', 'relative');
                                    $(this).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                                    // return false;
                                }
                            });
                        }
                    },
                    dayClick: (function () {

                        return function (date, jsEvent, view, event) {

                            //var startdate = moment();
                            //startdate = startdate.subtract(1, "days");

                            if (date.isBefore(startdate)) {
                                // $('.fc-highlight').css('background-color', '#ffffff');
                                return false;
                            }
                            else {
                                $(".add_event").bind('click', function () {

                                    var UTCDate = (date._d.getUTCMonth() + 1) + '/' + date._d.getUTCDate() + '/' + date._d.getUTCFullYear();

                                    $('#startdate').val(UTCDate);
                                    ShowEventPopup(date, "ADDEVENT");
                                    return false;
                                });
                            }
                        }
                    })(),
                    events: calarray
                });

            },
                error: function (data) {
                    //$("#LoadingImage").css("display", "none");
                    alert('The Requested URL Cannot be Found..!');
                }

        });

    $('.RequiredField').on('blur focusout', function () {

        var id = $(this).attr('id');

        if ($.trim($(this).val()) == '') {
            isValid = false;
            $(this).css({
                "border": "1px solid red"
            });
        }
        else {
            isValid = true;
            $(this).css({
                "border": "1px solid #D5D5D5"
            });
        }
    })

    $(document).on("click", 'a[name="overdueMilestoneClick"]', function (event) {

        var titleValue = $(this).attr('overdue');
        $('#hdnMilestoneNoteValue').val(titleValue);
        var titleArray = titleValue.split('_');

        $('#editstatustype').css('border-color', '#D5D5D5');

        $.ajax({
            url: '/Common/BindStatusList',
            type: 'GET',
            async: false,
            data: { module: 'Milestone' },

            success: successmiletype,
            error: errormiletype
        });

        GetDueDateAndStatusOfMilestone(titleArray[0], titleArray[1], titleArray[2]);

        //debugger

        $('#hdnProjectNo').val(titleArray[0]);
        $('#hdnJobNo').val(titleArray[1]);
        $('#hdnMilestoneName').val(titleArray[2]);

        if (titleArray[2] != 'Job-Site Ready') {
            //$('#divReadyToPull').removeClass('input-group');
            $('#readyToPull').hide();
            $('#job-site-miletstone-btn').addClass('hide-show-btn');
            $('#job-site-question').addClass('hide-show-btn');
            $('#simple-miletstone-btn').removeClass('hide-show-btn');
        }
        else {
            $('#job-site-question').removeClass('hide-show-btn');
            $('#job-site-miletstone-btn').removeClass('hide-show-btn');
            $('#simple-miletstone-btn').addClass('hide-show-btn');

            //$('#divReadyToPull').addClass('input-group');
            $('#readyToPull').hide();
            viewReadyToPull($('#hdnProjectNo').val(), $('#hdnJobNo').val());

        }

        var result = "Add new note for <b>" + titleArray[2] + "</b> milestone of Bank <b>" + titleArray[1] + "</b> under Project No. <b>" + titleArray[0] + "</b> which has Planned Date <b>" + $('#Editduedate').val() + "</b>";

        $('#milestoneNoteTitle').html('');
        $('#milestoneNoteTitle').html(result);
        $('#milestoneNoteModal').modal('show');

        $('#spanMilestoneName').html('<b>' + titleArray[2] + '<b>');
        if ($('#hdnUserRoleId').val() == 1 || $('#hdnUserRoleId').val() == 3) {
            $("#Editduedate").prop('disabled', false);
            $("#Editcompleteddate").prop('disabled', false);
            $("#editstatustype").prop('disabled', false);
        }
        else {
            $("#Editduedate").prop('disabled', true);
            $("#Editcompleteddate").prop('disabled', true);
            $("#editstatustype").prop('disabled', true);
        }
    })

    $(document).on("click", 'a[name="eventDetail"]', function (event) {

        var UserID = id;
        var urlLinkEvent = apiLink + 'GetEventByEventID?ID=' + $(this).data("value");

        $.ajax({
            type: "GET",
            url: urlLinkEvent,
            async: false,
            success: function (data) {

                if (data.Status == 1) {

                    var today = new Date();

                    var basicData = data.Data[0];
                    var result = "";

                    $('#hdnEventID').val(basicData.Id);
                    $('#ViewSendTo').val(basicData.SendTo);
                    $('#ViewSubject').val(basicData.Subject);
                    $('#ViewLocation').val(basicData.Location);

                    $('#ViewEndDate').val(basicData.EndDate);
                    $('#ViewMessageBody').val(basicData.MessageBody);

                    successcategory(data.List);

                    //$('#TodoModal').modal('show');

                    $('#ViewStartDate').datepicker('remove');
                    $('#ViewStartDate').val('');
                    $('#ViewStartDate').datepicker({
                        startDate: today.toLocaleDateString(),
                        autoclose: true,
                        forceParse: false
                    });
                    //$('#ViewStartDate').datepicker("setDate", basicData.StartDate);
                    $('#ViewStartDate').val(basicData.StartDate);


                    $('#ViewEndDate').datepicker('remove');
                    $('#ViewEndDate').val('');
                    $('#ViewEndDate').datepicker({
                        startDate: $('#ViewStartDate').val(), //today.toLocaleDateString(),
                        autoclose: true,
                        forceParse: false
                    });
                    //$('#ViewEndDate').datepicker("setDate", basicData.EndDate);
                    $('#ViewEndDate').val(basicData.EndDate);

                    $('#ViewStartDate').on("changeDate", function () {
                        $('#ViewEndDate').datepicker('remove');
                        $('#ViewEndDate').val('');
                        $('#ViewEndDate').datepicker({
                            startDate: $('#ViewStartDate').val(),
                            autoclose: true
                        });
                    })


                    if (basicData.CategoryData.CatID != 0) {
                        $('#ViewCategoryType').val(basicData.CategoryData.CatID);
                        $('.selectpicker').selectpicker('refresh');
                    }

                    $('#ViewUpdateYes').prop('hidden', true);
                    $('#ViewCancel').prop('hidden', true);
                    $('#ViewDeleteYes').prop('hidden', true);

                    if (UserID == basicData.CreatedBy) {
                        result = "Event Detail";
                        $('.View-Edit').css({ display: 'initial' });
                        $('#EventDetail').find('input,select,textarea').removeAttr('disabled');
                        $('#ViewUpdateYes').prop('hidden', false);
                        $('#ViewCancel').prop('hidden', false);
                        $('#ViewDeleteYes').prop('hidden', false);
                        $('#ViewEventYes').hide();
                    }
                    else {

                        $('.View-Edit').css({ display: 'none' });
                        $('#ViewEventYes').show();
                        $('#EventDetail').find('input,select,textarea').removeAttr('disabled');
                        $('#EventDetail').find('input:not(:button),select,textarea').attr('disabled', 'disabled');
                        result = "View Event";
                        $('.selectpicker.btn-default').addClass('disabled');
                    }

                    $('#EventDetailTitle').html(result);
                    $('#EventDetail').modal('show');

                } else {
                    alert(data.Message);
                }
            },
            error: function () {
                alert('Error while fetching data...');
            }
        });
    })

    $(document).on("click", '#btnAddEventYes', function (event) {
        //debugger
        if ($.trim($('#subject').val()) == '') {
            isValid = false;
            $('#subject').css({
                "border": "1px solid red"
            });
        }
        else if ($.trim($('#startdate').val()) == '') {
            isValid = false;
            $('#startdate').css({
                "border": "1px solid red"
            });
        }
        else if ($('#addcategorytype').val() == "0") {
            isValid = false;
            $('button[data-id=addcategorytype]').css({
                "border": "1px solid red"
            });
        }
        else {
            isValid = true;
        }

        if (isValid == true) {

            $('#TodoModal').modal('hide');
            $("#LoadingImage").css("display", "block");

            if ($('#sendto').val() != '') {
                if (($('#sendto').val().match(", $") || $('#sendto').val().match(",$")) == null) {
                    $('#sendto').val($('#sendto').val() + ', ');
                }
            }

            var addEventData = {
                SendTo: $('#sendto').val(),
                Subject: $('#subject').val(),
                MessageBody: $('#AddMessageBody').val(),
                CategoryID: $('#addcategorytype').val(),
                Location: $('#location').val(),
                StartDate: $('#startdate').val(),
                EndDate: $('#enddate').val(),
                CreatedBy: $('#hdnLoggedUserId').val(),
                AppAuditID: $('#hdnAppAuditID').val(),
                SessionID: $('#hdnSessionID').val()
            };

            $.ajax({
                type: "POST",
                url: apiLink + "AddCalendarEvent",
                data: JSON.stringify(addEventData),
                datatype: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("#LoadingImage").css("display", "none");

                    var msg = "";

                    if (data.Status == "1") {
                        $('#EventMessage').css('color', 'green');
                        msg = data.Message;
                        flag = true;
                        //location.reload();
                    } else {
                        $('#EventMessage').css('color', 'red');
                        msg = data.Message;
                        flag = false;
                        $('#TodoModal').modal('show');
                    }
                    //$('#TodoModal').find('input:not(:button),textarea').val("");
                    $('#EventMessage').html(msg);
                    $('#EventSuccessMessage').modal('show');
                    //location.reload();
                },
                error: function (data) {
                    $("#LoadingImage").css("display", "none");
                    alert("Internal Server Error, please contact admin.");
                }
            })

        }
        else {
            return false;
        }
    })

    $(document).on("keydown.autocomplete", '#sendto', function (event) {

        $(this).autocomplete(txtTooptions);

    })

    $(document).on("keydown.autocomplete", '#ViewSendTo', function (event) {
        $(this).autocomplete(txtUpdateoptions);
    })

    $(document).on("click", '#ViewUpdateYes', function (event) {

        if ($.trim($('#ViewSubject').val()) == '') {
            isValid = false;
            $('#ViewSubject').css({
                "border": "1px solid red"
            });
        }
        else if ($.trim($('#ViewStartDate').val()) == '') {
            isValid = false;
            $('#ViewStartDate').css({
                "border": "1px solid red"
            });
        }
        else if ($('#ViewCategoryType').val() == "0") {
            isValid = false;
            $('button[data-id=ViewCategoryType]').css({
                "border": "1px solid red"
            });
        }
            //else if ($.trim($('#ViewEndDate').val()) == '') {
            //    isValid = false;
            //    $('#ViewEndDate').css({
            //        "border": "1px solid red"
            //    });
            //}
        else {
            isValid = true;
        }

        if (isValid == true) {

            $('#EventDetail').modal('hide');
            $("#LoadingImage").css("display", "block");

            if ($('#ViewSendTo').val() != '') {
                if (($('#ViewSendTo').val().match(", $") || $('#ViewSendTo').val().match(",$")) == null) {
                    $('#ViewSendTo').val($('#ViewSendTo').val() + ', ');
                }
            }

            var data = {
                SendTo: $('#ViewSendTo').val(),
                Subject: $('#ViewSubject').val(),
                Location: $('#ViewLocation').val(),
                StartDate: $('#ViewStartDate').val(),
                EndDate: $('#ViewEndDate').val(),
                MessageBody: $('#ViewMessageBody').val(),
                CategoryID: $('#ViewCategoryType').val(),
                AppAuditID: $('#hdnAppAuditID').val(),
                SessionID: $('#hdnSessionID').val(),
                UpdatedBy: id,
                Id: $('#hdnEventID').val()
            }

            $.ajax({
                type: "POST",
                url: apiLink + "UpdateCalendarEvent",
                data: JSON.stringify(data),
                datatype: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#EventDetail').modal('hide');
                    var msg = "";

                    if (data.Status == "1") {
                        $('#EventMessage').css('color', 'green');
                        msg = data.Message;
                        flag = true;
                    } else {
                        $('#EventMessage').css('color', 'red');
                        msg = data.Message;
                        flag = false;
                        $('#EventDetail').modal('show');

                    }
                    $("#LoadingImage").css("display", "none");
                    $('#EventMessage').html(msg);
                    $('#EventSuccessMessage').modal('show');
                    //location.reload();
                },
                error: function (data) {
                    $("#LoadingImage").css("display", "none");
                    alert("Internal Server Error, please contact admin.");
                }
            })

        }
        else {
            return false;
        }
    })

    $(document).on("click", '#ViewDeleteYes', function (event) {

        $('#confirm').modal('show');

    })

    $(document).on('click', '#DeleteEvent', function (event) {

        var data = {
            Id: $('#hdnEventID').val(),
            DeletedBy: id,
            AppAuditID: $('#hdnAppAuditID').val(),
            SessionID: $('#hdnSessionID').val()
        }

        $("#LoadingImage").css("display", "block");

        $.ajax({
            type: "POST",
            url: apiLink + "RemoveCalendarEvents",
            data: JSON.stringify(data),
            datatype: "JSON",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('#EventDetail').modal('hide');
                var msg = "";

                if (data.Status == "1") {
                    $('#EventMessage').css('color', 'green');
                    msg = data.Message;
                } else {
                    $('#EventMessage').css('color', 'red');
                    msg = data.Message;
                }

                $('#EventMessage').html(msg);
                $('#EventSuccessMessage').modal('show');
                //location.reload();
            },
            error: function (data) {
                alert("Internal Server Error, please contact admin.");
            }
        })

        $("#LoadingImage").css("display", "none");
    })

    $('#sendto').focus();

    if ($('#sendto').val() != '') {
        ToList[ToList.length] = { Email: $('#sendto').val(), UserID: $('#sendto').val() }
    }

    var txtTooptions = {
        source: function (request, response) {
            var companyId = $('#hdnCompanyID').val();

            $.ajax({
                url: apiLink + "NotifyToData",
                data: '{ CompanyID:"' + companyId + '", searchTerm:"' + $('#sendto').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data.Data, function (item) {
                        return {
                            label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                            value: item.Email,
                            UserID: item.UserID
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {


            // insert userId into array
            if (ToList.length == 0) {
                ToList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
            }
            else {
                ToList[ToList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
            }

            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        },
        //change: function (event, ui) {
        //    if (!ui.item) {
        //        $(this).val("");
        //        //alert('');
        //        //$('#empty-message').show();
        //    } else {
        //        //$('#empty-message').hide();
        //    }
        //},
        minLength: 2,
    };

    $('#ViewSendTo').focus();
    if ($('#ViewSendTo').val() != '') {
        ToList[ToList.length] = { Email: $('#ViewSendTo').val(), UserID: $('#ViewSendTo').val() }
    }

    var txtUpdateoptions = {
        source: function (request, response) {
            var companyId = $('#hdnCompanyID').val();

            $.ajax({
                url: apiLink + "NotifyToData",
                data: '{ CompanyID:"' + companyId + '", searchTerm:"' + $('#ViewSendTo').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {

                    response($.map(data.Data, function (item) {
                        return {
                            label: item.FirstName + ' ' + item.LastName + ' ' + '(' + item.Email + ')',
                            value: item.Email,
                            UserID: item.UserID
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {

            // insert userId into array
            if (ToList.length == 0) {
                ToList[0] = { Email: ui.item.value, UserID: ui.item.UserID }
            }
            else {
                ToList[ToList.length] = { Email: ui.item.value, UserID: ui.item.UserID }
            }

            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        },
        //change: function (event, ui) {
        //    if (!ui.item) {
        //        $(this).val("");
        //        //alert('');
        //        //$('#empty-message').show();
        //    } else {
        //        //$('#empty-message').hide();
        //    }
        //},
        minLength: 2,
    };

    $(document).on('hide.bs.modal', '#TodoModal', function () {

        $('#subject').css({
            "border": "1px solid #D5D5D5"
        })

        $('#startdate').css({
            "border": "1px solid #D5D5D5"
        })

        $('button[data-id=addcategorytype]').css({
            "border": "1px solid #D5D5D5"
        });
        //$('#TodoModal').find('input:not(:button),textarea').val("");
    })

    $(document).on('hide.bs.modal', '#EventDetail', function () {

        $('#EventDetail').find('input:not(:button),select,textarea').removeAttr('disabled', 'disabled');
        $('#ViewCategoryType').removeAttr('disabled');

        $('#ViewSubject').css({
            "border": "1px solid #D5D5D5"
        });

        $('#ViewStartDate').css({
            "border": "1px solid #D5D5D5"
        });

        $('#ViewEndDate').css({
            "border": "1px solid #D5D5D5"
        });

        $('button[data-id=ViewCategoryType]').css({
            "border": "1px solid #D5D5D5"
        });
    })

    $('.fc-prev-button ').click(function () {

        //var date1 = $('#calendarid').fullCalendar('getDate')._d;//.toLocaleDateString();
        //// var month = date1.getMonth();
        //var makeDate = new Date(date1);
        //var prev = new Date(makeDate.getFullYear(), makeDate.getMonth(), date1.getDate());

        //calarray.length = 0;
        //CalendarData(id, prev.toLocaleDateString());

        if (($('.fc-view-container').find('.fc-basicWeek-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-sun fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-mon fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-tue fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-wed fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-thu fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-fri fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-sat fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    if (addEvent(abc, def)) {

                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        $(datacount.selector + '').eq(j).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;cursor:pointer;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;
                    }
                }
                return
            }
        }
        if (($('.fc-view-container').find('.fc-basicDay-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-mon fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    if (addEvent(abc, def)) {

                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        $(datacount.selector + '').eq(j).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;
                    }

                }
                return
            }
        }
    });

    $('.fc-next-button ').click(function () {
        //var date1 = $('#calendarid').fullCalendar('getDate')._d;//.toLocaleDateString();
        //var makeDate = new Date(date1);
        //var next = new Date(makeDate.getFullYear(), makeDate.getMonth());

        //calarray.length = 0;
        //CalendarData(id, next.toLocaleDateString());

        if (($('.fc-view-container').find('.fc-basicWeek-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-sun fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-mon fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-tue fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-wed fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-thu fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-fri fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-sat fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    if (addEvent(abc, def)) {

                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        $(datacount.selector + '').eq(j).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;
                    }
                }
                return
            }
        }
        if (($('.fc-view-container').find('.fc-basicDay-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-mon fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    if (addEvent(abc, def)) {

                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        $(datacount.selector + '').eq(j).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;
                    }
                }
                return
            }
        }
    });

    $('.fc-today-button ').click(function () {

        //var date1 = $('#calendarid').fullCalendar('getDate')._d;//.toLocaleDateString();
        //var makeDate = new Date(date1);
        //var next = new Date(makeDate.getFullYear(), makeDate.getMonth());

        //calarray.length = 0;
        //CalendarData(id, next.toLocaleDateString());

        if (($('.fc-view-container').find('.fc-basicWeek-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-sun fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-mon fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-tue fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-wed fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-thu fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-fri fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-sat fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    //var month = abc.getMonth()+1;
                    //var month2 = def.getMonth()+1;
                    //var date = abc.getDate();
                    //var date2 = def.getDate();
                    if (addEvent(abc, def)) {

                        // $(datacount)[i].css('position', 'relative');
                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        // datacount[i].attributes[0].nodeValue = 'position: relative;';
                        $(datacount.selector + '').eq(j).append('<a class="add_event"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;

                    }

                }
                return
            }
        }
        if (($('.fc-view-container').find('.fc-basicDay-view').length != 0)) {
            var html = '<thead><tr><td class="fc-day-top fc-mon fc-past"><a class="fc-day-number"></a></td></tr></thead>';
            $('.fc-content-skeleton table').append(html);
            var datacount = $('.fc-content-skeleton table thead tr td');
            var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
            k = 0;
            for (i = 0; i <= datacount.length; i++) {

                for (j = k; j <= daycount.length - 1; j++) {
                    var abc = new Date(daycount[j].attributes[1].nodeValue);
                    var def = new Date();
                    if (addEvent(abc, def)) {

                        $(datacount.selector + '').eq(j).css('position', 'relative');
                        $(datacount.selector + '').eq(j).append('<a class="add_event"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                        k++;
                    }
                }
                return
            }
        }
    });

    $('.fc-basicWeek-button,.fc-basicDay-button').click(function () {

        if (($('.fc-view-container').find('.fc-basicWeek-view').length != 0)) {
            if ($('.fc-content-skeleton table thead tr').find('.fc-day-top').length == 0) {
                var html = '<thead><tr><td class="fc-day-top fc-sun fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-mon fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-tue fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-wed fc-past" ><a class="fc-day-number"></a></td><td class="fc-day-top fc-thu fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-fri fc-past"><a class="fc-day-number"></a></td><td class="fc-day-top fc-sat fc-past"><a class="fc-day-number"></a></td></tr></thead>';
                $('.fc-content-skeleton table').append(html);
                var datacount = $('.fc-content-skeleton table thead tr td');
                var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
                k = 0;

                for (i = 0; i <= datacount.length; i++) {

                    for (j = k; j <= daycount.length - 1; j++) {
                        var abc = new Date(daycount[j].attributes[1].nodeValue);
                        var def = new Date();
                        //if (Date.parse(abc.toLocaleDateString()) >= Date.parse(def.toLocaleDateString())) {
                        if (addEvent(abc, def)) {
                            $(datacount.selector + '').eq(j).css('position', 'relative');
                            $(datacount.selector + '').eq(j).append('<a class="add_event"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                            k++;
                        }
                    }
                    return
                }
            }
        }
        if (($('.fc-view-container').find('.fc-basicDay-view').length != 0)) {
            if ($('.fc-content-skeleton table thead tr').find('.fc-day-top').length == 0) {
                var html = '<thead><tr><td class="fc-day-top fc-mon fc-past"><a class="fc-day-number"></a></td></tr></thead>';
                $('.fc-content-skeleton table').append(html);
                var datacount = $('.fc-content-skeleton table thead tr td');
                var daycount = $('.fc-content-skeleton table thead tr td').parent().parent().parent().parent().parent().children().find($('.fc-day'));
                k = 0;
                for (i = 0; i <= datacount.length; i++) {

                    for (j = k; j <= daycount.length - 1; j++) {
                        var abc = new Date(daycount[j].attributes[1].nodeValue);
                        var def = new Date();
                        if (addEvent(abc, def)) {
                            //if (Date.parse(abc.toLocaleDateString()) >= Date.parse(def.toLocaleDateString())) {
                            $(datacount.selector + '').eq(j).css('position', 'relative');
                            $(datacount.selector + '').eq(j).append('<a class="add_event" style="bottom:0;left:0;right:0;display: block;font-size:12px;color:#000;"><img src="/images/img/Todo.png" style="cursor:pointer;width: 25px;height: 25px;" data-toggle="tooltip" title="" data-original-title="Add Event" ></a>');
                            k++;
                        }
                    }
                    return
                }
            }
        }
    });

    $('#okbtn').click(function () {
        if (flag) {
            RefreshPage();
        }
    })

    var legendData = '<ul class="panel-controls smll" style="TOP:3PX; ">';
    legendData += '<li class="dropdown" data-tooltip="tooltip" data-placement="left" title="" data-original-title="info">';
    legendData += '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-info"></span></a>';
    legendData += '<ul class="dropdown-menu">';
    legendData += '<li class="infocls"><span class="smwt"><a title="Completed" href="javascript:void(0)" style="cursor:default;"><img src="/img/milestoneicon/Pending-20x20.png" width="17px" height="17px"></a></span><span class="fa fa-arrow-right"></span>In-Progress/Pending</li>';
    legendData += '<li class="infocls"><span class="smwt"><a title="Completed" href="javascript:void(0)" style="cursor:default;"><img src="/img/milestoneicon/Completed-20x20.png" width="17px" height="17px"></a></span><span class="fa fa-arrow-right"></span>Completed</li>';
    legendData += '<li class="infocls"><span class="smwt"><a title="Overdue" href="javascript:void(0)" style="cursor:default;"><img src="/img/milestoneicon/Overdue-20x20.png" width="17px" height="17px"></a></span><span class="fa fa-arrow-right"></span>Overdue</li>';
    legendData += '<li class="infocls"><span class="smwt"><a title="Due in 7 days" href="javascript:void(0)" style="cursor:default;"><img src="/img/milestoneicon/Due-20x20.png" width="17px" height="17px"></a></span><span class="fa fa-arrow-right"></span>Due in 7 days </li>';
    legendData += '</ul> </li> </ul> </div>';

    $(legendData).insertAfter(".fc-center");
})

function ValidateField() {

    return isValid;
}

function successmiletype(data) {
    var obj = data;

    statusoptions = '<option name = "statustypeoption" value="NULL">---Select---</option>';
    for (var i = 0; i < obj.length; i++) {

        statusoptions += '<option name = "statustypeoption" value="' +
            obj[i].StatusId +
            '">' +
            obj[i].StatusName +
            '</option>';
    }
    $("select#statustype").html(statusoptions);
    $("select#editstatustype").html(statusoptions);
}

function successcategory(data) {
    //var obj = data;

    $('#addcategorytype').html('');
    $('#ViewCategoryType').html('');
    $('#addcategorytype').append('<option value="0">' + "---Select Category---" + '</option>');
    $('#ViewCategoryType').append('<option value="0">' + "---Select Category---" + '</option>');

    $.each(data, function (i, value) {
        $('#addcategorytype')
            .append('<option class="option_color_pad" style="background-color:' + value.ColorCode
                    + '" value="' + value.CatID + '"><div></div>' + value.CategoryName + '</option>');

        $('#ViewCategoryType')
           .append('<option class="option_color_pad" style="background-color:' + value.ColorCode
                   + '" value="' + value.CatID + '"><div></div>' + value.CategoryName + '</option>');
    });

    $('.selectpicker').selectpicker('refresh');
    $('#addcategorytype').val(0);
}

function errormiletype(data) {
    return false;
}

function split(val) {
    return val.split(/,\s*/);
}

function ShowEventPopup(date, operation) {

    $.ajax({
        url: apiLink + 'GetCalendarCategoryList',
        type: 'GET',
        async: false,
        success: function (data) {
            successcategory(data.Data);
        },
        error: errormiletype
    });

    //$("#AddFollowUpCalendarModal").modal();

    if (operation == "ADDEVENT") {
        $('#TodoModal').modal('show');

        $('#startdate').datepicker('remove');
        $('#startdate').val('');
        $('#startdate').datepicker({
            startDate: date._i.toLocaleDateString(),
            autoclose: true,
            forceParse: false
        });
        
        var UTCDate = (date._d.getUTCMonth() + 1) + '/' + date._d.getUTCDate() + '/' + date._d.getUTCFullYear();

        $('#startdate').val(UTCDate);
        $('#startdate').datepicker("setDate", UTCDate);


        $('#enddate').datepicker('remove');
        $('#enddate').val('');
        $('#enddate').datepicker({
            startDate: $('#startdate').val(),
            autoclose: true,
            forceParse: false
        });

        $('#startdate').on("changeDate", function () {
            $('#enddate').datepicker('remove');
            $('#enddate').val('');
            $('#enddate').datepicker({
                startDate: $('#startdate').val(),
                autoclose: true
            });

        });
    }

}

function RefreshPage() {
    location.reload();
}


function GetDueDateAndStatusOfMilestone(ProjectNo, JobNo, MilestoneTitle) {

    var model = new Object();

    model.ProjectNo = ProjectNo;
    model.JobNo = JobNo;
    model.Milestone = MilestoneTitle;

    $.ajax({
        type: "POST",
        url: "/ManageProject/GetDueDateAndStatusOfMilestone/",
        data: model,
        async: false,
        success: function (data) {
            //$('#dashboardAllNewUser').text(data.usercount);
            //debugger
            if (data.length > 0) {
                var DueDate = data[0].DueDate.replace("12:00:00 AM", "");
                var CompletedDate = data[0].CompletedDate.replace("12:00:00 AM", "");
                $('#Editduedate').val(DueDate);
                $('#Editcompleteddate').val(CompletedDate);
                $('#editstatustype').val(data[0].StatusId);
                storedStatus = data[0].StatusId;
            }
        }

    })
}


/*

function viewReadyToPull(ProjectNo, JobNo) {

    $('#partialReadyToPull').load("/ManageProject/_ReadyToPullTemplate?ProjectNo=" + ProjectNo + "&JobNo=" + JobNo + "", function () {
        $(".icheckbox,.iradio").iCheck({ checkboxClass: 'icheckbox_minimal-grey', radioClass: 'iradio_minimal-grey' });
        if ($("input.fileinput").length > 0) {
            $("input.fileinput").bootstrapFileInput();
        }

        $('.onactive input').on('ifChanged', function (event) {
            var abc = event.target;
            $('#' + abc.id).parentsUntil('.onactive').find('.help-block').toggleClass('hide')
        });
        $('#modal_readypull').modal();

    });
}



*/