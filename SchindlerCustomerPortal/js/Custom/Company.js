﻿
BindStateForUser();

function CompanyDatatablePaging() {
    $('#CompanyDatatable_paginate').children('ul').addClass('flatpagi');
    $('#CompanyDatatable_previous').children('a').remove();
    $('#CompanyDatatable_previous').append('<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>');
    $('li.paginate_button.active').children('a').addClass('active');
    $('#CompanyDatatable_next').children('a').remove();
    $('#CompanyDatatable_next').append('<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>');
}

function AccountManagementTooltip() {
    $(".e-edit").tooltip({ title: 'Edit Account', placement: 'top' });
    $(".e-userLink").tooltip({ title: 'User Management', placement: 'top' });
}

function EditCompany(id) {
    window.location.href = '/ManageCompany/edit/?id=' + id;
}

$(document).ready(function () {

    $('#addPhone').mask('000-000-0000');
    $('#addinchargephone').mask('000-000-0000');
    $('#InchargePhone').mask('000-000-0000');
    $('#zipcode').mask('00000');
    $('#billingzipcode').mask('00000');

    BindheadofficeCountry();
    BindbillingofficeCountry();

    var hdnPageSize = parseInt($('#hdnPageSize').val());
    var userCompanyId = $('#hdnUserCompanyId').val();

    var table = $('#CompanyDatatable').DataTable({
       "info": false,  
        "lengthChange": false, "autoWidth": false,
        "ajax": {
            "url": "/ManageCompany/GetCompanyAll",
            "type": "Post",
            "datatype": "JSON"
        },

        'aaSorting': [[1, 'asc']],
        "lengthMenu": [[hdnPageSize, 10, 25, 50, -1], [hdnPageSize, 10, 25, 50, "All"]],
       
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "oLanguage": { "sSearch": "", "sProcessing": "<img src='/img/spinner.gif'> Loading.." },
        "bInfo": false,
        "columns": [
            { "data": "CompanyID", "name": "CompanyID", "Searchable": true, "sClass": "colHide txtAlignCenter" },
            {
                "data": "CompanyLogo",
                "name": "CompanyLogo",
                "render": function (data, type, row) {
                    return '<div class="companyLogoCenter"><img src="' + data + '" ></div>';
                }
            },
            {
                "data": "CompanyName",
                "name": "CompanyName",
                "Searchable": true,
            },
            { "data": "HeadOfficeAddress1", "name": "HeadOfficeAddress1", "Searchable": true },
            { "data": "PrimaryPhoneNumber", "name": "PrimaryPhoneNumber", "Searchable": true, "sClass": "txtAlignCenter" },
            { "data": "ACCTManagerName", "name": "ACCTManagerName", "Searchable": true, "sClass": "txtAlignCenter" },
            {
                "data": "CompanyID",
                "sClass":"txtAlignCenter",
                "render": function (data, type, row) {

                    var checkUserPermission = $('#hdnCheckUserManagementforAccountpage').val();

                    if (checkUserPermission) {
                        return '<a id="' + data + '" href="/ManageCompany/edit/?id=' + data + '" class="btnsml_action e-edit"  name="useredit" campid="1"  ></a>' +
                            '&nbsp;&nbsp;<a href="/ManageUser/Userlist/?companyId=' + data + '" class="btnsml_action e-userLink" name="companyUser" campid="1" ></a>'
                    }
                    else {
                        return '<a id="' + data + '" href="/ManageCompany/edit/?id=' + data + '" class="btnsml_action e-edit"  name="useredit" campid="1"  ></a>'
                    }
                }
            }

        ],
        createdRow: function (row, data, index) {
            $(row).addClass('table-click editData');
        },
        "initComplete": function (settings, json) {
            CompanyDatatablePaging();
            AccountManagementTooltip();
        },
        "drawCallback": function (settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        }
    });

    $('#CompanyDatatable').on('draw.dt', function () {
        CompanyDatatablePaging();
        AccountManagementTooltip();
    });

    $('.dataTables_length').hide();

    $('#CompanyDatatable_filter').hide();
     
    $('#CompanyDatatableSearch').unbind().keyup(function () {
        var value = $(this).val();
        if (userCompanyId == 0) {
            if (value.length > 1) {
                table.search(value).draw();
            }
            if (value == '') {
                table.search(value).draw();
            }
        }
    });


    function BindheadofficeCountry() {
        $.ajax({
            type: "GET",
            url: "/common/BindCountry",
            dataType: "json",
            success: function (data) {
                $('#headofficeCountry').html("");
                $('#headofficeCountry').append($("<option/>", ($({ value: '0', text: '--Select--' }))));
                $.each(data, function (i, val) {
                    $('#headofficeCountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));
                });
            },
            failure: function (data) {
                alert('oops something went wrong');
            }
        });

    }

    function BindbillingofficeCountry() {
        $.ajax({
            type: "GET",
            url: "/common/BindCountry",
            dataType: "json",
            success: function (data) {
                $('#billingofficeCountry').html("");
                $('#billingofficeCountry').append($("<option/>", ($({ value: '0', text: '--Select--' }))));
                $.each(data, function (i, val) {
                    $('#billingofficeCountry').append($("<option/>", ($({ value: val.CountryId, text: val.CountryName }))));
                });
            },
            failure: function (data) {
                alert('oops something went wrong');
            }
        });

    }

    $("#companyLogo").change(function (event) {

        event.preventDefault();
        var files = $("#companyLogo").get(0).files;
        if (files[0].type == "image/jpeg" || files[0].type == "image/png") {
            
            var image = new Image();
            image.src = window.URL.createObjectURL($('#companyLogo')[0].files[0]);
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height < 100 && width < 100) {
                   // alert("Height and Width must not exceed 100px.");
                    $('#CommonErrorMessage').html('Height and Width must not bellow 100px.');
                    $('#CommonErrorModal').modal('show');
                    return false;
                }
                else {
                    DisplayImage($('#companyLogo')[0].files, $('#addlogo').attr('id'));
                }
            }
        }
        else {
            //alert('Only jpg and png format');
            $('#CommonErrorMessage').html('Only jpg and png format.');
            $('#CommonErrorModal').modal('show');
        }

    });

    $("#headofficeCountry").on('change', function () {

        var countryid = $('#headofficeCountry').val();
        
        $.ajax({
            type: "GET",
            url: "/Common/BindState",
            dataType: "json",
            data: { countryid: countryid },
            success: function (citylist) {
                $('#headofficeState').html("");
                $('#headofficeState').append($("<option/>", ($({ value: '', text: '--State--' }))));
                $('#billingofficeState').append($("<option/>", ($({ value: '', text: '--State--' }))));
                $.each(citylist, function (i, val) {
                    $('#headofficeState').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));
                    $('#billingofficeState').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));
                });
            },
            failure: function (citylist) {
                alert('oops something went wrong');
            }
        });
    });

    $("#billingofficeCountry").on('change', function () {

        var countryid = $('#billingofficeCountry').val();

        $.ajax({
            type: "GET",
            url: "/Common/BindState",
            dataType: "json",
            data: { countryid: countryid },
            success: function (citylist) {
                $('#billingofficeState').html("");
                $('#billingofficeState').append($("<option/>", ($({ value: '', text: '--State--' }))));
                
                $.each(citylist, function (i, val) {
                    $('#billingofficeState').append($("<option/>", ($({ value: val.StateCode, text: val.StateName }))));
                });
            },
            failure: function (citylist) {
                alert('oops something went wrong');
            }
        });
    });

    $("#headofficeState").on('change', function () {

        var stateid = $('#headofficeState').val();
        var countryId = $('#headofficeCountry').val();
        $.ajax({
            type: "GET",
            url: "/Common/BindCity",
            dataType: "json",
            data: { countryId: countryId, stateCode: stateid },
            success: function (citylist) {
                $('#headofficeCity').html("");
                
                $('#headofficeCity').append($("<option/>", ($({ value: '', text: '--Select--' }))));
                $('#billingofficeCity').append($("<option/>", ($({ value: '', text: '--Select--' }))));
                $.each(citylist, function (i, val) {
                    $('#headofficeCity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
                    $('#billingofficeCity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
                });
            },
            failure: function (citylist) {
                alert('oops something went wrong');
            }
        });
    });

    $("#billingofficeState").on('change', function () {

        var stateid = $('#billingofficeState').val();
        var countryId = $('#billingofficeCountry').val();
        $.ajax({
            type: "GET",
            url: "/common/BindCity",
            dataType: "json",
            data: { countryId: countryId, stateCode: stateid },
            success: function (citylist) {
                $('#billingofficeCity').html("");
                $('#billingofficeCity').append($("<option/>", ($({ value: '0', text: '--Select--' }))));
                $.each(citylist, function (i, val) {
                    $('#billingofficeCity').append($("<option/>", ($({ value: val.CityId, text: val.CityName }))));
                });
            },
            failure: function (citylist) {
                alert('oops something went wrong');
            }
        });
    });

    $("#addNewCompany").click(function (e) {
        e.preventDefault();

        var isValid = true;

        if ($('#InchargePhone').val().trim() != null && $('#InchargePhone').val().trim() != undefined && $('#InchargePhone').val().trim() != '') {
            var phonelen = $('#InchargePhone').val().length;
            if (phonelen < 12) {
                $('#InchargePhone').css({
                    "border": "1px solid red"
                });
                $('#InchargePhone').focus();
                return false;
            }
        }


        var ziplen = $('#zipcode').val();
        if (ziplen.trim() != null && ziplen.trim() != undefined && ziplen.trim() != '') {
            if (ziplen.length < 5) {
                $('#zipcode').css({
                    "border": "1px solid red"

                });
                $('#zipcode').focus();
                return false;

            }
        }

        var bziplen = $('#billingzipcode').val();
        if (bziplen.trim() != null && bziplen.trim() != undefined && bziplen.trim() != '') {
            if (bziplen.length < 5) {
                $('#billingzipcode').css({
                    "border": "1px solid red"

                });
                $('#billingzipcode').focus();
                return false;

            }
        }



        $('input[class="addcompanyrequired form-control"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {

                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        var setfocus = "f";
        var ele = $('input[class="addcompanyrequired form-control"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }


        $('select[class="addcompanyrequired form-control"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {

                $(this).css({
                    "border": "",

                });
            }
        });

        if (setfocus == "f") {
            var ele = $('select[class="addcompanyrequired form-control"]');
            for (var i = ele.length; i > 0; i--) {
                if ($.trim($(ele[i - 1]).val()) == '') {
                    $(ele[i - 1]).focus();
                }
                else {
                }
            }
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var addinchargeemail = $("#Inchargeemail").val();
        var echeck = "";
        if (typeof addinchargeemail != 'undefined' && addinchargeemail != null && addinchargeemail.trim() != "") {
            if (!addinchargeemail.match(re)) {

                $("#Inchargeemail").css({
                    "border": "1px solid red"
                });
                $("#Inchargeemail").focus();
                echeck = "a";
            }

        }

        var propic = $('#companyLogo')[0].files.length;
        if (propic == 0) {
            $('#addlogo').css({
                "border": "1px solid red"

            });

            $('html, body').animate({
                scrollTop: $("#Divlogo").offset().top
            }, 2000);

            return false;
        }

        if (isValid) {

            var contact = $("#InchargePhone").val().replace('-', '').replace('-', '');

            var formData = new FormData();
            var files = $("#companyLogo").get(0).files;
            if (files[0] != null) {
                $('#fileName').val(files[0].name);
            }
            formData.append("img", files[0]);
            formData.append("CompanyName", $('#companyname').val().trim());
            formData.append("HeadOfficeAddress1", $('#address1').val().trim());
            formData.append("HeadOfficeAddress2", $('#address2').val().trim());

            formData.append("HeadOfficeCountryID", $('#headofficeCountry').val().trim());
            formData.append("HeadOfficeStateCode", $('#headofficeState').val().trim());

            formData.append("HeadOfficeCityCode", $('#headofficeCity').val().trim());
            formData.append("HeadOfficeZipCode", $('#zipcode').val().trim());
            formData.append("BillingAddress1", $('#billingA1').val().trim());
            formData.append("BillingAddress2", $('#billingA2').val().trim());

            formData.append("BillingCountryID", $('#billingofficeCountry').val().trim());
            formData.append("BillingStateCode", $('#billingofficeState').val().trim());

            formData.append("BillingCityCode", $('#billingofficeCity').val().trim());
            formData.append("BillingZipCode", $('#billingzipcode').val().trim());
            formData.append("InchargeFirstName", $('#IFirstName').val().trim());
            formData.append("InchargeLastName", $('#ILastName').val().trim());
            formData.append("InchargeDesignation", $('#Designation').val().trim());
            formData.append("InchargePhone", contact);
            formData.append("InchargeEmail", $('#Inchargeemail').val().trim());
            
            formData.append("ContactPersonFirstName", $('#ContactFirstName').val().trim());
            formData.append("ContactPersonLastName", $('#ContactLastName').val().trim());
            

            formData.append("CompanyLogo", $('#fileName').val().trim());

            $.ajax({
                url: '/ManageCompany/InsertCompany',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    
                    if (data.success) {

                        if (typeof data.isexist != undefined && data.isexist) {
                            //alert(data.message);
                            $('#CommonErrorMessage').html(data.message);
                            $('#CommonErrorModal').modal('show');
                            return;
                        }
                        else {
                            $('#addCompany').modal('hide');
                        }
                        alert(data.message);
                        var table = $('#CompanyDatatable').DataTable();
                        table.ajax.reload();
                    }
                    else {
                        alert(data.message);
                    }

                },
                error: function (data) {
                    alert(data.message);
                }
            })
        }

    });

    $("#companyname").blur(function (e) {
        $.ajax({
            type: "GET",
            url: "/common/IsCompanyNameExist",
            dataType: "json",
            data: { companyname: $(this).val() },
            success: function (data) {
                if (data.isexist) {
                    $("#companyname").css({
                        "border": "1px solid red"
                    });
                    alert(data.message);
                    return;
                }
                else {
                    $("#companyname").css({
                        "border": ""
                    });
                }
            },
            failure: function (data) {
                alert('oops something went wrong');
            }
        });
    });
});

var companyIdList = [];

$(document).on("click", ".table-click", function (e) {

    if ($(this).find("td").is(e.target)) {

        var targetClass = '.' + e.target.className;
        targetClass = targetClass.replace(" ", ".");
        var beforeColor = '';
        try {
            beforeColor = window.getComputedStyle(document.querySelector(targetClass), ':before').getPropertyValue('background-color');
        }
        catch (err) {

        }

        if ((beforeColor == "rgb(153, 153, 153)") || (beforeColor == "rgb(220, 0, 0)")) {

        }
        else {
            var clickedRow = $(this).find("td").parent('tr')[0];
            var CompanyId = $(clickedRow).find('td:not(:empty):first').text();
            EditCompany(CompanyId);
        }

    }
});

$('#chkAllCompany').change(function () {
    if ($(this).is(":checked")) {
        $('input[name="chkCompany"]').each(function () {
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var companyID = result[1];
            companyIdList.push(companyID);
            this.checked = true;
        });
    }
    else {
        $('input[name="chkCompany"]').each(function () {
            this.checked = false;
            var iddd = $(this).attr('id');
            var result = iddd.split('_');
            var companyID = result[1];

            companyIdList = jQuery.grep(companyIdList, function (value) {
                return value != companyID;
            });
        });
    }
});

$('#btnAllDelete').click(function (event) {
    event.preventDefault();

    var IsValiddd = true;
    if (companyIdList.length == 0) {
        IsValiddd = false;
    }

    var result = companyIdList.join(", ")

    if (IsValiddd) {
        $.ajax({
            type: "POST",
            url: "/ManageCompany/MultipleCompanyDelete",
            data: { companyID: result },
            success: function (userdetail) {
                $('#chkAllCompany').prop('checked', false);
                var table = $('#CompanyDatatable').DataTable();
                table.ajax.reload();
            }
        })
    }


})

$("#CompanyDatatable_paginate").on("click", "a", function () {
    $('#chkAllCompany').prop('checked', false);
    $('input[name="chkCompany"]').each(function () {
        this.checked = false;
        var iddd = $(this).attr('id');
        var result = iddd.split('_');
        var UserID = result[1];

        companyIdList = jQuery.grep(companyIdList, function (value) {
            return value != UserID;
        });
    });
});

$(document).on('change', 'input[name^="chkCompany"]', function (event) {
    
    if ($(this).is(":checked")) {
        
        var iddd = $(this).attr('id');
        var result = iddd.split('_');
        var companyID = result[1];
        companyIdList.push(companyID);
        this.checked = true;
        
    }
    else {
        
        this.checked = false;
        var iddd = $(this).attr('id');
        var result = iddd.split('_');
        var companyID = result[1];

        companyIdList = jQuery.grep(companyIdList, function (value) {
            return value != companyID;
        });
        
    }
})

//Check For Company Name Already Exists
function IsCompanyExist(companyname) {
    var result = false;
    $.ajax({
        type: "GET",
        url: "/common/IsCompanyNameExist",
        dataType: "json",
        data: { companyname: companyname },
        success: function (data) {
            if (data.isexist) {
                result = true;
            }
            else {
                result = false;
            }
        },
        failure: function (data) {
            alert('oops something went wrong');
        }
    });
    return result;
}

//Delete Company
$("#editdelete").click(function (e) {
    $('#deletesingle').modal('show');
});

$("#IsBillingAdd").click(function () {

    if ($(this).is(":checked")) {
        $('#billingA1').val($('#address1').val());
        $('#billingA2').val($('#address2').val());

        $('#billingofficeCountry').val($('#headofficeCountry').val());

        $('#billingofficeState').val($('#headofficeState').val());
        $('#billingofficeCity').val($('#headofficeCity').val());
        $('#billingzipcode').val($('#zipcode').val());
        $('#billingA1').attr('disabled', true)
        $('#billingA2').attr('disabled', true)
        $('#billingofficeState').attr('disabled', true)
        $('#billingofficeCity').attr('disabled', true)
        $('#billingzipcode').attr('disabled', true)
    } else {
        $('#billingA1').val("");
        $('#billingA2').val("");
        $('#billingofficeState').val("");
        $('#billingofficeCity').val("");
        $('#billingzipcode').val("");
        $('#billingofficeCity option[value="0"]').attr('selected', 'selected');
        $('#billingA1').attr('disabled', false)
        $('#billingA2').attr('disabled', false)
        $('#billingofficeState').attr('disabled', false)
        $('#billingofficeCity').attr('disabled', false)
        $('#billingzipcode').attr('disabled', false)
    }
    
});

$("#userdeletesingle").click(function () {

    $.ajax({
        url: '/managecompany/DeleteCompany',
        type: 'POST',
        data: { Id: $('#editcompanyid').val() },
        dataType: 'json',
        success: successDeletesingle,
        error: errorDeletesingle
    });

    function successDeletesingle(data) {
        if (data.success) { 
            var table = $('#CompanyDatatable').DataTable();
            table.ajax.reload();

            $('#deletesingle').modal('hide');
            $('#myDeleteModal').modal('show');

        }
        else {
            $('#deletesingle').modal('hide');
            $('#myInvalidModal').modal('show');
        }
    };
    function errorDeletesingle(data) {
        $('#deletesingle').modal('hide');
        $('#myInvalidModal').modal('show');
    };
});



