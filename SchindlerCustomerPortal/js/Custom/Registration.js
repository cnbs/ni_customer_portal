﻿function ValidateRegisteredEmail(email) {
    
        
    var regex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        if (regex.test(email)) {
            $('#Email').css({
                "border": ""
            });
            return true;
        }
        else {
            $('#Email').css({
                "border": "1px solid red"
            });
            return false;
        }

    };

$(document).ready(function () {

    //Added By Arpit Patel
    // State text Fetch for Lat long 
    $("#State").change(function () {
        $("#StateText").val($("#State option:selected").text());
    });

    //$('#recaptcha_response_field').addClass('RequiredFiled form-control');
   // $('#recaptcha_response_field').attr('placeholder','Type the two words:');
    function EditHeadState(stateid, countryId) { 
        $.ajax({
            type: "GET",
            url: "/common/BindState",
            dataType: "json",
            data: { countryId: countryId },
            success: function (response) {
                $('#State').html(""); 
                
                var options = '<option value="0">-- State--</option>';
                $.each(response, function (i, val) {
                 
                    if (i == 0)
                    {
                        $('#State').append($("<option/>", ($({ value: 0, text: '--State--' }))));
                    }
                    $('#State').append($("<option/>", ($({ value: val.StateID, text: val.StateName }))));
                });
            },
            failure: function (response) {
                bootbox.alert('oops something went wrong');
            }
        });
    }

    EditHeadState(0, 1);

    var USACountry = 1;
    var cityIdSelected = 0;

    var txtCityoptions = {
        source: function (request, response) {
            ;
            $.ajax({
                url: "/Common/BindCityAutocomplete/",
                data: '{ searchTerm:"' + $('#City').val() + '" , countryid:"' + USACountry + '" , stateCode:"' + $('#State').val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'Post',
                success: function (data) {
                    
                    response($.map(data, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            CityID: item.CityId,
                        }
                    }));
                }
            })
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            $("#City").val(ui.item.label);
            cityIdSelected = ui.item.CityID
            return false;
        },
        minLength: 2,
    };

    $(document).on("keydown.autocomplete", '#City', function (event) {
        $(this).autocomplete(txtCityoptions);
    })

    $('#ContactNumber').mask('000-000-0000');

    $('#PostalCode').mask('00000');

    $("#Email").tooltip({ title: 'Email Address, Follow this format Ex:abc@123.com', placement: 'top' });

    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    $('#Email').blur(function () {

        var email = $(this).val().trim();
        $.ajax({
            url: '/Login/CheckEmailExisting',
            type: 'POST',
            data: { EmailTo: email },
            dataType: 'json',
            success: successEmail,
            error: errorEmail
        });

        var Email = $("#Email").val();

        if (Email.trim() != null && Email != undefined && Email.trim() != "") {
            if (!Email.match(re)) {

                $("#Email").css({
                    "border": "1px solid red"
                });
                $("#Email").focus();
                isValid = false;
            }
            else {
                $("#Email").css({
                    "border": "",
                    "background": ""
                });

            }
        }
    });

    $('input[class="RequiredFiled form-control"]').blur(function () {

        var id = $(this).attr('id');

        if ($.trim($(this).val()) == '') {
            isValid = false;
            $(this).css({
                "border": "1px solid red"

            });

        }
        else {
            if (id == "Email") {
                if (!$.trim($(this).val()).match(re)) {

                    $("#Email").css({
                        "border": "1px solid red"

                    });

                }
                else {
                    $("#Email").css({
                        "border": ""

                    });
                }
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }

        }
    });

    //Code to submit form
    $('#save').click(function (e) {

        //$('#save').attr('disabled', 'disabled');

        var isValid = true;


        $('input[class="RequiredFiled form-control"]').each(function () {

            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red"
                });

            }
            else {

                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        var setfocus = "f";
        var ele = $('input[class="RequiredFiled form-control"]');
        for (var i = ele.length; i > 0; i--) {
            if ($.trim($(ele[i - 1]).val()) == '') {
                $(ele[i - 1]).focus();
                setfocus = "t";
            }
            else {
            }
        }

        var Email = $("#Email").val().trim();

        
        if (Email.trim() != null && Email != undefined && Email.trim() != "") {
            if (!Email.match(re)) {

                $("#Email").css({
                    "border": "1px solid red"
                });
                $("#Email").focus();
                isValid = false;
            }
            else {
                $("#Email").css({
                    "border": "",
                    "background": ""
                });

            }
        }
        
        //if (!ValidateRegisteredEmail(Email)) {
        //    isValid = false;
        //}


        //email address aleady exists or not
        
         $.ajax({
            url: '/Login/CheckEmailExisting',
            type: 'POST',
            data: { EmailTo: Email },
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.message == 'Exist') {
                    var er = "Account is already registered with " + $("#Email").val() + " email";
                    $("#Email").val('');
                    $("#Email").focus();

                    $('#notExist').text(er);
                    $('#EmailnotExist').modal('show');
                    isValid = false;
                }

                if (data.message == 'NotExist') {

                }
            },
            error: function (data) {

                $("#Email").val('');
                isValid = false;
            }
        });

       

        
        //email address aleady exists or not

        
        if (isValid == false) {
            //$('#save').attr('disabled', '');
            return false;
        }

        var phone = $("#ContactNumber").val().replace('-', '').replace('-', '');
        $('#ContactNumber').val(phone);
        //if ($('#InvalidCaptcha').val() != "") {
        //    $('#CommonErrorMessage').html($('#InvalidCaptcha').val());
        //    $('#CommonErrorModal').modal('show');
        //}
    });

    $('#userRegistration').submit(function () {
        $('#save').attr('disabled', 'disabled');
    });

    function successEmail(data) {
       
        if (data.message == 'Exist') {
            var er = "Account is already registered with " + $("#Email").val() + " email";
            $("#Email").val('');
            $("#Email").focus();

            $('#notExist').text(er);
            $('#EmailnotExist').modal('show');
            
        }

        if (data.message == 'NotExist') {

        }
    };

    function errorEmail(data) {
        $("#Email").val('');
        return false;
    };

    $('#NotExistOK').click(function () {
        $('#Email').focus();
    });

    var Redirect = $('#InvalidLoginID').val();
    if (Redirect == 'true') {
        $('#CommonSuccessMessage').text('Your registration information for myschindlerprojects account has been saved successfully. You will receive verification email from myschindlerprojects. Please verify your email for Portal Administrator to approve your registration request.');
        $('#CommonSuccessModal').modal('show');
    }

    $('#btnRegisterOk').click(function () {
        window.location.href = '/Login/UserLogin';
    })
    $('#FirstName').keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    
});