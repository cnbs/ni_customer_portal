﻿/* Create file on the fly... *//**
 * Terrific JavaScript Framework v1.0.0
 * http://terrifically.org
 *
 * Copyright 2011, Remo Brunschwiler
 * MIT Licensed.
 *
 * Date: Fri, 13 May 2011 07:06:59 GMT
 *
 *
 * Includes:
 * Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 *
 * @module Nx
 *
 *
 *
 *
 * Compress it with:
 * http://www.refresh-sf.com/yui/
 *
 *
 *
 */
var Nx = Nx || {};

/*
 * The jQuery object.
 */
Nx.$ = jQuery.noConflict(false);

/**
 * Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 *
 */
(function(){
    var initializing = false, fnTest = /xyz/.test(function() { xyz; }) ? /\b_super\b/ : /.*/;

    // The base Class implementation (does nothing)
    this.Class = function(){
    };

    // Create a new Class that inherits from this class
    Class.extend = function(prop){
        var _super = this.prototype;

        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;

        // Copy the properties over onto the new prototype
        for (var name in prop) {
            // Check if we're overwriting an existing function
            prototype[name] = typeof prop[name] == "function" &&
            typeof _super[name] == "function" &&
            fnTest.test(prop[name]) ? (function(name, fn){
                return function(){
                    var tmp = this._super;

                    // Add a new ._super() method that is the same method
                    // but on the super-class
                    this._super = _super[name];

                    // The method only need to be bound temporarily, so we
                    // remove it when we're done executing
                    var ret = fn.apply(this, arguments);
                    this._super = tmp;

                    return ret;
                };
            })(name, prop[name]) : prop[name];
        }

        // The dummy class constructor
        function Class(){
            // All construction is actually done in the init method
            if (!initializing && this.init) {
                this.init.apply(this, arguments);
            }
        }

        // Populate our constructed prototype object
        Class.prototype = prototype;

        // Enforce the constructor to be what we expect
        Class.constructor = Class;

        // And make this class extendable
        Class.extend = arguments.callee;

        return Class;
    };
})();

/**
 * Contains the application base config.
 * The base config can be extended or overwritten either via new Application ($ctx, config)
 * during bootstrapping the application or via /public/js/Nx.Config.js in the project folder.
 *
 * @author Remo Brunschwiler
 * @namespace Nx
 * @class Config
 * @static
 */
Nx.Config = {
    /**
     * The paths for the different dependency types.
     *
     * @property dependencyPath
     * @type Object
     */
    dependencyPath: {
        library: '/js/libraries/dynamic/',
        plugin: '/js/plugins/dynamic/',
        util: '/js/utils/dynamic/'
    }
};

(function($) {
    /**
     * Responsible for application wide issues (ie. the creation of modules).
     *
     * @author Remo Brunschwiler
     * @namespace Nx
     * @class Application
     */
    Nx.Application = Class.extend({

        /**
         * Initializes the Application.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Object} config the configuration
         */
        init: function($ctx, config) {
            /**
             * The configuration.
             *
             * @property config
             * @type Object
             */
            this.config = $.extend(Nx.Config, config);

            /**
             * The jquery context.
             *
             * @property $ctx
             * @type jQuery
             */
            this.$ctx = $ctx || $('body');

            /**
             * Contains references to all modules on the page.
             * -> could be useful ie. when there are interactions between flash <-> js.
             *
             * @property modules
             * @type Array
             */
            this.modules = [];

            /**
             * Contains references to all connectors on the page.
             *
             * @property connectors
             * @type Object
             */
            this.connectors = {};

            /**
             * Contains references to all wilcard components on the page.
             *
             * @property wildcardComponents
             * @type Array
             */
            this.wildcardComponents = [];

            /**
             * The sandbox to get the resources from (shared between all modules).
             *
             * @property sandbox
             * @type Sandbox
             */
            this.sandbox = new Nx.Sandbox(this, this.config);
        },

        /**
         * Registers all modules in the context scope automatically (as long as the modules uses the oocss naming conventions).
         *
         * @method registerModules
         * @param {jQuery} $ctx the jquery context.
         * @return {Array} a list containing the references of the registered modules.
         */
        registerModules : function($ctx) {
            var that = this,
                modules = [];

            $ctx = $ctx || this.$ctx;

            $('.mod', $ctx).each(function() {
                var $this = $(this);

                /*
                 * a module can have 3 types of classes:
                 * 1. .mod -> indicates that it is a base module (default -> no javascript need to be involved)
                 * 2. .mod<moduleName> (ie. .modBasic) -> indicates that it is a module from the type basic (derived from the base module)
                 * 3. .skin<moduleName><skinName> (ie. .skinBasicSubmarine) -> indicates that the module basic has the submarine skin. it will be decorated by the skin js (if existing).
                 *
                 * type 1 must occur exactly once
                 * type 2 can occur at most once
                 * type 3 can occur arbitrarily
                 *
                 * additionaly a module can have 1 type of data attributes:
                 * 1. data-connectors -> a comma separated value containing the connector ids -> schema of a connector id: <connectorName><connectorId><connectorRole>
                 *    (ie. MasterSlave1Master -> name = MasterSlave, id = 1, role = Master)
                 * 	-> indicates that the module should notify the MasterSlave connector (mediator) over all state changes
                 * 	-> the connector id is used to chain the appropriate modules together and to improve the reusability of the connector
                 *
                 * type 1 can contain multiple connector ids (ie. 1,2,MasterSlave1Master)
                 */

                var classes = $this.attr('class').split(' ');

                if (classes.length > 1) {
                    var modName,
                        skins = [],
                        connectors = [];

                    for (var i = 0, len = classes.length; i < len; i++) {
                        var part = $.trim(classes[i]);

                        if (part.indexOf('mod') === 0 && part.length > 3) {
                            modName = part.substr(3);
                        }
                        else if (part.indexOf('skin') === 0) {
                            // remove the mod name part from the skin name
                            skins.push(part.substr(4).replace(modName, ''));
                        }
                    }


                    if ($this.attr('data-connectors')) {
                        connectors = $this.attr('data-connectors').split(',');
                        for (var i = 0, len = connectors.length; i < len; i++) {
                            connectors[i] = $.trim(connectors[i]);
                        }
                    }


                    if (modName && Nx.Module[modName]) {
                        modules.push(that.registerModule($this, modName, skins, connectors));
                    }
                }
            });

            return modules;
        },

        /**
         * Unregisters the modules by the given module instances.
         *
         * @method unregisterModule
         * @param {Array} modules a list containting the module instances to unregister.
         * @return {void}
         */
        unregisterModules : function(modules) {
            var connectors = this.connectors,
                wildcardComponents = this.wildcardComponents;

            modules = modules || this.modules;

            if (modules === this.modules) {
                // empy everything if the arrays are equal
                this.wildcardComponents = [];
                this.connectors = [];
                this.modules = [];
            }
            else {
                // unregister the given modules
                for (var i = 0, len = modules.length; i < len; i++) {
                    var module = modules[i];
                    var index;

                    // delete the references in the connectors
                    for (var connId in connectors) {
                        connectors[connId].unregisterComponent(module);
                    }

                    // delete the references in the wildcard components
                    index = $.inArray(module, wildcardComponents);
                    delete wildcardComponents[index];

                    // delete the module instance itself
                    index = $.inArray(module, this.modules);
                    delete this.modules[index];
                }
            }
        },

        /**
         * Starts (intializes) the registered modules.
         *
         * @method start
         * @param {Array} modules a list of the modules to start.
         * @return {void}
         */
        start: function(modules) {
            var wildcardComponents = this.wildcardComponents,
                connectors = this.connectors;

            modules = modules || this.modules;

            // start the modules
            for (var i = 0, len = modules.length; i < len; i++) {
                modules[i].start();
            }

            /*
             * special treatment for the wildcard connection (conn*) -> it will be notified about
             * all state changes from all connections and is able to propagate its changes to all modules.
             * this must be done on init to make sure that all connectors on the page has been instantiated.
             * only do this for the given modules.
             */
            for (var i = 0, len = wildcardComponents.length; i < len; i++) {
                var component = wildcardComponents[i];
                if ($.inArray(component, modules) > -1) {
                    for (var connectorId in connectors) {
                        // the connector observes the component -> attach it as observer
                        component.attachConnector(connectors[connectorId]);
                        connectors[connectorId].registerComponent(component, '*');
                    }
                }
            }
        },

        /**
         * Stops the registered modules.
         *
         * @method stop
         * @param {Array} modules a list containting the module instances to stop.
         * @return {void}
         */
        stop: function(modules) {
            modules = modules || this.modules;

            // stop the modules
            for (var i = 0, len = modules.length; i < len; i++) {
                modules[i].stop();
            }
        },

        /**
         * Registers a module.
         *
         * @method registerModule
         * @param {jQuery} $node the module node.
         * @param {String} modName the module name. it must match the class name of the module (case sensitive).
         * @param {Array} skins a list of skin names. each entry must match a class name of a skin (case sensitive)).
         * @param {Array} connectors a list of connectors identifiers. schema: <connectorName><connectorId><connectorRole> (ie. MasterSlave1Master).
         * @return {Module} the reference to the registered module.
         */
        registerModule : function($node, modName, skins, connectors) {
            var modules = this.modules;

            modName = modName || null;
            skins = skins || [];
            connectors = connectors || [];

            if (modName && Nx.Module[modName]) {
                // generate a unique id for every module
                var modId = modules.length;
                $node.data('id', modId);

                modules[modId] = new Nx.Module[modName]($node, this.sandbox, modId);

                for (var i = 0, len = skins.length; i < len; i++) {
                    var skinName = skins[i];

                    if (Nx.Module[modName][skinName]) {
                        modules[modId] = modules[modId].getDecoratedModule(modName, skinName);
                    }
                }

                for (var i = 0, len = connectors.length; i < len; i++) {
                    this.registerConnection(connectors[i], modules[modId]);
                }

                return modules[modId];
            }

            return null;
        },

        /**
         * Registers a connection between a module and a connector.
         *
         * @method registerConnection
         * @param {String} connector the full connector name (ie. MasterSlave1Slave).
         * @param {Module} component the module instance.
         * @return {void}
         */
        registerConnection : function(connector, component) {
            var connectorType = connector.replace(/[0-9]+[a-zA-Z]*$/, ''),
                connectorId = connector.replace(/[a-zA-Z]*$/, '').replace(/^[a-zA-Z]*/, ''),
                connectorRole = connector.replace(/^[a-zA-Z]*[0-9]*/, '');

            if (connectorId === '*' && connectorRole === '*') {
                // add the component to the wildcard component stack
                this.wildcardComponents.push(component);
            }
            else {
                var connectors = this.connectors;

                if (!connectors[connectorId]) {
                    // instantiate the appropriate connector if it does not exist yet
                    if (connectorType === '') {
                        connectors[connectorId] = new Nx.Connector(connectorId);
                    }
                    else if (Nx.Connector[connectorType]) {
                        connectors[connectorId] = new Nx.Connector[connectorType](connectorId);
                    }
                }

                if (connectors[connectorId]) {
                    // the connector observes the component -> attach it as observer
                    component.attachConnector(connectors[connectorId]);

                    // the component wants to be informed over state changes -> register it as connector member
                    connectors[connectorId].registerComponent(component, connectorRole);
                }
            }
        }
    });
})(Nx.$);

(function($) {
    /**
     * The sandbox is used as a central point to get resources from / grant permissions etc.
     * It is shared between all modules.
     *
     * @author Remo Brunschwiler
     * @namespace Nx
     * @class Sandbox
     */
    Nx.Sandbox = Class.extend({

        /**
         * Initializes the Sandbox.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {Applicaton} application the application reference
         * @param {Object} config the configuration
         */
        init : function(application, config) {

            /**
             * The application
             *
             * @property application
             * @type Application
             */
            this.application = application;

            /**
             * The configuration.
             *
             * @property config
             * @type Object
             */
            this.config = config;

            /**
             * Contains the requested javascript dependencies.
             *
             * @property dependencies
             * @type Array
             */
            this.dependencies = [];

            /**
             * Contains the afterBinding module callbacks.
             *
             * @property afterBindingCallbacks
             * @type Array
             */
            this.afterBindingCallbacks = [];


            /**
             * Contains the first script node on the page.
             *
             * @property firstScript
             * @type Node
             */
            this.firstScript = $('script').get(0);
        },

        /**
         * Adds (register and start) all modules in the given context scope.
         *
         * @method addModules
         * @param {jQuery} $ctx the jquery context.
         * @return {Array} a list containing the references of the registered modules.
         */
        addModules: function($ctx) {
            var modules = [],
                application = this.application;

            if ($ctx) {
                // register modules
                modules = application.registerModules($ctx);

                // start modules
                application.start(modules);
            }

            return modules;
        },

        /**
         * Removes (stop and unregister) the modules by the given module instances.
         *
         * @method removeModules
         * @param {Array} modules a list containting the module instances to remove.
         * @return {void}
         */
        removeModules: function(modules) {
            var application = this.application;

            if (modules) {
                // stop modules
                application.stop(modules);

                // unregister modules
                application.unregisterModules(modules);
            }
        },

        /**
         * Gets the appropriate module for the given id.
         *
         * @method getModuleById
         * @param {int} id the module id
         * @return {Module} the appropriate module
         */
        getModuleById: function(id) {
            var application = this.application;

            if (application.modules[id] !== undefined) {
                return application.modules[id];
            }
            else {
                throw new Error('the module with the id ' + id + ' does not exist');
            }
        },

        /**
         * Gets the application config.
         *
         * @method getConfig
         * @return {Object} the configuration object
         */
        getConfig: function() {
            return this.config;
        },

        /**
         * Gets an application config param.
         *
         * @method getConfigParam
         * @param {String} name the param name
         * @return {mixed} the appropriate configuration param
         */
        getConfigParam: function(name) {
            var config = this.config;

            if (config.name !== undefined) {
                return config.name;
            }
            else {
                throw new Error('the config param ' + name + ' does not exist');
            }
        },

        /**
         * Loads a requested dependency (if not already loaded).
         *
         * @method loadDependency
         * @param {String} dependency the dependency (i.e. swfobject.js)
         * @param {String} type the dependency type (plugin | library | util | url)
         * @param {Function} callback the callback to execute after the dependency has successfully loaded
         * @param {String} phase the module phase where the dependency is needed (ie. beforeBinding, onBinding)
         * @return {void}
         */
        loadDependency: function(dependency, type, callback, phase) {
            var that = this;

            phase = phase || 'none'; // none indicates that it is not a dependency for a specific phase
            type = type || 'plugin';

            if (that.dependencies[dependency] && that.dependencies[dependency].state === 'requested') { // requested (but loading not finished)
                // the module should be notified, if the dependency has loaded
                that.dependencies[dependency].callbacks.push(function() {
                    callback(phase);
                });
            }
            else if (that.dependencies[dependency] && that.dependencies[dependency].state === 'loaded') { // loading finished
                callback(phase);
            }
            else {
                that.dependencies[dependency] = {
                    state: 'requested',
                    callbacks: []
                };

                var path;

                switch (type) {
                    case 'library':
                    case 'plugin':
                    case 'util':
                        path = this.config.dependencyPath[type];
                        break;
                    case 'url':
                        path = '';
                        break;
                    case 'default':
                        path = '';
                        break;
                }

                // load the appropriate dependency
                var script = document.createElement('script'),
                    firstScript = this.firstScript;

                script.src = path + dependency;

                script.onreadystatechange = script.onload = function () {
                    var readyState = script.readyState;
                    if (!readyState || readyState == 'loaded' || readyState == 'complete') {
                        that.dependencies[dependency].state = 'loaded';
                        callback(phase);

                        // notify the other modules with this dependency
                        var callbacks = that.dependencies[dependency].callbacks;
                        for (var i = 0, len = callbacks.length; i < len; i++) {
                            callbacks[i]();
                        }

                        // Handle memory leak in IE
                        script.onload = script.onreadystatechange = null;
                    }
                };

                firstScript.parentNode.insertBefore(script, firstScript);
            }
        },

        /**
         * Collects the module status messages (ready for after binding) and handles the callbacks.
         *
         * @method readyForAfterBinding
         * @param {Function} callback the afterBinding module callback
         * @return {void}
         */
        readyForAfterBinding: function(callback) {
            var afterBindingCallbacks = this.afterBindingCallbacks;

            // add the callback to the stack
            afterBindingCallbacks.push(callback);

            // check whether all modules are ready for the after binding phase
            if (this.application.modules.length == afterBindingCallbacks.length) {
                for (var i = 0; i < afterBindingCallbacks.length; i++) {
                    afterBindingCallbacks[i]();
                }
            }
        }
    });
})(Nx.$);

(function($) {
    /**
     * Base class for the different modules.
     *
     * @author Remo Brunschwiler
     * @namespace Nx
     * @class Module
     */
    Nx.Module = Class.extend({

        /**
         * Initializes the Module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            /**
             * Contains the module context.
             *
             * @property $ctx
             * @type jQuery
             */
            this.$ctx = $ctx;

            /**
             * Contains the unique module id.
             *
             * @property modId
             * @type String
             */
            this.modId = modId;

            /**
             * Contains the attached connectors.
             *
             * @property connectors
             * @type Array
             */
            this.connectors = [];

            /**
             * Contains the dependency counter for the different phases.
             *
             * @property dependencyCounter
             * @type Object
             */
            this.dependencyCounter = {
                beforeBinding: 0,
                onBinding: 1, // not 0, because of the beforeBinding callback (which is also a dependency)
                afterBinding: 1 // not 0, because a completed onBinding phase is required (which is also a dependency)
            };

            /**
             * The sandbox to get the resources from.
             *
             * @property sandbox
             * @type Sandbox
             */
            this.sandbox = sandbox;
        },

        /**
         * Template method to start (init) the module.
         * This method provides some hook functions which could be overridden from the concrete implementation
         *
         * @method start
         * @return {void}
         */
        start: function() {
            // call the hook method dependecies from the concrete implementation
            if (this.dependencies) {
                this.dependencies();
            }

            this.initBeforeBinding();
        },

        /**
         * Template method to stop the module.
         *
         * @method stop
         * @return {void}
         */
        stop: function() {
            var $ctx = this.$ctx;

            // remove all bound events and associated jquery data
            $('*', $ctx).unbind().removeData();
            $ctx.unbind().removeData();
        },

        /**
         * Initializes the before binding phase.
         *
         * @method initBeforeBinding
         * @return {void}
         */
        initBeforeBinding: function() {
            var that = this;

            // start the before binding phase if there are no dependency for this phase
            this.checkDependencies('beforeBinding', function() {
                // call the hook method beforeBinding from the concrete implementation
                // because there might be some ajax calls, the bindEvents method must be called from
                // the beforeBinding function after it has been run
                if (that.beforeBinding) {
                    that.beforeBinding(function() {
                        that.beforeBindingCallback();
                    });
                }
                else {
                    that.beforeBindingCallback();
                }
            });
        },

        /**
         * Callback for the before binding phase.
         *
         * @method beforeBindingCallback
         * @return {void}
         */
        beforeBindingCallback: function() {
            // decrement the dependency counter for the on binding phase
            this.dependencyCounter.onBinding--;
            this.initOnBinding();
        },

        /**
         * Initializes the on binding phase.
         *
         * @method initOnBinding
         * @return {void}
         */
        initOnBinding: function() {
            var that = this;

            // start the on binding phase if there are no dependencies for this phase
            this.checkDependencies('onBinding',function() {
                // call the hook method bindEvents from the concrete implementation
                if (that.onBinding) {
                    that.onBinding();
                }

                // decrement the dependency counter for the after binding phase
                that.dependencyCounter.afterBinding--;
                that.initAfterBinding();
            });
        },

        /**
         * Initializes the after binding phase.
         *
         * @method initAfterBinding
         * @return {void}
         */
        initAfterBinding: function() {
            var that = this;

            // start the after binding phase if there are no dependencies for this phase
            this.checkDependencies('afterBinding', function() {
                // inform the sandbox that the module is ready for the after binding phase
                that.sandbox.readyForAfterBinding(function() {

                    // call the hook method afterBinding from the concrete implementation
                    if (that.afterBinding) {
                        that.afterBinding();
                    }
                });
            });
        },

        /**
         * Checks the dependency load state of the given phase.
         * Initializes the appropriate phase if all dependencies are loaded.
         *
         * @method checkDependencies
         * @param {String} phase the phase to check / initialize
         * @param {Function} callback the callback to execute if all dependencies were loaded
         * @return {void}
         */
        checkDependencies: function(phase, callback) {
            if (this.dependencyCounter[phase] === 0) {
                // execute the callback
                callback();
            }
        },

        /**
         * Manages the required dependencies.
         *
         * @method require
         * @param {String} dependency the dependency (i.e. swfobject.js)
         * @param {String} type the dependency type (library | plugin | util | url)
         * @param {String} phase the module phase where the dependency is needed (ie. beforeBinding, onBinding)
         * @param {boolean} executeCallback indicates whether the phase callback should be executed or not (useful for dependencies that provide their own callback mechanism)
         * @return {void}
         */
        require: function(dependency, type, phase, executeCallback) {
            type = type || 'plugin';
            phase = phase || 'onBinding';
            executeCallback = executeCallback === false ? false : true;

            // increment the dependency counter
            this.dependencyCounter[phase]++;

            // proxy the callback to the outermost decorator
            var callback = $.proxy(function() {
                if (executeCallback) {
                    var that = this;

                    // decrement the dependency counter for the appropriate phase
                    this.dependencyCounter[phase]--;
                    that['init' + Nx.Utils.String.capitalize(phase)]();
                }
            }, this.sandbox.getModuleById(this.modId));

            this.sandbox.loadDependency(dependency, type, callback, phase);
        },

        /**
         * Notifies all attached connectors about changes.
         *
         * @method fire
         * @param {String} state the new state
         * @param {Object} data the data to provide to your connected modules
         * @param {Function} defaultAction the default action to perform
         * @return {void}
         */
        fire: function(state, data, defaultAction) {
            var that = this,
                connectors = this.connectors;

            data = data ||{};
            state = Nx.Utils.String.capitalize(state);

            $.each(connectors, function() {
                var connector = this;

                // callback combining the defaultAction and the afterAction
                var callback = function() {
                    if (typeof defaultAction == 'function') {
                        defaultAction();
                    }
                    connector.notify(that, 'after' + state, data);
                };

                if (connector.notify(that, 'on' + state, data, callback)) {
                    callback();
                }
            });

            if (connectors.length < 1) {
                if (typeof defaultAction == 'function') {
                    defaultAction();
                }
            }
        },

        /**
         * Attaches a connector (observer).
         *
         * @method attachConnector
         * @param {Connector} connector the connector to attach
         * @return {void}
         */
        attachConnector: function(connector) {
            this.connectors.push(connector);
        },

        /**
         * Decorates itself with the given skin.
         *
         * @method getDecoratedModule
         * @param {String} module the name of the module
         * @param {String} skin the name of the skin
         * @return {Module} the decorated module
         */
        getDecoratedModule: function(module, skin) {
            if (Nx.Module[module][skin]) {
                var decorator = Nx.Module[module][skin];

                /*
                 * Sets the prototype object to the module.
                 * So the "non-decorated" functions will be called on the module (without implementing the whole module interface).
                 */
                decorator.prototype = this;
                decorator.prototype.constructor = Nx.Module[module][skin];

                return new decorator(this);
            }

            return null;
        }
    });
})(Nx.$);

(function($) {
    /**
     * Base class for the different connectors.
     *
     * @author Remo Brunschwiler
     * @namespace Nx
     * @class Connector
     */
    Nx.Connector = Class.extend({

        /**
         * Initializes the Connector.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {String} connectorId the unique connector id
         * @param {Object} connectorId
         */
        init : function(connectorId) {
            this.connectorId = connectorId;
            this.components = [];
        },

        /**
         * Registers a component.
         *
         * @method registerComponent
         * @param {Module} component the module to register
         * @param {String} role the role of the module (ie. master, slave etc.)
         * @return {void}
         */
        registerComponent: function(component, role) {
            role = role || 'standard';

            this.components.push({
                'component': component,
                'role': role
            });
        },

        /**
         * Unregisters a component.
         *
         * @method unregisterComponent
         * @param {Module} component the module to unregister
         * @return {void}
         */
        unregisterComponent: function(component) {
            var components = this.components;

            for (var id in components) {
                if (components[id].component === component) {
                    delete components[id];
                }
            }
        },

        /**
         * Notifies all registered components about the state change (to be overriden in the specific connectors).
         *
         * @method notify
         * @param {Module} component the module that sends the state change
         * @param {String} state the state
         * @param {Object} data contains the state relevant data (if any)
         * @param {Function} callback the callback function (could be executed after an asynchronous action)
         * @return {boolean} indicates whether the default action should be excuted or not
         */
        notify: function(component, state, data, callback) {
            /*
             * gives the components the ability to prevent the default- and afteraction from the events
             * (by returning false in the on<Event>-Handler)
             */
            var proceed = true,
                components = this.components;

            for (var id in components) {
                if (components[id].component !== component && components[id].component[state]) {
                    if (components[id].component[state](data, callback) === false) {
                        proceed = false;
                    }
                }
            }

            return proceed;
        }
    });
})(Nx.$);

/*
 * Contains utility functions for several tasks.
 */
Nx.Utils = {};

/**
 * Contains utility functions for string concerning tasks.
 *
 * @author Remo Brunschwiler
 * @namespace Nx.Utils
 * @class String
 * @static
 */
(function($) {
    Nx.Utils.String = {
        /**
         * Capitalizes the first letter of the given string.
         *
         * @method capitalize
         * @param {String} str the original string
         * @return {String} the capitalized string
         */
        capitalize: function(str) {
            // capitalize the first letter
            return str.substr(0, 1).toUpperCase().concat(str.substr(1));
        }
    };
})(Nx.$);

/**
 * Blackbird - Open Source JavaScript Logging Utility
 * Author: G Scott Olson
 * Web: http://blackbirdjs.googlecode.com/
 *      http://www.gscottolson.com/blackbirdjs/
 * Version: 1.0
 *
 * The MIT License - Copyright (c) 2008 Blackbird Project
 *
 *
 *
 * @description modification
 * modified as jquery plugin - ernscht
 * call this file at http://lab2.namics.com/frontend/tools/js/debug/jquery.log/jquery.log.js
 * it is not nessessary to include additional files as stylesheets, pictures, ...
 * for productive pages use empty methods instead http://lab2.namics.com/frontend/tools/js/debug/jquery.log/jquery.log.production.js
 * the original blackbird uses now .profile() as timer. not changed in $.log();
 *
 * supported browser: IE6, IE7, FF2, FF3, ...
 * requires: jQuery >= 1.2.6
 * tested with 1.2.6, 1.3.0, 1.3.1, 1.3.2, 1.3.4, 1.5, 1.5.1. 1.5.2, 1.6, 1.6.1
 *
 * @example
 * 	$.log.debug('string');		// log a string as debug message
 * 	$.log.info('string');		// log a string as info
 * 	$.log.warn('string');		// log a string as warn
 * 	$.log.error('string');		// log a string as error
 * 	$.log.time('string'); 		// call two times the method .time()
 * 								// with the same string as param
 * 								// to start and stop the timer
 *
 *
 * -> copy from http://lab2.namics.com/frontend/tools/js/debug/jquery.log/jquery.log.js
 * -> removed CSS style loading
 *
 * @author namics (ernscht@namics.com)
 * @version 1.0
 *
 */
;(function($) {

    var NAMESPACE = 'log';
    var IE6_POSITION_FIXED = true; // enable IE6 {position:fixed}

    var bbird;
    var outputList;
    var cache = [];

    var state = getState();
    var classes = {};
    var profiler = {};

    var IDs = {
        blackbird: 'blackbird',
        checkbox: 'bbVis',
        filters: 'bbFilters',
        controls: 'bbControls',
        size: 'bbSize'
    };
    var messageTypes = { //order of these properties imply render order of filter controls
        debug: true,
        info: true,
        warn: true,
        error: true,
        time: true
    };


    function generateMarkup() { //build markup
        var spans = [];
        for ( var type in messageTypes ) {
            spans.push( [ '<span class="', type, '" type="', type, '"></span>'].join( '' ) );
        }

        var newNode = document.createElement( 'DIV' );
        newNode.id = IDs.blackbird;
        newNode.style.display = 'none';
        newNode.innerHTML = [
            '<div class="bbheader">',
            '<div class="bbleft">',
            '<div id="', IDs.filters, '" class="filters" title="click to filter by message type">', spans.join( '' ), '</div>',
            '</div>',
            '<div class="bbright">',
            '<div id="', IDs.controls, '" class="controls">',
            '<span id="', IDs.size ,'" title="contract" op="resize"></span>',
            '<span class="clear" title="clear" op="clear"></span>',
            '<span class="close" title="close" op="close"></span>',
            '</div>',
            '</div>',
            '</div>',
            '<div class="bbmain">',
            '<div class="bbleft"></div><div class="bbmainBody">',
            '<ol>', cache.join( '' ), '</ol>',
            '</div><div class="bbright"></div>',
            '</div>',
            '<div class="bbfooter">',
            '<div class="bbleft"><label for="', IDs.checkbox, '"><input type="checkbox" id="', IDs.checkbox, '" />Visible on page load</label></div>',
            '<div class="bbright"></div>',
            '</div>'
        ].join( '' );
        return newNode;
    }

    function backgroundImage() { //(IE6 only) change <BODY> tag's background to resolve {position:fixed} support
        var bodyTag = document.getElementsByTagName( 'BODY' )[ 0 ];

        if ( bodyTag.currentStyle && IE6_POSITION_FIXED ) {
            if (bodyTag.currentStyle.backgroundImage == 'none' ) {
                bodyTag.style.backgroundImage = 'url(about:blank)';
            }
            if (bodyTag.currentStyle.backgroundAttachment == 'scroll' ) {
                bodyTag.style.backgroundAttachment = 'fixed';
            }
        }
    }

    function addMessage( type, content ) { //adds a message to the output list
        content = ( content.constructor == Array ) ? content.join( '' ) : content;
        if ( outputList ) {
            var newMsg = document.createElement( 'LI' );
            newMsg.className = type;
            newMsg.innerHTML = [ '<span class="icon"></span>', content ].join( '' );
            outputList.appendChild( newMsg );
            scrollToBottom();
        } else {
            cache.push( [ '<li class="', type, '"><span class="icon"></span>', content, '</li>' ].join( '' ) );
        }
    }

    function clear() { //clear list output
        outputList.innerHTML = '';
    }

    function clickControl( evt ) {
        if (!evt) {
            evt = window.event;
        }
        var el = ( evt.target ) ? evt.target : evt.srcElement;

        if ( el.tagName == 'SPAN' ) {
            switch ( el.getAttributeNode( 'op' ).nodeValue ) {
                case 'resize': resize(); break;
                case 'clear':  clear();  break;
                case 'close':  hide();   break;
            }
        }
    }

    function clickFilter( evt ) { //show/hide a specific message type
        if (!evt) {
            evt = window.event;
        }
        var span = ( evt.target ) ? evt.target : evt.srcElement;

        if ( span && span.tagName == 'SPAN' ) {

            var type = span.getAttributeNode( 'type' ).nodeValue;

            if ( evt.altKey ) {
                var filters = document.getElementById( IDs.filters ).getElementsByTagName( 'SPAN' );

                var active = 0;
                for ( var entry in messageTypes ) {
                    if (messageTypes[entry]) {
                        active++;
                    }
                }
                var oneActiveFilter = ( active == 1 && messageTypes[ type ] );

                for ( var i = 0; filters[ i ]; i++ ) {
                    var spanType = filters[ i ].getAttributeNode( 'type' ).nodeValue;

                    filters[ i ].className = ( oneActiveFilter || ( spanType == type ) ) ? spanType : spanType + 'Disabled';
                    messageTypes[ spanType ] = oneActiveFilter || ( spanType == type );
                }
            }
            else {
                messageTypes[ type ] = ! messageTypes[ type ];
                span.className = ( messageTypes[ type ] ) ? type : type + 'Disabled';
            }

            //build outputList's class from messageTypes object
            var disabledTypes = [];
            for ( type in messageTypes ) {
                if (!messageTypes[type]) {
                    disabledTypes.push(type);
                }
            }
            disabledTypes.push( '' );
            outputList.className = disabledTypes.join( 'Hidden ' );

            scrollToBottom();
        }
    }

    function clickVis( evt ) {
        if (!evt) {
            evt = window.event;
        }
        var el = ( evt.target ) ? evt.target : evt.srcElement;

        state.load = el.checked;
        setState();
    }


    function scrollToBottom() { //scroll list output to the bottom
        outputList.scrollTop = outputList.scrollHeight;
    }

    function isVisible() { //determine the visibility
        return ( bbird.style.display == 'block' );
    }

    function hide() {
        bbird.style.display = 'none';
    }

    function show() {
        var body = document.getElementsByTagName( 'BODY' )[ 0 ];
        body.removeChild( bbird );
        body.appendChild( bbird );
        bbird.style.display = 'block';
    }

    //sets the position
    function reposition( position ) {
        if ( position === undefined || position == null ) {
            position = ( state && state.pos === null ) ? 1 : ( state.pos + 1 ) % 4; //set to initial position ('topRight') or move to next position
        }

        switch ( position ) {
            case 0: classes[ 0 ] = 'bbTopLeft'; break;
            case 1: classes[ 0 ] = 'bbTopRight'; break;
            case 2: classes[ 0 ] = 'bbBottomLeft'; break;
            case 3: classes[ 0 ] = 'bbBottomRight'; break;
        }
        state.pos = position;
        setState();
    }

    function resize( size ) {
        if ( size === undefined || size === null ) {
            size = ( state && state.size == null ) ? 0 : ( state.size + 1 ) % 2;
        }

        classes[ 1 ] = ( size === 0 ) ? 'bbSmall' : 'bbLarge';

        var span = document.getElementById( IDs.size );
        span.title = ( size === 1 ) ? 'small' : 'large';
        span.className = span.title;

        state.size = size;
        setState();
        scrollToBottom();
    }

    function setState() {
        var props = [];
        for ( var entry in state ) {
            var value = ( state[ entry ] && state[ entry ].constructor === String ) ? '"' + state[ entry ] + '"' : state[ entry ];
            props.push( entry + ':' + value );
        }
        props = props.join( ',' );

        var expiration = new Date();
        expiration.setDate( expiration.getDate() + 14 );
        document.cookie = [ 'blackbird={', props, '}; expires=', expiration.toUTCString() ,';' ].join( '' );

        var newClass = [];
        for ( var word in classes ) {
            newClass.push( classes[ word ] );
        }
        bbird.className = newClass.join( ' ' );
    }

    function getState() {
        var re = new RegExp( /blackbird=({[^;]+})(;|\b|$)/ );
        var match = re.exec( document.cookie );
        return ( match && match[ 1 ] ) ? eval( '(' + match[ 1 ] + ')' ) : { pos:null, size:null, load:null };
    }

    //event handler for 'keyup' event for window
    function readKey( evt ) {
        if (!evt) {
            evt = window.event;
        }
        var code = 113; //F2 key

        if ( evt && evt.keyCode == code ) {

            var visible = isVisible();

            if (visible && evt.shiftKey && evt.altKey) {
                clear();
            }
            else if (visible && evt.shiftKey) {
                reposition();
            }
            else if (!evt.shiftKey && !evt.altKey) {
                (visible) ? hide() : show();
            }
        }
    }


    $[ NAMESPACE ] = {
        toggle:
            function() { ( isVisible() ) ? hide() : show(); },
        resize:
            function() { resize(); },
        clear:
            function() { clear(); },
        move:
            function() { reposition(); },
        debug:
            function( msg ) { addMessage( 'debug', msg ); },
        warn:
            function( msg ) { addMessage( 'warn', msg ); },
        info:
            function( msg ) { addMessage( 'info', msg ); },
        error:
            function( msg ) { addMessage( 'error', msg ); },
        time:
            function( label ) {
                var currentTime = new Date(); //record the current time when time() is executed

                if ( label == undefined || label == '' ) {
                    addMessage( 'error', '<b>ERROR:</b> Please specify a label for your timer statement' );
                }
                else if ( profiler[ label ] ) {
                    addMessage( 'time', [ label, ': ', currentTime - profiler[ label ],	'ms' ].join( '' ) );
                    delete profiler[ label ];
                }
                else {
                    profiler[ label ] = currentTime;
                    addMessage( 'time', label );
                }
                return currentTime;
            }
    };


    $(window).load(function(){

        var body = document.getElementsByTagName( 'BODY' )[ 0 ];
        bbird = body.appendChild( generateMarkup() );
        var $bbird = $(bbird);
        outputList = bbird.getElementsByTagName( 'OL' )[ 0 ];

        backgroundImage();

        // try to make draggable
        try{
            $bbird.draggable({
                handle: '.bbheader',
                containment: 'window',
                stop: function(e, ui) {
                    // position
                }
            });
        }catch(e){}

        //add events
        $('#'+IDs.checkbox).click(clickVis);
        $('#'+IDs.filters).click(clickFilter);
        $('#'+IDs.controls).click(clickControl);
        $(document).keyup(readKey);

        resize( state.size );
        reposition( state.pos );
        if ( state.load ) {
            show();
            document.getElementById( IDs.checkbox ).checked = true;
        }

        scrollToBottom();

        $[ NAMESPACE ].init = function() {
            show();
            $[ NAMESPACE ].error( [ '<b>', NAMESPACE, '</b> can only be initialized once' ] );
        };


        $(window).unload(function(){
            $('#'+IDs.checkbox).unbind('click');
            $('#'+IDs.filters).unbind('click');
            $('#'+IDs.controls).unbind('click');
            $(document).unbind('keyup');
        });

    });

})(Nx.$);(function($) {
    /**
     * MasterSlave connector.
     *
     * @author Remo Brunschwiler
     * @namespace Nx.Connector
     * @class MasterSlave
     * @extends Nx.Connector
     */
    Nx.Connector.MasterSlave = Nx.Connector.extend({

        /**
         * Initializes the MasterSlave connector.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {String} connectorId the unique connector id
         */
        init: function(connectorId) {
            // call base constructor
            this._super(connectorId);
        },

        /**
         * Notifies all registered components about the master state change.
         *
         * @method notify
         * @param {Module} component the module that sends the state change
         * @param {String} state the state
         * @param {Object} data contains the state relevant data (if any)
         * @param {Function} callback the callback function (could be executed after an asynchronous action)
         * @return {boolean} indicates whether the default action should be excuted or not
         */
        notify: function(component, state, data, callback) {
            /*
             * gives the components the ability to prevent the default- and afteraction from the events
             * (by returning false in the on<Event>-Handler)
             */
            var proceed = true, id;

            // check whether the component is the master component
            for (id in this.components) {
                if (this.components[id].component.modId === component.modId && this.components[id].role === 'Master') {
                    for (id in this.components) {
                        if (this.components[id].component.modId !== component.modId && this.components[id].component[state]) {
                            if (this.components[id].component[state](data, callback) === false) {
                                proceed = false;
                            }
                        }
                    }
                }
            }

            return proceed;
        }
    });
})(Nx.$);
/**
 * Contains utility functions for string concerning tasks.
 *
 * @author Remo Brunschwiler
 * @namespace Nx.Utils
 * @class String
 * @static
 */
(function($) {
    Nx.Utils.String = {
        /**
         * Capitalizes the first letter of the given string.
         *
         * @method capitalize
         * @param {String} str the original string
         * @return {String} the capitalized string
         */
        capitalize: function(str) {
            // capitalize the first letter
            return str.substr(0, 1).toUpperCase().concat(str.substr(1));
        }
    };
})(Nx.$);
/*!
 * jQuery UI 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */

/* mgiger add curCSS legacy support */
(function($) {
    if (!$.curCSS) {
        $.curCSS = $.css;
    }
})(jQuery);

(function( $, undefined ) {

// prevent duplicate loading
// this is only a problem because we proxy existing functions
// and we don't want to double proxy them
    $.ui = $.ui || {};
    if ( $.ui.version ) {
        return;
    }

    $.extend( $.ui, {
        version: "1.8.16",

        keyCode: {
            ALT: 18,
            BACKSPACE: 8,
            CAPS_LOCK: 20,
            COMMA: 188,
            COMMAND: 91,
            COMMAND_LEFT: 91, // COMMAND
            COMMAND_RIGHT: 93,
            CONTROL: 17,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            INSERT: 45,
            LEFT: 37,
            MENU: 93, // COMMAND_RIGHT
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SHIFT: 16,
            SPACE: 32,
            TAB: 9,
            UP: 38,
            WINDOWS: 91 // COMMAND
        }
    });

// plugins
    $.fn.extend({
        propAttr: $.fn.prop || $.fn.attr,

        _focus: $.fn.focus,
        focus: function( delay, fn ) {
            return typeof delay === "number" ?
                this.each(function() {
                    var elem = this;
                    setTimeout(function() {
                        $( elem ).focus();
                        if ( fn ) {
                            fn.call( elem );
                        }
                    }, delay );
                }) :
                this._focus.apply( this, arguments );
        },

        scrollParent: function() {
            var scrollParent;
            if (($.browser.msie && (/(static|relative)/).test(this.css('position'))) || (/absolute/).test(this.css('position'))) {
                scrollParent = this.parents().filter(function() {
                    return (/(relative|absolute|fixed)/).test($.curCSS(this,'position',1)) && (/(auto|scroll)/).test($.curCSS(this,'overflow',1)+$.curCSS(this,'overflow-y',1)+$.curCSS(this,'overflow-x',1));
                }).eq(0);
            } else {
                scrollParent = this.parents().filter(function() {
                    return (/(auto|scroll)/).test($.curCSS(this,'overflow',1)+$.curCSS(this,'overflow-y',1)+$.curCSS(this,'overflow-x',1));
                }).eq(0);
            }

            return (/fixed/).test(this.css('position')) || !scrollParent.length ? $(document) : scrollParent;
        },

        zIndex: function( zIndex ) {
            if ( zIndex !== undefined ) {
                return this.css( "zIndex", zIndex );
            }

            if ( this.length ) {
                var elem = $( this[ 0 ] ), position, value;
                while ( elem.length && elem[ 0 ] !== document ) {
                    // Ignore z-index if position is set to a value where z-index is ignored by the browser
                    // This makes behavior of this function consistent across browsers
                    // WebKit always returns auto if the element is positioned
                    position = elem.css( "position" );
                    if ( position === "absolute" || position === "relative" || position === "fixed" ) {
                        // IE returns 0 when zIndex is not specified
                        // other browsers return a string
                        // we ignore the case of nested elements with an explicit value of 0
                        // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                        value = parseInt( elem.css( "zIndex" ), 10 );
                        if ( !isNaN( value ) && value !== 0 ) {
                            return value;
                        }
                    }
                    elem = elem.parent();
                }
            }

            return 0;
        },

        disableSelection: function() {
            return this.bind( ( $.support.selectstart ? "selectstart" : "mousedown" ) +
                ".ui-disableSelection", function( event ) {
                event.preventDefault();
            });
        },

        enableSelection: function() {
            return this.unbind( ".ui-disableSelection" );
        }
    });

    $.each( [ "Width", "Height" ], function( i, name ) {
        var side = name === "Width" ? [ "Left", "Right" ] : [ "Top", "Bottom" ],
            type = name.toLowerCase(),
            orig = {
                innerWidth: $.fn.innerWidth,
                innerHeight: $.fn.innerHeight,
                outerWidth: $.fn.outerWidth,
                outerHeight: $.fn.outerHeight
            };

        function reduce( elem, size, border, margin ) {
            $.each( side, function() {
                size -= parseFloat( $.curCSS( elem, "padding" + this, true) ) || 0;
                if ( border ) {
                    size -= parseFloat( $.curCSS( elem, "border" + this + "Width", true) ) || 0;
                }
                if ( margin ) {
                    size -= parseFloat( $.curCSS( elem, "margin" + this, true) ) || 0;
                }
            });
            return size;
        }

        $.fn[ "inner" + name ] = function( size ) {
            if ( size === undefined ) {
                return orig[ "inner" + name ].call( this );
            }

            return this.each(function() {
                $( this ).css( type, reduce( this, size ) + "px" );
            });
        };

        $.fn[ "outer" + name] = function( size, margin ) {
            if ( typeof size !== "number" ) {
                return orig[ "outer" + name ].call( this, size );
            }

            return this.each(function() {
                $( this).css( type, reduce( this, size, true, margin ) + "px" );
            });
        };
    });

// selectors
    function focusable( element, isTabIndexNotNaN ) {
        var nodeName = element.nodeName.toLowerCase();
        if ( "area" === nodeName ) {
            var map = element.parentNode,
                mapName = map.name,
                img;
            if ( !element.href || !mapName || map.nodeName.toLowerCase() !== "map" ) {
                return false;
            }
            img = $( "img[usemap=#" + mapName + "]" )[0];
            return !!img && visible( img );
        }
        return ( /input|select|textarea|button|object/.test( nodeName )
                ? !element.disabled
                : "a" == nodeName
                ? element.href || isTabIndexNotNaN
                : isTabIndexNotNaN)
            // the element and all of its ancestors must be visible
            && visible( element );
    }

    function visible( element ) {
        return !$( element ).parents().andSelf().filter(function() {
            return $.curCSS( this, "visibility" ) === "hidden" ||
                $.expr.filters.hidden( this );
        }).length;
    }

    $.extend( $.expr[ ":" ], {
        data: function( elem, i, match ) {
            return !!$.data( elem, match[ 3 ] );
        },

        focusable: function( element ) {
            return focusable( element, !isNaN( $.attr( element, "tabindex" ) ) );
        },

        tabbable: function( element ) {
            var tabIndex = $.attr( element, "tabindex" ),
                isTabIndexNaN = isNaN( tabIndex );
            return ( isTabIndexNaN || tabIndex >= 0 ) && focusable( element, !isTabIndexNaN );
        }
    });

// support
    $(function() {
        var body = document.body,
            div = body.appendChild( div = document.createElement( "div" ) );

        $.extend( div.style, {
            minHeight: "100px",
            height: "auto",
            padding: 0,
            borderWidth: 0
        });

        $.support.minHeight = div.offsetHeight === 100;
        $.support.selectstart = "onselectstart" in div;

        // set display to none to avoid a layout bug in IE
        // http://dev.jquery.com/ticket/4014
        body.removeChild( div ).style.display = "none";
    });





// deprecated
    $.extend( $.ui, {
        // $.ui.plugin is deprecated.  Use the proxy pattern instead.
        plugin: {
            add: function( module, option, set ) {
                var proto = $.ui[ module ].prototype;
                for ( var i in set ) {
                    proto.plugins[ i ] = proto.plugins[ i ] || [];
                    proto.plugins[ i ].push( [ option, set[ i ] ] );
                }
            },
            call: function( instance, name, args ) {
                var set = instance.plugins[ name ];
                if ( !set || !instance.element[ 0 ].parentNode ) {
                    return;
                }

                for ( var i = 0; i < set.length; i++ ) {
                    if ( instance.options[ set[ i ][ 0 ] ] ) {
                        set[ i ][ 1 ].apply( instance.element, args );
                    }
                }
            }
        },

        // will be deprecated when we switch to jQuery 1.4 - use jQuery.contains()
        contains: function( a, b ) {
            return document.compareDocumentPosition ?
            a.compareDocumentPosition( b ) & 16 :
            a !== b && a.contains( b );
        },

        // only used by resizable
        hasScroll: function( el, a ) {

            //If overflow is hidden, the element might have extra content, but the user wants to hide it
            if ( $( el ).css( "overflow" ) === "hidden") {
                return false;
            }

            var scroll = ( a && a === "left" ) ? "scrollLeft" : "scrollTop",
                has = false;

            if ( el[ scroll ] > 0 ) {
                return true;
            }

            // TODO: determine which cases actually cause this to happen
            // if the element doesn't have the scroll set, see if it's possible to
            // set the scroll
            el[ scroll ] = 1;
            has = ( el[ scroll ] > 0 );
            el[ scroll ] = 0;
            return has;
        },

        // these are odd functions, fix the API or move into individual plugins
        isOverAxis: function( x, reference, size ) {
            //Determines when x coordinate is over "b" element axis
            return ( x > reference ) && ( x < ( reference + size ) );
        },
        isOver: function( y, x, top, left, height, width ) {
            //Determines when x, y coordinates is over "b" element
            return $.ui.isOverAxis( y, top, height ) && $.ui.isOverAxis( x, left, width );
        }
    });

})( jQuery );
/*!
 * jQuery UI Widget 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Widget
 */
(function( $, undefined ) {

// jQuery 1.4+
    if ( $.cleanData ) {
        var _cleanData = $.cleanData;
        $.cleanData = function( elems ) {
            for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
                try {
                    $( elem ).triggerHandler( "remove" );
                    // http://bugs.jquery.com/ticket/8235
                } catch( e ) {}
            }
            _cleanData( elems );
        };
    } else {
        var _remove = $.fn.remove;
        $.fn.remove = function( selector, keepData ) {
            return this.each(function() {
                if ( !keepData ) {
                    if ( !selector || $.filter( selector, [ this ] ).length ) {
                        $( "*", this ).add( [ this ] ).each(function() {
                            try {
                                $( this ).triggerHandler( "remove" );
                                // http://bugs.jquery.com/ticket/8235
                            } catch( e ) {}
                        });
                    }
                }
                return _remove.call( $(this), selector, keepData );
            });
        };
    }

    $.widget = function( name, base, prototype ) {
        var namespace = name.split( "." )[ 0 ],
            fullName;
        name = name.split( "." )[ 1 ];
        fullName = namespace + "-" + name;

        if ( !prototype ) {
            prototype = base;
            base = $.Widget;
        }

        // create selector for plugin
        $.expr[ ":" ][ fullName ] = function( elem ) {
            return !!$.data( elem, name );
        };

        $[ namespace ] = $[ namespace ] || {};
        $[ namespace ][ name ] = function( options, element ) {
            // allow instantiation without initializing for simple inheritance
            if ( arguments.length ) {
                this._createWidget( options, element );
            }
        };

        var basePrototype = new base();
        // we need to make the options hash a property directly on the new instance
        // otherwise we'll modify the options hash on the prototype that we're
        // inheriting from
//	$.each( basePrototype, function( key, val ) {
//		if ( $.isPlainObject(val) ) {
//			basePrototype[ key ] = $.extend( {}, val );
//		}
//	});
        basePrototype.options = $.extend( true, {}, basePrototype.options );
        $[ namespace ][ name ].prototype = $.extend( true, basePrototype, {
            namespace: namespace,
            widgetName: name,
            widgetEventPrefix: $[ namespace ][ name ].prototype.widgetEventPrefix || name,
            widgetBaseClass: fullName
        }, prototype );

        $.widget.bridge( name, $[ namespace ][ name ] );
    };

    $.widget.bridge = function( name, object ) {
        $.fn[ name ] = function( options ) {
            var isMethodCall = typeof options === "string",
                args = Array.prototype.slice.call( arguments, 1 ),
                returnValue = this;

            // allow multiple hashes to be passed on init
            options = !isMethodCall && args.length ?
                $.extend.apply( null, [ true, options ].concat(args) ) :
                options;

            // prevent calls to internal methods
            if ( isMethodCall && options.charAt( 0 ) === "_" ) {
                return returnValue;
            }

            if ( isMethodCall ) {
                this.each(function() {
                    var instance = $.data( this, name ),
                        methodValue = instance && $.isFunction( instance[options] ) ?
                            instance[ options ].apply( instance, args ) :
                            instance;
                    // TODO: add this back in 1.9 and use $.error() (see #5972)
//				if ( !instance ) {
//					throw "cannot call methods on " + name + " prior to initialization; " +
//						"attempted to call method '" + options + "'";
//				}
//				if ( !$.isFunction( instance[options] ) ) {
//					throw "no such method '" + options + "' for " + name + " widget instance";
//				}
//				var methodValue = instance[ options ].apply( instance, args );
                    if ( methodValue !== instance && methodValue !== undefined ) {
                        returnValue = methodValue;
                        return false;
                    }
                });
            } else {
                this.each(function() {
                    var instance = $.data( this, name );
                    if ( instance ) {
                        instance.option( options || {} )._init();
                    } else {
                        $.data( this, name, new object( options, this ) );
                    }
                });
            }

            return returnValue;
        };
    };

    $.Widget = function( options, element ) {
        // allow instantiation without initializing for simple inheritance
        if ( arguments.length ) {
            this._createWidget( options, element );
        }
    };

    $.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        options: {
            disabled: false
        },
        _createWidget: function( options, element ) {
            // $.widget.bridge stores the plugin instance, but we do it anyway
            // so that it's stored even before the _create function runs
            $.data( element, this.widgetName, this );
            this.element = $( element );
            this.options = $.extend( true, {},
                this.options,
                this._getCreateOptions(),
                options );

            var self = this;
            this.element.bind( "remove." + this.widgetName, function() {
                self.destroy();
            });

            this._create();
            this._trigger( "create" );
            this._init();
        },
        _getCreateOptions: function() {
            return $.metadata && $.metadata.get( this.element[0] )[ this.widgetName ];
        },
        _create: function() {},
        _init: function() {},

        destroy: function() {
            this.element
                .unbind( "." + this.widgetName )
                .removeData( this.widgetName );
            this.widget()
                .unbind( "." + this.widgetName )
                .removeAttr( "aria-disabled" )
                .removeClass(
                    this.widgetBaseClass + "-disabled " +
                    "ui-state-disabled" );
        },

        widget: function() {
            return this.element;
        },

        option: function( key, value ) {
            var options = key;

            if ( arguments.length === 0 ) {
                // don't return a reference to the internal hash
                return $.extend( {}, this.options );
            }

            if  (typeof key === "string" ) {
                if ( value === undefined ) {
                    return this.options[ key ];
                }
                options = {};
                options[ key ] = value;
            }

            this._setOptions( options );

            return this;
        },
        _setOptions: function( options ) {
            var self = this;
            $.each( options, function( key, value ) {
                self._setOption( key, value );
            });

            return this;
        },
        _setOption: function( key, value ) {
            this.options[ key ] = value;

            if ( key === "disabled" ) {
                this.widget()
                    [ value ? "addClass" : "removeClass"](
                    this.widgetBaseClass + "-disabled" + " " +
                    "ui-state-disabled" )
                    .attr( "aria-disabled", value );
            }

            return this;
        },

        enable: function() {
            return this._setOption( "disabled", false );
        },
        disable: function() {
            return this._setOption( "disabled", true );
        },

        _trigger: function( type, event, data ) {
            var callback = this.options[ type ];

            event = $.Event( event );
            event.type = ( type === this.widgetEventPrefix ?
                type :
            this.widgetEventPrefix + type ).toLowerCase();
            data = data || {};

            // copy original event properties over to the new event
            // this would happen if we could call $.event.fix instead of $.Event
            // but we don't have a way to force an event to be fixed multiple times
            if ( event.originalEvent ) {
                for ( var i = $.event.props.length, prop; i; ) {
                    prop = $.event.props[ --i ];
                    event[ prop ] = event.originalEvent[ prop ];
                }
            }

            this.element.trigger( event, data );

            return !( $.isFunction(callback) &&
            callback.call( this.element[0], event, data ) === false ||
            event.isDefaultPrevented() );
        }
    };

})( jQuery );
/*!
 * jQuery UI Mouse 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Mouse
 *
 * Depends:
 *	jquery.ui.widget.js
 */
(function( $, undefined ) {

    var mouseHandled = false;
    $( document ).mouseup( function( e ) {
        mouseHandled = false;
    });

    $.widget("ui.mouse", {
        options: {
            cancel: ':input,option',
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var self = this;

            this.element
                .bind('mousedown.'+this.widgetName, function(event) {
                    return self._mouseDown(event);
                })
                .bind('click.'+this.widgetName, function(event) {
                    if (true === $.data(event.target, self.widgetName + '.preventClickEvent')) {
                        $.removeData(event.target, self.widgetName + '.preventClickEvent');
                        event.stopImmediatePropagation();
                        return false;
                    }
                });

            this.started = false;
        },

        // TODO: make sure destroying one instance of mouse doesn't mess with
        // other instances of mouse
        _mouseDestroy: function() {
            this.element.unbind('.'+this.widgetName);
        },

        _mouseDown: function(event) {
            // don't let more than one widget handle mouseStart
            if( mouseHandled ) { return };

            // we may have missed mouseup (out of window)
            (this._mouseStarted && this._mouseUp(event));

            this._mouseDownEvent = event;

            var self = this,
                btnIsLeft = (event.which == 1),
            // event.target.nodeName works around a bug in IE 8 with
            // disabled inputs (#7620)
                elIsCancel = (typeof this.options.cancel == "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
            if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
                return true;
            }

            this.mouseDelayMet = !this.options.delay;
            if (!this.mouseDelayMet) {
                this._mouseDelayTimer = setTimeout(function() {
                    self.mouseDelayMet = true;
                }, this.options.delay);
            }

            if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                this._mouseStarted = (this._mouseStart(event) !== false);
                if (!this._mouseStarted) {
                    event.preventDefault();
                    return true;
                }
            }

            // Click event may never have fired (Gecko & Opera)
            if (true === $.data(event.target, this.widgetName + '.preventClickEvent')) {
                $.removeData(event.target, this.widgetName + '.preventClickEvent');
            }

            // these delegates are required to keep context
            this._mouseMoveDelegate = function(event) {
                return self._mouseMove(event);
            };
            this._mouseUpDelegate = function(event) {
                return self._mouseUp(event);
            };
            $(document)
                .bind('mousemove.'+this.widgetName, this._mouseMoveDelegate)
                .bind('mouseup.'+this.widgetName, this._mouseUpDelegate);

            event.preventDefault();

            mouseHandled = true;
            return true;
        },

        _mouseMove: function(event) {
            // IE mouseup check - mouseup happened when mouse was out of window
            if ($.browser.msie && !(document.documentMode >= 9) && !event.button) {
                return this._mouseUp(event);
            }

            if (this._mouseStarted) {
                this._mouseDrag(event);
                return event.preventDefault();
            }

            if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                this._mouseStarted =
                    (this._mouseStart(this._mouseDownEvent, event) !== false);
                (this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event));
            }

            return !this._mouseStarted;
        },

        _mouseUp: function(event) {
            $(document)
                .unbind('mousemove.'+this.widgetName, this._mouseMoveDelegate)
                .unbind('mouseup.'+this.widgetName, this._mouseUpDelegate);

            if (this._mouseStarted) {
                this._mouseStarted = false;

                if (event.target == this._mouseDownEvent.target) {
                    $.data(event.target, this.widgetName + '.preventClickEvent', true);
                }

                this._mouseStop(event);
            }

            return false;
        },

        _mouseDistanceMet: function(event) {
            return (Math.max(
                    Math.abs(this._mouseDownEvent.pageX - event.pageX),
                    Math.abs(this._mouseDownEvent.pageY - event.pageY)
                ) >= this.options.distance
            );
        },

        _mouseDelayMet: function(event) {
            return this.mouseDelayMet;
        },

        // These are placeholder methods, to be overriden by extending plugin
        _mouseStart: function(event) {},
        _mouseDrag: function(event) {},
        _mouseStop: function(event) {},
        _mouseCapture: function(event) { return true; }
    });

})(jQuery);
/*
 * jQuery UI Position 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Position
 */
(function( $, undefined ) {

    $.ui = $.ui || {};

    var horizontalPositions = /left|center|right/,
        verticalPositions = /top|center|bottom/,
        center = "center",
        _position = $.fn.position,
        _offset = $.fn.offset;

    $.fn.position = function( options ) {
        if ( !options || !options.of ) {
            return _position.apply( this, arguments );
        }

        // make a copy, we don't want to modify arguments
        options = $.extend( {}, options );

        var target = $( options.of ),
            targetElem = target[0],
            collision = ( options.collision || "flip" ).split( " " ),
            offset = options.offset ? options.offset.split( " " ) : [ 0, 0 ],
            targetWidth,
            targetHeight,
            basePosition;

        if ( targetElem.nodeType === 9 ) {
            targetWidth = target.width();
            targetHeight = target.height();
            basePosition = { top: 0, left: 0 };
            // TODO: use $.isWindow() in 1.9
        } else if ( targetElem.setTimeout ) {
            targetWidth = target.width();
            targetHeight = target.height();
            basePosition = { top: target.scrollTop(), left: target.scrollLeft() };
        } else if ( targetElem.preventDefault ) {
            // force left top to allow flipping
            options.at = "left top";
            targetWidth = targetHeight = 0;
            basePosition = { top: options.of.pageY, left: options.of.pageX };
        } else {
            targetWidth = target.outerWidth();
            targetHeight = target.outerHeight();
            basePosition = target.offset();
        }

        // force my and at to have valid horizontal and veritcal positions
        // if a value is missing or invalid, it will be converted to center
        $.each( [ "my", "at" ], function() {
            var pos = ( options[this] || "" ).split( " " );
            if ( pos.length === 1) {
                pos = horizontalPositions.test( pos[0] ) ?
                    pos.concat( [center] ) :
                    verticalPositions.test( pos[0] ) ?
                        [ center ].concat( pos ) :
                        [ center, center ];
            }
            pos[ 0 ] = horizontalPositions.test( pos[0] ) ? pos[ 0 ] : center;
            pos[ 1 ] = verticalPositions.test( pos[1] ) ? pos[ 1 ] : center;
            options[ this ] = pos;
        });

        // normalize collision option
        if ( collision.length === 1 ) {
            collision[ 1 ] = collision[ 0 ];
        }

        // normalize offset option
        offset[ 0 ] = parseInt( offset[0], 10 ) || 0;
        if ( offset.length === 1 ) {
            offset[ 1 ] = offset[ 0 ];
        }
        offset[ 1 ] = parseInt( offset[1], 10 ) || 0;

        if ( options.at[0] === "right" ) {
            basePosition.left += targetWidth;
        } else if ( options.at[0] === center ) {
            basePosition.left += targetWidth / 2;
        }

        if ( options.at[1] === "bottom" ) {
            basePosition.top += targetHeight;
        } else if ( options.at[1] === center ) {
            basePosition.top += targetHeight / 2;
        }

        basePosition.left += offset[ 0 ];
        basePosition.top += offset[ 1 ];

        return this.each(function() {
            var elem = $( this ),
                elemWidth = elem.outerWidth(),
                elemHeight = elem.outerHeight(),
                marginLeft = parseInt( $.curCSS( this, "marginLeft", true ) ) || 0,
                marginTop = parseInt( $.curCSS( this, "marginTop", true ) ) || 0,
                collisionWidth = elemWidth + marginLeft +
                    ( parseInt( $.curCSS( this, "marginRight", true ) ) || 0 ),
                collisionHeight = elemHeight + marginTop +
                    ( parseInt( $.curCSS( this, "marginBottom", true ) ) || 0 ),
                position = $.extend( {}, basePosition ),
                collisionPosition;

            if ( options.my[0] === "right" ) {
                position.left -= elemWidth;
            } else if ( options.my[0] === center ) {
                position.left -= elemWidth / 2;
            }

            if ( options.my[1] === "bottom" ) {
                position.top -= elemHeight;
            } else if ( options.my[1] === center ) {
                position.top -= elemHeight / 2;
            }

            // prevent fractions (see #5280)
            position.left = Math.round( position.left );
            position.top = Math.round( position.top );

            collisionPosition = {
                left: position.left - marginLeft,
                top: position.top - marginTop
            };

            $.each( [ "left", "top" ], function( i, dir ) {
                if ( $.ui.position[ collision[i] ] ) {
                    $.ui.position[ collision[i] ][ dir ]( position, {
                        targetWidth: targetWidth,
                        targetHeight: targetHeight,
                        elemWidth: elemWidth,
                        elemHeight: elemHeight,
                        collisionPosition: collisionPosition,
                        collisionWidth: collisionWidth,
                        collisionHeight: collisionHeight,
                        offset: offset,
                        my: options.my,
                        at: options.at
                    });
                }
            });

            if ( $.fn.bgiframe ) {
                elem.bgiframe();
            }
            elem.offset( $.extend( position, { using: options.using } ) );
        });
    };

    $.ui.position = {
        fit: {
            left: function( position, data ) {
                var win = $( window ),
                    over = data.collisionPosition.left + data.collisionWidth - win.width() - win.scrollLeft();
                position.left = over > 0 ? position.left - over : Math.max( position.left - data.collisionPosition.left, position.left );
            },
            top: function( position, data ) {
                var win = $( window ),
                    over = data.collisionPosition.top + data.collisionHeight - win.height() - win.scrollTop();
                position.top = over > 0 ? position.top - over : Math.max( position.top - data.collisionPosition.top, position.top );
            }
        },

        flip: {
            left: function( position, data ) {
                if ( data.at[0] === center ) {
                    return;
                }
                var win = $( window ),
                    over = data.collisionPosition.left + data.collisionWidth - win.width() - win.scrollLeft(),
                    myOffset = data.my[ 0 ] === "left" ?
                        -data.elemWidth :
                        data.my[ 0 ] === "right" ?
                            data.elemWidth :
                            0,
                    atOffset = data.at[ 0 ] === "left" ?
                        data.targetWidth :
                        -data.targetWidth,
                    offset = -2 * data.offset[ 0 ];
                position.left += data.collisionPosition.left < 0 ?
                myOffset + atOffset + offset :
                    over > 0 ?
                    myOffset + atOffset + offset :
                        0;
            },
            top: function( position, data ) {
                if ( data.at[1] === center ) {
                    return;
                }
                var win = $( window ),
                    over = data.collisionPosition.top + data.collisionHeight - win.height() - win.scrollTop(),
                    myOffset = data.my[ 1 ] === "top" ?
                        -data.elemHeight :
                        data.my[ 1 ] === "bottom" ?
                            data.elemHeight :
                            0,
                    atOffset = data.at[ 1 ] === "top" ?
                        data.targetHeight :
                        -data.targetHeight,
                    offset = -2 * data.offset[ 1 ];
                position.top += data.collisionPosition.top < 0 ?
                myOffset + atOffset + offset :
                    over > 0 ?
                    myOffset + atOffset + offset :
                        0;
            }
        }
    };

// offset setter from jQuery 1.4
    if ( !$.offset.setOffset ) {
        $.offset.setOffset = function( elem, options ) {
            // set position first, in-case top/left are set even on static elem
            if ( /static/.test( $.curCSS( elem, "position" ) ) ) {
                elem.style.position = "relative";
            }
            var curElem   = $( elem ),
                curOffset = curElem.offset(),
                curTop    = parseInt( $.curCSS( elem, "top",  true ), 10 ) || 0,
                curLeft   = parseInt( $.curCSS( elem, "left", true ), 10)  || 0,
                props     = {
                    top:  (options.top  - curOffset.top)  + curTop,
                    left: (options.left - curOffset.left) + curLeft
                };

            if ( 'using' in options ) {
                options.using.call( elem, props );
            } else {
                curElem.css( props );
            }
        };

        $.fn.offset = function( options ) {
            var elem = this[ 0 ];
            if ( !elem || !elem.ownerDocument ) { return null; }
            if ( options ) {
                return this.each(function() {
                    $.offset.setOffset( this, options );
                });
            }
            return _offset.call( this );
        };
    }

}( jQuery ));
/*
 * jQuery UI Autocomplete 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Autocomplete
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.position.js
 */
(function( $, undefined ) {

// used to prevent race conditions with remote data sources
    var requestIndex = 0;

    $.widget( "ui.autocomplete", {
        options: {
            appendTo: "body",
            autoFocus: false,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null
        },

        pending: 0,

        _create: function() {
            var self = this,
                doc = this.element[ 0 ].ownerDocument,
                suppressKeyPress;

            this.element
                .addClass( "ui-autocomplete-input" )
                .attr( "autocomplete", "off" )
                // TODO verify these actually work as intended
                .attr({
                    role: "textbox",
                    "aria-autocomplete": "list",
                    "aria-haspopup": "true"
                })
                .bind( "keydown.autocomplete", function( event ) {
                    if ( self.options.disabled || self.element.propAttr( "readOnly" ) ) {
                        return;
                    }

                    suppressKeyPress = false;
                    var keyCode = $.ui.keyCode;
                    switch( event.keyCode ) {
                        case keyCode.PAGE_UP:
                            self._move( "previousPage", event );
                            break;
                        case keyCode.PAGE_DOWN:
                            self._move( "nextPage", event );
                            break;
                        case keyCode.UP:
                            self._move( "previous", event );
                            // prevent moving cursor to beginning of text field in some browsers
                            event.preventDefault();
                            break;
                        case keyCode.DOWN:
                            self._move( "next", event );
                            // prevent moving cursor to end of text field in some browsers
                            event.preventDefault();
                            break;
                        case keyCode.ENTER:
                        case keyCode.NUMPAD_ENTER:
                            // when menu is open and has focus
                            if ( self.menu.active ) {
                                // #6055 - Opera still allows the keypress to occur
                                // which causes forms to submit
                                suppressKeyPress = true;
                                event.preventDefault();
                            }
                        //passthrough - ENTER and TAB both select the current element
                        case keyCode.TAB:
                            if ( !self.menu.active ) {
                                return;
                            }
                            self.menu.select( event );
                            break;
                        case keyCode.ESCAPE:
                            self.element.val( self.term );
                            self.close( event );
                            break;
                        default:
                            // keypress is triggered before the input value is changed
                            clearTimeout( self.searching );
                            self.searching = setTimeout(function() {
                                // only search if the value has changed
                                if ( self.term != self.element.val() ) {
                                    self.selectedItem = null;
                                    self.search( null, event );
                                }
                            }, self.options.delay );
                            break;
                    }
                })
                .bind( "keypress.autocomplete", function( event ) {
                    if ( suppressKeyPress ) {
                        suppressKeyPress = false;
                        event.preventDefault();
                    }
                })
                .bind( "focus.autocomplete", function() {
                    if ( self.options.disabled ) {
                        return;
                    }

                    self.selectedItem = null;
                    self.previous = self.element.val();
                })
                .bind( "blur.autocomplete", function( event ) {
                    if ( self.options.disabled ) {
                        return;
                    }

                    clearTimeout( self.searching );
                    // clicks on the menu (or a button to trigger a search) will cause a blur event
                    self.closing = setTimeout(function() {
                        self.close( event );
                        self._change( event );
                    }, 150 );
                });
            this._initSource();
            this.response = function() {
                return self._response.apply( self, arguments );
            };
            this.menu = $( "<ul></ul>" )
                .addClass( "ui-autocomplete" )
                .appendTo( $( this.options.appendTo || "body", doc )[0] )
                // prevent the close-on-blur in case of a "slow" click on the menu (long mousedown)
                .mousedown(function( event ) {
                    // clicking on the scrollbar causes focus to shift to the body
                    // but we can't detect a mouseup or a click immediately afterward
                    // so we have to track the next mousedown and close the menu if
                    // the user clicks somewhere outside of the autocomplete
                    var menuElement = self.menu.element[ 0 ];
                    if ( !$( event.target ).closest( ".ui-menu-item" ).length ) {
                        setTimeout(function() {
                            $( document ).one( 'mousedown', function( event ) {
                                if ( event.target !== self.element[ 0 ] &&
                                    event.target !== menuElement &&
                                    !$.ui.contains( menuElement, event.target ) ) {
                                    self.close();
                                }
                            });
                        }, 1 );
                    }

                    // use another timeout to make sure the blur-event-handler on the input was already triggered
                    setTimeout(function() {
                        clearTimeout( self.closing );
                    }, 13);
                })
                .menu({
                    focus: function( event, ui ) {
                        var item = ui.item.data( "item.autocomplete" );
                        if ( false !== self._trigger( "focus", event, { item: item } ) ) {
                            // use value to match what will end up in the input, if it was a key event
                            if ( /^key/.test(event.originalEvent.type) ) {
                                self.element.val( item.value );
                            }
                        }
                    },
                    selected: function( event, ui ) {
                        var item = ui.item.data( "item.autocomplete" ),
                            previous = self.previous;

                        // only trigger when focus was lost (click on menu)
                        if ( self.element[0] !== doc.activeElement ) {
                            self.element.focus();
                            self.previous = previous;
                            // #6109 - IE triggers two focus events and the second
                            // is asynchronous, so we need to reset the previous
                            // term synchronously and asynchronously :-(
                            setTimeout(function() {
                                self.previous = previous;
                                self.selectedItem = item;
                            }, 1);
                        }

                        if ( false !== self._trigger( "select", event, { item: item } ) ) {
                            self.element.val( item.value );
                        }
                        // reset the term after the select event
                        // this allows custom select handling to work properly
                        self.term = self.element.val();

                        self.close( event );
                        self.selectedItem = item;
                    },
                    blur: function( event, ui ) {
                        // don't set the value of the text field if it's already correct
                        // this prevents moving the cursor unnecessarily
                        if ( self.menu.element.is(":visible") &&
                            ( self.element.val() !== self.term ) ) {
                            self.element.val( self.term );
                        }
                    }
                })
                .zIndex( this.element.zIndex() + 1 )
                // workaround for jQuery bug #5781 http://dev.jquery.com/ticket/5781
                .css({ top: 0, left: 0 })
                .hide()
                .data( "menu" );
            if ( $.fn.bgiframe ) {
                this.menu.element.bgiframe();
            }
        },

        destroy: function() {
            this.element
                .removeClass( "ui-autocomplete-input" )
                .removeAttr( "autocomplete" )
                .removeAttr( "role" )
                .removeAttr( "aria-autocomplete" )
                .removeAttr( "aria-haspopup" );
            this.menu.element.remove();
            $.Widget.prototype.destroy.call( this );
        },

        _setOption: function( key, value ) {
            $.Widget.prototype._setOption.apply( this, arguments );
            if ( key === "source" ) {
                this._initSource();
            }
            if ( key === "appendTo" ) {
                this.menu.element.appendTo( $( value || "body", this.element[0].ownerDocument )[0] )
            }
            if ( key === "disabled" && value && this.xhr ) {
                this.xhr.abort();
            }
        },

        _initSource: function() {
            var self = this,
                array,
                url;
            if ( $.isArray(this.options.source) ) {
                array = this.options.source;
                this.source = function( request, response ) {
                    response( $.ui.autocomplete.filter(array, request.term) );
                };
            } else if ( typeof this.options.source === "string" ) {
                url = this.options.source;
                this.source = function( request, response ) {
                    if ( self.xhr ) {
                        self.xhr.abort();
                    }
                    self.xhr = $.ajax({
                        url: url,
                        data: request,
                        dataType: "json",
                        autocompleteRequest: ++requestIndex,
                        success: function( data, status ) {
                            if ( this.autocompleteRequest === requestIndex ) {
                                response( data );
                            }
                        },
                        error: function() {
                            if ( this.autocompleteRequest === requestIndex ) {
                                response( [] );
                            }
                        }
                    });
                };
            } else {
                this.source = this.options.source;
            }
        },

        search: function( value, event ) {
            value = value != null ? value : this.element.val();

            // always save the actual value, not the one passed as an argument
            this.term = this.element.val();

            if ( value.length < this.options.minLength ) {
                return this.close( event );
            }

            clearTimeout( this.closing );
            if ( this._trigger( "search", event ) === false ) {
                return;
            }

            return this._search( value );
        },

        _search: function( value ) {
            this.pending++;
            this.element.addClass( "ui-autocomplete-loading" );

            this.source( { term: value }, this.response );
        },

        _response: function( content ) {
            if ( !this.options.disabled && content && content.length ) {
                content = this._normalize( content );
                this._suggest( content );
                this._trigger( "open" );
            } else {
                this.close();
            }
            this.pending--;
            if ( !this.pending ) {
                this.element.removeClass( "ui-autocomplete-loading" );
            }
        },

        close: function( event ) {
            clearTimeout( this.closing );
            if ( this.menu.element.is(":visible") ) {
                this.menu.element.hide();
                this.menu.deactivate();
                this._trigger( "close", event );
            }
        },

        _change: function( event ) {
            if ( this.previous !== this.element.val() ) {
                this._trigger( "change", event, { item: this.selectedItem } );
            }
        },

        _normalize: function( items ) {
            // assume all items have the right format when the first item is complete
            if ( items.length && items[0].label && items[0].value ) {
                return items;
            }
            return $.map( items, function(item) {
                if ( typeof item === "string" ) {
                    return {
                        label: item,
                        value: item
                    };
                }
                return $.extend({
                    label: item.label || item.value,
                    value: item.value || item.label
                }, item );
            });
        },

        _suggest: function( items ) {
            var ul = this.menu.element
                .empty()
                .zIndex( this.element.zIndex() + 1 );
            this._renderMenu( ul, items );
            // TODO refresh should check if the active item is still in the dom, removing the need for a manual deactivate
            this.menu.deactivate();
            this.menu.refresh();

            // size and position menu
            ul.show();
            this._resizeMenu();
            ul.position( $.extend({
                of: this.element
            }, this.options.position ));

            if ( this.options.autoFocus ) {
                this.menu.next( new $.Event("mouseover") );
            }
        },

        _resizeMenu: function() {
            var ul = this.menu.element;
            ul.outerWidth( Math.max(
                ul.width( "" ).outerWidth(),
                this.element.outerWidth()
            ) );
        },

        _renderMenu: function( ul, items ) {
            var self = this;
            $.each( items, function( index, item ) {
                self._renderItem( ul, item );
            });
        },

        _renderItem: function( ul, item) {
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( $( "<a></a>" ).text( item.label ) )
                .appendTo( ul );
        },

        _move: function( direction, event ) {
            if ( !this.menu.element.is(":visible") ) {
                this.search( null, event );
                return;
            }
            if ( this.menu.first() && /^previous/.test(direction) ||
                this.menu.last() && /^next/.test(direction) ) {
                this.element.val( this.term );
                this.menu.deactivate();
                return;
            }
            this.menu[ direction ]( event );
        },

        widget: function() {
            return this.menu.element;
        }
    });

    $.extend( $.ui.autocomplete, {
        escapeRegex: function( value ) {
            return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        },
        filter: function(array, term) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
            return $.grep( array, function(value) {
                return matcher.test( value.label || value.value || value );
            });
        }
    });

}( jQuery ));

/*
 * jQuery UI Menu (not officially released)
 *
 * This widget isn't yet finished and the API is subject to change. We plan to finish
 * it for the next release. You're welcome to give it a try anyway and give us feedback,
 * as long as you're okay with migrating your code later on. We can help with that, too.
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Menu
 *
 * Depends:
 *	jquery.ui.core.js
 *  jquery.ui.widget.js
 */
(function($) {

    $.widget("ui.menu", {
        _create: function() {
            var self = this;
            this.element
                .addClass("ui-menu ui-widget ui-widget-content ui-corner-all")
                .attr({
                    role: "listbox",
                    "aria-activedescendant": "ui-active-menuitem"
                })
                .click(function( event ) {
                    if ( !$( event.target ).closest( ".ui-menu-item a" ).length ) {
                        return;
                    }
                    // temporary
                    event.preventDefault();
                    self.select( event );
                });
            this.refresh();
        },

        refresh: function() {
            var self = this;

            // don't refresh list items that are already adapted
            var items = this.element.children("li:not(.ui-menu-item):has(a)")
                .addClass("ui-menu-item")
                .attr("role", "menuitem");

            items.children("a")
                .addClass("ui-corner-all")
                .attr("tabindex", -1)
                // mouseenter doesn't work with event delegation
                .mouseenter(function( event ) {
                    self.activate( event, $(this).parent() );
                })
                .mouseleave(function() {
                    self.deactivate();
                });
        },

        activate: function( event, item ) {
            this.deactivate();
            if (this.hasScroll()) {
                var offset = item.offset().top - this.element.offset().top,
                    scroll = this.element.scrollTop(),
                    elementHeight = this.element.height();
                if (offset < 0) {
                    this.element.scrollTop( scroll + offset);
                } else if (offset >= elementHeight) {
                    this.element.scrollTop( scroll + offset - elementHeight + item.height());
                }
            }
            this.active = item.eq(0)
                .children("a")
                .addClass("ui-state-hover")
                .attr("id", "ui-active-menuitem")
                .end();
            this._trigger("focus", event, { item: item });
        },

        deactivate: function() {
            if (!this.active) { return; }

            this.active.children("a")
                .removeClass("ui-state-hover")
                .removeAttr("id");
            this._trigger("blur");
            this.active = null;
        },

        next: function(event) {
            this.move("next", ".ui-menu-item:first", event);
        },

        previous: function(event) {
            this.move("prev", ".ui-menu-item:last", event);
        },

        first: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length;
        },

        last: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length;
        },

        move: function(direction, edge, event) {
            if (!this.active) {
                this.activate(event, this.element.children(edge));
                return;
            }
            var next = this.active[direction + "All"](".ui-menu-item").eq(0);
            if (next.length) {
                this.activate(event, next);
            } else {
                this.activate(event, this.element.children(edge));
            }
        },

        // TODO merge with previousPage
        nextPage: function(event) {
            if (this.hasScroll()) {
                // TODO merge with no-scroll-else
                if (!this.active || this.last()) {
                    this.activate(event, this.element.children(".ui-menu-item:first"));
                    return;
                }
                var base = this.active.offset().top,
                    height = this.element.height(),
                    result = this.element.children(".ui-menu-item").filter(function() {
                        var close = $(this).offset().top - base - height + $(this).height();
                        // TODO improve approximation
                        return close < 10 && close > -10;
                    });

                // TODO try to catch this earlier when scrollTop indicates the last page anyway
                if (!result.length) {
                    result = this.element.children(".ui-menu-item:last");
                }
                this.activate(event, result);
            } else {
                this.activate(event, this.element.children(".ui-menu-item")
                    .filter(!this.active || this.last() ? ":first" : ":last"));
            }
        },

        // TODO merge with nextPage
        previousPage: function(event) {
            if (this.hasScroll()) {
                // TODO merge with no-scroll-else
                if (!this.active || this.first()) {
                    this.activate(event, this.element.children(".ui-menu-item:last"));
                    return;
                }

                var base = this.active.offset().top,
                    height = this.element.height();
                result = this.element.children(".ui-menu-item").filter(function() {
                    var close = $(this).offset().top - base + height - $(this).height();
                    // TODO improve approximation
                    return close < 10 && close > -10;
                });

                // TODO try to catch this earlier when scrollTop indicates the last page anyway
                if (!result.length) {
                    result = this.element.children(".ui-menu-item:first");
                }
                this.activate(event, result);
            } else {
                this.activate(event, this.element.children(".ui-menu-item")
                    .filter(!this.active || this.first() ? ":last" : ":first"));
            }
        },

        hasScroll: function() {
            return this.element.height() < this.element[ $.fn.prop ? "prop" : "attr" ]("scrollHeight");
        },

        select: function( event ) {
            this._trigger("selected", event, { item: this.active });
        }
    });

}(jQuery));
/*
 * jQuery UI Datepicker 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Datepicker
 *
 * Depends:
 *	jquery.ui.core.js
 */
(function( $, undefined ) {

    $.extend($.ui, { datepicker: { version: "1.8.16" } });

    var PROP_NAME = 'datepicker';
    var dpuuid = new Date().getTime();
    var instActive;

    /* Date picker manager.
     Use the singleton instance of this class, $.datepicker, to interact with the date picker.
     Settings for (groups of) date pickers are maintained in an instance object,
     allowing multiple different settings on the same page. */

    function Datepicker() {
        this.debug = false; // Change this to true to start debugging
        this._curInst = null; // The current instance in use
        this._keyEvent = false; // If the last event was a key event
        this._disabledInputs = []; // List of date picker inputs that have been disabled
        this._datepickerShowing = false; // True if the popup picker is showing , false if not
        this._inDialog = false; // True if showing within a "dialog", false if not
        this._mainDivId = 'ui-datepicker-div'; // The ID of the main datepicker division
        this._inlineClass = 'ui-datepicker-inline'; // The name of the inline marker class
        this._appendClass = 'ui-datepicker-append'; // The name of the append marker class
        this._triggerClass = 'ui-datepicker-trigger'; // The name of the trigger marker class
        this._dialogClass = 'ui-datepicker-dialog'; // The name of the dialog marker class
        this._disableClass = 'ui-datepicker-disabled'; // The name of the disabled covering marker class
        this._unselectableClass = 'ui-datepicker-unselectable'; // The name of the unselectable cell marker class
        this._currentClass = 'ui-datepicker-current-day'; // The name of the current day marker class
        this._dayOverClass = 'ui-datepicker-days-cell-over'; // The name of the day hover marker class
        this.regional = []; // Available regional settings, indexed by language code
        this.regional[''] = { // Default regional settings
            closeText: 'Done', // Display text for close link
            prevText: 'Prev', // Display text for previous month link
            nextText: 'Next', // Display text for next month link
            currentText: 'Today', // Display text for current month link
            monthNames: ['January','February','March','April','May','June',
                'July','August','September','October','November','December'], // Names of months for drop-down and formatting
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // For formatting
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'], // For formatting
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'], // For formatting
            dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'], // Column headings for days starting at Sunday
            weekHeader: 'Wk', // Column header for week of the year
            dateFormat: 'mm/dd/yy', // See format options on parseDate
            firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
            isRTL: false, // True if right-to-left language, false if left-to-right
            showMonthAfterYear: false, // True if the year select precedes month, false for month then year
            yearSuffix: '' // Additional text to append to the year in the month headers
        };
        this._defaults = { // Global defaults for all the date picker instances
            showOn: 'focus', // 'focus' for popup on focus,
            // 'button' for trigger button, or 'both' for either
            showAnim: 'fadeIn', // Name of jQuery animation for popup
            showOptions: {}, // Options for enhanced animations
            defaultDate: null, // Used when field is blank: actual date,
            // +/-number for offset from today, null for today
            appendText: '', // Display text following the input box, e.g. showing the format
            buttonText: '...', // Text for trigger button
            buttonImage: '', // URL for trigger button image
            buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
            hideIfNoPrevNext: false, // True to hide next/previous month links
            // if not applicable, false to just disable them
            navigationAsDateFormat: false, // True if date formatting applied to prev/today/next links
            gotoCurrent: false, // True if today link goes back to current selection instead
            changeMonth: false, // True if month can be selected directly, false if only prev/next
            changeYear: false, // True if year can be selected directly, false if only prev/next
            yearRange: 'c-10:c+10', // Range of years to display in drop-down,
            // either relative to today's year (-nn:+nn), relative to currently displayed year
            // (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
            showOtherMonths: false, // True to show dates in other months, false to leave blank
            selectOtherMonths: false, // True to allow selection of dates in other months, false for unselectable
            showWeek: false, // True to show week of the year, false to not show it
            calculateWeek: this.iso8601Week, // How to calculate the week of the year,
            // takes a Date and returns the number of the week for it
            shortYearCutoff: '+10', // Short year values < this are in the current century,
            // > this are in the previous century,
            // string value starting with '+' for current year + value
            minDate: null, // The earliest selectable date, or null for no limit
            maxDate: null, // The latest selectable date, or null for no limit
            duration: 'fast', // Duration of display/closure
            beforeShowDay: null, // Function that takes a date and returns an array with
            // [0] = true if selectable, false if not, [1] = custom CSS class name(s) or '',
            // [2] = cell title (optional), e.g. $.datepicker.noWeekends
            beforeShow: null, // Function that takes an input field and
            // returns a set of custom settings for the date picker
            onSelect: null, // Define a callback function when a date is selected
            onChangeMonthYear: null, // Define a callback function when the month or year is changed
            onClose: null, // Define a callback function when the datepicker is closed
            numberOfMonths: 1, // Number of months to show at a time
            showCurrentAtPos: 0, // The position in multipe months at which to show the current month (starting at 0)
            stepMonths: 1, // Number of months to step back/forward
            stepBigMonths: 12, // Number of months to step back/forward for the big links
            altField: '', // Selector for an alternate field to store selected dates into
            altFormat: '', // The date format to use for the alternate field
            constrainInput: true, // The input is constrained by the current date format
            showButtonPanel: false, // True to show button panel, false to not show it
            autoSize: false, // True to size the input for the date format, false to leave as is
            disabled: false // The initial disabled state
        };
        $.extend(this._defaults, this.regional['']);
        this.dpDiv = bindHover($('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'));
    }

    $.extend(Datepicker.prototype, {
        /* Class name added to elements to indicate already configured with a date picker. */
        markerClassName: 'hasDatepicker',

        //Keep track of the maximum number of rows displayed (see #7043)
        maxRows: 4,

        /* Debug logging (if enabled). */
        log: function () {
            if (this.debug)
                console.log.apply('', arguments);
        },

        // TODO rename to "widget" when switching to widget factory
        _widgetDatepicker: function() {
            return this.dpDiv;
        },

        /* Override the default settings for all instances of the date picker.
         @param  settings  object - the new settings to use as defaults (anonymous object)
         @return the manager object */
        setDefaults: function(settings) {
            extendRemove(this._defaults, settings || {});
            return this;
        },

        /* Attach the date picker to a jQuery selection.
         @param  target    element - the target input field or division or span
         @param  settings  object - the new settings to use for this date picker instance (anonymous) */
        _attachDatepicker: function(target, settings) {
            // check for settings on the control itself - in namespace 'date:'
            var inlineSettings = null;
            for (var attrName in this._defaults) {
                var attrValue = target.getAttribute('date:' + attrName);
                if (attrValue) {
                    inlineSettings = inlineSettings || {};
                    try {
                        inlineSettings[attrName] = eval(attrValue);
                    } catch (err) {
                        inlineSettings[attrName] = attrValue;
                    }
                }
            }
            var nodeName = target.nodeName.toLowerCase();
            var inline = (nodeName == 'div' || nodeName == 'span');
            if (!target.id) {
                this.uuid += 1;
                target.id = 'dp' + this.uuid;
            }
            var inst = this._newInst($(target), inline);
            inst.settings = $.extend({}, settings || {}, inlineSettings || {});
            if (nodeName == 'input') {
                this._connectDatepicker(target, inst);
            } else if (inline) {
                this._inlineDatepicker(target, inst);
            }
        },

        /* Create a new instance object. */
        _newInst: function(target, inline) {
            var id = target[0].id.replace(/([^A-Za-z0-9_-])/g, '\\\\$1'); // escape jQuery meta chars
            return {id: id, input: target, // associated target
                selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
                drawMonth: 0, drawYear: 0, // month being drawn
                inline: inline, // is datepicker inline or not
                dpDiv: (!inline ? this.dpDiv : // presentation div
                    bindHover($('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')))};
        },

        /* Attach the date picker to an input field. */
        _connectDatepicker: function(target, inst) {
            var input = $(target);
            inst.append = $([]);
            inst.trigger = $([]);
            if (input.hasClass(this.markerClassName))
                return;
            this._attachments(input, inst);
            input.addClass(this.markerClassName).keydown(this._doKeyDown).
            keypress(this._doKeyPress).keyup(this._doKeyUp).
            bind("setData.datepicker", function(event, key, value) {
                inst.settings[key] = value;
            }).bind("getData.datepicker", function(event, key) {
                return this._get(inst, key);
            });
            this._autoSize(inst);
            $.data(target, PROP_NAME, inst);
            //If disabled option is true, disable the datepicker once it has been attached to the input (see ticket #5665)
            if( inst.settings.disabled ) {
                this._disableDatepicker( target );
            }
        },

        /* Make attachments based on settings. */
        _attachments: function(input, inst) {
            var appendText = this._get(inst, 'appendText');
            var isRTL = this._get(inst, 'isRTL');
            if (inst.append)
                inst.append.remove();
            if (appendText) {
                inst.append = $('<span class="' + this._appendClass + '">' + appendText + '</span>');
                input[isRTL ? 'before' : 'after'](inst.append);
            }
            input.unbind('focus', this._showDatepicker);
            if (inst.trigger)
                inst.trigger.remove();
            var showOn = this._get(inst, 'showOn');
            if (showOn == 'focus' || showOn == 'both') // pop-up date picker when in the marked field
                input.focus(this._showDatepicker);
            if (showOn == 'button' || showOn == 'both') { // pop-up date picker when button clicked
                var buttonText = this._get(inst, 'buttonText');
                var buttonImage = this._get(inst, 'buttonImage');
                inst.trigger = $(this._get(inst, 'buttonImageOnly') ?
                    $('<img/>').addClass(this._triggerClass).
                    attr({ src: buttonImage, alt: buttonText, title: buttonText }) :
                    $('<button type="button"></button>').addClass(this._triggerClass).
                    html(buttonImage == '' ? buttonText : $('<img/>').attr(
                        { src:buttonImage, alt:buttonText, title:buttonText })));
                input[isRTL ? 'before' : 'after'](inst.trigger);
                inst.trigger.click(function() {
                    if ($.datepicker._datepickerShowing && $.datepicker._lastInput == input[0])
                        $.datepicker._hideDatepicker();
                    else
                        $.datepicker._showDatepicker(input[0]);
                    return false;
                });
            }
        },

        /* Apply the maximum length for the date format. */
        _autoSize: function(inst) {
            if (this._get(inst, 'autoSize') && !inst.inline) {
                var date = new Date(2009, 12 - 1, 20); // Ensure double digits
                var dateFormat = this._get(inst, 'dateFormat');
                if (dateFormat.match(/[DM]/)) {
                    var findMax = function(names) {
                        var max = 0;
                        var maxI = 0;
                        for (var i = 0; i < names.length; i++) {
                            if (names[i].length > max) {
                                max = names[i].length;
                                maxI = i;
                            }
                        }
                        return maxI;
                    };
                    date.setMonth(findMax(this._get(inst, (dateFormat.match(/MM/) ?
                        'monthNames' : 'monthNamesShort'))));
                    date.setDate(findMax(this._get(inst, (dateFormat.match(/DD/) ?
                            'dayNames' : 'dayNamesShort'))) + 20 - date.getDay());
                }
                inst.input.attr('size', this._formatDate(inst, date).length);
            }
        },

        /* Attach an inline date picker to a div. */
        _inlineDatepicker: function(target, inst) {
            var divSpan = $(target);
            if (divSpan.hasClass(this.markerClassName))
                return;
            divSpan.addClass(this.markerClassName).append(inst.dpDiv).
            bind("setData.datepicker", function(event, key, value){
                inst.settings[key] = value;
            }).bind("getData.datepicker", function(event, key){
                return this._get(inst, key);
            });
            $.data(target, PROP_NAME, inst);
            this._setDate(inst, this._getDefaultDate(inst), true);
            this._updateDatepicker(inst);
            this._updateAlternate(inst);
            //If disabled option is true, disable the datepicker before showing it (see ticket #5665)
            if( inst.settings.disabled ) {
                this._disableDatepicker( target );
            }
            // Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
            // http://bugs.jqueryui.com/ticket/7552 - A Datepicker created on a detached div has zero height
            inst.dpDiv.css( "display", "block" );
        },

        /* Pop-up the date picker in a "dialog" box.
         @param  input     element - ignored
         @param  date      string or Date - the initial date to display
         @param  onSelect  function - the function to call when a date is selected
         @param  settings  object - update the dialog date picker instance's settings (anonymous object)
         @param  pos       int[2] - coordinates for the dialog's position within the screen or
         event - with x/y coordinates or
         leave empty for default (screen centre)
         @return the manager object */
        _dialogDatepicker: function(input, date, onSelect, settings, pos) {
            var inst = this._dialogInst; // internal instance
            if (!inst) {
                this.uuid += 1;
                var id = 'dp' + this.uuid;
                this._dialogInput = $('<input type="text" id="' + id +
                    '" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>');
                this._dialogInput.keydown(this._doKeyDown);
                $('body').append(this._dialogInput);
                inst = this._dialogInst = this._newInst(this._dialogInput, false);
                inst.settings = {};
                $.data(this._dialogInput[0], PROP_NAME, inst);
            }
            extendRemove(inst.settings, settings || {});
            date = (date && date.constructor == Date ? this._formatDate(inst, date) : date);
            this._dialogInput.val(date);

            this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null);
            if (!this._pos) {
                var browserWidth = document.documentElement.clientWidth;
                var browserHeight = document.documentElement.clientHeight;
                var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
                var scrollY = document.documentElement.scrollTop || document.body.scrollTop;
                this._pos = // should use actual width/height below
                    [(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY];
            }

            // move input on screen for focus, but hidden behind dialog
            this._dialogInput.css('left', (this._pos[0] + 20) + 'px').css('top', this._pos[1] + 'px');
            inst.settings.onSelect = onSelect;
            this._inDialog = true;
            this.dpDiv.addClass(this._dialogClass);
            this._showDatepicker(this._dialogInput[0]);
            if ($.blockUI)
                $.blockUI(this.dpDiv);
            $.data(this._dialogInput[0], PROP_NAME, inst);
            return this;
        },

        /* Detach a datepicker from its control.
         @param  target    element - the target input field or division or span */
        _destroyDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            $.removeData(target, PROP_NAME);
            if (nodeName == 'input') {
                inst.append.remove();
                inst.trigger.remove();
                $target.removeClass(this.markerClassName).
                unbind('focus', this._showDatepicker).
                unbind('keydown', this._doKeyDown).
                unbind('keypress', this._doKeyPress).
                unbind('keyup', this._doKeyUp);
            } else if (nodeName == 'div' || nodeName == 'span')
                $target.removeClass(this.markerClassName).empty();
        },

        /* Enable the date picker to a jQuery selection.
         @param  target    element - the target input field or division or span */
        _enableDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            if (nodeName == 'input') {
                target.disabled = false;
                inst.trigger.filter('button').
                each(function() { this.disabled = false; }).end().
                filter('img').css({opacity: '1.0', cursor: ''});
            }
            else if (nodeName == 'div' || nodeName == 'span') {
                var inline = $target.children('.' + this._inlineClass);
                inline.children().removeClass('ui-state-disabled');
                inline.find("select.ui-datepicker-month, select.ui-datepicker-year").
                removeAttr("disabled");
            }
            this._disabledInputs = $.map(this._disabledInputs,
                function(value) { return (value == target ? null : value); }); // delete entry
        },

        /* Disable the date picker to a jQuery selection.
         @param  target    element - the target input field or division or span */
        _disableDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            if (nodeName == 'input') {
                target.disabled = true;
                inst.trigger.filter('button').
                each(function() { this.disabled = true; }).end().
                filter('img').css({opacity: '0.5', cursor: 'default'});
            }
            else if (nodeName == 'div' || nodeName == 'span') {
                var inline = $target.children('.' + this._inlineClass);
                inline.children().addClass('ui-state-disabled');
                inline.find("select.ui-datepicker-month, select.ui-datepicker-year").
                attr("disabled", "disabled");
            }
            this._disabledInputs = $.map(this._disabledInputs,
                function(value) { return (value == target ? null : value); }); // delete entry
            this._disabledInputs[this._disabledInputs.length] = target;
        },

        /* Is the first field in a jQuery collection disabled as a datepicker?
         @param  target    element - the target input field or division or span
         @return boolean - true if disabled, false if enabled */
        _isDisabledDatepicker: function(target) {
            if (!target) {
                return false;
            }
            for (var i = 0; i < this._disabledInputs.length; i++) {
                if (this._disabledInputs[i] == target)
                    return true;
            }
            return false;
        },

        /* Retrieve the instance data for the target control.
         @param  target  element - the target input field or division or span
         @return  object - the associated instance data
         @throws  error if a jQuery problem getting data */
        _getInst: function(target) {
            try {
                return $.data(target, PROP_NAME);
            }
            catch (err) {
                throw 'Missing instance data for this datepicker';
            }
        },

        /* Update or retrieve the settings for a date picker attached to an input field or division.
         @param  target  element - the target input field or division or span
         @param  name    object - the new settings to update or
         string - the name of the setting to change or retrieve,
         when retrieving also 'all' for all instance settings or
         'defaults' for all global defaults
         @param  value   any - the new value for the setting
         (omit if above is an object or to retrieve a value) */
        _optionDatepicker: function(target, name, value) {
            var inst = this._getInst(target);
            if (arguments.length == 2 && typeof name == 'string') {
                return (name == 'defaults' ? $.extend({}, $.datepicker._defaults) :
                    (inst ? (name == 'all' ? $.extend({}, inst.settings) :
                        this._get(inst, name)) : null));
            }
            var settings = name || {};
            if (typeof name == 'string') {
                settings = {};
                settings[name] = value;
            }
            if (inst) {
                if (this._curInst == inst) {
                    this._hideDatepicker();
                }
                var date = this._getDateDatepicker(target, true);
                var minDate = this._getMinMaxDate(inst, 'min');
                var maxDate = this._getMinMaxDate(inst, 'max');
                extendRemove(inst.settings, settings);
                // reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
                if (minDate !== null && settings['dateFormat'] !== undefined && settings['minDate'] === undefined)
                    inst.settings.minDate = this._formatDate(inst, minDate);
                if (maxDate !== null && settings['dateFormat'] !== undefined && settings['maxDate'] === undefined)
                    inst.settings.maxDate = this._formatDate(inst, maxDate);
                this._attachments($(target), inst);
                this._autoSize(inst);
                this._setDate(inst, date);
                this._updateAlternate(inst);
                this._updateDatepicker(inst);
            }
        },

        // change method deprecated
        _changeDatepicker: function(target, name, value) {
            this._optionDatepicker(target, name, value);
        },

        /* Redraw the date picker attached to an input field or division.
         @param  target  element - the target input field or division or span */
        _refreshDatepicker: function(target) {
            var inst = this._getInst(target);
            if (inst) {
                this._updateDatepicker(inst);
            }
        },

        /* Set the dates for a jQuery selection.
         @param  target   element - the target input field or division or span
         @param  date     Date - the new date */
        _setDateDatepicker: function(target, date) {
            var inst = this._getInst(target);
            if (inst) {
                this._setDate(inst, date);
                this._updateDatepicker(inst);
                this._updateAlternate(inst);
            }
        },

        /* Get the date(s) for the first entry in a jQuery selection.
         @param  target     element - the target input field or division or span
         @param  noDefault  boolean - true if no default date is to be used
         @return Date - the current date */
        _getDateDatepicker: function(target, noDefault) {
            var inst = this._getInst(target);
            if (inst && !inst.inline)
                this._setDateFromField(inst, noDefault);
            return (inst ? this._getDate(inst) : null);
        },

        /* Handle keystrokes. */
        _doKeyDown: function(event) {
            var inst = $.datepicker._getInst(event.target);
            var handled = true;
            var isRTL = inst.dpDiv.is('.ui-datepicker-rtl');
            inst._keyEvent = true;
            if ($.datepicker._datepickerShowing)
                switch (event.keyCode) {
                    case 9: $.datepicker._hideDatepicker();
                        handled = false;
                        break; // hide on tab out
                    case 13: var sel = $('td.' + $.datepicker._dayOverClass + ':not(.' +
                        $.datepicker._currentClass + ')', inst.dpDiv);
                        if (sel[0])
                            $.datepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]);
                        var onSelect = $.datepicker._get(inst, 'onSelect');
                        if (onSelect) {
                            var dateStr = $.datepicker._formatDate(inst);

                            // trigger custom callback
                            onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);
                        }
                        else
                            $.datepicker._hideDatepicker();
                        return false; // don't submit the form
                        break; // select the value on enter
                    case 27: $.datepicker._hideDatepicker();
                        break; // hide on escape
                    case 33: $.datepicker._adjustDate(event.target, (event.ctrlKey ?
                        -$.datepicker._get(inst, 'stepBigMonths') :
                        -$.datepicker._get(inst, 'stepMonths')), 'M');
                        break; // previous month/year on page up/+ ctrl
                    case 34: $.datepicker._adjustDate(event.target, (event.ctrlKey ?
                        +$.datepicker._get(inst, 'stepBigMonths') :
                        +$.datepicker._get(inst, 'stepMonths')), 'M');
                        break; // next month/year on page down/+ ctrl
                    case 35: if (event.ctrlKey || event.metaKey) $.datepicker._clearDate(event.target);
                        handled = event.ctrlKey || event.metaKey;
                        break; // clear on ctrl or command +end
                    case 36: if (event.ctrlKey || event.metaKey) $.datepicker._gotoToday(event.target);
                        handled = event.ctrlKey || event.metaKey;
                        break; // current on ctrl or command +home
                    case 37: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, (isRTL ? +1 : -1), 'D');
                        handled = event.ctrlKey || event.metaKey;
                        // -1 day on ctrl or command +left
                        if (event.originalEvent.altKey) $.datepicker._adjustDate(event.target, (event.ctrlKey ?
                            -$.datepicker._get(inst, 'stepBigMonths') :
                            -$.datepicker._get(inst, 'stepMonths')), 'M');
                        // next month/year on alt +left on Mac
                        break;
                    case 38: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, -7, 'D');
                        handled = event.ctrlKey || event.metaKey;
                        break; // -1 week on ctrl or command +up
                    case 39: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, (isRTL ? -1 : +1), 'D');
                        handled = event.ctrlKey || event.metaKey;
                        // +1 day on ctrl or command +right
                        if (event.originalEvent.altKey) $.datepicker._adjustDate(event.target, (event.ctrlKey ?
                            +$.datepicker._get(inst, 'stepBigMonths') :
                            +$.datepicker._get(inst, 'stepMonths')), 'M');
                        // next month/year on alt +right
                        break;
                    case 40: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, +7, 'D');
                        handled = event.ctrlKey || event.metaKey;
                        break; // +1 week on ctrl or command +down
                    default: handled = false;
                }
            else if (event.keyCode == 36 && event.ctrlKey) // display the date picker on ctrl+home
                $.datepicker._showDatepicker(this);
            else {
                handled = false;
            }
            if (handled) {
                event.preventDefault();
                event.stopPropagation();
            }
        },

        /* Filter entered characters - based on date format. */
        _doKeyPress: function(event) {
            var inst = $.datepicker._getInst(event.target);
            if ($.datepicker._get(inst, 'constrainInput')) {
                var chars = $.datepicker._possibleChars($.datepicker._get(inst, 'dateFormat'));
                var chr = String.fromCharCode(event.charCode == undefined ? event.keyCode : event.charCode);
                return event.ctrlKey || event.metaKey || (chr < ' ' || !chars || chars.indexOf(chr) > -1);
            }
        },

        /* Synchronise manual entry and field/alternate field. */
        _doKeyUp: function(event) {
            var inst = $.datepicker._getInst(event.target);
            if (inst.input.val() != inst.lastVal) {
                try {
                    var date = $.datepicker.parseDate($.datepicker._get(inst, 'dateFormat'),
                        (inst.input ? inst.input.val() : null),
                        $.datepicker._getFormatConfig(inst));
                    if (date) { // only if valid
                        $.datepicker._setDateFromField(inst);
                        $.datepicker._updateAlternate(inst);
                        $.datepicker._updateDatepicker(inst);
                    }
                }
                catch (event) {
                    $.datepicker.log(event);
                }
            }
            return true;
        },

        /* Pop-up the date picker for a given input field.
         If false returned from beforeShow event handler do not show.
         @param  input  element - the input field attached to the date picker or
         event - if triggered by focus */
        _showDatepicker: function(input) {
            input = input.target || input;
            if (input.nodeName.toLowerCase() != 'input') // find from button/image trigger
                input = $('input', input.parentNode)[0];
            if ($.datepicker._isDisabledDatepicker(input) || $.datepicker._lastInput == input) // already here
                return;
            var inst = $.datepicker._getInst(input);
            if ($.datepicker._curInst && $.datepicker._curInst != inst) {
                if ( $.datepicker._datepickerShowing ) {
                    $.datepicker._triggerOnClose($.datepicker._curInst);
                }
                $.datepicker._curInst.dpDiv.stop(true, true);
            }
            var beforeShow = $.datepicker._get(inst, 'beforeShow');
            var beforeShowSettings = beforeShow ? beforeShow.apply(input, [input, inst]) : {};
            if(beforeShowSettings === false){
                //false
                return;
            }
            extendRemove(inst.settings, beforeShowSettings);
            inst.lastVal = null;
            $.datepicker._lastInput = input;
            $.datepicker._setDateFromField(inst);
            if ($.datepicker._inDialog) // hide cursor
                input.value = '';
            if (!$.datepicker._pos) { // position below input
                $.datepicker._pos = $.datepicker._findPos(input);
                $.datepicker._pos[1] += input.offsetHeight; // add the height
            }
            var isFixed = false;
            $(input).parents().each(function() {
                isFixed |= $(this).css('position') == 'fixed';
                return !isFixed;
            });
            if (isFixed && $.browser.opera) { // correction for Opera when fixed and scrolled
                $.datepicker._pos[0] -= document.documentElement.scrollLeft;
                $.datepicker._pos[1] -= document.documentElement.scrollTop;
            }
            var offset = {left: $.datepicker._pos[0], top: $.datepicker._pos[1]};
            $.datepicker._pos = null;
            //to avoid flashes on Firefox
            inst.dpDiv.empty();
            // determine sizing offscreen
            inst.dpDiv.css({position: 'absolute', display: 'block', top: '-1000px'});
            $.datepicker._updateDatepicker(inst);
            // fix width for dynamic number of date pickers
            // and adjust position before showing
            offset = $.datepicker._checkOffset(inst, offset, isFixed);
            inst.dpDiv.css({position: ($.datepicker._inDialog && $.blockUI ?
                'static' : (isFixed ? 'fixed' : 'absolute')), display: 'none',
                left: offset.left + 'px', top: offset.top + 'px'});
            if (!inst.inline) {
                var showAnim = $.datepicker._get(inst, 'showAnim');
                var duration = $.datepicker._get(inst, 'duration');
                var postProcess = function() {
                    var cover = inst.dpDiv.find('iframe.ui-datepicker-cover'); // IE6- only
                    if( !! cover.length ){
                        var borders = $.datepicker._getBorders(inst.dpDiv);
                        cover.css({left: -borders[0], top: -borders[1],
                            width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()});
                    }
                };
                inst.dpDiv.zIndex($(input).zIndex()+1);
                $.datepicker._datepickerShowing = true;
                if ($.effects && $.effects[showAnim])
                    inst.dpDiv.show(showAnim, $.datepicker._get(inst, 'showOptions'), duration, postProcess);
                else
                    inst.dpDiv[showAnim || 'show']((showAnim ? duration : null), postProcess);
                if (!showAnim || !duration)
                    postProcess();
                if (inst.input.is(':visible') && !inst.input.is(':disabled'))
                    inst.input.focus();
                $.datepicker._curInst = inst;
            }
        },

        /* Generate the date picker content. */
        _updateDatepicker: function(inst) {
            var self = this;
            self.maxRows = 4; //Reset the max number of rows being displayed (see #7043)
            var borders = $.datepicker._getBorders(inst.dpDiv);
            instActive = inst; // for delegate hover events
            inst.dpDiv.empty().append(this._generateHTML(inst));
            var cover = inst.dpDiv.find('iframe.ui-datepicker-cover'); // IE6- only
            if( !!cover.length ){ //avoid call to outerXXXX() when not in IE6
                cover.css({left: -borders[0], top: -borders[1], width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()})
            }
            inst.dpDiv.find('.' + this._dayOverClass + ' a').mouseover();
            var numMonths = this._getNumberOfMonths(inst);
            var cols = numMonths[1];
            var width = 17;
            inst.dpDiv.removeClass('ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4').width('');
            if (cols > 1)
                inst.dpDiv.addClass('ui-datepicker-multi-' + cols).css('width', (width * cols) + 'em');
            inst.dpDiv[(numMonths[0] != 1 || numMonths[1] != 1 ? 'add' : 'remove') +
            'Class']('ui-datepicker-multi');
            inst.dpDiv[(this._get(inst, 'isRTL') ? 'add' : 'remove') +
            'Class']('ui-datepicker-rtl');
            if (inst == $.datepicker._curInst && $.datepicker._datepickerShowing && inst.input &&
                // #6694 - don't focus the input if it's already focused
                // this breaks the change event in IE
                inst.input.is(':visible') && !inst.input.is(':disabled') && inst.input[0] != document.activeElement)
                inst.input.focus();
            // deffered render of the years select (to avoid flashes on Firefox)
            if( inst.yearshtml ){
                var origyearshtml = inst.yearshtml;
                setTimeout(function(){
                    //assure that inst.yearshtml didn't change.
                    if( origyearshtml === inst.yearshtml && inst.yearshtml ){
                        inst.dpDiv.find('select.ui-datepicker-year:first').replaceWith(inst.yearshtml);
                    }
                    origyearshtml = inst.yearshtml = null;
                }, 0);
            }
        },

        /* Retrieve the size of left and top borders for an element.
         @param  elem  (jQuery object) the element of interest
         @return  (number[2]) the left and top borders */
        _getBorders: function(elem) {
            var convert = function(value) {
                return {thin: 1, medium: 2, thick: 3}[value] || value;
            };
            return [parseFloat(convert(elem.css('border-left-width'))),
                parseFloat(convert(elem.css('border-top-width')))];
        },

        /* Check positioning to remain on screen. */
        _checkOffset: function(inst, offset, isFixed) {
            var dpWidth = inst.dpDiv.outerWidth();
            var dpHeight = inst.dpDiv.outerHeight();
            var inputWidth = inst.input ? inst.input.outerWidth() : 0;
            var inputHeight = inst.input ? inst.input.outerHeight() : 0;
            var viewWidth = document.documentElement.clientWidth + $(document).scrollLeft();
            var viewHeight = document.documentElement.clientHeight + $(document).scrollTop();

            offset.left -= (this._get(inst, 'isRTL') ? (dpWidth - inputWidth) : 0);
            offset.left -= (isFixed && offset.left == inst.input.offset().left) ? $(document).scrollLeft() : 0;
            offset.top -= (isFixed && offset.top == (inst.input.offset().top + inputHeight)) ? $(document).scrollTop() : 0;

            // now check if datepicker is showing outside window viewport - move to a better place if so.
            offset.left -= Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
                Math.abs(offset.left + dpWidth - viewWidth) : 0);
            offset.top -= Math.min(offset.top, (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
                Math.abs(dpHeight + inputHeight) : 0);

            return offset;
        },

        /* Find an object's position on the screen. */
        _findPos: function(obj) {
            var inst = this._getInst(obj);
            var isRTL = this._get(inst, 'isRTL');
            while (obj && (obj.type == 'hidden' || obj.nodeType != 1 || $.expr.filters.hidden(obj))) {
                obj = obj[isRTL ? 'previousSibling' : 'nextSibling'];
            }
            var position = $(obj).offset();
            return [position.left, position.top];
        },

        /* Trigger custom callback of onClose. */
        _triggerOnClose: function(inst) {
            var onClose = this._get(inst, 'onClose');
            if (onClose)
                onClose.apply((inst.input ? inst.input[0] : null),
                    [(inst.input ? inst.input.val() : ''), inst]);
        },

        /* Hide the date picker from view.
         @param  input  element - the input field attached to the date picker */
        _hideDatepicker: function(input) {
            var inst = this._curInst;
            if (!inst || (input && inst != $.data(input, PROP_NAME)))
                return;
            if (this._datepickerShowing) {
                var showAnim = this._get(inst, 'showAnim');
                var duration = this._get(inst, 'duration');
                var postProcess = function() {
                    $.datepicker._tidyDialog(inst);
                    this._curInst = null;
                };
                if ($.effects && $.effects[showAnim])
                    inst.dpDiv.hide(showAnim, $.datepicker._get(inst, 'showOptions'), duration, postProcess);
                else
                    inst.dpDiv[(showAnim == 'slideDown' ? 'slideUp' :
                        (showAnim == 'fadeIn' ? 'fadeOut' : 'hide'))]((showAnim ? duration : null), postProcess);
                if (!showAnim)
                    postProcess();
                $.datepicker._triggerOnClose(inst);
                this._datepickerShowing = false;
                this._lastInput = null;
                if (this._inDialog) {
                    this._dialogInput.css({ position: 'absolute', left: '0', top: '-100px' });
                    if ($.blockUI) {
                        $.unblockUI();
                        $('body').append(this.dpDiv);
                    }
                }
                this._inDialog = false;
            }
        },

        /* Tidy up after a dialog display. */
        _tidyDialog: function(inst) {
            inst.dpDiv.removeClass(this._dialogClass).unbind('.ui-datepicker-calendar');
        },

        /* Close date picker if clicked elsewhere. */
        _checkExternalClick: function(event) {
            if (!$.datepicker._curInst)
                return;
            var $target = $(event.target);
            if ($target[0].id != $.datepicker._mainDivId &&
                $target.parents('#' + $.datepicker._mainDivId).length == 0 &&
                !$target.hasClass($.datepicker.markerClassName) &&
                !$target.hasClass($.datepicker._triggerClass) &&
                $.datepicker._datepickerShowing && !($.datepicker._inDialog && $.blockUI))
                $.datepicker._hideDatepicker();
        },

        /* Adjust one of the date sub-fields. */
        _adjustDate: function(id, offset, period) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (this._isDisabledDatepicker(target[0])) {
                return;
            }
            this._adjustInstDate(inst, offset +
                (period == 'M' ? this._get(inst, 'showCurrentAtPos') : 0), // undo positioning
                period);
            this._updateDatepicker(inst);
        },

        /* Action for current link. */
        _gotoToday: function(id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
                inst.selectedDay = inst.currentDay;
                inst.drawMonth = inst.selectedMonth = inst.currentMonth;
                inst.drawYear = inst.selectedYear = inst.currentYear;
            }
            else {
                var date = new Date();
                inst.selectedDay = date.getDate();
                inst.drawMonth = inst.selectedMonth = date.getMonth();
                inst.drawYear = inst.selectedYear = date.getFullYear();
            }
            this._notifyChange(inst);
            this._adjustDate(target);
        },

        /* Action for selecting a new month/year. */
        _selectMonthYear: function(id, select, period) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            inst['selected' + (period == 'M' ? 'Month' : 'Year')] =
                inst['draw' + (period == 'M' ? 'Month' : 'Year')] =
                    parseInt(select.options[select.selectedIndex].value,10);
            this._notifyChange(inst);
            this._adjustDate(target);
        },

        /* Action for selecting a day. */
        _selectDay: function(id, month, year, td) {
            var target = $(id);
            if ($(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) {
                return;
            }
            var inst = this._getInst(target[0]);
            inst.selectedDay = inst.currentDay = $('a', td).html();
            inst.selectedMonth = inst.currentMonth = month;
            inst.selectedYear = inst.currentYear = year;
            this._selectDate(id, this._formatDate(inst,
                inst.currentDay, inst.currentMonth, inst.currentYear));
        },

        /* Erase the input field and hide the date picker. */
        _clearDate: function(id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            this._selectDate(target, '');
        },

        /* Update the input field with the selected date. */
        _selectDate: function(id, dateStr) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            dateStr = (dateStr != null ? dateStr : this._formatDate(inst));
            if (inst.input)
                inst.input.val(dateStr);
            this._updateAlternate(inst);
            var onSelect = this._get(inst, 'onSelect');
            if (onSelect)
                onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
            else if (inst.input)
                inst.input.trigger('change'); // fire the change event
            if (inst.inline)
                this._updateDatepicker(inst);
            else {
                this._hideDatepicker();
                this._lastInput = inst.input[0];
                if (typeof(inst.input[0]) != 'object')
                    inst.input.focus(); // restore focus
                this._lastInput = null;
            }
        },

        /* Update any alternate field to synchronise with the main field. */
        _updateAlternate: function(inst) {
            var altField = this._get(inst, 'altField');
            if (altField) { // update alternate field too
                var altFormat = this._get(inst, 'altFormat') || this._get(inst, 'dateFormat');
                var date = this._getDate(inst);
                var dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst));
                $(altField).each(function() { $(this).val(dateStr); });
            }
        },

        /* Set as beforeShowDay function to prevent selection of weekends.
         @param  date  Date - the date to customise
         @return [boolean, string] - is this date selectable?, what is its CSS class? */
        noWeekends: function(date) {
            var day = date.getDay();
            return [(day > 0 && day < 6), ''];
        },

        /* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
         @param  date  Date - the date to get the week for
         @return  number - the number of the week within the year that contains this date */
        iso8601Week: function(date) {
            var checkDate = new Date(date.getTime());
            // Find Thursday of this week starting on Monday
            checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
            var time = checkDate.getTime();
            checkDate.setMonth(0); // Compare with Jan 1
            checkDate.setDate(1);
            return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
        },

        /* Parse a string value into a date object.
         See formatDate below for the possible formats.

         @param  format    string - the expected format of the date
         @param  value     string - the date in the above format
         @param  settings  Object - attributes include:
         shortYearCutoff  number - the cutoff year for determining the century (optional)
         dayNamesShort    string[7] - abbreviated names of the days from Sunday (optional)
         dayNames         string[7] - names of the days from Sunday (optional)
         monthNamesShort  string[12] - abbreviated names of the months (optional)
         monthNames       string[12] - names of the months (optional)
         @return  Date - the extracted date value or null if value is blank */
        parseDate: function (format, value, settings) {
            if (format == null || value == null)
                throw 'Invalid arguments';
            value = (typeof value == 'object' ? value.toString() : value + '');
            if (value == '')
                return null;
            var shortYearCutoff = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff;
            shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
            new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
            var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort;
            var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames;
            var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort;
            var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames;
            var year = -1;
            var month = -1;
            var day = -1;
            var doy = -1;
            var literal = false;
            // Check whether a format character is doubled
            var lookAhead = function(match) {
                var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
                if (matches)
                    iFormat++;
                return matches;
            };
            // Extract a number from the string value
            var getNumber = function(match) {
                var isDoubled = lookAhead(match);
                var size = (match == '@' ? 14 : (match == '!' ? 20 :
                    (match == 'y' && isDoubled ? 4 : (match == 'o' ? 3 : 2))));
                var digits = new RegExp('^\\d{1,' + size + '}');
                var num = value.substring(iValue).match(digits);
                if (!num)
                    throw 'Missing number at position ' + iValue;
                iValue += num[0].length;
                return parseInt(num[0], 10);
            };
            // Extract a name from the string value and convert to an index
            var getName = function(match, shortNames, longNames) {
                var names = $.map(lookAhead(match) ? longNames : shortNames, function (v, k) {
                    return [ [k, v] ];
                }).sort(function (a, b) {
                    return -(a[1].length - b[1].length);
                });
                var index = -1;
                $.each(names, function (i, pair) {
                    var name = pair[1];
                    if (value.substr(iValue, name.length).toLowerCase() == name.toLowerCase()) {
                        index = pair[0];
                        iValue += name.length;
                        return false;
                    }
                });
                if (index != -1)
                    return index + 1;
                else
                    throw 'Unknown name at position ' + iValue;
            };
            // Confirm that a literal character matches the string value
            var checkLiteral = function() {
                if (value.charAt(iValue) != format.charAt(iFormat))
                    throw 'Unexpected literal at position ' + iValue;
                iValue++;
            };
            var iValue = 0;
            for (var iFormat = 0; iFormat < format.length; iFormat++) {
                if (literal)
                    if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                        literal = false;
                    else
                        checkLiteral();
                else
                    switch (format.charAt(iFormat)) {
                        case 'd':
                            day = getNumber('d');
                            break;
                        case 'D':
                            getName('D', dayNamesShort, dayNames);
                            break;
                        case 'o':
                            doy = getNumber('o');
                            break;
                        case 'm':
                            month = getNumber('m');
                            break;
                        case 'M':
                            month = getName('M', monthNamesShort, monthNames);
                            break;
                        case 'y':
                            year = getNumber('y');
                            break;
                        case '@':
                            var date = new Date(getNumber('@'));
                            year = date.getFullYear();
                            month = date.getMonth() + 1;
                            day = date.getDate();
                            break;
                        case '!':
                            var date = new Date((getNumber('!') - this._ticksTo1970) / 10000);
                            year = date.getFullYear();
                            month = date.getMonth() + 1;
                            day = date.getDate();
                            break;
                        case "'":
                            if (lookAhead("'"))
                                checkLiteral();
                            else
                                literal = true;
                            break;
                        default:
                            checkLiteral();
                    }
            }
            if (iValue < value.length){
                throw "Extra/unparsed characters found in date: " + value.substring(iValue);
            }
            if (year == -1)
                year = new Date().getFullYear();
            else if (year < 100)
                year += new Date().getFullYear() - new Date().getFullYear() % 100 +
                    (year <= shortYearCutoff ? 0 : -100);
            if (doy > -1) {
                month = 1;
                day = doy;
                do {
                    var dim = this._getDaysInMonth(year, month - 1);
                    if (day <= dim)
                        break;
                    month++;
                    day -= dim;
                } while (true);
            }
            var date = this._daylightSavingAdjust(new Date(year, month - 1, day));
            if (date.getFullYear() != year || date.getMonth() + 1 != month || date.getDate() != day)
                throw 'Invalid date'; // E.g. 31/02/00
            return date;
        },

        /* Standard date formats. */
        ATOM: 'yy-mm-dd', // RFC 3339 (ISO 8601)
        COOKIE: 'D, dd M yy',
        ISO_8601: 'yy-mm-dd',
        RFC_822: 'D, d M y',
        RFC_850: 'DD, dd-M-y',
        RFC_1036: 'D, d M y',
        RFC_1123: 'D, d M yy',
        RFC_2822: 'D, d M yy',
        RSS: 'D, d M y', // RFC 822
        TICKS: '!',
        TIMESTAMP: '@',
        W3C: 'yy-mm-dd', // ISO 8601

        _ticksTo1970: (((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) +
        Math.floor(1970 / 400)) * 24 * 60 * 60 * 10000000),

        /* Format a date object into a string value.
         The format can be combinations of the following:
         d  - day of month (no leading zero)
         dd - day of month (two digit)
         o  - day of year (no leading zeros)
         oo - day of year (three digit)
         D  - day name short
         DD - day name long
         m  - month of year (no leading zero)
         mm - month of year (two digit)
         M  - month name short
         MM - month name long
         y  - year (two digit)
         yy - year (four digit)
         @ - Unix timestamp (ms since 01/01/1970)
         ! - Windows ticks (100ns since 01/01/0001)
         '...' - literal text
         '' - single quote

         @param  format    string - the desired format of the date
         @param  date      Date - the date value to format
         @param  settings  Object - attributes include:
         dayNamesShort    string[7] - abbreviated names of the days from Sunday (optional)
         dayNames         string[7] - names of the days from Sunday (optional)
         monthNamesShort  string[12] - abbreviated names of the months (optional)
         monthNames       string[12] - names of the months (optional)
         @return  string - the date in the above format */
        formatDate: function (format, date, settings) {
            if (!date)
                return '';
            var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort;
            var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames;
            var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort;
            var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames;
            // Check whether a format character is doubled
            var lookAhead = function(match) {
                var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
                if (matches)
                    iFormat++;
                return matches;
            };
            // Format a number, with leading zero if necessary
            var formatNumber = function(match, value, len) {
                var num = '' + value;
                if (lookAhead(match))
                    while (num.length < len)
                        num = '0' + num;
                return num;
            };
            // Format a name, short or long as requested
            var formatName = function(match, value, shortNames, longNames) {
                return (lookAhead(match) ? longNames[value] : shortNames[value]);
            };
            var output = '';
            var literal = false;
            if (date)
                for (var iFormat = 0; iFormat < format.length; iFormat++) {
                    if (literal)
                        if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                            literal = false;
                        else
                            output += format.charAt(iFormat);
                    else
                        switch (format.charAt(iFormat)) {
                            case 'd':
                                output += formatNumber('d', date.getDate(), 2);
                                break;
                            case 'D':
                                output += formatName('D', date.getDay(), dayNamesShort, dayNames);
                                break;
                            case 'o':
                                output += formatNumber('o',
                                    Math.round((new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - new Date(date.getFullYear(), 0, 0).getTime()) / 86400000), 3);
                                break;
                            case 'm':
                                output += formatNumber('m', date.getMonth() + 1, 2);
                                break;
                            case 'M':
                                output += formatName('M', date.getMonth(), monthNamesShort, monthNames);
                                break;
                            case 'y':
                                output += (lookAhead('y') ? date.getFullYear() :
                                (date.getYear() % 100 < 10 ? '0' : '') + date.getYear() % 100);
                                break;
                            case '@':
                                output += date.getTime();
                                break;
                            case '!':
                                output += date.getTime() * 10000 + this._ticksTo1970;
                                break;
                            case "'":
                                if (lookAhead("'"))
                                    output += "'";
                                else
                                    literal = true;
                                break;
                            default:
                                output += format.charAt(iFormat);
                        }
                }
            return output;
        },

        /* Extract all possible characters from the date format. */
        _possibleChars: function (format) {
            var chars = '';
            var literal = false;
            // Check whether a format character is doubled
            var lookAhead = function(match) {
                var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
                if (matches)
                    iFormat++;
                return matches;
            };
            for (var iFormat = 0; iFormat < format.length; iFormat++)
                if (literal)
                    if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                        literal = false;
                    else
                        chars += format.charAt(iFormat);
                else
                    switch (format.charAt(iFormat)) {
                        case 'd': case 'm': case 'y': case '@':
                        chars += '0123456789';
                        break;
                        case 'D': case 'M':
                        return null; // Accept anything
                        case "'":
                            if (lookAhead("'"))
                                chars += "'";
                            else
                                literal = true;
                            break;
                        default:
                            chars += format.charAt(iFormat);
                    }
            return chars;
        },

        /* Get a setting value, defaulting if necessary. */
        _get: function(inst, name) {
            return inst.settings[name] !== undefined ?
                inst.settings[name] : this._defaults[name];
        },

        /* Parse existing date and initialise date picker. */
        _setDateFromField: function(inst, noDefault) {
            if (inst.input.val() == inst.lastVal) {
                return;
            }
            var dateFormat = this._get(inst, 'dateFormat');
            var dates = inst.lastVal = inst.input ? inst.input.val() : null;
            var date, defaultDate;
            date = defaultDate = this._getDefaultDate(inst);
            var settings = this._getFormatConfig(inst);
            try {
                date = this.parseDate(dateFormat, dates, settings) || defaultDate;
            } catch (event) {
                this.log(event);
                dates = (noDefault ? '' : dates);
            }
            inst.selectedDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = date.getFullYear();
            inst.currentDay = (dates ? date.getDate() : 0);
            inst.currentMonth = (dates ? date.getMonth() : 0);
            inst.currentYear = (dates ? date.getFullYear() : 0);
            this._adjustInstDate(inst);
        },

        /* Retrieve the default date shown on opening. */
        _getDefaultDate: function(inst) {
            return this._restrictMinMax(inst,
                this._determineDate(inst, this._get(inst, 'defaultDate'), new Date()));
        },

        /* A date may be specified as an exact value or a relative one. */
        _determineDate: function(inst, date, defaultDate) {
            var offsetNumeric = function(offset) {
                var date = new Date();
                date.setDate(date.getDate() + offset);
                return date;
            };
            var offsetString = function(offset) {
                try {
                    return $.datepicker.parseDate($.datepicker._get(inst, 'dateFormat'),
                        offset, $.datepicker._getFormatConfig(inst));
                }
                catch (e) {
                    // Ignore
                }
                var date = (offset.toLowerCase().match(/^c/) ?
                        $.datepicker._getDate(inst) : null) || new Date();
                var year = date.getFullYear();
                var month = date.getMonth();
                var day = date.getDate();
                var pattern = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g;
                var matches = pattern.exec(offset);
                while (matches) {
                    switch (matches[2] || 'd') {
                        case 'd' : case 'D' :
                        day += parseInt(matches[1],10); break;
                        case 'w' : case 'W' :
                        day += parseInt(matches[1],10) * 7; break;
                        case 'm' : case 'M' :
                        month += parseInt(matches[1],10);
                        day = Math.min(day, $.datepicker._getDaysInMonth(year, month));
                        break;
                        case 'y': case 'Y' :
                        year += parseInt(matches[1],10);
                        day = Math.min(day, $.datepicker._getDaysInMonth(year, month));
                        break;
                    }
                    matches = pattern.exec(offset);
                }
                return new Date(year, month, day);
            };
            var newDate = (date == null || date === '' ? defaultDate : (typeof date == 'string' ? offsetString(date) :
                (typeof date == 'number' ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : new Date(date.getTime()))));
            newDate = (newDate && newDate.toString() == 'Invalid Date' ? defaultDate : newDate);
            if (newDate) {
                newDate.setHours(0);
                newDate.setMinutes(0);
                newDate.setSeconds(0);
                newDate.setMilliseconds(0);
            }
            return this._daylightSavingAdjust(newDate);
        },

        /* Handle switch to/from daylight saving.
         Hours may be non-zero on daylight saving cut-over:
         > 12 when midnight changeover, but then cannot generate
         midnight datetime, so jump to 1AM, otherwise reset.
         @param  date  (Date) the date to check
         @return  (Date) the corrected date */
        _daylightSavingAdjust: function(date) {
            if (!date) return null;
            date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0);
            return date;
        },

        /* Set the date(s) directly. */
        _setDate: function(inst, date, noChange) {
            var clear = !date;
            var origMonth = inst.selectedMonth;
            var origYear = inst.selectedYear;
            var newDate = this._restrictMinMax(inst, this._determineDate(inst, date, new Date()));
            inst.selectedDay = inst.currentDay = newDate.getDate();
            inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth();
            inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear();
            if ((origMonth != inst.selectedMonth || origYear != inst.selectedYear) && !noChange)
                this._notifyChange(inst);
            this._adjustInstDate(inst);
            if (inst.input) {
                inst.input.val(clear ? '' : this._formatDate(inst));
            }
        },

        /* Retrieve the date(s) directly. */
        _getDate: function(inst) {
            var startDate = (!inst.currentYear || (inst.input && inst.input.val() == '') ? null :
                this._daylightSavingAdjust(new Date(
                    inst.currentYear, inst.currentMonth, inst.currentDay)));
            return startDate;
        },

        /* Generate the HTML for the current state of the date picker. */
        _generateHTML: function(inst) {
            var today = new Date();
            today = this._daylightSavingAdjust(
                new Date(today.getFullYear(), today.getMonth(), today.getDate())); // clear time
            var isRTL = this._get(inst, 'isRTL');
            var showButtonPanel = this._get(inst, 'showButtonPanel');
            var hideIfNoPrevNext = this._get(inst, 'hideIfNoPrevNext');
            var navigationAsDateFormat = this._get(inst, 'navigationAsDateFormat');
            var numMonths = this._getNumberOfMonths(inst);
            var showCurrentAtPos = this._get(inst, 'showCurrentAtPos');
            var stepMonths = this._get(inst, 'stepMonths');
            var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1);
            var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) :
                new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            var drawMonth = inst.drawMonth - showCurrentAtPos;
            var drawYear = inst.drawYear;
            if (drawMonth < 0) {
                drawMonth += 12;
                drawYear--;
            }
            if (maxDate) {
                var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(),
                    maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate()));
                maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw);
                while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) {
                    drawMonth--;
                    if (drawMonth < 0) {
                        drawMonth = 11;
                        drawYear--;
                    }
                }
            }
            inst.drawMonth = drawMonth;
            inst.drawYear = drawYear;
            var prevText = this._get(inst, 'prevText');
            prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText,
                this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)),
                this._getFormatConfig(inst)));
            var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ?
            '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._adjustDate(\'#' + inst.id + '\', -' + stepMonths + ', \'M\');"' +
            ' title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>' :
                (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="'+ prevText +'"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>'));
            var nextText = this._get(inst, 'nextText');
            nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText,
                this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)),
                this._getFormatConfig(inst)));
            var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ?
            '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._adjustDate(\'#' + inst.id + '\', +' + stepMonths + ', \'M\');"' +
            ' title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>' :
                (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="'+ nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>'));
            var currentText = this._get(inst, 'currentText');
            var gotoDate = (this._get(inst, 'gotoCurrent') && inst.currentDay ? currentDate : today);
            currentText = (!navigationAsDateFormat ? currentText :
                this.formatDate(currentText, gotoDate, this._getFormatConfig(inst)));
            var controls = (!inst.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._hideDatepicker();">' + this._get(inst, 'closeText') + '</button>' : '');
            var buttonPanel = (showButtonPanel) ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (isRTL ? controls : '') +
            (this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._gotoToday(\'#' + inst.id + '\');"' +
            '>' + currentText + '</button>' : '') + (isRTL ? '' : controls) + '</div>' : '';
            var firstDay = parseInt(this._get(inst, 'firstDay'),10);
            firstDay = (isNaN(firstDay) ? 0 : firstDay);
            var showWeek = this._get(inst, 'showWeek');
            var dayNames = this._get(inst, 'dayNames');
            var dayNamesShort = this._get(inst, 'dayNamesShort');
            var dayNamesMin = this._get(inst, 'dayNamesMin');
            var monthNames = this._get(inst, 'monthNames');
            var monthNamesShort = this._get(inst, 'monthNamesShort');
            var beforeShowDay = this._get(inst, 'beforeShowDay');
            var showOtherMonths = this._get(inst, 'showOtherMonths');
            var selectOtherMonths = this._get(inst, 'selectOtherMonths');
            var calculateWeek = this._get(inst, 'calculateWeek') || this.iso8601Week;
            var defaultDate = this._getDefaultDate(inst);
            var html = '';
            for (var row = 0; row < numMonths[0]; row++) {
                var group = '';
                this.maxRows = 4;
                for (var col = 0; col < numMonths[1]; col++) {
                    var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay));
                    var cornerClass = ' ui-corner-all';
                    var calender = '';
                    if (isMultiMonth) {
                        calender += '<div class="ui-datepicker-group';
                        if (numMonths[1] > 1)
                            switch (col) {
                                case 0: calender += ' ui-datepicker-group-first';
                                    cornerClass = ' ui-corner-' + (isRTL ? 'right' : 'left'); break;
                                case numMonths[1]-1: calender += ' ui-datepicker-group-last';
                                    cornerClass = ' ui-corner-' + (isRTL ? 'left' : 'right'); break;
                                default: calender += ' ui-datepicker-group-middle'; cornerClass = ''; break;
                            }
                        calender += '">';
                    }
                    calender += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + cornerClass + '">' +
                        (/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : '') +
                        (/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : '') +
                        this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate,
                            row > 0 || col > 0, monthNames, monthNamesShort) + // draw month headers
                        '</div><table class="ui-datepicker-calendar"><thead>' +
                        '<tr>';
                    var thead = (showWeek ? '<th class="ui-datepicker-week-col">' + this._get(inst, 'weekHeader') + '</th>' : '');
                    for (var dow = 0; dow < 7; dow++) { // days of the week
                        var day = (dow + firstDay) % 7;
                        thead += '<th' + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : '') + '>' +
                            '<span title="' + dayNames[day] + '">' + dayNamesMin[day] + '</span></th>';
                    }
                    calender += thead + '</tr></thead><tbody>';
                    var daysInMonth = this._getDaysInMonth(drawYear, drawMonth);
                    if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth)
                        inst.selectedDay = Math.min(inst.selectedDay, daysInMonth);
                    var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7;
                    var curRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
                    var numRows = (isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows); //If multiple months, use the higher number of rows (see #7043)
                    this.maxRows = numRows;
                    var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays));
                    for (var dRow = 0; dRow < numRows; dRow++) { // create date picker rows
                        calender += '<tr>';
                        var tbody = (!showWeek ? '' : '<td class="ui-datepicker-week-col">' +
                        this._get(inst, 'calculateWeek')(printDate) + '</td>');
                        for (var dow = 0; dow < 7; dow++) { // create date picker days
                            var daySettings = (beforeShowDay ?
                                beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, '']);
                            var otherMonth = (printDate.getMonth() != drawMonth);
                            var unselectable = (otherMonth && !selectOtherMonths) || !daySettings[0] ||
                                (minDate && printDate < minDate) || (maxDate && printDate > maxDate);
                            tbody += '<td class="' +
                                ((dow + firstDay + 6) % 7 >= 5 ? ' ui-datepicker-week-end' : '') + // highlight weekends
                                (otherMonth ? ' ui-datepicker-other-month' : '') + // highlight days from other months
                                ((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || // user pressed key
                                (defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ?
                                    // or defaultDate is current printedDate and defaultDate is selectedDate
                                ' ' + this._dayOverClass : '') + // highlight selected day
                                (unselectable ? ' ' + this._unselectableClass + ' ui-state-disabled': '') +  // highlight unselectable days
                                (otherMonth && !showOtherMonths ? '' : ' ' + daySettings[1] + // highlight custom dates
                                (printDate.getTime() == currentDate.getTime() ? ' ' + this._currentClass : '') + // highlight selected day
                                (printDate.getTime() == today.getTime() ? ' ui-datepicker-today' : '')) + '"' + // highlight today (if different)
                                ((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : '') + // cell title
                                (unselectable ? '' : ' onclick="DP_jQuery_' + dpuuid + '.datepicker._selectDay(\'#' +
                                inst.id + '\',' + printDate.getMonth() + ',' + printDate.getFullYear() + ', this);return false;"') + '>' + // actions
                                (otherMonth && !showOtherMonths ? '&#xa0;' : // display for other months
                                    (unselectable ? '<span class="ui-state-default">' + printDate.getDate() + '</span>' : '<a class="ui-state-default' +
                                    (printDate.getTime() == today.getTime() ? ' ui-state-highlight' : '') +
                                    (printDate.getTime() == currentDate.getTime() ? ' ui-state-active' : '') + // highlight selected day
                                    (otherMonth ? ' ui-priority-secondary' : '') + // distinguish dates from other months
                                    '" href="#">' + printDate.getDate() + '</a>')) + '</td>'; // display selectable date
                            printDate.setDate(printDate.getDate() + 1);
                            printDate = this._daylightSavingAdjust(printDate);
                        }
                        calender += tbody + '</tr>';
                    }
                    drawMonth++;
                    if (drawMonth > 11) {
                        drawMonth = 0;
                        drawYear++;
                    }
                    calender += '</tbody></table>' + (isMultiMonth ? '</div>' +
                        ((numMonths[0] > 0 && col == numMonths[1]-1) ? '<div class="ui-datepicker-row-break"></div>' : '') : '');
                    group += calender;
                }
                html += group;
            }
            html += buttonPanel + ($.browser.msie && parseInt($.browser.version,10) < 7 && !inst.inline ?
                    '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : '');
            inst._keyEvent = false;
            return html;
        },

        /* Generate the month and year header. */
        _generateMonthYearHeader: function(inst, drawMonth, drawYear, minDate, maxDate,
                                           secondary, monthNames, monthNamesShort) {
            var changeMonth = this._get(inst, 'changeMonth');
            var changeYear = this._get(inst, 'changeYear');
            var showMonthAfterYear = this._get(inst, 'showMonthAfterYear');
            var html = '<div class="ui-datepicker-title">';
            var monthHtml = '';
            // month selection
            if (secondary || !changeMonth)
                monthHtml += '<span class="ui-datepicker-month">' + monthNames[drawMonth] + '</span>';
            else {
                var inMinYear = (minDate && minDate.getFullYear() == drawYear);
                var inMaxYear = (maxDate && maxDate.getFullYear() == drawYear);
                monthHtml += '<select class="ui-datepicker-month" ' +
                    'onchange="DP_jQuery_' + dpuuid + '.datepicker._selectMonthYear(\'#' + inst.id + '\', this, \'M\');" ' +
                    '>';
                for (var month = 0; month < 12; month++) {
                    if ((!inMinYear || month >= minDate.getMonth()) &&
                        (!inMaxYear || month <= maxDate.getMonth()))
                        monthHtml += '<option value="' + month + '"' +
                            (month == drawMonth ? ' selected="selected"' : '') +
                            '>' + monthNamesShort[month] + '</option>';
                }
                monthHtml += '</select>';
            }
            if (!showMonthAfterYear)
                html += monthHtml + (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '');
            // year selection
            if ( !inst.yearshtml ) {
                inst.yearshtml = '';
                if (secondary || !changeYear)
                    html += '<span class="ui-datepicker-year">' + drawYear + '</span>';
                else {
                    // determine range of years to display
                    var years = this._get(inst, 'yearRange').split(':');
                    var thisYear = new Date().getFullYear();
                    var determineYear = function(value) {
                        var year = (value.match(/c[+-].*/) ? drawYear + parseInt(value.substring(1), 10) :
                            (value.match(/[+-].*/) ? thisYear + parseInt(value, 10) :
                                parseInt(value, 10)));
                        return (isNaN(year) ? thisYear : year);
                    };
                    var year = determineYear(years[0]);
                    var endYear = Math.max(year, determineYear(years[1] || ''));
                    year = (minDate ? Math.max(year, minDate.getFullYear()) : year);
                    endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear);
                    inst.yearshtml += '<select class="ui-datepicker-year" ' +
                        'onchange="DP_jQuery_' + dpuuid + '.datepicker._selectMonthYear(\'#' + inst.id + '\', this, \'Y\');" ' +
                        '>';
                    for (; year <= endYear; year++) {
                        inst.yearshtml += '<option value="' + year + '"' +
                            (year == drawYear ? ' selected="selected"' : '') +
                            '>' + year + '</option>';
                    }
                    inst.yearshtml += '</select>';

                    html += inst.yearshtml;
                    inst.yearshtml = null;
                }
            }
            html += this._get(inst, 'yearSuffix');
            if (showMonthAfterYear)
                html += (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '') + monthHtml;
            html += '</div>'; // Close datepicker_header
            return html;
        },

        /* Adjust one of the date sub-fields. */
        _adjustInstDate: function(inst, offset, period) {
            var year = inst.drawYear + (period == 'Y' ? offset : 0);
            var month = inst.drawMonth + (period == 'M' ? offset : 0);
            var day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) +
                (period == 'D' ? offset : 0);
            var date = this._restrictMinMax(inst,
                this._daylightSavingAdjust(new Date(year, month, day)));
            inst.selectedDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = date.getFullYear();
            if (period == 'M' || period == 'Y')
                this._notifyChange(inst);
        },

        /* Ensure a date is within any min/max bounds. */
        _restrictMinMax: function(inst, date) {
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            var newDate = (minDate && date < minDate ? minDate : date);
            newDate = (maxDate && newDate > maxDate ? maxDate : newDate);
            return newDate;
        },

        /* Notify change of month/year. */
        _notifyChange: function(inst) {
            var onChange = this._get(inst, 'onChangeMonthYear');
            if (onChange)
                onChange.apply((inst.input ? inst.input[0] : null),
                    [inst.selectedYear, inst.selectedMonth + 1, inst]);
        },

        /* Determine the number of months to show. */
        _getNumberOfMonths: function(inst) {
            var numMonths = this._get(inst, 'numberOfMonths');
            return (numMonths == null ? [1, 1] : (typeof numMonths == 'number' ? [1, numMonths] : numMonths));
        },

        /* Determine the current maximum date - ensure no time components are set. */
        _getMinMaxDate: function(inst, minMax) {
            return this._determineDate(inst, this._get(inst, minMax + 'Date'), null);
        },

        /* Find the number of days in a given month. */
        _getDaysInMonth: function(year, month) {
            return 32 - this._daylightSavingAdjust(new Date(year, month, 32)).getDate();
        },

        /* Find the day of the week of the first of a month. */
        _getFirstDayOfMonth: function(year, month) {
            return new Date(year, month, 1).getDay();
        },

        /* Determines if we should allow a "next/prev" month display change. */
        _canAdjustMonth: function(inst, offset, curYear, curMonth) {
            var numMonths = this._getNumberOfMonths(inst);
            var date = this._daylightSavingAdjust(new Date(curYear,
                curMonth + (offset < 0 ? offset : numMonths[0] * numMonths[1]), 1));
            if (offset < 0)
                date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth()));
            return this._isInRange(inst, date);
        },

        /* Is the given date in the accepted range? */
        _isInRange: function(inst, date) {
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            return ((!minDate || date.getTime() >= minDate.getTime()) &&
            (!maxDate || date.getTime() <= maxDate.getTime()));
        },

        /* Provide the configuration settings for formatting/parsing. */
        _getFormatConfig: function(inst) {
            var shortYearCutoff = this._get(inst, 'shortYearCutoff');
            shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
            new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
            return {shortYearCutoff: shortYearCutoff,
                dayNamesShort: this._get(inst, 'dayNamesShort'), dayNames: this._get(inst, 'dayNames'),
                monthNamesShort: this._get(inst, 'monthNamesShort'), monthNames: this._get(inst, 'monthNames')};
        },

        /* Format the given date for display. */
        _formatDate: function(inst, day, month, year) {
            if (!day) {
                inst.currentDay = inst.selectedDay;
                inst.currentMonth = inst.selectedMonth;
                inst.currentYear = inst.selectedYear;
            }
            var date = (day ? (typeof day == 'object' ? day :
                this._daylightSavingAdjust(new Date(year, month, day))) :
                this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
            return this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst));
        }
    });

    /*
     * Bind hover events for datepicker elements.
     * Done via delegate so the binding only occurs once in the lifetime of the parent div.
     * Global instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
     */
    function bindHover(dpDiv) {
        var selector = 'button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a';
        return dpDiv.bind('mouseout', function(event) {
            var elem = $( event.target ).closest( selector );
            if ( !elem.length ) {
                return;
            }
            elem.removeClass( "ui-state-hover ui-datepicker-prev-hover ui-datepicker-next-hover" );
        })
            .bind('mouseover', function(event) {
                var elem = $( event.target ).closest( selector );
                if ($.datepicker._isDisabledDatepicker( instActive.inline ? dpDiv.parent()[0] : instActive.input[0]) ||
                    !elem.length ) {
                    return;
                }
                elem.parents('.ui-datepicker-calendar').find('a').removeClass('ui-state-hover');
                elem.addClass('ui-state-hover');
                if (elem.hasClass('ui-datepicker-prev')) elem.addClass('ui-datepicker-prev-hover');
                if (elem.hasClass('ui-datepicker-next')) elem.addClass('ui-datepicker-next-hover');
            });
    }

    /* jQuery extend now ignores nulls! */
    function extendRemove(target, props) {
        $.extend(target, props);
        for (var name in props)
            if (props[name] == null || props[name] == undefined)
                target[name] = props[name];
        return target;
    };

    /* Determine whether an object is an array. */
    function isArray(a) {
        return (a && (($.browser.safari && typeof a == 'object' && a.length) ||
        (a.constructor && a.constructor.toString().match(/\Array\(\)/))));
    };

    /* Invoke the datepicker functionality.
     @param  options  string - a command, optionally followed by additional parameters or
     Object - settings for attaching new datepicker functionality
     @return  jQuery object */
    $.fn.datepicker = function(options){

        /* Verify an empty collection wasn't passed - Fixes #6976 */
        if ( !this.length ) {
            return this;
        }

        /* Initialise the date picker. */
        if (!$.datepicker.initialized) {
            $(document).mousedown($.datepicker._checkExternalClick).
            find('body').append($.datepicker.dpDiv);
            $.datepicker.initialized = true;
        }

        var otherArgs = Array.prototype.slice.call(arguments, 1);
        if (typeof options == 'string' && (options == 'isDisabled' || options == 'getDate' || options == 'widget'))
            return $.datepicker['_' + options + 'Datepicker'].
            apply($.datepicker, [this[0]].concat(otherArgs));
        if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string')
            return $.datepicker['_' + options + 'Datepicker'].
            apply($.datepicker, [this[0]].concat(otherArgs));
        return this.each(function() {
            typeof options == 'string' ?
                $.datepicker['_' + options + 'Datepicker'].
                apply($.datepicker, [this].concat(otherArgs)) :
                $.datepicker._attachDatepicker(this, options);
        });
    };

    $.datepicker = new Datepicker(); // singleton instance
    $.datepicker.initialized = false;
    $.datepicker.uuid = new Date().getTime();
    $.datepicker.version = "1.8.16";

// Workaround for #4055
// Add another global to avoid noConflict issues with inline event handlers
    window['DP_jQuery_' + dpuuid] = $;

})(jQuery);
/*
 * jQuery UI Effects 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/
 */
;jQuery.effects || (function($, undefined) {

    $.effects = {};



    /******************************************************************************/
    /****************************** COLOR ANIMATIONS ******************************/
    /******************************************************************************/

// override the animation for color styles
    $.each(['backgroundColor', 'borderBottomColor', 'borderLeftColor',
            'borderRightColor', 'borderTopColor', 'borderColor', 'color', 'outlineColor'],
        function(i, attr) {
            $.fx.step[attr] = function(fx) {
                if (!fx.colorInit) {
                    fx.start = getColor(fx.elem, attr);
                    fx.end = getRGB(fx.end);
                    fx.colorInit = true;
                }

                fx.elem.style[attr] = 'rgb(' +
                    Math.max(Math.min(parseInt((fx.pos * (fx.end[0] - fx.start[0])) + fx.start[0], 10), 255), 0) + ',' +
                    Math.max(Math.min(parseInt((fx.pos * (fx.end[1] - fx.start[1])) + fx.start[1], 10), 255), 0) + ',' +
                    Math.max(Math.min(parseInt((fx.pos * (fx.end[2] - fx.start[2])) + fx.start[2], 10), 255), 0) + ')';
            };
        });

// Color Conversion functions from highlightFade
// By Blair Mitchelmore
// http://jquery.offput.ca/highlightFade/

// Parse strings looking for color tuples [255,255,255]
    function getRGB(color) {
        var result;

        // Check if we're already dealing with an array of colors
        if ( color && color.constructor == Array && color.length == 3 )
            return color;

        // Look for rgb(num,num,num)
        if (result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))
            return [parseInt(result[1],10), parseInt(result[2],10), parseInt(result[3],10)];

        // Look for rgb(num%,num%,num%)
        if (result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))
            return [parseFloat(result[1])*2.55, parseFloat(result[2])*2.55, parseFloat(result[3])*2.55];

        // Look for #a0b1c2
        if (result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))
            return [parseInt(result[1],16), parseInt(result[2],16), parseInt(result[3],16)];

        // Look for #fff
        if (result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))
            return [parseInt(result[1]+result[1],16), parseInt(result[2]+result[2],16), parseInt(result[3]+result[3],16)];

        // Look for rgba(0, 0, 0, 0) == transparent in Safari 3
        if (result = /rgba\(0, 0, 0, 0\)/.exec(color))
            return colors['transparent'];

        // Otherwise, we're most likely dealing with a named color
        return colors[$.trim(color).toLowerCase()];
    }

    function getColor(elem, attr) {
        var color;

        do {
            color = $.curCSS(elem, attr);

            // Keep going until we find an element that has color, or we hit the body
            if ( color != '' && color != 'transparent' || $.nodeName(elem, "body") )
                break;

            attr = "backgroundColor";
        } while ( elem = elem.parentNode );

        return getRGB(color);
    };

// Some named colors to work with
// From Interface by Stefan Petre
// http://interface.eyecon.ro/

    var colors = {
        aqua:[0,255,255],
        azure:[240,255,255],
        beige:[245,245,220],
        black:[0,0,0],
        blue:[0,0,255],
        brown:[165,42,42],
        cyan:[0,255,255],
        darkblue:[0,0,139],
        darkcyan:[0,139,139],
        darkgrey:[169,169,169],
        darkgreen:[0,100,0],
        darkkhaki:[189,183,107],
        darkmagenta:[139,0,139],
        darkolivegreen:[85,107,47],
        darkorange:[255,140,0],
        darkorchid:[153,50,204],
        darkred:[139,0,0],
        darksalmon:[233,150,122],
        darkviolet:[148,0,211],
        fuchsia:[255,0,255],
        gold:[255,215,0],
        green:[0,128,0],
        indigo:[75,0,130],
        khaki:[240,230,140],
        lightblue:[173,216,230],
        lightcyan:[224,255,255],
        lightgreen:[144,238,144],
        lightgrey:[211,211,211],
        lightpink:[255,182,193],
        lightyellow:[255,255,224],
        lime:[0,255,0],
        magenta:[255,0,255],
        maroon:[128,0,0],
        navy:[0,0,128],
        olive:[128,128,0],
        orange:[255,165,0],
        pink:[255,192,203],
        purple:[128,0,128],
        violet:[128,0,128],
        red:[255,0,0],
        silver:[192,192,192],
        white:[255,255,255],
        yellow:[255,255,0],
        transparent: [255,255,255]
    };



    /******************************************************************************/
    /****************************** CLASS ANIMATIONS ******************************/
    /******************************************************************************/

    var classAnimationActions = ['add', 'remove', 'toggle'],
        shorthandStyles = {
            border: 1,
            borderBottom: 1,
            borderColor: 1,
            borderLeft: 1,
            borderRight: 1,
            borderTop: 1,
            borderWidth: 1,
            margin: 1,
            padding: 1
        };

    function getElementStyles() {
        var style = document.defaultView
                ? document.defaultView.getComputedStyle(this, null)
                : this.currentStyle,
            newStyle = {},
            key,
            camelCase;

        // webkit enumerates style porperties
        if (style && style.length && style[0] && style[style[0]]) {
            var len = style.length;
            while (len--) {
                key = style[len];
                if (typeof style[key] == 'string') {
                    camelCase = key.replace(/\-(\w)/g, function(all, letter){
                        return letter.toUpperCase();
                    });
                    newStyle[camelCase] = style[key];
                }
            }
        } else {
            for (key in style) {
                if (typeof style[key] === 'string') {
                    newStyle[key] = style[key];
                }
            }
        }

        return newStyle;
    }

    function filterStyles(styles) {
        var name, value;
        for (name in styles) {
            value = styles[name];
            if (
                // ignore null and undefined values
            value == null ||
            // ignore functions (when does this occur?)
            $.isFunction(value) ||
            // shorthand styles that need to be expanded
            name in shorthandStyles ||
            // ignore scrollbars (break in IE)
            (/scrollbar/).test(name) ||

            // only colors or values that can be converted to numbers
            (!(/color/i).test(name) && isNaN(parseFloat(value)))
            ) {
                delete styles[name];
            }
        }

        return styles;
    }

    function styleDifference(oldStyle, newStyle) {
        var diff = { _: 0 }, // http://dev.jquery.com/ticket/5459
            name;

        for (name in newStyle) {
            if (oldStyle[name] != newStyle[name]) {
                diff[name] = newStyle[name];
            }
        }

        return diff;
    }

    $.effects.animateClass = function(value, duration, easing, callback) {
        if ($.isFunction(easing)) {
            callback = easing;
            easing = null;
        }

        return this.queue(function() {
            var that = $(this),
                originalStyleAttr = that.attr('style') || ' ',
                originalStyle = filterStyles(getElementStyles.call(this)),
                newStyle,
                className = that.attr('class');

            $.each(classAnimationActions, function(i, action) {
                if (value[action]) {
                    that[action + 'Class'](value[action]);
                }
            });
            newStyle = filterStyles(getElementStyles.call(this));
            that.attr('class', className);

            that.animate(styleDifference(originalStyle, newStyle), {
                queue: false,
                duration: duration,
                easing: easing,
                complete: function() {
                    $.each(classAnimationActions, function(i, action) {
                        if (value[action]) { that[action + 'Class'](value[action]); }
                    });
                    // work around bug in IE by clearing the cssText before setting it
                    if (typeof that.attr('style') == 'object') {
                        that.attr('style').cssText = '';
                        that.attr('style').cssText = originalStyleAttr;
                    } else {
                        that.attr('style', originalStyleAttr);
                    }
                    if (callback) { callback.apply(this, arguments); }
                    $.dequeue( this );
                }
            });
        });
    };

    $.fn.extend({
        _addClass: $.fn.addClass,
        addClass: function(classNames, speed, easing, callback) {
            return speed ? $.effects.animateClass.apply(this, [{ add: classNames },speed,easing,callback]) : this._addClass(classNames);
        },

        _removeClass: $.fn.removeClass,
        removeClass: function(classNames,speed,easing,callback) {
            return speed ? $.effects.animateClass.apply(this, [{ remove: classNames },speed,easing,callback]) : this._removeClass(classNames);
        },

        _toggleClass: $.fn.toggleClass,
        toggleClass: function(classNames, force, speed, easing, callback) {
            if ( typeof force == "boolean" || force === undefined ) {
                if ( !speed ) {
                    // without speed parameter;
                    return this._toggleClass(classNames, force);
                } else {
                    return $.effects.animateClass.apply(this, [(force?{add:classNames}:{remove:classNames}),speed,easing,callback]);
                }
            } else {
                // without switch parameter;
                return $.effects.animateClass.apply(this, [{ toggle: classNames },force,speed,easing]);
            }
        },

        switchClass: function(remove,add,speed,easing,callback) {
            return $.effects.animateClass.apply(this, [{ add: add, remove: remove },speed,easing,callback]);
        }
    });



    /******************************************************************************/
    /*********************************** EFFECTS **********************************/
    /******************************************************************************/

    $.extend($.effects, {
        version: "1.8.16",

        // Saves a set of properties in a data storage
        save: function(element, set) {
            for(var i=0; i < set.length; i++) {
                if(set[i] !== null) element.data("ec.storage."+set[i], element[0].style[set[i]]);
            }
        },

        // Restores a set of previously saved properties from a data storage
        restore: function(element, set) {
            for(var i=0; i < set.length; i++) {
                if(set[i] !== null) element.css(set[i], element.data("ec.storage."+set[i]));
            }
        },

        setMode: function(el, mode) {
            if (mode == 'toggle') mode = el.is(':hidden') ? 'show' : 'hide'; // Set for toggle
            return mode;
        },

        getBaseline: function(origin, original) { // Translates a [top,left] array into a baseline value
            // this should be a little more flexible in the future to handle a string & hash
            var y, x;
            switch (origin[0]) {
                case 'top': y = 0; break;
                case 'middle': y = 0.5; break;
                case 'bottom': y = 1; break;
                default: y = origin[0] / original.height;
            };
            switch (origin[1]) {
                case 'left': x = 0; break;
                case 'center': x = 0.5; break;
                case 'right': x = 1; break;
                default: x = origin[1] / original.width;
            };
            return {x: x, y: y};
        },

        // Wraps the element around a wrapper that copies position properties
        createWrapper: function(element) {

            // if the element is already wrapped, return it
            if (element.parent().is('.ui-effects-wrapper')) {
                return element.parent();
            }

            // wrap the element
            var props = {
                    width: element.outerWidth(true),
                    height: element.outerHeight(true),
                    'float': element.css('float')
                },
                wrapper = $('<div></div>')
                    .addClass('ui-effects-wrapper')
                    .css({
                        fontSize: '100%',
                        background: 'transparent',
                        border: 'none',
                        margin: 0,
                        padding: 0
                    }),
                active = document.activeElement;

            element.wrap(wrapper);

            // Fixes #7595 - Elements lose focus when wrapped.
            if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
                $( active ).focus();
            }

            wrapper = element.parent(); //Hotfix for jQuery 1.4 since some change in wrap() seems to actually loose the reference to the wrapped element

            // transfer positioning properties to the wrapper
            if (element.css('position') == 'static') {
                wrapper.css({ position: 'relative' });
                element.css({ position: 'relative' });
            } else {
                $.extend(props, {
                    position: element.css('position'),
                    zIndex: element.css('z-index')
                });
                $.each(['top', 'left', 'bottom', 'right'], function(i, pos) {
                    props[pos] = element.css(pos);
                    if (isNaN(parseInt(props[pos], 10))) {
                        props[pos] = 'auto';
                    }
                });
                element.css({position: 'relative', top: 0, left: 0, right: 'auto', bottom: 'auto' });
            }

            return wrapper.css(props).show();
        },

        removeWrapper: function(element) {
            var parent,
                active = document.activeElement;

            if (element.parent().is('.ui-effects-wrapper')) {
                parent = element.parent().replaceWith(element);
                // Fixes #7595 - Elements lose focus when wrapped.
                if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
                    $( active ).focus();
                }
                return parent;
            }

            return element;
        },

        setTransition: function(element, list, factor, value) {
            value = value || {};
            $.each(list, function(i, x){
                unit = element.cssUnit(x);
                if (unit[0] > 0) value[x] = unit[0] * factor + unit[1];
            });
            return value;
        }
    });


    function _normalizeArguments(effect, options, speed, callback) {
        // shift params for method overloading
        if (typeof effect == 'object') {
            callback = options;
            speed = null;
            options = effect;
            effect = options.effect;
        }
        if ($.isFunction(options)) {
            callback = options;
            speed = null;
            options = {};
        }
        if (typeof options == 'number' || $.fx.speeds[options]) {
            callback = speed;
            speed = options;
            options = {};
        }
        if ($.isFunction(speed)) {
            callback = speed;
            speed = null;
        }

        options = options || {};

        speed = speed || options.duration;
        speed = $.fx.off ? 0 : typeof speed == 'number'
            ? speed : speed in $.fx.speeds ? $.fx.speeds[speed] : $.fx.speeds._default;

        callback = callback || options.complete;

        return [effect, options, speed, callback];
    }

    function standardSpeed( speed ) {
        // valid standard speeds
        if ( !speed || typeof speed === "number" || $.fx.speeds[ speed ] ) {
            return true;
        }

        // invalid strings - treat as "normal" speed
        if ( typeof speed === "string" && !$.effects[ speed ] ) {
            return true;
        }

        return false;
    }

    $.fn.extend({
        effect: function(effect, options, speed, callback) {
            var args = _normalizeArguments.apply(this, arguments),
            // TODO: make effects take actual parameters instead of a hash
                args2 = {
                    options: args[1],
                    duration: args[2],
                    callback: args[3]
                },
                mode = args2.options.mode,
                effectMethod = $.effects[effect];

            if ( $.fx.off || !effectMethod ) {
                // delegate to the original method (e.g., .show()) if possible
                if ( mode ) {
                    return this[ mode ]( args2.duration, args2.callback );
                } else {
                    return this.each(function() {
                        if ( args2.callback ) {
                            args2.callback.call( this );
                        }
                    });
                }
            }

            return effectMethod.call(this, args2);
        },

        _show: $.fn.show,
        show: function(speed) {
            if ( standardSpeed( speed ) ) {
                return this._show.apply(this, arguments);
            } else {
                var args = _normalizeArguments.apply(this, arguments);
                args[1].mode = 'show';
                return this.effect.apply(this, args);
            }
        },

        _hide: $.fn.hide,
        hide: function(speed) {
            if ( standardSpeed( speed ) ) {
                return this._hide.apply(this, arguments);
            } else {
                var args = _normalizeArguments.apply(this, arguments);
                args[1].mode = 'hide';
                return this.effect.apply(this, args);
            }
        },

        // jQuery core overloads toggle and creates _toggle
        __toggle: $.fn.toggle,
        toggle: function(speed) {
            if ( standardSpeed( speed ) || typeof speed === "boolean" || $.isFunction( speed ) ) {
                return this.__toggle.apply(this, arguments);
            } else {
                var args = _normalizeArguments.apply(this, arguments);
                args[1].mode = 'toggle';
                return this.effect.apply(this, args);
            }
        },

        // helper functions
        cssUnit: function(key) {
            var style = this.css(key), val = [];
            $.each( ['em','px','%','pt'], function(i, unit){
                if(style.indexOf(unit) > 0)
                    val = [parseFloat(style), unit];
            });
            return val;
        }
    });



    /******************************************************************************/
    /*********************************** EASING ***********************************/
    /******************************************************************************/

    /*
     * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
     *
     * Uses the built in easing capabilities added In jQuery 1.1
     * to offer multiple easing options
     *
     * TERMS OF USE - jQuery Easing
     *
     * Open source under the BSD License.
     *
     * Copyright 2008 George McGinley Smith
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * Redistributions of source code must retain the above copyright notice, this list of
     * conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice, this list
     * of conditions and the following disclaimer in the documentation and/or other materials
     * provided with the distribution.
     *
     * Neither the name of the author nor the names of contributors may be used to endorse
     * or promote products derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
     * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
     * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
     * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
     * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
     * OF THE POSSIBILITY OF SUCH DAMAGE.
     *
     */

// t: current time, b: begInnIng value, c: change In value, d: duration
    $.easing.jswing = $.easing.swing;

    $.extend($.easing,
        {
            def: 'easeOutQuad',
            swing: function (x, t, b, c, d) {
                //alert($.easing.default);
                return $.easing[$.easing.def](x, t, b, c, d);
            },
            easeInQuad: function (x, t, b, c, d) {
                return c*(t/=d)*t + b;
            },
            easeOutQuad: function (x, t, b, c, d) {
                return -c *(t/=d)*(t-2) + b;
            },
            easeInOutQuad: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return c/2*t*t + b;
                return -c/2 * ((--t)*(t-2) - 1) + b;
            },
            easeInCubic: function (x, t, b, c, d) {
                return c*(t/=d)*t*t + b;
            },
            easeOutCubic: function (x, t, b, c, d) {
                return c*((t=t/d-1)*t*t + 1) + b;
            },
            easeInOutCubic: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return c/2*t*t*t + b;
                return c/2*((t-=2)*t*t + 2) + b;
            },
            easeInQuart: function (x, t, b, c, d) {
                return c*(t/=d)*t*t*t + b;
            },
            easeOutQuart: function (x, t, b, c, d) {
                return -c * ((t=t/d-1)*t*t*t - 1) + b;
            },
            easeInOutQuart: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
                return -c/2 * ((t-=2)*t*t*t - 2) + b;
            },
            easeInQuint: function (x, t, b, c, d) {
                return c*(t/=d)*t*t*t*t + b;
            },
            easeOutQuint: function (x, t, b, c, d) {
                return c*((t=t/d-1)*t*t*t*t + 1) + b;
            },
            easeInOutQuint: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
                return c/2*((t-=2)*t*t*t*t + 2) + b;
            },
            easeInSine: function (x, t, b, c, d) {
                return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
            },
            easeOutSine: function (x, t, b, c, d) {
                return c * Math.sin(t/d * (Math.PI/2)) + b;
            },
            easeInOutSine: function (x, t, b, c, d) {
                return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
            },
            easeInExpo: function (x, t, b, c, d) {
                return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
            },
            easeOutExpo: function (x, t, b, c, d) {
                return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
            },
            easeInOutExpo: function (x, t, b, c, d) {
                if (t==0) return b;
                if (t==d) return b+c;
                if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
                return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
            },
            easeInCirc: function (x, t, b, c, d) {
                return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
            },
            easeOutCirc: function (x, t, b, c, d) {
                return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
            },
            easeInOutCirc: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
                return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
            },
            easeInElastic: function (x, t, b, c, d) {
                var s=1.70158;var p=0;var a=c;
                if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                if (a < Math.abs(c)) { a=c; var s=p/4; }
                else var s = p/(2*Math.PI) * Math.asin (c/a);
                return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
            },
            easeOutElastic: function (x, t, b, c, d) {
                var s=1.70158;var p=0;var a=c;
                if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                if (a < Math.abs(c)) { a=c; var s=p/4; }
                else var s = p/(2*Math.PI) * Math.asin (c/a);
                return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
            },
            easeInOutElastic: function (x, t, b, c, d) {
                var s=1.70158;var p=0;var a=c;
                if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
                if (a < Math.abs(c)) { a=c; var s=p/4; }
                else var s = p/(2*Math.PI) * Math.asin (c/a);
                if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
                return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
            },
            easeInBack: function (x, t, b, c, d, s) {
                if (s == undefined) s = 1.70158;
                return c*(t/=d)*t*((s+1)*t - s) + b;
            },
            easeOutBack: function (x, t, b, c, d, s) {
                if (s == undefined) s = 1.70158;
                return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
            },
            easeInOutBack: function (x, t, b, c, d, s) {
                if (s == undefined) s = 1.70158;
                if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
                return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
            },
            easeInBounce: function (x, t, b, c, d) {
                return c - $.easing.easeOutBounce (x, d-t, 0, c, d) + b;
            },
            easeOutBounce: function (x, t, b, c, d) {
                if ((t/=d) < (1/2.75)) {
                    return c*(7.5625*t*t) + b;
                } else if (t < (2/2.75)) {
                    return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
                } else if (t < (2.5/2.75)) {
                    return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
                } else {
                    return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
                }
            },
            easeInOutBounce: function (x, t, b, c, d) {
                if (t < d/2) return $.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
                return $.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
            }
        });

    /*
     *
     * TERMS OF USE - EASING EQUATIONS
     *
     * Open source under the BSD License.
     *
     * Copyright 2001 Robert Penner
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * Redistributions of source code must retain the above copyright notice, this list of
     * conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice, this list
     * of conditions and the following disclaimer in the documentation and/or other materials
     * provided with the distribution.
     *
     * Neither the name of the author nor the names of contributors may be used to endorse
     * or promote products derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
     * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
     * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
     * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
     * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
     * OF THE POSSIBILITY OF SUCH DAMAGE.
     *
     */

})(jQuery);
/*
 * jQuery UI Effects Blind 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Blind
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.blind = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'hide'); // Set Mode
            var direction = o.options.direction || 'vertical'; // Default direction

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            var wrapper = $.effects.createWrapper(el).css({overflow:'hidden'}); // Create Wrapper
            var ref = (direction == 'vertical') ? 'height' : 'width';
            var distance = (direction == 'vertical') ? wrapper.height() : wrapper.width();
            if(mode == 'show') wrapper.css(ref, 0); // Shift

            // Animation
            var animation = {};
            animation[ref] = mode == 'show' ? distance : 0;

            // Animate
            wrapper.animate(animation, o.duration, o.options.easing, function() {
                if(mode == 'hide') el.hide(); // Hide
                $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(el[0], arguments); // Callback
                el.dequeue();
            });

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Bounce 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Bounce
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.bounce = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'effect'); // Set Mode
            var direction = o.options.direction || 'up'; // Default direction
            var distance = o.options.distance || 20; // Default distance
            var times = o.options.times || 5; // Default # of times
            var speed = o.duration || 250; // Default speed per bounce
            if (/show|hide/.test(mode)) props.push('opacity'); // Avoid touching opacity to prevent clearType and PNG issues in IE

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            $.effects.createWrapper(el); // Create Wrapper
            var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left';
            var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg';
            var distance = o.options.distance || (ref == 'top' ? el.outerHeight({margin:true}) / 3 : el.outerWidth({margin:true}) / 3);
            if (mode == 'show') el.css('opacity', 0).css(ref, motion == 'pos' ? -distance : distance); // Shift
            if (mode == 'hide') distance = distance / (times * 2);
            if (mode != 'hide') times--;

            // Animate
            if (mode == 'show') { // Show Bounce
                var animation = {opacity: 1};
                animation[ref] = (motion == 'pos' ? '+=' : '-=') + distance;
                el.animate(animation, speed / 2, o.options.easing);
                distance = distance / 2;
                times--;
            };
            for (var i = 0; i < times; i++) { // Bounces
                var animation1 = {}, animation2 = {};
                animation1[ref] = (motion == 'pos' ? '-=' : '+=') + distance;
                animation2[ref] = (motion == 'pos' ? '+=' : '-=') + distance;
                el.animate(animation1, speed / 2, o.options.easing).animate(animation2, speed / 2, o.options.easing);
                distance = (mode == 'hide') ? distance * 2 : distance / 2;
            };
            if (mode == 'hide') { // Last Bounce
                var animation = {opacity: 0};
                animation[ref] = (motion == 'pos' ? '-=' : '+=')  + distance;
                el.animate(animation, speed / 2, o.options.easing, function(){
                    el.hide(); // Hide
                    $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                    if(o.callback) o.callback.apply(this, arguments); // Callback
                });
            } else {
                var animation1 = {}, animation2 = {};
                animation1[ref] = (motion == 'pos' ? '-=' : '+=') + distance;
                animation2[ref] = (motion == 'pos' ? '+=' : '-=') + distance;
                el.animate(animation1, speed / 2, o.options.easing).animate(animation2, speed / 2, o.options.easing, function(){
                    $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                    if(o.callback) o.callback.apply(this, arguments); // Callback
                });
            };
            el.queue('fx', function() { el.dequeue(); });
            el.dequeue();
        });

    };

})(jQuery);
/*
 * jQuery UI Effects Clip 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Clip
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.clip = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right','height','width'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'hide'); // Set Mode
            var direction = o.options.direction || 'vertical'; // Default direction

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            var wrapper = $.effects.createWrapper(el).css({overflow:'hidden'}); // Create Wrapper
            var animate = el[0].tagName == 'IMG' ? wrapper : el;
            var ref = {
                size: (direction == 'vertical') ? 'height' : 'width',
                position: (direction == 'vertical') ? 'top' : 'left'
            };
            var distance = (direction == 'vertical') ? animate.height() : animate.width();
            if(mode == 'show') { animate.css(ref.size, 0); animate.css(ref.position, distance / 2); } // Shift

            // Animation
            var animation = {};
            animation[ref.size] = mode == 'show' ? distance : 0;
            animation[ref.position] = mode == 'show' ? 0 : distance / 2;

            // Animate
            animate.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function() {
                if(mode == 'hide') el.hide(); // Hide
                $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(el[0], arguments); // Callback
                el.dequeue();
            }});

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Drop 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Drop
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.drop = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right','opacity'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'hide'); // Set Mode
            var direction = o.options.direction || 'left'; // Default Direction

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            $.effects.createWrapper(el); // Create Wrapper
            var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left';
            var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg';
            var distance = o.options.distance || (ref == 'top' ? el.outerHeight({margin:true}) / 2 : el.outerWidth({margin:true}) / 2);
            if (mode == 'show') el.css('opacity', 0).css(ref, motion == 'pos' ? -distance : distance); // Shift

            // Animation
            var animation = {opacity: mode == 'show' ? 1 : 0};
            animation[ref] = (mode == 'show' ? (motion == 'pos' ? '+=' : '-=') : (motion == 'pos' ? '-=' : '+=')) + distance;

            // Animate
            el.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function() {
                if(mode == 'hide') el.hide(); // Hide
                $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(this, arguments); // Callback
                el.dequeue();
            }});

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Explode 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Explode
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.explode = function(o) {

        return this.queue(function() {

            var rows = o.options.pieces ? Math.round(Math.sqrt(o.options.pieces)) : 3;
            var cells = o.options.pieces ? Math.round(Math.sqrt(o.options.pieces)) : 3;

            o.options.mode = o.options.mode == 'toggle' ? ($(this).is(':visible') ? 'hide' : 'show') : o.options.mode;
            var el = $(this).show().css('visibility', 'hidden');
            var offset = el.offset();

            //Substract the margins - not fixing the problem yet.
            offset.top -= parseInt(el.css("marginTop"),10) || 0;
            offset.left -= parseInt(el.css("marginLeft"),10) || 0;

            var width = el.outerWidth(true);
            var height = el.outerHeight(true);

            for(var i=0;i<rows;i++) { // =
                for(var j=0;j<cells;j++) { // ||
                    el
                        .clone()
                        .appendTo('body')
                        .wrap('<div></div>')
                        .css({
                            position: 'absolute',
                            visibility: 'visible',
                            left: -j*(width/cells),
                            top: -i*(height/rows)
                        })
                        .parent()
                        .addClass('ui-effects-explode')
                        .css({
                            position: 'absolute',
                            overflow: 'hidden',
                            width: width/cells,
                            height: height/rows,
                            left: offset.left + j*(width/cells) + (o.options.mode == 'show' ? (j-Math.floor(cells/2))*(width/cells) : 0),
                            top: offset.top + i*(height/rows) + (o.options.mode == 'show' ? (i-Math.floor(rows/2))*(height/rows) : 0),
                            opacity: o.options.mode == 'show' ? 0 : 1
                        }).animate({
                        left: offset.left + j*(width/cells) + (o.options.mode == 'show' ? 0 : (j-Math.floor(cells/2))*(width/cells)),
                        top: offset.top + i*(height/rows) + (o.options.mode == 'show' ? 0 : (i-Math.floor(rows/2))*(height/rows)),
                        opacity: o.options.mode == 'show' ? 1 : 0
                    }, o.duration || 500);
                }
            }

            // Set a timeout, to call the callback approx. when the other animations have finished
            setTimeout(function() {

                o.options.mode == 'show' ? el.css({ visibility: 'visible' }) : el.css({ visibility: 'visible' }).hide();
                if(o.callback) o.callback.apply(el[0]); // Callback
                el.dequeue();

                $('div.ui-effects-explode').remove();

            }, o.duration || 500);


        });

    };

})(jQuery);
/*
 * jQuery UI Effects Fade 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Fade
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.fade = function(o) {
        return this.queue(function() {
            var elem = $(this),
                mode = $.effects.setMode(elem, o.options.mode || 'hide');

            elem.animate({ opacity: mode }, {
                queue: false,
                duration: o.duration,
                easing: o.options.easing,
                complete: function() {
                    (o.callback && o.callback.apply(this, arguments));
                    elem.dequeue();
                }
            });
        });
    };

})(jQuery);
/*
 * jQuery UI Effects Fold 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Fold
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.fold = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'hide'); // Set Mode
            var size = o.options.size || 15; // Default fold size
            var horizFirst = !(!o.options.horizFirst); // Ensure a boolean value
            var duration = o.duration ? o.duration / 2 : $.fx.speeds._default / 2;

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            var wrapper = $.effects.createWrapper(el).css({overflow:'hidden'}); // Create Wrapper
            var widthFirst = ((mode == 'show') != horizFirst);
            var ref = widthFirst ? ['width', 'height'] : ['height', 'width'];
            var distance = widthFirst ? [wrapper.width(), wrapper.height()] : [wrapper.height(), wrapper.width()];
            var percent = /([0-9]+)%/.exec(size);
            if(percent) size = parseInt(percent[1],10) / 100 * distance[mode == 'hide' ? 0 : 1];
            if(mode == 'show') wrapper.css(horizFirst ? {height: 0, width: size} : {height: size, width: 0}); // Shift

            // Animation
            var animation1 = {}, animation2 = {};
            animation1[ref[0]] = mode == 'show' ? distance[0] : size;
            animation2[ref[1]] = mode == 'show' ? distance[1] : 0;

            // Animate
            wrapper.animate(animation1, duration, o.options.easing)
                .animate(animation2, duration, o.options.easing, function() {
                    if(mode == 'hide') el.hide(); // Hide
                    $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                    if(o.callback) o.callback.apply(el[0], arguments); // Callback
                    el.dequeue();
                });

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Highlight 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Highlight
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.highlight = function(o) {
        return this.queue(function() {
            var elem = $(this),
                props = ['backgroundImage', 'backgroundColor', 'opacity'],
                mode = $.effects.setMode(elem, o.options.mode || 'show'),
                animation = {
                    backgroundColor: elem.css('backgroundColor')
                };

            if (mode == 'hide') {
                animation.opacity = 0;
            }

            $.effects.save(elem, props);
            elem
                .show()
                .css({
                    backgroundImage: 'none',
                    backgroundColor: o.options.color || '#ffff99'
                })
                .animate(animation, {
                    queue: false,
                    duration: o.duration,
                    easing: o.options.easing,
                    complete: function() {
                        (mode == 'hide' && elem.hide());
                        $.effects.restore(elem, props);
                        (mode == 'show' && !$.support.opacity && this.style.removeAttribute('filter'));
                        (o.callback && o.callback.apply(this, arguments));
                        elem.dequeue();
                    }
                });
        });
    };

})(jQuery);
/*
 * jQuery UI Effects Pulsate 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Pulsate
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.pulsate = function(o) {
        return this.queue(function() {
            var elem = $(this),
                mode = $.effects.setMode(elem, o.options.mode || 'show');
            times = ((o.options.times || 5) * 2) - 1;
            duration = o.duration ? o.duration / 2 : $.fx.speeds._default / 2,
                isVisible = elem.is(':visible'),
                animateTo = 0;

            if (!isVisible) {
                elem.css('opacity', 0).show();
                animateTo = 1;
            }

            if ((mode == 'hide' && isVisible) || (mode == 'show' && !isVisible)) {
                times--;
            }

            for (var i = 0; i < times; i++) {
                elem.animate({ opacity: animateTo }, duration, o.options.easing);
                animateTo = (animateTo + 1) % 2;
            }

            elem.animate({ opacity: animateTo }, duration, o.options.easing, function() {
                if (animateTo == 0) {
                    elem.hide();
                }
                (o.callback && o.callback.apply(this, arguments));
            });

            elem
                .queue('fx', function() { elem.dequeue(); })
                .dequeue();
        });
    };

})(jQuery);
/*
 * jQuery UI Effects Scale 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Scale
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.puff = function(o) {
        return this.queue(function() {
            var elem = $(this),
                mode = $.effects.setMode(elem, o.options.mode || 'hide'),
                percent = parseInt(o.options.percent, 10) || 150,
                factor = percent / 100,
                original = { height: elem.height(), width: elem.width() };

            $.extend(o.options, {
                fade: true,
                mode: mode,
                percent: mode == 'hide' ? percent : 100,
                from: mode == 'hide'
                    ? original
                    : {
                    height: original.height * factor,
                    width: original.width * factor
                }
            });

            elem.effect('scale', o.options, o.duration, o.callback);
            elem.dequeue();
        });
    };

    $.effects.scale = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this);

            // Set options
            var options = $.extend(true, {}, o.options);
            var mode = $.effects.setMode(el, o.options.mode || 'effect'); // Set Mode
            var percent = parseInt(o.options.percent,10) || (parseInt(o.options.percent,10) == 0 ? 0 : (mode == 'hide' ? 0 : 100)); // Set default scaling percent
            var direction = o.options.direction || 'both'; // Set default axis
            var origin = o.options.origin; // The origin of the scaling
            if (mode != 'effect') { // Set default origin and restore for show/hide
                options.origin = origin || ['middle','center'];
                options.restore = true;
            }
            var original = {height: el.height(), width: el.width()}; // Save original
            el.from = o.options.from || (mode == 'show' ? {height: 0, width: 0} : original); // Default from state

            // Adjust
            var factor = { // Set scaling factor
                y: direction != 'horizontal' ? (percent / 100) : 1,
                x: direction != 'vertical' ? (percent / 100) : 1
            };
            el.to = {height: original.height * factor.y, width: original.width * factor.x}; // Set to state

            if (o.options.fade) { // Fade option to support puff
                if (mode == 'show') {el.from.opacity = 0; el.to.opacity = 1;};
                if (mode == 'hide') {el.from.opacity = 1; el.to.opacity = 0;};
            };

            // Animation
            options.from = el.from; options.to = el.to; options.mode = mode;

            // Animate
            el.effect('size', options, o.duration, o.callback);
            el.dequeue();
        });

    };

    $.effects.size = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right','width','height','overflow','opacity'];
            var props1 = ['position','top','bottom','left','right','overflow','opacity']; // Always restore
            var props2 = ['width','height','overflow']; // Copy for children
            var cProps = ['fontSize'];
            var vProps = ['borderTopWidth', 'borderBottomWidth', 'paddingTop', 'paddingBottom'];
            var hProps = ['borderLeftWidth', 'borderRightWidth', 'paddingLeft', 'paddingRight'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'effect'); // Set Mode
            var restore = o.options.restore || false; // Default restore
            var scale = o.options.scale || 'both'; // Default scale mode
            var origin = o.options.origin; // The origin of the sizing
            var original = {height: el.height(), width: el.width()}; // Save original
            el.from = o.options.from || original; // Default from state
            el.to = o.options.to || original; // Default to state
            // Adjust
            if (origin) { // Calculate baseline shifts
                var baseline = $.effects.getBaseline(origin, original);
                el.from.top = (original.height - el.from.height) * baseline.y;
                el.from.left = (original.width - el.from.width) * baseline.x;
                el.to.top = (original.height - el.to.height) * baseline.y;
                el.to.left = (original.width - el.to.width) * baseline.x;
            };
            var factor = { // Set scaling factor
                from: {y: el.from.height / original.height, x: el.from.width / original.width},
                to: {y: el.to.height / original.height, x: el.to.width / original.width}
            };
            if (scale == 'box' || scale == 'both') { // Scale the css box
                if (factor.from.y != factor.to.y) { // Vertical props scaling
                    props = props.concat(vProps);
                    el.from = $.effects.setTransition(el, vProps, factor.from.y, el.from);
                    el.to = $.effects.setTransition(el, vProps, factor.to.y, el.to);
                };
                if (factor.from.x != factor.to.x) { // Horizontal props scaling
                    props = props.concat(hProps);
                    el.from = $.effects.setTransition(el, hProps, factor.from.x, el.from);
                    el.to = $.effects.setTransition(el, hProps, factor.to.x, el.to);
                };
            };
            if (scale == 'content' || scale == 'both') { // Scale the content
                if (factor.from.y != factor.to.y) { // Vertical props scaling
                    props = props.concat(cProps);
                    el.from = $.effects.setTransition(el, cProps, factor.from.y, el.from);
                    el.to = $.effects.setTransition(el, cProps, factor.to.y, el.to);
                };
            };
            $.effects.save(el, restore ? props : props1); el.show(); // Save & Show
            $.effects.createWrapper(el); // Create Wrapper
            el.css('overflow','hidden').css(el.from); // Shift

            // Animate
            if (scale == 'content' || scale == 'both') { // Scale the children
                vProps = vProps.concat(['marginTop','marginBottom']).concat(cProps); // Add margins/font-size
                hProps = hProps.concat(['marginLeft','marginRight']); // Add margins
                props2 = props.concat(vProps).concat(hProps); // Concat
                el.find("*[width]").each(function(){
                    child = $(this);
                    if (restore) $.effects.save(child, props2);
                    var c_original = {height: child.height(), width: child.width()}; // Save original
                    child.from = {height: c_original.height * factor.from.y, width: c_original.width * factor.from.x};
                    child.to = {height: c_original.height * factor.to.y, width: c_original.width * factor.to.x};
                    if (factor.from.y != factor.to.y) { // Vertical props scaling
                        child.from = $.effects.setTransition(child, vProps, factor.from.y, child.from);
                        child.to = $.effects.setTransition(child, vProps, factor.to.y, child.to);
                    };
                    if (factor.from.x != factor.to.x) { // Horizontal props scaling
                        child.from = $.effects.setTransition(child, hProps, factor.from.x, child.from);
                        child.to = $.effects.setTransition(child, hProps, factor.to.x, child.to);
                    };
                    child.css(child.from); // Shift children
                    child.animate(child.to, o.duration, o.options.easing, function(){
                        if (restore) $.effects.restore(child, props2); // Restore children
                    }); // Animate children
                });
            };

            // Animate
            el.animate(el.to, { queue: false, duration: o.duration, easing: o.options.easing, complete: function() {
                if (el.to.opacity === 0) {
                    el.css('opacity', el.from.opacity);
                }
                if(mode == 'hide') el.hide(); // Hide
                $.effects.restore(el, restore ? props : props1); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(this, arguments); // Callback
                el.dequeue();
            }});

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Shake 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Shake
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.shake = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'effect'); // Set Mode
            var direction = o.options.direction || 'left'; // Default direction
            var distance = o.options.distance || 20; // Default distance
            var times = o.options.times || 3; // Default # of times
            var speed = o.duration || o.options.duration || 140; // Default speed per shake

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            $.effects.createWrapper(el); // Create Wrapper
            var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left';
            var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg';

            // Animation
            var animation = {}, animation1 = {}, animation2 = {};
            animation[ref] = (motion == 'pos' ? '-=' : '+=')  + distance;
            animation1[ref] = (motion == 'pos' ? '+=' : '-=')  + distance * 2;
            animation2[ref] = (motion == 'pos' ? '-=' : '+=')  + distance * 2;

            // Animate
            el.animate(animation, speed, o.options.easing);
            for (var i = 1; i < times; i++) { // Shakes
                el.animate(animation1, speed, o.options.easing).animate(animation2, speed, o.options.easing);
            };
            el.animate(animation1, speed, o.options.easing).
            animate(animation, speed / 2, o.options.easing, function(){ // Last shake
                $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(this, arguments); // Callback
            });
            el.queue('fx', function() { el.dequeue(); });
            el.dequeue();
        });

    };

})(jQuery);
/*
 * jQuery UI Effects Slide 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Slide
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.slide = function(o) {

        return this.queue(function() {

            // Create element
            var el = $(this), props = ['position','top','bottom','left','right'];

            // Set options
            var mode = $.effects.setMode(el, o.options.mode || 'show'); // Set Mode
            var direction = o.options.direction || 'left'; // Default Direction

            // Adjust
            $.effects.save(el, props); el.show(); // Save & Show
            $.effects.createWrapper(el).css({overflow:'hidden'}); // Create Wrapper
            var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left';
            var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg';
            var distance = o.options.distance || (ref == 'top' ? el.outerHeight({margin:true}) : el.outerWidth({margin:true}));
            if (mode == 'show') el.css(ref, motion == 'pos' ? (isNaN(distance) ? "-" + distance : -distance) : distance); // Shift

            // Animation
            var animation = {};
            animation[ref] = (mode == 'show' ? (motion == 'pos' ? '+=' : '-=') : (motion == 'pos' ? '-=' : '+=')) + distance;

            // Animate
            el.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function() {
                if(mode == 'hide') el.hide(); // Hide
                $.effects.restore(el, props); $.effects.removeWrapper(el); // Restore
                if(o.callback) o.callback.apply(this, arguments); // Callback
                el.dequeue();
            }});

        });

    };

})(jQuery);
/*
 * jQuery UI Effects Transfer 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Transfer
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function( $, undefined ) {

    $.effects.transfer = function(o) {
        return this.queue(function() {
            var elem = $(this),
                target = $(o.options.to),
                endPosition = target.offset(),
                animation = {
                    top: endPosition.top,
                    left: endPosition.left,
                    height: target.innerHeight(),
                    width: target.innerWidth()
                },
                startPosition = elem.offset(),
                transfer = $('<div class="ui-effects-transfer"></div>')
                    .appendTo(document.body)
                    .addClass(o.options.className)
                    .css({
                        top: startPosition.top,
                        left: startPosition.left,
                        height: elem.innerHeight(),
                        width: elem.innerWidth(),
                        position: 'absolute'
                    })
                    .animate(animation, o.duration, o.options.easing, function() {
                        transfer.remove();
                        (o.callback && o.callback.apply(elem[0], arguments));
                        elem.dequeue();
                    });
        });
    };

})(jQuery);
/*!
 * jQuery Migrate - v1.1.1 - 2013-02-16
 * https://github.com/jquery/jquery-migrate
 * Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors; Licensed MIT
 */
(function( jQuery, window, undefined ) {
// See http://bugs.jquery.com/ticket/13335
// "use strict";


    var warnedAbout = {};

// List of warnings already given; public read only
    jQuery.migrateWarnings = [];

// Set to true to prevent console output; migrateWarnings still maintained
// jQuery.migrateMute = false;

// Show a message on the console so devs know we're active
    if ( !jQuery.migrateMute && window.console && console.log ) {
        console.log("JQMIGRATE: Logging is active");
    }

// Set to false to disable traces that appear with warnings
    if ( jQuery.migrateTrace === undefined ) {
        jQuery.migrateTrace = true;
    }

// Forget any warnings we've already given; public
    jQuery.migrateReset = function() {
        warnedAbout = {};
        jQuery.migrateWarnings.length = 0;
    };

    function migrateWarn( msg) {
        if ( !warnedAbout[ msg ] ) {
            warnedAbout[ msg ] = true;
            jQuery.migrateWarnings.push( msg );
            if ( window.console && console.warn && !jQuery.migrateMute ) {
                console.warn( "JQMIGRATE: " + msg );
                if ( jQuery.migrateTrace && console.trace ) {
                    console.trace();
                }
            }
        }
    }

    function migrateWarnProp( obj, prop, value, msg ) {
        if ( Object.defineProperty ) {
            // On ES5 browsers (non-oldIE), warn if the code tries to get prop;
            // allow property to be overwritten in case some other plugin wants it
            try {
                Object.defineProperty( obj, prop, {
                    configurable: true,
                    enumerable: true,
                    get: function() {
                        migrateWarn( msg );
                        return value;
                    },
                    set: function( newValue ) {
                        migrateWarn( msg );
                        value = newValue;
                    }
                });
                return;
            } catch( err ) {
                // IE8 is a dope about Object.defineProperty, can't warn there
            }
        }

        // Non-ES5 (or broken) browser; just set the property
        jQuery._definePropertyBroken = true;
        obj[ prop ] = value;
    }

    if ( document.compatMode === "BackCompat" ) {
        // jQuery has never supported or tested Quirks Mode
        migrateWarn( "jQuery is not compatible with Quirks Mode" );
    }


    var attrFn = jQuery( "<input/>", { size: 1 } ).attr("size") && jQuery.attrFn,
        oldAttr = jQuery.attr,
        valueAttrGet = jQuery.attrHooks.value && jQuery.attrHooks.value.get ||
            function() { return null; },
        valueAttrSet = jQuery.attrHooks.value && jQuery.attrHooks.value.set ||
            function() { return undefined; },
        rnoType = /^(?:input|button)$/i,
        rnoAttrNodeType = /^[238]$/,
        rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        ruseDefault = /^(?:checked|selected)$/i;

// jQuery.attrFn
    migrateWarnProp( jQuery, "attrFn", attrFn || {}, "jQuery.attrFn is deprecated" );

    jQuery.attr = function( elem, name, value, pass ) {
        var lowerName = name.toLowerCase(),
            nType = elem && elem.nodeType;

        if ( pass ) {
            // Since pass is used internally, we only warn for new jQuery
            // versions where there isn't a pass arg in the formal params
            if ( oldAttr.length < 4 ) {
                migrateWarn("jQuery.fn.attr( props, pass ) is deprecated");
            }
            if ( elem && !rnoAttrNodeType.test( nType ) &&
                (attrFn ? name in attrFn : jQuery.isFunction(jQuery.fn[name])) ) {
                return jQuery( elem )[ name ]( value );
            }
        }

        // Warn if user tries to set `type`, since it breaks on IE 6/7/8; by checking
        // for disconnected elements we don't warn on $( "<button>", { type: "button" } ).
        if ( name === "type" && value !== undefined && rnoType.test( elem.nodeName ) && elem.parentNode ) {
            migrateWarn("Can't change the 'type' of an input or button in IE 6/7/8");
        }

        // Restore boolHook for boolean property/attribute synchronization
        if ( !jQuery.attrHooks[ lowerName ] && rboolean.test( lowerName ) ) {
            jQuery.attrHooks[ lowerName ] = {
                get: function( elem, name ) {
                    // Align boolean attributes with corresponding properties
                    // Fall back to attribute presence where some booleans are not supported
                    var attrNode,
                        property = jQuery.prop( elem, name );
                    return property === true || typeof property !== "boolean" &&
                    ( attrNode = elem.getAttributeNode(name) ) && attrNode.nodeValue !== false ?

                        name.toLowerCase() :
                        undefined;
                },
                set: function( elem, value, name ) {
                    var propName;
                    if ( value === false ) {
                        // Remove boolean attributes when set to false
                        jQuery.removeAttr( elem, name );
                    } else {
                        // value is true since we know at this point it's type boolean and not false
                        // Set boolean attributes to the same name and set the DOM property
                        propName = jQuery.propFix[ name ] || name;
                        if ( propName in elem ) {
                            // Only set the IDL specifically if it already exists on the element
                            elem[ propName ] = true;
                        }

                        elem.setAttribute( name, name.toLowerCase() );
                    }
                    return name;
                }
            };

            // Warn only for attributes that can remain distinct from their properties post-1.9
            if ( ruseDefault.test( lowerName ) ) {
                migrateWarn( "jQuery.fn.attr('" + lowerName + "') may use property instead of attribute" );
            }
        }

        return oldAttr.call( jQuery, elem, name, value );
    };

// attrHooks: value
    jQuery.attrHooks.value = {
        get: function( elem, name ) {
            var nodeName = ( elem.nodeName || "" ).toLowerCase();
            if ( nodeName === "button" ) {
                return valueAttrGet.apply( this, arguments );
            }
            if ( nodeName !== "input" && nodeName !== "option" ) {
                migrateWarn("jQuery.fn.attr('value') no longer gets properties");
            }
            return name in elem ?
                elem.value :
                null;
        },
        set: function( elem, value ) {
            var nodeName = ( elem.nodeName || "" ).toLowerCase();
            if ( nodeName === "button" ) {
                return valueAttrSet.apply( this, arguments );
            }
            if ( nodeName !== "input" && nodeName !== "option" ) {
                migrateWarn("jQuery.fn.attr('value', val) no longer sets properties");
            }
            // Does not return so that setAttribute is also used
            elem.value = value;
        }
    };


    var matched, browser,
        oldInit = jQuery.fn.init,
        oldParseJSON = jQuery.parseJSON,
    // Note this does NOT include the #9521 XSS fix from 1.7!
        rquickExpr = /^(?:[^<]*(<[\w\W]+>)[^>]*|#([\w\-]*))$/;

// $(html) "looks like html" rule change
    jQuery.fn.init = function( selector, context, rootjQuery ) {
        var match;

        if ( selector && typeof selector === "string" && !jQuery.isPlainObject( context ) &&
            (match = rquickExpr.exec( selector )) && match[1] ) {
            // This is an HTML string according to the "old" rules; is it still?
            if ( selector.charAt( 0 ) !== "<" ) {
                migrateWarn("$(html) HTML strings must start with '<' character");
            }
            // Now process using loose rules; let pre-1.8 play too
            if ( context && context.context ) {
                // jQuery object as context; parseHTML expects a DOM object
                context = context.context;
            }
            if ( jQuery.parseHTML ) {
                return oldInit.call( this, jQuery.parseHTML( jQuery.trim(selector), context, true ),
                    context, rootjQuery );
            }
        }
        return oldInit.apply( this, arguments );
    };
    jQuery.fn.init.prototype = jQuery.fn;

// Let $.parseJSON(falsy_value) return null
    jQuery.parseJSON = function( json ) {
        if ( !json && json !== null ) {
            migrateWarn("jQuery.parseJSON requires a valid JSON string");
            return null;
        }
        return oldParseJSON.apply( this, arguments );
    };

    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

// Don't clobber any existing jQuery.browser in case it's different
    if ( !jQuery.browser ) {
        matched = jQuery.uaMatch( navigator.userAgent );
        browser = {};

        if ( matched.browser ) {
            browser[ matched.browser ] = true;
            browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if ( browser.chrome ) {
            browser.webkit = true;
        } else if ( browser.webkit ) {
            browser.safari = true;
        }

        jQuery.browser = browser;
    }

// Warn if the code tries to get jQuery.browser
    migrateWarnProp( jQuery, "browser", jQuery.browser, "jQuery.browser is deprecated" );

    jQuery.sub = function() {
        function jQuerySub( selector, context ) {
            return new jQuerySub.fn.init( selector, context );
        }
        jQuery.extend( true, jQuerySub, this );
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init( selector, context ) {
            if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                context = jQuerySub( context );
            }

            return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        migrateWarn( "jQuery.sub() is deprecated" );
        return jQuerySub;
    };


// Ensure that $.ajax gets the new parseJSON defined in core.js
    jQuery.ajaxSetup({
        converters: {
            "text json": jQuery.parseJSON
        }
    });


    var oldFnData = jQuery.fn.data;

    jQuery.fn.data = function( name ) {
        var ret, evt,
            elem = this[0];

        // Handles 1.7 which has this behavior and 1.8 which doesn't
        if ( elem && name === "events" && arguments.length === 1 ) {
            ret = jQuery.data( elem, name );
            evt = jQuery._data( elem, name );
            if ( ( ret === undefined || ret === evt ) && evt !== undefined ) {
                migrateWarn("Use of jQuery.fn.data('events') is deprecated");
                return evt;
            }
        }
        return oldFnData.apply( this, arguments );
    };


    var rscriptType = /\/(java|ecma)script/i,
        oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;

    jQuery.fn.andSelf = function() {
        migrateWarn("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()");
        return oldSelf.apply( this, arguments );
    };

// Since jQuery.clean is used internally on older versions, we only shim if it's missing
    if ( !jQuery.clean ) {
        jQuery.clean = function( elems, context, fragment, scripts ) {
            // Set context per 1.8 logic
            context = context || document;
            context = !context.nodeType && context[0] || context;
            context = context.ownerDocument || context;

            migrateWarn("jQuery.clean() is deprecated");

            var i, elem, handleScript, jsTags,
                ret = [];

            jQuery.merge( ret, jQuery.buildFragment( elems, context ).childNodes );

            // Complex logic lifted directly from jQuery 1.8
            if ( fragment ) {
                // Special handling of each script element
                handleScript = function( elem ) {
                    // Check if we consider it executable
                    if ( !elem.type || rscriptType.test( elem.type ) ) {
                        // Detach the script and store it in the scripts array (if provided) or the fragment
                        // Return truthy to indicate that it has been handled
                        return scripts ?
                            scripts.push( elem.parentNode ? elem.parentNode.removeChild( elem ) : elem ) :
                            fragment.appendChild( elem );
                    }
                };

                for ( i = 0; (elem = ret[i]) != null; i++ ) {
                    // Check if we're done after handling an executable script
                    if ( !( jQuery.nodeName( elem, "script" ) && handleScript( elem ) ) ) {
                        // Append to fragment and handle embedded scripts
                        fragment.appendChild( elem );
                        if ( typeof elem.getElementsByTagName !== "undefined" ) {
                            // handleScript alters the DOM, so use jQuery.merge to ensure snapshot iteration
                            jsTags = jQuery.grep( jQuery.merge( [], elem.getElementsByTagName("script") ), handleScript );

                            // Splice the scripts into ret after their former ancestor and advance our index beyond them
                            ret.splice.apply( ret, [i + 1, 0].concat( jsTags ) );
                            i += jsTags.length;
                        }
                    }
                }
            }

            return ret;
        };
    }

    var eventAdd = jQuery.event.add,
        eventRemove = jQuery.event.remove,
        eventTrigger = jQuery.event.trigger,
        oldToggle = jQuery.fn.toggle,
        oldLive = jQuery.fn.live,
        oldDie = jQuery.fn.die,
        ajaxEvents = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
        rajaxEvent = new RegExp( "\\b(?:" + ajaxEvents + ")\\b" ),
        rhoverHack = /(?:^|\s)hover(\.\S+|)\b/,
        hoverHack = function( events ) {
            if ( typeof( events ) !== "string" || jQuery.event.special.hover ) {
                return events;
            }
            if ( rhoverHack.test( events ) ) {
                migrateWarn("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'");
            }
            return events && events.replace( rhoverHack, "mouseenter$1 mouseleave$1" );
        };

// Event props removed in 1.9, put them back if needed; no practical way to warn them
    if ( jQuery.event.props && jQuery.event.props[ 0 ] !== "attrChange" ) {
        jQuery.event.props.unshift( "attrChange", "attrName", "relatedNode", "srcElement" );
    }

// Undocumented jQuery.event.handle was "deprecated" in jQuery 1.7
    if ( jQuery.event.dispatch ) {
        migrateWarnProp( jQuery.event, "handle", jQuery.event.dispatch, "jQuery.event.handle is undocumented and deprecated" );
    }

// Support for 'hover' pseudo-event and ajax event warnings
    jQuery.event.add = function( elem, types, handler, data, selector ){
        if ( elem !== document && rajaxEvent.test( types ) ) {
            migrateWarn( "AJAX events should be attached to document: " + types );
        }
        eventAdd.call( this, elem, hoverHack( types || "" ), handler, data, selector );
    };
    jQuery.event.remove = function( elem, types, handler, selector, mappedTypes ){
        eventRemove.call( this, elem, hoverHack( types ) || "", handler, selector, mappedTypes );
    };

    jQuery.fn.error = function() {
        var args = Array.prototype.slice.call( arguments, 0);
        migrateWarn("jQuery.fn.error() is deprecated");
        args.splice( 0, 0, "error" );
        if ( arguments.length ) {
            return this.bind.apply( this, args );
        }
        // error event should not bubble to window, although it does pre-1.7
        this.triggerHandler.apply( this, args );
        return this;
    };

    jQuery.fn.toggle = function( fn, fn2 ) {

        // Don't mess with animation or css toggles
        if ( !jQuery.isFunction( fn ) || !jQuery.isFunction( fn2 ) ) {
            return oldToggle.apply( this, arguments );
        }
        migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");

        // Save reference to arguments for access in closure
        var args = arguments,
            guid = fn.guid || jQuery.guid++,
            i = 0,
            toggler = function( event ) {
                // Figure out which function to execute
                var lastToggle = ( jQuery._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
                jQuery._data( this, "lastToggle" + fn.guid, lastToggle + 1 );

                // Make sure that clicks stop
                event.preventDefault();

                // and execute the function
                return args[ lastToggle ].apply( this, arguments ) || false;
            };

        // link all the functions, so any of them can unbind this click handler
        toggler.guid = guid;
        while ( i < args.length ) {
            args[ i++ ].guid = guid;
        }

        return this.click( toggler );
    };

    jQuery.fn.live = function( types, data, fn ) {
        migrateWarn("jQuery.fn.live() is deprecated");
        if ( oldLive ) {
            return oldLive.apply( this, arguments );
        }
        jQuery( this.context ).on( types, this.selector, data, fn );
        return this;
    };

    jQuery.fn.die = function( types, fn ) {
        migrateWarn("jQuery.fn.die() is deprecated");
        if ( oldDie ) {
            return oldDie.apply( this, arguments );
        }
        jQuery( this.context ).off( types, this.selector || "**", fn );
        return this;
    };

// Turn global events into document-triggered events
    jQuery.event.trigger = function( event, data, elem, onlyHandlers  ){
        if ( !elem && !rajaxEvent.test( event ) ) {
            migrateWarn( "Global events are undocumented and deprecated" );
        }
        return eventTrigger.call( this,  event, data, elem || document, onlyHandlers  );
    };
    jQuery.each( ajaxEvents.split("|"),
        function( _, name ) {
            jQuery.event.special[ name ] = {
                setup: function() {
                    var elem = this;

                    // The document needs no shimming; must be !== for oldIE
                    if ( elem !== document ) {
                        jQuery.event.add( document, name + "." + jQuery.guid, function() {
                            jQuery.event.trigger( name, null, elem, true );
                        });
                        jQuery._data( this, name, jQuery.guid++ );
                    }
                    return false;
                },
                teardown: function() {
                    if ( this !== document ) {
                        jQuery.event.remove( document, name + "." + jQuery._data( this, name ) );
                    }
                    return false;
                }
            };
        }
    );


})( jQuery, window );/*! jCarousel - v0.3.4 - 2015-09-23
 * http://sorgalla.com/jcarousel/
 * Copyright (c) 2006-2015 Jan Sorgalla; Licensed MIT */
!function(a){"use strict";var b=a.jCarousel={};b.version="0.3.4";var c=/^([+\-]=)?(.+)$/;b.parseTarget=function(a){var b=!1,d="object"!=typeof a?c.exec(a):null;return d?(a=parseInt(d[2],10)||0,d[1]&&(b=!0,"-="===d[1]&&(a*=-1))):"object"!=typeof a&&(a=parseInt(a,10)||0),{target:a,relative:b}},b.detectCarousel=function(a){for(var b;a.length>0;){if(b=a.filter("[data-jcarousel]"),b.length>0)return b;if(b=a.find("[data-jcarousel]"),b.length>0)return b;a=a.parent()}return null},b.base=function(c){return{version:b.version,_options:{},_element:null,_carousel:null,_init:a.noop,_create:a.noop,_destroy:a.noop,_reload:a.noop,create:function(){return this._element.attr("data-"+c.toLowerCase(),!0).data(c,this),!1===this._trigger("create")?this:(this._create(),this._trigger("createend"),this)},destroy:function(){return!1===this._trigger("destroy")?this:(this._destroy(),this._trigger("destroyend"),this._element.removeData(c).removeAttr("data-"+c.toLowerCase()),this)},reload:function(a){return!1===this._trigger("reload")?this:(a&&this.options(a),this._reload(),this._trigger("reloadend"),this)},element:function(){return this._element},options:function(b,c){if(0===arguments.length)return a.extend({},this._options);if("string"==typeof b){if("undefined"==typeof c)return"undefined"==typeof this._options[b]?null:this._options[b];this._options[b]=c}else this._options=a.extend({},this._options,b);return this},carousel:function(){return this._carousel||(this._carousel=b.detectCarousel(this.options("carousel")||this._element),this._carousel||a.error('Could not detect carousel for plugin "'+c+'"')),this._carousel},_trigger:function(b,d,e){var f,g=!1;return e=[this].concat(e||[]),(d||this._element).each(function(){f=a.Event((c+":"+b).toLowerCase()),a(this).trigger(f,e),f.isDefaultPrevented()&&(g=!0)}),!g}}},b.plugin=function(c,d){var e=a[c]=function(b,c){this._element=a(b),this.options(c),this._init(),this.create()};return e.fn=e.prototype=a.extend({},b.base(c),d),a.fn[c]=function(b){var d=Array.prototype.slice.call(arguments,1),f=this;return this.each("string"==typeof b?function(){var e=a(this).data(c);if(!e)return a.error("Cannot call methods on "+c+' prior to initialization; attempted to call method "'+b+'"');if(!a.isFunction(e[b])||"_"===b.charAt(0))return a.error('No such method "'+b+'" for '+c+" instance");var g=e[b].apply(e,d);return g!==e&&"undefined"!=typeof g?(f=g,!1):void 0}:function(){var d=a(this).data(c);d instanceof e?d.reload(b):new e(this,b)}),f},e}}(jQuery),function(a,b){"use strict";var c=function(a){return parseFloat(a)||0};a.jCarousel.plugin("jcarousel",{animating:!1,tail:0,inTail:!1,resizeTimer:null,lt:null,vertical:!1,rtl:!1,circular:!1,underflow:!1,relative:!1,_options:{list:function(){return this.element().children().eq(0)},items:function(){return this.list().children()},animation:400,transitions:!1,wrap:null,vertical:null,rtl:null,center:!1},_list:null,_items:null,_target:a(),_first:a(),_last:a(),_visible:a(),_fullyvisible:a(),_init:function(){var a=this;return this.onWindowResize=function(){a.resizeTimer&&clearTimeout(a.resizeTimer),a.resizeTimer=setTimeout(function(){a.reload()},100)},this},_create:function(){this._reload(),a(b).on("resize.jcarousel",this.onWindowResize)},_destroy:function(){a(b).off("resize.jcarousel",this.onWindowResize)},_reload:function(){this.vertical=this.options("vertical"),null==this.vertical&&(this.vertical=this.list().height()>this.list().width()),this.rtl=this.options("rtl"),null==this.rtl&&(this.rtl=function(b){if("rtl"===(""+b.attr("dir")).toLowerCase())return!0;var c=!1;return b.parents("[dir]").each(function(){return/rtl/i.test(a(this).attr("dir"))?(c=!0,!1):void 0}),c}(this._element)),this.lt=this.vertical?"top":"left",this.relative="relative"===this.list().css("position"),this._list=null,this._items=null;var b=this.index(this._target)>=0?this._target:this.closest();this.circular="circular"===this.options("wrap"),this.underflow=!1;var c={left:0,top:0};return b.length>0&&(this._prepare(b),this.list().find("[data-jcarousel-clone]").remove(),this._items=null,this.underflow=this._fullyvisible.length>=this.items().length,this.circular=this.circular&&!this.underflow,c[this.lt]=this._position(b)+"px"),this.move(c),this},list:function(){if(null===this._list){var b=this.options("list");this._list=a.isFunction(b)?b.call(this):this._element.find(b)}return this._list},items:function(){if(null===this._items){var b=this.options("items");this._items=(a.isFunction(b)?b.call(this):this.list().find(b)).not("[data-jcarousel-clone]")}return this._items},index:function(a){return this.items().index(a)},closest:function(){var b,d=this,e=this.list().position()[this.lt],f=a(),g=!1,h=this.vertical?"bottom":this.rtl&&!this.relative?"left":"right";return this.rtl&&this.relative&&!this.vertical&&(e+=this.list().width()-this.clipping()),this.items().each(function(){if(f=a(this),g)return!1;var i=d.dimension(f);if(e+=i,e>=0){if(b=i-c(f.css("margin-"+h)),!(Math.abs(e)-i+b/2<=0))return!1;g=!0}}),f},target:function(){return this._target},first:function(){return this._first},last:function(){return this._last},visible:function(){return this._visible},fullyvisible:function(){return this._fullyvisible},hasNext:function(){if(!1===this._trigger("hasnext"))return!0;var a=this.options("wrap"),b=this.items().length-1,c=this.options("center")?this._target:this._last;return b>=0&&!this.underflow&&(a&&"first"!==a||this.index(c)<b||this.tail&&!this.inTail)?!0:!1},hasPrev:function(){if(!1===this._trigger("hasprev"))return!0;var a=this.options("wrap");return this.items().length>0&&!this.underflow&&(a&&"last"!==a||this.index(this._first)>0||this.tail&&this.inTail)?!0:!1},clipping:function(){return this._element["inner"+(this.vertical?"Height":"Width")]()},dimension:function(a){return a["outer"+(this.vertical?"Height":"Width")](!0)},scroll:function(b,c,d){if(this.animating)return this;if(!1===this._trigger("scroll",null,[b,c]))return this;a.isFunction(c)&&(d=c,c=!0);var e=a.jCarousel.parseTarget(b);if(e.relative){var f,g,h,i,j,k,l,m,n=this.items().length-1,o=Math.abs(e.target),p=this.options("wrap");if(e.target>0){var q=this.index(this._last);if(q>=n&&this.tail)this.inTail?"both"===p||"last"===p?this._scroll(0,c,d):a.isFunction(d)&&d.call(this,!1):this._scrollTail(c,d);else if(f=this.index(this._target),this.underflow&&f===n&&("circular"===p||"both"===p||"last"===p)||!this.underflow&&q===n&&("both"===p||"last"===p))this._scroll(0,c,d);else if(h=f+o,this.circular&&h>n){for(m=n,j=this.items().get(-1);m++<h;)j=this.items().eq(0),k=this._visible.index(j)>=0,k&&j.after(j.clone(!0).attr("data-jcarousel-clone",!0)),this.list().append(j),k||(l={},l[this.lt]=this.dimension(j),this.moveBy(l)),this._items=null;this._scroll(j,c,d)}else this._scroll(Math.min(h,n),c,d)}else if(this.inTail)this._scroll(Math.max(this.index(this._first)-o+1,0),c,d);else if(g=this.index(this._first),f=this.index(this._target),i=this.underflow?f:g,h=i-o,0>=i&&(this.underflow&&"circular"===p||"both"===p||"first"===p))this._scroll(n,c,d);else if(this.circular&&0>h){for(m=h,j=this.items().get(0);m++<0;){j=this.items().eq(-1),k=this._visible.index(j)>=0,k&&j.after(j.clone(!0).attr("data-jcarousel-clone",!0)),this.list().prepend(j),this._items=null;var r=this.dimension(j);l={},l[this.lt]=-r,this.moveBy(l)}this._scroll(j,c,d)}else this._scroll(Math.max(h,0),c,d)}else this._scroll(e.target,c,d);return this._trigger("scrollend"),this},moveBy:function(a,b){var d=this.list().position(),e=1,f=0;return this.rtl&&!this.vertical&&(e=-1,this.relative&&(f=this.list().width()-this.clipping())),a.left&&(a.left=d.left+f+c(a.left)*e+"px"),a.top&&(a.top=d.top+f+c(a.top)*e+"px"),this.move(a,b)},move:function(b,c){c=c||{};var d=this.options("transitions"),e=!!d,f=!!d.transforms,g=!!d.transforms3d,h=c.duration||0,i=this.list();if(!e&&h>0)return void i.animate(b,c);var j=c.complete||a.noop,k={};if(e){var l={transitionDuration:i.css("transitionDuration"),transitionTimingFunction:i.css("transitionTimingFunction"),transitionProperty:i.css("transitionProperty")},m=j;j=function(){a(this).css(l),m.call(this)},k={transitionDuration:(h>0?h/1e3:0)+"s",transitionTimingFunction:d.easing||c.easing,transitionProperty:h>0?function(){return f||g?"all":b.left?"left":"top"}():"none",transform:"none"}}g?k.transform="translate3d("+(b.left||0)+","+(b.top||0)+",0)":f?k.transform="translate("+(b.left||0)+","+(b.top||0)+")":a.extend(k,b),e&&h>0&&i.one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",j),i.css(k),0>=h&&i.each(function(){j.call(this)})},_scroll:function(b,c,d){if(this.animating)return a.isFunction(d)&&d.call(this,!1),this;if("object"!=typeof b?b=this.items().eq(b):"undefined"==typeof b.jquery&&(b=a(b)),0===b.length)return a.isFunction(d)&&d.call(this,!1),this;this.inTail=!1,this._prepare(b);var e=this._position(b),f=this.list().position()[this.lt];if(e===f)return a.isFunction(d)&&d.call(this,!1),this;var g={};return g[this.lt]=e+"px",this._animate(g,c,d),this},_scrollTail:function(b,c){if(this.animating||!this.tail)return a.isFunction(c)&&c.call(this,!1),this;var d=this.list().position()[this.lt];this.rtl&&this.relative&&!this.vertical&&(d+=this.list().width()-this.clipping()),this.rtl&&!this.vertical?d+=this.tail:d-=this.tail,this.inTail=!0;var e={};return e[this.lt]=d+"px",this._update({target:this._target.next(),fullyvisible:this._fullyvisible.slice(1).add(this._visible.last())}),this._animate(e,b,c),this},_animate:function(b,c,d){if(d=d||a.noop,!1===this._trigger("animate"))return d.call(this,!1),this;this.animating=!0;var e=this.options("animation"),f=a.proxy(function(){this.animating=!1;var a=this.list().find("[data-jcarousel-clone]");a.length>0&&(a.remove(),this._reload()),this._trigger("animateend"),d.call(this,!0)},this),g="object"==typeof e?a.extend({},e):{duration:e},h=g.complete||a.noop;return c===!1?g.duration=0:"undefined"!=typeof a.fx.speeds[g.duration]&&(g.duration=a.fx.speeds[g.duration]),g.complete=function(){f(),h.call(this)},this.move(b,g),this},_prepare:function(b){var d,e,f,g,h=this.index(b),i=h,j=this.dimension(b),k=this.clipping(),l=this.vertical?"bottom":this.rtl?"left":"right",m=this.options("center"),n={target:b,first:b,last:b,visible:b,fullyvisible:k>=j?b:a()};if(m&&(j/=2,k/=2),k>j)for(;;){if(d=this.items().eq(++i),0===d.length){if(!this.circular)break;if(d=this.items().eq(0),b.get(0)===d.get(0))break;if(e=this._visible.index(d)>=0,e&&d.after(d.clone(!0).attr("data-jcarousel-clone",!0)),this.list().append(d),!e){var o={};o[this.lt]=this.dimension(d),this.moveBy(o)}this._items=null}if(g=this.dimension(d),0===g)break;if(j+=g,n.last=d,n.visible=n.visible.add(d),f=c(d.css("margin-"+l)),k>=j-f&&(n.fullyvisible=n.fullyvisible.add(d)),j>=k)break}if(!this.circular&&!m&&k>j)for(i=h;;){if(--i<0)break;if(d=this.items().eq(i),0===d.length)break;if(g=this.dimension(d),0===g)break;if(j+=g,n.first=d,n.visible=n.visible.add(d),f=c(d.css("margin-"+l)),k>=j-f&&(n.fullyvisible=n.fullyvisible.add(d)),j>=k)break}return this._update(n),this.tail=0,m||"circular"===this.options("wrap")||"custom"===this.options("wrap")||this.index(n.last)!==this.items().length-1||(j-=c(n.last.css("margin-"+l)),j>k&&(this.tail=j-k)),this},_position:function(a){var b=this._first,c=b.position()[this.lt],d=this.options("center"),e=d?this.clipping()/2-this.dimension(b)/2:0;return this.rtl&&!this.vertical?(c-=this.relative?this.list().width()-this.dimension(b):this.clipping()-this.dimension(b),c+=e):c-=e,!d&&(this.index(a)>this.index(b)||this.inTail)&&this.tail?(c=this.rtl&&!this.vertical?c-this.tail:c+this.tail,this.inTail=!0):this.inTail=!1,-c},_update:function(b){var c,d=this,e={target:this._target,first:this._first,last:this._last,visible:this._visible,fullyvisible:this._fullyvisible},f=this.index(b.first||e.first)<this.index(e.first),g=function(c){var g=[],h=[];b[c].each(function(){e[c].index(this)<0&&g.push(this)}),e[c].each(function(){b[c].index(this)<0&&h.push(this)}),f?g=g.reverse():h=h.reverse(),d._trigger(c+"in",a(g)),d._trigger(c+"out",a(h)),d["_"+c]=b[c]};for(c in b)g(c);return this}})}(jQuery,window),function(a){"use strict";a.jcarousel.fn.scrollIntoView=function(b,c,d){var e,f=a.jCarousel.parseTarget(b),g=this.index(this._fullyvisible.first()),h=this.index(this._fullyvisible.last());if(e=f.relative?f.target<0?Math.max(0,g+f.target):h+f.target:"object"!=typeof f.target?f.target:this.index(f.target),g>e)return this.scroll(e,c,d);if(e>=g&&h>=e)return a.isFunction(d)&&d.call(this,!1),this;for(var i,j=this.items(),k=this.clipping(),l=this.vertical?"bottom":this.rtl?"left":"right",m=0;;){if(i=j.eq(e),0===i.length)break;if(m+=this.dimension(i),m>=k){var n=parseFloat(i.css("margin-"+l))||0;m-n!==k&&e++;break}if(0>=e)break;e--}return this.scroll(e,c,d)}}(jQuery),function(a){"use strict";a.jCarousel.plugin("jcarouselControl",{_options:{target:"+=1",event:"click",method:"scroll"},_active:null,_init:function(){this.onDestroy=a.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",a.proxy(this._create,this))},this),this.onReload=a.proxy(this._reload,this),this.onEvent=a.proxy(function(b){b.preventDefault();var c=this.options("method");a.isFunction(c)?c.call(this):this.carousel().jcarousel(this.options("method"),this.options("target"))},this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy).on("jcarousel:reloadend jcarousel:scrollend",this.onReload),this._element.on(this.options("event")+".jcarouselcontrol",this.onEvent),this._reload()},_destroy:function(){this._element.off(".jcarouselcontrol",this.onEvent),this.carousel().off("jcarousel:destroy",this.onDestroy).off("jcarousel:reloadend jcarousel:scrollend",this.onReload)},_reload:function(){var b,c=a.jCarousel.parseTarget(this.options("target")),d=this.carousel();if(c.relative)b=d.jcarousel(c.target>0?"hasNext":"hasPrev");else{var e="object"!=typeof c.target?d.jcarousel("items").eq(c.target):c.target;b=d.jcarousel("target").index(e)>=0}return this._active!==b&&(this._trigger(b?"active":"inactive"),this._active=b),this}})}(jQuery),function(a){"use strict";a.jCarousel.plugin("jcarouselPagination",{_options:{perPage:null,item:function(a){return'<a href="#'+a+'">'+a+"</a>"},event:"click",method:"scroll"},_carouselItems:null,_pages:{},_items:{},_currentPage:null,_init:function(){this.onDestroy=a.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",a.proxy(this._create,this))},this),this.onReload=a.proxy(this._reload,this),this.onScroll=a.proxy(this._update,this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy).on("jcarousel:reloadend",this.onReload).on("jcarousel:scrollend",this.onScroll),this._reload()},_destroy:function(){this._clear(),this.carousel().off("jcarousel:destroy",this.onDestroy).off("jcarousel:reloadend",this.onReload).off("jcarousel:scrollend",this.onScroll),this._carouselItems=null},_reload:function(){var b=this.options("perPage");if(this._pages={},this._items={},a.isFunction(b)&&(b=b.call(this)),null==b)this._pages=this._calculatePages();else for(var c,d=parseInt(b,10)||0,e=this._getCarouselItems(),f=1,g=0;;){if(c=e.eq(g++),0===c.length)break;this._pages[f]=this._pages[f]?this._pages[f].add(c):c,g%d===0&&f++}this._clear();var h=this,i=this.carousel().data("jcarousel"),j=this._element,k=this.options("item"),l=this._getCarouselItems().length;a.each(this._pages,function(b,c){var d=h._items[b]=a(k.call(h,b,c));d.on(h.options("event")+".jcarouselpagination",a.proxy(function(){var a=c.eq(0);if(i.circular){var d=i.index(i.target()),e=i.index(a);parseFloat(b)>parseFloat(h._currentPage)?d>e&&(a="+="+(l-d+e)):e>d&&(a="-="+(d+(l-e)))}i[this.options("method")](a)},h)),j.append(d)}),this._update()},_update:function(){var b,c=this.carousel().jcarousel("target");a.each(this._pages,function(a,d){return d.each(function(){return c.is(this)?(b=a,!1):void 0}),b?!1:void 0}),this._currentPage!==b&&(this._trigger("inactive",this._items[this._currentPage]),this._trigger("active",this._items[b])),this._currentPage=b},items:function(){return this._items},reloadCarouselItems:function(){return this._carouselItems=null,this},_clear:function(){this._element.empty(),this._currentPage=null},_calculatePages:function(){for(var a,b,c=this.carousel().data("jcarousel"),d=this._getCarouselItems(),e=c.clipping(),f=0,g=0,h=1,i={};;){if(a=d.eq(g++),0===a.length)break;b=c.dimension(a),f+b>e&&(h++,f=0),f+=b,i[h]=i[h]?i[h].add(a):a}return i},_getCarouselItems:function(){return this._carouselItems||(this._carouselItems=this.carousel().jcarousel("items")),this._carouselItems}})}(jQuery),function(a,b){"use strict";var c,d,e={hidden:"visibilitychange",mozHidden:"mozvisibilitychange",msHidden:"msvisibilitychange",webkitHidden:"webkitvisibilitychange"};a.each(e,function(a,e){return"undefined"!=typeof b[a]?(c=a,d=e,!1):void 0}),a.jCarousel.plugin("jcarouselAutoscroll",{_options:{target:"+=1",interval:3e3,autostart:!0},_timer:null,_started:!1,_init:function(){this.onDestroy=a.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",a.proxy(this._create,this))},this),this.onAnimateEnd=a.proxy(this._start,this),this.onVisibilityChange=a.proxy(function(){b[c]?this._stop():this._start()},this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy),a(b).on(d,this.onVisibilityChange),this.options("autostart")&&this.start()},_destroy:function(){this._stop(),this.carousel().off("jcarousel:destroy",this.onDestroy),a(b).off(d,this.onVisibilityChange)},_start:function(){return this._stop(),this._started?(this.carousel().one("jcarousel:animateend",this.onAnimateEnd),this._timer=setTimeout(a.proxy(function(){this.carousel().jcarousel("scroll",this.options("target"))},this),this.options("interval")),this):void 0},_stop:function(){return this._timer&&(this._timer=clearTimeout(this._timer)),this.carousel().off("jcarousel:animateend",this.onAnimateEnd),this},start:function(){return this._started=!0,this._start(),this},stop:function(){return this._started=!1,this._stop(),this}})}(jQuery,document);/**
 * jQuery Validation Plugin 1.8.1
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function($) {

    $.extend($.fn, {
        // http://docs.jquery.com/Plugins/Validation/validate
        validate: function( options ) {

            // if nothing is selected, return nothing; can't chain anyway
            if (!this.length) {
                options && options.debug && window.console && console.warn( "nothing selected, can't validate, returning nothing" );
                return;
            }

            // check if a validator for this form was already created
            var validator = $.data(this[0], 'validator');
            if ( validator ) {
                return validator;
            }

            validator = new $.validator( options, this[0] );
            $.data(this[0], 'validator', validator);

            if ( validator.settings.onsubmit ) {

                // allow suppresing validation by adding a cancel class to the submit button
                this.find("input, button").filter(".cancel").click(function() {
                    validator.cancelSubmit = true;
                });

                // when a submitHandler is used, capture the submitting button
                if (validator.settings.submitHandler) {
                    this.find("input, button").filter(":submit").click(function() {
                        validator.submitButton = this;
                    });
                }

                // validate the form on submit
                this.submit( function( event ) {
                    if ( validator.settings.debug )
                    // prevent form submit to be able to see console output
                        event.preventDefault();

                    function handle() {
                        if ( validator.settings.submitHandler ) {
                            if (validator.submitButton) {
                                // insert a hidden input as a replacement for the missing submit button
                                var hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);
                            }
                            validator.settings.submitHandler.call( validator, validator.currentForm );
                            if (validator.submitButton) {
                                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                                hidden.remove();
                            }
                            return false;
                        }
                        return true;
                    }

                    // prevent submit for invalid forms or custom submit handlers
                    if ( validator.cancelSubmit ) {
                        validator.cancelSubmit = false;
                        return handle();
                    }
                    if ( validator.form() ) {
                        if ( validator.pendingRequest ) {
                            validator.formSubmitted = true;
                            return false;
                        }
                        return handle();
                    } else {
                        validator.focusInvalid();
                        return false;
                    }
                });
            }

            return validator;
        },
        // http://docs.jquery.com/Plugins/Validation/valid
        valid: function() {
            if ( $(this[0]).is('form')) {
                return this.validate().form();
            } else {
                var valid = true;
                var validator = $(this[0].form).validate();
                this.each(function() {
                    valid &= validator.element(this);
                });
                return valid;
            }
        },
        // attributes: space seperated list of attributes to retrieve and remove
        removeAttrs: function(attributes) {
            var result = {},
                $element = this;
            $.each(attributes.split(/\s/), function(index, value) {
                result[value] = $element.attr(value);
                $element.removeAttr(value);
            });
            return result;
        },
        // http://docs.jquery.com/Plugins/Validation/rules
        rules: function(command, argument) {
            var element = this[0];

            if (command) {
                var settings = $.data(element.form, 'validator').settings;
                var staticRules = settings.rules;
                var existingRules = $.validator.staticRules(element);
                switch(command) {
                    case "add":
                        $.extend(existingRules, $.validator.normalizeRule(argument));
                        staticRules[element.name] = existingRules;
                        if (argument.messages)
                            settings.messages[element.name] = $.extend( settings.messages[element.name], argument.messages );
                        break;
                    case "remove":
                        if (!argument) {
                            delete staticRules[element.name];
                            return existingRules;
                        }
                        var filtered = {};
                        $.each(argument.split(/\s/), function(index, method) {
                            filtered[method] = existingRules[method];
                            delete existingRules[method];
                        });
                        return filtered;
                }
            }

            var data = $.validator.normalizeRules(
                $.extend(
                    {},
                    $.validator.metadataRules(element),
                    $.validator.classRules(element),
                    $.validator.attributeRules(element),
                    $.validator.staticRules(element)
                ), element);

            // make sure required is at front
            if (data.required) {
                var param = data.required;
                delete data.required;
                data = $.extend({required: param}, data);
            }

            return data;
        }
    });

// Custom selectors
    $.extend($.expr[":"], {
        // http://docs.jquery.com/Plugins/Validation/blank
        blank: function(a) {return !$.trim("" + a.value);},
        // http://docs.jquery.com/Plugins/Validation/filled
        filled: function(a) {return !!$.trim("" + a.value);},
        // http://docs.jquery.com/Plugins/Validation/unchecked
        unchecked: function(a) {return !a.checked;}
    });

// constructor for validator
    $.validator = function( options, form ) {
        this.settings = $.extend( true, {}, $.validator.defaults, options );
        this.currentForm = form;
        this.init();
    };

    $.validator.format = function(source, params) {
        if ( arguments.length == 1 )
            return function() {
                var args = $.makeArray(arguments);
                args.unshift(source);
                return $.validator.format.apply( this, args );
            };
        if ( arguments.length > 2 && params.constructor != Array  ) {
            params = $.makeArray(arguments).slice(1);
        }
        if ( params.constructor != Array ) {
            params = [ params ];
        }
        $.each(params, function(i, n) {
            source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
        });
        return source;
    };

    $.extend($.validator, {

        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: $( [] ),
            errorLabelContainer: $( [] ),
            onsubmit: true,
            ignore: [],
            ignoreTitle: false,
            onfocusin: function(element) {
                this.lastActive = element;

                // hide error label and remove error class on focus if enabled
                if ( this.settings.focusCleanup && !this.blockFocusCleanup ) {
                    this.settings.unhighlight && this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
                    this.addWrapper(this.errorsFor(element)).hide();
                }
            },
            onfocusout: function(element) {
                if ( !this.checkable(element) && (element.name in this.submitted || !this.optional(element)) ) {
                    this.element(element);
                }
            },
            onkeyup: function(element) {
                if ( element.name in this.submitted || element == this.lastElement ) {
                    this.element(element);
                }
            },
            onclick: function(element) {
                // click on selects, radiobuttons and checkboxes
                if ( element.name in this.submitted )
                    this.element(element);
                // or option elements, check parent select in that case
                else if (element.parentNode.name in this.submitted)
                    this.element(element.parentNode);
            },
            highlight: function(element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).addClass(errorClass).removeClass(validClass);
                } else {
                    $(element).addClass(errorClass).removeClass(validClass);
                }
            },
            unhighlight: function(element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).removeClass(errorClass).addClass(validClass);
                } else {
                    $(element).removeClass(errorClass).addClass(validClass);
                }
            }
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/setDefaults
        setDefaults: function(settings) {
            $.extend( $.validator.defaults, settings );
        },

        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: $.validator.format("Please enter no more than {0} characters."),
            minlength: $.validator.format("Please enter at least {0} characters."),
            rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
            range: $.validator.format("Please enter a value between {0} and {1}."),
            max: $.validator.format("Please enter a value less than or equal to {0}."),
            min: $.validator.format("Please enter a value greater than or equal to {0}.")
        },

        autoCreateRanges: false,

        prototype: {

            init: function() {
                this.labelContainer = $(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
                this.containers = $(this.settings.errorContainer).add( this.settings.errorLabelContainer );
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();

                var groups = (this.groups = {});
                $.each(this.settings.groups, function(key, value) {
                    $.each(value.split(/\s/), function(index, name) {
                        groups[name] = key;
                    });
                });
                var rules = this.settings.rules;
                $.each(rules, function(key, value) {
                    rules[key] = $.validator.normalizeRule(value);
                });

                function delegate(event) {
                    var validator = $.data(this[0].form, "validator"),
                        eventType = "on" + event.type.replace(/^validate/, "");
                    validator.settings[eventType] && validator.settings[eventType].call(validator, this[0] );
                }
                $(this.currentForm)
                    .validateDelegate(":text, :password, :file, select, textarea", "focusin focusout keyup", delegate)
                    .validateDelegate(":radio, :checkbox, select, option", "click", delegate);

                if (this.settings.invalidHandler)
                    $(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/form
            form: function() {
                this.checkForm();
                $.extend(this.submitted, this.errorMap);
                this.invalid = $.extend({}, this.errorMap);
                if (!this.valid())
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                this.showErrors();
                return this.valid();
            },

            checkForm: function() {
                this.prepareForm();
                for ( var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++ ) {
                    this.check( elements[i] );
                }
                return this.valid();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/element
            element: function( element ) {
                element = this.clean( element );
                this.lastElement = element;
                this.prepareElement( element );
                this.currentElements = $(element);
                var result = this.check( element );
                if ( result ) {
                    delete this.invalid[element.name];
                } else {
                    this.invalid[element.name] = true;
                }
                if ( !this.numberOfInvalids() ) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add( this.containers );
                }
                this.showErrors();
                return result;
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/showErrors
            showErrors: function(errors) {
                if(errors) {
                    // add items to error list and map
                    $.extend( this.errorMap, errors );
                    this.errorList = [];
                    for ( var name in errors ) {
                        this.errorList.push({
                            message: errors[name],
                            element: this.findByName(name)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = $.grep( this.successList, function(element) {
                        return !(element.name in errors);
                    });
                }
                this.settings.showErrors
                    ? this.settings.showErrors.call( this, this.errorMap, this.errorList )
                    : this.defaultShowErrors();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/resetForm
            resetForm: function() {
                if ( $.fn.resetForm )
                    $( this.currentForm ).resetForm();
                this.submitted = {};
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass( this.settings.errorClass );
            },

            numberOfInvalids: function() {
                return this.objectLength(this.invalid);
            },

            objectLength: function( obj ) {
                var count = 0;
                for ( var i in obj )
                    count++;
                return count;
            },

            hideErrors: function() {
                this.addWrapper( this.toHide ).hide();
            },

            valid: function() {
                return this.size() == 0;
            },

            size: function() {
                return this.errorList.length;
            },

            focusInvalid: function() {
                if( this.settings.focusInvalid ) {
                    try {
                        $(this.findLastActive() || this.errorList.length && this.errorList[0].element || [])
                            .filter(":visible")
                            .focus()
                            // manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
                            .trigger("focusin");
                    } catch(e) {
                        // ignore IE throwing errors when focusing hidden elements
                    }
                }
            },

            findLastActive: function() {
                var lastActive = this.lastActive;
                return lastActive && $.grep(this.errorList, function(n) {
                        return n.element.name == lastActive.name;
                    }).length == 1 && lastActive;
            },

            elements: function() {
                var validator = this,
                    rulesCache = {};

                // select all valid inputs inside the form (no submit or reset buttons)
                return $(this.currentForm)
                    .find("input, select, textarea")
                    .not(":submit, :reset, :image, [disabled]")
                    .not( this.settings.ignore )
                    .filter(function() {
                        !this.name && validator.settings.debug && window.console && console.error( "%o has no name assigned", this);

                        // select only the first element for each name, and only those with rules specified
                        if ( this.name in rulesCache || !validator.objectLength($(this).rules()) )
                            return false;

                        rulesCache[this.name] = true;
                        return true;
                    });
            },

            clean: function( selector ) {
                return $( selector )[0];
            },

            errors: function() {
                return $( this.settings.errorElement + "." + this.settings.errorClass, this.errorContext );
            },

            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = $([]);
                this.toHide = $([]);
                this.currentElements = $([]);
            },

            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add( this.containers );
            },

            prepareElement: function( element ) {
                this.reset();
                this.toHide = this.errorsFor(element);
            },

            check: function( element ) {
                element = this.clean( element );

                // if radio/checkbox, validate first element in group instead
                if (this.checkable(element)) {
                    element = this.findByName( element.name ).not(this.settings.ignore)[0];
                }

                var rules = $(element).rules();
                var dependencyMismatch = false;
                for (var method in rules ) {
                    var rule = { method: method, parameters: rules[method] };
                    try {
                        var result = $.validator.methods[method].call( this, element.value.replace(/\r/g, ""), element, rule.parameters );

                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if ( result == "dependency-mismatch" ) {
                            dependencyMismatch = true;
                            continue;
                        }
                        dependencyMismatch = false;

                        if ( result == "pending" ) {
                            this.toHide = this.toHide.not( this.errorsFor(element) );
                            return;
                        }

                        if( !result ) {
                            this.formatAndAdd( element, rule );
                            return false;
                        }
                    } catch(e) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + element.id
                            + ", check the '" + rule.method + "' method", e);
                        throw e;
                    }
                }
                if (dependencyMismatch)
                    return;
                if ( this.objectLength(rules) )
                    this.successList.push(element);
                return true;
            },

            // return the custom message for the given element and validation method
            // specified in the element's "messages" metadata
            customMetaMessage: function(element, method) {
                if (!$.metadata)
                    return;

                var meta = this.settings.meta
                    ? $(element).metadata()[this.settings.meta]
                    : $(element).metadata();

                return meta && meta.messages && meta.messages[method];
            },

            // return the custom message for the given element name and validation method
            customMessage: function( name, method ) {
                var m = this.settings.messages[name];
                return m && (m.constructor == String
                        ? m
                        : m[method]);
            },

            // return the first defined argument, allowing empty strings
            findDefined: function() {
                for(var i = 0; i < arguments.length; i++) {
                    if (arguments[i] !== undefined)
                        return arguments[i];
                }
                return undefined;
            },

            defaultMessage: function( element, method) {
                return this.findDefined(
                    this.customMessage( element.name, method ),
                    this.customMetaMessage( element, method ),
                    // title is never undefined, so handle empty string as undefined
                    !this.settings.ignoreTitle && element.title || undefined,
                    $.validator.messages[method],
                    "<strong>Warning: No message defined for " + element.name + "</strong>"
                );
            },

            formatAndAdd: function( element, rule ) {
                var message = this.defaultMessage( element, rule.method ),
                    theregex = /\$?\{(\d+)\}/g;
                if ( typeof message == "function" ) {
                    message = message.call(this, rule.parameters, element);
                } else if (theregex.test(message)) {
                    message = jQuery.format(message.replace(theregex, '{$1}'), rule.parameters);
                }
                this.errorList.push({
                    message: message,
                    element: element
                });

                this.errorMap[element.name] = message;
                this.submitted[element.name] = message;
            },

            addWrapper: function(toToggle) {
                if ( this.settings.wrapper )
                    toToggle = toToggle.add( toToggle.parent( this.settings.wrapper ) );
                return toToggle;
            },

            defaultShowErrors: function() {
                for ( var i = 0; this.errorList[i]; i++ ) {
                    var error = this.errorList[i];
                    this.settings.highlight && this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
                    this.showLabel( error.element, error.message );
                }
                if( this.errorList.length ) {
                    this.toShow = this.toShow.add( this.containers );
                }
                if (this.settings.success) {
                    for ( var i = 0; this.successList[i]; i++ ) {
                        this.showLabel( this.successList[i] );
                    }
                }
                if (this.settings.unhighlight) {
                    for ( var i = 0, elements = this.validElements(); elements[i]; i++ ) {
                        this.settings.unhighlight.call( this, elements[i], this.settings.errorClass, this.settings.validClass );
                    }
                }
                this.toHide = this.toHide.not( this.toShow );
                this.hideErrors();
                this.addWrapper( this.toShow ).show();
            },

            validElements: function() {
                return this.currentElements.not(this.invalidElements());
            },

            invalidElements: function() {
                return $(this.errorList).map(function() {
                    return this.element;
                });
            },

            showLabel: function(element, message) {
                var label = this.errorsFor( element );
                if ( label.length ) {
                    // refresh error/success class
                    label.removeClass().addClass( this.settings.errorClass );

                    // check if we have a generated label, replace the message then
                    label.attr("generated") && label.html(message);
                } else {
                    // create label
                    label = $("<" + this.settings.errorElement + "/>")
                        .attr({"for":  this.idOrName(element), generated: true})
                        .addClass(this.settings.errorClass)
                        .html(message || "");
                    if ( this.settings.wrapper ) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        label = label.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if ( !this.labelContainer.append(label).length )
                        this.settings.errorPlacement
                            ? this.settings.errorPlacement(label, $(element) )
                            : label.insertAfter(element);
                }
                if ( !message && this.settings.success ) {
                    label.text("");
                    typeof this.settings.success == "string"
                        ? label.addClass( this.settings.success )
                        : this.settings.success( label );
                }
                this.toShow = this.toShow.add(label);
            },

            errorsFor: function(element) {
                var name = this.idOrName(element);
                return this.errors().filter(function() {
                    return $(this).attr('for') == name;
                });
            },

            idOrName: function(element) {
                return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
            },

            checkable: function( element ) {
                return /radio|checkbox/i.test(element.type);
            },

            findByName: function( name ) {
                // select by name and filter by form for performance over form.find("[name=...]")
                var form = this.currentForm;
                return $(document.getElementsByName(name)).map(function(index, element) {
                    return element.form == form && element.name == name && element  || null;
                });
            },

            getLength: function(value, element) {
                switch( element.nodeName.toLowerCase() ) {
                    case 'select':
                        return $("option:selected", element).length;
                    case 'input':
                        if( this.checkable( element) )
                            return this.findByName(element.name).filter(':checked').length;
                }
                return value.length;
            },

            depend: function(param, element) {
                return this.dependTypes[typeof param]
                    ? this.dependTypes[typeof param](param, element)
                    : true;
            },

            dependTypes: {
                "boolean": function(param, element) {
                    return param;
                },
                "string": function(param, element) {
                    return !!$(param, element.form).length;
                },
                "function": function(param, element) {
                    return param(element);
                }
            },

            optional: function(element) {
                return !$.validator.methods.required.call(this, $.trim(element.value), element) && "dependency-mismatch";
            },

            startRequest: function(element) {
                if (!this.pending[element.name]) {
                    this.pendingRequest++;
                    this.pending[element.name] = true;
                }
            },

            stopRequest: function(element, valid) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0)
                    this.pendingRequest = 0;
                delete this.pending[element.name];
                if ( valid && this.pendingRequest == 0 && this.formSubmitted && this.form() ) {
                    $(this.currentForm).submit();
                    this.formSubmitted = false;
                } else if (!valid && this.pendingRequest == 0 && this.formSubmitted) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                    this.formSubmitted = false;
                }
            },

            previousValue: function(element) {
                return $.data(element, "previousValue") || $.data(element, "previousValue", {
                        old: null,
                        valid: true,
                        message: this.defaultMessage( element, "remote" )
                    });
            }

        },

        classRuleSettings: {
            required: {required: true},
            email: {email: true},
            url: {url: true},
            date: {date: true},
            dateISO: {dateISO: true},
            dateDE: {dateDE: true},
            number: {number: true},
            numberDE: {numberDE: true},
            digits: {digits: true},
            creditcard: {creditcard: true}
        },

        addClassRules: function(className, rules) {
            className.constructor == String ?
                this.classRuleSettings[className] = rules :
                $.extend(this.classRuleSettings, className);
        },

        classRules: function(element) {
            var rules = {};
            var classes = $(element).attr('class');
            classes && $.each(classes.split(' '), function() {
                if (this in $.validator.classRuleSettings) {
                    $.extend(rules, $.validator.classRuleSettings[this]);
                }
            });
            return rules;
        },

        attributeRules: function(element) {
            var rules = {};
            var $element = $(element);

            for (var method in $.validator.methods) {
                var value = $element.attr(method);
                if (value) {
                    rules[method] = value;
                }
            }

            // maxlength may be returned as -1, 2147483647 (IE) and 524288 (safari) for text inputs
            if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
                delete rules.maxlength;
            }

            return rules;
        },

        metadataRules: function(element) {
            if (!$.metadata) return {};

            var meta = $.data(element.form, 'validator').settings.meta;
            return meta ?
                $(element).metadata()[meta] :
                $(element).metadata();
        },

        staticRules: function(element) {
            var rules = {};
            var validator = $.data(element.form, 'validator');
            if (validator.settings.rules) {
                rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
            }
            return rules;
        },

        normalizeRules: function(rules, element) {
            // handle dependency check
            $.each(rules, function(prop, val) {
                // ignore rule when param is explicitly false, eg. required:false
                if (val === false) {
                    delete rules[prop];
                    return;
                }
                if (val.param || val.depends) {
                    var keepRule = true;
                    switch (typeof val.depends) {
                        case "string":
                            keepRule = !!$(val.depends, element.form).length;
                            break;
                        case "function":
                            keepRule = val.depends.call(element, element);
                            break;
                    }
                    if (keepRule) {
                        rules[prop] = val.param !== undefined ? val.param : true;
                    } else {
                        delete rules[prop];
                    }
                }
            });

            // evaluate parameters
            $.each(rules, function(rule, parameter) {
                rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
            });

            // clean number parameters
            $.each(['minlength', 'maxlength', 'min', 'max'], function() {
                if (rules[this]) {
                    rules[this] = Number(rules[this]);
                }
            });
            $.each(['rangelength', 'range'], function() {
                if (rules[this]) {
                    rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
                }
            });

            if ($.validator.autoCreateRanges) {
                // auto-create ranges
                if (rules.min && rules.max) {
                    rules.range = [rules.min, rules.max];
                    delete rules.min;
                    delete rules.max;
                }
                if (rules.minlength && rules.maxlength) {
                    rules.rangelength = [rules.minlength, rules.maxlength];
                    delete rules.minlength;
                    delete rules.maxlength;
                }
            }

            // To support custom messages in metadata ignore rule methods titled "messages"
            if (rules.messages) {
                delete rules.messages;
            }

            return rules;
        },

        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function(data) {
            if( typeof data == "string" ) {
                var transformed = {};
                $.each(data.split(/\s/), function() {
                    transformed[this] = true;
                });
                data = transformed;
            }
            return data;
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/addMethod
        addMethod: function(name, method, message) {
            $.validator.methods[name] = method;
            $.validator.messages[name] = message != undefined ? message : $.validator.messages[name];
            if (method.length < 3) {
                $.validator.addClassRules(name, $.validator.normalizeRule(name));
            }
        },

        methods: {

            // http://docs.jquery.com/Plugins/Validation/Methods/required
            required: function(value, element, param) {
                // check if dependency is met
                if ( !this.depend(param, element) )
                    return "dependency-mismatch";
                switch( element.nodeName.toLowerCase() ) {
                    case 'select':
                        // could be an array for select-multiple or a string, both are fine this way
                        var val = $(element).val();
                        return val && val.length > 0;
                    case 'input':
                        if ( this.checkable(element) )
                            return this.getLength(value, element) > 0;
                    default:
                        return $.trim(value).length > 0;
                }
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/remote
            remote: function(value, element, param) {
                if ( this.optional(element) )
                    return "dependency-mismatch";

                var previous = this.previousValue(element);
                if (!this.settings.messages[element.name] )
                    this.settings.messages[element.name] = {};
                previous.originalMessage = this.settings.messages[element.name].remote;
                this.settings.messages[element.name].remote = previous.message;

                param = typeof param == "string" && {url:param} || param;

                if ( this.pending[element.name] ) {
                    return "pending";
                }
                if ( previous.old === value ) {
                    return previous.valid;
                }

                previous.old = value;
                var validator = this;
                this.startRequest(element);
                var data = {};
                data[element.name] = value;
                $.ajax($.extend(true, {
                    url: param,
                    mode: "abort",
                    port: "validate" + element.name,
                    dataType: "json",
                    data: data,
                    success: function(response) {
                        validator.settings.messages[element.name].remote = previous.originalMessage;
                        var valid = response === true;
                        if ( valid ) {
                            var submitted = validator.formSubmitted;
                            validator.prepareElement(element);
                            validator.formSubmitted = submitted;
                            validator.successList.push(element);
                            validator.showErrors();
                        } else {
                            var errors = {};
                            var message = response || validator.defaultMessage( element, "remote" );
                            errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
                            validator.showErrors(errors);
                        }
                        previous.valid = valid;
                        validator.stopRequest(element, valid);
                    }
                }, param));
                return "pending";
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/minlength
            minlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/maxlength
            maxlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/rangelength
            rangelength: function(value, element, param) {
                var length = this.getLength($.trim(value), element);
                return this.optional(element) || ( length >= param[0] && length <= param[1] );
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/min
            min: function( value, element, param ) {
                return this.optional(element) || value >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/max
            max: function( value, element, param ) {
                return this.optional(element) || value <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/range
            range: function( value, element, param ) {
                return this.optional(element) || ( value >= param[0] && value <= param[1] );
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/email
            email: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/url
            url: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
                return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/date
            date: function(value, element) {
                return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/dateISO
            dateISO: function(value, element) {
                return this.optional(element) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/number
            number: function(value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/digits
            digits: function(value, element) {
                return this.optional(element) || /^\d+$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/creditcard
            // based on http://en.wikipedia.org/wiki/Luhn
            creditcard: function(value, element) {
                if ( this.optional(element) )
                    return "dependency-mismatch";
                // accept only digits and dashes
                if (/[^0-9-]+/.test(value))
                    return false;
                var nCheck = 0,
                    nDigit = 0,
                    bEven = false;

                value = value.replace(/\D/g, "");

                for (var n = value.length - 1; n >= 0; n--) {
                    var cDigit = value.charAt(n);
                    var nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9)
                            nDigit -= 9;
                    }
                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return (nCheck % 10) == 0;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/accept
            accept: function(value, element, param) {
                param = typeof param == "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/equalTo
            equalTo: function(value, element, param) {
                // bind to the blur event of the target in order to revalidate whenever the target field is updated
                // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
                var target = $(param).unbind(".validate-equalTo").bind("blur.validate-equalTo", function() {
                    $(element).valid();
                });
                return value == target.val();
            }

        }

    });

// deprecated, use $.validator.format instead
    $.format = $.validator.format;

})(jQuery);

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
;(function($) {
    var pendingRequests = {};
    // Use a prefilter if available (1.5+)
    if ( $.ajaxPrefilter ) {
        $.ajaxPrefilter(function(settings, _, xhr) {
            var port = settings.port;
            if (settings.mode == "abort") {
                if ( pendingRequests[port] ) {
                    pendingRequests[port].abort();
                }
                pendingRequests[port] = xhr;
            }
        });
    } else {
        // Proxy ajax
        var ajax = $.ajax;
        $.ajax = function(settings) {
            var mode = ( "mode" in settings ? settings : $.ajaxSettings ).mode,
                port = ( "port" in settings ? settings : $.ajaxSettings ).port;
            if (mode == "abort") {
                if ( pendingRequests[port] ) {
                    pendingRequests[port].abort();
                }
                return (pendingRequests[port] = ajax.apply(this, arguments));
            }
            return ajax.apply(this, arguments);
        };
    }
})(jQuery);

// provides cross-browser focusin and focusout events
// IE has native support, in other browsers, use event caputuring (neither bubbles)

// provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
// handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target
;(function($) {
    // only implement if not provided by jQuery core (since 1.4)
    // TODO verify if jQuery 1.4's implementation is compatible with older jQuery special-event APIs
    if (!jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener) {
        $.each({
            focus: 'focusin',
            blur: 'focusout'
        }, function( original, fix ){
            $.event.special[fix] = {
                setup:function() {
                    this.addEventListener( original, handler, true );
                },
                teardown:function() {
                    this.removeEventListener( original, handler, true );
                },
                handler: function(e) {
                    arguments[0] = $.event.fix(e);
                    arguments[0].type = fix;
                    return $.event.handle.apply(this, arguments);
                }
            };
            function handler(e) {
                e = $.event.fix(e);
                e.type = fix;
                return $.event.handle.call(this, e);
            }
        });
    };
    $.extend($.fn, {
        validateDelegate: function(delegate, type, handler) {
            return this.bind(type, function(event) {
                var target = $(event.target);
                if (target.is(delegate)) {
                    return handler.apply(target, arguments);
                }
            });
        }
    });
})(jQuery);
// StyleFix Dynamic DOM plugin
(function(self){

    if(!self) {
        return;
    }

    self.events = {
        DOMNodeInserted: function(evt) {
            var node = evt.target, tag = node.nodeName;

            if(node.nodeType != 1) {
                return;
            }

            if(/link/i.test(tag)) {
                self.link(node);
            }
            else if(/style/i.test(tag)) {
                self.styleElement(node);
            }
            else if (node.hasAttribute('style')) {
                self.styleAttribute(node);
            }
        },

        DOMAttrModified: function(evt) {
            if(evt.attrName === 'style') {
                document.removeEventListener('DOMAttrModified', self.events.DOMAttrModified, false);
                self.styleAttribute(evt.target);
                document.addEventListener('DOMAttrModified', self.events.DOMAttrModified, false);
            }
        }
    };

    document.addEventListener('DOMContentLoaded', function() {
        // Listen for new <link> and <style> elements
        document.addEventListener('DOMNodeInserted', self.events.DOMNodeInserted, false);

        // Listen for style attribute changes
        document.addEventListener('DOMAttrModified', self.events.DOMAttrModified, false);
    }, false);

})(window.StyleFix);

// PrefixFree CSSOM plugin
(function(self){

    if(!self) {
        return;
    }

// Add accessors for CSSOM property changes
    if(window.CSSStyleDeclaration) {
        for(var i=0; i<self.properties.length; i++) {
            var property = StyleFix.camelCase(self.properties[i]),
                prefixed = self.prefixProperty(property, true),
                proto = CSSStyleDeclaration.prototype,
                getter,
                setter;

            // Lowercase prefix for IE
            if(!(prefixed in proto)) {
                prefixed = prefixed.charAt(0).toLowerCase() + prefixed.slice(1);
                if(!(prefixed in proto)) {
                    continue;
                }
            }

            getter = (function(prefixed) {
                return function() {
                    return this[prefixed];
                }
            })(prefixed);
            setter = (function(prefixed) {
                return function(value) {
                    this[prefixed] = value;
                }
            })(prefixed);

            if(Object.defineProperty) {
                Object.defineProperty(proto, property, {
                    get: getter,
                    set: setter,
                    enumerable: true,
                    configurable: true
                });
            }
            else if(proto.__defineGetter__) {
                proto.__defineGetter__(property, getter);
                proto.__defineSetter__(property, setter);
            }
        }
    }

})(window.PrefixFree);

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options = $.extend({}, options); // clone object since it's unexpected behavior if the expired property were changed
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // NOTE Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};/*
 * jQuery postMessage - v0.5 - 9/11/2009
 * http://benalman.com/projects/jquery-postmessage-plugin/
 *
 * Copyright (c) 2009 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($){var g,d,j=1,a,b=this,f=!1,h="postMessage",e="addEventListener",c,i=b[h]&&!$.browser.opera;$[h]=function(k,l,m){if(!l){return}k=typeof k==="string"?k:$.param(k);m=m||parent;if(i){m[h](k,l.replace(/([^:]+:\/\/[^\/]+).*/,"$1"))}else{if(l){m.location=l.replace(/#.*$/,"")+"#"+(+new Date)+(j++)+"&"+k}}};$.receiveMessage=c=function(l,m,k){if(i){if(l){a&&c();a=function(n){if((typeof m==="string"&&n.origin!==m)||($.isFunction(m)&&m(n.origin)===f)){return f}l(n)}}if(b[e]){b[l?e:"removeEventListener"]("message",a,f)}else{b[l?"attachEvent":"detachEvent"]("onmessage",a)}}else{g&&clearInterval(g);g=null;if(l){k=typeof m==="number"?m:typeof k==="number"?k:100;g=setInterval(function(){var o=document.location.hash,n=/^#?\d+&/;if(o!==d&&n.test(o)){d=o;l({data:o.replace(n,"")})}},k)}}}})(jQuery);/*!
 * jQuery blockUI plugin
 * Version 2.70.0-2014.11.23
 * Requires jQuery v1.7 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */

;(function() {
    /*jshint eqeqeq:false curly:false latedef:false */
    "use strict";

    function setup($) {
        $.fn._fadeIn = $.fn.fadeIn;

        var noOp = $.noop || function() {};

        // this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
        // confusing userAgent strings on Vista)
        var msie = /MSIE/.test(navigator.userAgent);
        var ie6  = /MSIE 6.0/.test(navigator.userAgent) && ! /MSIE 8.0/.test(navigator.userAgent);
        var mode = document.documentMode || 0;
        var setExpr = $.isFunction( document.createElement('div').style.setExpression );

        // global $ methods for blocking/unblocking the entire page
        $.blockUI   = function(opts) { install(window, opts); };
        $.unblockUI = function(opts) { remove(window, opts); };

        // convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
        $.growlUI = function(title, message, timeout, onClose) {
            var $m = $('<div class="growlUI"></div>');
            if (title) $m.append('<h1>'+title+'</h1>');
            if (message) $m.append('<h2>'+message+'</h2>');
            if (timeout === undefined) timeout = 3000;

            // Added by konapun: Set timeout to 30 seconds if this growl is moused over, like normal toast notifications
            var callBlock = function(opts) {
                opts = opts || {};

                $.blockUI({
                    message: $m,
                    fadeIn : typeof opts.fadeIn  !== 'undefined' ? opts.fadeIn  : 700,
                    fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
                    timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
                    centerY: false,
                    showOverlay: false,
                    onUnblock: onClose,
                    css: $.blockUI.defaults.growlCSS
                });
            };

            callBlock();
            var nonmousedOpacity = $m.css('opacity');
            $m.mouseover(function() {
                callBlock({
                    fadeIn: 0,
                    timeout: 30000
                });

                var displayBlock = $('.blockMsg');
                displayBlock.stop(); // cancel fadeout if it has started
                displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
            }).mouseout(function() {
                $('.blockMsg').fadeOut(1000);
            });
            // End konapun additions
        };

        // plugin method for blocking element content
        $.fn.block = function(opts) {
            if ( this[0] === window ) {
                $.blockUI( opts );
                return this;
            }
            var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
            this.each(function() {
                var $el = $(this);
                if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
                    return;
                $el.unblock({ fadeOut: 0 });
            });

            return this.each(function() {
                if ($.css(this,'position') == 'static') {
                    this.style.position = 'relative';
                    $(this).data('blockUI.static', true);
                }
                this.style.zoom = 1; // force 'hasLayout' in ie
                install(this, opts);
            });
        };

        // plugin method for unblocking element content
        $.fn.unblock = function(opts) {
            if ( this[0] === window ) {
                $.unblockUI( opts );
                return this;
            }
            return this.each(function() {
                remove(this, opts);
            });
        };

        $.blockUI.version = 2.70; // 2nd generation blocking at no extra cost!

        // override these in your code to change the default behavior and style
        $.blockUI.defaults = {
            // message displayed when blocking (use null for no message)
            message:  '<h1>Please wait...</h1>',

            title: null,		// title string; only used when theme == true
            draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)

            theme: false, // set to true to use with jQuery UI themes

            // styles for the message when blocking; if you wish to disable
            // these and use an external stylesheet then do this in your code:
            // $.blockUI.defaults.css = {};
            css: {
                padding:	0,
                margin:		0,
                width:		'30%',
                top:		'40%',
                left:		'35%',
                textAlign:	'center',
                color:		'#000',
                border:		'3px solid #aaa',
                backgroundColor:'#fff',
                cursor:		'wait'
            },

            // minimal style set used when themes are used
            themedCSS: {
                width:	'30%',
                top:	'40%',
                left:	'35%'
            },

            // styles for the overlay
            overlayCSS:  {
                backgroundColor:	'#000',
                opacity:			0.6,
                cursor:				'wait'
            },

            // style to replace wait cursor before unblocking to correct issue
            // of lingering wait cursor
            cursorReset: 'default',

            // styles applied when using $.growlUI
            growlCSS: {
                width:		'350px',
                top:		'10px',
                left:		'',
                right:		'10px',
                border:		'none',
                padding:	'5px',
                opacity:	0.6,
                cursor:		'default',
                color:		'#fff',
                backgroundColor: '#000',
                '-webkit-border-radius':'10px',
                '-moz-border-radius':	'10px',
                'border-radius':		'10px'
            },

            // IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
            // (hat tip to Jorge H. N. de Vasconcelos)
            /*jshint scripturl:true */
            iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

            // force usage of iframe in non-IE browsers (handy for blocking applets)
            forceIframe: false,

            // z-index for the blocking overlay
            baseZ: 1000,

            // set these to true to have the message automatically centered
            centerX: true, // <-- only effects element blocking (page block controlled via css above)
            centerY: true,

            // allow body element to be stetched in ie6; this makes blocking look better
            // on "short" pages.  disable if you wish to prevent changes to the body height
            allowBodyStretch: true,

            // enable if you want key and mouse events to be disabled for content that is blocked
            bindEvents: true,

            // be default blockUI will supress tab navigation from leaving blocking content
            // (if bindEvents is true)
            constrainTabKey: true,

            // fadeIn time in millis; set to 0 to disable fadeIn on block
            fadeIn:  200,

            // fadeOut time in millis; set to 0 to disable fadeOut on unblock
            fadeOut:  400,

            // time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
            timeout: 0,

            // disable if you don't want to show the overlay
            showOverlay: true,

            // if true, focus will be placed in the first available input field when
            // page blocking
            focusInput: true,

            // elements that can receive focus
            focusableElements: ':input:enabled:visible',

            // suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
            // no longer needed in 2012
            // applyPlatformOpacityRules: true,

            // callback method invoked when fadeIn has completed and blocking message is visible
            onBlock: null,

            // callback method invoked when unblocking has completed; the callback is
            // passed the element that has been unblocked (which is the window object for page
            // blocks) and the options that were passed to the unblock call:
            //	onUnblock(element, options)
            onUnblock: null,

            // callback method invoked when the overlay area is clicked.
            // setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
            onOverlayClick: null,

            // don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
            quirksmodeOffsetHack: 4,

            // class name of the message block
            blockMsgClass: 'blockMsg',

            // if it is already blocked, then ignore it (don't unblock and reblock)
            ignoreIfBlocked: false
        };

        // private data and functions follow...

        var pageBlock = null;
        var pageBlockEls = [];

        function install(el, opts) {
            var css, themedCSS;
            var full = (el == window);
            var msg = (opts && opts.message !== undefined ? opts.message : undefined);
            opts = $.extend({}, $.blockUI.defaults, opts || {});

            if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
                return;

            opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
            css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
            if (opts.onOverlayClick)
                opts.overlayCSS.cursor = 'pointer';

            themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
            msg = msg === undefined ? opts.message : msg;

            // remove the current block (if there is one)
            if (full && pageBlock)
                remove(window, {fadeOut:0});

            // if an existing element is being used as the blocking content then we capture
            // its current place in the DOM (and current display style) so we can restore
            // it when we unblock
            if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
                var node = msg.jquery ? msg[0] : msg;
                var data = {};
                $(el).data('blockUI.history', data);
                data.el = node;
                data.parent = node.parentNode;
                data.display = node.style.display;
                data.position = node.style.position;
                if (data.parent)
                    data.parent.removeChild(node);
            }

            $(el).data('blockUI.onUnblock', opts.onUnblock);
            var z = opts.baseZ;

            // blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
            // layer1 is the iframe layer which is used to supress bleed through of underlying content
            // layer2 is the overlay layer which has opacity and a wait cursor (by default)
            // layer3 is the message content that is displayed while blocking
            var lyr1, lyr2, lyr3, s;
            if (msie || opts.forceIframe)
                lyr1 = $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>');
            else
                lyr1 = $('<div class="blockUI" style="display:none"></div>');

            if (opts.theme)
                lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>');
            else
                lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

            if (opts.theme && full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">';
                if ( opts.title ) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            }
            else if (opts.theme) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">';
                if ( opts.title ) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            }
            else if (full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
            }
            else {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
            }
            lyr3 = $(s);

            // if we have a message, style it
            if (msg) {
                if (opts.theme) {
                    lyr3.css(themedCSS);
                    lyr3.addClass('ui-widget-content');
                }
                else
                    lyr3.css(css);
            }

            // style the overlay
            if (!opts.theme /*&& (!opts.applyPlatformOpacityRules)*/)
                lyr2.css(opts.overlayCSS);
            lyr2.css('position', full ? 'fixed' : 'absolute');

            // make iframe layer transparent in IE
            if (msie || opts.forceIframe)
                lyr1.css('opacity',0.0);

            //$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
            var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
            $.each(layers, function() {
                this.appendTo($par);
            });

            if (opts.theme && opts.draggable && $.fn.draggable) {
                lyr3.draggable({
                    handle: '.ui-dialog-titlebar',
                    cancel: 'li'
                });
            }

            // ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
            var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
            if (ie6 || expr) {
                // give body 100% height
                if (full && opts.allowBodyStretch && $.support.boxModel)
                    $('html,body').css('height','100%');

                // fix ie6 issue when blocked element has a border width
                if ((ie6 || !$.support.boxModel) && !full) {
                    var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
                    var fixT = t ? '(0 - '+t+')' : 0;
                    var fixL = l ? '(0 - '+l+')' : 0;
                }

                // simulate fixed position
                $.each(layers, function(i,o) {
                    var s = o[0].style;
                    s.position = 'absolute';
                    if (i < 2) {
                        if (full)
                            s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"');
                        else
                            s.setExpression('height','this.parentNode.offsetHeight + "px"');
                        if (full)
                            s.setExpression('width','jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
                        else
                            s.setExpression('width','this.parentNode.offsetWidth + "px"');
                        if (fixL) s.setExpression('left', fixL);
                        if (fixT) s.setExpression('top', fixT);
                    }
                    else if (opts.centerY) {
                        if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
                        s.marginTop = 0;
                    }
                    else if (!opts.centerY && full) {
                        var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
                        var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
                        s.setExpression('top',expression);
                    }
                });
            }

            // show the message
            if (msg) {
                if (opts.theme)
                    lyr3.find('.ui-widget-content').append(msg);
                else
                    lyr3.append(msg);
                if (msg.jquery || msg.nodeType)
                    $(msg).show();
            }

            if ((msie || opts.forceIframe) && opts.showOverlay)
                lyr1.show(); // opacity is zero
            if (opts.fadeIn) {
                var cb = opts.onBlock ? opts.onBlock : noOp;
                var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
                var cb2 = msg ? cb : noOp;
                if (opts.showOverlay)
                    lyr2._fadeIn(opts.fadeIn, cb1);
                if (msg)
                    lyr3._fadeIn(opts.fadeIn, cb2);
            }
            else {
                if (opts.showOverlay)
                    lyr2.show();
                if (msg)
                    lyr3.show();
                if (opts.onBlock)
                    opts.onBlock.bind(lyr3)();
            }

            // bind key and mouse events
            bind(1, el, opts);

            if (full) {
                pageBlock = lyr3[0];
                pageBlockEls = $(opts.focusableElements,pageBlock);
                if (opts.focusInput)
                    setTimeout(focus, 20);
            }
            else
                center(lyr3[0], opts.centerX, opts.centerY);

            if (opts.timeout) {
                // auto-unblock
                var to = setTimeout(function() {
                    if (full)
                        $.unblockUI(opts);
                    else
                        $(el).unblock(opts);
                }, opts.timeout);
                $(el).data('blockUI.timeout', to);
            }
        }

        // remove the block
        function remove(el, opts) {
            var count;
            var full = (el == window);
            var $el = $(el);
            var data = $el.data('blockUI.history');
            var to = $el.data('blockUI.timeout');
            if (to) {
                clearTimeout(to);
                $el.removeData('blockUI.timeout');
            }
            opts = $.extend({}, $.blockUI.defaults, opts || {});
            bind(0, el, opts); // unbind events

            if (opts.onUnblock === null) {
                opts.onUnblock = $el.data('blockUI.onUnblock');
                $el.removeData('blockUI.onUnblock');
            }

            var els;
            if (full) // crazy selector to handle odd field errors in ie6/7
                els = $('body').children().filter('.blockUI').add('body > .blockUI');
            else
                els = $el.find('>.blockUI');

            // fix cursor issue
            if ( opts.cursorReset ) {
                if ( els.length > 1 )
                    els[1].style.cursor = opts.cursorReset;
                if ( els.length > 2 )
                    els[2].style.cursor = opts.cursorReset;
            }

            if (full)
                pageBlock = pageBlockEls = null;

            if (opts.fadeOut) {
                count = els.length;
                els.stop().fadeOut(opts.fadeOut, function() {
                    if ( --count === 0)
                        reset(els,data,opts,el);
                });
            }
            else
                reset(els, data, opts, el);
        }

        // move blocking element back into the DOM where it started
        function reset(els,data,opts,el) {
            var $el = $(el);
            if ( $el.data('blockUI.isBlocked') )
                return;

            els.each(function(i,o) {
                // remove via DOM calls so we don't lose event handlers
                if (this.parentNode)
                    this.parentNode.removeChild(this);
            });

            if (data && data.el) {
                data.el.style.display = data.display;
                data.el.style.position = data.position;
                data.el.style.cursor = 'default'; // #59
                if (data.parent)
                    data.parent.appendChild(data.el);
                $el.removeData('blockUI.history');
            }

            if ($el.data('blockUI.static')) {
                $el.css('position', 'static'); // #22
            }

            if (typeof opts.onUnblock == 'function')
                opts.onUnblock(el,opts);

            // fix issue in Safari 6 where block artifacts remain until reflow
            var body = $(document.body), w = body.width(), cssW = body[0].style.width;
            body.width(w-1).width(w);
            body[0].style.width = cssW;
        }

        // bind/unbind the handler
        function bind(b, el, opts) {
            var full = el == window, $el = $(el);

            // don't bother unbinding if there is nothing to unbind
            if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
                return;

            $el.data('blockUI.isBlocked', b);

            // don't bind events when overlay is not in use or if bindEvents is false
            if (!full || !opts.bindEvents || (b && !opts.showOverlay))
                return;

            // bind anchors and inputs for mouse and key events
            var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
            if (b)
                $(document).bind(events, opts, handler);
            else
                $(document).unbind(events, handler);

            // former impl...
            //		var $e = $('a,:input');
            //		b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
        }

        // event handler to suppress keyboard/mouse events when blocking
        function handler(e) {
            // allow tab navigation (conditionally)
            if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
                if (pageBlock && e.data.constrainTabKey) {
                    var els = pageBlockEls;
                    var fwd = !e.shiftKey && e.target === els[els.length-1];
                    var back = e.shiftKey && e.target === els[0];
                    if (fwd || back) {
                        setTimeout(function(){focus(back);},10);
                        return false;
                    }
                }
            }
            var opts = e.data;
            var target = $(e.target);
            if (target.hasClass('blockOverlay') && opts.onOverlayClick)
                opts.onOverlayClick(e);

            // allow events within the message content
            if (target.parents('div.' + opts.blockMsgClass).length > 0)
                return true;

            // allow events for content that is not being blocked
            return target.parents().children().filter('div.blockUI').length === 0;
        }

        function focus(back) {
            if (!pageBlockEls)
                return;
            var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
            if (e)
                e.focus();
        }

        function center(el, x, y) {
            var p = el.parentNode, s = el.style;
            var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
            var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
            if (x) s.left = l > 0 ? (l+'px') : '0';
            if (y) s.top  = t > 0 ? (t+'px') : '0';
        }

        function sz(el, p) {
            return parseInt($.css(el,p),10)||0;
        }

    }


    /*global define:true */
    if (typeof define === 'function' && define.amd && define.amd.jQuery) {
        define(['jquery'], setup);
    } else {
        setup(jQuery);
    }

})();/*! modernizr 3.2.0 (Custom Build) | MIT *
 * http://modernizr.com/download/?-cssvhunit-cssvminunit-cssvwunit-inlinesvg-mediaqueries-svg-svgasimg-setclasses !*/
!function(e,t,n){function i(e,t){return typeof e===t}function o(){var e,t,n,o,r,s,a;for(var l in c)if(c.hasOwnProperty(l)){if(e=[],t=c[l],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(o=i(t.fn,"function")?t.fn():t.fn,r=0;r<e.length;r++)s=e[r],a=s.split("."),1===a.length?Modernizr[a[0]]=o:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=o),f.push((o?"":"no-")+a.join("-"))}}function r(e){var t=h.className,n=Modernizr._config.classPrefix||"";if(m&&(t=t.baseVal),Modernizr._config.enableJSClass){var i=new RegExp("(^|\\s)"+n+"no-js(\\s|$)");t=t.replace(i,"$1"+n+"js$2")}Modernizr._config.enableClasses&&(t+=" "+n+e.join(" "+n),m?h.className.baseVal=t:h.className=t)}function s(e,t){return e-1===t||e===t||e+1===t}function a(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):m?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function l(e,t){if("object"==typeof e)for(var n in e)g(e,n)&&l(n,e[n]);else{e=e.toLowerCase();var i=e.split("."),o=Modernizr[i[0]];if(2==i.length&&(o=o[i[1]]),"undefined"!=typeof o)return Modernizr;t="function"==typeof t?t():t,1==i.length?Modernizr[i[0]]=t:(!Modernizr[i[0]]||Modernizr[i[0]]instanceof Boolean||(Modernizr[i[0]]=new Boolean(Modernizr[i[0]])),Modernizr[i[0]][i[1]]=t),r([(t&&0!=t?"":"no-")+i.join("-")]),Modernizr._trigger(e,t)}return Modernizr}function d(){var e=t.body;return e||(e=a(m?"svg":"body"),e.fake=!0),e}function u(e,n,i,o){var r,s,l,u,f="modernizr",c=a("div"),p=d();if(parseInt(i,10))for(;i--;)l=a("div"),l.id=o?o[i]:f+(i+1),c.appendChild(l);return r=a("style"),r.type="text/css",r.id="s"+f,(p.fake?p:c).appendChild(r),p.appendChild(c),r.styleSheet?r.styleSheet.cssText=e:r.appendChild(t.createTextNode(e)),c.id=f,p.fake&&(p.style.background="",p.style.overflow="hidden",u=h.style.overflow,h.style.overflow="hidden",h.appendChild(p)),s=n(c,e),p.fake?(p.parentNode.removeChild(p),h.style.overflow=u,h.offsetHeight):c.parentNode.removeChild(c),!!s}var f=[],c=[],p={_version:"3.2.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){c.push({name:e,fn:t,options:n})},addAsyncTest:function(e){c.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=p,Modernizr=new Modernizr,Modernizr.addTest("svg",!!t.createElementNS&&!!t.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect);var h=t.documentElement,m="svg"===h.nodeName.toLowerCase();Modernizr.addTest("inlinesvg",function(){var e=a("div");return e.innerHTML="<svg/>","http://www.w3.org/2000/svg"==("undefined"!=typeof SVGRect&&e.firstChild&&e.firstChild.namespaceURI)});var g;!function(){var e={}.hasOwnProperty;g=i(e,"undefined")||i(e.call,"undefined")?function(e,t){return t in e&&i(e.constructor.prototype[t],"undefined")}:function(t,n){return e.call(t,n)}}(),p._l={},p.on=function(e,t){this._l[e]||(this._l[e]=[]),this._l[e].push(t),Modernizr.hasOwnProperty(e)&&setTimeout(function(){Modernizr._trigger(e,Modernizr[e])},0)},p._trigger=function(e,t){if(this._l[e]){var n=this._l[e];setTimeout(function(){var e,i;for(e=0;e<n.length;e++)(i=n[e])(t)},0),delete this._l[e]}},Modernizr._q.push(function(){p.addTest=l}),Modernizr.addTest("svgasimg",t.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1"));var v=p.testStyles=u;v("#modernizr { height: 50vh; }",function(t){var n=parseInt(e.innerHeight/2,10),i=parseInt((e.getComputedStyle?getComputedStyle(t,null):t.currentStyle).height,10);Modernizr.addTest("cssvhunit",i==n)}),v("#modernizr1{width: 50vm;width:50vmin}#modernizr2{width:50px;height:50px;overflow:scroll}#modernizr3{position:fixed;top:0;left:0;bottom:0;right:0}",function(t){var n=t.childNodes[2],i=t.childNodes[1],o=t.childNodes[0],r=parseInt((i.offsetWidth-i.clientWidth)/2,10),a=o.clientWidth/100,l=o.clientHeight/100,d=parseInt(50*Math.min(a,l),10),u=parseInt((e.getComputedStyle?getComputedStyle(n,null):n.currentStyle).width,10);Modernizr.addTest("cssvminunit",s(d,u)||s(d,u-r))},3),v("#modernizr { width: 50vw; }",function(t){var n=parseInt(e.innerWidth/2,10),i=parseInt((e.getComputedStyle?getComputedStyle(t,null):t.currentStyle).width,10);Modernizr.addTest("cssvwunit",i==n)});var w=function(){var t=e.matchMedia||e.msMatchMedia;return t?function(e){var n=t(e);return n&&n.matches||!1}:function(t){var n=!1;return u("@media "+t+" { #modernizr { position: absolute; } }",function(t){n="absolute"==(e.getComputedStyle?e.getComputedStyle(t,null):t.currentStyle).position}),n}}();p.mq=w,Modernizr.addTest("mediaqueries",w("only all")),o(),r(f),delete p.addTest,delete p.addAsyncTest;for(var y=0;y<Modernizr._q.length;y++)Modernizr._q[y]();e.Modernizr=Modernizr}(window,document);/*
 * jQuery Scroll Indicator 2016 by Matthias Giger
 * http://naminho.ch/scroll-indicator
 * @author Matthias Giger <matthias.giger@namics.com>
 */
(function($) {

    $.fn.indicate = function(options) {

        if (this.length === 0)
            return;

        if (!$('head #scroll-indicator-animations').length) {
            // Source code found in jquery.indicate.src.css
            $('<style id="scroll-indicator-animations">@-webkit-keyframes right{from{-webkit-transform:translate(0);transform:translate(0)}to{-webkit-transform:translate(20px);transform:translate(20px);opacity:0;filter:alpha(opacity=0)}}@keyframes right{from{-webkit-transform:translate(0);transform:translate(0)}to{-webkit-transform:translate(20px);transform:translate(20px);opacity:0;filter:alpha(opacity=0)}}@-webkit-keyframes left{from{-webkit-transform:translate(0);transform:translate(0)}to{-webkit-transform:translate(-20px);transform:translate(-20px);opacity:0;filter:alpha(opacity=0)}}@keyframes left{from{-webkit-transform:translate(0);transform:translate(0)}to{-webkit-transform:translate(-20px);transform:translate(-20px);opacity:0;filter:alpha(opacity=0)}}.hide-right{-webkit-animation:right 1s forwards ease-out;animation:right 1s forwards ease-out}.hide-left{-webkit-animation:left 1s forwards ease-out;animation:left 1s forwards ease-out}</style>').appendTo('head');
        }

        this.each(function() {

            var settings, element, fadeRight, fadeLeft, arrow, isIframe, usePostMessage = false,
                tagName, contentWrapper, pluginWrapper, uniqueIdentifier;

            if (isInitialized()) {
                //console.log('updating settings');
                // TODO make update settings function
                contentWrapper = $(this).parent();
                pluginWrapper = $(this).parent().parent();
                fadeLeft = $('.fade-left', pluginWrapper);
                fadeRight = $('.fade-right', pluginWrapper);
                arrow = {
                    left: $('.arrow-left', pluginWrapper),
                    right: $('.arrow-right', pluginWrapper)
                };
                arrow.both = arrow.left.add(arrow.right);

                isIframe = $(this).prop("tagName").toLowerCase() === 'iframe';

                if (isIframe) {
                    usePostMessage = differentDomain($(this));
                }

                settings = contentWrapper.data('settings');
                settings = $.extend({}, settings, options);
                contentWrapper.data('settings', settings);

                uniqueIdentifier = contentWrapper.data('uniqueIdentifier');

                // TODO update with isIE()
                fadeLeft.css('box-shadow', '0 0 ' + settings.fadeWidth + ' ' + settings.fadeWidth + ' ' + settings.color);
                fadeRight.css('box-shadow', '0 0 ' + settings.fadeWidth + ' ' + settings.fadeWidth + ' ' + settings.color);

                if (settings.maxHeight) {
                    contentWrapper.css('max-height', settings.maxHeight);
                }

                if (settings.arrows) {
                    if (arrow.both.length < 2) {
                        addArrows(uniqueIdentifier, pluginWrapper, usePostMessage, isIframe, contentWrapper, $(this));
                    }
                } else {
                    arrow.both.hide();
                }

                scroll(isIframe, contentWrapper, $(this), arrow, fadeLeft, fadeRight, usePostMessage);
                resize(contentWrapper, $(this), fadeLeft, fadeRight, arrow, isIframe, usePostMessage);

                return;
            }

            uniqueIdentifier = 'i_' + getRandomInt(1, 999);

            //console.log(uniqueIdentifier);

            element = $(this);

            element.css('width', '100%');

            // Add a wrapper
            element.wrap(
                '<div class="scroll-indicator plugin-wrapper '+ uniqueIdentifier + '">' +
                '<div class="content-wrapper '+ uniqueIdentifier + '"></div>' +
                '<div class="fade-left '+ uniqueIdentifier + '"></div>' +
                '<div class="fade-right '+ uniqueIdentifier + '"></div>' +
                '</div>'
            );

            pluginWrapper = $('.plugin-wrapper.'+ uniqueIdentifier, element.parent().parent().parent());
            contentWrapper = $('.content-wrapper.'+ uniqueIdentifier, pluginWrapper);
            fadeLeft = $('.fade-left.'+ uniqueIdentifier, pluginWrapper);
            fadeRight = $('.fade-right.'+ uniqueIdentifier, pluginWrapper);

            settings = $.extend({}, $.fn.indicate.defaults, options);
            contentWrapper.data('settings', settings);
            contentWrapper.data('uniqueIdentifier', uniqueIdentifier);

            tagName = element.prop("tagName").toLowerCase();

            isIframe = element.prop("tagName").toLowerCase() === 'iframe';

            if (!settings.color) {
                settings.color = findColorAround(element);
                if (!settings.color) {
                    settings.color = settings.defaultColor;
                }
            }

            //console.log(settings.color);

            if (settings.maxHeight) {
                contentWrapper.css('max-height', settings.maxHeight);
            }

            if (isIframe) {
                usePostMessage = differentDomain(element);
            }

            pluginWrapper.css({
                'position': 'relative',
                'overflow': 'hidden'
            });

            contentWrapper.css({
                'overflow': 'auto',
                '-ms-overflow-style': 'none'
            });

            contentWrapper.css('-webkit-overflow-scrolling', 'touch', 'important');
            contentWrapper.css('overflow', 'auto', 'important');

            var fadeStyles = {
                'z-index': '1',
                'width': '0px',
                'height': '100%',
                'position': 'absolute',
                'top': '0'
                //'transition': 'all 1s linear'
            };

            var boxShadow = {
                'box-shadow': '0 0 ' + settings.fadeWidth + ' calc(' + settings.fadeWidth + ' * 1.2) ' + settings.color
            };

            if (isIE()) {
                boxShadow['box-shadow'] = '0 0 ' + settings.fadeWidth + ' ' + settings.fadeWidth + ' ' + settings.color;
            }

            fadeLeft.css(fadeStyles).css('left', '0').css(boxShadow).css('display', 'none');
            fadeRight.css(fadeStyles).css('right', '0').css(boxShadow);

            if (settings.arrows) {
                arrow = addArrows(uniqueIdentifier, pluginWrapper, usePostMessage, isIframe, contentWrapper, element);
            }

            if (settings.adaptToContentHeight) {
                adaptToContentHeight(element);
            }

            // Prevent accidentially selecting table, when clicking the arrows
            contentWrapper.attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);

            $(window).on('resize', function() {
                //console.log('resize');
                resize(contentWrapper, element, fadeLeft, fadeRight, arrow, isIframe, usePostMessage);
            });

            if (isIframe) {
                //console.log('is iframe');
                if (usePostMessage) {
                    //console.log('postmessage');
                    var src = element.attr('src');

                    window.addEventListener("message", function(event) {
                        var sourceIframe,
                            frames = document.getElementsByTagName('iframe');

                        for (var i = 0; i < frames.length; i++) {
                            if (frames[i].contentWindow === event.source) {
                                sourceIframe = frames[i];
                            }
                        }

                        if (!$(sourceIframe).prop("tagName")) {
                            return;
                        }

                        var parts = getPartsFromIframe($(sourceIframe));

                        var data = JSON.parse(event.data);
                        contentWrapper.data('width', data.width);
                        contentWrapper.data('height', data.height);
                        scroll(parts.isIframe, parts.contentWrapper, parts.element, parts.arrow, parts.fade.left, parts.fade.right, parts.usePostMessage, data.offset, data.width);
                    }, false);

                    contentWrapper.on('scroll', function () {
                        //console.log(iOS() + 'isiOS');
                        if (iOS()) {
                            var width = contentWrapper.data('width');
                            scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage, contentWrapper.scrollLeft(), width - 50);
                        } else {
                            scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
                        }
                    });
                } else {
                    //console.log('no postmessage');
                    element.load(function() {
                        if (iOS()) {
                            contentWrapper.on('scroll', function () {
                                scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
                            });
                        } else {
                            $(element.contents()).scroll(function () {
                                scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
                            });
                        }

                        scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
                        resize(contentWrapper, element, fadeLeft, fadeRight, arrow, isIframe, usePostMessage);
                    });
                }
            } else {
                //console.log('no iframe');
                contentWrapper.on('scroll', function () {
                    //console.log(iOS() + 'isiOS');
                    if (iOS()) {
                        var width = contentWrapper.data('width');
                        scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage, contentWrapper.scrollLeft(), width - 50);
                    } else {
                        scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
                    }
                });
            }

            scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage);
            resize(contentWrapper, element, fadeLeft, fadeRight, arrow, isIframe, usePostMessage);
        });

        return this;

        function scroll(isIframe, contentWrapper, element, arrow, fadeLeft, fadeRight, usePostMessage, offsetPost, widthPost) {
            //console.log('scroll');
            var settings = contentWrapper.data('settings'),
                wrapperWidth = contentWrapper.width(),
                elementWidth = Math.max(element.width(), element.get(0).scrollWidth),
                offset = 0;

            if (isIframe && !usePostMessage)
                offset = $(element.contents()).scrollLeft();
            if (!isIframe)
                offset = contentWrapper.scrollLeft();
            if (isIframe && usePostMessage)
                offset = offsetPost;
            if (isIframe && !usePostMessage && iOS())
                offset = contentWrapper.scrollLeft();

            if (isIframe && !usePostMessage)
                elementWidth = $(element.contents()).width();
            if (isIframe && usePostMessage)
                elementWidth = widthPost;

            if (offset > settings.fadeOffset) {
                fadeLeft.show();
                fadeLeft.removeClass('hide-left');
                if (settings.arrows) {
                    arrow.left.show();
                    arrow.left.removeClass('hide-left');
                }
            } else {
                fadeLeft.addClass('hide-left');
                if (settings.arrows) {
                    arrow.left.addClass('hide-left');
                }
            }

            //console.log(offset + '+' + wrapperWidth + '+' + settings.fadeOffset + '( ' + (offset + wrapperWidth + settings.fadeOffset) + ') <' + elementWidth);

            if (offset + wrapperWidth + settings.fadeOffset < elementWidth) {
                fadeRight.show();
                fadeRight.removeClass('hide-right');
                if (settings.arrows) {
                    arrow.right.show();
                    arrow.right.removeClass('hide-right');
                }
            } else {
                fadeRight.addClass('hide-right');
                if (settings.arrows) {
                    arrow.right.addClass('hide-right');
                }
            }
        }

        function resize(contentWrapper, element, fadeLeft, fadeRight, arrow, isIframe, usePostMessage) {
            //console.log('resize');
            var settings = contentWrapper.data('settings'),
                wrapperWidth = contentWrapper.width(),
                elementWidth = Math.max(element.width(), element.get(0).scrollWidth),
                offset;

            if (usePostMessage && contentWrapper.data('width')) {
                elementWidth = Math.max(contentWrapper.data('width'), elementWidth);
            }

            if (isIframe && !usePostMessage)
                elementWidth = $(element.contents()).width();

            if (iOS() && isIframe) {
                elementWidth = $(element.contents()).get(0).documentElement.width;
            }

            //console.log(elementWidth + '<=' + wrapperWidth);

            if (elementWidth <= wrapperWidth) {
                fadeLeft.hide();
                fadeRight.hide();
                arrow.both.hide();
                // TODO add to settings
                contentWrapper.css('white-space', 'normal');
            } else {
                contentWrapper.css('white-space', 'nowrap');
                if (isIframe && !usePostMessage)
                    offset = $(element.contents()).scrollLeft();
                else
                    offset = contentWrapper.scrollLeft();

                fadeRight.show();
                if (settings.arrows) {
                    arrow.right.show();
                }

                if (offset > settings.fadeOffset) {
                    fadeLeft.show();
                    if (settings.arrows) {
                        arrow.left.show();
                    }
                }
            }
        }

        function scrollTo(dir, isIframe, contentWrapper, element, usePostMessage) {
            var settings = contentWrapper.data('settings'),
                wrapperWidth = contentWrapper.width(),
                offset;

            if (!isIframe || (isIframe && iOS())) {
                offset = contentWrapper.scrollLeft();
            } else {
                if (!usePostMessage) {
                    offset = $(element.contents()).scrollLeft();
                }
            }

            var length = wrapperWidth / settings.scrollDenominator;

            if (!isIframe || (isIframe && iOS())) {
                if (dir === 'left')
                    contentWrapper.animate({scrollLeft: offset - length}, 300);
                else
                    contentWrapper.animate({scrollLeft: offset + length}, 300);
            } else {
                if (!usePostMessage) {
                    if (dir === 'left')
                        element.contents().find('body').animate({scrollLeft: offset - length}, 300);
                    else
                        element.contents().find('body').animate({scrollLeft: offset + length}, 300);
                } else {
                    var data = JSON.stringify({dir: dir, scrollDenominator: settings.scrollDenominator});
                    $('iframe', contentWrapper).get(0).contentWindow.postMessage(data, '*');
                }
            }
        }

        function addArrows(uniqueIdentifier, pluginWrapper, usePostMessage, isIframe, contentWrapper, element) {
            var settings = contentWrapper.data('settings'),
                arrowLeftImage = '<div class="arrow-left ' + uniqueIdentifier + '"><svg width="14" height="24" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="' + settings.arrowColor + '" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="12,22 2,12 12,2 "/></svg></div>',
                arrowRightImage = '<div class="arrow-right ' + uniqueIdentifier + '"><svg width="14" height="24" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="' + settings.arrowColor + '" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="2,22 12,12 2,2 "/></svg></div>',
                arrowLeft,
                arrowRight,
                arrows,
                wrapperStyles,
                svgStyles;

            pluginWrapper.append(arrowLeftImage);
            pluginWrapper.append(arrowRightImage);

            arrowLeft = $('.arrow-left.'+ uniqueIdentifier, pluginWrapper);
            arrowRight = $('.arrow-right.'+ uniqueIdentifier, pluginWrapper);

            arrows = arrowLeft.add(arrowRight);

            wrapperStyles = {
                'width': '50px',
                'height': '100%',
                'position': 'absolute',
                'top': '0',
                'z-index': '2'
            };

            svgStyles = {
                'width': '14px',
                'height': '24px',
                'margin-top': '10px',
                'transition-property': 'all',
                'transition-duration': '200ms',
                'transition-timing-function': 'ease'
            };

            arrowLeft.css(wrapperStyles).css('display', 'none');
            arrowRight.css(wrapperStyles).css('right', '0');

            arrowLeft.find('svg').css(svgStyles).css('margin-left', '2px');
            arrowRight.find('svg').css(svgStyles).css('margin-right', '2px').css({'position': 'absolute', 'right': '0'});

            arrows.hover(function(){
                if ($(this).find('svg').css('margin-right') === '2px')
                    arrowRight.find('svg').css('margin-right', '6px');
                else
                    arrowLeft.find('svg').css('margin-left', '6px');
            }, function(){
                if ($(this).find('svg').css('margin-right') === '6px')
                    arrowRight.find('svg').css('margin-right', '2px');
                else
                    arrowLeft.find('svg').css('margin-left', '2px');
            });

            arrowLeft.click(function() {
                scrollTo('left', isIframe, contentWrapper, element, usePostMessage);
            });

            arrowRight.click(function() {
                scrollTo('right', isIframe, contentWrapper, element, usePostMessage);
            });

            return {
                both: arrows,
                left: arrowLeft,
                right: arrowRight
            };
        }

        function adaptToContentHeight(element) {
            var parts = getPartsFromIframe(element);

            if (parts.settings.adaptToContentHeight) {
                if (parts.isIframe) {
                    if (parts.usePostMessage) {
                        var height = parts.contentWrapper.data('height');
                    } else {
                        element.load(function() {
                            element.animate({height: $(element).contents().height()}, 300);
                        });
                    }
                } else {
                    //console.log('TODO');
                    //console.log(element.height());
                }
            }
        }

        function getPartsFromIframe(element) {
            var wrapper = element.parent().parent();

            return {
                pluginWrapper: wrapper,
                contentWrapper: element.parent(),
                element: element,
                fade: {
                    left: $('.fade-left', wrapper),
                    right: $('.fade-right', wrapper)
                },
                arrow: {
                    left: $('.arrow-left', wrapper),
                    right: $('.arrow-right', wrapper),
                    both: $('.arrow-left', wrapper).add($('.arrow-right', wrapper))
                },
                isIframe: element.prop("tagName").toLowerCase() === 'iframe',
                usePostMessage: element.prop("tagName").toLowerCase() === 'iframe' && differentDomain(element),
                settings: element.parent().data('settings'),
                uniqueIdentifier: element.parent().data('uniqueIdentifier')
            };
        }

        function onLoaded(iframe, callback) {
            //console.log(iframe);
            iframe = iframe.get(0);
            iframe = iframe.contentDocument || iframe.contentWindow.document;

            if (iframe.readyState === 'complete') {
                //console.log('done loading');
                callback();
                return;
            }

            setTimeout(onLoaded(iframe, callback), 100);
        }

        function isInitialized(iframe) {
            if (iframe) {
                return iframe.parent().hasClass('content-wrapper');
            } else {
                return $(this).parent().hasClass('content-wrapper');
            }
        }

        function extractProtocol(domain) {
            return domain.split('/')[0];
        }

        function extractSubdomain(domain) {
            return domain.split('/')[2];
        }

        function subdomainWithProtocol(element) {
            var domain = element.attr('src');
            return domain.split('/')[0] + '//' + domain.split('/')[2];
        }

        function extractDomainName(subdomain) {
            var arr = subdomain.split('.');
            return arr[arr.length - 2] + '.' + arr[arr.length - 1];
        }

        function differentDomain(element) {
            var src = element.attr('src'),
                protocolIframe = extractProtocol(src),
                subdomainIframe = extractSubdomain(src);

            if (location.protocol != protocolIframe || location.host != subdomainIframe) {
                // TODO Check if domain or postmessage added? And warn accordingly.
                // console.warn(location.host + ' != ' + subdomainIframe);
                // console.warn('indicate will only work on iframes with the same origin.');
                var domainName = extractDomainName(subdomainIframe);
                //console.log(element);
                console.log(document.domain + " != " + domainName + (document.domain != domainName));
                if (document.domain != domainName) {
                    return true;
                } else {
                    document.domain = domainName;
                }
            }
            return false;
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function findColorAround(element) {
            // Returns the first parent node with a color that's not transparent
            var parent = element,
                color;

            while(parent = parent.parent()) {
                if (!isTransparent(color = parent.css('background-color'))) {
                    //console.log(color);
                    return color;
                }
            }
        }

        function isTransparent(color) {
            return color === 'rgba(0, 0, 0, 0)' || color === 'transparent';
        }

        function isIE(){
            var n=window.navigator.userAgent,r=n.indexOf("MSIE ");if(r>0)return parseInt(n.substring(r+5,n.indexOf(".",r)),10);var e=n.indexOf("Trident/");if(e>0){var i=n.indexOf("rv:");return parseInt(n.substring(i+3,n.indexOf(".",i)),10);}var t=n.indexOf("Edge/");return t>0?parseInt(n.substring(t+5,n.indexOf(".",t)),10):!1;
        }

        function iOS() {

            var iDevices = [
                'iPad Simulator',
                'iPhone Simulator',
                'iPod Simulator',
                'iPad',
                'iPhone',
                'iPod'
            ];

            if (!!navigator.platform) {
                while (iDevices.length) {
                    if (navigator.platform === iDevices.pop()){ return true; }
                }
            }

            return false;
        }

        function getContents(element) {
            // TODO try { return element.contents() }
        }
    };

    $.fn.indicate.defaults = {
        scrollDenominator: 5, // Clicking on the arrow will scroll (1 / value) of the container width
        defaultColor: '#FFFFFF',
        arrows: true,
        fadeWidth: '20px',
        arrowColor: '#000000',
        fadeOffset: 5,
        adaptToContentHeight: true
    };

})(jQuery);/*
 * jQuery Form Plugin
 * version: 2.36 (07-NOV-2009)
 * @requires jQuery v1.2.6 or later
 *
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
;(function($) {

    /*
     Usage Note:
     -----------
     Do not use both ajaxSubmit and ajaxForm on the same form.  These
     functions are intended to be exclusive.  Use ajaxSubmit if you want
     to bind your own submit handler to the form.  For example,

     $(document).ready(function() {
     $('#myForm').bind('submit', function() {
     $(this).ajaxSubmit({
     target: '#output'
     });
     return false; // <-- important!
     });
     });

     Use ajaxForm when you want the plugin to manage all the event binding
     for you.  For example,

     $(document).ready(function() {
     $('#myForm').ajaxForm({
     target: '#output'
     });
     });

     When using ajaxForm, the ajaxSubmit function will be invoked for you
     at the appropriate time.
     */

    /**
     * ajaxSubmit() provides a mechanism for immediately submitting
     * an HTML form using AJAX.
     */
    $.fn.ajaxSubmit = function(options) {
        // fast fail if nothing selected (http://dev.jquery.com/ticket/2752)
        if (!this.length) {
            log('ajaxSubmit: skipping submit process - no element selected');
            return this;
        }

        if (typeof options == 'function')
            options = { success: options };

        var url = $.trim(this.attr('action'));
        if (url) {
            // clean url (don't include hash vaue)
            url = (url.match(/^([^#]+)/)||[])[1];
        }
        url = url || window.location.href || '';

        options = $.extend({
            url:  url,
            type: this.attr('method') || 'GET',
            iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
        }, options || {});

        // hook for manipulating the form data before it is extracted;
        // convenient for use with rich editors like tinyMCE or FCKEditor
        var veto = {};
        this.trigger('form-pre-serialize', [this, options, veto]);
        if (veto.veto) {
            log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
            return this;
        }

        // provide opportunity to alter form data before it is serialized
        if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
            log('ajaxSubmit: submit aborted via beforeSerialize callback');
            return this;
        }

        var a = this.formToArray(options.semantic);
        if (options.data) {
            options.extraData = options.data;
            for (var n in options.data) {
                if(options.data[n] instanceof Array) {
                    for (var k in options.data[n])
                        a.push( { name: n, value: options.data[n][k] } );
                }
                else
                    a.push( { name: n, value: options.data[n] } );
            }
        }

        // give pre-submit callback an opportunity to abort the submit
        if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
            log('ajaxSubmit: submit aborted via beforeSubmit callback');
            return this;
        }

        // fire vetoable 'validate' event
        this.trigger('form-submit-validate', [a, this, options, veto]);
        if (veto.veto) {
            log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
            return this;
        }

        var q = $.param(a);

        if (options.type.toUpperCase() == 'GET') {
            options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
            options.data = null;  // data is null for 'get'
        }
        else
            options.data = q; // data is the query string for 'post'

        var $form = this, callbacks = [];
        if (options.resetForm) callbacks.push(function() { $form.resetForm(); });
        if (options.clearForm) callbacks.push(function() { $form.clearForm(); });

        // perform a load on the target only if dataType is not provided
        if (!options.dataType && options.target) {
            var oldSuccess = options.success || function(){};
            callbacks.push(function(data) {
                $(options.target).html(data).each(oldSuccess, arguments);
            });
        }
        else if (options.success)
            callbacks.push(options.success);

        options.success = function(data, status) {
            for (var i=0, max=callbacks.length; i < max; i++)
                callbacks[i].apply(options, [data, status, $form]);
        };

        // are there files to upload?
        var files = $('input:file', this).fieldValue();
        var found = false;
        for (var j=0; j < files.length; j++)
            if (files[j])
                found = true;

        var multipart = false;
//	var mp = 'multipart/form-data';
//	multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);

        // options.iframe allows user to force iframe mode
        // 06-NOV-09: now defaulting to iframe mode if file input is detected
        if ((files.length && options.iframe !== false) || options.iframe || found || multipart) {
            // hack to fix Safari hang (thanks to Tim Molendijk for this)
            // see:  http://groups.google.com/group/jquery-dev/browse_thread/thread/36395b7ab510dd5d
            if (options.closeKeepAlive)
                $.get(options.closeKeepAlive, fileUpload);
            else
                fileUpload();
        }
        else
            $.ajax(options);

        // fire 'notify' event
        this.trigger('form-submit-notify', [this, options]);
        return this;


        // private function for handling file uploads (hat tip to YAHOO!)
        function fileUpload() {
            var form = $form[0];

            if ($(':input[name=submit]', form).length) {
                alert('Error: Form elements must not be named "submit".');
                return;
            }

            var opts = $.extend({}, $.ajaxSettings, options);
            var s = $.extend(true, {}, $.extend(true, {}, $.ajaxSettings), opts);

            var id = 'jqFormIO' + (new Date().getTime());
            var $io = $('<iframe id="' + id + '" name="' + id + '" src="'+ opts.iframeSrc +'" />');
            var io = $io[0];

            $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });

            var xhr = { // mock object
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: 'n/a',
                getAllResponseHeaders: function() {},
                getResponseHeader: function() {},
                setRequestHeader: function() {},
                abort: function() {
                    this.aborted = 1;
                    $io.attr('src', opts.iframeSrc); // abort op in progress
                }
            };

            var g = opts.global;
            // trigger ajax global events so that activity/block indicators work like normal
            if (g && ! $.active++) $.event.trigger("ajaxStart");
            if (g) $.event.trigger("ajaxSend", [xhr, opts]);

            if (s.beforeSend && s.beforeSend(xhr, s) === false) {
                s.global && $.active--;
                return;
            }
            if (xhr.aborted)
                return;

            var cbInvoked = 0;
            var timedOut = 0;

            // add submitting element to data if we know it
            var sub = form.clk;
            if (sub) {
                var n = sub.name;
                if (n && !sub.disabled) {
                    options.extraData = options.extraData || {};
                    options.extraData[n] = sub.value;
                    if (sub.type == "image") {
                        options.extraData[name+'.x'] = form.clk_x;
                        options.extraData[name+'.y'] = form.clk_y;
                    }
                }
            }

            // take a breath so that pending repaints get some cpu time before the upload starts
            setTimeout(function() {
                // make sure form attrs are set
                var t = $form.attr('target'), a = $form.attr('action');

                // update form attrs in IE friendly way
                form.setAttribute('target',id);
                if (form.getAttribute('method') != 'POST')
                    form.setAttribute('method', 'POST');
                if (form.getAttribute('action') != opts.url)
                    form.setAttribute('action', opts.url);

                // ie borks in some cases when setting encoding
                if (! options.skipEncodingOverride) {
                    $form.attr({
                        encoding: 'multipart/form-data',
                        enctype:  'multipart/form-data'
                    });
                }

                // support timout
                if (opts.timeout)
                    setTimeout(function() { timedOut = true; cb(); }, opts.timeout);

                // add "extra" data to form if provided in options
                var extraInputs = [];
                try {
                    if (options.extraData)
                        for (var n in options.extraData)
                            extraInputs.push(
                                $('<input type="hidden" name="'+n+'" value="'+options.extraData[n]+'" />')
                                    .appendTo(form)[0]);

                    // add iframe to doc and submit the form
                    $io.appendTo('body');
                    io.attachEvent ? io.attachEvent('onload', cb) : io.addEventListener('load', cb, false);
                    form.submit();
                }
                finally {
                    // reset attrs and remove "extra" input elements
                    form.setAttribute('action',a);
                    t ? form.setAttribute('target', t) : $form.removeAttr('target');
                    $(extraInputs).remove();
                }
            }, 10);

            var domCheckCount = 50;

            function cb() {
                if (cbInvoked++) return;

                io.detachEvent ? io.detachEvent('onload', cb) : io.removeEventListener('load', cb, false);

                var ok = true;
                try {
                    if (timedOut) throw 'timeout';
                    // extract the server response from the iframe
                    var data, doc;

                    doc = io.contentWindow ? io.contentWindow.document : io.contentDocument ? io.contentDocument : io.document;

                    var isXml = opts.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                    log('isXml='+isXml);
                    if (!isXml && (doc.body == null || doc.body.innerHTML == '')) {
                        if (--domCheckCount) {
                            // in some browsers (Opera) the iframe DOM is not always traversable when
                            // the onload callback fires, so we loop a bit to accommodate
                            cbInvoked = 0;
                            setTimeout(cb, 100);
                            return;
                        }
                        log('Could not access iframe DOM after 50 tries.');
                        return;
                    }

                    xhr.responseText = doc.body ? doc.body.innerHTML : null;
                    xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                    xhr.getResponseHeader = function(header){
                        var headers = {'content-type': opts.dataType};
                        return headers[header];
                    };

                    if (opts.dataType == 'json' || opts.dataType == 'script') {
                        // see if user embedded response in textarea
                        var ta = doc.getElementsByTagName('textarea')[0];
                        if (ta)
                            xhr.responseText = ta.value;
                        else {
                            // account for browsers injecting pre around json response
                            var pre = doc.getElementsByTagName('pre')[0];
                            if (pre)
                                xhr.responseText = pre.innerHTML;
                        }
                    }
                    else if (opts.dataType == 'xml' && !xhr.responseXML && xhr.responseText != null) {
                        xhr.responseXML = toXml(xhr.responseText);
                    }
                    data = $.httpData(xhr, opts.dataType);
                }
                catch(e){
                    ok = false;
                    $.handleError(opts, xhr, 'error', e);
                }

                // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
                if (ok) {
                    opts.success(data, 'success');
                    if (g) $.event.trigger("ajaxSuccess", [xhr, opts]);
                }
                if (g) $.event.trigger("ajaxComplete", [xhr, opts]);
                if (g && ! --$.active) $.event.trigger("ajaxStop");
                if (opts.complete) opts.complete(xhr, ok ? 'success' : 'error');

                // clean up
                setTimeout(function() {
                    $io.remove();
                    xhr.responseXML = null;
                }, 100);
            };

            function toXml(s, doc) {
                if (window.ActiveXObject) {
                    doc = new ActiveXObject('Microsoft.XMLDOM');
                    doc.async = 'false';
                    doc.loadXML(s);
                }
                else
                    doc = (new DOMParser()).parseFromString(s, 'text/xml');
                return (doc && doc.documentElement && doc.documentElement.tagName != 'parsererror') ? doc : null;
            };
        };
    };

    /**
     * ajaxForm() provides a mechanism for fully automating form submission.
     *
     * The advantages of using this method instead of ajaxSubmit() are:
     *
     * 1: This method will include coordinates for <input type="image" /> elements (if the element
     *	is used to submit the form).
     * 2. This method will include the submit element's name/value data (for the element that was
     *	used to submit the form).
     * 3. This method binds the submit() method to the form for you.
     *
     * The options argument for ajaxForm works exactly as it does for ajaxSubmit.  ajaxForm merely
     * passes the options argument along after properly binding events for submit elements and
     * the form itself.
     */
    $.fn.ajaxForm = function(options) {
        return this.ajaxFormUnbind().bind('submit.form-plugin', function() {
            $(this).ajaxSubmit(options);
            return false;
        }).bind('click.form-plugin', function(e) {
            var target = e.target;
            var $el = $(target);
            if (!($el.is(":submit,input:image"))) {
                // is this a child element of the submit el?  (ex: a span within a button)
                var t = $el.closest(':submit');
                if (t.length == 0)
                    return;
                target = t[0];
            }
            var form = this;
            form.clk = target;
            if (target.type == 'image') {
                if (e.offsetX != undefined) {
                    form.clk_x = e.offsetX;
                    form.clk_y = e.offsetY;
                } else if (typeof $.fn.offset == 'function') { // try to use dimensions plugin
                    var offset = $el.offset();
                    form.clk_x = e.pageX - offset.left;
                    form.clk_y = e.pageY - offset.top;
                } else {
                    form.clk_x = e.pageX - target.offsetLeft;
                    form.clk_y = e.pageY - target.offsetTop;
                }
            }
            // clear form vars
            setTimeout(function() { form.clk = form.clk_x = form.clk_y = null; }, 100);
        });
    };

// ajaxFormUnbind unbinds the event handlers that were bound by ajaxForm
    $.fn.ajaxFormUnbind = function() {
        return this.unbind('submit.form-plugin click.form-plugin');
    };

    /**
     * formToArray() gathers form element data into an array of objects that can
     * be passed to any of the following ajax functions: $.get, $.post, or load.
     * Each object in the array has both a 'name' and 'value' property.  An example of
     * an array for a simple login form might be:
     *
     * [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
     *
     * It is this array that is passed to pre-submit callback functions provided to the
     * ajaxSubmit() and ajaxForm() methods.
     */
    $.fn.formToArray = function(semantic) {
        var a = [];
        if (this.length == 0) return a;

        var form = this[0];
        var els = semantic ? form.getElementsByTagName('*') : form.elements;
        if (!els) return a;
        for(var i=0, max=els.length; i < max; i++) {
            var el = els[i];
            var n = el.name;
            if (!n) continue;

            if (semantic && form.clk && el.type == "image") {
                // handle image inputs on the fly when semantic == true
                if(!el.disabled && form.clk == el) {
                    a.push({name: n, value: $(el).val()});
                    a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
                }
                continue;
            }

            var v = $.fieldValue(el, true);
            if (v && v.constructor == Array) {
                for(var j=0, jmax=v.length; j < jmax; j++)
                    a.push({name: n, value: v[j]});
            }
            else if (v !== null && typeof v != 'undefined')
                a.push({name: n, value: v});
        }

        if (!semantic && form.clk) {
            // input type=='image' are not found in elements array! handle it here
            var $input = $(form.clk), input = $input[0], n = input.name;
            if (n && !input.disabled && input.type == 'image') {
                a.push({name: n, value: $input.val()});
                a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
            }
        }
        return a;
    };

    /**
     * Serializes form data into a 'submittable' string. This method will return a string
     * in the format: name1=value1&amp;name2=value2
     */
    $.fn.formSerialize = function(semantic) {
        //hand off to jQuery.param for proper encoding
        return $.param(this.formToArray(semantic));
    };

    /**
     * Serializes all field elements in the jQuery object into a query string.
     * This method will return a string in the format: name1=value1&amp;name2=value2
     */
    $.fn.fieldSerialize = function(successful) {
        var a = [];
        this.each(function() {
            var n = this.name;
            if (!n) return;
            var v = $.fieldValue(this, successful);
            if (v && v.constructor == Array) {
                for (var i=0,max=v.length; i < max; i++)
                    a.push({name: n, value: v[i]});
            }
            else if (v !== null && typeof v != 'undefined')
                a.push({name: this.name, value: v});
        });
        //hand off to jQuery.param for proper encoding
        return $.param(a);
    };

    /**
     * Returns the value(s) of the element in the matched set.  For example, consider the following form:
     *
     *  <form><fieldset>
     *	  <input name="A" type="text" />
     *	  <input name="A" type="text" />
     *	  <input name="B" type="checkbox" value="B1" />
     *	  <input name="B" type="checkbox" value="B2"/>
     *	  <input name="C" type="radio" value="C1" />
     *	  <input name="C" type="radio" value="C2" />
     *  </fieldset></form>
     *
     *  var v = $(':text').fieldValue();
     *  // if no values are entered into the text inputs
     *  v == ['','']
     *  // if values entered into the text inputs are 'foo' and 'bar'
     *  v == ['foo','bar']
     *
     *  var v = $(':checkbox').fieldValue();
     *  // if neither checkbox is checked
     *  v === undefined
     *  // if both checkboxes are checked
     *  v == ['B1', 'B2']
     *
     *  var v = $(':radio').fieldValue();
     *  // if neither radio is checked
     *  v === undefined
     *  // if first radio is checked
     *  v == ['C1']
     *
     * The successful argument controls whether or not the field element must be 'successful'
     * (per http://www.w3.org/TR/html4/interact/forms.html#successful-controls).
     * The default value of the successful argument is true.  If this value is false the value(s)
     * for each element is returned.
     *
     * Note: This method *always* returns an array.  If no valid value can be determined the
     *	   array will be empty, otherwise it will contain one or more values.
     */
    $.fn.fieldValue = function(successful) {
        for (var val=[], i=0, max=this.length; i < max; i++) {
            var el = this[i];
            var v = $.fieldValue(el, successful);
            if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length))
                continue;
            v.constructor == Array ? $.merge(val, v) : val.push(v);
        }
        return val;
    };

    /**
     * Returns the value of the field element.
     */
    $.fieldValue = function(el, successful) {
        var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
        if (typeof successful == 'undefined') successful = true;

        if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
            (t == 'checkbox' || t == 'radio') && !el.checked ||
            (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
            tag == 'select' && el.selectedIndex == -1))
            return null;

        if (tag == 'select') {
            var index = el.selectedIndex;
            if (index < 0) return null;
            var a = [], ops = el.options;
            var one = (t == 'select-one');
            var max = (one ? index+1 : ops.length);
            for(var i=(one ? index : 0); i < max; i++) {
                var op = ops[i];
                if (op.selected) {
                    var v = op.value;
                    if (!v) // extra pain for IE...
                        v = (op.attributes && op.attributes['value'] && !(op.attributes['value'].specified)) ? op.text : op.value;
                    if (one) return v;
                    a.push(v);
                }
            }
            return a;
        }
        return el.value;
    };

    /**
     * Clears the form data.  Takes the following actions on the form's input fields:
     *  - input text fields will have their 'value' property set to the empty string
     *  - select elements will have their 'selectedIndex' property set to -1
     *  - checkbox and radio inputs will have their 'checked' property set to false
     *  - inputs of type submit, button, reset, and hidden will *not* be effected
     *  - button elements will *not* be effected
     */
    $.fn.clearForm = function() {
        return this.each(function() {
            $('input,select,textarea', this).clearFields();
        });
    };

    /**
     * Clears the selected form elements.
     */
    $.fn.clearFields = $.fn.clearInputs = function() {
        return this.each(function() {
            var t = this.type, tag = this.tagName.toLowerCase();
            if (t == 'text' || t == 'password' || tag == 'textarea')
                this.value = '';
            else if (t == 'checkbox' || t == 'radio')
                this.checked = false;
            else if (tag == 'select')
                this.selectedIndex = -1;
        });
    };

    /**
     * Resets the form data.  Causes all form elements to be reset to their original value.
     */
    $.fn.resetForm = function() {
        return this.each(function() {
            // guard against an input with the name of 'reset'
            // note that IE reports the reset function as an 'object'
            if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType))
                this.reset();
        });
    };

    /**
     * Enables or disables any matching elements.
     */
    $.fn.enable = function(b) {
        if (b == undefined) b = true;
        return this.each(function() {
            this.disabled = !b;
        });
    };

    /**
     * Checks/unchecks any matching checkboxes or radio buttons and
     * selects/deselects and matching option elements.
     */
    $.fn.selected = function(select) {
        if (select == undefined) select = true;
        return this.each(function() {
            var t = this.type;
            if (t == 'checkbox' || t == 'radio')
                this.checked = select;
            else if (this.tagName.toLowerCase() == 'option') {
                var $sel = $(this).parent('select');
                if (select && $sel[0] && $sel[0].type == 'select-one') {
                    // deselect all other options
                    $sel.find('option').selected(false);
                }
                this.selected = select;
            }
        });
    };

// helper fn for console logging
// set $.fn.ajaxSubmit.debug to true to enable debug logging
    function log() {
        if ($.fn.ajaxSubmit.debug && window.console && window.console.log)
            window.console.log('[jquery.form] ' + Array.prototype.join.call(arguments,''));
    };

})(jQuery);
/**
 * Polyfill for the vw, vh, vm units
 * Requires StyleFix from -prefix-free http://leaverou.github.com/prefixfree/
 * @author Lea Verou
 */

(function() {

    if(!window.StyleFix) {
        return;
    }

// Feature test
    var dummy = document.createElement('_').style,
        units = ['vw', 'vh', 'vmin', 'vmax'].filter(function(unit) {
            dummy.width = '';
            dummy.width = '10' + unit;
            return !dummy.width;
        });

    if(!units.length) {
        return;
    }

    StyleFix.register(function(css) {
        var w = innerWidth, h = innerHeight;
        // For help with this monster regex: see debuggex.com: https://www.debuggex.com/r/cpzpAHiWPagP3Zru
        return css.replace(RegExp('(-?[a-z]+(?:-[a-z]+)*\\s*:\\s*)\\b([0-9]*\\.?[0-9]+)(' + units.join('|') + ')\\b(?:\\s*;\\s*\\1\\b(?:[0-9]*\\.?[0-9]+)(?:px)\\b)?;?', 'gi'), function($0, property, num, unit) {
            if (!unit) return $0;
            var value;
            switch (unit) {
                case 'vw':
                    value = num * w / 100 + 'px';
                    break;
                case 'vh':
                    value = num * h / 100 + 'px';
                    break;
                case 'vmin':
                    value = num * Math.min(w,h) / 100 + 'px';
                    break;
                case 'vmax':
                    value = num * Math.max(w,h) / 100 + 'px';
                    break;
            }
            return property + num + unit + ';' + property + value + ';';
        });
    });

    var styleFixResizeTimer;

    var resizeListener = function () {
        // 100ms interruptable delay because the computation is expensive
        if (typeof styleFixResizeTimer !== 'undefined') clearTimeout(styleFixResizeTimer);
        styleFixResizeTimer = setTimeout(function () {
            StyleFix.process();
        }, 100);
    };

    window.addEventListener('resize', resizeListener, false);
    window.addEventListener('orientationchange', resizeListener, false);

})();/*
 * jQuery Highlight plugin
 *
 * Based on highlight v3 by Johann Burkard
 * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
 *
 * Code a little bit refactored and cleaned (in my humble opinion).
 * Most important changes:
 *  - has an option to highlight only entire words (wordsOnly - false by default),
 *  - has an option to be case sensitive (caseSensitive - false by default)
 *  - highlight element tag and class names can be specified in options
 *
 * Usage:
 *   // wrap every occurrance of text 'lorem' in content
 *   // with <span class='highlight'> (default options)
 *   $('#content').highlight('lorem');
 *
 *   // search for and highlight more terms at once
 *   // so you can save some time on traversing DOM
 *   $('#content').highlight(['lorem', 'ipsum']);
 *   $('#content').highlight('lorem ipsum');
 *
 *   // search only for entire word 'lorem'
 *   $('#content').highlight('lorem', { wordsOnly: true });
 *
 *   // don't ignore case during search of term 'lorem'
 *   $('#content').highlight('lorem', { caseSensitive: true });
 *
 *   // wrap every occurrance of term 'ipsum' in content
 *   // with <em class='important'>
 *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
 *
 *   // remove default highlight
 *   $('#content').unhighlight();
 *
 *   // remove custom highlight
 *   $('#content').unhighlight({ element: 'em', className: 'important' });
 *
 *
 * Copyright (c) 2009 Bartek Szopka
 *
 * Licensed under MIT license.
 *
 */

jQuery.extend({
    highlight: function (node, re, nodeName, className) {
        if (node.nodeType === 3) {
            var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
            !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
            !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++) {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options) {
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function (words, options) {
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
    jQuery.extend(settings, options);

    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i){
        return word != '';
    });
    if (words.length == 0) { return this; };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function () {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};

(function(a){if(typeof define==="function"&&define.amd&&define.amd.jQuery){define(["jquery"],a)}else{if(typeof module!=="undefined"&&module.exports){a(require("jquery"))}else{a(jQuery)}}}(function(f){var y="1.6.15",p="left",o="right",e="up",x="down",c="in",A="out",m="none",s="auto",l="swipe",t="pinch",B="tap",j="doubletap",b="longtap",z="hold",E="horizontal",u="vertical",i="all",r=10,g="start",k="move",h="end",q="cancel",a="ontouchstart" in window,v=window.navigator.msPointerEnabled&&!window.navigator.pointerEnabled&&!a,d=(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&!a,C="TouchSwipe";var n={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,hold:null,triggerOnTouchEnd:true,triggerOnTouchLeave:false,allowPageScroll:"auto",fallbackToMouseEvents:true,excludedElements:"label, button, input, select, textarea, a, .noSwipe",preventDefaultEvents:true};f.fn.swipe=function(H){var G=f(this),F=G.data(C);if(F&&typeof H==="string"){if(F[H]){return F[H].apply(this,Array.prototype.slice.call(arguments,1))}else{f.error("Method "+H+" does not exist on jQuery.swipe")}}else{if(F&&typeof H==="object"){F.option.apply(this,arguments)}else{if(!F&&(typeof H==="object"||!H)){return w.apply(this,arguments)}}}return G};f.fn.swipe.version=y;f.fn.swipe.defaults=n;f.fn.swipe.phases={PHASE_START:g,PHASE_MOVE:k,PHASE_END:h,PHASE_CANCEL:q};f.fn.swipe.directions={LEFT:p,RIGHT:o,UP:e,DOWN:x,IN:c,OUT:A};f.fn.swipe.pageScroll={NONE:m,HORIZONTAL:E,VERTICAL:u,AUTO:s};f.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,FOUR:4,FIVE:5,ALL:i};function w(F){if(F&&(F.allowPageScroll===undefined&&(F.swipe!==undefined||F.swipeStatus!==undefined))){F.allowPageScroll=m}if(F.click!==undefined&&F.tap===undefined){F.tap=F.click}if(!F){F={}}F=f.extend({},f.fn.swipe.defaults,F);return this.each(function(){var H=f(this);var G=H.data(C);if(!G){G=new D(this,F);H.data(C,G)}})}function D(a5,au){var au=f.extend({},au);var az=(a||d||!au.fallbackToMouseEvents),K=az?(d?(v?"MSPointerDown":"pointerdown"):"touchstart"):"mousedown",ax=az?(d?(v?"MSPointerMove":"pointermove"):"touchmove"):"mousemove",V=az?(d?(v?"MSPointerUp":"pointerup"):"touchend"):"mouseup",T=az?(d?"mouseleave":null):"mouseleave",aD=(d?(v?"MSPointerCancel":"pointercancel"):"touchcancel");var ag=0,aP=null,a2=null,ac=0,a1=0,aZ=0,H=1,ap=0,aJ=0,N=null;var aR=f(a5);var aa="start";var X=0;var aQ={};var U=0,a3=0,a6=0,ay=0,O=0;var aW=null,af=null;try{aR.bind(K,aN);aR.bind(aD,ba)}catch(aj){f.error("events not supported "+K+","+aD+" on jQuery.swipe")}this.enable=function(){aR.bind(K,aN);aR.bind(aD,ba);return aR};this.disable=function(){aK();return aR};this.destroy=function(){aK();aR.data(C,null);aR=null};this.option=function(bd,bc){if(typeof bd==="object"){au=f.extend(au,bd)}else{if(au[bd]!==undefined){if(bc===undefined){return au[bd]}else{au[bd]=bc}}else{if(!bd){return au}else{f.error("Option "+bd+" does not exist on jQuery.swipe.options")}}}return null};function aN(be){if(aB()){return}if(f(be.target).closest(au.excludedElements,aR).length>0){return}var bf=be.originalEvent?be.originalEvent:be;var bd,bg=bf.touches,bc=bg?bg[0]:bf;aa=g;if(bg){X=bg.length}else{if(au.preventDefaultEvents!==false){be.preventDefault()}}ag=0;aP=null;a2=null;aJ=null;ac=0;a1=0;aZ=0;H=1;ap=0;N=ab();S();ai(0,bc);if(!bg||(X===au.fingers||au.fingers===i)||aX()){U=ar();if(X==2){ai(1,bg[1]);a1=aZ=at(aQ[0].start,aQ[1].start)}if(au.swipeStatus||au.pinchStatus){bd=P(bf,aa)}}else{bd=false}if(bd===false){aa=q;P(bf,aa);return bd}else{if(au.hold){af=setTimeout(f.proxy(function(){aR.trigger("hold",[bf.target]);if(au.hold){bd=au.hold.call(aR,bf,bf.target)}},this),au.longTapThreshold)}an(true)}return null}function a4(bf){var bi=bf.originalEvent?bf.originalEvent:bf;if(aa===h||aa===q||al()){return}var be,bj=bi.touches,bd=bj?bj[0]:bi;var bg=aH(bd);a3=ar();if(bj){X=bj.length}if(au.hold){clearTimeout(af)}aa=k;if(X==2){if(a1==0){ai(1,bj[1]);a1=aZ=at(aQ[0].start,aQ[1].start)}else{aH(bj[1]);aZ=at(aQ[0].end,aQ[1].end);aJ=aq(aQ[0].end,aQ[1].end)}H=a8(a1,aZ);ap=Math.abs(a1-aZ)}if((X===au.fingers||au.fingers===i)||!bj||aX()){aP=aL(bg.start,bg.end);a2=aL(bg.last,bg.end);ak(bf,a2);ag=aS(bg.start,bg.end);ac=aM();aI(aP,ag);be=P(bi,aa);if(!au.triggerOnTouchEnd||au.triggerOnTouchLeave){var bc=true;if(au.triggerOnTouchLeave){var bh=aY(this);bc=F(bg.end,bh)}if(!au.triggerOnTouchEnd&&bc){aa=aC(k)}else{if(au.triggerOnTouchLeave&&!bc){aa=aC(h)}}if(aa==q||aa==h){P(bi,aa)}}}else{aa=q;P(bi,aa)}if(be===false){aa=q;P(bi,aa)}}function M(bc){var bd=bc.originalEvent?bc.originalEvent:bc,be=bd.touches;if(be){if(be.length&&!al()){G(bd);return true}else{if(be.length&&al()){return true}}}if(al()){X=ay}a3=ar();ac=aM();if(bb()||!am()){aa=q;P(bd,aa)}else{if(au.triggerOnTouchEnd||(au.triggerOnTouchEnd==false&&aa===k)){if(au.preventDefaultEvents!==false){bc.preventDefault()}aa=h;P(bd,aa)}else{if(!au.triggerOnTouchEnd&&a7()){aa=h;aF(bd,aa,B)}else{if(aa===k){aa=q;P(bd,aa)}}}}an(false);return null}function ba(){X=0;a3=0;U=0;a1=0;aZ=0;H=1;S();an(false)}function L(bc){var bd=bc.originalEvent?bc.originalEvent:bc;if(au.triggerOnTouchLeave){aa=aC(h);P(bd,aa)}}function aK(){aR.unbind(K,aN);aR.unbind(aD,ba);aR.unbind(ax,a4);aR.unbind(V,M);if(T){aR.unbind(T,L)}an(false)}function aC(bg){var bf=bg;var be=aA();var bd=am();var bc=bb();if(!be||bc){bf=q}else{if(bd&&bg==k&&(!au.triggerOnTouchEnd||au.triggerOnTouchLeave)){bf=h}else{if(!bd&&bg==h&&au.triggerOnTouchLeave){bf=q}}}return bf}function P(be,bc){var bd,bf=be.touches;if(J()||W()){bd=aF(be,bc,l)}if((Q()||aX())&&bd!==false){bd=aF(be,bc,t)}if(aG()&&bd!==false){bd=aF(be,bc,j)}else{if(ao()&&bd!==false){bd=aF(be,bc,b)}else{if(ah()&&bd!==false){bd=aF(be,bc,B)}}}if(bc===q){if(W()){bd=aF(be,bc,l)}if(aX()){bd=aF(be,bc,t)}ba(be)}if(bc===h){if(bf){if(!bf.length){ba(be)}}else{ba(be)}}return bd}function aF(bf,bc,be){var bd;if(be==l){aR.trigger("swipeStatus",[bc,aP||null,ag||0,ac||0,X,aQ,a2]);if(au.swipeStatus){bd=au.swipeStatus.call(aR,bf,bc,aP||null,ag||0,ac||0,X,aQ,a2);if(bd===false){return false}}if(bc==h&&aV()){clearTimeout(aW);clearTimeout(af);aR.trigger("swipe",[aP,ag,ac,X,aQ,a2]);if(au.swipe){bd=au.swipe.call(aR,bf,aP,ag,ac,X,aQ,a2);if(bd===false){return false}}switch(aP){case p:aR.trigger("swipeLeft",[aP,ag,ac,X,aQ,a2]);if(au.swipeLeft){bd=au.swipeLeft.call(aR,bf,aP,ag,ac,X,aQ,a2)}break;case o:aR.trigger("swipeRight",[aP,ag,ac,X,aQ,a2]);if(au.swipeRight){bd=au.swipeRight.call(aR,bf,aP,ag,ac,X,aQ,a2)}break;case e:aR.trigger("swipeUp",[aP,ag,ac,X,aQ,a2]);if(au.swipeUp){bd=au.swipeUp.call(aR,bf,aP,ag,ac,X,aQ,a2)}break;case x:aR.trigger("swipeDown",[aP,ag,ac,X,aQ,a2]);if(au.swipeDown){bd=au.swipeDown.call(aR,bf,aP,ag,ac,X,aQ,a2)}break}}}if(be==t){aR.trigger("pinchStatus",[bc,aJ||null,ap||0,ac||0,X,H,aQ]);if(au.pinchStatus){bd=au.pinchStatus.call(aR,bf,bc,aJ||null,ap||0,ac||0,X,H,aQ);if(bd===false){return false}}if(bc==h&&a9()){switch(aJ){case c:aR.trigger("pinchIn",[aJ||null,ap||0,ac||0,X,H,aQ]);if(au.pinchIn){bd=au.pinchIn.call(aR,bf,aJ||null,ap||0,ac||0,X,H,aQ)}break;case A:aR.trigger("pinchOut",[aJ||null,ap||0,ac||0,X,H,aQ]);if(au.pinchOut){bd=au.pinchOut.call(aR,bf,aJ||null,ap||0,ac||0,X,H,aQ)}break}}}if(be==B){if(bc===q||bc===h){clearTimeout(aW);clearTimeout(af);if(Z()&&!I()){O=ar();aW=setTimeout(f.proxy(function(){O=null;aR.trigger("tap",[bf.target]);if(au.tap){bd=au.tap.call(aR,bf,bf.target)}},this),au.doubleTapThreshold)}else{O=null;aR.trigger("tap",[bf.target]);if(au.tap){bd=au.tap.call(aR,bf,bf.target)}}}}else{if(be==j){if(bc===q||bc===h){clearTimeout(aW);clearTimeout(af);O=null;aR.trigger("doubletap",[bf.target]);if(au.doubleTap){bd=au.doubleTap.call(aR,bf,bf.target)}}}else{if(be==b){if(bc===q||bc===h){clearTimeout(aW);O=null;aR.trigger("longtap",[bf.target]);if(au.longTap){bd=au.longTap.call(aR,bf,bf.target)}}}}}return bd}function am(){var bc=true;if(au.threshold!==null){bc=ag>=au.threshold}return bc}function bb(){var bc=false;if(au.cancelThreshold!==null&&aP!==null){bc=(aT(aP)-ag)>=au.cancelThreshold}return bc}function ae(){if(au.pinchThreshold!==null){return ap>=au.pinchThreshold}return true}function aA(){var bc;if(au.maxTimeThreshold){if(ac>=au.maxTimeThreshold){bc=false}else{bc=true}}else{bc=true}return bc}function ak(bc,bd){if(au.preventDefaultEvents===false){return}if(au.allowPageScroll===m){bc.preventDefault()}else{var be=au.allowPageScroll===s;switch(bd){case p:if((au.swipeLeft&&be)||(!be&&au.allowPageScroll!=E)){bc.preventDefault()}break;case o:if((au.swipeRight&&be)||(!be&&au.allowPageScroll!=E)){bc.preventDefault()}break;case e:if((au.swipeUp&&be)||(!be&&au.allowPageScroll!=u)){bc.preventDefault()}break;case x:if((au.swipeDown&&be)||(!be&&au.allowPageScroll!=u)){bc.preventDefault()}break}}}function a9(){var bd=aO();var bc=Y();var be=ae();return bd&&bc&&be}function aX(){return !!(au.pinchStatus||au.pinchIn||au.pinchOut)}function Q(){return !!(a9()&&aX())}function aV(){var bf=aA();var bh=am();var be=aO();var bc=Y();var bd=bb();var bg=!bd&&bc&&be&&bh&&bf;return bg}function W(){return !!(au.swipe||au.swipeStatus||au.swipeLeft||au.swipeRight||au.swipeUp||au.swipeDown)}function J(){return !!(aV()&&W())}function aO(){return((X===au.fingers||au.fingers===i)||!a)}function Y(){return aQ[0].end.x!==0}function a7(){return !!(au.tap)}function Z(){return !!(au.doubleTap)}function aU(){return !!(au.longTap)}function R(){if(O==null){return false}var bc=ar();return(Z()&&((bc-O)<=au.doubleTapThreshold))}function I(){return R()}function aw(){return((X===1||!a)&&(isNaN(ag)||ag<au.threshold))}function a0(){return((ac>au.longTapThreshold)&&(ag<r))}function ah(){return !!(aw()&&a7())}function aG(){return !!(R()&&Z())}function ao(){return !!(a0()&&aU())}function G(bc){a6=ar();ay=bc.touches.length+1}function S(){a6=0;ay=0}function al(){var bc=false;if(a6){var bd=ar()-a6;if(bd<=au.fingerReleaseThreshold){bc=true}}return bc}function aB(){return !!(aR.data(C+"_intouch")===true)}function an(bc){if(!aR){return}if(bc===true){aR.bind(ax,a4);aR.bind(V,M);if(T){aR.bind(T,L)}}else{aR.unbind(ax,a4,false);aR.unbind(V,M,false);if(T){aR.unbind(T,L,false)}}aR.data(C+"_intouch",bc===true)}function ai(be,bc){var bd={start:{x:0,y:0},last:{x:0,y:0},end:{x:0,y:0}};bd.start.x=bd.last.x=bd.end.x=bc.pageX||bc.clientX;bd.start.y=bd.last.y=bd.end.y=bc.pageY||bc.clientY;aQ[be]=bd;return bd}function aH(bc){var be=bc.identifier!==undefined?bc.identifier:0;var bd=ad(be);if(bd===null){bd=ai(be,bc)}bd.last.x=bd.end.x;bd.last.y=bd.end.y;bd.end.x=bc.pageX||bc.clientX;bd.end.y=bc.pageY||bc.clientY;return bd}function ad(bc){return aQ[bc]||null}function aI(bc,bd){bd=Math.max(bd,aT(bc));N[bc].distance=bd}function aT(bc){if(N[bc]){return N[bc].distance}return undefined}function ab(){var bc={};bc[p]=av(p);bc[o]=av(o);bc[e]=av(e);bc[x]=av(x);return bc}function av(bc){return{direction:bc,distance:0}}function aM(){return a3-U}function at(bf,be){var bd=Math.abs(bf.x-be.x);var bc=Math.abs(bf.y-be.y);return Math.round(Math.sqrt(bd*bd+bc*bc))}function a8(bc,bd){var be=(bd/bc)*1;return be.toFixed(2)}function aq(){if(H<1){return A}else{return c}}function aS(bd,bc){return Math.round(Math.sqrt(Math.pow(bc.x-bd.x,2)+Math.pow(bc.y-bd.y,2)))}function aE(bf,bd){var bc=bf.x-bd.x;var bh=bd.y-bf.y;var be=Math.atan2(bh,bc);var bg=Math.round(be*180/Math.PI);if(bg<0){bg=360-Math.abs(bg)}return bg}function aL(bd,bc){var be=aE(bd,bc);if((be<=45)&&(be>=0)){return p}else{if((be<=360)&&(be>=315)){return p}else{if((be>=135)&&(be<=225)){return o}else{if((be>45)&&(be<135)){return x}else{return e}}}}}function ar(){var bc=new Date();return bc.getTime()}function aY(bc){bc=f(bc);var be=bc.offset();var bd={left:be.left,right:be.left+bc.outerWidth(),top:be.top,bottom:be.top+bc.outerHeight()};return bd}function F(bc,bd){return(bc.x>bd.left&&bc.x<bd.right&&bc.y>bd.top&&bc.y<bd.bottom)}}}));/*
 * jQuery Backstretch
 * Version 1.2.5
 * http://srobbin.com/jquery-plugins/jquery-backstretch/
 *
 * Add a dynamically-resized background image to the page
 *
 * Copyright (c) 2011 Scott Robbin (srobbin.com)
 * Dual licensed under the MIT and GPL licenses.
 */

(function($) {

    $.backstretch = function(src, options, callback) {
        var defaultSettings = {
                centeredX: true,         // Should we center the image on the X axis?
                centeredY: true,         // Should we center the image on the Y axis?
                speed: 0                 // fadeIn speed for background after image loads (e.g. "fast" or 500)
            },
            container = $("#backstretch"),
            settings = container.data("settings") || defaultSettings, // If this has been called once before, use the old settings as the default
            existingSettings = container.data('settings'),
            rootElement = ("onorientationchange" in window) ? $(document) : $(window), // hack to acccount for iOS position:fixed shortcomings
            imgRatio, bgImg, bgWidth, bgHeight, bgOffset, bgCSS;

        // Extend the settings with those the user has provided
        if(options && typeof options == "object") $.extend(settings, options);

        // Just in case the user passed in a function without options
        if(options && typeof options == "function") callback = options;

        // Initialize
        $(document).ready(_init);

        // For chaining
        return this;

        function _init() {
            // Prepend image, wrapped in a DIV, with some positioning and zIndex voodoo
            if(src) {
                var img;

                // If this is the first time that backstretch is being called
                if(container.length == 0) {
                    container = $("<div />").attr("id", "backstretch")
                        .css({left: 0, top: 0, position: "fixed", overflow: "hidden", zIndex: 1, margin: 0, padding: 0, height: "100%", width: "100%"});
                } else {
                    // Prepare to delete any old images
                    container.find("img").addClass("deleteable");
                }

                img = $("<img />").css({position: "absolute", display: "none", margin: 0, padding: 0, border: "none", zIndex: 1})
                    .bind("load", function(e) {
                        var self = $(this),
                            imgWidth, imgHeight;

                        self.css({width: "auto", height: "auto"});
                        imgWidth = this.width || $(e.target).width();
                        imgHeight = this.height || $(e.target).height();
                        imgRatio = imgWidth / imgHeight;

                        _adjustBG(function() {
                            self.fadeIn(settings.speed, function(){
                                // Remove the old images, if necessary.
                                container.find('.deleteable').remove();
                                // Callback
                                if(typeof callback == "function") callback();
                            });
                        });

                    })
                    .appendTo(container);

                // Append the container to the body, if it's not already there
                if($("body #backstretch").length == 0) {
                    $("body").append(container);
                }

                // Attach the settings
                container.data("settings", settings);

                img.attr("src", src); // Hack for IE img onload event
                // Adjust the background size when the window is resized or orientation has changed (iOS)
                $(window).resize(_adjustBG);
            }
        }

        function _adjustBG(fn) {
            try {
                bgCSS = {left: 0, top: 0}
                bgWidth = rootElement.width();
                bgHeight = bgWidth / imgRatio;

                // Make adjustments based on image ratio
                // Note: Offset code provided by Peter Baker (http://ptrbkr.com/). Thanks, Peter!
                if(bgHeight >= rootElement.height()) {
                    bgOffset = (bgHeight - rootElement.height()) /2;
                    if(settings.centeredY) $.extend(bgCSS, {top: "-" + bgOffset + "px"});
                } else {
                    bgHeight = rootElement.height();
                    bgWidth = bgHeight * imgRatio;
                    bgOffset = (bgWidth - rootElement.width()) / 2;
                    if(settings.centeredX) $.extend(bgCSS, {left: "-" + bgOffset + "px"});
                }

                $("#backstretch, #backstretch img:not(.deleteable)").width( bgWidth ).height( bgHeight )
                    .filter("img").css(bgCSS);
            } catch(err) {
                // IE7 seems to trigger _adjustBG before the image is loaded.
                // This try/catch block is a hack to let it fail gracefully.
            }

            // Executed the passed in function, if necessary
            if (typeof fn == "function") fn();
        }
    };

})(jQuery);
/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 *
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

;(function($) {
    var tmp, loading, overlay, wrap, outer, content, close, title, nav_left, nav_right,

        selectedIndex = 0, selectedOpts = {}, selectedArray = [], currentIndex = 0, currentOpts = {}, currentArray = [],

        ajaxLoader = null, imgPreloader = new Image(), imgRegExp = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, swfRegExp = /[^\.]\.(swf)\s*$/i,

        loadingTimer, loadingFrame = 1,

        titleHeight = 0, titleStr = '', start_pos, final_pos, busy = false, fx = $.extend($('<div/>')[0], { prop: 0 }),

        isIE6 = false; //$.browser.msie && $.browser.version < 7 && !window.XMLHttpRequest,

    /*
     * Private methods
     */

    _abort = function() {
        loading.hide();

        imgPreloader.onerror = imgPreloader.onload = null;

        if (ajaxLoader) {
            ajaxLoader.abort();
        }

        tmp.empty();
    },

        _error = function() {
            if (false === selectedOpts.onError(selectedArray, selectedIndex, selectedOpts)) {
                loading.hide();
                busy = false;
                return;
            }

            selectedOpts.titleShow = false;

            selectedOpts.width = 'auto';
            selectedOpts.height = 'auto';

            tmp.html( '<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>' );

            _process_inline();
        },

        _start = function() {
            var obj = selectedArray[ selectedIndex ],
                href,
                type,
                title,
                str,
                emb,
                ret;

            _abort();

            selectedOpts = $.extend({}, $.fn.fancybox.defaults, (typeof $(obj).data('fancybox') == 'undefined' ? selectedOpts : $(obj).data('fancybox')));

            ret = selectedOpts.onStart(selectedArray, selectedIndex, selectedOpts);

            if (ret === false) {
                busy = false;
                return;
            } else if (typeof ret == 'object') {
                selectedOpts = $.extend(selectedOpts, ret);
            }

            title = selectedOpts.title || (obj.nodeName ? $(obj).attr('title') : obj.title) || '';

            if (obj.nodeName && !selectedOpts.orig) {
                selectedOpts.orig = $(obj).children("img:first").length ? $(obj).children("img:first") : $(obj);
            }

            if (title === '' && selectedOpts.orig && selectedOpts.titleFromAlt) {
                title = selectedOpts.orig.attr('alt');
            }

            href = selectedOpts.href || (obj.nodeName ? $(obj).attr('href') : obj.href) || null;

            if ((/^(?:javascript)/i).test(href) || href == '#') {
                href = null;
            }

            if (selectedOpts.type) {
                type = selectedOpts.type;

                if (!href) {
                    href = selectedOpts.content;
                }

            } else if (selectedOpts.content) {
                type = 'html';

            } else if (href) {
                if (href.match(imgRegExp)) {
                    type = 'image';

                } else if (href.match(swfRegExp)) {
                    type = 'swf';

                } else if ($(obj).hasClass("iframe")) {
                    type = 'iframe';

                } else if (href.indexOf("#") === 0) {
                    type = 'inline';

                } else {
                    type = 'ajax';
                }
            }

            if (!type) {
                _error();
                return;
            }

            if (type == 'inline') {
                obj	= href.substr(href.indexOf("#"));
                // mgiger
                //type = $(obj).length > 0 ? 'inline' : 'ajax';
            }

            selectedOpts.type = type;
            selectedOpts.href = href;
            selectedOpts.title = title;

            if (selectedOpts.autoDimensions) {
                if (selectedOpts.type == 'html' || selectedOpts.type == 'inline' || selectedOpts.type == 'ajax') {
                    selectedOpts.width = 'auto';
                    selectedOpts.height = 'auto';
                } else {
                    selectedOpts.autoDimensions = false;
                }
            }

            if (selectedOpts.modal) {
                selectedOpts.overlayShow = true;
                selectedOpts.hideOnOverlayClick = false;
                selectedOpts.hideOnContentClick = false;
                selectedOpts.enableEscapeButton = false;
                selectedOpts.showCloseButton = false;
            }

            selectedOpts.padding = parseInt(selectedOpts.padding, 10);
            selectedOpts.margin = parseInt(selectedOpts.margin, 10);

            tmp.css('padding', (selectedOpts.padding + selectedOpts.margin));

            $('.fancybox-inline-tmp').unbind('fancybox-cancel').bind('fancybox-change', function() {
                $(this).replaceWith(content.children());
            });

            switch (type) {
                case 'html' :
                    tmp.html( selectedOpts.content );
                    _process_inline();
                    break;

                case 'inline' :
                    if ( $(obj).parent().is('#fancybox-content') === true) {
                        busy = false;
                        return;
                    }

                    $('<div class="fancybox-inline-tmp" />')
                        .hide()
                        .insertBefore( $(obj) )
                        .bind('fancybox-cleanup', function() {
                            $(this).replaceWith(content.children());
                        }).bind('fancybox-cancel', function() {
                        $(this).replaceWith(tmp.children());
                    });

                    $(obj).appendTo(tmp);

                    _process_inline();
                    break;

                case 'image':
                    busy = false;

                    $.fancybox.showActivity();

                    imgPreloader = new Image();

                    imgPreloader.onerror = function() {
                        _error();
                    };

                    imgPreloader.onload = function() {
                        busy = true;

                        imgPreloader.onerror = imgPreloader.onload = null;

                        _process_image();
                    };

                    imgPreloader.src = href;
                    break;

                case 'swf':
                    selectedOpts.scrolling = 'no';

                    str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"><param name="movie" value="' + href + '"></param>';
                    emb = '';

                    $.each(selectedOpts.swf, function(name, val) {
                        str += '<param name="' + name + '" value="' + val + '"></param>';
                        emb += ' ' + name + '="' + val + '"';
                    });

                    str += '<embed src="' + href + '" type="application/x-shockwave-flash" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"' + emb + '></embed></object>';

                    tmp.html(str);

                    _process_inline();
                    break;

                case 'ajax':
                    busy = false;

                    $.fancybox.showActivity();

                    selectedOpts.ajax.win = selectedOpts.ajax.success;

                    ajaxLoader = $.ajax($.extend({}, selectedOpts.ajax, {
                        url	: href,
                        data : selectedOpts.ajax.data || {},
                        error : function(XMLHttpRequest, textStatus, errorThrown) {
                            if ( XMLHttpRequest.status > 0 ) {
                                _error();
                            }
                        },
                        success : function(data, textStatus, XMLHttpRequest) {
                            var o = typeof XMLHttpRequest == 'object' ? XMLHttpRequest : ajaxLoader;
                            if (o.status == 200) {
                                if ( typeof selectedOpts.ajax.win == 'function' ) {
                                    ret = selectedOpts.ajax.win(href, data, textStatus, XMLHttpRequest);

                                    if (ret === false) {
                                        loading.hide();
                                        return;
                                    } else if (typeof ret == 'string' || typeof ret == 'object') {
                                        data = ret;
                                    }
                                }

                                tmp.html( data );
                                _process_inline();
                            }
                        }
                    }));

                    break;

                case 'iframe':
                    _show();
                    break;
            }
        },

        _process_inline = function() {
            var
                w = selectedOpts.width,
                h = selectedOpts.height;

            if (w.toString().indexOf('%') > -1) {
                w = parseInt( ($(window).width() - (selectedOpts.margin * 2)) * parseFloat(w) / 100, 10) + 'px';

            } else {
                w = w == 'auto' ? 'auto' : w + 'px';
            }

            if (h.toString().indexOf('%') > -1) {
                h = parseInt( ($(window).height() - (selectedOpts.margin * 2)) * parseFloat(h) / 100, 10) + 'px';

            } else {
                h = h == 'auto' ? 'auto' : h + 'px';
            }

            tmp.wrapInner('<div style="width:' + w + ';height:' + h + ';overflow: ' + (selectedOpts.scrolling == 'auto' ? 'auto' : (selectedOpts.scrolling == 'yes' ? 'scroll' : 'hidden')) + ';position:relative;"></div>');

            selectedOpts.width = tmp.width();
            selectedOpts.height = tmp.height();

            _show();
        },

        _process_image = function() {
            selectedOpts.width = imgPreloader.width;
            selectedOpts.height = imgPreloader.height;

            $("<img />").attr({
                'id' : 'fancybox-img',
                'src' : imgPreloader.src,
                'alt' : selectedOpts.title
            }).appendTo( tmp );

            _show();
        },

        _show = function() {
            var pos, equal;

            loading.hide();

            if (wrap.is(":visible") && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
                $.event.trigger('fancybox-cancel');

                busy = false;
                return;
            }

            busy = true;

            $(content.add( overlay )).unbind();

            $(window).unbind("resize.fb scroll.fb");
            $(document).unbind('keydown.fb');

            if (wrap.is(":visible") && currentOpts.titlePosition !== 'outside') {
                wrap.css('height', wrap.height());
            }

            currentArray = selectedArray;
            currentIndex = selectedIndex;
            currentOpts = selectedOpts;

            if (currentOpts.overlayShow) {
                overlay.css({
                    'background-color' : currentOpts.overlayColor,
                    'opacity' : currentOpts.overlayOpacity,
                    'cursor' : currentOpts.hideOnOverlayClick ? 'pointer' : 'auto',
                    'height' : $(document).height()
                });

                if (!overlay.is(':visible')) {
                    if (isIE6) {
                        $('select:not(#fancybox-tmp select)').filter(function() {
                            return this.style.visibility !== 'hidden';
                        }).css({'visibility' : 'hidden'}).one('fancybox-cleanup', function() {
                            this.style.visibility = 'inherit';
                        });
                    }

                    overlay.show();
                }
            } else {
                overlay.hide();
            }

            final_pos = _get_zoom_to();

            _process_title();

            if (wrap.is(":visible")) {
                $( close.add( nav_left ).add( nav_right ) ).hide();

                pos = wrap.position(),

                    start_pos = {
                        top	 : pos.top,
                        left : pos.left,
                        width : wrap.width(),
                        height : wrap.height()
                    };

                equal = (start_pos.width == final_pos.width && start_pos.height == final_pos.height);

                content.fadeTo(currentOpts.changeFade, 0.3, function() {
                    var finish_resizing = function() {
                        content.html( tmp.contents() ).fadeTo(currentOpts.changeFade, 1, _finish);
                    };

                    $.event.trigger('fancybox-change');

                    content
                        .empty()
                        .removeAttr('filter')
                        .css({
                            'border-width' : currentOpts.padding,
                            'width'	: final_pos.width - currentOpts.padding * 2,
                            'height' : selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
                        });

                    if (equal) {
                        finish_resizing();

                    } else {
                        fx.prop = 0;

                        $(fx).animate({prop: 1}, {
                            duration : currentOpts.changeSpeed,
                            easing : currentOpts.easingChange,
                            step : _draw,
                            complete : finish_resizing
                        });
                    }
                });

                return;
            }

            wrap.removeAttr("style");

            content.css('border-width', currentOpts.padding);

            if (currentOpts.transitionIn == 'elastic') {
                start_pos = _get_zoom_from();

                content.html( tmp.contents() );

                wrap.show();

                if (currentOpts.opacity) {
                    final_pos.opacity = 0;
                }

                fx.prop = 0;

                $(fx).animate({prop: 1}, {
                    duration : currentOpts.speedIn,
                    easing : currentOpts.easingIn,
                    step : _draw,
                    complete : _finish
                });

                return;
            }

            if (currentOpts.titlePosition == 'inside' && titleHeight > 0) {
                title.show();
            }

            content
                .css({
                    'width' : final_pos.width - currentOpts.padding * 2,
                    'height' : selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
                })
                .html( tmp.contents() );

            wrap
                .css(final_pos)
                .fadeIn( currentOpts.transitionIn == 'none' ? 0 : currentOpts.speedIn, _finish );
        },

        _format_title = function(title) {
            if (title && title.length) {
                if (currentOpts.titlePosition == 'float') {
                    return '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + title + '</td><td id="fancybox-title-float-right"></td></tr></table>';
                }

                return '<div id="fancybox-title-' + currentOpts.titlePosition + '">' + title + '</div>';
            }

            return false;
        },

        _process_title = function() {
            titleStr = currentOpts.title || '';
            titleHeight = 0;

            title
                .empty()
                .removeAttr('style')
                .removeClass();

            if (currentOpts.titleShow === false) {
                title.hide();
                return;
            }

            titleStr = $.isFunction(currentOpts.titleFormat) ? currentOpts.titleFormat(titleStr, currentArray, currentIndex, currentOpts) : _format_title(titleStr);

            if (!titleStr || titleStr === '') {
                title.hide();
                return;
            }

            title
                .addClass('fancybox-title-' + currentOpts.titlePosition)
                .html( titleStr )
                .appendTo( 'body' )
                .show();

            switch (currentOpts.titlePosition) {
                case 'inside':
                    title
                        .css({
                            'width' : final_pos.width - (currentOpts.padding * 2),
                            'marginLeft' : currentOpts.padding,
                            'marginRight' : currentOpts.padding
                        });

                    titleHeight = title.outerHeight(true);

                    title.appendTo( outer );

                    final_pos.height += titleHeight;
                    break;

                case 'over':
                    title
                        .css({
                            'marginLeft' : currentOpts.padding,
                            'width'	: final_pos.width - (currentOpts.padding * 2),
                            'bottom' : currentOpts.padding
                        })
                        .appendTo( outer );
                    break;

                case 'float':
                    title
                        .css('left', parseInt((title.width() - final_pos.width - 40)/ 2, 10) * -1)
                        .appendTo( wrap );
                    break;

                default:
                    title
                        .css({
                            'width' : final_pos.width - (currentOpts.padding * 2),
                            'paddingLeft' : currentOpts.padding,
                            'paddingRight' : currentOpts.padding
                        })
                        .appendTo( wrap );
                    break;
            }

            title.hide();
        },

        _set_navigation = function() {
            if (currentOpts.enableEscapeButton || currentOpts.enableKeyboardNav) {
                $(document).bind('keydown.fb', function(e) {
                    if (e.keyCode == 27 && currentOpts.enableEscapeButton) {
                        e.preventDefault();
                        $.fancybox.close();

                    } else if ((e.keyCode == 37 || e.keyCode == 39) && currentOpts.enableKeyboardNav && e.target.tagName !== 'INPUT' && e.target.tagName !== 'TEXTAREA' && e.target.tagName !== 'SELECT') {
                        e.preventDefault();
                        $.fancybox[ e.keyCode == 37 ? 'prev' : 'next']();
                    }
                });
            }

            if (!currentOpts.showNavArrows) {
                nav_left.hide();
                nav_right.hide();
                return;
            }

            if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex !== 0) {
                nav_left.show();
            }

            if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex != (currentArray.length -1)) {
                nav_right.show();
            }
        },

        _finish = function () {
            if (!$.support.opacity) {
                content.get(0).style.removeAttribute('filter');
                wrap.get(0).style.removeAttribute('filter');
            }

            if (selectedOpts.autoDimensions) {
                content.css('height', 'auto');
            }

            wrap.css('height', 'auto');

            if (titleStr && titleStr.length) {
                title.show();
            }

            if (currentOpts.showCloseButton) {
                close.show();
            }

            _set_navigation();

            if (currentOpts.hideOnContentClick)	{
                content.bind('click', $.fancybox.close);
            }

            if (currentOpts.hideOnOverlayClick)	{
                overlay.bind('click', $.fancybox.close);
            }

            $(window).bind("resize.fb", $.fancybox.resize);

            if (currentOpts.centerOnScroll) {
                $(window).bind("scroll.fb", $.fancybox.center);
            }

            if (currentOpts.type == 'iframe') {
                $('<iframe id="fancybox-frame" name="fancybox-frame' + new Date().getTime() + '" frameborder="0" hspace="0" ' + (false ? 'allowtransparency="true""' : '') + ' scrolling="' + selectedOpts.scrolling + '" src="' + currentOpts.href + '"></iframe>').appendTo(content);
            }

            wrap.show();

            busy = false;

            $.fancybox.center();

            currentOpts.onComplete(currentArray, currentIndex, currentOpts);

            _preload_images();
        },

        _preload_images = function() {
            var href,
                objNext;

            if ((currentArray.length -1) > currentIndex) {
                href = currentArray[ currentIndex + 1 ].href;

                if (typeof href !== 'undefined' && href.match(imgRegExp)) {
                    objNext = new Image();
                    objNext.src = href;
                }
            }

            if (currentIndex > 0) {
                href = currentArray[ currentIndex - 1 ].href;

                if (typeof href !== 'undefined' && href.match(imgRegExp)) {
                    objNext = new Image();
                    objNext.src = href;
                }
            }
        },

        _draw = function(pos) {
            var dim = {
                width : parseInt(start_pos.width + (final_pos.width - start_pos.width) * pos, 10),
                height : parseInt(start_pos.height + (final_pos.height - start_pos.height) * pos, 10),

                top : parseInt(start_pos.top + (final_pos.top - start_pos.top) * pos, 10),
                left : parseInt(start_pos.left + (final_pos.left - start_pos.left) * pos, 10)
            };

            if (typeof final_pos.opacity !== 'undefined') {
                dim.opacity = pos < 0.5 ? 0.5 : pos;
            }

            wrap.css(dim);

            content.css({
                'width' : dim.width - currentOpts.padding * 2,
                'height' : dim.height - (titleHeight * pos) - currentOpts.padding * 2
            });
        },

        _get_viewport = function() {
            return [
                $(window).width() - (currentOpts.margin * 2),
                $(window).height() - (currentOpts.margin * 2),
                $(document).scrollLeft() + currentOpts.margin,
                $(document).scrollTop() + currentOpts.margin
            ];
        },

        _get_zoom_to = function () {
            var view = _get_viewport(),
                to = {},
                resize = currentOpts.autoScale,
                double_padding = currentOpts.padding * 2,
                ratio;

            if (currentOpts.width.toString().indexOf('%') > -1) {
                to.width = parseInt((view[0] * parseFloat(currentOpts.width)) / 100, 10);
            } else {
                to.width = currentOpts.width + double_padding;
            }

            if (currentOpts.height.toString().indexOf('%') > -1) {
                to.height = parseInt((view[1] * parseFloat(currentOpts.height)) / 100, 10);
            } else {
                to.height = currentOpts.height + double_padding;
            }

            if (resize && (to.width > view[0] || to.height > view[1])) {
                if (selectedOpts.type == 'image' || selectedOpts.type == 'swf') {
                    ratio = (currentOpts.width ) / (currentOpts.height );

                    if ((to.width ) > view[0]) {
                        to.width = view[0];
                        to.height = parseInt(((to.width - double_padding) / ratio) + double_padding, 10);
                    }

                    if ((to.height) > view[1]) {
                        to.height = view[1];
                        to.width = parseInt(((to.height - double_padding) * ratio) + double_padding, 10);
                    }

                } else {
                    to.width = Math.min(to.width, view[0]);
                    to.height = Math.min(to.height, view[1]);
                }
            }

            to.top = parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - to.height - 40) * 0.5)), 10);
            to.left = parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - to.width - 40) * 0.5)), 10);

            return to;
        },

        _get_obj_pos = function(obj) {
            var pos = obj.offset();

            pos.top += parseInt( obj.css('paddingTop'), 10 ) || 0;
            pos.left += parseInt( obj.css('paddingLeft'), 10 ) || 0;

            pos.top += parseInt( obj.css('border-top-width'), 10 ) || 0;
            pos.left += parseInt( obj.css('border-left-width'), 10 ) || 0;

            pos.width = obj.width();
            pos.height = obj.height();

            return pos;
        },

        _get_zoom_from = function() {
            var orig = selectedOpts.orig ? $(selectedOpts.orig) : false,
                from = {},
                pos,
                view;

            if (orig && orig.length) {
                pos = _get_obj_pos(orig);

                from = {
                    width : pos.width + (currentOpts.padding * 2),
                    height : pos.height + (currentOpts.padding * 2),
                    top	: pos.top - currentOpts.padding - 20,
                    left : pos.left - currentOpts.padding - 20
                };

            } else {
                view = _get_viewport();

                from = {
                    width : currentOpts.padding * 2,
                    height : currentOpts.padding * 2,
                    top	: parseInt(view[3] + view[1] * 0.5, 10),
                    left : parseInt(view[2] + view[0] * 0.5, 10)
                };
            }

            return from;
        },

        _animate_loading = function() {
            if (!loading.is(':visible')){
                clearInterval(loadingTimer);
                return;
            }

            $('div', loading).css('top', (loadingFrame * -40) + 'px');

            loadingFrame = (loadingFrame + 1) % 12;
        };

    /*
     * Public methods
     */

    $.fn.fancybox = function(options) {
        if (!$(this).length) {
            return this;
        }

        $(this)
            .data('fancybox', $.extend({}, options, ($.metadata ? $(this).metadata() : {})))
            .unbind('click.fb')
            .bind('click.fb', function(e) {
                e.preventDefault();

                if (busy) {
                    return;
                }

                busy = true;

                $(this).blur();

                selectedArray = [];
                selectedIndex = 0;

                var rel = $(this).attr('rel') || '';

                if (!rel || rel == '' || rel === 'nofollow') {
                    selectedArray.push(this);

                } else {
                    selectedArray = $("a[rel=" + rel + "], area[rel=" + rel + "]");
                    selectedIndex = selectedArray.index( this );
                }

                _start();

                return;
            });

        return this;
    };

    $.fancybox = function(obj) {
        var opts;

        if (busy) {
            return;
        }

        busy = true;
        opts = typeof arguments[1] !== 'undefined' ? arguments[1] : {};

        selectedArray = [];
        selectedIndex = parseInt(opts.index, 10) || 0;

        if ($.isArray(obj)) {
            for (var i = 0, j = obj.length; i < j; i++) {
                if (typeof obj[i] == 'object') {
                    $(obj[i]).data('fancybox', $.extend({}, opts, obj[i]));
                } else {
                    obj[i] = $({}).data('fancybox', $.extend({content : obj[i]}, opts));
                }
            }

            selectedArray = jQuery.merge(selectedArray, obj);

        } else {
            if (typeof obj == 'object') {
                $(obj).data('fancybox', $.extend({}, opts, obj));
            } else {
                obj = $({}).data('fancybox', $.extend({content : obj}, opts));
            }

            selectedArray.push(obj);
        }

        if (selectedIndex > selectedArray.length || selectedIndex < 0) {
            selectedIndex = 0;
        }

        _start();
    };

    $.fancybox.showActivity = function() {
        clearInterval(loadingTimer);

        loading.show();
        loadingTimer = setInterval(_animate_loading, 66);
    };

    $.fancybox.hideActivity = function() {
        loading.hide();
    };

    $.fancybox.next = function() {
        return $.fancybox.pos( currentIndex + 1);
    };

    $.fancybox.prev = function() {
        return $.fancybox.pos( currentIndex - 1);
    };

    $.fancybox.pos = function(pos) {
        if (busy) {
            return;
        }

        pos = parseInt(pos);

        selectedArray = currentArray;

        if (pos > -1 && pos < currentArray.length) {
            selectedIndex = pos;
            _start();

        } else if (currentOpts.cyclic && currentArray.length > 1) {
            selectedIndex = pos >= currentArray.length ? 0 : currentArray.length - 1;
            _start();
        }

        return;
    };

    $.fancybox.cancel = function() {
        if (busy) {
            return;
        }

        busy = true;

        $.event.trigger('fancybox-cancel');

        _abort();

        selectedOpts.onCancel(selectedArray, selectedIndex, selectedOpts);

        busy = false;
    };

    // Note: within an iframe use - parent.$.fancybox.close();
    $.fancybox.close = function() {
        if (busy || wrap.is(':hidden')) {
            return;
        }

        busy = true;

        if (currentOpts && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
            busy = false;
            return;
        }

        _abort();

        $(close.add( nav_left ).add( nav_right )).hide();

        $(content.add( overlay )).unbind();

        $(window).unbind("resize.fb scroll.fb");
        $(document).unbind('keydown.fb');

        content.find('iframe').attr('src', isIE6 && /^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank');

        if (currentOpts.titlePosition !== 'inside') {
            title.empty();
        }

        wrap.stop();

        function _cleanup() {
            overlay.fadeOut('fast');

            title.empty().hide();
            wrap.hide();

            $.event.trigger('fancybox-cleanup');

            // Hack by mg, orherwise it won't open twice
            //content.empty();

            currentOpts.onClosed(currentArray, currentIndex, currentOpts);

            currentArray = selectedOpts	= [];
            currentIndex = selectedIndex = 0;
            currentOpts = selectedOpts	= {};

            busy = false;
        }

        if (currentOpts.transitionOut == 'elastic') {
            start_pos = _get_zoom_from();

            var pos = wrap.position();

            final_pos = {
                top	 : pos.top ,
                left : pos.left,
                width :	wrap.width(),
                height : wrap.height()
            };

            if (currentOpts.opacity) {
                final_pos.opacity = 1;
            }

            title.empty().hide();

            fx.prop = 1;

            $(fx).animate({ prop: 0 }, {
                duration : currentOpts.speedOut,
                easing : currentOpts.easingOut,
                step : _draw,
                complete : _cleanup
            });

        } else {
            wrap.fadeOut( currentOpts.transitionOut == 'none' ? 0 : currentOpts.speedOut, _cleanup);
        }
    };

    $.fancybox.resize = function() {
        if (overlay.is(':visible')) {
            overlay.css('height', $(document).height());
        }

        $.fancybox.center(true);
    };

    $.fancybox.center = function() {
        var view, align;

        if (busy) {
            return;
        }

        align = arguments[0] === true ? 1 : 0;
        view = _get_viewport();

        if (!align && (wrap.width() > view[0] || wrap.height() > view[1])) {
            return;
        }

        wrap
            .stop()
            .animate({
                'top' : parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - content.height() - 40) * 0.5) - currentOpts.padding)),
                'left' : parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - content.width() - 40) * 0.5) - currentOpts.padding))
            }, typeof arguments[0] == 'number' ? arguments[0] : 200);
    };

    $.fancybox.init = function() {
        if ($("#fancybox-wrap").length) {
            return;
        }

        $('body').append(
            tmp	= $('<div id="fancybox-tmp"></div>'),
            loading	= $('<div id="fancybox-loading"><div></div></div>'),
            overlay	= $('<div id="fancybox-overlay"></div>'),
            wrap = $('<div id="fancybox-wrap"></div>')
        );

        outer = $('<div id="fancybox-outer"></div>')
            .append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>')
            .appendTo( wrap );

        outer.append(
            content = $('<div id="fancybox-content"></div>'),
            close = $('<a id="fancybox-close"></a>'),
            title = $('<div id="fancybox-title"></div>'),

            nav_left = $('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'),
            nav_right = $('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')
        );

        close.click($.fancybox.close);
        loading.click($.fancybox.cancel);

        nav_left.click(function(e) {
            e.preventDefault();
            $.fancybox.prev();
        });

        nav_right.click(function(e) {
            e.preventDefault();
            $.fancybox.next();
        });

        if ($.fn.mousewheel) {
            wrap.bind('mousewheel.fb', function(e, delta) {
                if (busy) {
                    e.preventDefault();

                } else if ($(e.target).get(0).clientHeight == 0 || $(e.target).get(0).scrollHeight === $(e.target).get(0).clientHeight) {
                    e.preventDefault();
                    $.fancybox[ delta > 0 ? 'prev' : 'next']();
                }
            });
        }

        if (!$.support.opacity) {
            wrap.addClass('fancybox-ie');
        }

        if (isIE6) {
            loading.addClass('fancybox-ie6');
            wrap.addClass('fancybox-ie6');

            $('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank' ) + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(outer);
        }
    };

    $.fn.fancybox.defaults = {
        padding : 10,
        margin : 40,
        opacity : false,
        modal : false,
        cyclic : false,
        scrolling : 'auto',	// 'auto', 'yes' or 'no'

        width : 560,
        height : 340,

        autoScale : true,
        autoDimensions : true,
        centerOnScroll : false,

        ajax : {},
        swf : { wmode: 'transparent' },

        hideOnOverlayClick : true,
        hideOnContentClick : false,

        overlayShow : true,
        overlayOpacity : 0.7,
        overlayColor : '#777',

        titleShow : true,
        titlePosition : 'float', // 'float', 'outside', 'inside' or 'over'
        titleFormat : null,
        titleFromAlt : false,

        transitionIn : 'fade', // 'elastic', 'fade' or 'none'
        transitionOut : 'fade', // 'elastic', 'fade' or 'none'

        speedIn : 300,
        speedOut : 300,

        changeSpeed : 300,
        changeFade : 'fast',

        easingIn : 'swing',
        easingOut : 'swing',

        showCloseButton	 : true,
        showNavArrows : true,
        enableEscapeButton : true,
        enableKeyboardNav : true,

        onStart : function(){},
        onCancel : function(){},
        onComplete : function(){},
        onCleanup : function(){},
        onClosed : function(){},
        onError : function(){}
    };

    $(document).ready(function() {
        $.fancybox.init();
    });

})(jQuery);//
// https://github.com/sukhoi1, 25 Jan 2016
// bicubic-img-interpolation v0.1.0 jQuery plugin

(function ( $ ) {

    $.fn.bicubicImgInterpolation = function(settings) {
        var self = this;
        $(self).each(function() {
            if(this.tagName === "IMG") {
                var src = $(this).attr('src');
                var imgW = this.width;
                var imgH = this.height;
                $(this).after("<canvas style='display: none' width='" + imgW +  "' height='" + imgH + "'></canvas>");
                var can = $(this).next()[0];
                var callback = drawHighResolutionImgThumbnail;
                drawCanvas(can, imgW*6, imgH*6, src, callback, this, settings.crossOrigin);
            }
        });

        function drawCanvas(can, imgW, imgH, src, callback, imgEl, crossOrigin) {

            var ctx = can.getContext('2d');
            var img = new Image();
            if(crossOrigin) {
                img.setAttribute('crossOrigin', 'anonymous'); //tainted canvases may not be exported chrome, ie will also throw security error
            }

            var w = imgW;
            var h = imgH;

            img.onload = function() {

                // Step it down several times
                var can2 = document.createElement('canvas');
                can2.width = w;
                can2.height = h;
                var ctx2 = can2.getContext('2d');

                // Draw it at 1/2 size 3 times (step down three times)
                ctx2.drawImage(img, 0, 0, w/2, h/2);
                ctx2.drawImage(can2, 0, 0, w/2, h/2, 0, 0, w/4, h/4);
                ctx2.drawImage(can2, 0, 0, w/4, h/4, 0, 0, w/6, h/6);
                ctx.drawImage(can2, 0, 0, w/6, h/6, 0, 0, w/6, h/6);
                if(callback) {
                    callback(can, this.src, imgEl);
                }
            };

            img.src = src;
        };

        function drawHighResolutionImgThumbnail(can, attrSrc, imgEl) {
            $(imgEl).attr('src', can.toDataURL("image/png"));
            $(imgEl).attr('data-src', attrSrc);
        };
    };

}( jQuery ));

/*
 * jQuery mmenu v5.5.2
 * @requires jQuery 1.7.0 or later
 *
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 * www.frebsite.nl
 *
 * Licensed under the MIT license:
 * http://en.wikipedia.org/wiki/MIT_License
 */
!function(e){function n(){e[t].glbl||(l={$wndw:e(window),$html:e("html"),$body:e("body")},a={},i={},r={},e.each([a,i,r],function(e,n){n.add=function(e){e=e.split(" ");for(var t=0,s=e.length;s>t;t++)n[e[t]]=n.mm(e[t])}}),a.mm=function(e){return"mm-"+e},a.add("wrapper menu panels panel nopanel current highest opened subopened navbar hasnavbar title btn prev next listview nolistview inset vertical selected divider spacer hidden fullsubopen"),a.umm=function(e){return"mm-"==e.slice(0,3)&&(e=e.slice(3)),e},i.mm=function(e){return"mm-"+e},i.add("parent sub"),r.mm=function(e){return e+".mm"},r.add("transitionend webkitTransitionEnd mousedown mouseup touchstart touchmove touchend click keydown"),e[t]._c=a,e[t]._d=i,e[t]._e=r,e[t].glbl=l)}var t="mmenu",s="5.5.2";if(!(e[t]&&e[t].version>s)){e[t]=function(e,n,t){this.$menu=e,this._api=["bind","init","update","setSelected","getInstance","openPanel","closePanel","closeAllPanels"],this.opts=n,this.conf=t,this.vars={},this.cbck={},"function"==typeof this.___deprecated&&this.___deprecated(),this._initMenu(),this._initAnchors();var s=this.$pnls.children();return this._initAddons(),this.init(s),"function"==typeof this.___debug&&this.___debug(),this},e[t].version=s,e[t].addons={},e[t].uniqueId=0,e[t].defaults={extensions:[],navbar:{add:!0,title:"",titleLink:"panel"},onClick:{setSelected:!0},slidingSubmenus:!0},e[t].configuration={classNames:{divider:"Divider",inset:"Inset",panel:"Panel",selected:"Selected",spacer:"Spacer",vertical:"Vertical"},clone:!1,openingInterval:25,panelNodetype:"ul, ol, div",transitionDuration:400},e[t].prototype={init:function(e){e=e.not("."+a.nopanel),e=this._initPanels(e),this.trigger("init",e),this.trigger("update")},update:function(){this.trigger("update")},setSelected:function(e){this.$menu.find("."+a.listview).children().removeClass(a.selected),e.addClass(a.selected),this.trigger("setSelected",e)},openPanel:function(n){var s=n.parent();if(s.hasClass(a.vertical)){var i=s.parents("."+a.subopened);if(i.length)return this.openPanel(i.first());s.addClass(a.opened)}else{if(n.hasClass(a.current))return;var r=this.$pnls.children("."+a.panel),l=r.filter("."+a.current);r.removeClass(a.highest).removeClass(a.current).not(n).not(l).not("."+a.vertical).addClass(a.hidden),e[t].support.csstransitions||l.addClass(a.hidden),n.hasClass(a.opened)?n.nextAll("."+a.opened).addClass(a.highest).removeClass(a.opened).removeClass(a.subopened):(n.addClass(a.highest),l.addClass(a.subopened)),n.removeClass(a.hidden).addClass(a.current),setTimeout(function(){n.removeClass(a.subopened).addClass(a.opened)},this.conf.openingInterval)}this.trigger("openPanel",n)},closePanel:function(e){var n=e.parent();n.hasClass(a.vertical)&&(n.removeClass(a.opened),this.trigger("closePanel",e))},closeAllPanels:function(){this.$menu.find("."+a.listview).children().removeClass(a.selected).filter("."+a.vertical).removeClass(a.opened);var e=this.$menu.children("."+a.panel),n=e.first();this.$menu.children("."+a.panel).not(n).removeClass(a.subopened).removeClass(a.opened).removeClass(a.current).removeClass(a.highest).addClass(a.hidden),this.openPanel(n)},togglePanel:function(e){var n=e.parent();n.hasClass(a.vertical)&&this[n.hasClass(a.opened)?"closePanel":"openPanel"](e)},getInstance:function(){return this},bind:function(e,n){this.cbck[e]=this.cbck[e]||[],this.cbck[e].push(n)},trigger:function(){var e=this,n=Array.prototype.slice.call(arguments),t=n.shift();if(this.cbck[t])for(var s=0,a=this.cbck[t].length;a>s;s++)this.cbck[t][s].apply(e,n)},_initMenu:function(){this.opts.offCanvas&&this.conf.clone&&(this.$menu=this.$menu.clone(!0),this.$menu.add(this.$menu.find("[id]")).filter("[id]").each(function(){e(this).attr("id",a.mm(e(this).attr("id")))})),this.$menu.contents().each(function(){3==e(this)[0].nodeType&&e(this).remove()}),this.$pnls=e('<div class="'+a.panels+'" />').append(this.$menu.children(this.conf.panelNodetype)).prependTo(this.$menu),this.$menu.parent().addClass(a.wrapper);var n=[a.menu];this.opts.slidingSubmenus||n.push(a.vertical),this.opts.extensions=this.opts.extensions.length?"mm-"+this.opts.extensions.join(" mm-"):"",this.opts.extensions&&n.push(this.opts.extensions),this.$menu.addClass(n.join(" "))},_initPanels:function(n){var t=this,s=this.__findAddBack(n,"ul, ol");this.__refactorClass(s,this.conf.classNames.inset,"inset").addClass(a.nolistview+" "+a.nopanel),s.not("."+a.nolistview).addClass(a.listview);var r=this.__findAddBack(n,"."+a.listview).children();this.__refactorClass(r,this.conf.classNames.selected,"selected"),this.__refactorClass(r,this.conf.classNames.divider,"divider"),this.__refactorClass(r,this.conf.classNames.spacer,"spacer"),this.__refactorClass(this.__findAddBack(n,"."+this.conf.classNames.panel),this.conf.classNames.panel,"panel");var l=e(),d=n.add(n.find("."+a.panel)).add(this.__findAddBack(n,"."+a.listview).children().children(this.conf.panelNodetype)).not("."+a.nopanel);this.__refactorClass(d,this.conf.classNames.vertical,"vertical"),this.opts.slidingSubmenus||d.addClass(a.vertical),d.each(function(){var n=e(this),s=n;n.is("ul, ol")?(n.wrap('<div class="'+a.panel+'" />'),s=n.parent()):s.addClass(a.panel);var i=n.attr("id");n.removeAttr("id"),s.attr("id",i||t.__getUniqueId()),n.hasClass(a.vertical)&&(n.removeClass(t.conf.classNames.vertical),s.add(s.parent()).addClass(a.vertical)),l=l.add(s)});var o=e("."+a.panel,this.$menu);l.each(function(){var n=e(this),s=n.parent(),r=s.children("a, span").first();if(s.is("."+a.panels)||(s.data(i.sub,n),n.data(i.parent,s)),!s.children("."+a.next).length&&s.parent().is("."+a.listview)){var l=n.attr("id"),d=e('<a class="'+a.next+'" href="#'+l+'" data-target="#'+l+'" />').insertBefore(r);r.is("span")&&d.addClass(a.fullsubopen)}if(!n.children("."+a.navbar).length&&!s.hasClass(a.vertical)){if(s.parent().is("."+a.listview))var s=s.closest("."+a.panel);else var r=s.closest("."+a.panel).find('a[href="#'+n.attr("id")+'"]').first(),s=r.closest("."+a.panel);var o=e('<div class="'+a.navbar+'" />');if(s.length){var l=s.attr("id");switch(t.opts.navbar.titleLink){case"anchor":_url=r.attr("href");break;case"panel":case"parent":_url="#"+l;break;case"none":default:_url=!1}o.append('<a class="'+a.btn+" "+a.prev+'" href="#'+l+'" data-target="#'+l+'" />').append(e('<a class="'+a.title+'"'+(_url?' href="'+_url+'"':"")+" />").text(r.text())).prependTo(n),t.opts.navbar.add&&n.addClass(a.hasnavbar)}else t.opts.navbar.title&&(o.append('<a class="'+a.title+'">'+t.opts.navbar.title+"</a>").prependTo(n),t.opts.navbar.add&&n.addClass(a.hasnavbar))}});var c=this.__findAddBack(n,"."+a.listview).children("."+a.selected).removeClass(a.selected).last().addClass(a.selected);c.add(c.parentsUntil("."+a.menu,"li")).filter("."+a.vertical).addClass(a.opened).end().not("."+a.vertical).each(function(){e(this).parentsUntil("."+a.menu,"."+a.panel).not("."+a.vertical).first().addClass(a.opened).parentsUntil("."+a.menu,"."+a.panel).not("."+a.vertical).first().addClass(a.opened).addClass(a.subopened)}),c.children("."+a.panel).not("."+a.vertical).addClass(a.opened).parentsUntil("."+a.menu,"."+a.panel).not("."+a.vertical).first().addClass(a.opened).addClass(a.subopened);var h=o.filter("."+a.opened);return h.length||(h=l.first()),h.addClass(a.opened).last().addClass(a.current),l.not("."+a.vertical).not(h.last()).addClass(a.hidden).end().appendTo(this.$pnls),l},_initAnchors:function(){var n=this;l.$body.on(r.click+"-oncanvas","a[href]",function(s){var i=e(this),r=!1,l=n.$menu.find(i).length;for(var d in e[t].addons)if(r=e[t].addons[d].clickAnchor.call(n,i,l))break;if(!r&&l){var o=i.attr("href");if(o.length>1&&"#"==o.slice(0,1))try{var c=e(o,n.$menu);c.is("."+a.panel)&&(r=!0,n[i.parent().hasClass(a.vertical)?"togglePanel":"openPanel"](c))}catch(h){}}if(r&&s.preventDefault(),!r&&l&&i.is("."+a.listview+" > li > a")&&!i.is('[rel="external"]')&&!i.is('[target="_blank"]')){n.__valueOrFn(n.opts.onClick.setSelected,i)&&n.setSelected(e(s.target).parent());var p=n.__valueOrFn(n.opts.onClick.preventDefault,i,"#"==o.slice(0,1));p&&s.preventDefault(),n.__valueOrFn(n.opts.onClick.close,i,p)&&n.close()}})},_initAddons:function(){for(var n in e[t].addons)e[t].addons[n].add.call(this),e[t].addons[n].add=function(){};for(var n in e[t].addons)e[t].addons[n].setup.call(this)},__api:function(){var n=this,t={};return e.each(this._api,function(){var e=this;t[e]=function(){var s=n[e].apply(n,arguments);return"undefined"==typeof s?t:s}}),t},__valueOrFn:function(e,n,t){return"function"==typeof e?e.call(n[0]):"undefined"==typeof e&&"undefined"!=typeof t?t:e},__refactorClass:function(e,n,t){return e.filter("."+n).removeClass(n).addClass(a[t])},__findAddBack:function(e,n){return e.find(n).add(e.filter(n))},__filterListItems:function(e){return e.not("."+a.divider).not("."+a.hidden)},__transitionend:function(e,n,t){var s=!1,a=function(){s||n.call(e[0]),s=!0};e.one(r.transitionend,a),e.one(r.webkitTransitionEnd,a),setTimeout(a,1.1*t)},__getUniqueId:function(){return a.mm(e[t].uniqueId++)}},e.fn[t]=function(s,a){return n(),s=e.extend(!0,{},e[t].defaults,s),a=e.extend(!0,{},e[t].configuration,a),this.each(function(){var n=e(this);if(!n.data(t)){var i=new e[t](n,s,a);n.data(t,i.__api())}})},e[t].support={touch:"ontouchstart"in window||navigator.msMaxTouchPoints,csstransitions:function(){if("undefined"!=typeof Modernizr&&"undefined"!=typeof Modernizr.csstransitions)return Modernizr.csstransitions;var e=document.body||document.documentElement,n=e.style,t="transition";if("string"==typeof n[t])return!0;var s=["Moz","webkit","Webkit","Khtml","O","ms"];t=t.charAt(0).toUpperCase()+t.substr(1);for(var a=0;a<s.length;a++)if("string"==typeof n[s[a]+t])return!0;return!1}()};var a,i,r,l}}(jQuery);
/*
 * jQuery mmenu offCanvas addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(e){var t="mmenu",o="offCanvas";e[t].addons[o]={setup:function(){if(this.opts[o]){var s=this.opts[o],i=this.conf[o];a=e[t].glbl,this._api=e.merge(this._api,["open","close","setPage"]),("top"==s.position||"bottom"==s.position)&&(s.zposition="front"),"string"!=typeof i.pageSelector&&(i.pageSelector="> "+i.pageNodetype),a.$allMenus=(a.$allMenus||e()).add(this.$menu),this.vars.opened=!1;var r=[n.offcanvas];"left"!=s.position&&r.push(n.mm(s.position)),"back"!=s.zposition&&r.push(n.mm(s.zposition)),this.$menu.addClass(r.join(" ")).parent().removeClass(n.wrapper),this.setPage(a.$page),this._initBlocker(),this["_initWindow_"+o](),this.$menu[i.menuInjectMethod+"To"](i.menuWrapperSelector)}},add:function(){n=e[t]._c,s=e[t]._d,i=e[t]._e,n.add("offcanvas slideout blocking modal background opening blocker page"),s.add("style"),i.add("resize")},clickAnchor:function(e){if(!this.opts[o])return!1;var t=this.$menu.attr("id");if(t&&t.length&&(this.conf.clone&&(t=n.umm(t)),e.is('[href="#'+t+'"]')))return this.open(),!0;if(a.$page){var t=a.$page.first().attr("id");return t&&t.length&&e.is('[href="#'+t+'"]')?(this.close(),!0):!1}}},e[t].defaults[o]={position:"left",zposition:"back",blockUI:!0,moveBackground:!0},e[t].configuration[o]={pageNodetype:"div",pageSelector:null,noPageSelector:[],wrapPageIfNeeded:!0,menuWrapperSelector:"body",menuInjectMethod:"prepend"},e[t].prototype.open=function(){if(!this.vars.opened){var e=this;this._openSetup(),setTimeout(function(){e._openFinish()},this.conf.openingInterval),this.trigger("open")}},e[t].prototype._openSetup=function(){var t=this,r=this.opts[o];this.closeAllOthers(),a.$page.each(function(){e(this).data(s.style,e(this).attr("style")||"")}),a.$wndw.trigger(i.resize+"-"+o,[!0]);var p=[n.opened];r.blockUI&&p.push(n.blocking),"modal"==r.blockUI&&p.push(n.modal),r.moveBackground&&p.push(n.background),"left"!=r.position&&p.push(n.mm(this.opts[o].position)),"back"!=r.zposition&&p.push(n.mm(this.opts[o].zposition)),this.opts.extensions&&p.push(this.opts.extensions),a.$html.addClass(p.join(" ")),setTimeout(function(){t.vars.opened=!0},this.conf.openingInterval),this.$menu.addClass(n.current+" "+n.opened)},e[t].prototype._openFinish=function(){var e=this;this.__transitionend(a.$page.first(),function(){e.trigger("opened")},this.conf.transitionDuration),a.$html.addClass(n.opening),this.trigger("opening")},e[t].prototype.close=function(){if(this.vars.opened){var t=this;this.__transitionend(a.$page.first(),function(){t.$menu.removeClass(n.current).removeClass(n.opened),a.$html.removeClass(n.opened).removeClass(n.blocking).removeClass(n.modal).removeClass(n.background).removeClass(n.mm(t.opts[o].position)).removeClass(n.mm(t.opts[o].zposition)),t.opts.extensions&&a.$html.removeClass(t.opts.extensions),a.$page.each(function(){e(this).attr("style",e(this).data(s.style))}),t.vars.opened=!1,t.trigger("closed")},this.conf.transitionDuration),a.$html.removeClass(n.opening),this.trigger("close"),this.trigger("closing")}},e[t].prototype.closeAllOthers=function(){a.$allMenus.not(this.$menu).each(function(){var o=e(this).data(t);o&&o.close&&o.close()})},e[t].prototype.setPage=function(t){var s=this,i=this.conf[o];t&&t.length||(t=a.$body.find(i.pageSelector),i.noPageSelector.length&&(t=t.not(i.noPageSelector.join(", "))),t.length>1&&i.wrapPageIfNeeded&&(t=t.wrapAll("<"+this.conf[o].pageNodetype+" />").parent())),t.each(function(){e(this).attr("id",e(this).attr("id")||s.__getUniqueId())}),t.addClass(n.page+" "+n.slideout),a.$page=t,this.trigger("setPage",t)},e[t].prototype["_initWindow_"+o]=function(){a.$wndw.off(i.keydown+"-"+o).on(i.keydown+"-"+o,function(e){return a.$html.hasClass(n.opened)&&9==e.keyCode?(e.preventDefault(),!1):void 0});var e=0;a.$wndw.off(i.resize+"-"+o).on(i.resize+"-"+o,function(t,o){if(1==a.$page.length&&(o||a.$html.hasClass(n.opened))){var s=a.$wndw.height();(o||s!=e)&&(e=s,a.$page.css("minHeight",s))}})},e[t].prototype._initBlocker=function(){var t=this;this.opts[o].blockUI&&(a.$blck||(a.$blck=e('<div id="'+n.blocker+'" class="'+n.slideout+'" />')),a.$blck.appendTo(a.$body).off(i.touchstart+"-"+o+" "+i.touchmove+"-"+o).on(i.touchstart+"-"+o+" "+i.touchmove+"-"+o,function(e){e.preventDefault(),e.stopPropagation(),a.$blck.trigger(i.mousedown+"-"+o)}).off(i.mousedown+"-"+o).on(i.mousedown+"-"+o,function(e){e.preventDefault(),a.$html.hasClass(n.modal)||(t.closeAllOthers(),t.close())}))};var n,s,i,a}(jQuery);
/*
 * jQuery mmenu autoHeight addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(t){var e="mmenu",s="autoHeight";t[e].addons[s]={setup:function(){if(this.opts.offCanvas){switch(this.opts.offCanvas.position){case"left":case"right":return}var n=this,o=this.opts[s];if(this.conf[s],h=t[e].glbl,"boolean"==typeof o&&o&&(o={height:"auto"}),"object"!=typeof o&&(o={}),o=this.opts[s]=t.extend(!0,{},t[e].defaults[s],o),"auto"==o.height){this.$menu.addClass(i.autoheight);var u=function(t){var e=parseInt(this.$pnls.css("top"),10)||0;_bot=parseInt(this.$pnls.css("bottom"),10)||0,this.$menu.addClass(i.measureheight),t=t||this.$pnls.children("."+i.current),t.is("."+i.vertical)&&(t=t.parents("."+i.panel).not("."+i.vertical).first()),this.$menu.height(t.outerHeight()+e+_bot).removeClass(i.measureheight)};this.bind("update",u),this.bind("openPanel",u),this.bind("closePanel",u),this.bind("open",u),h.$wndw.off(a.resize+"-autoheight").on(a.resize+"-autoheight",function(){u.call(n)})}}},add:function(){i=t[e]._c,n=t[e]._d,a=t[e]._e,i.add("autoheight measureheight"),a.add("resize")},clickAnchor:function(){}},t[e].defaults[s]={height:"default"};var i,n,a,h}(jQuery);
/*
 * jQuery mmenu backButton addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(o){var t="mmenu",n="backButton";o[t].addons[n]={setup:function(){if(this.opts.offCanvas){var i=this,e=this.opts[n];if(this.conf[n],a=o[t].glbl,"boolean"==typeof e&&(e={close:e}),"object"!=typeof e&&(e={}),e=o.extend(!0,{},o[t].defaults[n],e),e.close){var c="#"+i.$menu.attr("id");this.bind("opened",function(){location.hash!=c&&history.pushState(null,document.title,c)}),o(window).on("popstate",function(o){a.$html.hasClass(s.opened)?(o.stopPropagation(),i.close()):location.hash==c&&(o.stopPropagation(),i.open())})}}},add:function(){return window.history&&window.history.pushState?(s=o[t]._c,i=o[t]._d,void(e=o[t]._e)):void(o[t].addons[n].setup=function(){})},clickAnchor:function(){}},o[t].defaults[n]={close:!1};var s,i,e,a}(jQuery);
/*
 * jQuery mmenu counters addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(t){var n="mmenu",e="counters";t[n].addons[e]={setup:function(){var s=this,o=this.opts[e];this.conf[e],c=t[n].glbl,"boolean"==typeof o&&(o={add:o,update:o}),"object"!=typeof o&&(o={}),o=this.opts[e]=t.extend(!0,{},t[n].defaults[e],o),this.bind("init",function(n){this.__refactorClass(t("em",n),this.conf.classNames[e].counter,"counter")}),o.add&&this.bind("init",function(n){n.each(function(){var n=t(this).data(a.parent);n&&(n.children("em."+i.counter).length||n.prepend(t('<em class="'+i.counter+'" />')))})}),o.update&&this.bind("update",function(){this.$pnls.find("."+i.panel).each(function(){var n=t(this),e=n.data(a.parent);if(e){var c=e.children("em."+i.counter);c.length&&(n=n.children("."+i.listview),n.length&&c.html(s.__filterListItems(n.children()).length))}})})},add:function(){i=t[n]._c,a=t[n]._d,s=t[n]._e,i.add("counter search noresultsmsg")},clickAnchor:function(){}},t[n].defaults[e]={add:!1,update:!1},t[n].configuration.classNames[e]={counter:"Counter"};var i,a,s,c}(jQuery);
/*
 * jQuery mmenu dividers addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(i){var e="mmenu",s="dividers";i[e].addons[s]={setup:function(){var n=this,a=this.opts[s];if(this.conf[s],l=i[e].glbl,"boolean"==typeof a&&(a={add:a,fixed:a}),"object"!=typeof a&&(a={}),a=this.opts[s]=i.extend(!0,{},i[e].defaults[s],a),this.bind("init",function(){this.__refactorClass(i("li",this.$menu),this.conf.classNames[s].collapsed,"collapsed")}),a.add&&this.bind("init",function(e){switch(a.addTo){case"panels":var s=e;break;default:var s=i(a.addTo,this.$pnls).filter("."+d.panel)}i("."+d.divider,s).remove(),s.find("."+d.listview).not("."+d.vertical).each(function(){var e="";n.__filterListItems(i(this).children()).each(function(){var s=i.trim(i(this).children("a, span").text()).slice(0,1).toLowerCase();s!=e&&s.length&&(e=s,i('<li class="'+d.divider+'">'+s+"</li>").insertBefore(this))})})}),a.collapse&&this.bind("init",function(e){i("."+d.divider,e).each(function(){var e=i(this),s=e.nextUntil("."+d.divider,"."+d.collapsed);s.length&&(e.children("."+d.subopen).length||(e.wrapInner("<span />"),e.prepend('<a href="#" class="'+d.subopen+" "+d.fullsubopen+'" />')))})}),a.fixed){var o=function(e){e=e||this.$pnls.children("."+d.current);var s=e.find("."+d.divider).not("."+d.hidden);if(s.length){this.$menu.addClass(d.hasdividers);var n=e.scrollTop()||0,t="";e.is(":visible")&&e.find("."+d.divider).not("."+d.hidden).each(function(){i(this).position().top+n<n+1&&(t=i(this).text())}),this.$fixeddivider.text(t)}else this.$menu.removeClass(d.hasdividers)};this.$fixeddivider=i('<ul class="'+d.listview+" "+d.fixeddivider+'"><li class="'+d.divider+'"></li></ul>').prependTo(this.$pnls).children(),this.bind("openPanel",o),this.bind("init",function(e){e.off(t.scroll+"-dividers "+t.touchmove+"-dividers").on(t.scroll+"-dividers "+t.touchmove+"-dividers",function(){o.call(n,i(this))})})}},add:function(){d=i[e]._c,n=i[e]._d,t=i[e]._e,d.add("collapsed uncollapsed fixeddivider hasdividers"),t.add("scroll")},clickAnchor:function(i,e){if(this.opts[s].collapse&&e){var n=i.parent();if(n.is("."+d.divider)){var t=n.nextUntil("."+d.divider,"."+d.collapsed);return n.toggleClass(d.opened),t[n.hasClass(d.opened)?"addClass":"removeClass"](d.uncollapsed),!0}}return!1}},i[e].defaults[s]={add:!1,addTo:"panels",fixed:!1,collapse:!1},i[e].configuration.classNames[s]={collapsed:"Collapsed"};var d,n,t,l}(jQuery);
/*
 * jQuery mmenu dragOpen addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(e){function t(e,t,n){return t>e&&(e=t),e>n&&(e=n),e}var n="mmenu",o="dragOpen";e[n].addons[o]={setup:function(){if(this.opts.offCanvas){var i=this,a=this.opts[o],p=this.conf[o];if(r=e[n].glbl,"boolean"==typeof a&&(a={open:a}),"object"!=typeof a&&(a={}),a=this.opts[o]=e.extend(!0,{},e[n].defaults[o],a),a.open){var d,f,c,u,h,l={},m=0,g=!1,v=!1,w=0,_=0;switch(this.opts.offCanvas.position){case"left":case"right":l.events="panleft panright",l.typeLower="x",l.typeUpper="X",v="width";break;case"top":case"bottom":l.events="panup pandown",l.typeLower="y",l.typeUpper="Y",v="height"}switch(this.opts.offCanvas.position){case"right":case"bottom":l.negative=!0,u=function(e){e>=r.$wndw[v]()-a.maxStartPos&&(m=1)};break;default:l.negative=!1,u=function(e){e<=a.maxStartPos&&(m=1)}}switch(this.opts.offCanvas.position){case"left":l.open_dir="right",l.close_dir="left";break;case"right":l.open_dir="left",l.close_dir="right";break;case"top":l.open_dir="down",l.close_dir="up";break;case"bottom":l.open_dir="up",l.close_dir="down"}switch(this.opts.offCanvas.zposition){case"front":h=function(){return this.$menu};break;default:h=function(){return e("."+s.slideout)}}var b=this.__valueOrFn(a.pageNode,this.$menu,r.$page);"string"==typeof b&&(b=e(b));var y=new Hammer(b[0],a.vendors.hammer);y.on("panstart",function(e){u(e.center[l.typeLower]),r.$slideOutNodes=h(),g=l.open_dir}).on(l.events+" panend",function(e){m>0&&e.preventDefault()}).on(l.events,function(e){if(d=e["delta"+l.typeUpper],l.negative&&(d=-d),d!=w&&(g=d>=w?l.open_dir:l.close_dir),w=d,w>a.threshold&&1==m){if(r.$html.hasClass(s.opened))return;m=2,i._openSetup(),i.trigger("opening"),r.$html.addClass(s.dragging),_=t(r.$wndw[v]()*p[v].perc,p[v].min,p[v].max)}2==m&&(f=t(w,10,_)-("front"==i.opts.offCanvas.zposition?_:0),l.negative&&(f=-f),c="translate"+l.typeUpper+"("+f+"px )",r.$slideOutNodes.css({"-webkit-transform":"-webkit-"+c,transform:c}))}).on("panend",function(){2==m&&(r.$html.removeClass(s.dragging),r.$slideOutNodes.css("transform",""),i[g==l.open_dir?"_openFinish":"close"]()),m=0})}}},add:function(){return"function"!=typeof Hammer||Hammer.VERSION<2?void(e[n].addons[o].setup=function(){}):(s=e[n]._c,i=e[n]._d,a=e[n]._e,void s.add("dragging"))},clickAnchor:function(){}},e[n].defaults[o]={open:!1,maxStartPos:100,threshold:50,vendors:{hammer:{}}},e[n].configuration[o]={width:{perc:.8,min:140,max:440},height:{perc:.8,min:140,max:880}};var s,i,a,r}(jQuery);
/*
 * jQuery mmenu fixedElements addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(s){var i="mmenu",t="fixedElements";s[i].addons[t]={setup:function(){if(this.opts.offCanvas){var n=this.opts[t];this.conf[t],d=s[i].glbl,n=this.opts[t]=s.extend(!0,{},s[i].defaults[t],n);var a=function(s){var i=this.conf.classNames[t].fixed;this.__refactorClass(s.find("."+i),i,"slideout").appendTo(d.$body)};a.call(this,d.$page),this.bind("setPage",a)}},add:function(){n=s[i]._c,a=s[i]._d,e=s[i]._e,n.add("fixed")},clickAnchor:function(){}},s[i].configuration.classNames[t]={fixed:"Fixed"};var n,a,e,d}(jQuery);
/*
 * jQuery mmenu iconPanels addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(e){var n="mmenu",i="iconPanels";e[n].addons[i]={setup:function(){var a=this,l=this.opts[i];if(this.conf[i],d=e[n].glbl,"boolean"==typeof l&&(l={add:l}),"number"==typeof l&&(l={add:!0,visible:l}),"object"!=typeof l&&(l={}),l=this.opts[i]=e.extend(!0,{},e[n].defaults[i],l),l.visible++,l.add){this.$menu.addClass(s.iconpanel);for(var t=[],o=0;o<=l.visible;o++)t.push(s.iconpanel+"-"+o);t=t.join(" ");var c=function(n){var i=a.$pnls.children("."+s.panel).removeClass(t),d=i.filter("."+s.subopened);d.removeClass(s.hidden).add(n).slice(-l.visible).each(function(n){e(this).addClass(s.iconpanel+"-"+n)})};this.bind("openPanel",c),this.bind("init",function(n){c.call(a,a.$pnls.children("."+s.current)),l.hideNavbars&&n.removeClass(s.hasnavbar),n.each(function(){e(this).children("."+s.subblocker).length||e(this).prepend('<a href="#'+e(this).closest("."+s.panel).attr("id")+'" class="'+s.subblocker+'" />')})})}},add:function(){s=e[n]._c,a=e[n]._d,l=e[n]._e,s.add("iconpanel subblocker")},clickAnchor:function(){}},e[n].defaults[i]={add:!1,visible:3,hideNavbars:!1};var s,a,l,d}(jQuery);
/*
 * jQuery mmenu navbar addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(n){var a="mmenu",t="navbars";n[a].addons[t]={setup:function(){var r=this,s=this.opts[t],c=this.conf[t];if(i=n[a].glbl,"undefined"!=typeof s){s instanceof Array||(s=[s]);var d={};n.each(s,function(i){var o=s[i];"boolean"==typeof o&&o&&(o={}),"object"!=typeof o&&(o={}),"undefined"==typeof o.content&&(o.content=["prev","title"]),o.content instanceof Array||(o.content=[o.content]),o=n.extend(!0,{},r.opts.navbar,o);var l=o.position,h=o.height;"number"!=typeof h&&(h=1),h=Math.min(4,Math.max(1,h)),"bottom"!=l&&(l="top"),d[l]||(d[l]=0),d[l]++;var f=n("<div />").addClass(e.navbar+" "+e.navbar+"-"+l+" "+e.navbar+"-"+l+"-"+d[l]+" "+e.navbar+"-size-"+h);d[l]+=h-1;for(var v=0,p=o.content.length;p>v;v++){var u=n[a].addons[t][o.content[v]]||!1;u?u.call(r,f,o,c):(u=o.content[v],u instanceof n||(u=n(o.content[v])),u.each(function(){f.append(n(this))}))}var b=Math.ceil(f.children().not("."+e.btn).length/h);b>1&&f.addClass(e.navbar+"-content-"+b),f.children("."+e.btn).length&&f.addClass(e.hasbtns),f.prependTo(r.$menu)});for(var o in d)r.$menu.addClass(e.hasnavbar+"-"+o+"-"+d[o])}},add:function(){e=n[a]._c,r=n[a]._d,s=n[a]._e,e.add("close hasbtns")},clickAnchor:function(){}},n[a].configuration[t]={breadcrumbSeparator:"/"},n[a].configuration.classNames[t]={panelTitle:"Title",panelNext:"Next",panelPrev:"Prev"};var e,r,s,i}(jQuery),/*
 * jQuery mmenu navbar addon breadcrumbs content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="breadcrumbs";n[a].addons[t][e]=function(t,e,r){var s=n[a]._c,i=n[a]._d;s.add("breadcrumbs separator"),t.append('<span class="'+s.breadcrumbs+'"></span>'),this.bind("init",function(a){a.removeClass(s.hasnavbar).each(function(){for(var a=[],t=n(this),e=n('<span class="'+s.breadcrumbs+'"></span>'),c=n(this).children().first(),d=!0;c&&c.length;){c.is("."+s.panel)||(c=c.closest("."+s.panel));var o=c.children("."+s.navbar).children("."+s.title).text();a.unshift(d?"<span>"+o+"</span>":'<a href="#'+c.attr("id")+'">'+o+"</a>"),d=!1,c=c.data(i.parent)}e.append(a.join('<span class="'+s.separator+'">'+r.breadcrumbSeparator+"</span>")).appendTo(t.children("."+s.navbar))})});var c=function(){var n=this.$pnls.children("."+s.current),a=t.find("."+s.breadcrumbs),e=n.children("."+s.navbar).children("."+s.breadcrumbs);a.html(e.html())};this.bind("openPanel",c),this.bind("init",c)}}(jQuery),/*
 * jQuery mmenu navbar addon close content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="close";n[a].addons[t][e]=function(t){var e=n[a]._c,r=n[a].glbl;t.append('<a class="'+e.close+" "+e.btn+'" href="#"></a>');var s=function(n){t.find("."+e.close).attr("href","#"+n.attr("id"))};s.call(this,r.$page),this.bind("setPage",s)}}(jQuery),/*
 * jQuery mmenu navbar addon next content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="next";n[a].addons[t][e]=function(e){var r=n[a]._c;e.append('<a class="'+r.next+" "+r.btn+'" href="#"></a>');var s=function(n){n=n||this.$pnls.children("."+r.current);var a=e.find("."+r.next),s=n.find("."+this.conf.classNames[t].panelNext),i=s.attr("href"),c=s.html();a[i?"attr":"removeAttr"]("href",i),a[i||c?"removeClass":"addClass"](r.hidden),a.html(c)};this.bind("openPanel",s),this.bind("init",function(){s.call(this)})}}(jQuery),/*
 * jQuery mmenu navbar addon prev content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="prev";n[a].addons[t][e]=function(e){var r=n[a]._c;e.append('<a class="'+r.prev+" "+r.btn+'" href="#"></a>'),this.bind("init",function(n){n.removeClass(r.hasnavbar)});var s=function(){var n=this.$pnls.children("."+r.current),a=e.find("."+r.prev),s=n.find("."+this.conf.classNames[t].panelPrev);s.length||(s=n.children("."+r.navbar).children("."+r.prev));var i=s.attr("href"),c=s.html();a[i?"attr":"removeAttr"]("href",i),a[i||c?"removeClass":"addClass"](r.hidden),a.html(c)};this.bind("openPanel",s),this.bind("init",s)}}(jQuery),/*
 * jQuery mmenu navbar addon searchfield content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="searchfield";n[a].addons[t][e]=function(t){var e=n[a]._c,r=n('<div class="'+e.search+'" />').appendTo(t);"object"!=typeof this.opts.searchfield&&(this.opts.searchfield={}),this.opts.searchfield.add=!0,this.opts.searchfield.addTo=r}}(jQuery),/*
 * jQuery mmenu navbar addon title content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
    function(n){var a="mmenu",t="navbars",e="title";n[a].addons[t][e]=function(e,r){var s=n[a]._c;e.append('<a class="'+s.title+'"></a>');var i=function(n){n=n||this.$pnls.children("."+s.current);var a=e.find("."+s.title),i=n.find("."+this.conf.classNames[t].panelTitle);i.length||(i=n.children("."+s.navbar).children("."+s.title));var c=i.attr("href"),d=i.html()||r.title;a[c?"attr":"removeAttr"]("href",c),a[c||d?"removeClass":"addClass"](s.hidden),a.html(d)};this.bind("openPanel",i),this.bind("init",function(){i.call(this)})}}(jQuery);
/*
 * jQuery mmenu searchfield addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(e){function s(e){switch(e){case 9:case 16:case 17:case 18:case 37:case 38:case 39:case 40:return!0}return!1}var n="mmenu",a="searchfield";e[n].addons[a]={setup:function(){var o=this,d=this.opts[a],c=this.conf[a];r=e[n].glbl,"boolean"==typeof d&&(d={add:d}),"object"!=typeof d&&(d={}),d=this.opts[a]=e.extend(!0,{},e[n].defaults[a],d),this.bind("close",function(){this.$menu.find("."+l.search).find("input").blur()}),this.bind("init",function(n){if(d.add){switch(d.addTo){case"panels":var a=n;break;default:var a=e(d.addTo,this.$menu)}a.each(function(){var s=e(this);if(!s.is("."+l.panel)||!s.is("."+l.vertical)){if(!s.children("."+l.search).length){var n=c.form?"form":"div",a=e("<"+n+' class="'+l.search+'" />');if(c.form&&"object"==typeof c.form)for(var t in c.form)a.attr(t,c.form[t]);a.append('<input placeholder="'+d.placeholder+'" type="text" autocomplete="off" />'),s.hasClass(l.search)?s.replaceWith(a):s.prepend(a).addClass(l.hassearch)}if(d.noResults){var i=s.closest("."+l.panel).length;if(i||(s=o.$pnls.children("."+l.panel).first()),!s.children("."+l.noresultsmsg).length){var r=s.children("."+l.listview).first();e('<div class="'+l.noresultsmsg+'" />').append(d.noResults)[r.length?"insertAfter":"prependTo"](r.length?r:s)}}}}),d.search&&e("."+l.search,this.$menu).each(function(){var n=e(this),a=n.closest("."+l.panel).length;if(a)var r=n.closest("."+l.panel),c=r;else var r=e("."+l.panel,o.$menu),c=o.$menu;var h=n.children("input"),u=o.__findAddBack(r,"."+l.listview).children("li"),f=u.filter("."+l.divider),p=o.__filterListItems(u),v="> a",m=v+", > span",b=function(){var s=h.val().toLowerCase();r.scrollTop(0),p.add(f).addClass(l.hidden).find("."+l.fullsubopensearch).removeClass(l.fullsubopen).removeClass(l.fullsubopensearch),p.each(function(){var n=e(this),a=v;(d.showTextItems||d.showSubPanels&&n.find("."+l.next))&&(a=m),e(a,n).text().toLowerCase().indexOf(s)>-1&&n.add(n.prevAll("."+l.divider).first()).removeClass(l.hidden)}),d.showSubPanels&&r.each(function(){var s=e(this);o.__filterListItems(s.find("."+l.listview).children()).each(function(){var s=e(this),n=s.data(t.sub);s.removeClass(l.nosubresults),n&&n.find("."+l.listview).children().removeClass(l.hidden)})}),e(r.get().reverse()).each(function(s){var n=e(this),i=n.data(t.parent);i&&(o.__filterListItems(n.find("."+l.listview).children()).length?(i.hasClass(l.hidden)&&i.children("."+l.next).not("."+l.fullsubopen).addClass(l.fullsubopen).addClass(l.fullsubopensearch),i.removeClass(l.hidden).removeClass(l.nosubresults).prevAll("."+l.divider).first().removeClass(l.hidden)):a||(n.hasClass(l.opened)&&setTimeout(function(){o.openPanel(i.closest("."+l.panel))},1.5*(s+1)*o.conf.openingInterval),i.addClass(l.nosubresults)))}),c[p.not("."+l.hidden).length?"removeClass":"addClass"](l.noresults),this.update()};h.off(i.keyup+"-searchfield "+i.change+"-searchfield").on(i.keyup+"-searchfield",function(e){s(e.keyCode)||b.call(o)}).on(i.change+"-searchfield",function(){b.call(o)})})}})},add:function(){l=e[n]._c,t=e[n]._d,i=e[n]._e,l.add("search hassearch noresultsmsg noresults nosubresults fullsubopensearch"),i.add("change keyup")},clickAnchor:function(){}},e[n].defaults[a]={add:!1,addTo:"panels",search:!0,placeholder:"Search",noResults:"No results found.",showTextItems:!1,showSubPanels:!0},e[n].configuration[a]={form:!1};var l,t,i,r}(jQuery);
/*
 * jQuery mmenu sectionIndexer addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(e){var a="mmenu",r="sectionIndexer";e[a].addons[r]={setup:function(){var i=this,d=this.opts[r];this.conf[r],t=e[a].glbl,"boolean"==typeof d&&(d={add:d}),"object"!=typeof d&&(d={}),d=this.opts[r]=e.extend(!0,{},e[a].defaults[r],d),this.bind("init",function(a){if(d.add){switch(d.addTo){case"panels":var r=a;break;default:var r=e(d.addTo,this.$menu).filter("."+n.panel)}r.find("."+n.divider).closest("."+n.panel).addClass(n.hasindexer)}if(!this.$indexer&&this.$pnls.children("."+n.hasindexer).length){this.$indexer=e('<div class="'+n.indexer+'" />').prependTo(this.$pnls).append('<a href="#a">a</a><a href="#b">b</a><a href="#c">c</a><a href="#d">d</a><a href="#e">e</a><a href="#f">f</a><a href="#g">g</a><a href="#h">h</a><a href="#i">i</a><a href="#j">j</a><a href="#k">k</a><a href="#l">l</a><a href="#m">m</a><a href="#n">n</a><a href="#o">o</a><a href="#p">p</a><a href="#q">q</a><a href="#r">r</a><a href="#s">s</a><a href="#t">t</a><a href="#u">u</a><a href="#v">v</a><a href="#w">w</a><a href="#x">x</a><a href="#y">y</a><a href="#z">z</a>'),this.$indexer.children().on(s.mouseover+"-sectionindexer "+n.touchstart+"-sectionindexer",function(){var a=e(this).attr("href").slice(1),r=i.$pnls.children("."+n.current),s=r.find("."+n.listview),t=!1,d=r.scrollTop(),h=s.position().top+parseInt(s.css("margin-top"),10)+parseInt(s.css("padding-top"),10)+d;r.scrollTop(0),s.children("."+n.divider).not("."+n.hidden).each(function(){t===!1&&a==e(this).text().slice(0,1).toLowerCase()&&(t=e(this).position().top+h)}),r.scrollTop(t!==!1?t:d)});var t=function(e){i.$menu[(e.hasClass(n.hasindexer)?"add":"remove")+"Class"](n.hasindexer)};this.bind("openPanel",t),t.call(this,this.$pnls.children("."+n.current))}})},add:function(){n=e[a]._c,i=e[a]._d,s=e[a]._e,n.add("indexer hasindexer"),s.add("mouseover touchstart")},clickAnchor:function(e){return e.parent().is("."+n.indexer)?!0:void 0}},e[a].defaults[r]={add:!1,addTo:"panels"};var n,i,s,t}(jQuery);
/*
 * jQuery mmenu toggles addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
!function(t){var e="mmenu",c="toggles";t[e].addons[c]={setup:function(){var n=this;this.opts[c],this.conf[c],l=t[e].glbl,this.bind("init",function(e){this.__refactorClass(t("input",e),this.conf.classNames[c].toggle,"toggle"),this.__refactorClass(t("input",e),this.conf.classNames[c].check,"check"),t("input."+s.toggle+", input."+s.check,e).each(function(){var e=t(this),c=e.closest("li"),i=e.hasClass(s.toggle)?"toggle":"check",l=e.attr("id")||n.__getUniqueId();c.children('label[for="'+l+'"]').length||(e.attr("id",l),c.prepend(e),t('<label for="'+l+'" class="'+s[i]+'"></label>').insertBefore(c.children("a, span").last()))})})},add:function(){s=t[e]._c,n=t[e]._d,i=t[e]._e,s.add("toggle check")},clickAnchor:function(){}},t[e].configuration.classNames[c]={toggle:"Toggle",check:"Check"};var s,n,i,l}(jQuery);/*!
 * jQuery Browser Plugin 0.1.0
 * https://github.com/gabceb/jquery-browser-plugin
 *
 * Original jquery-browser code Copyright 2005, 2015 jQuery Foundation, Inc. and other contributors
 * http://jquery.org/license
 *
 * Modifications Copyright 2015 Gabriel Cebrian
 * https://github.com/gabceb
 *
 * Released under the MIT license
 *
 * Date: 05-07-2015
 */
/*global window: false */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], function ($) {
            return factory($);
        });
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        // Node-like environment
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function(jQuery) {
    "use strict";

    function uaMatch( ua ) {
        // If an UA is not provided, default to the current browser UA.
        if ( ua === undefined ) {
            ua = window.navigator.userAgent;
        }
        ua = ua.toLowerCase();

        var match = /(edge)\/([\w.]+)/.exec( ua ) ||
            /(opr)[\/]([\w.]+)/.exec( ua ) ||
            /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(iemobile)[\/]([\w.]+)/.exec( ua ) ||
            /(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        var platform_match = /(ipad)/.exec( ua ) ||
            /(ipod)/.exec( ua ) ||
            /(windows phone)/.exec( ua ) ||
            /(iphone)/.exec( ua ) ||
            /(kindle)/.exec( ua ) ||
            /(silk)/.exec( ua ) ||
            /(android)/.exec( ua ) ||
            /(win)/.exec( ua ) ||
            /(mac)/.exec( ua ) ||
            /(linux)/.exec( ua ) ||
            /(cros)/.exec( ua ) ||
            /(playbook)/.exec( ua ) ||
            /(bb)/.exec( ua ) ||
            /(blackberry)/.exec( ua ) ||
            [];

        var browser = {},
            matched = {
                browser: match[ 5 ] || match[ 3 ] || match[ 1 ] || "",
                version: match[ 2 ] || match[ 4 ] || "0",
                versionNumber: match[ 4 ] || match[ 2 ] || "0",
                platform: platform_match[ 0 ] || ""
            };

        if ( matched.browser ) {
            browser[ matched.browser ] = true;
            browser.version = matched.version;
            browser.versionNumber = parseInt(matched.versionNumber, 10);
        }

        if ( matched.platform ) {
            browser[ matched.platform ] = true;
        }

        // These are all considered mobile platforms, meaning they run a mobile browser
        if ( browser.android || browser.bb || browser.blackberry || browser.ipad || browser.iphone ||
            browser.ipod || browser.kindle || browser.playbook || browser.silk || browser[ "windows phone" ]) {
            browser.mobile = true;
        }

        // These are all considered desktop platforms, meaning they run a desktop browser
        if ( browser.cros || browser.mac || browser.linux || browser.win ) {
            browser.desktop = true;
        }

        // Chrome, Opera 15+ and Safari are webkit based browsers
        if ( browser.chrome || browser.opr || browser.safari ) {
            browser.webkit = true;
        }

        // IE11 has a new token so we will assign it msie to avoid breaking changes
        if ( browser.rv || browser.iemobile) {
            var ie = "msie";

            matched.browser = ie;
            browser[ie] = true;
        }

        // Edge is officially known as Microsoft Edge, so rewrite the key to match
        if ( browser.edge ) {
            delete browser.edge;
            var msedge = "msedge";

            matched.browser = msedge;
            browser[msedge] = true;
        }

        // Blackberry browsers are marked as Safari on BlackBerry
        if ( browser.safari && browser.blackberry ) {
            var blackberry = "blackberry";

            matched.browser = blackberry;
            browser[blackberry] = true;
        }

        // Playbook browsers are marked as Safari on Playbook
        if ( browser.safari && browser.playbook ) {
            var playbook = "playbook";

            matched.browser = playbook;
            browser[playbook] = true;
        }

        // BB10 is a newer OS version of BlackBerry
        if ( browser.bb ) {
            var bb = "blackberry";

            matched.browser = bb;
            browser[bb] = true;
        }

        // Opera 15+ are identified as opr
        if ( browser.opr ) {
            var opera = "opera";

            matched.browser = opera;
            browser[opera] = true;
        }

        // Stock Android browsers are marked as Safari on Android.
        if ( browser.safari && browser.android ) {
            var android = "android";

            matched.browser = android;
            browser[android] = true;
        }

        // Kindle browsers are marked as Safari on Kindle
        if ( browser.safari && browser.kindle ) {
            var kindle = "kindle";

            matched.browser = kindle;
            browser[kindle] = true;
        }

        // Kindle Silk browsers are marked as Safari on Kindle
        if ( browser.safari && browser.silk ) {
            var silk = "silk";

            matched.browser = silk;
            browser[silk] = true;
        }

        // Assign the name and platform variable
        browser.name = matched.browser;
        browser.platform = matched.platform;
        return browser;
    }

    // Run the matching process, also assign the function to the returned object
    // for manual, jQuery-free use if desired
    window.jQBrowser = uaMatch( window.navigator.userAgent );
    window.jQBrowser.uaMatch = uaMatch;

    // Only assign to jQuery.browser if jQuery is loaded
    if ( jQuery ) {
        jQuery.browser = window.jQBrowser;
    }

    return window.jQBrowser;
}));(function ($) {
    /*-------------------------------------------
     Glossary-JS version 3.2
     Michigan State University
     Virtual University Design and Technology
     Creator:  Nathan Lounds
     Project Page: http://code.google.com/p/glossary-js/
     Dependencies:  jquery.js (http://jquery.com)
     auto-highlighting requires jquery.highlight.js (http://bartaz.github.com/sandbox.js/jquery.highlight.html)
     glossary.css
     Copyright (c) 2011 Michigan State University Board of Trustees
     License: http://www.opensource.org/licenses/mit-license.php
     --------------------------------------------*/

    GlossaryJS_enabled = window.GlossaryJS_enabled || false;
    GlossaryJS_txt = window.GlossaryJS_txt || "glossary.txt";
    GlossaryJS_section = window.GlossaryJS_section || "";
    GlossaryJS_autohighlight = window.GlossaryJS_autohighlight || false;
    GlossaryJS_toc = window.GlossaryJS_toc || true;
    GlossaryJS_selector =window.GlossaryJS_selector || document.body;
    GlossaryJS_toc_array = window.GlossaryJS_toc_array || "A.B.C.D.E.F.G.H.I.J.K.L.M.N.O.P.Q.R.S.T.U.V.W.X.Y.Z".split(".");
    var GlossaryJS = {
        glossary: new Array(),
        initialize : function() {
            $(".glossary").each(function(i){
                var cur = this;
                cur.className = "highlightSpan";
            });

            $(".highlightSpan")
                .on("mouseover", function() {
                    GlossaryJS.word = this;
                    clearTimeout(GlossaryJS.timer);
                    GlossaryJS.getDefinition();
                })
                .on("mouseout", function() {
                    var the_div = document.getElementById("glossaryTooltip");
                    if(the_div) {
                        GlossaryJS.timer = setTimeout("jQuery('#glossaryTooltip').remove()",1000);
                    }
                })
                .on("click", function() {
                    var me = this;
                    me.blur();
                    $("#glossaryTooltip").attr("title","click to close");
                    GlossaryJS.word = me;
                    GlossaryJS.getDefinition();
                });

            GlossaryJS.loadGlossary();
        },
        loadGlossary: function() {
            $.ajax({
                data: "",
                dataType: 'html',
                url: GlossaryJS_txt,
                success: function(str){
                    // Create the array of glossary terms
                    var the_words = str.split("\n"), counter = 0, inarray, the_word_arr, highlight_arr = [];
                    the_words.sort(function(x,y) {
                        if(x.toLowerCase() !== y.toLowerCase()) {
                            x = x.toLowerCase();
                            y = y.toLowerCase();
                        }
                        return x > y ? 1 : (x < y ? -1 : 0);
                    });
                    for (i = 0; i < the_words.length; i++) {
                        inarray = false;
                        the_word_arr = the_words[i].split("|");
                        if(the_word_arr.length<3 || (the_word_arr.length==3&&GlossaryJS_section==the_word_arr[1])) {
                            if(the_word_arr) {
                                if(the_word_arr[0].length>1 && the_word_arr.length>1) {
                                    for(k = 0; k < GlossaryJS.glossary.length; k++) {
                                        if(the_word_arr[0]===GlossaryJS.glossary[k].word) {
                                            inarray = true;
                                            k = GlossaryJS.glossary.length;
                                        }
                                    }
                                    if(inarray===false) {
                                        GlossaryJS.glossary[counter] = {};
                                        GlossaryJS.glossary[counter].word = the_word_arr[0];
                                        if(GlossaryJS_autohighlight===true) {
                                            highlight_arr.push(GlossaryJS.glossary[counter].word);
                                        }
                                        GlossaryJS.glossary[counter].def = "";
                                        GlossaryJS.glossary[counter].section = "";
                                        if(the_word_arr.length===3) {
                                            GlossaryJS.glossary[counter].section = the_word_arr[1];
                                            GlossaryJS.glossary[counter].def = the_word_arr[2];
                                        } else if (the_word_arr.length===2) {
                                            GlossaryJS.glossary[counter].def = the_word_arr[1];
                                        }
                                        counter++;
                                    }
                                } else {
                                    //console.log(the_word_arr);  uncomment this line to find lines without def's
                                }
                            }
                        }
                    }
                    if(GlossaryJS_autohighlight===true && highlight_arr.length>0) {
                        //$("#section_instruct_proc").highlight(highlight_arr, { wordsOnly: true, className: 'highlightSpan' });
                        $(GlossaryJS_selector).highlight(highlight_arr, { wordsOnly: true, className: 'highlightSpan' });
                    }

                    // Make the glossary unordered list (if there's a div for it)
                    var str_output = "";
                    var GlossaryUL = document.getElementById("GlossaryJS");
                    var k = 0;
                    var tmp_output = "";
                    var tmp_first_letter = "";
                    var prev_first_letter = "";
                    var jump_bar_arr = new Array();
                    var jump_bar = "";
                    GlossaryJS_toc_array[0].toUpperCase();
                    if(GlossaryUL) {
                        for (i=0; i <= GlossaryJS.glossary.length-1; i++) {
                            //if(GlossaryJS.glossary[i]) {
                            tmp_output = "<dt>"+GlossaryJS.glossary[i].word + "</dt>\n<dd>"+GlossaryJS.glossary[i].def+"</dd>\n";
                            if(GlossaryJS_toc===true) {
                                tmp_first_letter = GlossaryJS.glossary[i].word.substring(0,1).toUpperCase();
                                if(prev_first_letter!=tmp_first_letter) {
                                    // new letter
                                    while(k<=GlossaryJS_toc_array.length-1) {
                                        if(GlossaryJS_toc_array[k]===tmp_first_letter) {
                                            tmp_output = "<a name='glossary_letter_" + GlossaryJS_toc_array[k] + "' class='anchor'></a><h3 class='toc_letter'>" + GlossaryJS_toc_array[k] + "</h3>" + tmp_output;
                                            jump_bar_arr.push(new Array(GlossaryJS_toc_array[k], true));
                                            k++;
                                            break;
                                        } else {
                                            jump_bar_arr.push(new Array(GlossaryJS_toc_array[k], false));
                                            k++;
                                        }
                                    }
                                }
                                prev_first_letter = tmp_first_letter;
                            }
                            str_output += tmp_output;
                            //} else {
                            //	console.log(GlossaryJS.glossary[i-1]);
                            //}
                        }
                        while(k<=GlossaryJS_toc_array.length-1) {
                            jump_bar_arr.push(new Array(GlossaryJS_toc_array[k], false));
                            k++;
                        }
                        for ( var i=0, len=jump_bar_arr.length; i<len; ++i ) {
                            if(jump_bar_arr[i][1]===true) {
                                jump_bar += "<a href='#glossary_letter_"+jump_bar_arr[i][0]+"'>" + jump_bar_arr[i][0] + "</a>";
                            } else {
                                jump_bar += "<span>" + jump_bar_arr[i][0] + "</span>";
                            }
                        }
                        $("#GlossaryJS").html("<dl>"+str_output+"</dl>");
                        if(GlossaryJS_toc===true) {
                            jump_bar = "<div class='jump_bar'>" + jump_bar + "</div>";
                            //$("#GlossaryJS").before(jump_bar).after(jump_bar);
                            $("#GlossaryJS a.anchor").before(jump_bar);
                        }
                    }
                }
            });
        },
        getDefinition : function() {
            for (i = 0; i < GlossaryJS.glossary.length; i++) {
                var the_term = GlossaryJS.word.childNodes[0].nodeValue;
                if(jQuery.trim(GlossaryJS.glossary[i].word.toUpperCase())==jQuery.trim(the_term.toUpperCase())) {
                    GlossaryJS.createGlossaryTooltip(GlossaryJS.word,GlossaryJS.glossary[i]);
                    return true;
                }
            }
            var tmp_obj = {};
            tmp_obj.def = "<span style='color:red;font-weight:bold'>Error:</span> not in glossary";
            tmp_obj.word = GlossaryJS.word;
            GlossaryJS.createGlossaryTooltip(GlossaryJS.word,tmp_obj);
        },
        createGlossaryTooltip : function(word_div,glossary_element) {
            $("#glossaryTooltip").remove();
            var position = $(word_div).position();
            var the_left;
            $(word_div).after('<div id="glossaryTooltip"><div class="g_shadow"><div class="g_content">'+glossary_element.def+'</div></div></div>');
            if(position.left > ($(document).width()*0.5)) {  // put tooltip to left of word
                the_left = (position.left-$("#glossaryTooltip").innerWidth()) + "px";
            } else {  // put tooltip to right of word
                the_left = (position.left+$(word_div).width()) + "px";
            }
            $("#glossaryTooltip")
                .css("left",the_left)
                .css("top",(position.top+$(word_div).height())+"px")
                .click(function() {
                    $(this).remove();
                })
                .mouseover(function() {
                    clearTimeout(GlossaryJS.timer);
                })
                .mouseout(function() {
                    GlossaryJS.timer = setTimeout("jQuery('#glossaryTooltip').remove()",1000);
                }).focus();
        }
    }

    $(document).ready(function() {
        if (GlossaryJS_enabled) {
            GlossaryJS.initialize();
        }
    });

}(Nx.$));

(function($) {
    /**
     * Privacypolicy module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Privacypolicy
     * @extends Nx.Module
     */
    Nx.Module.Privacypolicy = Nx.Module.extend({

        /**
         * Initializes the Privacypolicy module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },


        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }
    });
})(Nx.$);
(function($) {
    // @mgiger
    Nx.Module.Bildergalerie = Nx.Module.extend({

        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        dependencies: function() {
            this.require('jquery.slimbox2.js', 'plugin', 'beforeBinding');
        },

        onBinding: function() {
            Nx.current_page = 0;
            Nx.current_image_element = 0;
            Nx.images_on_page = 8;

            Nx.$(this.$ctx).each(function(index) {
                if (!Nx.$(this).find('.bd').attr('id') || Nx.$(this).find('.bd').attr('id') == '') {
                    var id = Math.floor(Math.random()*10001);
                    Nx.$(this).find('.bd').attr('id',id);
                };
            });

            Nx.showpage = function(page,id) {

                var start = page * Nx.images_on_page;
                var i = 0;
                Nx.$('#'+id+' .gallery li').each(function() {
                    if (i < start) {
                        Nx.$(this).hide();
                    }
                    if (i >= start && i < (start + Nx.images_on_page)) {
                        Nx.$(this).show();
                    }
                    if (i >= (start + Nx.images_on_page)) {
                        Nx.$(this).hide();
                    }
                    i++;
                });

                //mark current page in pager active
                Nx.$('#'+id+' .paging a').attr('class', '');
                Nx.$('#'+id+' .paging a[href*=' + (page + 1) + ']').attr('class', 'active');
                Nx.current_page = page;
                Nx.current_image_element = page * Nx.images_on_page;
            };

            Nx.populate_pager = function(id) {

                var o = [];

                o[o.length] = '<a href="#page-prev"><img src="/img/slimbox/pagingback.gif" width="10" height="10" alt="" /></a>';

                for (var i = 0; i < Nx.pages_total; i++) {
                    o[o.length] = '<a href="#page-' + (i + 1) + '">' + (i + 1) + '</a>';
                }

                o[o.length] = '<a href="#page-next"><img src="/img/slimbox/pagingnext.gif" width="10" height="10" alt="" /></a>';

                Nx.$('#'+id+' .paging').html(o.join(''));

            };


            Nx.id = Nx.$(this.$ctx).find('.bd').attr('id');

            Nx.images_total = Nx.$('#'+Nx.id+' .gallery li').size();
            Nx.pages_total = Math.ceil(Nx.images_total / Nx.images_on_page);

            Nx.populate_pager(Nx.id);
            Nx.showpage(Nx.current_page,Nx.id);

            Nx.filenames = [];
            Nx.$('#'+Nx.id+' .gallery li a').each(function() {
                Nx.filenames[Nx.filenames.length] = Nx.$(this).attr('href');
            });

            Nx.$('#'+Nx.id+' .paging a').click(function(ev) {
                Nx.id = Nx.$(this).parent().parent('.bd').attr('id');

                var slices = (Nx.$(this).attr('href').split('-')).length;
                var page = Nx.$(this).attr('href').split('-')[slices-1];
                Nx.current_page = Nx.$(this).parent().find('.active').attr('href').split('-')[slices-1];
                Nx.current_page = Nx.current_page - 1;

                Nx.images_total = Nx.$('#'+Nx.id+' .gallery li').size();
                Nx.pages_total = Math.ceil(Nx.images_total / Nx.images_on_page);

                if (page == 'prev') {
                    if (Nx.current_page < 1) {
                        page = 0;
                    } else {
                        page = Nx.current_page - 1;
                    }
                    Nx.showpage(page,Nx.id);
                } else if (page == 'next') {

                    if (Nx.current_page >= Nx.pages_total - 2) {
                        page = Nx.pages_total - 1;
                    } else {
                        page = Nx.current_page + 1;
                    }
                    Nx.showpage(page,Nx.id);
                } else {
                    page = page - 1; //pages in navigation begin at 1
                    Nx.showpage(page,Nx.id);
                }

            });

            //slimbox modifications
            Nx.$('#lbNextLink, #lbPrevLink').click(function() {
                var filename = Nx.$('#lbImage').attr('style').split(');')[0].split('url(')[1];
                if (filename.match(/\//)) {
                    filename = filename.split('').reverse().join('').split('/')[0].split('').reverse().join('');
                }
                var re = new RegExp(filename, 'g');

                for (var i = 0; i < Nx.filenames.length; i++) {

                    if (re.exec(Nx.filenames[i])) {
                        if (Nx.$(this).attr('id').match(/next/gi)) {
                            Nx.current_image_element = i + 1;
                        } else {
                            Nx.current_image_element = i - 1;
                        }
                    }
                }
            });

            Nx.$('#lbOverlay').click(function() {

                var page = Math.floor(Nx.current_image_element / Nx.images_total * Nx.pages_total);
                Nx.showpage(page);

            });

            //AUTOLOAD CODE BLOCK (MAY BE CHANGED OR REMOVED)
            var self = this;
            Nx.$(function($) {
                Nx.$(self.$ctx).find("a[rel^='lightbox']").slimbox({
                    counterText: $(self.$ctx).find('.gallerylabel').text(),
                    closeKeys: [27, 70],
                    nextKeys: [39, 83]
                }, null, function(el) {
                    return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
                });
            });

            $('#lbCloseLink').appendTo('#lbImage');

        },

    });
})(Nx.$);(function($) {
    /**
     * SchindlerCity module implementation.
     *
     * @author Namics\samschmid
     * @namespace Nx.Module
     * @class SchindlerCity
     * @extends Nx.Module
     */
    Nx.Module.SchindlerCity = Nx.Module.extend({

        videoplayer: {},
        settings: {},
        mediaspace: {},
        detailpage:{},
        nextpage: {}, //Carouselpage
        nextpageready: false,
        nextcontentpage: {}, //Contentbereich unten
        nextcontentpageready: false,
        previouspage: {},
        previouspageready: false,
        previouscontentpage: {},
        previouscontentpageready: false,
        eventqueue: null,
        fn: {},
        state: 'new', //init, ready, overview, zooming-in, detailview, zooming-out,
        setState: function(state) {
            this.state = state;
        },
        getState: function() {
            return this.state;
        },
        setPreviousPageReady: function() {
            this.previouspageready = true;
            this.callWaitingEvent();
        },
        setPreviousContentPageReady: function() {
            this.previouscontentpageready = true;
            this.callWaitingEvent();
        },
        setNextPageReady: function() {
            this.nextpageready = true;
            this.callWaitingEvent();
        },
        setNextContentPageReady: function() {
            this.nextcontentpageready = true;
            this.callWaitingEvent();
        },
        callWaitingEvent: function() {
            if(this.eventqueue !== null) {
                this.fn = this.eventqueue;
                this.eventqueue = null;
                this.fn();
            }

        },
        /**
         * Initializes the InteractiveVideo module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {

            this._super($ctx, sandbox, modId);
            this.setState("init");
            this._defineMediaspaceForVideoPlayer($ctx, modId);
            this._initSettings();
            this._choosePlayer();
            this.setState('ready');


        },

        _defineMediaspaceForVideoPlayer: function($ctx, modId) {
            this.mediaspace = $('.mediaspace', $ctx);
            this.mediaspace.attr('id', 'mediaspace' + modId);

        },

        _initSettings: function() {
            this.settings = {
                overview:  this.mediaspace.data('overview'),
                flashplayer:  this.mediaspace.data('flashplayer'),
                videos:  $('.bd', this.$ctx).data('videos')
            }
        },

        _choosePlayer: function(){
            if(this._isIPad()) {
                this.videoplayer = this.ipad;
            } else {
                this.videoplayer = this.jw;
            }
        },

        _isIPad : function() {
            return this._browserCheck(/iPad/i);
        },
        _isIPhone : function() {
            return this._browserCheck(/iPhone/i);
        },
        _isAndroid : function() {
            return this._browserCheck(/Android/i);
        },

        _browserCheck: function(regex) {
            return  navigator.userAgent.match(regex) != null;

        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
            if(!this._isIPad()) {
                this.require('jwplayer.js', 'library', 'beforeBinding');
            }
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            this.removeTitleAttributeFromContent();

            if($('.bd', this.$ctx).data('author') === true ) {
                this.showOverview();
            } else {
                if(this._isIPhone() || this._isAndroid()) {

                    if(window.location.hash !== "") {
                        var video = this.settings.videos[window.location.hash.substr(1)];
                        if(video !== undefined) {
                            this.loadDetailPageAndReplaceContentIPhone(video.carouselpart);
                            this.loadContentPageAndReplaceContent(video.contentpart);
                        }
                    } else {

                        this.showOverview();
                    }
                } else {
                    this.initOverview();
                    this.videoplayer.init(this.$ctx, this.modId, this);

                    //Check if #TOWER
                    if(window.location.hash !== "") {
                        var video = this.settings.videos[window.location.hash.substr(1)];
                        if(video !== undefined) {
                            this.loadDetailPageAndReplaceContent(video.carouselpart);
                            this.loadContentPageAndReplaceContent(video.contentpart);
                        }
                    } else {

                        this.showOverview();
                    }
                }
            }


            callback();
        },

        removeTitleAttributeFromContent: function() {
            $(this.$ctx).find('img').each(function() {
                var titleAttr = $(this).attr('title');
                if (typeof titleAttr !== 'undefined' && titleAttr !== false) {
                    $(this).removeAttr('title');
                }
            });
        },

        loadDetailPageAndReplaceContentIPhone: function(url) {
            var detailpage = url;
            var self = this;
            $.ajax({
                url: detailpage,
                success:  function(response,status,jqXHR) {

                    self.detailpage = $('.bd > *',response);
                    var layer = $('.overlay-zoomedin', this.$ctx)[0];
                    self.setState('detailview');

                    var layer = $('.overlay-zoomedin', this.$ctx)[0];
                    self._removeChildNodes(layer);
                    $('.overlay-zoomedin', this.$ctx).append(self.detailpage);

                    $('.overlay-zoomedin', this.$ctx).show();
                    $('.overlay-overview', this.$ctx).hide();
                }
            });
        },
        loadDetailPageAndReplaceContent: function(url) {
            var detailpage = url;
            var self = this;
            $.ajax({
                url: detailpage,
                success:  function(response,status,jqXHR) {
                    self.detailpage = $('.bd > *',response);
                    var layer = $('.overlay-zoomedin', this.$ctx)[0];
                    self.setState('detailview');

                    var layer = $('.overlay-zoomedin', this.$ctx)[0];
                    self._removeChildNodes(layer);
                    $('.overlay-zoomedin', this.$ctx).append(self.detailpage);
                    self.removeTitleAttributeFromContent();
                    self.bindDetailPageEventsAndLoadNextAndPrevious();
                    $('.overlay-zoomedin', this.$ctx).show();
                    $('.overlay-overview', this.$ctx).hide();
                }
            });
        },
        loadContentPageAndReplaceContent: function(url) {
            var contentpage = url;
            var self = this;
            $.ajax({
                url: contentpage,
                success:  function(response,status,jqXHR) {
                    self.contentpage = response;
                    self.replaceContentWithContentpage();
                }
            });

        },

        initOverview: function() {
            this.bindMapEvents();
        },
        bindMapEvents: function() {
            var self = this;
            $('.overlay-overview area', this.$ctx).bind('click', function(event) {
                event.preventDefault();
                var url = this.href;
                var filename = url.match(/([^\/]+)(?=\.\w+$)/)[0];

                self.zoomIn(self.settings.videos[filename],filename);
            });
        },
        showOverview: function(video) {
            $('.overlay-overview', this.$ctx).show();
            if(this.overviewcontent !== undefined) {
                this.replaceContentOverview();
            }
            this.setState('overview');
        },

        zoomIn: function(video,id) {
            window.location.hash = "#"+id;
            this.videoplayer.zoomIn(video);
            this.setState('zooming-in');
            this.loadDetailPage(video.carouselpart);
            this.loadContentPage(video.contentpart);
        },

        loadDetailPage: function(url) {
            var detailpage = url;
            var self = this;
            $.ajax({
                url: detailpage,
                success:  function(response,status,jqXHR) {
                    self.detailpage = $('.bd > *',response);
                }
            });

        },

        loadContentPage: function(url) {
            var contentpage = url;
            var self = this;
            $.ajax({
                url: contentpage,
                success:  function(response,status,jqXHR) {
                    self.contentpage = response;
                }
            });

        },


        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },


        zoomOut: function(event) {
            event.preventDefault();
            var video = event.data.player.settings.videos[window.location.hash.substr(1)];

            window.location.hash = "";

            event.data.player.videoplayer.zoomOut(video);
            event.data.player.setState('zooming-out');
        },

        hideOverview: function() {
            $('.overlay-overview', this.$ctx).hide();
        },

        showDetail: function(video) {
            this.setState('detailview');

            var layer = $('.overlay-zoomedin', this.$ctx)[0];
            this._removeChildNodes(layer);
            $('.overlay-zoomedin', this.$ctx).append(this.detailpage);
            this.removeTitleAttributeFromContent();
            this.bindDetailPageEventsAndLoadNextAndPrevious();
            $('.overlay-zoomedin', this.$ctx).show();

            this.replaceContentWithContentpage();
        },

        bindDetailPageEventsAndLoadNextAndPrevious: function() {
            this.bindGoToOverview();
            this.bindGoToPrevious();
            this.bindGoToNext();

            this.loadPreviousPage();
            this.loadPreviousContentPage();
            this.loadNextPage();
            this.loadNextContentPage();
        },

        bindGoToOverview: function() {
            var self = this;
            $('.overlay-zoomedin .slider .overview > p > a', this.$ctx).bind('click', {player: self, video: self.settings.videos[0]}, self.zoomOut);
        },

        bindGoToPrevious: function() {
            var self = this;
            $('.overlay-zoomedin .slider .prev > p > a', this.$ctx).bind('click', function(event) {
                event.preventDefault();
                var url = this.href;
                var id =  url.match(/([^\/]+)(?=\.\w+$)/)[0];
                window.location.hash = "#"+id;
                self.goToPrevious();
            });
        },

        goToPrevious: function() {
            //IF BOTH ARE Ready
            if(this.checkPreviousPageReady()) {
                this.appendPreviousPageAndContent();
            } else {
                //Append to queue
                this.eventqueue =  this.goToPrevious;
            }
        },

        checkPreviousPageReady: function() {
            return (this.previouscontentpageready === true && this.previouspageready === true);
        },
        appendPreviousPageAndContent: function() {
            var layer = $('.overlay-zoomedin', this.$ctx)[0];
            this._removeChildNodes(layer);
            $('.overlay-zoomedin', this.$ctx).append(this.previouspage);
            this.removeTitleAttributeFromContent();
            this.bindDetailPageEventsAndLoadNextAndPrevious();
            this.replaceContentWithPrevious();
        },

        loadPreviousPage: function() {
            var self=this;
            var url = $('.overlay-zoomedin .slider .prev > p > a', this.$ctx).attr('href');
            if(url !== undefined) {
                var filename = url.match(/([^\/]+)(?=\.\w+$)/)[0];
                var page = this.settings.videos[filename].carouselpart;
                self.previouspageready = false;
                $.ajax({
                    url: page,
                    success:  function(response,status,jqXHR) {
                        self.previouspage = response;
                        self.setPreviousPageReady();
                    }
                });
            }
        },
        loadPreviousContentPage: function() {
            var self=this;
            var url = $('.overlay-zoomedin .slider .prev > p > a', this.$ctx).attr('href');
            if(url !== undefined) {
                var filename = url.match(/([^\/]+)(?=\.\w+$)/)[0];
                var page = this.settings.videos[filename].contentpart;
                self.previouscontentpageready = false;
                $.ajax({
                    url: page,
                    success:  function(response,status,jqXHR) {
                        self.previouscontentpage = response;
                        self.setPreviousContentPageReady();
                    }
                });
            }
        },

        bindGoToNext: function() {
            var self = this;
            $('.overlay-zoomedin .slider .next > p > a', this.$ctx).bind('click', function(event) {
                event.preventDefault();
                var url = this.href;
                var id =  url.match(/([^\/]+)(?=\.\w+$)/)[0];
                window.location.hash = "#"+id;
                self.goToNext();
            });
        },

        goToNext: function() {
            if(this.checkNextPageReady()) {
                this.appendNextPageAndContent();
            } else {
                //Append to queue
                this.eventqueue = this.goToNext;
            }

        },

        checkNextPageReady: function() {
            return (this.nextcontentpageready === true && this.nextpageready === true);
        },
        appendNextPageAndContent: function() {
            var layer = $('.overlay-zoomedin', this.$ctx)[0];
            this._removeChildNodes(layer);
            $('.overlay-zoomedin', this.$ctx).append(this.nextpage);
            this.removeTitleAttributeFromContent();
            this.bindDetailPageEventsAndLoadNextAndPrevious();
            this.replaceContentWithNext();
        },

        loadNextPage: function() {
            var self=this;
            var url = $('.overlay-zoomedin .slider .next > p > a', this.$ctx).attr('href');
            if(url !== undefined) {
                var filename = url.match(/([^\/]+)(?=\.\w+$)/)[0];
                var page = this.settings.videos[filename].carouselpart;
                self.nextpageready = false;
                $.ajax({
                    url: page,
                    success:  function(response,status,jqXHR) {

                        self.nextpage = response;
                        self.setNextPageReady();
                    }
                });
            }
        },
        loadNextContentPage: function() {
            var self=this;
            var url = $('.overlay-zoomedin .slider .next > p > a', this.$ctx).attr('href');
            if(url !== undefined) {
                var filename = url.match(/([^\/]+)(?=\.\w+$)/)[0];
                var page = this.settings.videos[filename].contentpart;
                self.nextcontentpageready = false;
                $.ajax({
                    url: page,
                    success:  function(response,status,jqXHR) {
                        self.nextcontentpage = response;
                        self.setNextContentPageReady();
                    }
                });
            }
        },
        replaceContentWithContentpage: function() {
            this.overviewcontent = $('.schindlerCityContent');
            $('.schindlerCityContent').replaceWith(this.contentpage);
            this.registerModulesOnResponse('.schindlerCityContent');
        },
        replaceContentWithPrevious: function() {
            $('.schindlerCityContent').replaceWith(this.previouscontentpage);
            this.registerModulesOnResponse('.schindlerCityContent');
        },
        replaceContentWithNext: function() {
            $('.schindlerCityContent').replaceWith(this.nextcontentpage);
            this.registerModulesOnResponse('.schindlerCityContent');
        },

        replaceContentOverview: function() {
            $('.schindlerCityContent').replaceWith(this.overviewcontent);
            this.registerModulesOnResponse('.schindlerCityContent');
        },

        hideDetail: function() {
            $('.overlay-zoomedin', this.$ctx).hide();
        },

        registerModulesOnResponse: function(selector) {
            var application = new Nx.Application($(selector));
            application.registerModules();
            application.start();
        },

        _removeChildNodes: function(node) {
            while (node.hasChildNodes()) {
                node.removeChild(node.lastChild);
            };
        },

        getFirstVideo: function() {
            var videos = this.settings.videos;
            for(prop in videos) {
                for(p in videos[prop]) {
                    if(p === "zoomIn") {
                        return videos[prop][p];
                    }

                }

            }

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        },

        /** jwplayer */
        jw: {
            ctx: {},
            modId: '',
            modul: {},
            currentVideo: {},
            zoomout: false,
            isTour: false,
            isPlaying: false,
            loaderTimer: {},
            laoderTimerStarted: false,
            init: function(ctx, modId, modul) {
                this.$ctx = ctx;
                this.modId = modId;
                this.modul = modul;

                this.initJWPlayer('mediaspace' + modId);
            },

            initJWPlayer: function(id) {
                this.setupPlayer(id);
            },

            setupPlayer: function(id) {

                var self = this;

                var firstvideo=this.modul.getFirstVideo();

                jwplayer(id).setup({

                    'file': firstvideo,
                    'width': '100%',
                    'height': '406px',
                    'controlbar': 'none',
                    'contorls' : 'false',
                    'controller': 'false',
                    'stretching' : 'uniform',
                    'dock' : 'true',
                    'icons' : 'false',
                    'mobilecontrols' : 'false',
                    'autostart':'false',
                    'menu' : 'false',
                    'controlbar.idlehide' : 'true',
                    'modes': [
                        {type: 'flash', src: this.modul.settings.flashplayer},
                        { type: "html5" }
                    ]
                });

                jwplayer(id).onPlay(function(e) {
                    var player = this;
                    self.onPlay(e,player, self.modul);
                });

                jwplayer(id).onComplete(function(e) {
                    var player = this;
                    self.onComplete(e,player,self);
                });
                jwplayer(id).onBufferChange(function(e) {
                    var player = this;
                    self.onBufferChange(e,player,self);
                });
                jwplayer(id).onBufferFull(function() {
                    var player = this;
                    self.onBufferFull(player,self);
                });
                jwplayer(id).onIdle(function() {
                    var player = this;
                    self.onIdle(player,self);
                });

                this.player = jwplayer(id);

            },

            onPlay: function(e,player, modul) {
                setTimeout(function() {
                    modul.hideDetail();
                    modul.hideOverview();
                },300);
                player.isPlaying = true;
                clearTimeout(player.loaderTimer);
                player.laoderTimerStarted = false;
                $('.loader', this.$ctx).hide();
            },


            onComplete: function(event, player, self) {
                clearTimeout(player.loaderTimer);
                player.isCompleted = true;
                player.laoderTimerStarted = false;
                $('.loader', this.$ctx).hide();
                if(self.zoomout === false) {
                    self.modul.showDetail(self.currentVideo);
                } else {
                    self.modul.showOverview();

                }
            },

            onBufferChange: function(e,player, modul) {
                if(e.bufferPercent < 100 && !$('.loader', this.$ctx).is(':visible') && !player.laoderTimerStarted && !player.isPlaying && !player.isCompleted) {
                    player.laoderTimerStarted = true;
                    player.loaderTimer = setTimeout(function() {$('.loader', this.$ctx).show();},1500);
                }
            },

            onBufferFull: function(player, modul) {
                clearTimeout(player.loaderTimer);
                player.laoderTimerStarted = false;
                $('.loader', this.$ctx).hide();
            },

            onIdle: function(player, modul) {
                clearTimeout(player.loaderTimer);
                player.laoderTimerStarted = false;
                $('.loader', this.$ctx).hide();
                player.isPlaying = false;

            },

            startVideo: function() {
                this.player.play();
            },

            zoomIn: function(video) {
                this.zoomout = false;
                this.player.isCompleted = false;
                //this.player.load(video.zoomIn);
                this.player.load([{file:video.zoomIn}]);
                this.startVideo();
                this.currentVideo = video;
            },

            pause: function() {
                this.player.pause();
            },

            zoomOut: function(video) {
                this.zoomout = true;
                this.player.isCompleted = false;
                //this.player.load(video.zoomOut);
                this.player.load([{file:video.zoomOut}]);
                this.startVideo();
                this.currentVideo = video;
            }
        },

        /** iPad Player*/
        ipad: {
            ctx: {},
            modId: '',
            modul: {},
            videotag: {},
            currentVideo: {},
            loaderTimer: {},
            init: function(ctx, modId, modul) {
                this.$ctx = ctx;
                this.modId = modId;
                this.modul = modul;
                this.initVideoPlayer('mediaspace' + modId);
            },

            initVideoPlayer: function(id) {
                this.playerId = id;
                var firstvideo=this.modul.getFirstVideo();
                this.embeddVideoPlayer(firstvideo);
            },

            embeddVideoPlayer: function(videoFile) {
                this.setupVideoPlayer(videoFile);
                this.appendVideoPlayer();
            },

            /** Setup Video-Player with Size 950px x 406px */
            setupVideoPlayer: function(videoFile) {
                this.videotag = document.createElement('video');
                this.videotag.src = videoFile;
                this.videotag.height = "406";
                this.videotag.width = "950";
                this.videotag.seekable = true;
            },

            /** Setup Video-Player to DOM,  */
            appendVideoPlayer: function() {
                var node = document.getElementById(this.playerId);
                this._removeChildNodes(node);
                node.appendChild(this.videotag);

            },
            _removeChildNodes: function(node) {
                while (node.hasChildNodes()) {
                    node.removeChild(node.lastChild);
                };
            },

            pause: function(player) {
                var vid = $('video', this.$ctx)[0];
                vid.pause();
            },

            zoomIn: function(video) {
                this.embeddVideoPlayer(video.zoomIn)
                this.startZoomIn(video);
                this.currentVideo = video;
            },

            startZoomIn: function(video) {
                this.currentVideo = video;
                var vid = $('video', this.$ctx)[0];
                var player = $('video', this.$ctx);
                vid.play();
                player.bind('ended', {player: this},this.onZoomedIn);
                if(vid.readyState !== 4){ //HAVE_ENOUGH_DATA
                    player.bind('canplaythrough', {player: this}, this.onCanZoomIn);
                    player.bind('load', {player: this}, this.onCanZoomIn); //add load event as well to avoid errors, sometimes 'canplaythrough' won't dispatch.
                    this.loaderTimer = setTimeout(function() {$('.loader', this.$ctx).show();},1500);
                    setTimeout(function(){
                        vid.pause(); //block play so it buffers before playing
                    }, 1); //it needs to be after a delay otherwise it doesn't work properly.
                }else{
                    //video is ready
                }
            },

            onZoomedIn: function(event) {
                var vid = $('video', this.$ctx);
                vid.unbind('ended', this.onZoomedIn);
                event.data.player.modul.showDetail(event.data.player.currentVideo);
            },

            onCanZoomIn: function(event) {
                clearTimeout(event.data.player.loaderTimer);
                var vid = $('video', this.$ctx);
                vid.unbind('canplaythrough', this.onCanZoomIn);
                vid.unbind('load', this.onCanZoomIn);
                //video is ready
                vid.get(0).play();
                $('.loader', this.$ctx).hide();
                event.data.player.modul.hideOverview();
            },

            zoomOut: function(video) {
                this.embeddVideoPlayer(video.zoomOut)
                this.startZoomOut(video);
                this.currentVideo = video;
            },

            startZoomOut: function(video) {
                this.currentVideo = video;
                var vid = $('video', this.$ctx)[0];
                var player = $('video', this.$ctx);
                vid.play();

                player.bind('ended', {player: this},this.onZoomedOut);
                if(vid.readyState !== 4){ //HAVE_ENOUGH_DATA
                    player.bind('canplaythrough', {player: this}, this.onCanZoomOut);
                    player.bind('load', {player: this}, this.onCanZoomOut); //add load event as well to avoid errors, sometimes 'canplaythrough' won't dispatch.
                    this.loaderTimer = setTimeout(function() {$('.loader', this.$ctx).show();},1500);
                    setTimeout(function(){
                        vid.pause(); //block play so it buffers before playing
                    }, 1); //it needs to be after a delay otherwise it doesn't work properly.
                }else{
                    //video is ready
                }
            },

            onZoomedOut: function(event) {
                clearTimeout(event.data.player.loaderTimer);
                var vid = $('video', this.$ctx);
                vid.unbind('ended', this.onZoomedOut);
                $('.loader', this.$ctx).hide();
                event.data.player.modul.showOverview();
            },

            onCanZoomOut: function(event) {
                clearTimeout(event.data.player.loaderTimer);
                var vid = $('video', this.$ctx);
                vid.unbind('canplaythrough', this.onCanZoomOut);
                vid.unbind('load', this.onCanZoomOut);
                //video is ready
                vid.get(0).play();
                $('.loader', this.$ctx).hide();
                event.data.player.modul.hideDetail();
            }
        }
    });
})(Nx.$);(function($) {
    /**
     * Droplistteaser module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Droplistteaser
     * @extends Nx.Module
     */
    Nx.Module.Droplistteaser = Nx.Module.extend({

        /**
         * Initializes the Droplistteaser module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            $('select.facility').find('option:first').attr('selected', 'selected');
            $('select.product').find('option:first').attr('selected', 'selected');
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {



            $("select.facility,select.product").change(function() {
                var selectionUrl = $(this).val();
                if (selectionUrl.length != 0) {
                    window.location = selectionUrl;
                }
                return false;
            });
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Stage module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Stage
     * @extends Nx.Module
     */
    Nx.Module.Stage = Nx.Module.extend({

        /**
         * Initializes the Stage module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Sernavi module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Sernavi
     * @extends Nx.Module
     */
    Nx.Module.Sernavi = Nx.Module.extend({

        /**
         * Initializes the Sernavi module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Teaser module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Teaser
     * @extends Nx.Module
     */
    Nx.Module.Teaser = Nx.Module.extend({

        /**
         * Initializes the Teaser module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * SchindlerCityCategoryOverview module implementation.
     *
     * @author Namics\samschmid
     * @namespace Nx.Module
     * @class SchindlerCityCategoryOverview
     * @extends Nx.Module
     */
    Nx.Module.SchindlerCityCategoryOverview = Nx.Module.extend({

        videoplayer: {},
        settings: {},
        mediaspace: {},
        state: 'new', //init, ready, overview, zooming-in, detailview, zooming-out,
        setState: function(state) {
            //console.log(state);
            this.state = state;
        },
        getState: function() {
            return this.state;
        },
        /**
         * Initializes the InteractiveVideo module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            this.setState("init");
            $(".modSchindlerCityBuildingCarousel").attr("data-player",'modSchindlerCityCategoryOverview');
            this._defineMediaspaceForVideoPlayer($ctx, modId);
            this._initSettings();
            //console.log(this.settings);
            var images = this._getOverlayImages();
            this._preload(images);
            this._choosePlayer();
            this.setState('ready');
            this._super($ctx, sandbox, modId);
        },



        _defineMediaspaceForVideoPlayer: function($ctx, modId) {
            this.mediaspace = $('.mediaspace', $ctx);
            this.mediaspace.attr('id', 'mediaspace' + modId);

        },

        _initSettings: function() {
            this.settings = {
                overview:  this.mediaspace.data('overview'),
                flashplayer:  this.mediaspace.data('flashplayer'),
                tour:  this.mediaspace.data('tour'),
                video:  this.mediaspace.data('video'),
                videos:  $.parseJSON(this.mediaspace.data('videos'))
            }
        },

        _getOverlayImages: function() {
            var numOfImages = 1;
            if(this.settings.videos !== undefined && this.settings.videos !== null){
                numOfImages = numOfImages + this.settings.videos.length;
            }
            var images = new Array(numOfImages);
            images[0] = this.settings.overview;
            if(this.settings.videos !== undefined && this.settings.videos !== null){
                for (var i = 0; i < this.settings.videos.length; i++) {
                    images[i+1] = this.settings.videos[i].zoomedPoster;
                }
            }
            return images;
        },

        _preload: function(images) {
            if (images) {
                var i = 0;

                var imageObj = new Image();
                for(i=0; i<=images.length-1; i++) {
                    //document.write('<img src="' + imageArray[i] + '" />');// Write to page (uncomment to check images)
                    imageObj.src=images[i];
                }
            }
        },

        _choosePlayer: function(){
            //console.log("ipad?");
            if(this._isIPad()) {
                this.videoplayer = this.ipad;
            } else {
                this.videoplayer = this.jw;
            }
        },

        _isIPad : function() {
            return this._browserCheck(/iPad/i);
        },

        _browserCheck: function(regex) {
            //console.log("brosercheck " + regex);
            return  navigator.userAgent.match(regex) != null;

        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
            if(!this._isIPad()) {
                this.require('jwplayer.js', 'library', 'beforeBinding');
            }
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {

            this.initOverview();
            this.showOverview();
            this.videoplayer.init(this.$ctx, this.modId, this);

            callback();
        },

        initOverview: function() {

            //HIer wird jetzt ein Bild angezeigt:
            var layer = $('.player .overlay-overview')[0];
            this._removeChildNodes(layer);
            var img = $('<img>');
            img.attr('src', this.settings.overview);
            img.appendTo('.overlay-overview');

            //MAP
            $(".player .overlay-overview > img").attr('usemap', '#overview');
            var shape =  $('<area>');
            shape.attr('shape', 'rect');
            shape.attr('coords', '300,180,400,380');
            shape.attr('href', '#');
            shape.bind('click', {player: this, video: this.settings.videos[0], id:0}, this.zoomIn);

            var shape2 =  $('<area>');
            shape2.attr('shape', 'rect');
            shape2.attr('coords', '600,280,750,380');
            shape2.attr('href', '#');
            shape2.bind('click', {player: this, video: this.settings.videos[1], id:1}, this.zoomIn);

            //console.log(shape);
            var map = $('<map>');
            map.attr('name', 'overview');
            shape.appendTo(map);
            shape2.appendTo(map);
            map.appendTo('.overlay-overview');

            //TODO Lade HTML mit Bild und Imagemap aus CQ
        },

        showOverview: function(video) {
            $('.player .overlay-overview').show();
            this.setState('overview');
        },

        zoomIn: function(event) {
            var id= event.data.id;

            event.data.player.videoplayer.zoomIn(event.data.video);
            event.data.player.setState('zooming-in');
            event.data.player.setCarouselPosition(id);

        },

        setCarouselPosition: function(id) {
            //console.log("scroll "+ id);
            $("#mycarousel").jcarousel('scroll', id+1, false);
        },

        bindCarouselEvents: function() {
            var self = this;

            $(".overlay-zoomedin .slider .overview > p").bind('click', {player: self, video: self.settings.videos[0]}, self.zoomOut);

        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            this.bindCarouselEvents();
        },

        zoomOut: function(event) {
            var video = $(".modSchindlerCityCategoryOverview").attr("data-zoomoutvideo")
            //console.log(video)
            var videoObj = $.parseJSON(video);
            event.data.player.videoplayer.zoomOut(videoObj);
            event.data.player.setState('zooming-out');
        },

        hideOverview: function() {
            $('.player .overlay-overview').hide();
        },

        showDetail: function(video) {
            this.setState('detailview');
            $('.player .modSchindlerCityBuildingCarousel').show();
            //Hier wird jetzt ein Bild angezeigt (zoomedPoster)
            /*var layer = $('.player .overlay-zoomedin')[0];
             this._removeChildNodes(layer);
             var img = $('<img>');
             img.attr('src', video.zoomedPoster);
             img.appendTo('.overlay-zoomedin');

             //MAP
             $(".player .overlay-zoomedin > img").attr('usemap', '#tower');
             var shape =  $('<area>');
             shape.attr('shape', 'rect');
             shape.attr('coords', '400,340,550,380');
             shape.attr('href', '#');
             shape.bind('click', {player: this, video: this.settings.videos[0]}, this.zoomOut);
             //console.log(shape);
             var map = $('<map>');
             map.attr('name', 'tower');
             shape.appendTo(map);
             map.appendTo('.overlay-zoomedin');*/
            //TODO Nicht bild anzeigen, sondern HTML aus CQ


            $('.player .overlay-zoomedin').show();
        },

        hideDetail: function() {
            $('.player .overlay-zoomedin').hide();
        },

        _removeChildNodes: function(node) {
            while (node.hasChildNodes()) {
                node.removeChild(node.lastChild);
            };
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        },

        /** jwplayer */
        jw: {
            ctx: {},
            modId: '',
            modul: {},
            currentVideo: {},
            zoomout: false,
            isTour: false,

            init: function(ctx, modId, modul) {
                this.$ctx = ctx;
                this.modId = modId;
                this.modul = modul;
                //console.log("jwplayer");

                this.initJWPlayer('mediaspace' + modId);
            },

            initJWPlayer: function(id) {
                this.setupPlayer(id);
            },

            setupPlayer: function(id) {

                var self = this;

                jwplayer(id).setup({

                    'file': this.modul.settings.videos[0].zoomIn,
                    'width': '950',
                    'height': '406',
                    'controlbar': 'none',
                    'contorls' : 'false',
                    'controller': 'false',
                    'stretching' : 'uniform',
                    'dock' : 'true',
                    'icons' : 'false',
                    'mobilecontrols' : 'false',
                    'autostart':'false',
                    'menu' : 'false',
                    'controlbar.idlehide' : 'true',
                    'modes': [
                        {type: 'flash', src: this.modul.settings.flashplayer},
                        { type: "html5" }
                    ]
                });

                jwplayer(id).onPlay(function(e) {
                    var player = this;
                    self.onPlay(e,player, self.modul);
                });

                jwplayer(id).onComplete(function(e) {
                    var player = this;
                    self.onComplete(e,player,self);
                });

                this.player = jwplayer(id);

            },

            onPlay: function(e,player, modul) {
                //console.log('play');
                setTimeout(function() {
                    //console.log("removePoster");
                    modul.hideDetail();
                    modul.hideOverview();
                },300);
            },

            onComplete: function(event, player, self) {

                if(self.zoomout === false) {
                    //console.log("complete zoomedin")
                    self.modul.showDetail(self.currentVideo);
                } else {
                    self.modul.showOverview();
                }

            },

            startVideo: function() {
                this.player.play();
            },

            zoomIn: function(video) {
                this.zoomout = false;
                this.player.load(video.zoomIn);
                this.startVideo();
                this.currentVideo = video;
            },

            pause: function() {
                this.player.pause();
            },

            zoomOut: function(video) {
                this.zoomout = true;
                this.player.load(video.zoomOut);
                this.startVideo();
                this.currentVideo = video;
            }
        },

        /** iPad Player*/
        ipad: {
            ctx: {},
            modId: '',
            modul: {},
            videotag: {},
            currentVideo: {},

            init: function(ctx, modId, modul) {
                this.$ctx = ctx;
                this.modId = modId;
                this.modul = modul;
                //console.log("embedded");
                this.initVideoPlayer('mediaspace' + modId);
            },

            initVideoPlayer: function(id) {
                this.playerId = id;
                this.embeddVideoPlayer(this.modul.settings.videos[0].zoomIn);
            },

            embeddVideoPlayer: function(videoFile) {
                this.setupVideoPlayer(videoFile);
                this.appendVideoPlayer();
            },

            /** Setup Video-Player with Size 950px x 406px */
            setupVideoPlayer: function(videoFile) {
                this.videotag = document.createElement('video');
                this.videotag.src = videoFile;
                this.videotag.height = "406";
                this.videotag.width = "950";
                this.videotag.seekable = true;
            },

            /** Setup Video-Player to DOM,  */
            appendVideoPlayer: function() {
                var node = document.getElementById(this.playerId);
                this._removeChildNodes(node);
                node.appendChild(this.videotag);

            },
            _removeChildNodes: function(node) {
                while (node.hasChildNodes()) {
                    node.removeChild(node.lastChild);
                };
            },

            pause: function(player) {
                var vid = $('.player video')[0];
                vid.pause();
            },

            zoomIn: function(video) {
                //console.log("nextvideo");
                this.embeddVideoPlayer(video.zoomIn)
                this.startZoomIn(video);
                this.currentVideo = video;
            },

            startZoomIn: function(video) {
                this.currentVideo = video;
                var vid = $('.player video')[0];
                var player = $('.player video');
                //console.log("start video...")
                vid.play();
                player.bind('ended', {player: this},this.onZoomedIn);
                if(vid.readyState !== 4){ //HAVE_ENOUGH_DATA
                    player.bind('canplaythrough', {player: this}, this.onCanZoomIn);
                    player.bind('load', {player: this}, this.onCanZoomIn); //add load event as well to avoid errors, sometimes 'canplaythrough' won't dispatch.
                    setTimeout(function(){
                        vid.pause(); //block play so it buffers before playing
                        //console.log("video paused...")
                    }, 1); //it needs to be after a delay otherwise it doesn't work properly.
                }else{
                    //video is ready
                }
            },

            onZoomedIn: function(event) {
                //console.log("ended");
                var vid = $('.player video');
                vid.unbind('ended', this.onZoomedIn);
                //console.log(self.nextVideo);
                //	if(event.data.player.nextVideo.tour === undefined || event.data.player.nextVideo.tour === false) {
                //	event.data.player.modul.showDetail(event.data.player.currentVideo);
                //} else {
                event.data.player.modul.showDetail(event.data.player.currentVideo);
                //}
            },

            onCanZoomIn: function(event) {
                var vid = $('.player video');
                vid.unbind('canplaythrough', this.onCanZoomIn);
                vid.unbind('load', this.onCanZoomIn);
                //video is ready
                vid.get(0).play();
                //console.log("video started...");
                event.data.player.modul.hideOverview();
            },

            zoomOut: function(video) {
                this.embeddVideoPlayer(video.zoomOut)
                this.startZoomOut(video);
                this.currentVideo = video;
            },

            startZoomOut: function(video) {
                this.currentVideo = video;
                var vid = $('.player video')[0];
                var player = $('.player video');
                //console.log("zoomOut video...")
                vid.play();
                player.bind('ended', {player: this},this.onZoomedOut);
                if(vid.readyState !== 4){ //HAVE_ENOUGH_DATA
                    player.bind('canplaythrough', {player: this}, this.onCanZoomOut);
                    player.bind('load', {player: this}, this.onCanZoomOut); //add load event as well to avoid errors, sometimes 'canplaythrough' won't dispatch.
                    setTimeout(function(){
                        vid.pause(); //block play so it buffers before playing
                        //console.log("video paused...")
                    }, 1); //it needs to be after a delay otherwise it doesn't work properly.
                }else{
                    //video is ready
                    //console.log("video is ready...");
                }
            },

            onZoomedOut: function(event) {
                var vid = $('.player video');
                vid.unbind('ended', this.onZoomedOut);
                event.data.player.modul.showOverview();
            },

            onCanZoomOut: function(event) {
                var vid = $('.player video');
                vid.unbind('canplaythrough', this.onCanZoomOut);
                vid.unbind('load', this.onCanZoomOut);
                //video is ready
                vid.get(0).play();
                event.data.player.modul.hideDetail();
                //console.log("video started...");
            }

        }
    });
})(Nx.$);
(function($) {
    /**
     * Medialibtest module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Medialibtest
     * @extends Nx.Module
     */
    Nx.Module.Medialibtest = Nx.Module.extend({

        /**
         * Initializes the Medialibtest module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Mediadownload module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Mediadownload
     * @extends Nx.Module
     */
    Nx.Module.Mediadownload = Nx.Module.extend({

        /**
         * Initializes the Mediadownload module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;

            $('a.downloadlightbox',this.$ctx).fancybox({
                'overlayOpacity'    : '0.6',
                'overlayColor'      : '#000000',
                'transitionIn'		: 'none',
                'transitionOut'		: 'none',
                'titleShow'         : 'false',
                'padding'           : '16px',
                'titlePosition'   	: 'inside'
            });
            $('#fancybox-wrap').addClass('download');

            $('input:reset').on('click', $.fancybox.close);

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * ColCtrl module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class ColCtrl
     * @extends Nx.Module
     */
    Nx.Module.ColCtrl = Nx.Module.extend({

        /**
         * Initializes the ColCtrl module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * SearchResults module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class SearchResults
     * @extends Nx.Module
     */
    Nx.Module.SearchResults = Nx.Module.extend({

        /**
         * Initializes the SearchResults module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {
            var language = this.$ctx.attr("data-language");
            var engineId = this.$ctx.attr("data-engineId");
            var context = this.$ctx;
            $("form[name='search'] .searchfield", context).autocomplete({
                source: function( request, response ) {
                    jQuery.ajax({
                        url: "http://clients1.google.com/complete/search?q=" + encodeURIComponent($("form[name='search'] .searchfield", context).val()) + "&hl=" + language + "&client=partner&source=gcsc&partnerid=" + engineId + "&ds=cse&nocache=" + Math.random().toString(),
                        dataType: "jsonp",
                        success: function( data ) {
                            response( jQuery.map( data[1], function( item ) {
                                return {
                                    label: item[0],
                                    value: item[0]
                                }
                            }));
                        }
                    });
                },
                minLength: 3,
                appendTo: "body",
                select: function (event, ui) {
                    $("form[name='search'] .searchfield", context).val(ui.item.value);
                    $("form[name='search'] .searchfield", context).parents('form').submit();
                },
                open: function(){
                    var width = $("form[name='search'] .searchfield", context).width() - 5;
                    $("form[name='search'] .searchfield", context).autocomplete('widget')
                        .width(width)
                        .css('z-index', 1337)
                }
            });
        }

    });
})(Nx.$);
(function($) {
    /**
     * Table module implementation.
     *
     * @author Namics\mgiger
     * @namespace Nx.Module
     * @class Table
     * @extends Nx.Module
     */
    Nx.Module.Table = Nx.Module.extend({

        init: function($ctx, sandbox, modId) {
            this._super($ctx, sandbox, modId);

            $('table', $ctx).indicate()
        },

    });
})(Nx.$);(function($) {
    /**
     * Mainnavi module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Mainnavi
     * @extends Nx.Module
     */
    Nx.Module.Mainnavi = Nx.Module.extend({

        /**
         * Initializes the Mainnavi module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },


        timerIn: null,
        timerOut: null,
        timerTimeOut: 10,
        timerTimeIn: 10,
        fadeTime: 300,
        isHover: false,

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            $.blockUI.defaults.overlayCSS.backgroundColor = '#ffffff';
            $.blockUI.defaults.overlayCSS.opacity = 0;

            elem = $("#nav li.trigger");
            /*if (elem.hasClass ("active")) {
             $('#nav li.active div.flyout').attr('style','display: block;');
             };*/
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            $.blockUI.defaults.fadeIn = Nx.Module.Mainnavi.prototype.fadeTime;
            $.blockUI.defaults.fadeOut = Nx.Module.Mainnavi.prototype.fadeTime;


            $('#nav > li.trigger').bind('click',function() {
                var s = $(this);

                clearTimeout(Nx.Module.Mainnavi.prototype.timerIn);
                clearTimeout(Nx.Module.Mainnavi.prototype.timerOut);

                if($(this).find('div.flyout').length > 0 && !$(this).find('div.flyout').is(':visible')) {

                    Nx.Module.Mainnavi.prototype.timerIn = setTimeout(function() {
                        Nx.Module.Mainnavi.prototype.fadeInNav(s.attr('id'));
                    }, Nx.Module.Mainnavi.prototype.timerTimeIn);
                } else {

                    Nx.Module.Mainnavi.prototype.timerIn = setTimeout(function() {
                        Nx.Module.Mainnavi.prototype.fadeOutNav(s.attr('id'));
                    }, Nx.Module.Mainnavi.prototype.timerTimeIn);
                };
                /*return false;*/
            });

            //Bind Links with class lightboxcontent
            this.bindLightboxContent();
        },

        bindLightboxContent: function() {
            $('a.lightboxcontent').each(function(index) {
                //console.log(this.href);
                var newurl = this.href;
                newurl = newurl.replace(".html", ".lightboxcontent.html");
                //console.log(newurl);
                $(this).fancybox({
                    'href'				: newurl,
                    'overlayOpacity'    : '0.6',
                    'overlayColor'      : '#000000',
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'titleShow'         : 'false',
                    'padding'           : '16px'

                });
            });

        },

        fadeInNav: function(t) {
            clearTimeout(Nx.Module.Mainnavi.prototype.timerIn);
            clearTimeout(Nx.Module.Mainnavi.prototype.timerOut);

            $('#nav > li.trigger').removeClass('hover');
            $('#' + t).addClass('hover');
            $('#' + t).find('div.flyout').fadeIn(Nx.Module.Mainnavi.prototype.fadeTime);
        },

        fadeOutNav: function(t) {
            clearTimeout(Nx.Module.Mainnavi.prototype.timerIn);
            clearTimeout(Nx.Module.Mainnavi.prototype.timerOut);

            $('#nav > li.trigger').removeClass('hover');
            $('#' + t).find('div.flyout').fadeOut(Nx.Module.Mainnavi.prototype.fadeTime);
        },


        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Sitetools module implementation.
     *
     * @author Namics\mgiger
     * @namespace Nx.Module
     * @class Sitetools
     * @extends Nx.Module
     */
    Nx.Module.Sitetools = Nx.Module.extend({

        /**
         * Initializes the Sitetools module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
            //$('.share .social_button.facebook', $ctx).attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + document.title + ' ' + window.location.href)
            //$('.share .social_button.twitter', $ctx).attr('href', 'https://twitter.com/intent/tweet?text=' + document.title + ' ' + window.location.href)
            //$('.share .social_button.google', $ctx).attr('href', 'https://plus.google.com/share?url=' + document.title + ' ' + window.location.href)
            //$('.share .social_button.linkedin', $ctx).attr('href', 'https://www.linkedin.com/shareArticle?mini=true&url=' + window.location.href + '&title=' + document.title + '&summary=&source=')

        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;

            $('#tellafriend',this.$ctx).fancybox({
                'overlayOpacity'    : '0.6',
                'overlayColor'      : '#000000',
                'transitionIn'		: 'none',
                'transitionOut'		: 'none',
                'titleShow'         : 'false',
                'padding'           : '16px',
                'titlePosition'   	: 'inside'
            });
            $('#fancybox-wrap').addClass('form');
        },

    });
})(Nx.$);
(function($) {
    /**
     * Textimage module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Textimage
     * @extends Nx.Module
     */
    Nx.Module.Textimage = Nx.Module.extend({

        /**
         * Initializes the Textimage module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;
            $('a.lightbox',this.$ctx).fancybox({
                'overlayOpacity'    : '0.6',
                'overlayColor'      : '#000000',
                'transitionIn'		: 'none',
                'transitionOut'		: 'none',
                'titleShow'         : 'false',
                'padding'           : '16px',
                'titlePosition'   	: 'inside'
            });
            $('#fancybox-wrap').addClass('image');
        },

    });
})(Nx.$);
(function($) {
    // @mgiger
    Nx.Module.Newsticker = Nx.Module.extend({

        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        dependencies: function() {

        },

        beforeBinding: function(callback) {
            callback();
        },

        onBinding: function() {
            $(document).ready(function () {
                var isRTL = document.dir === 'rtl';

                var prev = $('.modNewsticker .jcarousel').append('<a class="nav jcarousel-prev' +  (isRTL ? '': ' disabled') + '" href="#"><svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="10,18 2,10 10,2 "/></svg></a>');
                var next = $('.modNewsticker .jcarousel').append('<a class="nav jcarousel-next' +  (isRTL ? ' disabled': '') + '" href="#"><svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="2,18 10,10 2,2 "/></svg></a>');

                $('.modNewsticker .jcarousel').on('jcarousel:create jcarousel:reload', function() {
                    // Number of items visible according to viewport size
                    var windowWidth = viewport().width;
                    var cols = 1;
                    if (windowWidth > 500)
                        cols = 2;
                    if (windowWidth > 849)
                        cols = 3;
                    var element = $(this), width = element.innerWidth();
                    element.jcarousel('items').css('width', width / cols + 'px');
                }).on('jcarousel:animateend', function(event, carousel) {
                    // Hide arrows, when no items left in their direction
                    var lastIndex = $(this).jcarousel('items').index($(this).jcarousel('items').last());
                    var firstIndex = $(this).jcarousel('items').index($(this).jcarousel('items').first());
                    var currentIndex = $(this).jcarousel('items').index($(this).jcarousel('target'));
                    var size = $(this).jcarousel('fullyvisible').size();
                    if (!isRTL) {
                        if (currentIndex > lastIndex - size)
                            $('.modNewsticker .jcarousel .jcarousel-next').addClass('disabled');
                        else
                            $('.modNewsticker .jcarousel .jcarousel-next').removeClass('disabled');
                        if (currentIndex <= firstIndex)
                            $('.modNewsticker .jcarousel .jcarousel-prev').addClass('disabled');
                        else
                            $('.modNewsticker .jcarousel .jcarousel-prev').removeClass('disabled');
                    } else {
                        if (currentIndex > lastIndex - size)
                            $('.modNewsticker .jcarousel .jcarousel-prev').addClass('disabled');
                        else
                            $('.modNewsticker .jcarousel .jcarousel-prev').removeClass('disabled');
                        if (currentIndex <= firstIndex)
                            $('.modNewsticker .jcarousel .jcarousel-next').addClass('disabled');
                        else
                            $('.modNewsticker .jcarousel .jcarousel-next').removeClass('disabled');
                    }
                }).on('jcarousel:createend', function(event, carousel) {
                    var windowWidth = viewport().width;
                    var cols = 1;
                    if (windowWidth > 500)
                        cols = 2;
                    if (windowWidth > 849)
                        cols = 3;

                    if (isRTL) {
                        size = $(this).jcarousel('items').size();
                        if (size <= cols)
                            $('.jcarousel-prev').addClass('disabled');
                    } else {
                        size = $(this).jcarousel('items').size();
                        if (size <= cols)
                            $('.jcarousel-next').addClass('disabled');
                    }
                }).jcarousel({});



                if (!isRTL) {
                    $('.modNewsticker .jcarousel-prev').jcarouselControl({
                        target: '-=1'
                    });
                    $('.modNewsticker .jcarousel-next').jcarouselControl({
                        target: '+=1'
                    });
                    $(".jcarousel").swipe({
                        swipe: function(event, direction) {
                            if (direction === 'left') {
                                $('.modNewsticker .jcarousel').jcarousel('scroll', '+=1');
                            } else {
                                $('.modNewsticker .jcarousel').jcarousel('scroll', '-=1');
                            }
                        }
                    });
                } else {
                    $('.modNewsticker .jcarousel-prev').jcarouselControl({
                        target: '+=1'
                    });
                    $('.modNewsticker .jcarousel-next').jcarouselControl({
                        target: '-=1'
                    });
                    $(".jcarousel").swipe({
                        swipe: function(event, direction) {
                            if (direction === 'left') {
                                $('.modNewsticker .jcarousel').jcarousel('scroll', '-=1');
                            } else {
                                $('.modNewsticker .jcarousel').jcarousel('scroll', '+=1');
                            }
                        }
                    });
                }
            });
        },

        afterBinding: function() {

        }

    });

    function viewport() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }

})(Nx.$);
(function($) {
    /**
     * Formular module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Formular
     * @extends Nx.Module
     */
    Nx.Module.Formular = Nx.Module.extend({

        /**
         * Initializes the Formular module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;
            var tmpFormID = '';


            $("#formSubmit",this.$ctx).click(function() {
                $formCtx = $(this).closest('form');
                tmpFormID = $formCtx.attr('id');

                $('.errorText ul', $formCtx).empty();

                var tmpErrorMsg = '';
                $('.errorMessageInline',$formCtx).each(function(index) {
                    var tmpParent = '';
                    tmpParent = $(this).closest(".frmitem").find("input").attr("name");
                    if(tmpParent == undefined){
                        tmpParent = $(this).closest(".frmitem").find("textarea").attr("name");
                    }
                    if(tmpParent == undefined){
                        tmpParent = $(this).closest(".frmitem").find("select").attr("name");
                    }
                    tmpErrorMsg = tmpErrorMsg + '<li><label for="'+tmpParent+'" class="error">'+$(this).text()+'</label></li>';
                });
                $('.errorText ul', $formCtx).append(tmpErrorMsg);

                var container = $('div.errorText', $formCtx);
                // validate the form when it is submitted

                var validator = $('#' +tmpFormID).validate({
                    errorClass: "error",
                    errorContainer: container,
                    errorLabelContainer: $("ol", container),
                    wrapper: 'li',
                    meta: "validate",

                    highlight: function(element, errorClass) {

                        if ($(element).parent().parent().hasClass('frmline')) {
                            $(element).parent().parent().addClass(errorClass);
                        } else {
                            $(element).parent().parent().parent().addClass(errorClass);
                        };
                    },
                    unhighlight: function(element, errorClass) {
                        if ($(element).parent().parent().hasClass('frmline')) {
                            $(element).parent().parent().removeClass(errorClass);
                        } else {
                            $(element).parent().parent().parent().removeClass(errorClass);
                        };
                    }
                });
            });


            $(".cancel").click(function() {
                validator.resetForm();
            });

            var $form = $('#submitOnLoad')[0];
            if ($form && $form.textContent == "true") {
                $('#' +tmpFormID).submit();
            }

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Newslist module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Newslist
     * @extends Nx.Module
     */
    Nx.Module.Newslist = Nx.Module.extend({

        /**
         * Initializes the Newslist module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;
            $('.newsEntry',this.$ctx).hover(function() {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                });
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Offscreen Navigation module implementation.
     *
     * @author Matthias Giger
     * @namespace Nx.Module
     * @class Offscreen
     * @extends Nx.Module
     */
    Nx.Module.Offscreen = Nx.Module.extend({

        /**
         * Initializes the Offscreen module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            var languagePageUrls = [];

            $('#mmenu li.country a').each(function() {
                languagePageUrls.push($(this).attr('href'));
            });

            $(document).ready(function() {
                if ($('html').attr('dir') != 'rtl') {
                    $("#mmenu").mmenu({extensions: ["pagedim-black", "effect-menu-slide"], offCanvas: {position: "right"}});
                } else {
                    $("#mmenu").mmenu({extensions: ["pagedim-black"], offCanvas: {position: "left"}});
                    $("#mmenu").css('right', 'auto');
                }

                var currentUrl = window.location.pathname;

                currentUrl = currentUrl.replace('.html', '').replace('/content', '');

                var match = false;

                languagePageUrls.forEach(function(url) {
                    url = url.replace('/content', '');
                    if (url === currentUrl) {
                        match = true;
                    }
                });

                if (match) {
                    $(".mm-panel.mm-hasnavbar").removeClass('mm-opened mm-subopened mm-current').addClass('mm-hidden');
                    $("#mm-1").addClass('mm-opened').removeClass('mm-hidden mm-subopened');
                }
            });
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Disclaimernavi module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Disclaimernavi
     * @extends Nx.Module
     */
    Nx.Module.Disclaimernavi = Nx.Module.extend({

        /**
         * Initializes the Disclaimernavi module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * GlossaryOverview module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class GlossaryOverview
     * @extends Nx.Module
     */
    Nx.Module.GlossaryOverview = Nx.Module.extend({

        /**
         * Initializes the GlossaryOverview module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Iframe module implementation.
     *
     * @author Namics\mgiger
     * @namespace Nx.Module
     * @class Iframe
     * @extends Nx.Module
     */
    Nx.Module.Iframe = Nx.Module.extend({

        /**
         * Initializes the Iframe module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            $('iframe', $ctx).indicate();
            this._super($ctx, sandbox, modId);
        }

    });
})(Nx.$);
(function($) {
    /**
     * Claim module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Claim
     * @extends Nx.Module
     */
    Nx.Module.Claim = Nx.Module.extend({

        /**
         * Initializes the Claim module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * SchindlerCityBuildingCarousel module implementation.
     *
     * @author Namics\samschmid
     * @namespace Nx.Module
     * @class SchindlerCityBuildingCarousel
     * @extends Nx.Module
     */
    Nx.Module.SchindlerCityBuildingCarousel = Nx.Module.extend({
        player:'',
        /**
         * Initializes the SchindlerCityBuildingCarousel module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            this.player =   $(".modSchindlerCityBuildingCarousel").attr("data-player");
            //console.log(this.player);
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {


            var self= this;

            $(document).ready(function () {



                jQuery('#mycarousel').jcarousel({
                    scroll: 1,
                    itemVisibleInCallback: function(carousel, li) {
                        //console.log($(li).attr('data-zoomOut'));

                        $("."+self.player).attr('data-zoomOutVideo',$(li).attr('data-zoomOut'));
                    }
                });


            });



            //$(".overlay-zoomedin .slider .overview > p").bind('click', {player: self, video: self.settings.videos[0]}, self.zoomOut);
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * GlossarOverview module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class GlossarOverview
     * @extends Nx.Module
     */
    Nx.Module.GlossarOverview = Nx.Module.extend({

        /**
         * Initializes the GlossarOverview module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Searchbox module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Searchbox
     * @extends Nx.Module
     */
    Nx.Module.Searchbox = Nx.Module.extend({

        /**
         * Initializes the Searchbox module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            //global vars
            var inpsearchtext = $("#searchtext").text();
            var context = this.$ctx;
            $('#inpsearch').attr("value", inpsearchtext);

            //show/hide default text
            $("#inpsearch").focus(function(){
                if(inpsearchtext == $("#searchtext").text()){
                    $(this).attr("value", '');
                }else{
                    $(this).attr("value", inpsearchtext);
                }
            });
            $("#inpsearch").blur(function(){
                $(this).attr("value", inpsearchtext);
            });
            $("#inpsearch").keyup(function(){
                inpsearchtext = $(this).attr('value');
            });


            $("form", context).submit(function(){
                if($("#inpsearch").attr("value") == $("#searchtext").text()){
                    $("#inpsearch").attr("value","");
                }
                return true; // false would cancel form submit
            });


            $('.searchboxPart .searchbutton input').attr('value', '');
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {
            var language = this.$ctx.attr("data-language");
            var engineId = this.$ctx.attr("data-engineId");
            var context = this.$ctx;
            $("form[name='search'] .searchfield", context).autocomplete({
                source: function( request, response ) {
                    jQuery.ajax({
                        url: "http://clients1.google.com/complete/search?q=" + encodeURIComponent($("form[name='search'] .searchfield", context).val()) + "&hl=" + language + "&client=partner&source=gcsc&partnerid=" + engineId + "&ds=cse&nocache=" + Math.random().toString(),
                        dataType: "jsonp",
                        success: function( data ) {
                            response( jQuery.map( data[1], function( item ) {
                                return {
                                    label: item[0],
                                    value: item[0]
                                }
                            }));
                        }
                    });
                },
                minLength: 3,
                appendTo: "body",
                select: function (event, ui) {
                    $("form[name='search'] .searchfield", context).val(ui.item.value);
                    $("form[name='search'] .searchfield", context).parents('form').submit();
                },
                open: function(){
                    var width = $("form[name='search'] .searchfield", context).width() - 5;
                    $("form[name='search'] .searchfield", context).autocomplete('widget')
                        .width(width)
                        .css('z-index', 1337);

                    $('.ui-autocomplete').css('top', ($(this).offset().top + 21) + 'px', 'important');
                    $('.ui-autocomplete').css('margin-left', '-1px', 'important');
                }
            });
        }

    });
})(Nx.$);
(function($) {
    /**
     * JobsearchBox module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class JobsearchBox
     * @extends Nx.Module
     */
    Nx.Module.JobsearchBox = Nx.Module.extend({

        /**
         * Initializes the JobsearchBox module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;
            var sapSearchButton = $(".searchbutton", this.$ctx);
            var isSap = sapSearchButton.length > 0;

            if (isSap) {
                sapSearchButton.click(function() {
                    var targetUrl = $('.targetUrl', that.$ctx).attr("href");
                    var selectedFields = $('select', this.$ctx);
                    $.each(selectedFields, function() {
                        targetUrl = that.addUrlParameter(targetUrl, $(this).attr("id"), $(this).val());
                    });
                    window.location.href = targetUrl;
                });
            } else {
                $('select', this.$ctx).change(function() {
                    var selectedVal = $(this).val();
                    var selectedId = $(this).attr("id");
                    if (selectedVal == '') {
                        // do nothing
                    } else {
                        var targetUrl = $('.targetUrl', that.$ctx).attr("href");
                        if (targetUrl.length > 0) {
                            window.location.href = that.addUrlParameter(targetUrl, selectedId, selectedVal);
                        }
                    }
                });
            }

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        },

        /**
         * Adds an url as parameter to an url
         *
         * @return string
         */
        addUrlParameter: function(url, key, val) {
            url += (url.split('?')[1] ? '&':'?') + encodeURIComponent(key) + "=" + encodeURIComponent(val);
            return url;
        }

    });
})(Nx.$);
(function($) {
    /**
     * GlossaryEntry module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class GlossaryEntry
     * @extends Nx.Module
     */
    Nx.Module.GlossaryEntry = Nx.Module.extend({

        /**
         * Initializes the GlossaryEntry module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Logo module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Logo
     * @extends Nx.Module
     */
    Nx.Module.Logo = Nx.Module.extend({

        /**
         * Initializes the Logo module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            $( document ).ready(function() {
                /*console.log(jQuery.browser.msie);
                 if (jQuery.browser.msie) {
                 $('img', $ctx).bicubicImgInterpolation({
                 crossOrigin: 'anonymous'
                 });
                 }*/
            });
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Langselect module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Langselect
     * @extends Nx.Module
     */
    Nx.Module.Langselect = Nx.Module.extend({

        /**
         * Initializes the Langselect module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;
            $(".language a",this.$ctx).click(function() {
                var targetLink = $(this).attr("href");
                // only set cookie if it is a relative link.
                if (targetLink.indexOf("/") == 0) {
                    var linkNodes = targetLink.split("/");
                    // Remove first empty entry
                    linkNodes.shift();
                    // Remove content entry if it exists
                    if (linkNodes[0] == "content") {
                        linkNodes.shift();
                    }
                    var country = linkNodes[0];
                    var tenant = linkNodes[1];
                    var language = linkNodes[2];
                    // TODO Make expires configurable
                    $.cookie(country + "_" + tenant, language ,{ expires: 14, path: "/" });
                }
            });
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Video module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Video
     * @extends Nx.Module
     */
    Nx.Module.Video = Nx.Module.extend({

        /**
         * Initializes the Video module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
            this.require('jwplayer.js', 'library', 'beforeBinding');
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            var that = this,
                $ctx = this.$ctx,
                modId = this.modId,
                $player = $('.mediaspace', $ctx),
                player,
                isSchindlerCity = $ctx.parent().hasClass('schindlercitytour');

            // mgiger
            var aspectratio = '16:9';

            if ($player.parent().attr('class') === 'flashpart4x3') {
                aspectratio = '4:3';
            }

            aspectratio = '16:9';

            $player.attr('id', 'mediaspace' + modId);

            if (!isSchindlerCity) {
                player = jwplayer('mediaspace' + modId).setup({
                    flashplayer: $player.data('playerurl'),
                    file: $player.data('videourl'),
                    skin: $player.data('videoSkin'),
                    image: $player.data('videoimage'),
                    controlbar: 'bottom',
                    // SCHINDLER-579
                    stretching: 'none',
                    dock: 'true',
                    icons: 'true',
                    menu: 'false',
                    volume: '30',
                    'controlbar.idlehide': 'true',
                    width: "100%",
                    aspectratio: aspectratio
                });
            } else {
                player = jwplayer('mediaspace' + modId).setup({
                    flashplayer: $player.data('playerurl'),
                    file: $player.data('videourl'),
                    skin: $player.data('videoSkin'),
                    image: $player.data('videoimage'),
                    controlbar: 'bottom',
                    // SCHINDLER-579
                    dock: 'true',
                    icons: 'true',
                    menu: 'false',
                    volume: '30',
                    'controlbar.idlehide': 'true',
                    width: '950px',
                    height: '406px'
                });
            }
            jwplayer('mediaspace' + modId).onPlay(function(e) {
                that.fire('stop');
            });
            jwplayer('mediaspace' + modId).onPause(function(e) {
                that.fire('play');
            });
            jwplayer('mediaspace' + modId).onComplete(function(e) {
                that.fire('play');
            });

            this.player = player;

            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Faq module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Faq
     * @extends Nx.Module
     */
    Nx.Module.Faq = Nx.Module.extend({

        /**
         * Initializes the Faq module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var $ctx = this.$ctx;

            var tmp = $(".modFaq .faqitem.open");
            $(tmp).children(".content").slideDown('slow', function() {});

            $('h3.faqtitle',$ctx).click(function () {
                var parent = $(this).parent();
                if ($(parent).hasClass("close")){
                    $(parent).children(".content").slideDown('slow', function() {});
                    $(parent).addClass("open");
                    $(parent).removeClass("close");
                }
                else {
                    $(parent).children(".content").slideUp('slow', function() {});
                    $(parent).addClass("close");
                    $(parent).removeClass("open");
                }
            });
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Countryselect module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Countryselect
     * @extends Nx.Module
     */
    Nx.Module.Countryselect = Nx.Module.extend({

        /**
         * Initializes the Countryselect module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
            this.cookieExpTime = parseInt($('#countryselect',this.$ctx).attr("data-cookie-expiration-time"));
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            var that = this;
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {
            var that = this;

            //Split the entries to two columns, if there are more than 19 countries in one continent.
            that.sortElementsAndAddClasses();

            // Open Countryselector and split entries
            $('#countryselect',this.$ctx).fancybox({
                'autoDimensions'	: false,
                'width'	:  970,
                'height'	:  700,
                'scrolling'	: 'no',
                'overlayOpacity'    : '0.6',
                'overlayColor'      : '#000000',
                'transitionIn'		: 'none',
                'transitionOut'		: 'none',
                'titleShow'         : 'false',
                'padding'           : '16px',
                'onClosed'			: function(){
                    that.closeCountrySelector();
                }
            });

            //Check an remember the cookie
            $('#countrylightbox li a').click(function(){
                that.checkAndSetRememberCookie($(this));
            });

            // mgiger

            function resize() {
                var isMobile = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 981;

                if (!isMobile) {
                    $('#countrylightbox ul').show();
                } else {
                    $('#countrylightbox ul').hide();

                    if (!$('#countrylightbox h3.base').hasClass('bound')) {
                        $('#countrylightbox h3.base').addClass('bound').click(function() {
                            if ($(this).next().css('display') === 'none')
                                $(this).parent().parent().find('ul').hide();
                            $(this).next().slideToggle('fast');
                            if ($(this).next().next().hasClass('country2col'))
                                $(this).next().next().slideToggle('fast');
                        });
                    }
                }
            }

            $(window).resize(resize);

            resize();
        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {
            var that = this;
            $openCS = $('#countryselect',this.$ctx).attr("data-open-countryselector");
            if ($openCS) {
                that.openCountrySelector(this.$ctx);
            }
        },

        sortElementsAndAddClasses: function() {
            $countryLightbox = $('#countrylightbox');

            if (!$countryLightbox.hasClass('alreadyopened')) {

                $countryLightbox.addClass('alreadyopened');

                $continentDivs = $('#countrylightbox .content').find("div.unit");


                $continentDivs.each(function(index) {
                    $countriesInContinent = $(this).find('li');
                    var numberOfCountriesInContinent = $countriesInContinent.length;
                    if (numberOfCountriesInContinent > 19) {
                        if (!$(this).hasClass('lastUnit')) {
                            $(this).addClass("size2of6");
                        }

                        var firstHalfOfCountries = Math.ceil(numberOfCountriesInContinent / 2);
                        var secondHalfOfCountries = numberOfCountriesInContinent - firstHalfOfCountries;
                        $(this).find('ul').remove();
                        $firstCol = $('<ul class="country2col"></ul>');
                        $secondCol = $('<ul class="country2col"></ul>');

                        $countriesInContinent.each(function(index) {
                            if (index < firstHalfOfCountries) {
                                $firstCol.append($(this));
                            } else {
                                $secondCol.append($(this));
                            }
                        });
                        $(this).append($firstCol);
                        $(this).append($secondCol);
                    } else {
                        if (!$(this).hasClass('lastUnit')) {
                            $(this).addClass("size1of6");
                        }
                    }
                });
            }
        },

        checkAndSetRememberCookie: function($ctx) {
            var targetLink = $ctx.attr("href");
            if (targetLink.lastIndexOf(".html") > -1) {
                targetLink = targetLink.slice(0, targetLink.lastIndexOf(".html"));
            }
            var checked = $("#rememberCountry:checked").length;
            if (checked === 1) {
                var expTime = this.cookieExpTime;
                // cookie to remember the choosen country
                $.cookie("country", targetLink ,{ expires: expTime, path: "/" });
                // cookie to prevent the country selector from popping-up again
                $.cookie("nocountryselector", "nocountryselector" ,{ expires: expTime, path: "/" });
            }
        },

        openCountrySelector: function($ctx) {
            // check for no-selection cookie before auto-open
            if(!$.cookie("nocountryselector")) {
                $('#countryselect').trigger('click');
            }
        },

        closeCountrySelector: function() {
            // create cookie if the country selector is closed without
            // any selection (click on close button or blackedout screen)
            var expTime = this.cookieExpTime;
            $.cookie("nocountryselector", "nocountryselector" ,{ expires: expTime, path: "/" });
        }

    });
})(Nx.$);(function($) {
    /**
     * Contactbox module implementation.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module
     * @class Contactbox
     * @extends Nx.Module
     */
    Nx.Module.Contactbox = Nx.Module.extend({

        /**
         * Initializes the Contactbox module.
         *
         * @method init
         * @return {void}
         * @constructor
         * @param {jQuery} $ctx the jquery context
         * @param {Sandbox} sandbox the sandbox to get the resources from
         * @param {String} modId the unique module id
         */
        init: function($ctx, sandbox, modId) {
            // call base constructor
            this._super($ctx, sandbox, modId);
        },

        /**
         * Hook function to load the module specific dependencies.
         *
         * @method dependencies
         * @return void
         */
        dependencies: function() {
        },

        /**
         * Hook function to do module specific stuff before binding the events (i.e. fetching some data).
         *
         * @method beforeBinding
         * @param {Function} callback the callback function which must be called at the end
         * @return void
         */
        beforeBinding: function(callback) {
            callback();
        },

        /**
         * Hook function to bind the module specific events.
         *
         * @method onBinding
         * @return void
         */
        onBinding: function() {

        },

        /**
         * Hook function to do module specific stuff after binding the events (i.e. triggering some events).
         *
         * @method afterBinding
         * @return void
         */
        afterBinding: function() {

        }

    });
})(Nx.$);
(function($) {
    /**
     * Static Skin implementation for module Privacypolicy.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Privacypolicy
     * @class Static
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Privacypolicy.Static = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();

            var that = this;

            var tmpCookieExpiredtime = '';
            tmpCookieExpiredtime = $('.modPrivacypolicy').data('cookieexpiredtime');

            if (isNaN( tmpCookieExpiredtime) || tmpCookieExpiredtime == '') {
                tmpCookieExpiredtime = 1825;
            };

            this.checkCookie();

            $('.cnt .left a',this.$ctx).click(function() {
                $.cookie("tracking", "ok", {
                    expires: tmpCookieExpiredtime,
                    path: '/'
                });
                $('.modPrivacypolicy').hide()
                return false;
            });
            $('.cnt .right a',this.$ctx).click(function() {
                $.cookie("tracking", "ok", {
                    expires: tmpCookieExpiredtime,
                    path: '/'
                });
                $('.modPrivacypolicy').hide()
            });

        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
        this.checkCookie =  function(){
            var checkCookie = '';
            checkCookie = $.cookie("tracking");

            if (checkCookie == 'ok') {
                $('.modPrivacypolicy').hide();
            } else {
                $('.modPrivacypolicy').show();
            };
        };
    };
})(Nx.$);
(function($) {
    /**
     * Lightbox Skin implementation for module Privacypolicy.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Privacypolicy
     * @class Lightbox
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Privacypolicy.Lightbox = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();

            var that = this;

            var tmpCookieExpiredtime = '';
            tmpCookieExpiredtime = $('.modPrivacypolicy').data('cookieexpiredtime');

            if (isNaN( tmpCookieExpiredtime) || tmpCookieExpiredtime == '') {
                tmpCookieExpiredtime = 1825;
            };


            var checkCookie = '';
            checkCookie = $.cookie("tracking");

            if (checkCookie == 'ok') {

            } else {
                $('.modPrivacypolicy').show();
                $('body').addClass('privacypolicy');
                $('.skinPrivacypolicyLightbox').attr('id','flyout');

                $('html').block({
                    title: null,
                    theme: false,
                    message: $('#flyout'),
                    css: {
                        padding:        0,
                        margin:         0,
                        width:          '100%',
                        position:       'fixed',
                        bottom:         '0',
                        left:           '0',
                        textAlign:      'center',
                        color:          '#000',
                        border:         'none',
                        backgroundColor:'transparent',
                        cursor:         'default'
                    },
                    overlayCSS:  {
                        backgroundColor: '#000',
                        opacity:         0.6,
                        cursor:          'default'
                    }
                });

                $('.blockElement').css('top', 'auto');

                $('.cnt .left a',this.$ctx).click(function() {
                    $.cookie("tracking", "ok", {
                        expires: tmpCookieExpiredtime,
                        path: '/'
                    });
                    $('.modPrivacypolicy').hide();
                    $('html').unblock();
                    /* NOTE fix for rtlcontentprivacy2 and contentprivacy2 */
                    setTimeout(function(){
                        $('.modPrivacypolicy').hide();
                    }, 500);
                });
                $('.cnt .right a',this.$ctx).click(function() {
                    $.cookie("tracking", "ok", {
                        expires: tmpCookieExpiredtime,
                        path: '/'
                    });
                    $('.modPrivacypolicy').hide();
                    $('html').unblock();
                });
            };
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);
(function($) {
    /**
     * Home Skin implementation for module Stage.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Stage
     * @class Home
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Stage.Home = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);
(function($) {
    /**
     * Noexist Skin implementation for module Stage.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Stage
     * @class Noexist
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Stage.Noexist = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);
(function($) {
    /**
     * Random Skin implementation for module Teaser.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Teaser
     * @class Random
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Teaser.Random = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            var $ctx = this.$ctx;
            // calling parent method
            parent.onBinding();
            // Listen to orientation change event because random teaser has problems if not reloaded
            /*$(window).bind("orientationchange",function(e) {
             window.location.reload();
             });*/
            // Load randomteaser
            $(document).ready(function() {
                var interval = parseInt($('#randomhometeaser', $ctx).attr('data-interval'));

                if (isNaN(interval)) {
                    interval = 5;
                }
                // Remove li entries if there is no a-tag in it because these are not valid elemements
                $("#randomhometeaser li", $ctx).not(":has(a)").remove();
                if ($("#randomhometeaser li", $ctx).length < 2) {
                    interval = 0;
                    $('.jcarousel-control', $ctx).hide();
                }

                interval = interval*1000;

                $('.modTeaser .jcarousel').on('jcarousel:create jcarousel:reload', function() {
                    var element = $(this), width = element.innerWidth();
                    element.jcarousel('items').css('width', width + 'px');
                }).jcarousel({wrap: 'circular', vertical: true});

                $('.modTeaser.skinTeaserRandom .jcarousel').append('<p class="jcarousel-pagination"></p>');
                if(interval > 0){
                    $('.modTeaser.skinTeaserRandom .jcarousel').jcarouselAutoscroll({
                        interval: interval
                    });
                }

                setTimeout(function() {
                    $('.jcarousel-pagination').children().first().addClass('active');
                }, 500);

                $('.jcarousel-pagination').jcarouselPagination({
                    item: function(page, items) {
                        return '<a href="#' + page + '"><svg width="15" height="15"><rect x="2" y="2" width="10" height="10" /></svg></a>';
                    }
                });

                $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
                    $(this).addClass('active')
                });

                $('.jcarousel-pagination').on('jcarouselpagination:inactive', 'a', function() {
                    $(this).removeClass('active')
                });
            });
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };

        function viewport() {
            var e = window, a = 'inner';
            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
        }
    };
})(Nx.$);
(function($) {
    /**
     * Error Skin implementation for module Formular.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Formular
     * @class Error
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Formular.Error = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);
(function($) {
    /**
     * Border Skin implementation for module Iframe.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Iframe
     * @class Border
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Iframe.Border = function(parent) {


    };
})(Nx.$);
(function($) {
    /**
     * Logo2 Skin implementation for module Logo.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Logo
     * @class Logo2
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Logo.Logo2 = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);
(function($) {
    /**
     * Dropdown Skin implementation for module Contactbox.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Contactbox
     * @class Dropdown
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Contactbox.Dropdown = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
            var that = this;

            this.fillStateData();

            $('.dropdown',this.$ctx).delegate('#state', 'change', function() {
                var selectedState = $(this).val();
                if (selectedState == '--' || selectedState == '') {
                    $('.stateinfo').hide();
                } else {
                    $('.stateinfo').hide();
                    if ($('#'+selectedState + ' .contactPath').length > 0) {
                        var contactPath = $('#'+selectedState + ' .contactPath').attr("href");
                        window.location.href = contactPath;
                    } else {
                        $('#'+selectedState).show();
                    }
                }
            });
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };

        this.fillStateData = function() {
            var contactsUrl = $(".contactsLink");
            if (contactsUrl.length < 1) {
                return;
            }
            var that = this;
            $.get(contactsUrl.attr("href"), function(data) {
                $.each(data, function(idx, contact) {
                    if (typeof contact.levelTitle != "undefined") {
                        that.createSeparator();
                        that.createLevelTitle(contact);
                        that.createSeparator();
                        return;
                    }
                    if (!$.isEmptyObject(contact.children)) {
                        var opt = $("<optgroup />");
                        opt.attr("label", contact.title);
                        $("#state").append(opt);
                        $.each(contact.children, function(key, value) {
                            that.createStateInfo(value);
                            that.createOption(value, true, opt);
                        });
                        if (idx != data.length - 1) {
                            //that.createSeparator();
                        }
                    } else {
                        that.createStateInfo(contact);
                        that.createOption(contact, false);
                    }
                });
            });
        };

        this.createLevelTitle = function(contact) {
            var o = $("<optgroup />");
            //o.val("");
            o.attr("label", contact.levelTitle);
            $("#state").append(o);
        }

        this.createOption = function(contact, isChild, optGroup) {
            var o = $("<option />");
            o.val(contact.id);
            if (!$.isEmptyObject(contact.children)) {
                o.attr("label", contact.title);
            } else if (isChild) {
                o.text(contact.title);
            } else {
                o.text(contact.title);
            }
            if (!$.isEmptyObject(optGroup)) {
                optGroup.append(o);
            } else {
                $("#state").append(o);
            }
        };

        this.createStateInfo = function(contact) {
            var info = $("<div />");
            info.attr("id", contact.id);
            info.addClass("stateinfo");
            info.html(contact.richtext + contact.location);
            $(".result").append(info);
            if (contact.path.length > 0) {
                var path = $("<a />");
                path.addClass("contactPath");
                path.attr("href", contact.path);
                path.hide();
                info.append(path);
            }
        };

        this.createSeparator = function() {
            var o = $("<option />");
            o.attr("disabled", "disabled");
            o.val("");
            o.text("----------");
            $("#state").append(o);
        };

    };
})(Nx.$);
(function($) {
    /**
     * Link Skin implementation for module Contactbox.
     *
     * @author Namics\tbauer
     * @namespace Nx.Module.Contactbox
     * @class Link
     * @extends Nx.Module
     * @constructor
     */
    Nx.Module.Contactbox.Link = function(parent) {
        /**
         * override the appropriate methods from the decorated module (ie. this.get = function()).
         * the former/original method may be called via parent.<method>()
         */
        this.dependencies = function() {
            // calling parent method
            parent.dependencies();
        };

        this.beforeBinding = function(callback) {
            // calling parent method
            parent.beforeBinding(function() {
                callback();
            });
        };

        this.onBinding = function() {
            // calling parent method
            parent.onBinding();
            var that = this;

            $('#state',this.$ctx).change(function() {
                var selectValue = $(this).val();

                if (selectValue == '--'){

                } else {
                    window.location = '/index/content/'+selectValue;
                }

            });
        };

        this.afterBinding = function() {
            // calling parent method
            parent.afterBinding();
        };
    };
})(Nx.$);


function loginuser(failedLoginMessage) {
    return true;
}

function signout(redirectTarget) {
	document.location = "/libs/security/logout.html?redirect="+redirectTarget;
}

/*************************************************************************
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

// Does the whole client-side detection to forward the visitor to the best-suited site flavor
// This is especially useful when a link has been shared across various devices, to redirect the client to the best URL
var DetectMobile = function (apiUrl, eventHandlers) {
    
    var $ = jQuery;
    var that = this;
    var cookieName        = "DetectMobile";
    var cookieValue       = cookie(cookieName);
    var cookieMatch       = cookieValue && cookieValue.match(/^([^:]+):(.*)$/);
    var cookieState       = cookieMatch && cookieMatch[1];
    var cookiePreviousUrl = cookieMatch && cookieMatch[2];
    var locationMatch     = window.location.pathname.match(/^([^\.]+)\.(?:([^\/\.]+)\.)*([^\/\.]+)(.*)$/);
    var locationPath      = locationMatch[1];
    var locationSelector  = locationMatch[2];
    var locationExtension = locationMatch[3];
    var locationSuffix    = locationMatch[4];
    
    // ----< Public Methods >----------------------------------------------------------------------
    
    that.setAutoRedirectCookie = function () {
        cookie(cookieName, "auto:" + window.location.href);
    };
    
    that.setManualRedirectCookie = function () {
        cookie(cookieName, "manual:" + window.location.href);
    };
    
    that.isMobileUrl = function(url) {
    	return url.indexOf("/mobile/") > -1;
    };
    
    that.setMobileCookie = function() {
    	cookie(cookieName, "mobile");
    };
    
    that.go = function (url, isAuto) {
        if (isAuto) {
            this.setAutoRedirectCookie();
        } else {
            this.setManualRedirectCookie();
        }
        if (this.isMobileUrl(url)) {
        	this.setMobileCookie();
        }
        window.location.href = url;
    };
    
    // Call this when user wants to go back to the original page that redirected him
    // Returns true if it is going back and false if previousUrl is missing and it cannot go back
    that.goBack = function () {
        if (previousUrl) {
            that.go(previousUrl, false);
        }
    };
    
    // Call this when user wants to stay on the current page, even if it is not the optimized one
    that.noRedirect = function () {
        cookie(cookieName, "done");
    };
    
    // ----< Private Methods >---------------------------------------------------------------------
    
    // Initialization method
    function init() {
    	var mobileDetectionEnabled = $("head meta[name='schindler-mobile-detection']").attr("content");
    	if (typeof mobileDetectionEnabled == 'undefined' || mobileDetectionEnabled.length < 1) {
    		return;
    	}
        // In case this is the first page visited
        if (!cookieValue || (cookieValue == "mobile" && !that.isMobileUrl(locationPath))) {
        	var isIpad = navigator.userAgent.match(/.*?iPad.*?/g);
        	if (isIpad){
        		// users with iPad stays where they are
        		that.noRedirect();
        	} else {
	            $.ajax({
	                dataType: "json",
	                url: apiUrl + "?path=" + locationPath,
	                success: function (data) {
	                    if (data.status == "success") {
	                        if (data.needRedirection) {
	                            if (data.isExactMatch) {
	                            	if ((typeof CQ === 'undefined') || !("WCM" in CQ)){
	                                //if ( !CQ && !("WCM" in CQ)) {
	                                    that.go(data.path + "." + locationExtension + locationSuffix + window.location.search, true);
	                                }
	                            } else {
	                                checkSelector(data.deviceGroup);
	                                fireEvent("askAutoRedirect", data.path + "." + locationExtension);
	                            }
	                        } else {
	                            that.noRedirect();
	                            checkSelector(data.deviceGroup);
	                        }
	                    } else if (CQ && ("WCM" in CQ)) {
	                        if (data.message) {
	                            alert("Detect Mobile error: " + data.message);
	                        } else {
	                            alert("Detect Mobile error: invalid response from the JSON API");
	                        }
	                    }
	                }
	            });
        	}
        
        // In case we just have been redirected
        } else if (cookiePreviousUrl) {
            that.noRedirect();
            if (cookieState == "auto") {
                fireEvent("autoRedirectDone", cookiePreviousUrl);
            } else if (cookieState == "manual") {
                fireEvent("manualRedirectDone", cookiePreviousUrl);
            }
        }
    }
    
    function checkSelector(requiredSelector) {
        if (locationSelector && requiredSelector && (locationSelector != requiredSelector)) {
            window.location.href = locationPath + "." + locationExtension + locationSuffix + window.location.search;
        }
    }
    
    function fireEvent(eventName, data) {
        if (typeof eventHandlers[eventName] == "function") {
            $(function () {
                eventHandlers[eventName](data);
            });
        }
    }
    
    // This function acts as getter if value is undefined, as setter if value is
    // defined, and as eraser if value is null.
    // Inspired by PPK's cookie functions: http://www.quirksmode.org/js/cookies.html
    function cookie(name, value) {
        // Get cookie
        if (value === undefined) {
            var nameEQ = name + "=",
                ca = document.cookie.split(";");
            
            for (var i = ca.length; i--;) {
                var c = ca[i];
                while (c.charAt(0) == " ") {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) == 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        
        // Write cookie
        } else {
            document.cookie = name + "=" + (value || "") + "; path=/";
        }
    }
    
    init();
};

/*************************************************************************
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

(function($) {
    
    // This will be called after having been automatically redirected
    function autoRedirectDone (previousUrl) {
        var div = $('#message-detectmobile-auto').slideDown();
        div.find('a.message-detectmobile-link').attr('href', previousUrl).click(detectMobile.goBack);
        div.find('a.message-detectmobile-close').click(function () {
            div.slideUp();
            return false;
        });
    };
    
    // This will be called after having been manually redirected
    function manualRedirectDone(previousUrl) {
        var div = $('#message-detectmobile-manual').slideDown();
        div.find('a.message-detectmobile-link').attr('href', previousUrl).click(detectMobile.goBack);
        div.find('a.message-detectmobile-close').click(function () {
            div.slideUp();
            return false;
        });
    };
    
    // This will be called to ask if the visitor wants to be automatically redirected to
    // This gets called only when the aimed page isn't the same as the current page
    function askAutoRedirect(nextUrl) {
        var div = $('#message-detectmobile-ask').slideDown();
        div.find('a.message-detectmobile-link').attr('href', nextUrl).click(DetectMobile.go);
        div.find('a.message-detectmobile-close').click(function () {
            div.slideUp();
            detectMobile.noRedirect();
            return false;
        });
    };

    var detectMobile = new DetectMobile(
        "/content/detectmobile/_jcr_content.json",
        {
            "autoRedirectDone":   autoRedirectDone,
            "manualRedirectDone": manualRedirectDone,
            "askAutoRedirect":    askAutoRedirect
        }
    );
    
    // Wait until the DOM finished loading
    $(function () {
        // Set the required cookie before the link gets followed
        $('.detectmobile-footer a').click(detectMobile.setManualRedirectCookie);
    });
    
})(jQuery);


var scLink;

Nx.$(document).ready(function() {
	if(typeof s == "undefined") return;
	initScLink();
	intCheck();
	formEvent();
	intSearchResults();
	medialib();
	
});

function initScLink() {
	scLink = Nx.$('<a></a>').attr('href','true').attr('id','scdummy').appendTo('body').click(function(){s.tl(this, 'o')});
}

function scTrack() {
	scLink.click();
}

function medialib() {
	if(Nx.$('body.assetshare').length) {
		Nx.$('.x-form-trigger.x-form-search-trigger').click(function(){
			trkMedLib();
		});
		Nx.$('.x-form-text.x-form-field.fulltextField').keypress(function(e){
			if(e.which == 13) {
				trkMedLib();
			}
		});
	}
}

function trkMedLib() {
	var sTerm = Nx.$('.x-form-text.x-form-field.fulltextField').val();
	var tags="";
	Nx.$('input:checkbox:checked[name^="0_group"]').each(function(){
		tags+=Nx.$(this).val()+";";
	});
	if(sTerm != "") {
		s.prop12 = sTerm;
		s.linkTrackEvents="None";
		s.linkTrackVars = "prop12";
		scTrack();
	}
	if(tags != "") {
		s.prop13 = tags;
		s.linkTrackEvents="None";
		s.linkTrackVars = "prop13";
		scTrack();
	}
	
}

function intSearchResults() {
	if(Nx.$('.modSearchResults').length) {
		var $p = Nx.$('.pagination');
		var totRes = 0;
		if($p.length) {
			totRes = Nx.$('.pagination').data('totalresults');
		}
		s.prop9 = s.prop8 +";"+ totRes;
		s.linkTrackEvents="None";
		s.linkTrackVars="prop9";
		scTrack();
	}
}

function formEvent() {
	var $forms = Nx.$('.modFormular form');
	
	for(var i = 0; i < $forms.length; i++) {
		var $form = Nx.$($forms[i]);
		if($form.attr('id') != "recommendationForm") {
			s.event1 = "formstart";
			s.events = "event1";
			s.linkTrackEvents="event1";
			s.linkTrackVars = "None";
			scTrack();
		}
		$form.bind("submit", function(e) {
			
			s.event2 = "formsend";
			s.events = "event2";
			s.linkTrackEvents="event2";
			s.linkTrackVars = "prop11,eVar4";
			
			var requestType = "";
			//get request type
			if(Nx.$('input[name="sc_requesttype"]', this).length) {
				requestType = Nx.$('input[name="sc_requesttype"]:checked', this).val();
			}
			
			if(Nx.$('select[name="sc_requesttype"]', this).length) {
				requestType = Nx.$('select[name="sc_requesttype"]', this).val();
			}
			
			s.prop11 = requestType;
			s.eVar4 = requestType;
			
			scTrack();
		});
	}
	
	Nx.$('#tellafriend').click(function(){
		s.event1 = "formstart";
		s.events = "event1";
		s.linkTrackEvents="event1";
		s.linkTrackVars = "None";
		scTrack();
	})
}

function intCheck() {
	if(typeof s == "undefined") return;
	
	if(!Nx.$.cookie('int')) {
		var requestUrl = location.pathname.replace('html','analytics.html') + location.search;
		
		Nx.$.ajax({
			url: requestUrl,
			success: function(data){
				setIntCookie(data.internal);
				setIntVars(data.internal);
			},
			error: function() {
				setIntCookie(false);
				setIntVars(false);
			}, 
			dataType: 'json'
		});
	} else {
		setIntVars(Nx.$.cookie('int'));
	}
}

function setIntCookie(state) {
	Nx.$.cookie('int', state, {path: '/'});
}

function setIntVars(state) {
	if(typeof state == "string") {
		state = state == "true" ? true : false;
	}
	var prop = state ? "internal" : "external";
	s.prop18 = prop;
	s.eVar5 = s.prop18;
	s.linkTrackEvents="None";
	s.linkTrackVars="prop18,eVar5";
	scTrack();
}
