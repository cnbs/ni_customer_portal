var slideIndex = 1;
var Caption = "";

function openModal(data, caption, imgName) {
    $('#divphotolinkSlideContent').html('');
    var slidedata = "";
    Caption = caption;
    //ImageName = decodeURIComponent(AllImageName.split(',')[index]);

    slidedata += '<div class="caption-container"><p id="caption">' + caption + ', Document: ' + imgName + '</p></div>';
    $.each(data.split(','), function (index, value) {
        if (value != "") {
            //slidedata += '<div class="caption-container"><p id="caption">' + caption + 'Document: ' + ImageName[index + 1] + '</p></div>';
            slidedata += '<div class="mySlides"><div class="numbertext">' + (index + 1) + ' / ' + (data.split(',').length-1) + '</div><img src="' + value + '" style="width:100%"></div>';
        }
    });
    slidedata += '<a class="prev" onclick="plusSlides(-1)">&#10094;</a><a class="next" onclick="plusSlides(1)">&#10095;</a>';
    $('#divphotolinkSlideContent').append(slidedata);
    document.getElementById('divphotolinkSlide').style.display = "block";
}

function closeModal() {
    document.getElementById('divphotolinkSlide').style.display = "none";
}

//showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  //for (i = 0; i < dots.length; i++) {
  //    dots[i].className = dots[i].className.replace(" active", "");
  //}
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = Caption + ", Document: " + dots[slideIndex - 1].alt;
}