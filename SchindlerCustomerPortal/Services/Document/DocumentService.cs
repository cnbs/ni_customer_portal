﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.Document
{
    public class DocumentService : IDocumentService
    {
        DocumentDAL documentdal = new DocumentDAL();


        public DocumentEntity DocumentInsert(DocumentEntity doc)
        {
            try
            {
                DocumentEntity docds = (from row in documentdal.DocumentInsert(doc)
                                     select new DocumentEntity
                                     {
                                         DocumentName = row["DocumentName"].ToString(),

                                     }).FirstOrDefault();

                return docds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DocumentEntity> GetAllDocument(DataTableModel model, ref int count)
        {
            var docList = new List<DocumentEntity>();
            try
            {
                docList = (from row in documentdal.DocumentSelect(model, ref count)
                            select new DocumentEntity
                            {
                                DocumentID = Convert.ToInt32(row["DocumentId"]),
                                DocumentName = row["DocumentName"].ToString(),
                                Description = row["Description"].ToString(),
                                ExpiredDate = Convert.ToString(row["Expireddate"]),
                                PublishedStatus = row["Published"].ToString(),
                                CompanyName = row["CompanyName"].ToString(),
                                DocumentPath = row["DocumentPath"].ToString()
                                //DocumentPath = row["DocumentPath"].ToString(),
                                //PublishedOn = DBNull.Value.Equals(row["PublishedOn"]) ? (DateTime?)null : Convert.ToDateTime(row["PublishedOn"])

                            }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<DocumentEntity> GetAllDocumentByCompanyId(DataTableModel model, ref int count)
        {
            var docList = new List<DocumentEntity>();
            try
            {
                docList = (from row in documentdal.DocumentSelectByCompany(model, ref count)
                           select new DocumentEntity
                           {
                               DocumentID = Convert.ToInt32(row["DocumentId"]),
                               DocumentName = row["DocumentName"].ToString(),
                               Description = row["Description"].ToString(),
                               ExpiredDate = Convert.ToString(row["Expireddate"]),
                               PublishedStatus = row["Published"].ToString(),
                               DocumentPath = row["DocumentPath"].ToString()
                               //DocumentPath = row["DocumentPath"].ToString(),
                               //PublishedOn = DBNull.Value.Equals(row["PublishedOn"]) ? (DateTime?)null : Convert.ToDateTime(row["PublishedOn"])

                           }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DocumentEntity GetDocById(int DocId)
        {
            DocumentEntity doc = new DocumentEntity();
            doc = (from row in documentdal.DocumentSelectById(DocId)
                    select new DocumentEntity
                    {
                        DocumentID = Convert.ToInt32(row["DocumentID"]),
                        DocumentName = row["DocumentName"].ToString(),
                        DocumentTypeID = Convert.ToInt32(row["DocumentTypeID"]),
                        Description = row["Description"].ToString(),
                        ExpiredDate = Convert.ToString(row["Expireddate"]),
                        StatusId = Convert.ToInt32(row["StatusId"]),
                        CompanyName = row["CompanyName"].ToString(),
                        ProjectName = row["ProjectName"].ToString(),
                        JobName = row["JobName"].ToString(),
                        IsPublished = Convert.ToBoolean(row["IsPublished"]),
                        DocumentPath = row["DocumentPath"].ToString()
                    }).FirstOrDefault();
            return doc;
        }

        public DocumentEntity DocumentUpdateService(DocumentEntity doc)
        {
            try
            {
                DocumentEntity docds = (from row in documentdal.DocumentUpdate(doc)
                                        select new DocumentEntity
                                        {
                                            DocumentName = row["DocumentName"].ToString(),

                                        }).FirstOrDefault();

                return docds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDocumentService(DocumentEntity doc)
        {
            var result = documentdal.DeleteDocument(doc);
            return result;
        }

        public List<DocumentPermission> GetUserDocumentPermission(int docid)
        {
            List<DocumentPermission> UserPermissionlst = new List<DocumentPermission>();
            UserPermissionlst = (from row in documentdal.GetDocumentPermissionByUser(docid)
                                 select new DocumentPermission
                                 {

                                     UserID = Convert.ToInt32(row["UserID"]),
                                     DocumentID = Convert.ToInt32(row["DocumentID"]),
                                     UserName = row["UserName"].ToString(),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasUploadPermission = Convert.ToBoolean(row["Upload_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"])


                                 }).ToList();

            return UserPermissionlst;

        }

        public int InsertUpdateUserDocumentAccessPermissionByModule(UserPermissionDocumentModel obj)
        {
            return documentdal.InsertUpdateUserDocumentAccessPermissionByModule(obj);
        }

        public bool UserDocumentAccessService_UpdateAll(UserPermissionDocumentMulti obj)
        {
            return documentdal.UserDocumentAccess_UpdateAll(obj);
        }

        public List<DocumentEntity> GetAllDocumentByType(DataTableModel model, ref int count)
        {
            var docList = new List<DocumentEntity>();
            try
            {
                docList = (from row in documentdal.DocumentSelectByType(model, ref count)
                           select new DocumentEntity
                           {
                               DocumentID = Convert.ToInt32(row["DocumentId"]),
                               DocumentName = row["DocumentName"].ToString(),
                               Description = row["Description"].ToString(),
                               ExpiredDate = Convert.ToString(row["Expireddate"]),
                               PublishedStatus = row["Published"].ToString(),
                               CompanyName = row["CompanyName"].ToString(),
                               DocumentPath = row["DocumentPath"].ToString()
                               

                           }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DocumentEntity> GetAllDocumentBycompany_ByType(DataTableModel model, ref int count)
        {
            var docList = new List<DocumentEntity>();
            try
            {
                docList = (from row in documentdal.DocumentSelectByCompanyId_ByProjectId_ByType(model, ref count)
                           select new DocumentEntity
                           {
                               DocumentID = Convert.ToInt32(row["DocumentID"]),
                               DocumentName = row["DocumentName"].ToString(),
                               Description = row["Description"].ToString(),
                               ExpiredDate = Convert.ToString(row["Expireddate"]),
                               PublishedStatus = row["Published"].ToString(),
                               CompanyName = row["CompanyName"].ToString(),
                               DocumentPath = row["DocumentPath"].ToString()


                           }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}