﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Document
{
    public interface IDocumentService
    {
        DocumentEntity DocumentInsert(DocumentEntity doc);

        List<DocumentEntity> GetAllDocument(DataTableModel model, ref int count);

        DocumentEntity GetDocById(int DocId);

        DocumentEntity DocumentUpdateService(DocumentEntity doc);
        bool DeleteDocumentService(DocumentEntity doc);

        List<DocumentPermission> GetUserDocumentPermission(int docid);

        int InsertUpdateUserDocumentAccessPermissionByModule(UserPermissionDocumentModel obj);
        bool UserDocumentAccessService_UpdateAll(UserPermissionDocumentMulti obj);

        List<DocumentEntity> GetAllDocumentByCompanyId(DataTableModel model, ref int count);
        List<DocumentEntity> GetAllDocumentByType(DataTableModel model, ref int count);

        List<DocumentEntity> GetAllDocumentBycompany_ByType(DataTableModel model, ref int count);
    }
}
