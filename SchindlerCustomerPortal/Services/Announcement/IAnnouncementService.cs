﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Announcement
{
   public interface IAnnouncementService
    {
        List<AnnouncementEntity> GetAnnouncement(DataTableModel model, ref int count);
        AnnouncementEntity InsertAnnouncement(AnnouncementEntity data);
        AnnouncementEntity GetAnnouncementbyID(int AnnouncementId);
        AnnouncementEntity UpdateAnnouncement(AnnouncementEntity data);

        bool DeleteAnnouncement(AnnouncementEntity data);

        List<AnnouncementEntity> GetAllAnnouncement(string DisplayOn);
    }
}
