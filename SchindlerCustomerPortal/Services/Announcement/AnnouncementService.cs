﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.DAL;
using System.Text.RegularExpressions;

namespace SchindlerCustomerPortal.Services.Announcement
{
    public class AnnouncementService : IAnnouncementService
    {
        AnnouncementDal Announcementdal = new AnnouncementDal();
        public bool DeleteAnnouncement(AnnouncementEntity data)
        {
            var result = Announcementdal.DeleteAnnouncement(data);
            return result;
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public List<AnnouncementEntity> GetAnnouncement(DataTableModel model, ref int count)
        {
            var announceList = new List<AnnouncementEntity>();
            try
            {
                announceList = (from row in Announcementdal.AnnouncementSelect(model, ref count)
                            select new AnnouncementEntity
                            {
                                AnnMsgId = Convert.ToInt32(row["AnnMsgId"]),
                                MessageText = StripHTML((Convert.ToString(row["MessageText"]))),
                                DisplayOn = Convert.ToString(row["DisplayOn"]),
                                Frequency = Convert.ToString(row["Frequency"]),
                                ValidDate = Convert.ToDateTime(row["ValidDate"]).ToString("MM/dd/yyyy")

                            }).ToList();

                return announceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AnnouncementEntity GetAnnouncementbyID(int AnnouncementId)
        {
            AnnouncementEntity announcement = new AnnouncementEntity();
            announcement = (from row in Announcementdal.AnnouncementSelectById(AnnouncementId)
                    select new AnnouncementEntity
                    {
                        AnnMsgId = Convert.ToInt32(row["AnnMsgId"]),
                        MessageText = Convert.ToString(row["MessageText"]),
                        DisplayOn = Convert.ToString(row["DisplayOn"]),
                        Frequency = Convert.ToString(row["Frequency"]),
                        ValidDate = Convert.ToDateTime(row["ValidDate"]).ToString("MM/dd/yyyy")

                    }).FirstOrDefault();
            return announcement;
        }

        public AnnouncementEntity InsertAnnouncement(AnnouncementEntity data)
        {
            try
            {
                AnnouncementEntity announceds = (from row in Announcementdal.InsertAnnouncement(data)
                                     select new AnnouncementEntity
                                     {
                                         Title = row["Data"].ToString(),

                                     }).FirstOrDefault();

                return announceds;
                //return helpdal.HelpInsert(help);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public AnnouncementEntity UpdateAnnouncement(AnnouncementEntity data)
        {
            try
            {
                AnnouncementEntity announceds = (from row in Announcementdal.UpdateAnnouncement(data)
                                                 select new AnnouncementEntity
                                                 {
                                                     Title = row["Data"].ToString(),

                                                 }).FirstOrDefault();

                return announceds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<AnnouncementEntity> GetAllAnnouncement(string DisplayOn)
        {
            var announceList = new List<AnnouncementEntity>();
            try
            {
                announceList = (from row in Announcementdal.AllAnnouncementSelect(DisplayOn)
                                select new AnnouncementEntity
                                {
                                    AnnMsgId = Convert.ToInt32(row["AnnMsgId"]),
                                    MessageText = Convert.ToString(row["MessageText"]),
                                    DisplayOn = Convert.ToString(row["DisplayOn"]),
                                    Frequency = Convert.ToString(row["Frequency"]),
                                    ValidDate = Convert.ToDateTime(row["ValidDate"]).ToString("MM/dd/yyyy")

                                }).ToList();

                return announceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}