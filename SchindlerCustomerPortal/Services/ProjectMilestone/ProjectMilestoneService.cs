﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.ProjectMilestone
{
    public class ProjectMilestoneService : IProjectMilestoneService
    {
        ProjectMilestoneDAL milestonedal = new ProjectMilestoneDAL();
        ProjectDAL projectDal = new ProjectDAL();

        public ProjectMilestoneEntity MilestoneInsert(ProjectMilestoneEntity res)
        {
            try
            {
                ProjectMilestoneEntity obj = new ProjectMilestoneEntity();
                obj = (from row in milestonedal.MilestoneInsertdata(res)
                       select new ProjectMilestoneEntity
                       {
                           Exist = row["Exist"].ToString(),

                       }).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ProjectMilestoneEntity> GetAllMilestone(DataTableModel model, ref int count)
        {
            var docList = new List<ProjectMilestoneEntity>();
            try
            {
                docList = (from row in milestonedal.MilestoneSelect(model, ref count)
                           select new ProjectMilestoneEntity
                           {
                               MilestoneId = Convert.ToInt32(row["Milestoneid"]),
                               MilestoneName = setMilestoneNewName(row["MilestoneTitle"].ToString()),
                               PredecessorName = row["Predecessor"].ToString(),
                               SuccessorName = row["Successor"].ToString(),
                               DueDate = DBNull.Value.Equals(row["DueDate"]) ? (DateTime?)null : Convert.ToDateTime(row["DueDate"]),
                               StatusName = row["StatusName"].ToString(),
                               WaitingOn = row["WaitingOn"].ToString(),
                               Note = Convert.ToString(row["Note"]),
                               CompletedDate = DBNull.Value.Equals(row["CompletedDate"]) ? (DateTime?)null : Convert.ToDateTime(row["CompletedDate"])

                           }).Where( a => a.MilestoneName.ToLower() != "material ordered").ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string setMilestoneNewName(string oldMilestoneName)
        {
            string newMilestonename = oldMilestoneName;
            if(oldMilestoneName.ToLower() == "final inspection")
            {
                newMilestonename = "Equipment Turnover";
            }
            else if (oldMilestoneName.ToLower() == "material at hub")
            {
                newMilestonename = "Material in Storage Hub";
            }
            return newMilestonename;
        }


        public ProjectMilestoneEntity GetMileById(int MileId)
        {
            ProjectMilestoneEntity mile = new ProjectMilestoneEntity();
            mile = (from row in milestonedal.MilestoneSelectById(MileId)
                    select new ProjectMilestoneEntity
                    {
                        MilestoneId = Convert.ToInt32(row["Milestoneid"]),
                        MilestoneTitle = Convert.ToInt32(row["MilestoneTitle"]),
                        Predecessor = Convert.ToInt32(row["Predecessor"]),
                        Successor = Convert.ToInt32(row["Successor"]),
                        DueDateStr = row["DueDate"].ToString(),
                        CompletedDateStr = row["CompletedDate"].ToString(),
                        Status = Convert.ToInt32(row["Status"]),
                        WaitingOn = row["WaitingOn"].ToString(),
                        Note = Convert.ToString(row["Note"]) == "" ? string.Empty : Convert.ToString(row["Note"]).Replace("\\r\\n", "\r\n"),
                        MilestoneName = row["MilestoneName"].ToString()
                    }).FirstOrDefault();
            return mile;
        }

        public ProjectMilestoneEntity MilestoneUpdateService(ProjectMilestoneEntity milestone)
        {
            ProjectMilestoneEntity obj = new ProjectMilestoneEntity();
            try
            {
                obj = (from row in milestonedal.MilestoneUpdate(milestone)
                       select new ProjectMilestoneEntity
                       {
                           Exist = row["Exist"].ToString(),

                       }).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool DeleteMilestoneService(ProjectMilestoneEntity doc)
        {
            var result = milestonedal.DeleteMilestone(doc);
            return result;
        }


        public List<ReadyToPullTemplateMaster> GetAllReadyToPullTemplateMasterList(DataTableModel model)
        {
            List<ReadyToPullTemplateMaster> projectJobList = new List<ReadyToPullTemplateMaster>();
            projectJobList = (from row in milestonedal.GetAllReadyToPullTemplateMasterList(model)
                              select new ReadyToPullTemplateMaster
                              {
                                  JobNo = Convert.ToString(row["JobNo"]),
                                  TemplateId = Convert.ToInt32(row["TemplateId"]),
                                  TemplateName = Convert.ToString(row["TemplateName"]),
                                  ProjectNo = Convert.ToString(row["ProjectNo"]),
                                  Comment = Convert.ToString(row["Comment"]),
                                  Attachment = CommonConfiguration.readyToPullAttachmentsPath + Convert.ToString(row["Attachment"]),
                                  RealAttachment = Convert.ToString(row["RealAttachment"]),
                                  Selected = (Convert.ToString(row["ProjectNo"]) == "" ? false : true)

                              }).ToList();

            return projectJobList;
        }

        public bool AddReadyToPullTemplateDetails(List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails, int ProjectNo, string JobNo)
        {
            try
            {
                return milestonedal.AddMilestoneReadyToPullTemplateDetails(lstReadyToPullTemplateDetails, ProjectNo, JobNo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //ReadyToPullTemplateDetails AddReadyToPullTemplateDetails(ReadyToPullTemplateDetails doc);

        public List<UserEntity> GetProjectAssignedUserListForMilestoneEmail(string projectNo)
        {
            List<UserEntity> user = new List<UserEntity>();
            user = (from row in milestonedal.GetProjectAssignedUserListForMilestoneEmail(projectNo)
                    select new UserEntity
                    {
                        Email = Convert.ToString(row["Email"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                        DeviceID = Convert.ToString(row["DeviceId"]),
                        DeviceType = Convert.ToString(row["DeviceType"]),
                        DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"])
                    }).ToList();
            return user;
        }

        public string GetProjectandJobDetail(string projectNo, string JobNo)
        {
            return projectDal.GetProjectData(projectNo, JobNo);
        }
    }
}