﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.ProjectMilestone
{
    public interface IProjectMilestoneService
    {
        ProjectMilestoneEntity MilestoneInsert(ProjectMilestoneEntity res);
        List<ProjectMilestoneEntity> GetAllMilestone(DataTableModel model, ref int count);
        ProjectMilestoneEntity GetMileById(int MileId);
        ProjectMilestoneEntity MilestoneUpdateService(ProjectMilestoneEntity milestone);
        bool DeleteMilestoneService(ProjectMilestoneEntity doc);

        List<ReadyToPullTemplateMaster> GetAllReadyToPullTemplateMasterList(DataTableModel model);
        bool AddReadyToPullTemplateDetails(List<ReadyToPullTemplateDetails> lstReadyToPullTemplateDetails,int ProjectNo,string JobNo);

        List<UserEntity> GetProjectAssignedUserListForMilestoneEmail(string projectNo);
        string GetProjectandJobDetail(string projectNo, string JobNo);

    }
}
