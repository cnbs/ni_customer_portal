﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.MailBox
{
    public interface IMailBoxService
    {
        List<MailBoxEntity> SendMail(MailBoxEntity mail);

        List<UserEntity> GetAllUserByCompanyId(int companyId, string searchTerm);

        List<MailBoxEntity> GetAllUserMailByUserMail(DataTableModel model, ref int count);

        MailBoxEntity GetUserMailByMailId(int mailId, int appAuditID, string ApplicationOperation, string SessionID, int UserId);

        MailBoxEntity GetMailCountByUserMail(string email);

        bool UpdateUserMailStatus(string mailID, string viewStatus, int status, int appAuditID, string ApplicationOperation, string SessionID, int UserId);

        bool UserMailLogicalDelete(string mailID, int appAuditID, string sessionID, int UserId, string ApplicationOperation);

        List<MailBoxEntity> GetAllUserMailforHeadermenu(string email);

        List<MailBoxEntity> GetAllUserMailforDashboardWidget(string email);

        int SaveDraft(MailBoxEntity mail);

    }
}