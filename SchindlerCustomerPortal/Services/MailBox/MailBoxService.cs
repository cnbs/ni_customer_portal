﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.MailBox
{
    public class MailBoxService : IMailBoxService
    {
        MailBoxDAL mailBox = new MailBoxDAL();
        public List<MailBoxEntity> SendMail(MailBoxEntity mail)
        {
            var userList = new List<MailBoxEntity>();
            //var result = mailBox.SendNewMail(mail);
            //return result;

            try
            {
                var dataList = mailBox.SendNewMail(mail);
                if (dataList != null && dataList[0].ItemArray[0].ToString() == "-1")
                {
                    return userList;
                }
                else
                {
                    userList = (from row in dataList
                                select new MailBoxEntity
                                {
                                    MailId = Convert.ToInt32(row["MailId"]),
                                    //CompanyId = Convert.ToInt32(row["CompanyId"]),
                                    //Sender = row["SenderId"].ToString(),
                                    Receiver = row["ReceiverId"].ToString(),
                                    //Subject = row["Subject"].ToString(),
                                    //Message = row["Message"].ToString(),
                                    //ViewStatus = row["View_Status"].ToString(),
                                    //HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                    //Status = Convert.ToInt32(row["Status"]),
                                    //DateSent = row["DateSent"].ToString(),
                                    //IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                    ToList = Convert.ToString(row["Tolist"]),
                                    CC = Convert.ToString(row["CC"]),
                                    BCC = Convert.ToString(row["BCC"]),
                                    UserName = Convert.ToString(row["UserName"]),
                                    DeviceID = Convert.ToString(row["DeviceId"]),
                                    DeviceType = Convert.ToString(row["DeviceType"]),
                                    DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"])
                                }).ToList();
                }
                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<UserEntity> GetAllUserByCompanyId(int companyId, string searchTerm)
        {
            var userList = new List<UserEntity>();
            try
            {
                userList = (from row in mailBox.GetUserByCompanyIdForMailBox(companyId, searchTerm)
                            select new UserEntity
                            {

                                Email = row["Email"].ToString(),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = row["FirstName"].ToString(),
                                LastName = row["LastName"].ToString(),
                                //CompanyID = Convert.ToInt32(row["CompanyID"]),
                                //CompanyName = row["CompanyName"].ToString(),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                Designation = row["DesignationName"].ToString(),
                                DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MailBoxEntity> GetAllUserMailByUserMail(DataTableModel model, ref int count)
        {
            var userList = new List<MailBoxEntity>();
            try
            {
                userList = (from row in mailBox.GetUserMailByEmail(model, ref count)
                            select new MailBoxEntity
                            {

                                MailId = Convert.ToInt32(row["MailId"]),
                                CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                Message = row["Message"].ToString(),
                                ViewStatus = row["View_Status"].ToString(),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                Status = Convert.ToInt32(row["Status"]),
                                DateSent = row["DateSent"].ToString(),
                                IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                //Designation = row["DesignationName"].ToString(),
                                //DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MailBoxEntity GetUserMailByMailId(int mailId, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {
            var userList = new MailBoxEntity();
            try
            {
                userList = (from row in mailBox.GetUserMailByMailId(mailId, appAuditID, ApplicationOperation, SessionID, UserId)
                            select new MailBoxEntity
                            {

                                MailId = Convert.ToInt32(row["MailId"]),
                                CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                Message = row["Message"].ToString(),
                                ViewStatus = row["View_Status"].ToString(),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                Status = Convert.ToInt32(row["Status"]),
                                DateSent = row["DateSent"].ToString(),
                                IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                ToList = Convert.ToString(row["Tolist"]),
                                CC = Convert.ToString(row["CC"]),
                                BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                AttachmentPath = Convert.ToString(row["AttachmentPath"]),
                                ProfilePic = CommonConfiguration.userProfilePicPath + Convert.ToString(row["ProfilePic"]),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                //Designation = row["DesignationName"].ToString(),
                                //DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).FirstOrDefault();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MailBoxEntity GetMailCountByUserMail(string email)
        {
            var userList = new MailBoxEntity();
            try
            {
                userList = (from row in mailBox.GetUserMailCount(email)
                            select new MailBoxEntity
                            {

                                InboxUnreadCount = Convert.ToInt32(row["InboxUnreadCount"]),
                                DraftCount = Convert.ToInt32(row["DraftCount"]),

                            }).FirstOrDefault();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateUserMailStatus(string mailID, string viewStatus, int status, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {
            var result = mailBox.UpdateStatusOfUserMail(mailID, viewStatus, status, appAuditID, ApplicationOperation, SessionID, UserId);
            return result;
        }

        public bool UserMailLogicalDelete(string mailID, int appAuditID, string sessionID, int UserId, string ApplicationOperation)
        {
            var result = mailBox.GetUserMailLogicalDelete(mailID, appAuditID, sessionID, UserId, ApplicationOperation);
            return result;
        }

        public List<MailBoxEntity> GetAllUserMailforHeadermenu(string email)
        {
            var userList = new List<MailBoxEntity>();
            try
            {
                userList = (from row in mailBox.GetUserMailforHeaderMenu(email)
                            select new MailBoxEntity
                            {

                                MailId = Convert.ToInt32(row["MailId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                UserName = Convert.ToString(row["UserName"]),
                                ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MailBoxEntity> GetAllUserMailforDashboardWidget(string email)
        {
            var userList = new List<MailBoxEntity>();
            try
            {
                userList = (from row in mailBox.GetUserMailforDashboardWidget(email)
                            select new MailBoxEntity
                            {

                                MailId = Convert.ToInt32(row["MailId"]),
                                //CompanyId = Convert.ToInt32(row["CompanyId"]),
                                Sender = row["SenderId"].ToString(),
                                Receiver = row["ReceiverId"].ToString(),
                                Subject = row["Subject"].ToString(),
                                //InboxUnreadCount = Convert.ToInt32(row["InboxUnreadCount"]),
                                //Message = row["Message"].ToString(),
                                ViewStatus = Convert.ToString(row["View_Status"]),
                                HasAttachment = Convert.ToBoolean(row["HasAttachment"]),
                                //Status = Convert.ToInt32(row["Status"]),
                                //DateSent = row["DateSent"].ToString(),
                                //IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                                //ToList = Convert.ToString(row["Tolist"]),
                                //CC = Convert.ToString(row["CC"]),
                                //BCC = Convert.ToString(row["BCC"]),
                                UserName = Convert.ToString(row["UserName"]),
                                //ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                //IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                //AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                //Designation = row["DesignationName"].ToString(),
                                //DesignationId = Convert.ToInt32(row["DesignationId"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),
                                DateSent = Convert.ToString(row["DateSent"]),
                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveDraft(MailBoxEntity mail)
        {
            return mailBox.SaveDraftMail(mail);
        }
    }
}