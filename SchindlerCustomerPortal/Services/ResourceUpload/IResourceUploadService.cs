﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.ResourceUpload
{
    public interface IResourceUploadService
    {
        List<ResourceUploadEntity> GetAllResources(DataTableModel model,ref  int count);

        bool ResourceUploadInsertService(ResourceUploadEntity res);

        ResourceUploadEntity GetREsById(int ResId);

        bool ResourceUpdateService(ResourceUploadEntity res);

        bool DeleteResource(ResourceUploadEntity doc);
    }
}
