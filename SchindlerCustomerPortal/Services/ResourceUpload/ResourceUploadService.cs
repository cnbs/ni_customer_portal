﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Resource;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.ResourceUpload
{
    public class ResourceUploadService : IResourceUploadService
    {
        ResourceUploadDAL resourcedal = new ResourceUploadDAL();

        public List<ResourceUploadEntity> GetAllResources(DataTableModel model,ref  int count)
        {
            var docList = new List<ResourceUploadEntity>();
            try
            {
                docList = (from row in resourcedal.ResourceSelect(model, ref count)
                           select new ResourceUploadEntity
                           {
                               ResourceId = Convert.ToInt32(row["ResourceID"]),
                               ResourceName = row["ResourceName"].ToString(),
                               ProductName = row["ProductName"].ToString(),
                               DocumentType = row["DocumentType"].ToString(),
                               //ExpiredDate = DBNull.Value.Equals(row["Expireddate"]) ? (DateTime?)null : Convert.ToDateTime(row["Expireddate"]),
                               CreatedBy = row["CreatedBy"].ToString(),
                               UpdatedBy = row["UpdatedBy"].ToString(),
                               UploadDate = Convert.ToDateTime(row["CreatedDate"]),
                               PublishedStatus= row["Published"].ToString(),
                               URLName= row["URLName"].ToString(),
                               DocumentPath = row["DocumentPath"].ToString(),
                               TitleImage = row["TitleImage"].ToString()
                               //DocumentPath = row["DocumentPath"].ToString(),
                               //PublishedOn = DBNull.Value.Equals(row["PublishedOn"]) ? (DateTime?)null : Convert.ToDateTime(row["PublishedOn"])

                           }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResourceUploadInsertService(ResourceUploadEntity res)
        {
            try
            {
                bool resds = resourcedal.ResourceUploadInsert(res);
                    
                return resds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ResourceUploadEntity GetREsById(int ResId)
        {
            ResourceUploadEntity doc = new ResourceUploadEntity();
            doc = (from row in resourcedal.ResourceSelectById(ResId)
                   select new ResourceUploadEntity
                   {
                       ResourceId = Convert.ToInt32(row["ResourceID"]),
                       ResourceName = row["ResourceName"].ToString(),
                       ProductName = row["ProductName"].ToString(),
                       DocumentTypeId = Convert.ToInt32(row["DocumentTypeID"]),
                       IsPublished= Convert.ToBoolean(row["IsPublished"]),
                       ResourceType = row["ResourceType"].ToString(),
                       URLName = row["URLName"].ToString(),
                       DocumentPath = row["DocumentPath"].ToString(),
                       //TitleImage= CommonConfiguration.userProfilePicPath + "/" + row["TitleImage"].ToString()
                       TitleImage = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["TitleImage"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["TitleImage"].ToString() : CommonConfiguration.userProfilePicPath + row["TitleImage"].ToString(),

                   }).FirstOrDefault();
            return doc;
        }

        public bool ResourceUpdateService(ResourceUploadEntity res)
        {
            try
            {
                bool resds = resourcedal.ResourceUpdate(res);
                
                return resds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool DeleteResource(ResourceUploadEntity doc)
        {
            var result = resourcedal.DeleteResource(doc);
            return result;
        }
    }
}