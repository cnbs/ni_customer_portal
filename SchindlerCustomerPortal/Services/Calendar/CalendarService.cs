﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SchindlerCustomerPortal.Services.Calendar
{
    public class CalendarService : ICalendarService
    {
        CalendarDAL calendar = new CalendarDAL();

        public List<CalendarEventEntity> GetAllUserCalendarData(string email)
        {
            var EventList = new List<CalendarEventEntity>();
            try
            {
                EventList = (from row in calendar.GetUserCalendarEventHeaderMenu(email)
                             select new CalendarEventEntity
                             {
                                 EventId = Convert.ToInt32(row["CalTaskMstId"]),
                                 Sender = row["SenderName"].ToString(),
                                 Receiver = row["ReceiverName"].ToString(),
                                 Subject = row["EmailSubject"].ToString(),
                                 UserID = Convert.ToInt32(row["SenderId"]),
                                 ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                             }).ToList();

                return EventList;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - GetAllUserCalendarData");
                throw ex;
            }
        }
    }
}