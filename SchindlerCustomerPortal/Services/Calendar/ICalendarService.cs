﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Calendar
{
    public interface ICalendarService
    {
        List<CalendarEventEntity> GetAllUserCalendarData(string email);
    }
}
