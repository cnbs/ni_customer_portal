﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using Elmah;

namespace LiteratureApplication.Services
{
    /// <summary>
    /// Summary description for LoggerService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LoggerService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        [WebMethod]
        public void ErrorLogData(string errorMessage, string errorUrl, string errorLine)
        {
            try
            {
                Exception ex = new Exception(errorMessage);
                ex.Source = errorUrl + errorLine;
                throw ex;
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(
                  HttpContext.Current).Log(new Elmah.Error(ex, HttpContext.Current));
            }

        }
    }
}
