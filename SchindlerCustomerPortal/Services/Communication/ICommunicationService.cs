﻿using SchindlerCustomerPortal.Models;
using System.Collections.Generic;

namespace SchindlerCustomerPortal.Services.Communication
{
    public interface ICommunicationService
    {

        CommunicationEntity AddProjectCommunication(CommunicationEntity communication);

        List<CommunicationEntity> GetAllCommunication(int ProjectNo, string JobNo, int appAuditID, string ApplicationOperation, string SessionID,int UserId);

        List<PostEntity> GetAllPostByProjectId(PostEntity obj);
        //List<PostDetailViewModel> GetPostDetailByPostId(CommunicationThreadSearchInfo obj);
        int AddThreadEntity(ThreadEntity obj);
        ThreadEntity GetSingleThreadById(int threadid);
        int AddPostByProject(PostEntity obj);
        List<PostDetailViewModel> GetAllPost_ThreadByProjectId(PostEntity obj);


        #region NotUsed
        PostEntity GetPostById(int postid);
        
        #endregion

    }
}
