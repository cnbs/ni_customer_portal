﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.Communication
{
    public class CommunicationService : ICommunicationService
    {
        CommunicationDAL communicationDAL = new CommunicationDAL();
        public CommunicationEntity AddProjectCommunication(CommunicationEntity communication)
        {
            CommunicationEntity obj = new CommunicationEntity();
            obj = (from row in communicationDAL.AddProjectCommunication(communication)
                   select new CommunicationEntity
                   {
                       CommunicationId = Convert.ToInt32(row["CommunicationId"]),
                       //ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                       //JobNo = Convert.ToString(row["JobNo"]),
                       //UserId = Convert.ToInt32(row["UserId"]),
                       //UserName = Convert.ToString(row["UserName"]),
                       //ProfilePic = CommonConfiguration.userProfilePicPath + Convert.ToString(row["ProfilePic"]),
                       //Message = Convert.ToString(row["MessageText"]),
                       //NotifyToEmail = Convert.ToString(row["NotifiedTo"]),
                       //AttachmentPath = Convert.ToString(row["AttachmentPath"]),
                       //CreatedBy = Convert.ToString(row["CreatedBy"]),
                       //CreatedOn = Convert.ToString(row["CreatedOn"]),
                       //IsDeleted = Convert.ToBoolean(row["IsDeleted"])
                   }).FirstOrDefault();
            return obj;
        }

        public List<CommunicationEntity> GetAllCommunication(int ProjectNo, string JobNo, int appAuditID, string ApplicationOperation, string SessionID, int UserId)
        {
            List<CommunicationEntity> obj = new List<CommunicationEntity>();
            obj = (from row in communicationDAL.GetAllCommunicationList(ProjectNo, JobNo,  appAuditID,  ApplicationOperation,  SessionID, UserId)
                   select new CommunicationEntity
                   {
                       CommunicationId = Convert.ToInt32(row["CommunicationId"]),
                       ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                       JobNo = Convert.ToString(row["JobNo"]),
                       UserId = Convert.ToInt32(row["UserId"]),
                       UserName = Convert.ToString(row["UserName"]),
                       ProfilePic = CommonConfiguration.userProfilePicPath + Convert.ToString(row["ProfilePic"]),
                       Message = Convert.ToString(row["MessageText"]),
                       NotifyToEmail = Convert.ToString(row["NotifiedTo"]),
                       AttachmentPath = Convert.ToString(row["AttachmentPath"]),
                       //CreatedBy = Convert.ToString(row["CreatedBy"]),
                       CreatedOn = Convert.ToString(row["CreatedDate"]),
                       IsDeleted = Convert.ToBoolean(row["IsDeleted"])
                   }).ToList();
            return obj;
        }

        #region Post

        public List<PostEntity> GetAllPostByProjectId(PostEntity obj)
        {

            return (from row in communicationDAL.GetPostlistByProjectId(obj.ProjectId)
                    select new PostEntity
                    {
                        PostId = Convert.ToInt32(row["PostId"]),
                        CreatedBy = row["CreatedBy"].ToString(),
                        CreatedOn = Convert.ToDateTime(row["CreatedOn"]),
                        IsActive = Convert.ToBoolean(row["IsActive"]),
                        IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                        ParentPostId = Convert.ToInt32(row["ParentPostId"]),
                        PostContent = row["PostContent"].ToString(),
                        PostTitle = row["PostTitle"].ToString(),
                        PostType = row["PostType"].ToString(),
                        PostTypeId = Convert.ToInt32(row["PostType"].ToString()),
                        ProjectId = Convert.ToInt32(row["ProjectId"].ToString()),
                        UpdatedBy = row["UpdatedBy"].ToString(),
                        UpdatedOn = Convert.ToDateTime(row["UpdatedOn"].ToString())
                    }).ToList();

        }

        public PostEntity GetPostById(int postid)
        {
            PostEntity obj = new PostEntity();
            obj = (from row in communicationDAL.GetPostById(postid)
                   select new PostEntity
                   {
                       PostId = Convert.ToInt32(row["PostId"]),
                       CreatedBy =Convert.ToString(row["CreatedBy"]),
                       CreatedOn = Convert.ToDateTime(row["CreatedOn"]),
                       IsActive = Convert.ToBoolean(row["IsActive"]),
                       IsDeleted = Convert.ToBoolean(row["IsDeleted"]),
                       ParentPostId = Convert.ToInt32(row["ParentPostId"]),
                       PostContent =Convert.ToString(row["PostContent"]),
                       PostTitle = Convert.ToString(row["PostTitle"]),
                       PostType = Convert.ToString(row["PostType"]),
                       PostTypeId = Convert.ToInt32(row["PostTypeId"]),
                       ProjectId = Convert.ToInt32(row["ProjectId"]),
                      // UpdatedBy =Convert.ToString(row["UpdatedBy"].ToString())
                      // UpdatedOn = Convert.ToDateTime(row["UpdatedOn"]==null?"":.ToString())
                   }).FirstOrDefault();
            return obj;

        }


        public int AddThreadEntity(ThreadEntity obj)
        {
            return communicationDAL.InsertCommunicationThread(obj);
        }

        //public List<PostDetailViewModel> GetPostDetailByPostId(CommunicationThreadSearchInfo objp)
        //{
        //    List<PostDetailViewModel> obj = new List<PostDetailViewModel>();
        //    obj = (from row in base.GetSinglePostByIdForPostDetail(objp)
        //           select new PostDetailViewModel
        //           {
        //               PostId = Convert.ToInt32(row["PostId"]),
        //               CreatedBy = row["CreatedBy"].ToString(),
        //               CreatedOn = Convert.ToDateTime(row["CreatedOn"]),
        //               IsActive = Convert.ToBoolean(row["IsActive"]),
        //               PostContent = row["PostContent"].ToString(),
        //               PostTitle = row["PostTitle"].ToString(),
        //               Content = row["Content"].ToString(),
        //               NotifyToEmail = row["NotifyToEmail"].ToString(),
        //               ThreadId = Convert.ToInt32(row["ThreadId"].ToString()),
        //               ThreadType = row["ThreadType"].ToString(),
        //               Title = row["Title"].ToString(),
        //               ParentThreadId =Convert.ToInt32(row["ParentThreadId"])
        //           }).ToList();
        //    return obj;

        //}


        public ThreadEntity GetSingleThreadById(int threadid)
        {
            ThreadEntity obj = new ThreadEntity();
            obj = (from row in communicationDAL.GetSingleThreadById(threadid)
                   select new ThreadEntity
                   {
                       strNotifyToEmail =Convert.ToString(row["NotifyToEmail"]),
                       Content = Convert.ToString(row["NotifyToEmail"]),
                       ThreadId = Convert.ToInt32(row["ThreadId"]),
                       ParentThreadId = Convert.ToInt32(row["ParentThreadId"]),
                       PostId = Convert.ToInt32(row["PostId"]),
                       ThreadType = Convert.ToInt32(row["ThreadType"]),
                       Title = Convert.ToString(row["Title"]),
                       PostBy = Convert.ToInt32(row["PostId"]),
                       PostByEmail = Convert.ToString(row["PostByEmail"]),
                       IsReply = Convert.ToBoolean(row["IsReply"]),
                       CreatedBy =Convert.ToString(row["CreatedBy"]),
                       CreatedOn = Convert.ToDateTime(row["CreatedOn"]),
                       IsActive = Convert.ToBoolean(row["IsActive"]),
                       IsDeleted = Convert.ToBoolean(row["IsDeleted"])                      
                   }).FirstOrDefault();
            return obj;

        }


        public int AddPostByProject(PostEntity obj)
        {
            return communicationDAL.InsertCommunicationPost(obj);

        }

        public List<PostDetailViewModel> GetAllPost_ThreadByProjectId(PostEntity objp)
        {
            List<PostDetailViewModel> obj = new List<PostDetailViewModel>();
            obj = (from row in communicationDAL.GetPostByProjectId(objp)
                   select new PostDetailViewModel
                   {
                       PostId = Convert.ToInt32(row["PostId"]),
                      // CreatedBy = row["CreatedBy"].ToString(),
                       //CreatedOn = Convert.ToDateTime(row["CreatedOn"]),
                       //IsActive = Convert.ToBoolean(row["IsActive"]),
                       PostContent = row["PostContent"].ToString(),
                       PostTitle = row["PostTitle"].ToString(),
                       Content = row["Content"].ToString(),
                       NotifyToEmail = row["NotifyToEmail"].ToString(),
                       ThreadId = Convert.ToInt32(row["ThreadId"].ToString()),
                       ParentPostid = Convert.ToInt32(row["ParentPostid"].ToString()),
                      // ThreadType = row["PostType"].ToString(),
                       Title = row["Title"].ToString(),
                     //  ParentThreadId = Convert.ToInt32(row["ParentThreadId"])
                   }).ToList();
            return obj;

        }

        #endregion

        #region NotUsed

        #region Thread


        public List<ThreadEntity> GetThreadByPost(int postid)
        {
            List<ThreadEntity> obj = new List<ThreadEntity>();
            return obj;

        }

        public ThreadEntity GetThreadById(int threadid)
        {
            ThreadEntity obj = new ThreadEntity();
            return obj;
        }



        public List<ThreadEntity> GetThreadByParentThread(int parentthreadid)
        {
            List<ThreadEntity> obj = new List<ThreadEntity>();
            return obj;
        }
        #endregion

        public int InsertPost(PostEntity obj) { return 0; }
        public bool UpdatePost(PostEntity obj) { return true; }
        public bool DeletePost(PostEntity obj) { return true; }


        public int InsertThread(ThreadEntity obj) { return 0; }
        public bool UpdateThread(ThreadEntity obj) { return true; }
        public bool DeleteThread(ThreadEntity obj) { return true; }


        #endregion
    }
}