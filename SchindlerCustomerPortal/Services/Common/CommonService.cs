﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Models.Job;
using SchindlerCustomerPortal.Models.Project;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;

namespace SchindlerCustomerPortal.Services.Common
{
    public class CommonService : ICommonService
    {
        Utility utility = new Utility();
        ModuleDAL moduledal = new ModuleDAL();

        public List<CountryEntity> countryList()
        {
            try
            {
                List<CountryEntity> country = (from row in utility.GetCountyList()
                                               select new CountryEntity
                                               {
                                                   CountryId = Convert.ToInt32(row["CountryID"]),
                                                   CountryName = row["CountryName"].ToString(),
                                               }).ToList();
                return country;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }


        public List<StatusEntity> StatusList(string module)
        {
            try
            {
                List<StatusEntity> status = (from row in utility.GetStatusList(module)
                                             select new StatusEntity
                                             {
                                                 StatusId = Convert.ToInt32(row["StatusId"]),
                                                 StatusName = row["StatusName"].ToString(),
                                                 Module = row["Module"].ToString(),
                                             }).ToList();
                return status;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<StateEntity> stateList(int countryId)
        {
            try
            {
                List<StateEntity> state = (from row in utility.GetStateList(countryId)
                                           select new StateEntity
                                           {
                                               StateID = Convert.ToInt32(row["StateID"]),
                                               CountryId = Convert.ToInt32(row["CountryId"]),
                                               CountryName = row["CountryName"].ToString(),
                                               StateCode = row["StateCode"].ToString(),
                                               StateName = row["StateName"].ToString(),
                                           }).ToList();
                return state;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<CityEntity> cityList(int countryId, string stateCode)
        {
            try
            {
                List<CityEntity> city = (from row in utility.GetCityList(countryId, stateCode)
                                         select new CityEntity
                                         {
                                             CityId = Convert.ToInt32(row["CityId"]),
                                             CityName = row["CityName"].ToString(),
                                         }).ToList();

                return city;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        public List<CityEntity> cityListAutoComplete(int countryId, string stateCode, string searchterm)
        {
            try
            {
                List<CityEntity> city = (from row in utility.GetCityListAutoComplete(countryId, stateCode, searchterm)
                                         select new CityEntity
                                         {
                                             CityId = Convert.ToInt32(row["CityId"]),
                                             CityName = row["CityName"].ToString(),
                                         }).ToList();
                return city;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }



        //public Int32 AuditTrail(string PageName, string Message, string UserID, int RoleID, string Operation, string SessionID, string IPAddress, out int id)
        //{
        //    id = utility.ApplicationAuditRecord(PageName, Message, UserID, RoleID, Operation, SessionID, IPAddress, out id);
        //    return id;
        //}



        //public List<RoleEntity> roleList()
        //{
        //    try
        //    {
        //        List<RoleEntity> role = (from row in utility.GetRoleList()
        //                                       select new RoleEntity
        //                                       {
        //                                           RoleID = Convert.ToInt32(row["RoleId"]),
        //                                           RoleName = row["RoleName"].ToString(),
        //                                       }).ToList();
        //        return role;
        //    }
        //    catch (SqlException ex)
        //    {
        //        string error = ex.Message;
        //        throw;
        //    }
        //}

        public List<DocumentTypeEntity> DoctypeList()
        {
            try
            {
                List<DocumentTypeEntity> doctype = (from row in utility.GetDocTypeList()
                                                    select new DocumentTypeEntity
                                                    {
                                                        DocumentTypeID = Convert.ToInt32(row["Documenttypeid"]),
                                                        DocumentTypeName = row["documenttype"].ToString(),
                                                    }).ToList();
                return doctype;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<MilestoneEntity> Miletypelist()
        {
            try
            {
                List<MilestoneEntity> Miletype = (from row in utility.GetMilestoneTypeList()
                                                  select new MilestoneEntity
                                                  {
                                                      MilestoneId = Convert.ToInt32(row["MilestoneId"]),
                                                      MilestoneTitle = row["Milestone"].ToString(),
                                                  }).ToList();
                return Miletype;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }


        public List<DesignationEntity> designationList()
        {
            try
            {
                List<DesignationEntity> designation = (from row in utility.GetDesignationList()
                                                       select new DesignationEntity
                                                       {
                                                           DesignationId = Convert.ToInt32(row["DesignationId"]),
                                                           Designation = row["Designation"].ToString(),
                                                       }).ToList();
                return designation;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }


        public List<ModuleEntity> moduleList()
        {
            try
            {
                List<ModuleEntity> module = (from row in moduledal.GetAllModules()
                                             select new ModuleEntity
                                             {
                                                 ModuleId = Convert.ToInt32(row["ModuleId"]),
                                                 //ModuleName = row["ModuleName"].ToString(),
                                                 ModuleName = row["SchindlerDisplayName"].ToString(),
                                             }).ToList();
                return module;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public ModuleEntity ModuleByName(string ModuleName)
        {
            ModuleEntity mod = new ModuleEntity();
            mod = (from row in moduledal.GetModuleByName(ModuleName)
                   select new ModuleEntity
                   {
                       ModuleId = Convert.ToInt32(row["ModuleId"]),


                   }).FirstOrDefault();
            return mod;
        }

        public List<CityEntity> cityListForAutoComplete(string searchTerm, int countryId, string stateCode)
        {
            try
            {
                List<CityEntity> city = (from row in utility.GetCityListAutoCompleteForAdd(searchTerm, countryId, stateCode)
                                         select new CityEntity
                                         {
                                             CityId = Convert.ToInt32(row["CityId"]),
                                             CityName = row["CityName"].ToString(),
                                         }).ToList();
                return city;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<MilestoneEntity> milestoneListByJob(int JobId)
        {
            try
            {
                List<MilestoneEntity> milestone = (from row in utility.GetMilestoneListOfJob(JobId)
                                                   select new MilestoneEntity
                                                   {
                                                       MilestoneId = Convert.ToInt32(row["MilestoneId"]),
                                                       MilestoneTitle = row["MilestoneTitle"].ToString(),
                                                   }).ToList();
                return milestone;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<StatusEntity> projectstatusList()
        {
            try
            {
                List<StatusEntity> status = (from row in utility.GetProjectStatusList()
                                             select new StatusEntity
                                             {
                                                 StatusId = Convert.ToInt32(row["StatusId"]),
                                                 StatusName = row["StatusName"].ToString(),
                                             }).ToList();
                return status;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<CompanyEntity> companyList(string CompanyName)
        {
            try
            {
                List<CompanyEntity> company = (from row in utility.GetCompanyList(CompanyName)
                                               select new CompanyEntity
                                               {
                                                   CompanyID = Convert.ToInt32(row["CompanyID"]),
                                                   CompanyName = row["CompanyName"].ToString(),
                                               }).ToList();
                return company;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<ProjectEntity> GetProjectListbyCompanyId(string ProjectName, int CompanyId, int RoleId)
        {
            try
            {
                List<ProjectEntity> company = (from row in utility.GetProjectListbyCompanyId(ProjectName, CompanyId, RoleId)
                                               select new ProjectEntity
                                               {
                                                   ProjectID = Convert.ToInt32(row["ProjectId"]),
                                                   ProjectName = row["ProjectName"].ToString(),
                                                   ProjectNo = Convert.ToString(row["ProjectNo"])
                                               }).ToList();
                return company;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<JobEntity> GetJobListbyProjectNo(string JobName, string ProjectNo)
        {
            try
            {
                List<JobEntity> job = (from row in utility.GetJobListbyProjectNo(JobName, Convert.ToInt32(ProjectNo))
                                       select new JobEntity
                                       {
                                           JobId = Convert.ToInt32(row["JobId"]),
                                           JobName = Convert.ToString(row["JobName"]),
                                           JobNo = Convert.ToString(row["JobNo"])
                                       }).ToList();
                return job;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<DocumentTypeEntity> ResourceDoctypeList()
        {
            try
            {
                List<DocumentTypeEntity> doctype = (from row in utility.GetResourceDocTypeList()
                                                    select new DocumentTypeEntity
                                                    {
                                                        DocumentTypeID = Convert.ToInt32(row["Documenttypeid"]),
                                                        DocumentTypeName = row["documenttype"].ToString(),
                                                    }).ToList();
                return doctype;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<AlertMessageEntity> AlertMessgaeByModule(string moduleName)
        {
            try
            {
                List<AlertMessageEntity> message = (from row in utility.GetAlertMessageByModule(moduleName)
                                                    select new AlertMessageEntity
                                                    {
                                                        AlertID = Convert.ToInt32(row["AlertId"]),
                                                        ModuleName = Convert.ToString(row["ModuleName"]),
                                                        MessageName = Convert.ToString(row["MessageName"]),
                                                        Message = Convert.ToString(row["Message"]),
                                                        ServerSide = Convert.ToString(row["ServerSide"]),
                                                        ClientSide = Convert.ToString(row["ClientSide"]),
                                                        DictionaryValue = Convert.ToString(row["Dictionary"])
                                                    }).ToList();
                return message;
            }
            catch (SqlException ex)
            {
                string error = ex.Message;
                throw;
            }
        }

        public List<AuditTrailModel> ApplicationAuditMasterList(DataTableModel model, ref int count)
        {
            var userList = new List<AuditTrailModel>();
            UserDAL userdal = new UserDAL();
            try
            {
                userList = (from row in userdal.ApplicationAuditMasterList(model, ref count)
                            select new AuditTrailModel
                            {
                                Datetime = Convert.ToString(row["LogDate"]),
                                UserName = Convert.ToString(row["UserName"]),
                                UserRole = Convert.ToString(row["UserRole"]),
                                ModuleName = Convert.ToString(row["PageName"]),
                                Operation = Convert.ToString(row["Operation"]),
                                Action = Convert.ToString(row["ApplicationOperation"]),
                                ClientIp = Convert.ToString(row["IPAddress"]),
                                ServerIp = Convert.ToString(row["ServerIP"]),
                                ClientBrowser = Convert.ToString(row["Clientbrowser"]),
                                ServerName = Convert.ToString(row["ServerName"]),
                                SearchCriteria = Convert.ToString(row["SearchCriteria"]),

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsValidUserForProject(DataTableModel model)
        {
            UserDAL userDal = new UserDAL();
            bool flag = false;
            try
            {
                flag = userDal.IsUserValidForProject(model);
            }
            catch (Exception ex)
            { throw ex; }
            return flag;
        }


        public Tuple<double, double> GenerateLatLong(string address)
        {
            string secureKey = WebConfigurationManager.AppSettings["GoogleLocationKey"].ToString();
            double latitude = 0.00;
            double longitude = 0.00;
            try
            {
                var requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address=" + Uri.EscapeDataString(address) + "&key=" + secureKey);

                var request = WebRequest.Create(requestUri);
                var response = request.GetResponse();
                var xdoc = XDocument.Load(response.GetResponseStream());

                if (xdoc.Element("GeocodeResponse").Element("status").Value == "OK")
                {
                    var result = xdoc.Element("GeocodeResponse").Element("result");
                    var locationElement = result.Element("geometry").Element("location");

                    latitude = Convert.ToDouble(locationElement.Element("lat").Value);
                    longitude = Convert.ToDouble(locationElement.Element("lng").Value);
                }
                else if (xdoc.Element("GeocodeResponse").Element("status").Value == "OVER_QUERY_LIMIT")
                {
                    return new Tuple<double, double>(0, 0);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<double, double>(0, 0);
            }
            return new Tuple<double, double>(latitude, longitude);
        }
    }
}