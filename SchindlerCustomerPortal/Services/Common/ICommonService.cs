﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Models.Job;
using SchindlerCustomerPortal.Models.Project;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Common
{
    public interface ICommonService
    {
        List<CityEntity> cityListAutoComplete(int countryId, string stateCode, string searchterm);

        List<CountryEntity> countryList();

        List<StateEntity> stateList(int countryId);

        List<CityEntity> cityList(int countryId, string stateCode);

        //Int32 AuditTrail(string PageName, string Message, string UserID, int RoleID, string Operation, string SessionID, string IPAddress, out int id);

        List<DocumentTypeEntity> DoctypeList();

        List<StatusEntity> StatusList(string module);
        //List<RoleEntity> roleList();

        List<DesignationEntity> designationList();
 

         
 

        List<ModuleEntity> moduleList();

        ModuleEntity ModuleByName(string ModuleName);

        List<CityEntity> cityListForAutoComplete(string searchTerm, int countryId, string stateCode);


        List<MilestoneEntity> milestoneListByJob(int JobId);

        List<StatusEntity> projectstatusList();


        List<CompanyEntity> companyList(string CompanyName);

        List<ProjectEntity> GetProjectListbyCompanyId(string ProjectName, int CompanyId, int RoleId);

        List<JobEntity> GetJobListbyProjectNo(string JobName, string ProjectNo);

        List<DocumentTypeEntity> ResourceDoctypeList();


        List<MilestoneEntity> Miletypelist();


        List<AlertMessageEntity> AlertMessgaeByModule(string moduleName);

        Tuple<double, double> GenerateLatLong(string address);

        List<AuditTrailModel> ApplicationAuditMasterList(DataTableModel model, ref int count);

        bool IsValidUserForProject(DataTableModel model);
    }
}
