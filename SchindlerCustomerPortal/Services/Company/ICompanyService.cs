﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Company
{
    public interface ICompanyService
    {
        CompanyEntity GetCompanyByID(int id);
        /// <summary>
        /// AddCompany
        /// </summary>
        /// <param name="objcompany"></param>
        /// <returns>2 INT value for already Company exist by same name</returns>
        int AddCompany(CompanyEntity objcompany);
        bool UpdateCompany(CompanyEntity objcompany); 
        bool DeleteCompany(CommonAuditModel obj);
     
        List<CompanyEntity> GetAllCompany(SearchInfoCompanyViewModel obj, ref int totalcount);
        bool CheckDelete(int id);

        bool DeleteMultipleCompany(CompanyEntity objcompany);

        List<CompanyEntity> GetCompanyByCustomer(int copanyId);
         
    }
}
