﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Common;

namespace SchindlerCustomerPortal.Services.Company
{


    public class CompanyService : CompanyDAL, ICompanyService
    {
        public string CompanyFolderpath = CommonConfiguration.companyLogoPath;
        public CompanyEntity GetCompanyByID(int id)
        {

            return (from row in base.GetCompanyById(id)
                    select new CompanyEntity
                    {
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        CompanyLogo = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/12.png" : CompanyFolderpath + row["CompanyLogo"],
                        CompanyLogoFileName = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/12.png" : CompanyFolderpath + row["CompanyLogo"],
                        HeadOfficeAddress1 = Convert.ToString(row["HeadOfficeAddress1"]),
                        HeadOfficeAddress2 = Convert.ToString(row["HeadOfficeAddress2"]),
                        HeadOfficeCountryID = Convert.ToInt32(row["HeadOfficeCountryID"].ToString()),
                        HeadOfficeStateCode = Convert.ToString(row["HeadOfficeStateCode"]),
                        HeadOfficeCityCode = Convert.ToString(row["HeadOfficeCityCode"]),
                        HeadOfficeZipCode = Convert.ToString(row["HeadOfficeZipCode"]),
                        BillingAddress1 = Convert.ToString(row["BillingAddress1"]),
                        BillingAddress2 = Convert.ToString(row["BillingAddress2"]),
                        BillingCountryID = Convert.ToInt32(row["BillingCountryID"]),
                        BillingStateCode = Convert.ToString(row["BillingStateCode"]),
                        BillingCityCode = Convert.ToString(row["BillingCityCode"]),
                        BillingZipCode = Convert.ToString(row["BillingZipCode"]),
                        InchargeFirstName = Convert.ToString(row["InchargeFirstName"]),
                        InchargeLastName = Convert.ToString(row["InchargeLastName"]),
                        InchargeEmail = Convert.ToString(row["InchargeEmail"]),
                        InchargeDesignation = Convert.ToString(row["InchargeDesignation"]),
                        InchargePhone = Convert.ToString(row["InchargePhone"]) == "0" ? string.Empty : row["InchargePhone"].ToString().Insert(3, "-").Insert(7, "-"),
                        ContactPersonFirstName = Convert.ToString(row["ContactPersonFirstName"]),
                        ContactPersonLastName = Convert.ToString(row["ContactPersonLastName"]),
                        CreatedBy = Convert.ToString(row["CreatedBy"]),
                        //appAuditID =Convert.ToInt32( row["appAuditID"].ToString()),
                        //  SessionID = row["SessionID"].ToString(),
                        OfflineAccessDuration = Convert.ToInt32(row["OfflineAccessDuration"].ToString()),
                        CCEmailAddress = Convert.ToString(row["CCEmailAddress"]),
                        IsDeleted = Convert.ToString(row["IsDeleted"]) == "1" ? true : false,
                        AccountNumber = Convert.ToString(row["AccountNumber"]).ToUpper(),
                        ExternalAccountNumber = Convert.ToString(row["ExternalAccountNumber"]),
                        ACCTManagerEmail = Convert.ToString(row["ACCTManagerEmail"]),
                        ACCTManagerName = Convert.ToString(row["ACCTManagerName"]),
                        ACCTManagerPhone = Convert.ToString(row["ACCTManagerPhone"]) == "0" ? string.Empty : row["ACCTManagerPhone"].ToString().Insert(3, "-").Insert(7, "-"),
                        PrimaryPhoneNumber = Convert.ToString(row["PrimaryPhoneNumber"]) == "0" ? string.Empty : row["PrimaryPhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        HeadOfficePhoneNumber = Convert.ToString(row["HeadOfficePhoneNumber"]) == "0" ? string.Empty : row["HeadOfficePhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        BillingPhoneNumber = Convert.ToString(row["BillingPhoneNumber"]) == "0" ? string.Empty : row["BillingPhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                    }).FirstOrDefault();
        }

        
        public List<CompanyEntity> GetAllCompany(SearchInfoCompanyViewModel obj, ref int totalcount)
        {

            return (from row in base.CompanyMaster(obj, ref totalcount)
                    select new CompanyEntity
                    {
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        CompanyLogo = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/12.png" : CompanyFolderpath + row["CompanyLogo"],
                        //InchargePhone = string.Empty,
                        InchargePhone = Convert.ToString(row["InchargePhone"]) ==  "0" ? string.Empty : row["InchargePhone"].ToString().Insert(3, "-").Insert(7, "-"),
                        HeadOfficeAddress1 = Convert.ToString(row["Address"]),
                        InchargeFirstName = Convert.ToString(row["InchargeName"]),
                        InchargeEmail = Convert.ToString(row["InchargeEmail"]),
                        ContactPersonFirstName = Convert.ToString(row["ContactPersonName"]),
                        IsDeleted = row["IsDeleted"].ToString() == "1" ? true : false,
                        ACCTManagerName = Convert.ToString(row["ACCTManagerName"]),
                        //PrimaryPhoneNumber = string.Empty,
                        PrimaryPhoneNumber = Convert.ToString(row["PrimaryPhoneNumber"]) == "0" ? string.Empty : row["PrimaryPhoneNumber"].ToString().Insert(3, "-").Insert(7, "-")
                    }).ToList();
        }

        /// <summary>
        /// AddCompany
        /// </summary>
        /// <param name="objcompany"></param>
        /// <returns>2 INT value for already Company exist by same name</returns>
        public int AddCompany(CompanyEntity objcompany)
        {
            // objcompany.appAuditID= Convert.ToInt32(Session["appAuditID"]);
            objcompany.AccountNumber = Utility.GetUniqueKey(8);
            objcompany.HeadOfficeAddress2 = string.IsNullOrEmpty(objcompany.HeadOfficeAddress2) ? "" : objcompany.HeadOfficeAddress2;
            objcompany.BillingAddress2 = string.IsNullOrEmpty(objcompany.BillingAddress2) ? "" : objcompany.BillingAddress2;
            objcompany.OfflineAccessDuration = (objcompany.OfflineAccessDuration == 0) ? 30 : objcompany.OfflineAccessDuration;
            objcompany.CCEmailAddress = string.IsNullOrEmpty(objcompany.CCEmailAddress) ? "" : objcompany.CCEmailAddress;
            return base.InsertCompanyMaster(objcompany);
        }

        public bool UpdateCompany(CompanyEntity objcompany)
        {
            objcompany.HeadOfficeAddress2 = string.IsNullOrEmpty(objcompany.HeadOfficeAddress2) ? "" : objcompany.HeadOfficeAddress2;
            objcompany.BillingAddress2 = string.IsNullOrEmpty(objcompany.BillingAddress2) ? "" : objcompany.BillingAddress2;
            objcompany.OfflineAccessDuration = (objcompany.OfflineAccessDuration == 0) ? 30 : objcompany.OfflineAccessDuration;
            objcompany.CompanyLogo = objcompany.CompanyLogoFileName;
            objcompany.CCEmailAddress = string.IsNullOrEmpty(objcompany.CCEmailAddress) ? "" : objcompany.CCEmailAddress;

            // objcompany.appAuditID=
            //   AppAuditID = Convert.ToInt32(Session["appAuditID"]);
            return base.UpdateCompanyMaster(objcompany);
        }


        public bool DeleteCompany(CommonAuditModel obj)
        {
            return base.DeleteCompanyMaster(obj);
        }


        public bool CheckDelete(int id)
        {
            return true;
            //return base.CheckDelete(id)
        }

        public bool DeleteMultipleCompany(CompanyEntity objcompany)
        {
            var result = base.DeleteMultipleCompany(objcompany);
            return result;
        }

        public List<CompanyEntity> GetCompanyByCustomer(int copanyId)
        {
            //return (from row in base.GetCompanyByCustomer(copanyId)
            //        select new CompanyEntity
            //        {
            //            CompanyID = Convert.ToInt32(row["CompanyID"]),
            //            CompanyName = Convert.ToString(row["CompanyName"]),
            //            CompanyLogo = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/no_images.png" : CompanyFolderpath + row["CompanyLogo"],
            //            InchargePhone = Convert.ToString(row["InchargePhone"]) == "0" ? string.Empty : row["InchargePhone"].ToString().Insert(3, "-").Insert(7, "-"),
            //            HeadOfficeAddress1 = Convert.ToString(row["Address"]),
            //            InchargeFirstName = Convert.ToString(row["InchargeName"]),
            //            InchargeEmail = Convert.ToString(row["InchargeEmail"]),
            //            ContactPersonFirstName = Convert.ToString(row["ContactPersonName"]),
            //            IsDeleted = Convert.ToString(row["IsDeleted"]) == "1" ? true : false
            //        }).ToList();


            return (from row in base.GetCompanyById(copanyId)
                    select new CompanyEntity
                    {
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        CompanyLogo = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/12.png" : CompanyFolderpath + row["CompanyLogo"],
                        CompanyLogoFileName = Convert.ToString(row["CompanyLogo"]) == "0" ? "/img/no_images.png" : CompanyFolderpath + row["CompanyLogo"],
                        HeadOfficeAddress1 = Convert.ToString(row["HeadOfficeAddress1"]),
                        HeadOfficeAddress2 = Convert.ToString(row["HeadOfficeAddress2"]),
                        HeadOfficeCountryID = Convert.ToInt32(row["HeadOfficeCountryID"].ToString()),
                        HeadOfficeStateCode = Convert.ToString(row["HeadOfficeStateCode"]),
                        HeadOfficeCityCode = Convert.ToString(row["HeadOfficeCityCode"]),
                        HeadOfficeZipCode = Convert.ToString(row["HeadOfficeZipCode"]),
                        BillingAddress1 = Convert.ToString(row["BillingAddress1"]),
                        BillingAddress2 = Convert.ToString(row["BillingAddress2"]),
                        BillingCountryID = Convert.ToInt32(row["BillingCountryID"]),
                        BillingStateCode = Convert.ToString(row["BillingStateCode"]),
                        BillingCityCode = Convert.ToString(row["BillingCityCode"]),
                        BillingZipCode = Convert.ToString(row["BillingZipCode"]),
                        InchargeFirstName = Convert.ToString(row["InchargeFirstName"]),
                        InchargeLastName = Convert.ToString(row["InchargeLastName"]),
                        InchargeEmail = Convert.ToString(row["InchargeEmail"]),
                        InchargeDesignation = Convert.ToString(row["InchargeDesignation"]),
                        InchargePhone = Convert.ToString(row["InchargePhone"]) == "0" ? string.Empty : row["InchargePhone"].ToString().Insert(3, "-").Insert(7, "-"),
                        ContactPersonFirstName = Convert.ToString(row["ContactPersonFirstName"]),
                        ContactPersonLastName = Convert.ToString(row["ContactPersonLastName"]),
                        CreatedBy = Convert.ToString(row["CreatedBy"]),
                        //appAuditID =Convert.ToInt32( row["appAuditID"].ToString()),
                        //  SessionID = row["SessionID"].ToString(),
                        OfflineAccessDuration = Convert.ToInt32(row["OfflineAccessDuration"].ToString()),
                        CCEmailAddress = Convert.ToString(row["CCEmailAddress"]),
                        IsDeleted = Convert.ToString(row["IsDeleted"]) == "1" ? true : false,
                        AccountNumber = Convert.ToString(row["AccountNumber"]).ToUpper(),
                        ExternalAccountNumber = Convert.ToString(row["ExternalAccountNumber"]),
                        ACCTManagerEmail = Convert.ToString(row["ACCTManagerEmail"]),
                        ACCTManagerName = Convert.ToString(row["ACCTManagerName"]),
                        ACCTManagerPhone = Convert.ToString(row["ACCTManagerPhone"]) == "0" ? string.Empty : row["ACCTManagerPhone"].ToString().Insert(3, "-").Insert(7, "-"),
                        PrimaryPhoneNumber = Convert.ToString(row["PrimaryPhoneNumber"]) == "0" ? string.Empty : row["PrimaryPhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        HeadOfficePhoneNumber = Convert.ToString(row["HeadOfficePhoneNumber"]) == "0" ? string.Empty : row["HeadOfficePhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        BillingPhoneNumber = Convert.ToString(row["BillingPhoneNumber"]) == "0" ? string.Empty : row["BillingPhoneNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                    }).ToList();

        }

    }
}