﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Help;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Help
{
    public interface IHelpService
    {
        HelpEntity HelpInsert(HelpEntity help);
        List<HelpEntity> GetAllHelp(DataTableModel model, ref int count);
        HelpEntity GetHelpById(int HelpId);
        HelpEntity HelpUpdateService(HelpEntity doc);
        bool DeleteHelpService(HelpEntity help);
        List<HelpEntity> GetAllModuleHelp(DataTableModel model, ref int count);

    }
}
