﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Help;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SchindlerCustomerPortal.Services.Help
{
    public class HelpService : IHelpService
    {
        HelpDAL helpdal = new HelpDAL();

        public HelpEntity HelpInsert(HelpEntity help)
        {
            try
            {
                HelpEntity helpds = (from row in helpdal.HelpInsert(help)
                                     select new HelpEntity
                                     {
                                         Title = row["title"].ToString(),

                                     }).FirstOrDefault();

                return helpds;
                //return helpdal.HelpInsert(help);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public List<HelpEntity> GetAllHelp(DataTableModel model, ref int count)
        {
            var helpList = new List<HelpEntity>();
            try
            {
                helpList = (from row in helpdal.HelpSelect(model, ref count)
                            select new HelpEntity
                           {
                               HelpId = Convert.ToInt32(row["HelpId"]),
                               ModuleName = Convert.ToString(row["ModuleName"]),
                               Title = Convert.ToString(row["Title"]),
                               Description = StripHTML(Convert.ToString(row["Description"])),
                               PublishedStatus = Convert.ToBoolean(row["PublishedStatus"])

                           }).ToList();

                return helpList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HelpEntity GetHelpById(int HelpId)
        {
            HelpEntity help = new HelpEntity();
            help = (from row in helpdal.HelpSelectById(HelpId)
                   select new HelpEntity
                   {
                       HelpId = Convert.ToInt32(row["HelpId"]),
                       ModuleId = Convert.ToInt32(row["ModuleId"]),
                       Title = row["Title"].ToString(),
                       Description = row["Description"].ToString(),
                       PublishedStatus = Convert.ToBoolean(row["PublishedStatus"]),
                       
                   }).FirstOrDefault();
            return help;
        }

        public HelpEntity HelpUpdateService(HelpEntity doc)
        {
            try
            {
                HelpEntity helpds = (from row in helpdal.HelpUpdate(doc)
                                        select new HelpEntity
                                        {
                                            Title = row["title"].ToString(),

                                        }).FirstOrDefault();

                return helpds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool DeleteHelpService(HelpEntity help)
        {
            var result = helpdal.DeleteHelp(help);
            return result;
        }

        public List<HelpEntity> GetAllModuleHelp(DataTableModel model, ref int count)
        {
            var helpList = new List<HelpEntity>();
            try
            {
                helpList = (from row in helpdal.HelpSelectByModule(model, ref count)
                            select new HelpEntity
                            {
                                HelpId = Convert.ToInt32(row["HelpId"]),
                                ModuleName = row["ModuleName"].ToString(),
                                Title = row["Title"].ToString(),
                                Description = row["Description"].ToString(),
                                PublishedStatus = Convert.ToBoolean(row["PublishedStatus"])

                            }).ToList();

                return helpList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}