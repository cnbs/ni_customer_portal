﻿
using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace SchindlerCustomerPortal.Services.OfficeManager
{
    public class OfficeManagerService : IOfficeManagerService
    {
        OfficeManagerDAL officeManagerDAL = new OfficeManagerDAL();

        //public OfficeManagerEntity Find(string Email)
        //{
        //    try
        //    {
        //        OfficeManagerEntity userds = (from row in officeManagerDAL.LoginNow(Email)
        //                                      select new OfficeManagerEntity
        //                                      {

        //                                          Email = row["Email"].ToString(),
        //                                          RoleID = Convert.ToInt32(row["RoleID"]),
        //                                          UserID = Convert.ToInt32(row["UserID"]),
        //                                          FirstName = row["FirstName"].ToString(),
        //                                          LastName = row["LastName"].ToString(),
        //                                          CompanyID = Convert.IsDBNull(row["CompanyID"]) ? 0 : Convert.ToInt32(row["CompanyID"]),
        //                                          CompanyName = row["CompanyName"].ToString(),
        //                                          ContactNumber = row["ContactNumber"].ToString(),
        //                                          ProfilePic = string.IsNullOrEmpty(row["ProfilePic"].ToString()) ? "" : row["ProfilePic"].ToString(),
        //                                          CreatedBy = row["CreatedBy"].ToString(),
        //                                          UpdatedBy = row["UpdatedBy"].ToString(),
        //                                          Password = row["Password"].ToString(),
        //                                          PasswordSalt = row["Password_Salt"].ToString(),
        //                                          AccountStatus = Convert.ToInt32(row["AccountStatus"])


        //                                      }).FirstOrDefault();

        //        return userds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }


        //}

        public OfficeManagerEntity GetOfficeManagerById(int userId)
        {
            OfficeManagerEntity officemanager = new OfficeManagerEntity();
            officemanager = (from row in officeManagerDAL.OfficeManagerSelectById(userId)
                    select new OfficeManagerEntity
                    {
                        Email = row["Email"].ToString(),
                        OfficeManagerID = Convert.ToInt32(row["OfficeManagerID"].ToString()),
                        FirstName = row["FirstName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        OfficeID = row["OfficeID"].ToString(),
                        Phone = string.IsNullOrEmpty(Convert.ToString(row["Phone"])) ? Convert.ToString(row["Phone"]) : Convert.ToString(row["Phone"]).Insert(3, "-").Insert(7, "-"),
                        //Phone = row["Phone"].ToString(),
                        SapID = row["SapID"].ToString(),
                        Office_Desc = row["Office_Desc"].ToString(),
                        Title = row["Title"].ToString(),
                        Street = row["Street"].ToString(),
                        City = row["City"].ToString(),
                        State = row["State"].ToString(),
                        StateName = row["StateName"].ToString(),
                        Zip = row["Zip"].ToString()
                    }).FirstOrDefault();
            return officemanager;
        }


        public List<OfficeManagerEntity> GetAllOfficeManagers(DataTableModel model, ref int count)
        {
            var userList = new List<OfficeManagerEntity>();
            try
            {
                userList = (from row in officeManagerDAL.OfficeManagerSelect(model, ref count)
                            select new OfficeManagerEntity
                            {
                                FullName = Convert.ToString(row["FullName"]),
                                Email = Convert.ToString(row["Email"]),
                                OfficeManagerID = Convert.ToInt32(row["OfficeManagerID"].ToString()), 
                                FirstName = Convert.ToString(row["FirstName"]),
                                LastName = Convert.ToString(row["LastName"]),
                                CompanyID = Convert.ToInt32(row["CompanyID"]),
                                Phone = string.IsNullOrEmpty(Convert.ToString(row["Phone"])) ? Convert.ToString(row["Phone"]): Convert.ToString(row["Phone"]).Insert(3, "-").Insert(7, "-"),
                                //Phone = row["Phone"].ToString(),
                                SapID = Convert.ToString(row["SapID"]),
                                Office_Desc = Convert.ToString(row["Office_Desc"]),
                                Title = Convert.ToString(row["Title"]),
                                Street = Convert.ToString(row["Street"]),
                                City = Convert.ToString(row["City"]),
                                State = Convert.ToString(row["State"]),
                                Zip = Convert.ToString(row["Zip"])                             

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddOfficeManager(OfficeManagerEntity officemanager)
        {
            int result = officeManagerDAL.InsertOfficeManager(officemanager);
            return result;
        }

        public bool UpdateOfficeManager(OfficeManagerEntity officemanager)
        {
            bool result = officeManagerDAL.UpdateOfficeManager(officemanager);
            return result;
        }

        public bool DeleteOfficeManager(OfficeManagerEntity officemanager)
        {
            var result = officeManagerDAL.DeleteOfficeManager(officemanager);
            return result;
        }
             
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        //public bool DeleteMultipleUser(OfficeManagerEntity officemanager)
        //{
        //    var result = officeManagerDAL.DeleteMultipleCompanyUser(officemanager);
        //    return result;
        //}

        //public bool UpdateUserStatus(OfficeManagerEntity officemanager)
        //{
        //    var result = officeManagerDAL.UpdateUserAccountStatus(officemanager);
        //    return result;
        //}

        //public List<OfficeManagerEntity> GetAllNewRegisteredUser(DataTableModel model, ref int count)
        //{
        //    var userList = new List<OfficeManagerEntity>();
        //    try
        //    {
        //        userList = (from row in officeManagerDAL.NewlyRegisteredUser(model, ref count)
        //                    select new OfficeManagerEntity
        //                    {
        //                        Email = row["Email"].ToString(),
        //                        RoleID = Convert.ToInt32(row["RoleID"]),
        //                        UserID = Convert.ToInt32(row["UserID"]),
        //                        FirstName = row["UserName"].ToString(),
        //                        CompanyName = Convert.ToString(row["CompanyName"]),
        //                        ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
        //                        ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
        //                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
        //                        AccountStatus = Convert.ToInt32(row["AccountStatus"]),
        //                        Designation = row["DesignationName"].ToString(),
        //                        DesignationId = Convert.ToInt32(row["DesignationId"]),
        //                        RegisteredDate = Convert.ToString(row["RegisteredDate"]),
        //                        Requested_Projects= Convert.ToString(row["Requested_Projects"])
        //                    }).ToList();

        //        return userList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        
        //public OfficeManagerEntity GetNewRegisteredUserById(int userId)
        //{
        //    OfficeManagerEntity officemanager = new OfficeManagerEntity();
        //    officemanager = (from row in officeManagerDAL.NewlyRegisteredUserEdit(userId)
        //            select new OfficeManagerEntity
        //            {
        //                Email = row["Email"].ToString(),
        //                RoleID = Convert.ToInt32(row["RoleID"]),
        //                UserID = Convert.ToInt32(row["UserID"]),
        //                FirstName = row["FirstName"].ToString(),
        //                CompanyName = row["CompanyName"].ToString(),
        //                ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
        //                ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
        //                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
        //                Designation = row["DesignationName"].ToString(),
        //                DesignationId = Convert.ToInt32(row["DesignationId"]),
        //                Address1 = Convert.ToString(row["Address1"]),
        //                State = Convert.ToString(row["State"]),
        //                City = Convert.ToString(row["City"]),
        //                PostalCode = Convert.ToString(row["PostalCode"]),
        //                AccountStatus = Convert.ToInt32(row["AccountStatus"]),
        //                Requested_Projects = Convert.ToString(row["Requested_Projects"])
        //            }).FirstOrDefault();
        //    return officemanager;
        //}

    }
}