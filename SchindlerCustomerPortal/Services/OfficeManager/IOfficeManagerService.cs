﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.OfficeManager
{
    public interface IOfficeManagerService
    {
       

        OfficeManagerEntity GetOfficeManagerById(int userId);

        List<OfficeManagerEntity> GetAllOfficeManagers(DataTableModel model, ref int count);

        int AddOfficeManager(OfficeManagerEntity officemanager);

        bool UpdateOfficeManager(OfficeManagerEntity officemanager);

        bool DeleteOfficeManager(OfficeManagerEntity officemanager);


    }
}