﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.ProjectDocument
{
    public interface IContractDocumentService
    {
        List<DocumentEntity> GetAllDocumentByProject(DataTableModel model, ref int count);

        ProjectEntity GetProjectById(int ProjectID);
    }
}
