﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Document;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.ProjectDocument
{
    public class ContractDocumentService : IContractDocumentService
    {
        ProjectDocumentDAL projectdocumentdal = new ProjectDocumentDAL();

        public List<DocumentEntity> GetAllDocumentByProject(DataTableModel model, ref int count)
        {
            var docList = new List<DocumentEntity>();
            try
            {
                docList = (from row in projectdocumentdal.DocumentSelectByProject(model, ref count)
                           select new DocumentEntity
                           {
                               DocumentID = Convert.ToInt32(row["DocumentID"]),
                               DocumentName = row["DocumentName"].ToString(),
                               DocumentType = row["DocumentType"].ToString(),
                               Description = row["Description"].ToString(),
                               StatusName = row["StatusName"].ToString(),
                               PublishedBy = row["PublishedBy"].ToString(),
                               ExpiredDate = Convert.ToString(row["Expireddate"]),
                               DocumentPath = row["DocumentPath"].ToString(),
                               PublishedOn = DBNull.Value.Equals(row["PublishedOn"]) ? (DateTime?)null : Convert.ToDateTime(row["PublishedOn"]),
                               CreatedDate = Convert.ToDateTime(row["CreatedDate"])
                           }).ToList();

                return docList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProjectEntity GetProjectById(int ProjectID)
        {
            ProjectEntity pro = new ProjectEntity();
            pro = (from row in projectdocumentdal.ProjectSelectById(ProjectID)
                   select new ProjectEntity
                   {
                       ProjectID = Convert.ToInt32(row["ProjectID"]),
                       ProjectName = row["ProjectName"].ToString(),
                      

                   }).FirstOrDefault();
            return pro;
        }
    }
}