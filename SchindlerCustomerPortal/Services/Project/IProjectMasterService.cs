﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Project;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchindlerCustomerPortal.Services.Project
{
    public interface IProjectMasterService
    {
        List<ProjectEntity> GetAllProject(ProjectSearchInfoViewModel obj, ref int totalcount);
        List<ProjectMasterListEntity> GetAllProjectMasterList(DataTableModel model, ref int count);
        ProjectMasterEntity GetProjectMasterRecordByProjectID(DataTableModel model);
        List<ProjectJobListEntity> GetAllProjectJobList(DataTableModel model, ref int count);
        bool UpdateProjectMaster(ProjectMasterEntity projectMaster);
        bool CheckDocumentName(ProjectDocumentEntity checkdata);
        ProjectJobEntity GetProjectJobRecordByJobNo(DataTableModel model);
        List<ProjectDocumentEntity> GetProjectDocumentList(DataTableModel model, ref int count);
        ProjectDocumentEntity AddProjectDocument(ProjectDocumentEntity doc);
        ProjectDocumentEntity UpdateProjectDocument(ProjectDocumentEntity doc);
        bool DeleteProjectDocument(ProjectDocumentEntity doc);
        ProjectDocumentEntity GetProjectDocumentByID(int docId);
        ProjectChartResult GetProjectChart(DataTableModel model);
        List<ProjectMilestoneStatusEntity> DashboardProjectListingMilestone(DataTableModel model);

        List<ProjectStatusDueDateEntity> GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model);
        bool AddProjectBankMilestoneComment(string comment, string projectBankTitle, int userID);

        bool AddProjectBankMilestoneCommentDueDate(string comment, string projectBankTitle, int userID, string statusid, string duedate, string completeddate);
        List<ProjectMilestoneEntity> GetJobDataDateInformation(DataTableModel model);
        DataTable GetAllBankListWithDetail(DataTableModel obj, ref int totalcount, ref int projectCompanyId);
        string DashboardPaymentDocAndContactData(DataTableModel model);
        bool Update_BankDescByJobNo(string Description, string UpdatedBy, int appAuditID, string SessionID, string JobNo, string ApplicationOperation);
        bool editEmailAndPhoneData(DataTableModel model);
    }
}
