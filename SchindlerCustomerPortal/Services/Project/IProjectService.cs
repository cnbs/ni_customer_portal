﻿using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Job;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.Project
{
    public interface IProjectService
    {
        bool AddProjectContact(ProjectContactEntity user);

        ProjectContactEntity ProjectContactSelectById(int contactID);

        bool UpdateProjectContact(ProjectContactEntity user);

        bool DeleteProjectContact(ProjectContactEntity user);

        List<ProjectContactEntity> GetProjectContactList(DataTableModel model, ref int count);

        List<JobEntity> GetAllJobByProjectNoandSearchTerm(int projectNo, string searchTerm);

        List<ProjectAssignEntity> GetAllProjectByCompanyIdandSearchTerm(int CompanyId, string searchTerm, int RoleId, int UserId);

        List<ProjectContactEntity> GetProjectContactListByCompanyId(DataTableModel model, ref int count);
    }
}