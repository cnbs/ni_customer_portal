﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchindlerCustomerPortal.Common;
using System.Data;
using SchindlerCustomerPortal.Models.ProjectMilestone;
using Newtonsoft.Json;
using SchindlerCustomerPortal.Services.ProjectMilestone;

namespace SchindlerCustomerPortal.Services.Project
{
    public class ProjectMasterService : ProjectDAL, IProjectMasterService
    {
        DocumentDAL documentDAL = new DocumentDAL();
        ProjectMilestoneService projectMilestoneService = new ProjectMilestoneService();

        public List<ProjectEntity> GetAllProject(ProjectSearchInfoViewModel obj, ref int totalcount)
        {
            List<ProjectEntity> projectlst = new List<ProjectEntity>();
            projectlst = (from row in base.GetAllProject(obj, ref totalcount)
                          select new ProjectEntity
                          {
                              ProjectID = Convert.ToInt32(row["ProjectID"]),
                              InceptionDatestr = (string.IsNullOrEmpty(Convert.ToString(row["InceptionDate"])) || Convert.ToString(row["InceptionDate"]) == "01/01/1900") ? "" : Convert.ToDateTime(row["InceptionDate"]).ToShortDateString(),
                              Job_Address = row["Job_Address"].ToString(),
                              Job_State = row["Job_Address"].ToString(),
                              ParentProjectId = Convert.ToInt32(row["ParentProjectId"]),
                              ProjectName = row["ProjectName"].ToString(),
                              UnitCount = Convert.ToInt32(row["UnitCount"]),
                              Statusstr = Convert.ToString(row["Status"])
                          }).ToList();
            return projectlst;
        }
        public List<ProjectMasterListEntity> GetAllProjectMasterList(DataTableModel model, ref int totalcount)
        {
            List<ProjectMasterListEntity> projectList = new List<ProjectMasterListEntity>();
            projectList = (from row in base.GetAllProjectList(model, ref totalcount)
                           select new ProjectMasterListEntity
                           {
                               ProjectID = Convert.ToInt32(row["ProjectID"]),
                               ProjectNo = Convert.ToString(row["ProjectNo"]),
                               ProjectName = Convert.ToString(row["ProjectName"]),
                               CompanyName = Convert.ToString(row["CompanyName"]),
                               Address = Convert.ToString(row["Job_Address"]),
                               Banks = Convert.ToInt32(row["Banks"] == null ? 0 : row["Banks"]),
                               Units = Convert.ToInt32(row["UnitCount"] == null ? 0 : row["UnitCount"]),
                               ContractorSuper = Convert.ToString(row["ContractorSuper"]),
                               ContractorProjMgr = Convert.ToString(row["ContractorProjMgr"]),
                               SchindlerProjMgr = Convert.ToString(row["SchindlerProjMgr"]),
                               SchindlerSuperintendent = Convert.ToString(row["SchindlerSuperintendent"]),
                               Status = Convert.ToString(row["Status"]),
                               MessageCount = Convert.ToString(row["MessageCount"])
                           }).ToList();

            return projectList;
        }
        public List<ProjectJobListEntity> GetAllProjectJobList(DataTableModel model, ref int totalcount)
        {

            List<ProjectJobListEntity> projectJobList = new List<ProjectJobListEntity>();
            bool ViewForCustomers = true;
            if (model.RoleID == 1 || model.RoleID == 3) { ViewForCustomers = false; } else { ViewForCustomers = true; }

            projectJobList = (from row in base.GetAllProjectJobList(model, ref totalcount)
                              select new ProjectJobListEntity
                              {
                                  JobNo = Convert.ToString(row["JobNo"]),
                                  BankDesc = Convert.ToString(row["BankDesc"]),
                                  Units = Convert.ToInt32(row["UnitCount"]),
                                  ProdType = Convert.ToString(row["ProdType"]),
                                  ProdCode = Convert.ToString(row["ProdCode"]),
                                  Capacity = Convert.ToString(row["Capacity"]),
                                  Speed = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Speed"]) ? 0 : Convert.ToInt32(row["Speed"])).ToString(),
                                  Floor = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Floor"]) ? 0 : Convert.ToInt32(row["Floor"])).ToString(),
                                  Rear = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Rear"]) ? 0 : Convert.ToInt32(row["Rear"])).ToString(),
                                  StatusName = projectMilestoneService.setMilestoneNewName(Convert.ToString(row["StatusName"])),
                                  MessageCount = Convert.ToString(row["MessageCount"])
                              }).ToList();

            return projectJobList;
        }
        public ProjectMasterEntity GetProjectMasterRecordByProjectID(DataTableModel model)
        {
            ProjectMasterEntity projectMasterRecord = new ProjectMasterEntity();

            projectMasterRecord = (from row in base.GetProjectMasterRecordByProjectID(model)
                                   select new ProjectMasterEntity
                                   {
                                       ProjectID = Convert.ToInt32(row["ProjectID"]),
                                       ProjectNo = Convert.ToString(row["ProjectNo"]),
                                       ProjectName = Convert.ToString(row["ProjectName"]),
                                       Banks = Convert.ToInt32(row["Banks"]),
                                       CompanyId = Convert.ToInt32(row["CompanyId"]),
                                       CompanyName = Convert.ToString(row["CompanyName"]),
                                       ProjectMgrPhone = Convert.ToString(row["ProjectMgrPhone"]),
                                       OriginalNODDate = Convert.ToString(row["OriginalNODDate"]).Replace(" 12:00:00 AM", ""),
                                       NOD = Convert.ToString(row["NOD"]).Replace(" 12:00:00 AM", ""),
                                       NODConfirm = Convert.ToString(row["NODConfirm"]),
                                       IsContractSigned = Convert.ToString(row["IsContractSigned"]),
                                       ContractorSuper = Convert.ToString(row["ContractorSuper"]),
                                       SchindlerSuperintendent = Convert.ToString(row["SchindlerSuperintendent"]),
                                       SchindlerSalesExecutive = Convert.ToString(row["SchindlerSalesExecutive"]),
                                       TeamAssigned = Convert.ToString(row["TeamAssigned"]),
                                       Job_Address = Convert.ToString(row["Job_Address"]),
                                       Job_City = Convert.ToString(row["Job_City"]),
                                       Job_State = Convert.ToString(row["Job_State"]),
                                       Job_Zip = Convert.ToString(row["Job_Zip"]),
                                       FactoryMaterialEstimate = Convert.ToDouble(Convert.ToString(row["FactoryMaterialEstimate"])),
                                       FactoryMaterialActual = !string.IsNullOrEmpty(Convert.ToString(row["FactoryMaterialActual"]))?Convert.ToDouble(Convert.ToString(row["FactoryMaterialActual"])):0,
                                       ContractorProjMgr = Convert.ToString(row["ContractorProjMgr"]),
                                       ContractReceivedDate = Convert.ToString(row["ContractReceivedDate"]).Replace(" 12:00:00 AM", ""),
                                       AwardDate = Convert.ToString(row["AwardDate"]).Replace(" 12:00:00 AM", ""),
                                       CustomerNo = Convert.ToInt64(row["CustomerNo"]),
                                       NODConfirmDate = Convert.ToString(row["NODConfirmDate"]).Replace(" 12:00:00 AM", ""),
                                       UnitCount = Convert.ToInt32(row["UnitCount"]),
                                       BookingComplDate = Convert.ToString(row["BookingComplDate"]).Replace(" 12:00:00 AM", ""),
                                       Proj_Mgr_Email = Convert.ToString(row["Proj_Mgr_Email"]),
                                       Status = Convert.ToInt32(row["Status"]),
                                       StatusName = projectMilestoneService.setMilestoneNewName(Convert.ToString(row["StatusName"])),

                                       InceptionDate = Convert.ToString(row["InceptionDate"]).Replace(" 12:00:00 AM", ""),
                                       TurnOverDate = Convert.ToString(row["TurnOverDate"]).Replace(" 12:00:00 AM", ""),
                                       AlternateProjectNo = Convert.ToString(row["AlternateProjectNo"]),
                                       ContractorContactPerson = Convert.ToString(row["ContractorContactPerson"]),
                                       SchindlerProjMgr = Convert.ToString(row["SchindlerProjMgr"]),
                                       ParentProjectId = Convert.ToInt32(row["ParentProjectId"]),

                                       SchindlerSuperintendentEmail = Convert.ToString(row["SchindlerSuperintendentEmail"]),
                                       SchindlerSalesExecutiveEmail = Convert.ToString(row["SchindlerSalesExecutiveEmail"]),
                                       SchindlerProjMgrEmail = Convert.ToString(row["SchindlerProjMgrEmail"]),

                                       SchindlerSuperintendent_Phone = Convert.ToString(row["SchindlerSuperintendent_Phone"]),
                                       SchindlerSalesExecutive_Phone = Convert.ToString(row["SchindlerSalesExecutive_Phone"]),
                                       SchindlerProjMgr_Phone = Convert.ToString(row["SchindlerProjMgr_Phone"]),

                                   }).FirstOrDefault();

            return projectMasterRecord;
        }
        public bool UpdateProjectMaster(ProjectMasterEntity projectMaster)
        {

            bool result = base.UpdateProjectMaster(projectMaster);
            return result;
        }
        public bool CheckDocumentName(ProjectDocumentEntity checkdata)
        {
            if (string.IsNullOrEmpty(checkdata.JobId))
            {
                checkdata.JobId = string.Empty;
            }
            bool result = base.CheckDocumentName(checkdata);
            return result;
        }
        public ProjectJobEntity GetProjectJobRecordByJobNo(DataTableModel model)
        {
            try
            {
                bool ViewForCustomers = true;
                if (model.RoleID == 1 || model.RoleID == 3) { ViewForCustomers = false; } else { ViewForCustomers = true; }

                ProjectJobEntity projectJobRecord = new ProjectJobEntity();
                projectJobRecord = (from row in base.GetProjectJobRecordByJobNo(model)
                                    select new ProjectJobEntity
                                    {
                                        JobId = DBNull.Value.Equals(row["JobId"]) ? 0 : Convert.ToInt32(row["JobId"]),
                                        JobNo = Convert.ToString(row["JobNo"]),
                                        JobName = Convert.ToString(row["JobName"]),
                                        ProjectId = DBNull.Value.Equals(row["ProjectId"]) ? 0 : Convert.ToInt32(row["ProjectId"]),
                                        BankDesc = Convert.ToString(row["BankDesc"]),
                                        Prod = Convert.ToString(row["Prod"]),
                                        District = Convert.ToString(row["District"]),
                                        Office = DBNull.Value.Equals(row["Office"]) ? 0 : Convert.ToInt32(row["Office"]),
                                        ProdCode = Convert.ToString(row["ProdCode"]),
                                        OriginalNODDate = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["OriginalNODDate"])) ? "" : Convert.ToString(row["OriginalNODDate"]).Replace(" 12:00:00 AM", "")),
                                        NOD = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOD"])) ? "" : Convert.ToString(row["NOD"]).Replace(" 12:00:00 AM", "")),
                                        NODConfirm = (ViewForCustomers == true ? "" : Convert.ToString(row["NODConfirm"])),
                                        ActionText = (ViewForCustomers == true ? "" : Convert.ToString(row["ActionText"])),
                                        IsContractSigned = Convert.ToString(row["IsContractSigned"]),
                                        ForecastCloseDate = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ForecastCloseDate"])) ? "" : Convert.ToString(row["ForecastCloseDate"]).Replace(" 12:00:00 AM", "")),
                                        Price = (ViewForCustomers == true ? 0 : string.IsNullOrEmpty(Convert.ToString(row["Price"])) ? 0 : Convert.ToDecimal(row["Price"])),
                                        PaidPercent = string.IsNullOrEmpty(Convert.ToString(row["PaidPercent"])) ? 0 : Convert.ToDecimal(row["PaidPercent"]),
                                        BilledPercent = string.IsNullOrEmpty(Convert.ToString(row["BilledPercent"])) ? 0 : Convert.ToDecimal(row["BilledPercent"]),
                                        BaseHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["BaseHrs"]) ? 0 : Convert.ToInt32(row["BaseHrs"])),
                                        EstHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["EstHrs"]) ? 0 : Convert.ToInt32(row["EstHrs"])),
                                        ActHrs = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["ActHrs"]) ? 0 : Convert.ToInt32(row["ActHrs"])),
                                        CompletionPercent = (ViewForCustomers == true ? 0 : (string.IsNullOrEmpty(Convert.ToString(row["CompletionPercent"]))) ? 0 : Convert.ToDecimal(row["CompletionPercent"])),
                                        ProjectMgrPhone = string.IsNullOrEmpty(Convert.ToString((row["ProjectMgrPhone"]))) ? 0 : Convert.ToDecimal(row["ProjectMgrPhone"]),
                                        ContractorSuper = Convert.ToString(row["ContractorSuper"]),
                                        TeamAssigned = (ViewForCustomers == true ? "" : Convert.ToString(row["TeamAssigned"])),
                                        Status = DBNull.Value.Equals(row["Status"]) ? 0 : Convert.ToInt32(row["Status"]),
                                        StatusName = projectMilestoneService.setMilestoneNewName(Convert.ToString(row["StatusName"])),
                                        Elev_Type = Convert.ToString(row["Elev_Type"]),
                                        Speed = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Speed"]) ? 0 : Convert.ToInt32(row["Speed"])),
                                        Capacity = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Capacity"]) ? 0 : Convert.ToInt32(row["Capacity"])),
                                        Num_Floors = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Num_Floors"]) ? 0 : Convert.ToInt32(row["Num_Floors"])),
                                        Volt = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Volt"]) ? 0 : Convert.ToInt32(row["Volt"])),
                                        DoorType = (ViewForCustomers == true ? "" : Convert.ToString(row["DoorType"])),
                                        Rear = (ViewForCustomers == true ? 0 : DBNull.Value.Equals(row["Rear"]) ? 0 : Convert.ToInt32(row["Rear"])),
                                        Travel = (ViewForCustomers == true ? "" : Convert.ToString(row["Travel"])),
                                        CwtLoc = (ViewForCustomers == true ? "" : Convert.ToString(row["CwtLoc"])),
                                        FactoryMaterialEstimate = string.IsNullOrEmpty(Convert.ToString((row["FactoryMaterialEstimate"]))) ? 0 : Convert.ToDecimal(row["FactoryMaterialEstimate"]),
                                        FactoryMaterialActual = string.IsNullOrEmpty(Convert.ToString((row["FactoryMaterialActual"]))) ? 0 : Convert.ToDecimal(row["FactoryMaterialActual"]),
                                        Serial_Num = Convert.ToString(row["Serial_Num"]),
                                        ContractorProjMgr = Convert.ToString(row["ContractorProjMgr"]),
                                        ContractRcvdDate = string.IsNullOrEmpty(Convert.ToString(row["ContractRcvdDate"])) ? "" : Convert.ToString(row["ContractRcvdDate"]).Replace(" 12:00:00 AM", ""),
                                        AwardDate = string.IsNullOrEmpty(Convert.ToString(row["AwardDate"])) ? "" : Convert.ToString(row["AwardDate"]).Replace(" 12:00:00 AM", ""),
                                        ApprovalstoGC = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ApprovalstoGC"])) ? "" : Convert.ToString(row["ApprovalstoGC"]).Replace(" 12:00:00 AM", "")),
                                        ApprovalstoGBer = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["ApprovalstoGBer"])) ? "" : Convert.ToString(row["ApprovalstoGBer"]).Replace(" 12:00:00 AM", "")),
                                        NOSPlanned = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOSPlanned"])) ? "" : Convert.ToString(row["NOSPlanned"]).Replace(" 12:00:00 AM", "")),
                                        NOSActual = (ViewForCustomers == true ? "" : string.IsNullOrEmpty(Convert.ToString(row["NOSActual"])) ? "" : Convert.ToString(row["NOSActual"]).Replace(" 12:00:00 AM", "")),
                                        NODConfirmDate = string.IsNullOrEmpty(Convert.ToString(row["NODConfirmDate"])) ? "" : Convert.ToString(row["NODConfirmDate"]).Replace(" 12:00:00 AM", ""),
                                        Proj_Mgr_Email = Convert.ToString(row["Proj_Mgr_Email"]),
                                        TurnOverDate = string.IsNullOrEmpty(Convert.ToString(row["TurnOverDate"])) ? "" : Convert.ToString(row["TurnOverDate"]).Replace(" 12:00:00 AM", ""),
                                        UnitCount = DBNull.Value.Equals(row["UnitCount"]) ? 0 : Convert.ToInt32(row["UnitCount"]),
                                        SuperComments = (ViewForCustomers == true ? "" : Convert.ToString(row["SuperComments"])),
                                        SalesComments = (ViewForCustomers == true ? "" : Convert.ToString(row["SalesComments"])),
                                        ProjectName = Convert.ToString(row["ProjectName"]),
                                        ProjectNo = Convert.ToString(row["ProjectNo"]),
                                        CompanyName = Convert.ToString(row["CompanyName"]),
                                        IsDeleted = Convert.ToBoolean(row["IsDeleted"])
                                    }).FirstOrDefault();

                return projectJobRecord;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw;
            }
        }
        public List<ProjectDocumentEntity> GetProjectDocumentList(DataTableModel model, ref int totalcount)
        {
            List<ProjectDocumentEntity> projectDocumentList = new List<ProjectDocumentEntity>();
            projectDocumentList = (from row in base.GetProjectDocumentList(model, ref totalcount)
                                   select new ProjectDocumentEntity
                                   {
                                       DocumentID = Convert.ToInt32(row["DocumentID"]),
                                       DocumentName = row["DocumentName"].ToString(),
                                       Description = row["Description"].ToString(),
                                       ExpiredDate = Convert.ToString(row["Expireddate"]),
                                       PublishedStatus = row["Published"].ToString(),
                                       PublishedBy = row["PublishedBy"].ToString(),
                                       CompanyName = row["CompanyName"].ToString(),
                                       DocumentPath = row["DocumentPath"].ToString(),
                                       DocumentType = Convert.ToString(row["DocumentType"])
                                   }).ToList();

            return projectDocumentList;
        }
        public ProjectDocumentEntity AddProjectDocument(ProjectDocumentEntity doc)
        {
            try
            {
                ////SchindlerCustomerPortal.Models.Document.DocumentEntity
                //SchindlerCustomerPortal.Models.Document.DocumentEntity emp = new Models.Document.DocumentEntity();
                //emp = Utility.Cast<SchindlerCustomerPortal.Models.Document.DocumentEntity>(doc);

                ProjectDocumentEntity docds = (from row in documentDAL.DocumentInsert(StaticUtilities.Cast<SchindlerCustomerPortal.Models.Document.DocumentEntity>(doc))
                                               select new ProjectDocumentEntity
                                               {
                                                   DocumentName = row["DocumentName"].ToString(),

                                               }).FirstOrDefault();

                return docds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteProjectDocument(ProjectDocumentEntity doc)
        {
            var result = documentDAL.DeleteDocument(StaticUtilities.Cast<SchindlerCustomerPortal.Models.Document.DocumentEntity>(doc));
            return result;
        }
        public ProjectDocumentEntity UpdateProjectDocument(ProjectDocumentEntity doc)
        {
            try
            {
                ProjectDocumentEntity docds = (from row in documentDAL.DocumentUpdate(StaticUtilities.Cast<SchindlerCustomerPortal.Models.Document.DocumentEntity>(doc))
                                               select new ProjectDocumentEntity
                                               {
                                                   DocumentName = row["DocumentName"].ToString(),

                                               }).FirstOrDefault();

                return docds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ProjectDocumentEntity GetProjectDocumentByID(int DocId)
        {
            ProjectDocumentEntity doc = new ProjectDocumentEntity();
            doc = (from row in documentDAL.DocumentSelectById(DocId)
                   select new ProjectDocumentEntity
                   {
                       DocumentID = Convert.ToInt32(row["DocumentID"]),
                       DocumentName = row["DocumentName"].ToString(),
                       DocumentTypeID = Convert.ToInt32(row["DocumentTypeID"]),
                       Description = row["Description"].ToString(),
                       ExpiredDate = Convert.ToString(row["Expireddate"]).Replace(" 12:00:00 AM", ""),
                       StatusId = Convert.ToInt32(row["StatusId"]),
                       CompanyName = row["CompanyName"].ToString(),
                       ProjectName = row["ProjectName"].ToString(),
                       JobName = row["JobName"].ToString(),
                       IsPublished = Convert.ToBoolean(row["IsPublished"]),
                       DocumentPath = row["DocumentPath"].ToString()
                   }).FirstOrDefault();
            return doc;
        }
        public ProjectChartResult GetProjectChart(DataTableModel model)
        {
            ProjectChartResult projectChartResult = new ProjectChartResult();

            List<ProjectChartEntity> Piechart = new List<ProjectChartEntity>();
            List<ProjectChartMilestone> Stackedchart = new List<ProjectChartMilestone>();

            List<ProjectCount> projectCount = new List<ProjectCount>();

            var result = base.GetProjectMilestoneChart(model);

            projectCount = (from row in result.Item1
                            select new ProjectCount
                            {
                                SystemDate = row.ItemArray[0].ToString(),
                                ProjCount = row.ItemArray[1].ToString()
                            }).ToList();



            Stackedchart = (from row in result.Item3
                            select new ProjectChartMilestone
                            {
                                KeyData = row.ItemArray[0].ToString()

                            }).ToList();


            Piechart = (from row in result.Item2
                        select new ProjectChartEntity
                        {
                            StatusName = row.ItemArray[0].ToString(),
                            ProjectCount = row.ItemArray[1].ToString()

                        }).ToList();

            projectChartResult.lstProjectCount = projectCount;
            projectChartResult.lstProjectChartEntity = Piechart;
            projectChartResult.lstProjectChartMilestone = Stackedchart;


            return projectChartResult;
        }
        public List<ProjectMilestoneStatusEntity> DashboardProjectListingMilestone(DataTableModel model)
        {
            List<ProjectMilestoneStatusEntity> projectListResult = new List<ProjectMilestoneStatusEntity>();

            ProjectDAL obj = new ProjectDAL();

            projectListResult = (from row in obj.DashboardProjectListingMilestone(model)
                                 select new ProjectMilestoneStatusEntity
                                 {
                                     ProjectNo = Convert.ToString(row["ProjectNo"]),
                                     JobNo = Convert.ToString(row["JobNo"]),
                                     ProjectName = Convert.ToString(row["ProjectName"]),
                                     ContractExecuted = Convert.ToString(row["Contract Executed"]),
                                     DownPaymentRecord = Convert.ToString(row["Down Payment Rec'd"]),
                                     DrawingsApproved = Convert.ToString(row["Drawings Approved"]),
                                     FinishesComplete = Convert.ToString(row["Finishes Complete"]),
                                     InstallationStart = Convert.ToString(row["Installation Start"]),
                                     ReadyToPull = Convert.ToString(row["Job-Site Ready"]),
                                     MaterialInStorageHub = Convert.ToString(row["Material in storage Hub"]),
                                     MaterialOrdered = Convert.ToString(row["Material Ordered"]),
                                     //MaterialAtHub = Convert.ToString(row["Material in storage Hub"]),
                                     EquipmentTurnover = Convert.ToString(row["Equipment Turnover"]),
                                     //FinalInspection = Convert.ToString(row["Equipment Turnover"]),
                                     Title = Convert.ToString(row["Title"]),
                                     DataType = Convert.ToString(row["DataType"]),
                                     TotalProjectCount = Convert.ToInt32(row["TotalRows"]),
                                     DueDate = Convert.ToString(row["DueDate"]),
                                     PaidPercent = Convert.ToString(row["PaidPercent"]),
                                     BilledPercent = Convert.ToString(row["BilledPercent"]),
                                     ProjectCompanyId = Convert.ToInt32(row["ProjectCompanyID"]),

                                     ContractExecutedDate = Convert.ToString(row["Contract Executed Date"]),
                                     DownPaymentRecordDate = Convert.ToString(row["Down Payment Rec'd Date"]),
                                     DrawingsApprovedDate = Convert.ToString(row["Drawings Approved Date"]),
                                     FinishesCompleteDate = Convert.ToString(row["Finishes Complete Date"]),
                                     InstallationStartDate = Convert.ToString(row["Installation Start Date"]),
                                     ReadyToPullDate = Convert.ToString(row["Job-Site Ready Date"]),
                                     MaterialInStorageHubDate = Convert.ToString(row["Material in storage Hub Date"]),
                                     MaterialOrderedDate = Convert.ToString(row["Material Ordered Date"]),
                                     //MaterialAtHub = Convert.ToString(row["Material in storage Hub"]),
                                     EquipmentTurnoverDate = Convert.ToString(row["Equipment Turnover Date"])
                                 }).ToList();

            List<ProjectMilestoneStatusEntity> onlyProjectRecords = projectListResult.Where(x => x.DataType == "Project Data").ToList();

            foreach (var item in onlyProjectRecords)
            {
                var onlyProjectRelatedBanksRecords = projectListResult.Where(x => x.DataType == "Job Data" && x.ProjectNo == item.ProjectNo).ToList();

                item.BankList.AddRange(onlyProjectRelatedBanksRecords);
            }

            return onlyProjectRecords;
        }

        public List<ProjectStatusDueDateEntity> GetDueDateAndStatusOfMilestone(ProjectJobDueStatusModel model)
        {
            List<ProjectStatusDueDateEntity> projectListResult = new List<ProjectStatusDueDateEntity>();

            ProjectDAL obj = new ProjectDAL();

            projectListResult = (from row in obj.GetDueDateAndStatusOfMilestone(model)
                                 select new ProjectStatusDueDateEntity
                                 {
                                     ProjectNo = Convert.ToString(row["ProjectNo"]),
                                     JobNo = Convert.ToString(row["JobNo"]),
                                     StatusName = projectMilestoneService.setMilestoneNewName(Convert.ToString(row["StatusName"])),
                                     CompletedDate = Convert.ToString(row["CompletedDate"]),
                                     DueDate = Convert.ToString(row["DueDate"]),
                                     Milestone = Convert.ToString(row["Milestone"]),
                                     Milestoneid = Convert.ToInt32(row["Milestoneid"]),
                                     StatusId = Convert.ToInt32(row["StatusId"])
                                 }).ToList();

            return projectListResult;
        }

        public bool AddProjectBankMilestoneComment(string comment, string projectBankTitle, int userID)
        {
            ProjectDAL obj = new ProjectDAL();
            var result = obj.AddProjectBankMilestoneComment(comment, projectBankTitle, userID);
            return result;
        }

        public List<ProjectMilestoneEntity> GetJobDataDateInformation(DataTableModel model)
        {
            List<ProjectMilestoneEntity> projectListResult = new List<ProjectMilestoneEntity>();

            ProjectDAL obj = new ProjectDAL();

            projectListResult = (from row in obj.GetJobData(model)
                                 select new ProjectMilestoneEntity
                                 {
                                     ProjectNo = Convert.ToString(row["ProjectNo"]),
                                     ProjectName = Convert.ToString(row["ProjectName"]),
                                     JobNo = Convert.ToString(row["JobNo"]),
                                     JobDesc = Convert.ToString(row["BankDesc"]),
                                     MilestoneId = Convert.ToInt32(row["MilestoneId"]),
                                     MilestoneShortName = Convert.ToString(row["Milestone"]),
                                     MilestoneName = Convert.ToString(row["MilestoneFullName"]),
                                     StatusName = Convert.ToString(row["Status"]),
                                     Date = Convert.ToString(row["Date"]).Substring(0, 10),
                                     Color = ((Convert.ToString(row["Status"]) == "O") ? "#ec4a4a" :
                                             (Convert.ToString(row["Status"]) == "C") ? "#15bb15" :
                                             (Convert.ToString(row["Status"]) == "P") ? "#bbbbbb" : "#ffdf36").ToString()
                                 }).ToList();

            return projectListResult;
        }

        public DataTable GetAllBankListWithDetail(DataTableModel obj, ref int totalcount, ref int projectCompanyId)
        {
            List<JobListWithParameter> projectlst = new List<JobListWithParameter>();
            return base.GetBankListWithDetail(obj, ref totalcount, ref projectCompanyId);

        }

        public string DashboardPaymentDocAndContactData(DataTableModel model)
        {
            DashboardChartDocAndContactData allDashboardData = new DashboardChartDocAndContactData();

            ProjectDAL obj = new ProjectDAL();

            var result = obj.GetDashboardPieDocInvoiceAndContact(model);

            string data = result[0].ItemArray[0].ToString();

            return data;

            // List<object> list = JsonConvert.DeserializeObject<List<object>>(data);

            //dynamic json = JsonConvert.DeserializeObject(result[0].ItemArray[0].ToString());
            //allDashboardData.ProjectPieChartData = (from row in result
            //             select new ProjectPieChartResult
            //             {
            //                 ReceivedDataValue = Convert.ToInt32(row["ReceivedDataValue"].ToString()),
            //                 BalanceOwedDataValue = Convert.ToInt32(row["BalanceOwedDataValue"].ToString())
            //             }).FirstOrDefault();


            //allDashboardData.lstProjectDocumentData = (from row in result
            //                   select new ProjectDocumentResult
            //                   {
            //                       DocID = Convert.ToInt32(row["DocID"].ToString()),
            //                       DocName = row["DocName"].ToString(),
            //                       DocLink = row["DocLink"].ToString()
            //                   }).ToList();


            //allDashboardData.lstProjectInvoiceData= (from row in result
            //                   select new ProjectDocumentResult
            //                   {
            //                       DocID = Convert.ToInt32(row["DocID"].ToString()),
            //                       DocName = row["DocName"].ToString(),
            //                       DocLink = row["DocLink"].ToString()
            //                   }).ToList();


            //allDashboardData.lstProjectContactData = (from row in result
            //                  select new ProjectContactResult
            //                  {
            //                      Name = row["DocID"].ToString(),
            //                      Email = row["DocName"].ToString()
            //                  }).ToList();


        }

        public bool Update_BankDescByJobNo(string Description, string UpdatedBy, int appAuditID, string SessionID, string JobNo, string ApplicationOperation)
        {
            return base.UpdateBankDescByJobNo(Description, UpdatedBy, appAuditID, SessionID, JobNo, ApplicationOperation);
        }

        public bool editEmailAndPhoneData(DataTableModel model)
        {
            ProjectDAL obj = new ProjectDAL();
            return (obj.updateEmailAndPhoneData(model));
        }
    }
}