﻿using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using SchindlerCustomerPortal.Models.Job;
using SchindlerCustomerPortal.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.Project
{
    public class ProjectService : IProjectService
    {
        ProjectContactDAL projectContactDAL = new ProjectContactDAL();

        public bool AddProjectContact(ProjectContactEntity user)
        {
            var result = projectContactDAL.AddProjectContact(user);
            return result;
        }

        public ProjectContactEntity ProjectContactSelectById(int contactID)
        {
            ProjectContactEntity user = new ProjectContactEntity();
            user = (from row in projectContactDAL.ProjectContactSelectById(contactID)
                    select new ProjectContactEntity
                    {
                        ContactID = Convert.ToInt32(row["ContactId"]),
                        ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        //LastName = row["LastName"].ToString(),
                        DesignationId = Convert.ToInt32(row["Designation"]),
                        Designation = Convert.ToString(row["DesignationName"]),
                        Email = Convert.ToString(row["Email"]),
                        ContactNumber = Convert.ToString(row["PhoneNo"]) == "" ? string.Empty : row["PhoneNo"].ToString().Insert(3, "-").Insert(7, "-"),
                        //Address = row["JobStreetAddress"].ToString(),
                        //City = row["City"].ToString(),
                        //State = row["State"].ToString(),
                        //PostalCode = row["Zip"].ToString(),
                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                        JobName = Convert.ToString(row["JobName"]),
                        ProjectName = Convert.ToString(row["ProjectName"])
                    }).FirstOrDefault();
            return user;
        }

        public bool UpdateProjectContact(ProjectContactEntity user)
        {
            var result = projectContactDAL.UpdateProjectContact(user);
            return result;
        }

        public bool DeleteProjectContact(ProjectContactEntity user)
        {
            var result = projectContactDAL.DeleteProjectContact(user);
            return result;
        }

        public List<ProjectContactEntity> GetProjectContactList(DataTableModel model, ref int count)
        {
            var userList = new List<ProjectContactEntity>();
            try
            {
                userList = (from row in projectContactDAL.ProjectContactList(model, ref count)
                            select new ProjectContactEntity
                            {
                                ContactID = Convert.ToInt32(row["ContactId"]),
                                ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                                FirstName = Convert.ToString(row["UserName"]),
                                //LastName = row["LastName"].ToString(),
                                Designation = Convert.ToString(row["DesignationName"]),
                                Email = Convert.ToString(row["Email"]),
                                ContactNumber = Convert.ToString(row["PhoneNo"]) == "" ? string.Empty : row["PhoneNo"].ToString().Insert(3, "-").Insert(7, "-"),
                                //Address = row["JobStreetAddress"].ToString(),
                                //City = row["City"].ToString(),
                                //State = row["State"].ToString(),
                                //PostalCode = row["Zip"].ToString(),
                                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                JobName = Convert.ToString(row["JobName"]),
                                JobNo = Convert.ToString(row["JobNo"]),
                                BankDesc = Convert.ToString(row["BankDesc"]),
                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<JobEntity> GetAllJobByProjectNoandSearchTerm(int projectNo, string searchTerm)
        {
            var jobList = new List<JobEntity>();
            try
            {
                jobList = (from row in projectContactDAL.GetJobListByProjectNoandSearchTerm(projectNo, searchTerm)
                            select new JobEntity
                            {
                                JobId = Convert.ToInt32(row["JobId"]),
                                JobName = Convert.ToString(row["JobName"]),
                                JobNo = Convert.ToString(row["JobNo"])
                            }).ToList();

                return jobList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProjectAssignEntity> GetAllProjectByCompanyIdandSearchTerm(int CompanyId, string searchTerm, int RoleId, int UserId)
        {
            var jobList = new List<ProjectAssignEntity>();
            try
            {
                jobList = (from row in projectContactDAL.GetProjectListByCompanyIdandSearchTerm(CompanyId, searchTerm, RoleId, UserId)
                           select new ProjectAssignEntity
                           {
                               Company = Convert.ToString(row["CompanyName"]),
                               ProjectNo = Convert.ToString(row["ProjectNo"]),
                               ProjectName = Convert.ToString(row["ProjectName"]),
                               CheckAssigned = ((!string.IsNullOrEmpty(row["CheckAssigned"].ToString())) ? "checked" : ""),
                               UnSubscribedCheckAssigned = (((row["IsUnSubscribedProjectNo"].ToString()) != "0") ? "checked" : ""),
                               IsUnSubscribedUserMst = Convert.ToBoolean(row["IsUnSubscribedUserMst"])
                           }).ToList();

                return jobList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProjectContactEntity> GetProjectContactListByCompanyId(DataTableModel model, ref int count)
        {
            var userList = new List<ProjectContactEntity>();
            try
            {
                userList = (from row in projectContactDAL.ProjectContactListByCompanyId(model, ref count)
                            select new ProjectContactEntity
                            {
                                ContactID = Convert.ToInt32(row["ContactId"]),
                                ProjectNo = Convert.ToInt32(row["ProjectNo"]),
                                ProjectName = Convert.ToString(row["ProjectName"]),
                                FirstName = Convert.ToString(row["UserName"]),
                                //LastName = row["LastName"].ToString(),
                                Designation = Convert.ToString(row["DesignationName"]),
                                Email = Convert.ToString(row["Email"]),
                                ContactNumber = Convert.ToString(row["PhoneNo"]) == "" ? string.Empty : row["PhoneNo"].ToString().Insert(3, "-").Insert(7, "-"),
                                //Address = row["JobStreetAddress"].ToString(),
                                //City = row["City"].ToString(),
                                //State = row["State"].ToString(),
                                //PostalCode = row["Zip"].ToString(),
                                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                JobName = Convert.ToString(row["JobName"]),
                                JobNo = Convert.ToString(row["JobNo"]),
                                BankDesc = Convert.ToString(row["BankDesc"]),
                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}