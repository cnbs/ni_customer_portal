﻿using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchindlerCustomerPortal.Services.User
{
    public interface IUserService
    {
        UserEntity Find(string Email);

        UserEntity ForgotPwdService(string Email);

        bool UpdatePwdService(string Token, string Password, string PasswordSalt, int AccountStatus);

        UserEntity GetUserById(int userId);

        List<UserEntity> GetAllUserByCompanyId(DataTableModel model, ref int count);

        int AddUser(UserEntity user);
        bool AddNotification(NotificationEntity data);

        bool UpdateUser(UserEntity user);

        bool DeleteUser(UserEntity user);

        void SetTokenService(string Token, int UserID, int ExpireToken);

        UserEntity GetTokenExpiration(string Token);

        UserEntity GetUserEmailFromToken(string Token);

        UserEntity RegisterUserInsert(UserEntity user);

        bool changePasswordForUser(ChangePassword user);

        List<UserPermissionViewModel> GetUserModulePermission(int userid);

        bool DeleteMultipleUser(UserEntity user);

        CompanyEntity CheckCompanyNameService(string CompanyName);

        CompanyEntity CheckCompanyNameServiceNew(string CompanyName);

        int InsertUpdateUserModuleAccess(UserPermissionViewModel obj);

        int InsertUpdateUserModuleAccessPermissionByModule(UserPermissionModuleModel obj);

        List<UserPermissionViewModel> GetUserModulePermissionForMenu(int userid, int parentId);

        UserPermissionViewModel CheckUserModulePermission(int userId, string moduleName);

        bool UserModuleAccess_UpdateAll(UserPermissionModuleModelMulti obj);

        bool UpdateUserStatus(UserEntity user);

        List<UserEntity> GetAllNewRegisteredUser(DataTableModel model, ref int count);

        int GetAllNewRegisteredUsersCount(int CompanyID,ref int count);

        UserEntity GetNewRegisteredUserById(int userId);

        bool UpdateNewRegisteredUser(UserEntity user);

        bool UpdateuserProjectAssign(UserProjectAssignEntity user);

        List<UserProjectAssignEntity> GetUserProjectAssign(UserProjectAssignEntity model);
        bool CheckCompanyforEditNewRegisterUser(string companyName);
        List<UserEntity> CheckPermissionForMailNotification(int companyId);

    }
}