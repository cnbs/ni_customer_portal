﻿using SchindlerCustomerPortal.Common;
using SchindlerCustomerPortal.DAL;
using SchindlerCustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace SchindlerCustomerPortal.Services.User
{
    public class UserService : IUserService
    {
        UserDAL userdal = new UserDAL();

        public UserEntity Find(string Email)
        {
            try
            {
                UserEntity userds = (from row in userdal.LoginNow(Email)
                                     select new UserEntity
                                     {

                                         Email = row["Email"].ToString(),
                                         RoleID = Convert.ToInt32(row["RoleID"]),
                                         UserID = Convert.ToInt32(row["UserID"]),
                                         FirstName = row["FirstName"].ToString(),
                                         LastName = row["LastName"].ToString(),
                                         CompanyID = Convert.IsDBNull(row["CompanyID"]) ? 0 : Convert.ToInt32(row["CompanyID"]),
                                         CompanyName = row["CompanyName"].ToString(),
                                         ContactNumber = row["ContactNumber"].ToString(),
                                         ProfilePic = string.IsNullOrEmpty(row["ProfilePic"].ToString()) ? "" : row["ProfilePic"].ToString(),
                                         //CreatedBy = row["CreatedBy"].ToString(),
                                         //UpdatedBy = row["UpdatedBy"].ToString(),
                                         Password = row["Password"].ToString(),
                                         PasswordSalt = row["Password_Salt"].ToString(),
                                         AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                         DeviceID = Convert.ToString(row["DeviceId"]),
                                         DeviceType = Convert.ToString(row["DeviceType"]),
                                         DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"])

                                     }).FirstOrDefault();

                return userds;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public UserEntity ForgotPwdService(string Email)
        {

            try
            {

                UserEntity user = (from row in userdal.CheckForgotPwdEmail(Email)
                                   select new UserEntity
                                   {
                                       Email = row["Email"].ToString(),
                                       FirstName = row["FirstName"].ToString(),
                                       UserID = Convert.ToInt32(row["UserID"]),
                                       AccountStatus = Convert.ToInt32(row["AccountStatus"])
                                   }).FirstOrDefault();


                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdatePwdService(string Token, string Password, string PasswordSalt, int AccountStatus)
        {
            try
            {
                bool CheckUpdated = userdal.UpdatePassword(Token, Password, PasswordSalt, AccountStatus);

                return CheckUpdated;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetTokenService(string Token, int UserID, int ExpireToken)
        {
            try
            {
                bool CheckTokenSet = userdal.SetTokenOnEmail(Token, UserID, ExpireToken);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserEntity GetTokenExpiration(string Token)
        {
            try
            {
                UserEntity userds = (from row in userdal.GetExpireTokenDateTime(Token)
                                     select new UserEntity
                                     {
                                         TokenExpireDate = Convert.ToDateTime(row["TokenExpireDate"]),

                                     }).FirstOrDefault();

                return userds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserEntity GetUserEmailFromToken(string Token)
        {
            try
            {
                UserEntity userds = (from row in userdal.GetUserEmailFromToken(Token)
                                     select new UserEntity
                                     {
                                         Email = Convert.ToString(row["Email"]),
                                         UserID = Convert.ToInt32(row["UserID"])

                                     }).FirstOrDefault();

                return userds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserEntity GetUserById(int userId)
        {
            UserEntity user = new UserEntity();
            user = (from row in userdal.CompanyUserSelectById(userId)
                    select new UserEntity
                    {
                        Email = row["Email"].ToString(),
                        RoleID = Convert.ToInt32(row["RoleID"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        //LastName = row["LastName"].ToString(),
                        CompanyID = Convert.ToInt32(row["CompanyID"]),
                        CompanyName = Convert.ToString(row["CompanyName"]),
                        ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                        Designation = row["DesignationName"].ToString(),
                        DesignationId = Convert.ToInt32(row["DesignationId"]),
                        Address1 = Convert.ToString(row["Address1"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        PostalCode = Convert.ToString(row["PostalCode"]),
                        AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                        Requested_Projects= Convert.ToString(row["Requested_Projects"]),
                        Unsubscribe = Convert.ToBoolean(row["IsUnSubscribed"])
                        //CreatedBy = row["CreatedBy"].ToString(),
                        //UpdatedBy = row["UpdatedBy"].ToString(),

                    }).FirstOrDefault();
            return user;
        }

        public UserEntity RegisterUserInsert(UserEntity user)
        {
            try
            {
                UserEntity userds = (from row in userdal.Registration(user)
                                     select new UserEntity
                                     {
                                         //TokenExpireDate = Convert.ToDateTime(row["TokenExpireDate"]),
                                         Done = row["Email"].ToString(),
                                         Email = row["emailid"].ToString(),
                                         UserID = Convert.ToInt32(row["UserID"]),
                                         FirstName = row["FirstName"].ToString(),
                                         CompanyID = Convert.ToInt32(row["CompanyID"])

                                     }).FirstOrDefault();

                return userds;
            }
            catch (Exception ex)
            {
                ErrorLogClass lm = new ErrorLogClass();
                lm.CreateLogFiles();
                var Errorlogpath = WebConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                lm.ErrorLog(Errorlogpath, ex.Message + " - Company");
                throw ex;

            }
        }

        public List<UserEntity> GetAllUserByCompanyId(DataTableModel model, ref int count)
        {
            var userList = new List<UserEntity>();
            try
            {
                userList = (from row in userdal.CompanyUserSelect(model, ref count)
                            select new UserEntity
                            {

                                Email = row["Email"].ToString(),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = row["UserName"].ToString(),
                                //LastName = row["LastName"].ToString(),
                                //CompanyID = Convert.ToInt32(row["CompanyID"]),
                                //CompanyName = row["CompanyName"].ToString(),
                                ContactNumber = Convert.ToString(row["ContactNumber"]).Insert(3, "-").Insert(7, "-"),
                                //var Newphone = Phone.Insert(3, "-").Insert(7, "-");
                                // ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                Designation = row["DesignationName"].ToString(),
                                DesignationId = Convert.ToInt32(row["DesignationId"]),
                                Requested_Projects = row["Requested_Projects"].ToString()
                                //HasViewPermission = Convert.ToBoolean(row["View_Permission"])
                                //CreatedBy = row["CreatedBy"].ToString(),
                                //UpdatedBy = row["UpdatedBy"].ToString(),

                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUser(UserEntity user)
        {
            int result = userdal.InsertUser(user);
            return result;
        }

        public bool AddNotification(NotificationEntity data)
        {
            bool result = userdal.InsertNotification(data);
            return result;
        }

        public bool UpdateUser(UserEntity user)
        {
            bool result = userdal.UpdateUser(user);
            return result;
        }

        public bool DeleteUser(UserEntity user)
        {
            var result = userdal.DeleteCompanyUser(user);
            return result;
        }

        public bool changePasswordForUser(ChangePassword user)
        {
            bool result = userdal.UpdateUserPassword(user);
            return result;
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public List<UserPermissionViewModel> GetUserModulePermission(int userid)
        {
            List<UserPermissionViewModel> UserPermissionlst = new List<UserPermissionViewModel>();
            UserPermissionlst = (from row in userdal.GetModulesPermissionByUser(userid)
                                 select new UserPermissionViewModel
                                 {

                                     UserID = Convert.ToInt32(row["UserID"]),
                                     ModuleId = Convert.ToInt32(row["ModuleId"]),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                                     HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                                     HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                                     ModuleName = StripHTML((row["DisplayName"]).ToString()),
                                     ParentID = Convert.ToInt32(row["ParentID"]),

                                 }).ToList();

            return UserPermissionlst;

        }

        public int InsertUpdateUserModuleAccess(UserPermissionViewModel obj)
        {
            return userdal.InsertUpdateUserModuleAccess(obj);
        }
        public int InsertUpdateUserModuleAccessPermissionByModule(UserPermissionModuleModel obj)
        {
            return userdal.InsertUpdateUserModuleAccessPermissionByModule(obj);
        }

        public bool UserModuleAccess_UpdateAll(UserPermissionModuleModelMulti obj)
        {
            return userdal.UserModuleAccess_UpdateAll(obj);
        }

        public bool DeleteMultipleUser(UserEntity user)
        {
            var result = userdal.DeleteMultipleCompanyUser(user);
            return result;
        }

        public CompanyEntity CheckCompanyNameService(string CompanyName)
        {
            try
            {

                CompanyEntity Company = (from row in userdal.CheckCompanyName(CompanyName)
                                         select new CompanyEntity
                                         {
                                             CheckCompany = Convert.ToInt32(row["CheckName"]),
                                             CompanyID = Convert.ToInt32(row["CompanyID"]),
                                             CompanyName = row["CompanyName"].ToString() == "No" ? "" : row["CompanyName"].ToString()

                                         }).FirstOrDefault();


                return Company;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public CompanyEntity CheckCompanyNameServiceNew(string CompanyName)
        {
            try
            {

                CompanyEntity Company = (from row in userdal.CheckCompanyNameNew(CompanyName)
                                         select new CompanyEntity
                                         {
                                             CheckCompany = Convert.ToInt32(row["CheckName"]),
                                             CompanyID = Convert.ToInt32(row["CompanyID"]),
                                             CompanyName = row["CompanyName"].ToString() == "No" ? "" : row["CompanyName"].ToString()

                                         }).FirstOrDefault();


                return Company;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<UserPermissionViewModel> GetUserModulePermissionForMenu(int userid, int parentId)
        {
            List<UserPermissionViewModel> UserPermissionlst = new List<UserPermissionViewModel>();
            UserPermissionlst = (from row in userdal.GetModulesPermissionForDynamicMenu(userid, parentId)
                                 select new UserPermissionViewModel
                                 {

                                     UserID = Convert.ToInt32(row["UserID"]),
                                     ModuleId = Convert.ToInt32(row["ModuleId"]),
                                     ModuleURL = (row["URL"]).ToString(),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                                     HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                                     HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                                     ModulePermissionName = (row["ModulePermissionName"]).ToString(),
                                     ModuleDisplayName = (row["DisplayName"]).ToString(),
                                     SequenceNO = Convert.ToInt32(row["SequenceNO"]),
                                     Restricted = Convert.ToInt32(row["Restricted"]),
                                     VisibleOnMenu = Convert.ToInt32(row["VisibleOnMenu"]),

                                 }).ToList();

            return UserPermissionlst;

        }

        public UserPermissionViewModel CheckUserModulePermission(int userId, string moduleName)
        {
            UserPermissionViewModel UserPermissionlst = new UserPermissionViewModel();
            UserPermissionlst = (from row in userdal.CheckModulePermission(userId, moduleName)
                                 select new UserPermissionViewModel
                                 {

                                     UserID = Convert.ToInt32(row["UserID"]),
                                     ModuleId = Convert.ToInt32(row["ModuleId"]),
                                     ModuleURL = (row["URL"]).ToString(),
                                     HasViewPermission = Convert.ToBoolean(row["View_Permission"]),
                                     HasAddPermission = Convert.ToBoolean(row["Add_Permission"]),
                                     HasEditPermission = Convert.ToBoolean(row["Edit_Permission"]),
                                     HasDeletePermission = Convert.ToBoolean(row["Delete_Permission"]),
                                     HasPrintPermission = Convert.ToBoolean(row["Print_Permission"]),
                                     ModulePermissionName = (row["ModulePermissionName"]).ToString(),
                                     ModuleDisplayName = (row["DisplayName"]).ToString(),
                                     SequenceNO = Convert.ToInt32(row["SequenceNO"]),
                                     Restricted = Convert.ToInt32(row["Restricted"]),
                                     VisibleOnMenu = Convert.ToInt32(row["VisibleOnMenu"]),

                                 }).FirstOrDefault();

            return UserPermissionlst;

        }

        public bool UpdateUserStatus(UserEntity user)
        {
            var result = userdal.UpdateUserAccountStatus(user);
            return result;
        }

        public List<UserEntity> GetAllNewRegisteredUser(DataTableModel model, ref int count)
        {
            var userList = new List<UserEntity>();
            try
            {
                userList = (from row in userdal.NewlyRegisteredUser(model, ref count)
                            select new UserEntity
                            {
                                Email = Convert.ToString(row["Email"]),
                                RoleID = Convert.ToInt32(row["RoleID"]),
                                UserID = Convert.ToInt32(row["UserID"]),
                                FirstName = Convert.ToString(row["UserName"]),
                                CompanyName = Convert.ToString(row["CompanyName"]),
                                ContactNumber = Convert.ToString(row["ContactNumber"]).Insert(3, "-").Insert(7, "-"),
                                //ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                                IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                                AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                                Designation = Convert.ToString(row["DesignationName"]),
                                DesignationId = Convert.ToInt32(row["DesignationId"]),
                                RegisteredDate = Convert.ToString(row["RegisteredDate"]),
                                Requested_Projects= Convert.ToString(row["Requested_Projects"])
                            }).ToList();

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetAllNewRegisteredUsersCount(int CompanyID,ref int count)
        {  
            try
            {
                int i = userdal.NewlyRegisteredUserCount(CompanyID,ref count);
                            

                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        public UserEntity GetNewRegisteredUserById(int userId)
        {
            UserEntity user = new UserEntity();
            user = (from row in userdal.NewlyRegisteredUserEdit(userId)
                    select new UserEntity
                    {
                        Email = row["Email"].ToString(),
                        RoleID = Convert.ToInt32(row["RoleID"]),
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        CompanyName = row["CompanyName"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString().Insert(3, "-").Insert(7, "-"),
                        // ProfilePic = CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        ProfilePic = File.Exists(HttpContext.Current.Server.MapPath(CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString())) == true ? CommonConfiguration.userProfilePicPath + "Thumb_" + row["ProfilePic"].ToString() : CommonConfiguration.userProfilePicPath + row["ProfilePic"].ToString(),
                        IsDeleted = Convert.ToInt32(row["IsDeleted"]),
                        Designation = row["DesignationName"].ToString(),
                        DesignationId = Convert.ToInt32(row["DesignationId"]),
                        Address1 = Convert.ToString(row["Address1"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        PostalCode = Convert.ToString(row["PostalCode"]),
                        AccountStatus = Convert.ToInt32(row["AccountStatus"]),
                        Requested_Projects = Convert.ToString(row["Requested_Projects"])
                    }).FirstOrDefault();
            return user;
        }

        public bool UpdateNewRegisteredUser(UserEntity user)
        {
            bool result = userdal.NewlyRegisteredUserUpdate(user);
            return result;
        }

        public bool UpdateuserProjectAssign(UserProjectAssignEntity user)
        {
            bool result = userdal.UpdateUserProjectAssign(user);
            return result;
        }

        public List<UserProjectAssignEntity> GetUserProjectAssign(UserProjectAssignEntity model)
        {
            List<UserProjectAssignEntity> user = new List<UserProjectAssignEntity>();
            user = (from row in userdal.GetUserProjectAssign(model)
                    select new UserProjectAssignEntity
                    {
                        ProjectNo = Convert.ToString(row["ProjectNo"]),
                        ProjectName = Convert.ToString(row["ProjectName"])
                    }).ToList();
            return user;
        }

        public bool CheckCompanyforEditNewRegisterUser(string companyName)
        {
            try
            {
                bool CheckUpdated = userdal.CheckCompanyforEditNewRegisterUser(companyName);

                return CheckUpdated;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserEntity> CheckPermissionForMailNotification(int companyId)
        {
            List<UserEntity> user = new List<UserEntity>();
            user = (from row in userdal.CheckPermissionForMailNotification(companyId)
                    select new UserEntity
                    {
                        Email = Convert.ToString(row["Email"]),
                        FirstName = row["FirstName"].ToString(),
                        UserID=    Convert.ToInt32(row["UserID"]),
                        DeviceID = Convert.ToString(row["DeviceId"]),
                        DeviceType = Convert.ToString(row["DeviceType"]),
                        DeviceLastLoginDateTime = Convert.ToString(row["DeviceLastLoginDateTime"])
                    }).ToList();
            return user;
        }
    }
}